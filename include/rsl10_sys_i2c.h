/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_i2c.h
 * - Inter-Integrated Circuit (I2C) interface support
 * ----------------------------------------------------------------------------
 * $Revision: 1.10 $
 * $Date: 2017/07/25 13:31:40 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_I2C_H
#define RSL10_SYS_I2C_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * I2C interface support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_I2C_Config(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the I2C interface for operation
 * Inputs        : cfg    - I2C interface configuration; use
 *                          I2C_MASTER_SPEED_*,
 *                          a slave address constant shifted to
 *                          I2C_CTRL0_SLAVE_ADDRESS_Pos,
 *                          I2C_CONTROLLER_[CM3 | DMA],
 *                          I2C_STOP_INT_[ENABLE | DISABLE],
 *                          I2C_AUTO_ACK_[ENABLE | DISABLE],
 *                          I2C_SAMPLE_CLK_[ENABLE | DISABLE], and
 *                          I2C_SLAVE_[ENABLE | DISABLE],
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_I2C_Config(uint32_t cfg)
{
    I2C->CTRL0 = (cfg & (I2C_CTRL0_SPEED_Mask                        |
                            I2C_CTRL0_SLAVE_ADDRESS_Mask                |
                            (1U << I2C_CTRL0_CONTROLLER_Pos)            |
                            (1U << I2C_CTRL0_STOP_INT_ENABLE_Pos)       |
                            (1U << I2C_CTRL0_AUTO_ACK_ENABLE_Pos)       |
                            (1U << I2C_CTRL0_I2C_SAMPLE_CLK_ENABLE_Pos) |
                            (1U << I2C_CTRL0_SLAVE_ENABLE_Pos)));
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_I2C_Get_Status(void)
 * ----------------------------------------------------------------------------
 * Description   : Get the current I2C interface status
 * Inputs        : None
 * Outputs       : status - Current I2C interface status; compare with
 *                          I2C_[NO_ERROR | ERROR]_S,
 *                          I2C_[BUS | NO_BUS]_ERROR_S,
 *                          I2C_START_[PENDING | NOT_PENDING],
 *                          I2C_MASTER_[ACTIVE | INACTIVE],
 *                          I2C_[DMA | NO_DMA]_REQUEST,
 *                          I2C_[STOP | NO_STOP]_DETECTED,
 *                          I2C_[DATA | NON_DATA]_EVENT,
 *                          I2C_[ERROR | NO_ERROR],
 *                          I2C_[BUS_ERROR | NO_BUS_ERROR],
 *                          I2C_BUFFER_[FULL | EMPTY],
 *                          I2C_CLK_[STRETCHED | NOT_STRETCHED],
 *                          I2C_BUS_[FREE | BUSY],
 *                          I2C_DATA_IS_[ADDR | DATA],
 *                          I2C_IS_[READ | WRITE],
 *                          I2C_ADDR_[GEN_CALL | OTHER] and
 *                          I2C_HAS_[NACK | ACK]
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_I2C_Get_Status(void)
{
    return I2C->STATUS;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_I2C_StartRead(uint32_t addr)
 * ----------------------------------------------------------------------------
 * Description   : Initialize an I2C master read transfer on the I2C interface
 * Inputs        : addr - I2C slave address to initiate a transfer with
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_I2C_StartRead(uint32_t addr)
{
    I2C->ADDR_START = (I2C_START_READ                           |
                       ((addr << I2C_ADDR_START_ADDRESS_Pos)    &
                        I2C_ADDR_START_ADDRESS_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_I2C_StartWrite(uint32_t addr)
 * ----------------------------------------------------------------------------
 * Description   : Initialize an I2C master write transfer on the I2C interface
 * Inputs        : addr - I2C slave address to initiate a transfer with
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_I2C_StartWrite(uint32_t addr)
{
    I2C->ADDR_START = (I2C_START_WRITE                          |
                       ((addr << I2C_ADDR_START_ADDRESS_Pos)    &
                        I2C_ADDR_START_ADDRESS_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_I2C_ACK(void)
 * ----------------------------------------------------------------------------
 * Description   : Manually acknowledge the latest transfer
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_I2C_ACK(void)
{
    I2C_CTRL1->ACK_ALIAS = I2C_ACK_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_I2C_NACK(void)
 * ----------------------------------------------------------------------------
 * Description   : Manually not-acknowledge the latest transfer (releases the
 *                 bus to continue with a transfer)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_I2C_NACK(void)
{
    I2C_CTRL1->NACK_ALIAS = I2C_NACK_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_I2C_NACKAndStop(void)
 * ----------------------------------------------------------------------------
 * Description   : Manually not-acknowledge the latest transfer and send a stop
 *                 condition (Master mode only)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_I2C_NACKAndStop(void)
{
    I2C_CTRL1->NACK_ALIAS = I2C_NACK_BITBAND;
    I2C_CTRL1->STOP_ALIAS = I2C_STOP_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_I2C_LastData(void)
 * ----------------------------------------------------------------------------
 * Description   : Indicate that this is the last byte in the transfer
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_I2C_LastData(void)
{
    I2C_CTRL1->LAST_DATA_ALIAS = I2C_LAST_DATA_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_I2C_Reset(void)
 * ----------------------------------------------------------------------------
 * Description   : Reset the I2C interface
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_I2C_Reset(void)
{
    I2C_CTRL1->RESET_ALIAS = I2C_RESET_BITBAND;
}


/* ----------------------------------------------------------------------------
 * Function      : void Sys_I2C_DIOConfig(uint32_t cfg, uint32_t scl,
 *                                         uint32_t sda)
 * ----------------------------------------------------------------------------
 * Description   : Configure two DIOs for the specified I2C interface
 * Inputs        : - cfg    - DIO pin configuration for the I2C pads
 *                 - scl    - DIO to use as the I2C SCL pad
 *                 - sda    - DIO to use as the I2C SDA pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_I2C_DIOConfig(uint32_t cfg, uint32_t scl,
                                        uint32_t sda)
{
    Sys_DIO_Config(scl, ((cfg) | DIO_MODE_SCL));
    Sys_DIO_Config(sda, ((cfg) | DIO_MODE_SDA));

    DIO->I2C_SRC = (((scl << DIO_I2C_SRC_SCL_Pos) & DIO_I2C_SRC_SCL_Mask)    |
                    ((sda << DIO_I2C_SRC_SDA_Pos) & DIO_I2C_SRC_SDA_Mask));
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_I2C_H */
