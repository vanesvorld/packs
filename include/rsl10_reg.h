/* ----------------------------------------------------------------------------
 * Copyright (c) 2014-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_reg.h
 * - ARM Cortex-M3 processor register support macros
 * ----------------------------------------------------------------------------
 * $Revision: 1.4 $
 * $Date: 2017/05/25 21:02:51 $
 * ------------------------------------------------------------------------- */ 
#ifndef RSL10_REG_H
#define RSL10_REG_H

/* ----------------------------------------------------------------------------
 * Core Register Setting Support Defines
 * ------------------------------------------------------------------------- */
#define XPSR_SATURATION_NOT_DETECTED    (0x0 << 27)
#define XPSR_SATURATION_DETECTED        (0x1 << 27)

#define XPSR_OVERFLOW_NOT_DETECTED      (0x0 << 28)
#define XPSR_OVERFLOW_DETECTED          (0x1 << 28)

#define XPSR_CARRY_NOT_DETECTED         (0x0 << 29)
#define XPSR_CARRY_DETECTED             (0x1 << 29)

#define XPSR_ZERO_NOT_DETECTED          (0x0 << 30)
#define XPSR_ZERO_DETECTED              (0x1 << 30)

#define XPSR_NEGATIVE_NOT_DETECTED      (0x0 << 31)
#define XPSR_NEGATIVE_DETECTED          (0x1 << 31)

#define PRIMASK_ENABLE_INTERRUPTS       0x0
#define PRIMASK_DISABLE_INTERRUPTS      0x1

#define FAULTMASK_ENABLE_INTERRUPTS     0x0
#define FAULTMASK_DISABLE_INTERRUPTS    0x1

/* ----------------------------------------------------------------------------
 * Support Macros
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Macro         : SYS_CALC_BITBAND(addr, pos)
 * ----------------------------------------------------------------------------
 * Description   : Calculate the bit-band location for a given address and bit
 *                 position
 * Inputs        : - addr   - The bit-band address for which the bit-band alias
 *                            is being calculated
 *                 - pos    - The bit for which the bit-band alias is being
 *                            calculated
 * Outputs       : return value  - The bit-band alias address corresponding 
 *                                 to this address and bit position
 * ------------------------------------------------------------------------- */
#define SYS_CALC_BITBAND(addr, pos)     (((addr) & 0xF0000000) \
                                         + 0x2000000 \
                                         + (((addr) & 0xFFFFF) << 5) \
                                         + ((pos) << 2))

#endif /* RSL10_REG_H */
