/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_dio.h
 * - Digital input/output support
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2017/09/20 17:26:01 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_DIO_H
#define RSL10_SYS_DIO_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Local defines
 * ------------------------------------------------------------------------- */
typedef struct
{
    __IO uint32_t ALIAS[16];
} DIO_DATA_Type;

#define DIO_DATA_BASE                   SYS_CALC_BITBAND((uint32_t)&DIO->DATA, 0)
#define DIO_DATA                        ((DIO_DATA_Type *) DIO_DATA_BASE)

/* ----------------------------------------------------------------------------
 * DIO peripheral support functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DIO_Config(uint32_t pad, uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the specified digital I/O
 * Inputs        : - pad    -   Digital I/O pad to configure; use a constant
 *                              between 0 and 15
 *                 - cfg    -   I/O configuration; use DIO_*X_DRIVE,
 *                              DIO_LPF_[ENABLE | DISABLE],
 *                              DIO_[NO_PULL | STRONG_PULL_UP | WEAK_PULL_UP |
 *                              WEAK_PULL_DOWN], and DIO_MODE_*
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DIO_Config(uint32_t pad, uint32_t cfg)
{
    DIO->CFG[pad] = cfg ;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DIO_Set_Direction(uint32_t dir)
 * ----------------------------------------------------------------------------
 * Description   : Set the input/output direction for any DIOs configured
 *                 as GPIOs
 * Inputs        : dir -    Input/output configuration for those DIOs
 *                          configured as GPIOs; use
 *                              DIO*_INPUT, and DIO*_OUTPUT
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DIO_Set_Direction(uint32_t dir)
{
    DIO->DIR = (dir & DIO_DIR_DIO_Mask);
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_DIO_Get_Mode(void)
 * ----------------------------------------------------------------------------
 * Description   : Get the DIO mode (DIO or GPIO)
 * Inputs        : None
 * Outputs       : return value - Current DIO/GPIO mode
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_DIO_Get_Mode(void)
{
    return DIO->MODE;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DIO_NMIConfig(uint32_t cfg, uint32_t nmi)
 * ----------------------------------------------------------------------------
 * Description   : Configure a DIO for the specified NMI input selection
 * Inputs        : - cfg    -   DIO pin configuration for the NMI input pad
 *                 - nmi    -   DIO to use as the NMI input pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DIO_NMIConfig(uint32_t cfg, uint32_t nmi)
{
    Sys_DIO_Config(nmi, ((cfg) | (DIO_MODE_INPUT)));
    DIO->NMI_SRC = (nmi << DIO_NMI_SRC_NMI_Pos);
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DIO_IntConfig(uint32_t index, uint32_t cfg,
 *                                        uint32_t dbnc_clk, uint32_t dbnc_cnt)
 * ----------------------------------------------------------------------------
 * Description   : Configure a DIO interrupt source
 * Inputs        : - index    - DIO interrupt source to configure; use [0- 3]
 *                 - cfg      - DIO interrupt configuration; use
 *                              DIO_DEBOUNCE_[DISABLE | ENABLE],
 *                              DIO_SRC_DIO_*, and
 *                              DIO_EVENT_[NONE | HIGH_LEVEL | LOW_LEVEL |
 *                              RISING_EDGE | FALLING_EDGE | TRANSITION]
 *                 - dbnc_clk - Interrupt button debounce filter clock; use
 *                              DIO_DEBOUNCE_SLOWCLK_DIV[32 | 1024]
 *                 - dbnc_cnt - Interrupt button debounce filter count
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DIO_IntConfig(uint32_t index, uint32_t cfg,
                                       uint32_t dbnc_clk, uint32_t dbnc_cnt)
{
    DIO->INT_CFG[index] = cfg;
    DIO->INT_DEBOUNCE = (dbnc_clk |
                        ((dbnc_cnt << DIO_INT_DEBOUNCE_DEBOUNCE_COUNT_Pos) &
                                DIO_INT_DEBOUNCE_DEBOUNCE_COUNT_Mask));
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_DIO_H */
