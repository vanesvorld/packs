/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_rtc.h
 * - Real-Time Clock functions and macros
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_RTC_H
#define RSL10_SYS_RTC_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Real Time Clock support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_RTC_Config(uint32_t start_value,
 *                                     uint32_t rtc_ctrl_cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure RTC block
 * Inputs        : - start_value     - Start value for the RTC timer
 *                                     counter; use a 32 bit value
 *                 - rtc_ctrl_cfg    - RTC control register; use
 *                                     RTC_RESET,
 *                                     RTC_FORCE_CLOCK,
 *                                     RTC_ALARM_*,
 *                                     RTC_[DISABLE | ENABLE], and
 *                                     RTC_CLK_SRC_[XTAL32K | RC_OSC]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_RTC_Config(uint32_t start_value,
                                    uint32_t rtc_ctrl_cfg)
{
    ACS->RTC_CFG = start_value;
    ACS->RTC_CTRL = rtc_ctrl_cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_RTC_Start(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Enable or disable the RTC
 * Inputs        : cfg       - Value for enabling or disabling RTC; use
 *                             RTC_[DISABLE | ENABLE]_BITBAND
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_RTC_Start(uint32_t cfg)
{
    ACS_RTC_CTRL->ENABLE_ALIAS = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_RTC_Value(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Read the current value of the RTC timer
 * Inputs        : None
 * Outputs       : return value - RTC timer counter current value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_RTC_Value(void)
{
    /* The read of the RTC_COUNT register is byte wise in the ACS bridge and
     * non-atomic. This can lead to read failures of the counter value due to
     * synchronization between SYSCLK and RTC_CLK clock domains.
     * To avoid the issue, we perform three reads to make sure the counter is
     * not clocked in the middle of the read.
     * Interrupts should be disabled to avoid potential delays between the
     * first and second read. */

    uint32_t read1 = ACS->RTC_COUNT;
    uint32_t read2 = ACS->RTC_COUNT;
    return (read1 != read2) ? ACS->RTC_COUNT : read1;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_RTC_H */
