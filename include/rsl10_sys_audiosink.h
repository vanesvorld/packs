/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2016 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_audiosink.h
 * - The audio sink functions and macros
 * ----------------------------------------------------------------------------
 * $Revision: 1.10 $
 * $Date: 2018/02/15 16:03:49 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_AUDIOSINK_H
#define RSL10_SYS_AUDIOSINK_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Audiosink inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_Audiosink_Counter(void)
 * ----------------------------------------------------------------------------
 * Description   : Read the value of the audio sink Clock counter
 * Inputs        : None
 * Outputs       : return value - The current value of the audio sink Clock counter
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_Audiosink_Counter(void)
{
    return AUDIOSINK->CNT;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_Audiosink_PhaseCounter(void)
 * ----------------------------------------------------------------------------
 * Description   : Read the value of the audio sink Clock phase counter
 * Inputs        : None
 * Outputs       : return value - The current value of the audio sink Clock
 *                                phase counter
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_Audiosink_PhaseCounter(void)
{
    return AUDIOSINK->PHASE_CNT;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_Audiosink_PeriodCounter(void)
 * ----------------------------------------------------------------------------
 * Description   : Read the value of the audio sink Clock period counter
 * Inputs        : None
 * Outputs       : return value - The current value of the audio sink Clock
 *                                period counter
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_Audiosink_PeriodCounter(void)
{
    return AUDIOSINK->PERIOD_CNT;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audiosink_Set_Ctrl(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the audio sink Clock control
 * Inputs        : cfg     - The control value for the audio sink; use
 *                           PHASE_CNT_[STOP | START], and
 *                           CNT_RESET
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Audiosink_Set_Ctrl(uint32_t cfg)
{
    AUDIOSINK->CTRL= cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audiosink_Config(uint32_t cfg,
 *                                    uint32_t phasecnt, uint32_t periodcnt)
 * ----------------------------------------------------------------------------
 * Description   : Configure the audio sink block and set values for clock
 *                 counter, clock phase counter and clock period counter
 * Inputs        : - cfg          - The number of the audio sink Clock periods
 *                                  over which the period counter measures;
 *                                  use AUDIO_SINK_PERIODS_*
 *                 - phasecnt     - The sink clock phase counter initial value
 *                 - periodcnt    - The sink clock period counter initial value
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Audiosink_Config(uint32_t cfg, uint32_t phasecnt,
                                            uint32_t periodcnt)
{
    AUDIOSINK->PHASE_CNT = phasecnt & AUDIOSINK_PHASE_CNT_PHASE_CNT_Mask;
    AUDIOSINK->PERIOD_CNT = periodcnt & AUDIOSINK_PERIOD_CNT_PERIOD_CNT_Mask;
    AUDIOSINK->CFG = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audiosink_InputClock(uint32_t cfg, uint32_t sink)
 * ----------------------------------------------------------------------------
 * Description   : Configure a source for the audio sink input
 * Inputs        : - cfg      - DIO pin configuration for the audio sink input
 *                 - sink     - Source to use as the audio sink input pad; use
 *                              AUDIOSINK_CLK_SRC_DIO_*,
 *                              AUDIOSINK_CLK_SRC_CONST_[LOW | HIGH], or
 *                              AUDIOSINK_CLK_SRC_[STANDBYCLK | DMIC_OD]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Audiosink_InputClock(uint32_t cfg, uint32_t sink)
{
    /* DIO configuration is only required */
    if (sink < AUDIOSINK_CLK_SRC_CONST_LOW)
    {
        Sys_DIO_Config((sink >> DIO_AUDIOSINK_SRC_CLK_Pos),
                       ((cfg) | (DIO_MODE_INPUT)));
    }
    DIO->AUDIOSINK_SRC = sink;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audiosink_ResetCounters(void)
 * ----------------------------------------------------------------------------
 * Description   : Reset counter, phase counter and period counter mechanism.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Audiosink_ResetCounters(void)
{
    AUDIOSINK_CTRL->PHASE_CNT_START_ALIAS = 1;
    AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = 1;
    AUDIOSINK_CTRL->CNT_RESET_ALIAS = 1;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_AUDIOSINK_H */
