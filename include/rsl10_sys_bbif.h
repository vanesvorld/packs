/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_bbif.h
 * - BaseBand interface functions and macros
 * ----------------------------------------------------------------------------
 * rsl10_sys_bbif.h
 * $Revision: 1.10 $
 * $Date: 2017/07/11 19:34:34 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_BBIF_H
#define RSL10_SYS_BBIF_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Baseband Interface inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_BBIF_SyncConfig(uint32_t cfg, uint32_t linklbl,
 *                                          uint32_t linkformat)
 * ----------------------------------------------------------------------------
 * Description   : Configure the link synchronization
 * Inputs        : - cfg         - Configuration of the link synchronization
 *                                  mechanism mode; use
 *                                  RX_[IDLE | ACTIVE],
 *                                  SYNC_[DISABLE | ENABLE],
 *                                  [IDLE | ACTIVE], and
 *                                  SYNC_SOURCE_[BLE_RX | BLE_RX_AUDIO* |
 *                                               RF_RX | BLE_TX]
 *                 - linklbl      - The BLE link label for
 *                                  synchronization; use a 5-bit number
 *                 - linkformat   - Configure the BLE link format for
 *                                  synchronization; use
 *                                  [SLAVE, MASTER]_CONNECT
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_BBIF_SyncConfig(uint32_t cfg, uint32_t linklbl,
                                         uint32_t linkformat)
{
    BBIF->SYNC_CFG= (cfg                                       |
            ((linkformat) & BBIF_SYNC_CFG_LINK_FORMAT_Mask)    |
            ((linklbl<< BBIF_SYNC_CFG_LINK_LABEL_Pos) &
            BBIF_SYNC_CFG_LINK_LABEL_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_BBIF_RFFE(uint32_t gpio_num)
 * ----------------------------------------------------------------------------
 * Description   : Configure a DIO as a source for RF front-end audio
 *                 synchronization pulse
 * Inputs        : gpio_num      - GPIO number used for SYNC_PULSE generation
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_BBIF_RFFE(uint32_t gpio_num){
    DIO->CFG[gpio_num] = DIO_MODE_GPIO_OUT_0;
    DIO->BB_RX_SRC &= ((uint32_t)(gpio_num << DIO_BB_RX_SRC_RF_SYNC_P_Pos)) |
            ~DIO_BB_RX_SRC_RF_SYNC_P_Mask;
}

/* ----------------------------------------------------------------------------
 * Function      : Sys_BBIF_ConnectRFFE(void)
 * ----------------------------------------------------------------------------
 * Description   : Internally connect the baseband to the RF front-end.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_BBIF_ConnectRFFE(void)
{
    DIO->RF_SPI_SRC = (RF_SPI_MOSI_SRC_BB_SPI_MOSI      |
                       RF_SPI_CSN_SRC_BB_SPI_CSN        |
                       RF_SPI_CLK_SRC_BB_SPI_CLK);
    DIO->BB_SPI_SRC = BB_SPI_MISO_SRC_RF_SPI_MISO;
    DIO->RF_GPIO47_SRC &= (RF_GPIO4_SRC_BB_TX_DATA_VALID &\
                        ~(DIO_RF_GPIO47_SRC_GPIO4_Mask));
    DIO->RF_GPIO03_SRC &= ((RF_GPIO3_SRC_BB_TX_DATA      |
                          RF_GPIO2_SRC_BB_SYNC_P       )&\
                        ~(DIO_RF_GPIO03_SRC_GPIO3_Mask  |
                          DIO_RF_GPIO03_SRC_GPIO2_Mask));
    DIO->BB_RX_SRC = (BB_RX_CLK_SRC_RF_GPIO1            |
                      BB_RX_DATA_SRC_RF_GPIO0);
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_BBIF_SPIConfig(uint32_t cfg, uint32_t miso,
 *                  uint32_t csn, uint32_t mosi, uint32_t clk)
 * ----------------------------------------------------------------------------
 * Description   :  Configure DIOs as an SPI slave for the Bluetooth baseband
 *                  controller; disable the RF SPI slave interface
 * Inputs        :  - cfg       - DIO pin configuration for the output pads
 *                  - miso      - DIO to use as the MISO pad
 *                  - csn       - DIO to use as the CSN pad
 *                  - mosi      - DIO to use as the MOSI pad
 *                  - clk       - DIO to use as the CLK pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_BBIF_SPIConfig(uint32_t cfg, uint32_t miso,
                      uint32_t csn, uint32_t mosi, uint32_t clk)
{
    DIO->RF_SPI_SRC =   RF_SPI_MOSI_SRC_CONST_LOW   |
                        RF_SPI_CLK_SRC_CONST_LOW    |
                        RF_SPI_CSN_SRC_CONST_HIGH;
    Sys_DIO_Config(clk, ((cfg) | (DIO_MODE_BB_SPI_CLK)));
    Sys_DIO_Config(csn, ((cfg) | (DIO_MODE_BB_SPI_CSN)));
    Sys_DIO_Config(mosi, ((cfg) | (DIO_MODE_BB_SPI_MOSI)));
    Sys_DIO_Config(miso, ((cfg) | (DIO_MODE_INPUT)));
    DIO->BB_SPI_SRC = (miso<<DIO_BB_SPI_SRC_MISO_Pos);
}

/* ----------------------------------------------------------------------------
 * Function      :  void Sys_BBIF_DIOConfig(uint32_t cfg,
 *                              uint32_t gpi_rx_clk, uint32_t gpi_rx_data,
 *                              uint32_t tx_data_valid, uint32_t tx_data,
 *                              uint32_t sync_p)
 * ----------------------------------------------------------------------------
 * Description   : Configure DIO pads connected to radio pins of
 *                 baseband controller
 * Inputs        : - cfg           - DIO pin configuration for the output pads
 *                 - rx_clk        - DIO to use as the BB_RX_CLK pad
 *                 - rx_data       - DIO to use as the BB_RX_DATA pad
 *                 - tx_data_valid - DIO to use as the BB_TX_DATA_VALID pad
 *                 - tx_data       - DIO to use as the BB_TX_DATA pad
 *                 - sync_p        - DIO to use as the BB_SYNC_P pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_BBIF_DIOConfig(uint32_t cfg,
                                uint32_t rx_clk, uint32_t rx_data,
                                uint32_t tx_data_valid, uint32_t tx_data,
                                uint32_t sync_p)
{
    DIO->RF_GPIO47_SRC &= (RF_GPIO4_SRC_CONST_LOW &
                        ~(DIO_RF_GPIO47_SRC_GPIO4_Mask));
    DIO->RF_GPIO03_SRC &= ((RF_GPIO3_SRC_CONST_LOW &
                        ~(DIO_RF_GPIO03_SRC_GPIO3_Mask)) |
                         (RF_GPIO2_SRC_CONST_LOW &
                        ~(DIO_RF_GPIO03_SRC_GPIO2_Mask)));
    Sys_DIO_Config(rx_clk, ((cfg) | (DIO_MODE_INPUT)));
    Sys_DIO_Config(rx_data, ((cfg) | (DIO_MODE_INPUT)));
    DIO->BB_RX_SRC &= (((rx_clk << DIO_BB_RX_SRC_CLK_Pos)             |
                      (rx_data << DIO_BB_RX_SRC_DATA_Pos)) &
                       ~(DIO_BB_RX_SRC_CLK_Mask | DIO_BB_RX_SRC_DATA_Mask));

    Sys_DIO_Config(tx_data_valid, ((cfg) | (DIO_MODE_BB_TX_DATA_VALID)));
    Sys_DIO_Config(tx_data, ((cfg) | (DIO_MODE_BB_TX_DATA)));
    Sys_DIO_Config(sync_p, ((cfg) | (DIO_MODE_BB_SYNC_P)));
}

/* ----------------------------------------------------------------------------
 * Function      : Sys_BBIF_RFFEDrivenExternal(uint32_t cfg, uint32_t clk,
 *      uint32_t mosi, uint32_t miso, uint32_t csn, uint32_t rx_clk,
 *      uint32_t rx_data, uint32_t tx_data_val, uint32_t tx_data,
 *      uint32_t sync_p)
 * ----------------------------------------------------------------------------
 * Description   : Configure DIO pads connected to the RF frontend interface
 *                  to be driven from an external device
 * Inputs        : - cfg            - DIO pin configuration for the output pads
 *                 - clk            - DIO to use as the clock pad
 *                 - mosi           - DIO to use as the MOSI pad
 *                 - miso           - DIO to use as the MISO pad
 *                 - csn            - DIO to use as the chip select pad
 *                 - rx_clk         - DIO to use as the BB_RX_CLK pad
 *                 - rx_data        - DIO to use as the BB_RX_DATA pad
 *                 - tx_data_valid  - DIO to use as the BB_TX_DATA_VALID pad
 *                 - tx_data        - DIO to use as the BB_TX_DATA pad
 *                 - sync_p         - DIO to use as the BB_SYNC_P pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_BBIF_RFFEDrivenExternal(uint32_t cfg, uint32_t clk,
        uint32_t mosi, uint32_t miso, uint32_t csn, uint32_t rx_clk,
        uint32_t rx_data, uint32_t tx_data_val, uint32_t tx_data,
        uint32_t sync_p)
{
    /*SPI configuration*/
    Sys_DIO_Config(clk, ((cfg) | DIO_MODE_INPUT));
    Sys_DIO_Config(csn, ((cfg) | DIO_MODE_INPUT));
    Sys_DIO_Config(mosi, ((cfg) | DIO_MODE_INPUT));
    DIO->RF_SPI_SRC = (
    ((RF_SPI_CLK_SRC_DIO_0  + (uint32_t)(clk << DIO_RF_SPI_SRC_CLK_Pos) ) &
            DIO_RF_SPI_SRC_CLK_Mask)    |
    ((RF_SPI_CSN_SRC_DIO_0  + (uint32_t)(csn << DIO_RF_SPI_SRC_CSN_Pos) ) &
            DIO_RF_SPI_SRC_CSN_Mask)    |
    ((RF_SPI_MOSI_SRC_DIO_0 + (uint32_t)(mosi << DIO_RF_SPI_SRC_MOSI_Pos) ) &
            DIO_RF_SPI_SRC_MOSI_Mask)
        );
    Sys_DIO_Config(miso, ((cfg) | (DIO_MODE_RF_SPI_MISO)));

    /*RF interface configuration*/
    Sys_DIO_Config(rx_data, ((cfg) | (DIO_MODE_RF_GPIO0)));
    Sys_DIO_Config(rx_clk, ((cfg) | (DIO_MODE_RF_GPIO1)));

    Sys_DIO_Config(tx_data, ((cfg) | DIO_MODE_INPUT));
    Sys_DIO_Config(tx_data_val, ((cfg) | DIO_MODE_INPUT));
    Sys_DIO_Config(sync_p, ((cfg) | DIO_MODE_INPUT));
    DIO->RF_GPIO03_SRC &= (
            ((RF_GPIO2_SRC_DIO_0 +
                    ((uint32_t)(sync_p << DIO_RF_GPIO03_SRC_GPIO2_Pos))) |
            (RF_GPIO3_SRC_DIO_0 +
                ((uint32_t)(tx_data << DIO_RF_GPIO03_SRC_GPIO3_Pos)))) &
                ~(DIO_RF_GPIO03_SRC_GPIO2_Mask  | DIO_RF_GPIO03_SRC_GPIO3_Mask)
             );
    DIO->RF_GPIO47_SRC &= (
             (RF_GPIO4_SRC_DIO_0 +
                ((uint32_t)(tx_data_val << DIO_RF_GPIO47_SRC_GPIO4_Pos))) &
                ~(DIO_RF_GPIO47_SRC_GPIO4_Mask)
             );
}
/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_BBIF_H */
