/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_flash.h
 * - Flash and flash support functions and macros
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2017/07/05 16:26:49 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_FLASH_H
#define RSL10_SYS_FLASH_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Flash copier support function prototypes
 * ------------------------------------------------------------------------- */
extern void Sys_Flash_Copy(uint32_t src_addr, uint32_t dest_addr,
                           uint32_t length, uint32_t cpy_dest);

extern uint32_t Sys_Flash_Compare(uint32_t cfg, uint32_t addr,
                                  uint32_t length, uint32_t value,
                                  uint32_t value_ecc);

/* ----------------------------------------------------------------------------
 * Flash error-correction coding support functions
 * ------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------
 * Function      : void Sys_Flash_ECC_Config(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the flash error-correction control support
 * Inputs        : cfg      - Configuration for the flash error-correction
 *                            control block; use
 *                            FLASH_IDBUS_ECC_[ENABLE | DISABLE],
 *                            FLASH_DMA_ECC_[ENABLE | DISABLE],
 *                            FLASH_CMD_ECC_[ENABLE | DISABLE],
 *                            FLASH_COPIER_ECC_[ENABLE | DISABLE], and
 *                            FLASH_ECC_COR_INT_THRESHOLD_* or a constant
 *                            shifted to
 *                            FLASH_ECC_CTRL_ECC_COR_CNT_INT_THRESHOLD_Pos
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Flash_ECC_Config(uint32_t cfg)
{
    FLASH->ECC_CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_FLASH_H */
