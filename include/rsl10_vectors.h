/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_vectors.h
 * - Interrupt vector numbers
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2017/06/13 15:04:15 $
 * ------------------------------------------------------------------------- */ 

#ifndef RSL10_VECTORS_H
#define RSL10_VECTORS_H

#if defined(__cplusplus)
extern "C"
  {
#endif

/* ----------------------------------------------------------------------------
 * Interrupt Vector Table Numbers
 *  - Equivalent to the offset from the base of the external interrupt list
 *  - Can be converted into an index into the vector table by adding 16
 * ------------------------------------------------------------------------- */

typedef enum IRQn
{
    Reset_IRQn                  = -15,  /*  1 Reset Vector */
    NonMaskableInt_IRQn         = -14,  /*  2 Non Maskable Interrupt */
    HardFault_IRQn              = -13,  /*  3 Hard Fault Interrupt */
    MemoryManagement_IRQn       = -12,  /*  4 Memory Management Interrupt */
    BusFault_IRQn               = -11,  /*  5 Bus Fault Interrupt */
    UsageFault_IRQn             = -10,  /*  6 Usage Fault Interrupt */
    SVCall_IRQn                 = -5,   /* 11 SV Call Interrupt */
    DebugMonitor_IRQn           = -4,   /* 12 Debug Monitor Interrupt */
    PendSV_IRQn                 = -2,   /* 14 Pend SV Interrupt */
    SysTick_IRQn                = -1,   /* 15 System Tick Interrupt */
    WAKEUP_IRQn                 = 0,    /* 16 Wake-up interrupt */
    RTC_ALARM_IRQn              = 1,    /* 17 RTC alarm interrupt */
    RTC_CLOCK_IRQn              = 2,    /* 18 RTL clock interrupt */
    ADC_BATMON_IRQn             = 3,    /* 19 ADC / Battery monitor interrupt */
    TIMER0_IRQn                 = 4,    /* 20 Timer0 interrupt */
    TIMER1_IRQn                 = 5,    /* 21 Timer1 interrupt */
    TIMER2_IRQn                 = 6,    /* 22 Timer2 interrupt */
    TIMER3_IRQn                 = 7,    /* 23 Timer3 interrupt */
    DMA0_IRQn                   = 8,    /* 24 DMA channel 0 interrupt */
    DMA1_IRQn                   = 9,    /* 25 DMA channel 1 interrupt */
    DMA2_IRQn                   = 10,   /* 26 DMA channel 2 interrupt */
    DMA3_IRQn                   = 11,   /* 27 DMA channel 3 interrupt */
    DMA4_IRQn                   = 12,   /* 28 DMA channel 4 interrupt */
    DMA5_IRQn                   = 13,   /* 29 DMA channel 5 interrupt */
    DMA6_IRQn                   = 14,   /* 30 DMA channel 6 interrupt */
    DMA7_IRQn                   = 15,   /* 31 DMA channel 7 interrupt */
    DIO0_IRQn                   = 16,   /* 32 DIO0 interrupt */
    DIO1_IRQn                   = 17,   /* 33 DIO1 interrupt */
    DIO2_IRQn                   = 18,   /* 34 DIO2 interrupt */
    DIO3_IRQn                   = 19,   /* 35 DIO3 interrupt */   
    WATCHDOG_IRQn               = 20,   /* 36 Watchdog interrupt */
    SPI0_RX_IRQn                = 21,   /* 37 SPI0 receive interrupt */
    SPI0_TX_IRQn                = 22,   /* 38 SPI0 transmit interrupt */
    SPI0_ERROR_IRQn             = 23,   /* 39 SPI0 error interrupt */
    SPI1_RX_IRQn                = 24,   /* 40 SPI1 receive interrupt */
    SPI1_TX_IRQn                = 25,   /* 41 SPI1 transmit interrupt */
    SPI1_ERROR_IRQn             = 26,   /* 42 SPI1 error interrupt */
    I2C_IRQn                    = 27,   /* 43 I2C interrupt */
    UART_RX_IRQn                = 28,   /* 44 UART receive interrupt */
    UART_TX_IRQn                = 29,   /* 45 UART transmit interrupt */
    UART_ERROR_IRQn             = 30,   /* 46 UART error interrupt */
    DMIC_OUT_OD_IN_IRQn         = 31,   /* 47 DMIC and output driver data ready interrupt */
    DMIC_OD_ERROR_IRQn          = 32,   /* 48 DMIC overrun and output driver underrun detect interrupt */
    PCM_RX_IRQn                 = 33,   /* 49 PCM receive interrupt */
    PCM_TX_IRQn                 = 34,   /* 50 PCM transmit interrupt */
    PCM_ERROR_IRQn              = 35,   /* 51 PCM error interrupt */
    DSP0_IRQn                   = 36,   /* 52 DSP event interrupt 0 */
    DSP1_IRQn                   = 37,   /* 53 DSP event interrupt 1 */
    DSP2_IRQn                   = 38,   /* 54 DSP event interrupt 2 */
    DSP3_IRQn                   = 39,   /* 55 DSP event interrupt 3 */
    DSP4_IRQn                   = 40,   /* 56 DSP event interrupt 4 */
    DSP5_IRQn                   = 41,   /* 57 DSP event interrupt 5 */
    DSP6_IRQn                   = 42,   /* 58 DSP event interrupt 6 */
    DSP7_IRQn                   = 43,   /* 59 DSP event interrupt 7 */
    BLE_CSCNT_IRQn              = 44,   /* 60 BLE 625us time reference interrupt */
    BLE_SLP_IRQn                = 45,   /* 61 BLE sleep mode interrupt */
    BLE_RX_IRQn                 = 46,   /* 62 BLE received packet interrupt */
    BLE_EVENT_IRQn              = 47,   /* 63 BLE event interrupt */
    BLE_CRYPT_IRQn              = 48,   /* 64 BLE AES ciphering complete interrupt */
    BLE_ERROR_IRQn              = 49,   /* 65 BLE baseband error interrupt */
    BLE_GROSSTGTIM_IRQn         = 50,   /* 66 BLE gross timer interrupt */
    BLE_FINETGTIM_IRQn          = 51,   /* 67 BLE fine timer interrupt */
    BLE_SW_IRQn                 = 52,   /* 68 BLE SW triggered inerrupt */
    BLE_COEX_RX_TX_IRQn         = 53,   /* 69 Coexistance BLE start/stop RX or TX interrupt */
    BLE_COEX_IN_PROCESS_IRQn    = 54,   /* 70 Coexistance BLE in process interrupt */
    RF_TX_IRQn                  = 55,   /* 71 BLE transmit interrupt */
    RF_RXSTOP_IRQn              = 56,   /* 72 BLE receive stop interrupt */
    RF_IRQ_RECEIVED_IRQn        = 57,   /* 73 BLE received packet interrupt */
    RF_SYNC_IRQn                = 58,   /* 74 BLE received sync word interrupt */
    RF_TXFIFO_IRQn              = 59,   /* 75 TX FIFO near underflow detect interrupt */
    RF_RXFIFO_IRQn              = 60,   /* 76 RX FIFO near overflow detect interrupt */
    ASRC_ERROR_IRQn             = 61,   /* 77 ASRC error interrupt */
    ASRC_IN_IRQn                = 62,   /* 78 ASRC data input interrupt */
    ASRC_OUT_IRQn               = 63,   /* 79 ASRC data output interrupt */
    AUDIOSINK_PHASE_IRQn        = 64,   /* 80 Audio sink clock phase interrupt */
    AUDIOSINK_PERIOD_IRQn       = 65,   /* 81 Audio sink clock period interrupt */
    CLKDET_IRQn                 = 66,   /* 82 Clock detection interrupt */
    FLASH_COPY_IRQn             = 67,   /* 83 Flash copy interrupt */
    FLASH_ECC_IRQn              = 68,   /* 84 Flash ECC event interrupt */
    MEMORY_ERROR_IRQn           = 69,   /* 85 Memory error interrupt */
    BLE_AUDIO0_IRQn             = 70,   /* 86 Audio over BLE channel 0 interrupt */
    BLE_AUDIO1_IRQn             = 71,   /* 87 Audio over BLE channel 1 interrupt */
    BLE_AUDIO2_IRQn             = 72    /* 88 Audio over BLE channel 2 interrupt */
} IRQn_Type;                      
                                  
#if defined(__cplusplus)          
}                                 
#endif

#endif /* RSL10_VECTORS_H */
