/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_MFi.h
 * - Top-level header file for support of MFi
 * ----------------------------------------------------------------------------
 * $Revision: 1.4 $
 * $Date: 2018/06/26 17:17:31 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_LEA_H
#define RSL10_LEA_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
 extern "C" {
#endif

/* ----------------------------------------------------------------------------
 * MFi support header files
 * ------------------------------------------------------------------------- */
#include <MFi/rwam0_config.h>
#include <MFi/am0_api.h>
#include <MFi/am0_has.h>
#include <MFi/am0_has_task.h>
#include <MFi/am0_task.h>
#include <MFi/audio.h>
#include <MFi/leaudio_emul.h>
#include <MFi/rsa.h>

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_LEA_H */
