/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_aes.h
 * - AES wrapper support
 * ----------------------------------------------------------------------------
 * $Revision: 1.14 $
 * $Date: 2018/05/17 16:13:34 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_AES_H
#define RSL10_SYS_AES_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * AES wrapper support function prototypes
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * AES wrapper support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_AES_Config(uint32_t * key, uint32_t mem_zone)
 * ----------------------------------------------------------------------------
 * Description   : Configure AES-128 engine for a ciphering method
 * Inputs        : - key        -   Pointer to AES encryption 128-bit key
 *                 - mem_zone   -   Memory offset from the top of the exchange
 *                                  memory; points to a 32-byte array
 *                                  consisting of the 16-byte plain-text input,
 *                                  followed by the 16-byte cipher-text output
 * Outputs       : None
 * Assumptions   : The baseband block should be enabled.
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_AES_Config(uint32_t * key, uint32_t mem_zone)
{
    BB->AESKEY31_0 = *key++;
    BB->AESKEY63_32 = *key++;
    BB->AESKEY95_64 = *key++;
    BB->AESKEY127_96 = *key;
    BB->AESPTR = mem_zone;
    BB_AESCNTL->AES_MODE_ALIAS = AES_MODE_0_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_AES_Cipher(void)
 * ----------------------------------------------------------------------------
 * Description   : Run AES-128 cipher engine
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : The baseband block should be enabled. Sys_AES_Config()
 *                 has been called.
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_AES_Cipher(void)
{
    BB_AESCNTL->AES_START_ALIAS = AES_START_1_BITBAND;

    /* Wait for the AES block to complete ciphering the plain text */
    while (BB_AESCNTL->AES_START_ALIAS == AES_START_1_BITBAND);
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_AES_H */
