/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_ble.h
 * - Top-level header file for support of the Bluetooth Low Energy Stack
 * ----------------------------------------------------------------------------
 * $Revision: 1.2 $
 * $Date: 2017/09/19 19:04:02 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_BLE_H
#define RSL10_BLE_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
 extern "C" {
#endif

/* ----------------------------------------------------------------------------
 * BLE support header files
 * ------------------------------------------------------------------------- */
#include <ble/rwble_hl_config.h>
#include <ble/rwble_hl_error.h>
#include <ble/rwble_hl.h>
#include <ble/rwprf_config.h>
#include <ble/prf.h>
#include <ble/ahi.h>
#include <ble/ahi_task.h>
#include <ble/att.h>
#include <ble/attc.h>
#include <ble/attm.h>
#include <ble/attm_db.h>
#include <ble/atts.h>
#include <ble/ecc_p256.h>
#include <ble/gap.h>
#include <ble/gapc.h>
#include <ble/gapm_int.h>
#include <ble/gapc_task.h>
#include <ble/gapm.h>
#include <ble/gapm_task.h>
#include <ble/gapm_util.h>
#include <ble/gatt.h>
#include <ble/gattc.h>
#include <ble/gattc_task.h>
#include <ble/gattm.h>
#include <ble/gattm_task.h>
#include <ble/h4tl.h>
#include <ble/hci.h>
#include <ble/l2cc.h>
#include <ble/l2cc_pdu.h>
#include <ble/l2cc_task.h>
#include <ble/l2cm.h>
#include <ble/smp_common.h>
#include <ble/smpc.h>
#include <ble/smpc_api.h>
#include <ble/smpc_crypto.h>
#include <ble/smpc_util.h>
#include <ble/smpm_api.h>
#include <ble/prf_types.h>
#include <ble/prf_utils.h>

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_BLE_H */
