/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC
 * (d/b/a ON Semiconductor). All rights reserved.
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor. The
 * terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_math.h
 * - Header file for the math library
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2017/05/25 21:02:51 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_MATH_H
#define RSL10_MATH_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Firmware Math Library Version Code
 * ------------------------------------------------------------------------- */
#define MATH_FW_VER_MAJOR                0x01
#define MATH_FW_VER_MINOR                0x01
#define MATH_FW_VER_REVISION             0x01

#define MATH_FW_VER                      ((MATH_FW_VER_MAJOR << 12) | \
                                          (MATH_FW_VER_MINOR << 8)  | \
                                          (MATH_FW_VER_REVISION))

extern const short MMCU_MathLib_Version;

#include <stdint.h>

/* ----------------------------------------------------------------------------
 * Math library defines
 * ------------------------------------------------------------------------- */
#define MAX_FRAC32 0x7FFFFFFF
#define MIN_FRAC32 0x80000000

/* ----------------------------------------------------------------------------
 * Math library function prototypes
 * ------------------------------------------------------------------------- */

/* Floating-Point functions */
extern float Math_ExpAvg(float alpha, float x, float y1);
extern float Math_AttackRelease(float a, float b, float x, float y1);
extern float Math_LinearInterp(float x0, float x1, float y0, float y1, float x);
extern void Math_SingleVar_Reg(float* x, float* y, unsigned int N, float* a);

/* Fixed-Point functions */
extern int32_t Math_Mult_frac32(int32_t x, int32_t y);
extern int32_t Math_Add_frac32(int32_t x, int32_t y);
extern int32_t Math_Sub_frac32(int32_t x, int32_t y);
extern int32_t Math_ExpAvg_frac32(int32_t alpha, int32_t x, int32_t y1);
extern int32_t Math_AttackRelease_frac32(int32_t a, int32_t b, int32_t x, int32_t y1);
extern int32_t Math_LinearInterp_frac32(int32_t y0, int32_t y1, int32_t x);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif
#endif  /* RSL10_MATH_H */
