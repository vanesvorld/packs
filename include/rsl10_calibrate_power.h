/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_calibrate_power.h
 * - Calibration support header
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2017/06/16 17:55:08 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_CALIBRATE_POWER_H
#define RSL10_CALIBRATE_POWER_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Constant Definitions
 * ----------------------------------------------------------------------------
 * Assumptions: SYSCLK = 16 MHz, ADC = low frequency mode (SLOWCLK = 1 MHz)
 * ------------------------------------------------------------------------- */
/* 3 times the length of time corresponding to the minimum sample rate,
 * which is deemed sufficient to allow the ADC to stabilize */
#define ADC_STABILIZATION_DELAY         0x7530

/* Corresponds to sample rate of the ADC as configured (100 Hz) */
#define ADC_MEASUREMENT_DELAY           0x2710

/* Set the ADC measurement error to 8 [mV]. The trim step size is 10 mV
 * so ideally every value 5 mV apart can be reached, and with a safety factor
 * of 1.5 we get a measurement error of 7.5 mV, rounded up to 8 mV. */
#define ADC_MEASUREMENT_ERROR           8

/* ----------------------------------------------------------------------------
 * Function Prototypes
 * ------------------------------------------------------------------------- */
void Calibrate_Power_Initialize(void);
unsigned int Calibrate_Power_VBG(unsigned int adc_num,
                                 uint32_t *adc_ptr,
                                 uint32_t target);
unsigned int Calibrate_Power_VDDRF(unsigned int adc_num,
                                   uint32_t *adc_ptr,
                                   uint32_t target);
unsigned int Calibrate_Power_VDDPA(unsigned int adc_num,
                                   uint32_t *adc_ptr,
                                   uint32_t target);
unsigned int Calibrate_Power_DCDC(unsigned int adc_num,
                                  uint32_t *adc_ptr,
                                  uint32_t target);
unsigned int Calibrate_Power_VDDC(unsigned int adc_num,
                                  uint32_t *adc_ptr,
                                  uint32_t target);
unsigned int Calibrate_Power_VDDM(unsigned int adc_num,
                                  uint32_t *adc_ptr,
                                  uint32_t target);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_CALIBRATE_POWER_H */
