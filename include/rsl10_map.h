/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_map.h
 * - Memory map 
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2017/09/20 17:24:20 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_MAP_H
#define RSL10_MAP_H

/* ----------------------------------------------------------------------------
 * Instruction and Data Bus Memory Structures
 * ------------------------------------------------------------------------- */

/* Boot ROM (PROM)      : 4 KB */
#define PROM_BASE                       0x00000000
#define PROM_TOP                        0x00000FFF
#define PROM_SIZE                       (PROM_TOP - PROM_BASE + 1)

/* Flash Redundancy 1   : 2 KB */
#define FLASH_REDUNDANCY_1_BASE         0x00060000
#define FLASH_REDUNDANCY_1_TOP          0x000607FF
#define FLASH_REDUNDANCY_1_SIZE         (FLASH_REDUNDANCY_1_TOP - FLASH_REDUNDANCY_1_BASE + 1)

/* Flash Redundancy 2   : 2 KB */
#define FLASH_REDUNDANCY_2_BASE         0x00060800
#define FLASH_REDUNDANCY_2_TOP          0x00060FFF
#define FLASH_REDUNDANCY_2_SIZE         (FLASH_REDUNDANCY_2_TOP - FLASH_REDUNDANCY_2_BASE + 1)

/* Flash NVR1           : 2 KB */
#define FLASH_NVR1_BASE                 0x00080000
#define FLASH_NVR1_TOP                  0x000807FF
#define FLASH_NVR1_SIZE                 (FLASH_NVR1_TOP - FLASH_NVR1_BASE + 1)

/* Flash NVR2           : 2 KB */
#define FLASH_NVR2_BASE                 0x00080800
#define FLASH_NVR2_TOP                  0x00080FFF
#define FLASH_NVR2_SIZE                 (FLASH_NVR2_TOP - FLASH_NVR2_BASE + 1)

/* Flash NVR3           : 2 KB */
#define FLASH_NVR3_BASE                 0x00081000
#define FLASH_NVR3_TOP                  0x000817FF
#define FLASH_NVR3_SIZE                 (FLASH_NVR3_TOP - FLASH_NVR3_BASE + 1)

/* Flash NVR4           : 1 KB */
#define FLASH_NVR4_0_BASE               0x00081800
#define FLASH_NVR4_0_TOP                0x000818FF
#define FLASH_NVR4_0_SIZE               (FLASH_NVR4_0_TOP - FLASH_NVR4_0_BASE + 1)

#define FLASH_NVR4_1_BASE               0x00081A00
#define FLASH_NVR4_1_TOP                0x00081AFF
#define FLASH_NVR4_1_SIZE               (FLASH_NVR4_1_TOP - FLASH_NVR4_1_BASE + 1)

#define FLASH_NVR4_2_BASE               0x00081C00
#define FLASH_NVR4_2_TOP                0x00081CFF
#define FLASH_NVR4_2_SIZE               (FLASH_NVR4_2_TOP - FLASH_NVR4_2_BASE + 1)

#define FLASH_NVR4_3_BASE               0x00081E00
#define FLASH_NVR4_3_TOP                0x00081EFF
#define FLASH_NVR4_3_SIZE               (FLASH_NVR4_3_TOP - FLASH_NVR4_3_BASE + 1)

/* Flash                : 384 KB */
#define FLASH_MAIN_BASE                 0x00100000
#define FLASH_MAIN_TOP                  0x0015FFFF
#define FLASH_MAIN_SIZE                 (FLASH_MAIN_TOP - FLASH_MAIN_BASE + 1)

/* PRAM 0               : 8 KB */
#define PRAM0_BASE                      0x00200000
#define PRAM0_TOP                       0x00201FFF
#define PRAM0_SIZE                      (PRAM0_TOP - PRAM0_BASE + 1)

/* PRAM 1               : 8 KB */
#define PRAM1_BASE                      0x00202000
#define PRAM1_TOP                       0x00203FFF
#define PRAM1_SIZE                      (PRAM1_TOP - PRAM1_BASE + 1)

/* PRAM 2               : 8 KB */
#define PRAM2_BASE                      0x00204000
#define PRAM2_TOP                       0x00205FFF
#define PRAM2_SIZE                      (PRAM2_TOP - PRAM2_BASE + 1)

/* PRAM 3               : 8 KB */
#define PRAM3_BASE                      0x00206000
#define PRAM3_TOP                       0x00207FFF
#define PRAM3_SIZE                      (PRAM3_TOP - PRAM3_BASE + 1)

/* PRAM                 : 4 x 8 KB */
#define PRAM_BASE                       PRAM0_BASE
#define PRAM_TOP                        PRAM3_TOP
#define PRAM_SIZE                       (PRAM_TOP - PRAM_BASE + 1)

/* DSP PRAM 3 (remapped): 8 KB */
#define DSP_PRAM3_REMAPPED_BASE         0x00208000
#define DSP_PRAM3_REMAPPED_TOP          0x00209FFF

/* DSP PRAM 2 (remapped): 8 KB */
#define DSP_PRAM2_REMAPPED_BASE         0x0020A000
#define DSP_PRAM2_REMAPPED_TOP          0x0020BFFF

/* DSP PRAM 1 (remapped): 8 KB */
#define DSP_PRAM1_REMAPPED_BASE         0x0020C000
#define DSP_PRAM1_REMAPPED_TOP          0x0020DFFF

/* DSP PRAM 0 (remapped): 8 KB */
#define DSP_PRAM0_REMAPPED_BASE         0x0020E000
#define DSP_PRAM0_REMAPPED_TOP          0x0020FFFF

/* DSP PRAM (remapped)  : 4 x 8 KB */
#define DSP_PRAM_REMAPPED_BASE          DSP_PRAM3_REMAPPED_BASE
#define DSP_PRAM_REMAPPED_TOP           DSP_PRAM0_REMAPPED_TOP

/* DSP PRAM 0           : 8 KB */
#define DSP_PRAM0_BASE                  0x00220000
#define DSP_PRAM0_TOP                   0x00221FFF
#define DSP_PRAM0_SIZE                  (DSP_PRAM0_TOP - DSP_PRAM0_BASE + 1)

/* DSP PRAM 1           : 8 KB */
#define DSP_PRAM1_BASE                  0x00222000
#define DSP_PRAM1_TOP                   0x00223FFF
#define DSP_PRAM1_SIZE                  (DSP_PRAM1_TOP - DSP_PRAM1_BASE + 1)

/* DSP PRAM 2           : 8 KB */
#define DSP_PRAM2_BASE                  0x00224000
#define DSP_PRAM2_TOP                   0x00225FFF
#define DSP_PRAM2_SIZE                  (DSP_PRAM2_TOP - DSP_PRAM2_BASE + 1)

/* DSP PRAM 3           : 8 KB */
#define DSP_PRAM3_BASE                  0x00226000
#define DSP_PRAM3_TOP                   0x00227FFF
#define DSP_PRAM3_SIZE                  (DSP_PRAM3_TOP - DSP_PRAM3_BASE + 1)

/* DSP PRAM 0 (bits 39:32)  : 2 KB */
#define DSP_PRAM0_EXT_BASE              0x00230000
#define DSP_PRAM0_EXT_TOP               0x00231FFF
#define DSP_PRAM0_EXT_SIZE              (DSP_PRAM0_EXT_TOP - DSP_PRAM0_EXT_BASE + 1)

/* DSP PRAM 1 (bits 39:32)  : 2 KB */
#define DSP_PRAM1_EXT_BASE              0x00232000
#define DSP_PRAM1_EXT_TOP               0x00233FFF
#define DSP_PRAM1_EXT_SIZE              (DSP_PRAM1_EXT_TOP - DSP_PRAM1_EXT_BASE + 1)

/* DSP PRAM 2 (bits 39:32)  : 2 KB */
#define DSP_PRAM2_EXT_BASE              0x00234000
#define DSP_PRAM2_EXT_TOP               0x00235FFF
#define DSP_PRAM2_EXT_SIZE              (DSP_PRAM2_EXT_TOP - DSP_PRAM2_EXT_BASE + 1)

/* DSP PRAM 3 (bits 39:32)  : 2 KB */
#define DSP_PRAM3_EXT_BASE              0x00236000
#define DSP_PRAM3_EXT_TOP               0x00237FFF
#define DSP_PRAM3_EXT_SIZE              (DSP_PRAM3_EXT_TOP - DSP_PRAM3_EXT_BASE + 1)

/* DSP PRAM             : 4 x 8 KB */
#define DSP_PRAM_BASE                   DSP_PRAM0_BASE
#define DSP_PRAM_TOP                    DSP_PRAM3_TOP
#define DSP_PRAM_SIZE                   (DSP_PRAM_TOP - DSP_PRAM_BASE + 1)

/* ----------------------------------------------------------------------------
 * System Bus Memory Structures
 * ------------------------------------------------------------------------- */

/* Data RAM 0           : 8 KB */
#define DRAM0_BASE                      0x20000000
#define DRAM0_TOP                       0x20001FFF
#define DRAM0_SIZE                      (DRAM0_TOP - DRAM0_BASE + 1)

/* Data RAM 1           : 8 KB */
#define DRAM1_BASE                      0x20002000
#define DRAM1_TOP                       0x20003FFF
#define DRAM1_SIZE                      (DRAM1_TOP - DRAM1_BASE + 1)

/* Data RAM 2           : 8 KB */
#define DRAM2_BASE                      0x20004000
#define DRAM2_TOP                       0x20005FFF
#define DRAM2_SIZE                      (DRAM2_TOP - DRAM2_BASE + 1)

/* Data RAM             : 3 x 8 KB */
#define DRAM_BASE                       DRAM0_BASE
#define DRAM_TOP                        DRAM2_TOP
#define DRAM_SIZE                       (DRAM_TOP - DRAM_BASE + 1)

/* DSP Data RAM 0       : 8 KB */
#define DSP_DRAM0_BASE                  0x20006000
#define DSP_DRAM0_TOP                   0x20007FFF
#define DSP_DRAM0_SIZE                  (DSP_DRAM0_TOP - DSP_DRAM0_BASE + 1)

/* DSP Data RAM 1       : 8 KB */
#define DSP_DRAM1_BASE                  0x20008000
#define DSP_DRAM1_TOP                   0x20009FFF
#define DSP_DRAM1_SIZE                  (DSP_DRAM1_TOP - DSP_DRAM1_BASE + 1)

/* DSP Data RAM 2       : 8 KB */
#define DSP_DRAM2_BASE                  0x2000A000
#define DSP_DRAM2_TOP                   0x2000BFFF
#define DSP_DRAM2_SIZE                  (DSP_DRAM2_TOP - DSP_DRAM2_BASE + 1)

/* DSP Data RAM 3       : 8 KB */
#define DSP_DRAM3_BASE                  0x2000C000
#define DSP_DRAM3_TOP                   0x2000DFFF
#define DSP_DRAM3_SIZE                  (DSP_DRAM3_TOP - DSP_DRAM3_BASE + 1)

/* DSP Data RAM 4       : 8 KB */
#define DSP_DRAM4_BASE                  0x2000E000
#define DSP_DRAM4_TOP                   0x2000FFFF
#define DSP_DRAM4_SIZE                  (DSP_DRAM4_TOP - DSP_DRAM4_BASE + 1)

/* DSP Data RAM 5       : 8 KB */
#define DSP_DRAM5_BASE                  0x20010000
#define DSP_DRAM5_TOP                   0x20011FFF
#define DSP_DRAM5_SIZE                  (DSP_DRAM5_TOP - DSP_DRAM5_BASE + 1)

/* DSP Data RAM         :  6 x 8 KB */
#define DSP_DRAM_BASE                   DSP_DRAM0_BASE
#define DSP_DRAM_TOP                    DSP_DRAM5_TOP
#define DSP_DRAM_SIZE                   (DSP_DRAM_TOP - DSP_DRAM_BASE + 1)

/* Baseband Data RAM 0  : 8 KB */
#define BB_DRAM0_BASE                   0x20012000
#define BB_DRAM0_TOP                    0x20013FFF
#define BB_DRAM0_SIZE                   (BB_DRAM0_TOP - BB_DRAM0_BASE + 1)

/* Baseband Data RAM 1  : 8 KB */
#define BB_DRAM1_BASE                   0x20014000
#define BB_DRAM1_TOP                    0x20015FFF
#define BB_DRAM1_SIZE                   (BB_DRAM1_TOP - BB_DRAM1_BASE + 1)

/* Base Band Data RAM   : 2 x 8 KB */
#define BB_DRAM_BASE                    BB_DRAM0_BASE
#define BB_DRAM_TOP                     BB_DRAM1_TOP
#define BB_DRAM_SIZE                    (BB_DRAM_TOP - BB_DRAM_BASE + 1)

/* Interleaved DSP Data RAM 0,1 : 16 KB */
#define DSP_DRAM01_BASE                 0x21000000
#define DSP_DRAM01_TOP                  0x21003FFF
#define DSP_DRAM01_SIZE                 (DSP_DRAM01_TOP - DSP_DRAM01_BASE + 1)

/* Interleaved DSP Data RAM 2,3 : 16 KB */
#define DSP_DRAM23_BASE                 0x21004000
#define DSP_DRAM23_TOP                  0x21007FFF
#define DSP_DRAM23_SIZE                 (DSP_DRAM23_TOP - DSP_DRAM23_BASE + 1)

/* Interleaved DSP Data RAM 4,5 : 16 KB */
#define DSP_DRAM45_BASE                 0x21008000
#define DSP_DRAM45_TOP                  0x2100BFFF
#define DSP_DRAM45_SIZE                 (DSP_DRAM45_TOP - DSP_DRAM45_BASE + 1)

/* Interleaved Data RAM 1,2     : 16 KB */
#define DRAM12_BASE                     0x2100C000
#define DRAM12_TOP                      0x2100FFFF
#define DRAM12_SIZE                     (DRAM12_TOP - DRAM12_BASE + 1)

/* DSP Data RAM 4 (remapped)    : 8 KB */
#define DSP_DRAM4_REMAPPED_BASE         0x21800000
#define DSP_DRAM4_REMAPPED_TOP          0x21801FFF
#define DSP_DRAM4_REMAPPED_SIZE         (DSP_DRAM4_REMAPPED_TOP - DSP_DRAM4_REMAPPED_BASE + 1)

/* DSP Data RAM 5 (remapped)    : 8 KB */
#define DSP_DRAM5_REMAPPED_BASE         0x21802000
#define DSP_DRAM5_REMAPPED_TOP          0x21803FFF
#define DSP_DRAM5_REMAPPED_SIZE         (DSP_DRAM5_REMAPPED_TOP - DSP_DRAM5_REMAPPED_BASE + 1)

/* Data RAM 1 (remapped)        : 8 KB */
#define DRAM1_REMAPPED_BASE             0x21804000
#define DRAM1_REMAPPED_TOP              0x21805FFF
#define DRAM1_REMAPPED_SIZE             (DRAM1_REMAPPED_TOP - DRAM1_REMAPPED_BASE + 1)

/* Data RAM 2 (remapped)        : 8 KB */
#define DRAM2_REMAPPED_BASE             0x21806000
#define DRAM2_REMAPPED_TOP              0x21807FFF
#define DRAM2_REMAPPED_SIZE             (DRAM2_REMAPPED_TOP - DRAM2_REMAPPED_BASE + 1)

#define DRAM_BITBAND_BASE               0x22000000

/* ----------------------------------------------------------------------------
 * Peripheral Bus Memory-Mapped Control Registers
 * ------------------------------------------------------------------------- */
#define PERIPHERAL_BASE                 0x40000000
#define PERIPHERAL_BITBAND_BASE         0x42000000

/* ----------------------------------------------------------------------------
 * Private Peripheral Bus Internal Memory-Mapped Control Registers
 * ------------------------------------------------------------------------- */
#define PRIVATE_PERIPHERAL_BASE         0xE0000000
#define PRIVATE_PERIPHERAL_SYS_BASE     (PRIVATE_PERIPHERAL_BASE + 0xE000)

#endif /* RSL10_MAP_H */
