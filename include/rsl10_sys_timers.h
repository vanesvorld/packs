/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_timers.h
 * - General-purpose system timer hardware support functions and macros
 * ----------------------------------------------------------------------------
 * $Revision: 1.12 $
 * $Date: 2017/07/10 14:52:29 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_TIMERS_H
#define RSL10_SYS_TIMERS_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Timer selection defines
 * ------------------------------------------------------------------------- */
#define SELECT_TIMER0                   (1U << 0)
#define SELECT_TIMER1                   (1U << 1)
#define SELECT_TIMER2                   (1U << 2)
#define SELECT_TIMER3                   (1U << 3)

#define SELECT_ALL_TIMERS               (SELECT_TIMER0 | SELECT_TIMER1 | \
                                         SELECT_TIMER2 | SELECT_TIMER3)
#define SELECT_NO_TIMERS                (0)

/* ----------------------------------------------------------------------------
 * General-purpose system timer support function prototypes
 * ------------------------------------------------------------------------- */
extern void Sys_Timers_Start(uint32_t cfg);
extern void Sys_Timers_Stop(uint32_t cfg);

/* ----------------------------------------------------------------------------
 * General-purpose system timer support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Timer_Set_Control(uint32_t num, uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Set up a general-purpose system timer
 * Inputs        : - num    - Timer to configure; use 0, 1, 2, or 3
 *                 - cfg    - Control configuration for the specified timer; use
 *                            TIMER_MULTI_COUNT_*,
 *                            TIMER_[SHOT_MODE | FREE_RUN],
 *                            TIMER_PRESCALE_*
 *                            and a timeout count setting
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Timer_Set_Control(uint32_t num, uint32_t cfg)
{
    TIMER->CFG[num] = (cfg & (TIMER_CFG_MULTI_COUNT_Mask         |
                                (1U << TIMER_CFG_CLK_SRC_Pos)    |
                                (1U << TIMER_CFG_MODE_Pos)       |
                                 TIMER_CFG_PRESCALE_Mask         |
                                 TIMER_CFG_TIMEOUT_VALUE_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_Timer_Get_Status(uint32_t num)
 * ----------------------------------------------------------------------------
 * Description   : Return the current running or stopped status of the
 *                 specified general-purpose system timer
 * Inputs        : num          - Timer to read status from; use 0, 1, 2, or
 *                                3
 * Outputs       : - return value - The current timer status; value loaded
 *                                  from TIMER_CTRL_[*].TIMER_STATUS_ALIAS
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_Timer_Get_Status(uint32_t num)
{
    return TIMER_CTRL[num].TIMER_STATUS_ALIAS;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Timer_BBConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the baseband timer
 * Inputs        : - cfg - Value for configuration of the baseband timer
 *                         clock; use:
 *                         BB_TIMER_[RESET | NRESET],
 *                         BB_CLK_PRESCALE_*
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Timer_BBConfig(uint32_t cfg)
{
    ACS->BB_TIMER_CTRL= cfg;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_TIMERS_H */
