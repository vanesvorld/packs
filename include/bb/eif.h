/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * eif.h
 * - Header for external interface for API
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2018/09/08 22:48:06 $
 * ------------------------------------------------------------------------- */

#ifndef EIF_H
#define EIF_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

#include <rsl10.h>
#include "compiler.h"
#include "arch.h"
#include "reg_assert_mgr.h"
#include "rwip.h"

/* External interface parameters */
typedef struct
{
    uint8_t *bufptr;
    uint32_t size;
    rwip_eif_callback callback;
    void* data;
} eif;

/* Interface for data transmission to DMA */
extern eif TX_INTERFACE;

/* Interface for data reception from DMA */
extern eif RX_INTERFACE;

eif* eif_create(uint8_t* buffer, uint32_t size,
                            rwip_eif_callback callback, void* data);

void eif_read(uint8_t* buffer, uint32_t size, rwip_eif_callback callback,
              void* data);

void eif_write(uint8_t* buffer, uint32_t size, rwip_eif_callback callback,
               void* data);

void eif_flow_on(void);

bool eif_flow_off(void);

extern uint8_t eif_callback_check(eif INF);

extern void eif_callback_handler(eif INF);

extern void DMA_read(void);

extern void DMA_write(void);

#endif /* EIF_H */
