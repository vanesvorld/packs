/**
 ****************************************************************************************
 *
 * @file lld_sleep.h
 *
 * @brief Functions for RWBLE core sleep mode management
 *
 * Copyright (C) RivieraWaves 2009-2015
 *
 *
 ****************************************************************************************
 */

#ifndef LLD_SLEEP_H_
#define LLD_SLEEP_H_

/**
 ****************************************************************************************
 * @addtogroup LLDSLEEP LLDSLEEP
 * @ingroup LLD
 * @brief Functions for RWBLE core sleep mode management
 *
 * This module implements the function that manages deep sleep of BLE core.
 *
 * @{
 ****************************************************************************************
 */


/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "rwip_config.h"

#if (DEEP_SLEEP)
#include <stdint.h>
#include <stdbool.h>

#include <rsl10_sys_power_modes.h>


/* Customized parameters for the LLD SLEEP module
 * respect to OSC wake-up timings in us */
struct lld_sleep_params_t
{
    uint16_t twosc;
};

/*
 * FUNCTION DECLARATIONS
 ****************************************************************************************
 */

#if (!BT_DUAL_MODE)


/**
 ****************************************************************************************
 * @brief Set lld_sleep_params
 ****************************************************************************************
 */
extern struct lld_sleep_params_t lld_sleep_params;

void BLE_LLD_Sleep_Params_Set(struct lld_sleep_params_t desired_lld_sleep_params);

/**
 ****************************************************************************************
 * @brief Set ble_is_awake_flag to indicate BLE is awake
 ****************************************************************************************
 */
void BLE_Is_Awake_Flag_Set(void);

/**
 ****************************************************************************************
 * @brief Clear ble_is_awake_flag to indicate BLE is not awake
 ****************************************************************************************
 */
void BLE_Is_Awake_Flag_Clear(void);

/**
 ****************************************************************************************
 * @brief Read ble_is_awake_flag to check if BLE is awake
 ****************************************************************************************
 */
bool BLE_Is_Awake(void);

/**
 ****************************************************************************************
 * @brief  Set the value of RC clock period
 ****************************************************************************************
 */
void RTCCLK_Period_Value_Set(float value);

/**
 ****************************************************************************************
 * @brief  Set the value of RC clock period or external clock
 ****************************************************************************************
 */
void LowPowerClock_Source_Set(uint8_t value);
	
/**
 ****************************************************************************************
 * @brief Initialize sleep module
 ****************************************************************************************
 */
void lld_sleep_init(void);

/**
 ****************************************************************************************
 * @brief The Sleep function. Enter BLE Core in deep sleep
 *
 * @param[in]    sleep_duration  Duration of deep sleep (slot time duration)
 * @param[in]    ext_wakeup      False: external wake-up disabled / True: enabled
 ****************************************************************************************
 */
void lld_power_mode_enter(
                uint32_t power_mode_duration,
                bool ext_wakeup,
                void *power_mode_env,
                uint8_t power_mode);

/**
 ****************************************************************************************
 * @brief Function to wake up BLE core
 ****************************************************************************************
 */
void lld_sleep_wakeup(void);

/**
 ****************************************************************************************
 * @brief Function to handle the end of BLE core wake up
 ****************************************************************************************
 */
void lld_sleep_wakeup_end(void);

#endif // !BT_DUAL_MODE

/**
 ****************************************************************************************
 * @brief Check if sleep mode is possible
 *
 * The function takes as argument the allowed sleep duration that must not be increased.
 * If BLE needs an earlier wake-up than initial duration, the allowed sleep duration
 * is updated.
 * If BLE needs a shorter duration than the wake-up delay, sleep is not possible and
 * the function returns a bad status.
 *
 * @param[in/out] sleep_duration   Initial allowed sleep duration (in slot of 625us)
 * @param[in]     wakeup_delay     Delay for system wake-up (in slot of 625us)
 *
 * @return true if sleep is allowed, false otherwise
 ****************************************************************************************
 */
//bool lld_sleep_check(uint32_t *sleep_duration, uint32_t wakeup_delay);

#endif // DEEP_SLEEP

/// @} LLDSLEEP

#endif // LLD_SLEEP_H_
