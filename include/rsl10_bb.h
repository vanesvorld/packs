/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_bb.h
 * - Top-level header file for support of shared baseband elements
 * ----------------------------------------------------------------------------
 * $Revision: 1.2 $
 * $Date: 2017/09/19 19:04:02 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_BB_H
#define RSL10_BB_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
 extern "C" {
#endif

/* ----------------------------------------------------------------------------
 * Baseband support header files
 * ------------------------------------------------------------------------- */
#include <bb/rwble_config.h>
#include <bb/rwble.h>
#include <bb/rwip.h>
#include <bb/rwip_config.h>
#include <bb/_reg_ble_em_cs.h>
#include <bb/_reg_ble_em_ral.h>
#include <bb/_reg_ble_em_rx_buffer.h>
#include <bb/_reg_ble_em_rx_desc.h>
#include <bb/_reg_ble_em_tx_buffer_cntl.h>
#include <bb/_reg_ble_em_tx_buffer_data.h>
#include <bb/_reg_ble_em_tx_buffer_data.h>
#include <bb/_reg_ble_em_tx_desc.h>
#include <bb/_reg_ble_em_wpb.h>
#include <bb/_reg_ble_em_wpv.h>
#include <bb/reg_blecore.h>
#include <bb/reg_access.h>
#include <bb/reg_assert_mgr.h>
#include <bb/reg_common_em_et.h>
#include <bb/reg_ble_em_cs.h>
#include <bb/reg_ble_em_ral.h>
#include <bb/reg_ble_em_rx_buffer.h>
#include <bb/reg_ble_em_rx_desc.h>
#include <bb/reg_ble_em_tx_buffer_cntl.h>
#include <bb/reg_ble_em_tx_buffer_data.h>
#include <bb/reg_ble_em_tx_buffer_data.h>
#include <bb/reg_ble_em_tx_desc.h>
#include <bb/reg_ble_em_wpb.h>
#include <bb/reg_ble_em_wpv.h>
#include <bb/compiler.h>
#include <bb/arch.h>
#include <bb/co_bt.h>
#include <bb/co_bt_defines.h>
#include <bb/co_endian.h>
#include <bb/co_error.h>
#include <bb/co_hci.h>
#include <bb/co_list.h>
#include <bb/co_llcp.h>
#include <bb/co_lmp.h>
#include <bb/co_version.h>
#include <bb/co_math.h>
#include <bb/co_utils.h>
#include <bb/dbg.h>
#include <bb/dbg_task.h>
#include <bb/dbg_swdiag.h>
#include <bb/dbg_mwsgen.h>
#include <bb/ea.h>
#include <bb/em_buf.h>
#include <bb/em_map.h>
#include <bb/em_map_ble.h>
#include <bb/ll.h>
#include <bb/llc.h>
#include <bb/llc_ch_asses.h>
#include <bb/llc_llcp.h>
#include <bb/llc_task.h>
#include <bb/llc_util.h>
#include <bb/lld.h>
#include <bb/lld_pdu.h>
#include <bb/lld_wlcoex.h>
#include <bb/lld_evt.h>
#include <bb/lld_sleep.h>
#include <bb/lld_util.h>
#include <bb/llm.h>
#include <bb/llm_task.h>
#include <bb/llm_util.h>
#include <bb/rf.h>
#include <bb/rwip_task.h>

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_BB_H */
