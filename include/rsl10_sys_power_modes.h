/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_power_modes.h
 * - Power modes support
 * ----------------------------------------------------------------------------
 * $Revision: 1.45 $
 * $Date: 2019/02/05 19:12:30 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_POWER_MODES_H
#define RSL10_SYS_POWER_MODES_H

#include <stdbool.h>

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/

#define POWER_MODE_SLEEP                0
#define POWER_MODE_STANDBY              1
#define POWER_MODE_WAKEUP_INFO_SIZE     (6 * 4)

/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/

/* Parameters and configurations for Sleep Mode initialization:
 *
 * uint32_t wakeup_cfg:  Wake up configuration, use the following:
 *                       WAKEUP_DELAY_[1 | 2 | 4 | ... | 128],
 *                       WAKEUP_DCDC_OVERLOAD_[ENABLE | DISABLE],
 *                       WAKEUP_WAKEUP_PAD_[RISING | FALLING],
 *                       WAKEUP_DIO*_[RISING | FALLING],
 *                       WAKEUP_DIO*_[ENABLE | DISABLE]
 *
 * uint32_t wakeup_addr: Wake up restore address
 *
 * uint32_t app_addr: Wake up application start address
 *
 * uint32_t mem_power_cfg_wakeup: Memory power configuration for wake-up
 *     The selection of memory instances to be powered on or off when
 *     from RAM depends on the application
 *
 * uint8_t rtc_ctrl: RTC clock source and duration, use the following:
 *     RTC_CLK_SRC_XTAL32K: 32KHz XTAL oscillator
 *     RTC_CLK_SRC_RC_OSC:  32KHz RC oscillator
 *     RTC_CLK_SRC_DIO0:    DIO0
 *     RTC_CLK_SRC_DIO1:    DIO1
 *     RTC_CLK_SRC_DIO2:    DIO2
 *     RTC_CLK_SRC_DIO3:    DIO3
 *     RTC_ALARM_[ 1 | 2 | 4 | ... | 64 ]S
 *
 * uint8_t DMA_channel_RF: The selection of the DMA channel to be used */
struct sleep_mode_init_env_tag
{
    /* Wake up configuration */
    uint32_t wakeup_cfg;

    /* Wake up restore address */
    uint32_t wakeup_addr;

    /* Wake up application start address */
    uint32_t app_addr;

    /* Memory power configuration for wake-up */
    uint32_t mem_power_cfg_wakeup;

    /* Clock source for RTC */
    uint8_t rtc_ctrl;

    /* DMA channel used to save/restore RF registers
     * in each sleep/wake-up cycle */
    uint8_t DMA_channel_RF;
};

/* Parameters and configurations for Sleep Mode:
 *
 * uint32_t wakeup_ctrl: Wake up configuration, use the following:
 *                       PADS_RETENTION_[ENABLE | DISABLE],
 *                       BOOT_FLASH_APP_REBOOT_[ENABLE | DISABLE],
 *                       BOOT_XTAL_[ENABLE | DISABLE],
 *                       BOOT_[CUSTOM | FLASH_XTAL_*],
 *                       WAKEUP_DCDC_OVERLOAD_CLEAR,
 *                       WAKEUP_PAD_EVENT_CLEAR,
 *                       WAKEUP_RTC_ALARM_CLEAR,
 *                       WAKEUP_BB_TIMER_CLEAR,
 *                       WAKEUP_DIO3_EVENT_CLEAR,
 *                       WAKEUP_DIO2_EVENT_CLEAR,
 *                       WAKEUP_DIO1_EVENT_CLEAR,
 *                       WAKEUP_DIO0_EVENT_CLEAR
 *
 * uint32_t mem_power_cfg: Memory power configuration for sleep
 *     The selection of memory instances to be powered on or off in sleep
 *     mode depends on the application */
struct sleep_mode_env_tag
{
    /* Wake up control */
    uint32_t wakeup_ctrl;

    /* Memory power configuration for sleep */
    uint32_t mem_power_cfg;
};

/* Parameters and configurations for Sleep Mode with wake-up from flash:
 *
 * uint32_t wakeup_cfg:  Wake up configuration, use the following:
 *                       WAKEUP_DELAY_[1 | 2 | 4 | ... | 128],
 *                       WAKEUP_DCDC_OVERLOAD_[ENABLE | DISABLE],
 *                       WAKEUP_WAKEUP_PAD_[RISING | FALLING],
 *                       WAKEUP_DIO*_[RISING | FALLING],
 *                       WAKEUP_DIO*_[ENABLE | DISABLE]
 *
 * uint32_t wakeup_ctrl: Wake up configuration, use the following:
 *                       PADS_RETENTION_[ENABLE | DISABLE],
 *                       BOOT_FLASH_APP_REBOOT_[ENABLE | DISABLE],
 *                       BOOT_XTAL_[ENABLE | DISABLE],
 *                       BOOT_[CUSTOM | FLASH_XTAL_*],
 *                       WAKEUP_DCDC_OVERLOAD_CLEAR,
 *                       WAKEUP_PAD_EVENT_CLEAR,
 *                       WAKEUP_RTC_ALARM_CLEAR,
 *                       WAKEUP_BB_TIMER_CLEAR,
 *                       WAKEUP_DIO3_EVENT_CLEAR,
 *                       WAKEUP_DIO2_EVENT_CLEAR,
 *                       WAKEUP_DIO1_EVENT_CLEAR,
 *                       WAKEUP_DIO0_EVENT_CLEAR
 *
 * uint32_t mem_power_cfg: Memory power configuration for sleep
 *     The selection of memory instances to be powered on or off in sleep
 *     mode depends on the application
 *
 * uint8_t VDDCRET_enable: VDDC retention regulator enable/disable
 *     VDDCRET_ENABLE_BITBAND:  Enable VDDC retention regulator
 *     VDDCRET_DISABLE_BITBAND: Disable VDDC retention regulator
 *
 * uint8_t VDDTRET_enable: VDDT retention regulator enable/disable
 *     VDDTRET_ENABLE_BITBAND:  Enable VDDT retention regulator
 *     VDDTRET_DISABLE_BITBAND: Disable VDDT retention regulator
 *
 * uint8_t VDDMRET_enable: VDDM retention regulator enable/disable
 *     VDDMRET_ENABLE_BITBAND:  Enable VDDM retention regulator
 *     VDDMRET_DISABLE_BITBAND: Disable VDDM retention regulator
 *
 * uint32_t rtc_cfg: RTC timer counter preload value
 *
 * uint8_t rtc_ctrl: RTC clock source and duration, use the following:
 *     RTC_CLK_SRC_XTAL32K: 32KHz XTAL oscillator
 *     RTC_CLK_SRC_RC_OSC:  32KHz RC oscillator
 *     RTC_CLK_SRC_DIO0:    DIO0
 *     RTC_CLK_SRC_DIO1:    DIO1
 *     RTC_CLK_SRC_DIO2:    DIO2
 *     RTC_CLK_SRC_DIO3:    DIO3
 *     RTC_ALARM_[ 1 | 2 | 4 | ... | 64 ]S */
struct sleep_mode_flash_env_tag
{
    /* Wake up configuration */
    uint32_t wakeup_cfg;

    /* Wake up control */
    uint32_t wakeup_ctrl;

    /* Memory power configuration for sleep */
    uint32_t mem_power_cfg;

    /* VDDC retention enable/disable */
    uint8_t VDDCRET_enable;

    /* VDDT retention enable/disable */
    uint8_t VDDTRET_enable;

    /* VDDM retention enable/disable */
    uint8_t VDDMRET_enable;
};

/* Parameters and configurations for Standby Mode:
 *
 * int32_t standby_duration_adjustment_lpcycles: the adjustment of
 *            standby duration [number of low power clock cycles]
 *
 * uint32_t wakeup_cfg:  Wake up configuration, use the following:
 *                       WAKEUP_DELAY_[1 | 2 | 4 | ... | 128],
 *                       WAKEUP_DCDC_OVERLOAD_[ENABLE | DISABLE],
 *                       WAKEUP_WAKEUP_PAD_[RISING | FALLING],
 *                       WAKEUP_DIO*_[RISING | FALLING],
 *                       WAKEUP_DIO*_[ENABLE | DISABLE]
 *
 * uint32_t wakeup_ctrl: Wake up configuration, use the following:
 *                       PADS_RETENTION_[ENABLE | DISABLE],
 *                       BOOT_FLASH_APP_REBOOT_[ENABLE | DISABLE],
 *                       BOOT_XTAL_[ENABLE | DISABLE],
 *                       BOOT_[CUSTOM | FLASH_XTAL_*],
 *                       WAKEUP_DCDC_OVERLOAD_CLEAR,
 *                       WAKEUP_PAD_EVENT_CLEAR,
 *                       WAKEUP_RTC_ALARM_CLEAR,
 *                       WAKEUP_BB_TIMER_CLEAR,
 *                       WAKEUP_DIO3_EVENT_CLEAR,
 *                       WAKEUP_DIO2_EVENT_CLEAR,
 *                       WAKEUP_DIO1_EVENT_CLEAR,
 *                       WAKEUP_DIO0_EVENT_CLEAR
 *
 * uint8_t RTC_clk_src: The selection of clock source for RTC
 *     RTC_CLK_SRC_XTAL32K: 32KHz XTAL oscillator
 *     RTC_CLK_SRC_RC_OSC:  32KHz RC oscillator
 *     RTC_CLK_SRC_DIO0:    DIO0
 *     RTC_CLK_SRC_DIO1:    DIO1
 *     RTC_CLK_SRC_DIO2:    DIO2
 *     RTC_CLK_SRC_DIO3:    DIO3
 *
 * uint8_t DMA_channel_RF: The selection of the DMA channel to be used */
struct standby_mode_env_tag
{
    /* Adjustment of standby duration [number of low power clock cycles]
     * This must remain the first parameter as it is used inside blelib */
    int32_t standby_duration_adjustment_lpcycles;

    /* Wake up configuration */
    uint32_t wakeup_cfg;

    /* Wake up control */
    uint32_t wakeup_ctrl;

    /* Standby duration */
    int32_t standby_duration;

    /* Clock source for RTC */
    uint8_t RTC_clk_src;

    /* DMA channel used to save/restore RF registers
     * in each standby/wake-up cycle */
    uint8_t DMA_channel_RF;
};

/* ----------------------------------------------------------------------------
 * Function prototypes
 * ------------------------------------------------------------------------- */

void Sys_PowerModes_Sleep_Init(struct sleep_mode_init_env_tag *sleep_mode_env);

void Sys_PowerModes_Sleep_Init_2Mbps(
		                   struct sleep_mode_init_env_tag *sleep_mode_env);

void Sys_PowerModes_Sleep(struct sleep_mode_env_tag *sleep_mode_env)
     __attribute__ ((section (".sys_powermodes_sleep")));

void Sys_PowerModes_Wakeup(void)
     __attribute__ ((section (".sys_powermodes_wakeup")));

void Sys_PowerModes_Wakeup_2Mbps(void)
     __attribute__ ((section (".sys_powermodes_wakeup_2mbps")));

void Sys_PowerModes_Sleep_WakeupFromFlash(
                           struct sleep_mode_flash_env_tag *sleep_mode_env);

void Sys_PowerModes_Standby_Init(struct standby_mode_env_tag *standby_mode_env);

void Sys_PowerModes_Standby(struct standby_mode_env_tag *standby_mode_env);

void Sys_PowerModes_Standby_Wakeup(struct standby_mode_env_tag *standby_mode_env);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_POWER_MODES_H */
