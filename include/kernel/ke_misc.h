
/**
 ****************************************************************************************
 * @brief Initializes the RW KERNEL.
 *
 ****************************************************************************************
 */
void Kernel_Init(uint32_t error);

// RWBT Environment
#if (DEEP_SLEEP)
struct rwip_env_tag
{
    /// Power_up delay
    uint32_t lp_cycle_wakeup_delay;
    /// Contains sleep duration accumulated timing error
    uint32_t sleep_acc_error;
    /// Duration of sleep and wake-up algorithm (depends on CPU speed) expressed in us.
    uint16_t sleep_algo_dur;
    /// Prevent sleep bit field
    uint16_t prevent_sleep;
    /// Sleep algorithm enabled
    bool sleep_enable;
    /// External wake-up support
    bool     ext_wakeup_enable;
};
#endif //DEEP_SLEEP

extern struct rwip_env_tag rwip_env;
