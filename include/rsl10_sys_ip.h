/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_ip.h
 * - IP protection functions and macros
 * ----------------------------------------------------------------------------
 * $Revision: 1.11 $
 * $Date: 2017/08/11 22:12:51 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_IP_H
#define RSL10_SYS_IP_H

#include <string.h>
/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * IP protection support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_IP_Lock(uint32_t * key)
 * ----------------------------------------------------------------------------
 * Description   : Configure the debug lock key and set the device SWJ-DP to
 *                 lock mode
 * Inputs        : key      - Pointer to the 128-bit key as a debug lock key
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_IP_Lock(uint32_t * key)
{
    uint32_t *dest = (uint32_t *) &(SYSCTRL->DBG_LOCK_KEY);
    *dest++ = *key++;
    *dest++ = *key++;
    *dest++ = *key++;
    *dest = *key;

    SYSCTRL->DBG_LOCK = DBG_ACCESS_LOCK;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_IP_Unlock(void)
 * ----------------------------------------------------------------------------
 * Description   : Set the device SWJ-DP to unlock mode
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_IP_Unlock(void)
{
    SYSCTRL->DBG_LOCK = DBG_ACCESS_UNLOCK;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_IP_H */
