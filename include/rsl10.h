/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10.h
 * - Top-level CMSIS Cortex-M3 compatible header file
 *
 *   Provides the required <Device>.h implementation for CMSIS compatibility
 * ----------------------------------------------------------------------------
 * $Revision: 1.12 $
 * $Date: 2017/06/13 15:04:15 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_H
#define RSL10_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
 extern "C" {
#endif

/* ----------------------------------------------------------------------------
 * Version Code
 * ------------------------------------------------------------------------- */
#define RSL10_SYS_VER_MAJOR             0x01
#define RSL10_SYS_VER_MINOR             0x01
#define RSL10_SYS_VER_REVISION          0x02

#define RSL10_SYS_VER                   ((RSL10_SYS_VER_MAJOR << 12) | \
                                         (RSL10_SYS_VER_MINOR << 8)  | \
                                         (RSL10_SYS_VER_REVISION))

extern const short RSL10_Sys_Version;

/* ----------------------------------------------------------------------------
 * Interrupt and Exception Vector Definitions
 * ------------------------------------------------------------------------- */
#include <rsl10_vectors.h>

/* ----------------------------------------------------------------------------
 * Core CMSIS Configuration
 * ------------------------------------------------------------------------- */
#define __CM3_REV                       0x0201
#define __NVIC_PRIO_BITS                3
#define __MPU_PRESENT                   0
#define __FPU_PRESENT                   0
#define __Vendor_SysTickConfig          0

#include <core_cm3.h>
#include <system_rsl10.h>
#include <rsl10_start.h>

/* ----------------------------------------------------------------------------
 * Peripheral Register Definitions
 * ------------------------------------------------------------------------- */
#if defined ( __CC_ARM )
#pragma anon_unions
#endif

#include <rsl10_hw.h>

#if defined ( __CC_ARM )
#pragma no_anon_unions
#endif

/* ----------------------------------------------------------------------------
 * Memory Map Definitions
 * ------------------------------------------------------------------------- */
#include <rsl10_map.h>
#include <rsl10_map_nvr.h>

/* ----------------------------------------------------------------------------
 * Inline functions supporting functions exposed through the ROM jump table
 * ------------------------------------------------------------------------- */
#include <rsl10_romvect.h>

/* ----------------------------------------------------------------------------
 * Additional Peripheral Support Definitions, Macros and Inline Functions
 * ------------------------------------------------------------------------- */
#include <rsl10_sys_dio.h>
#include <rsl10_sys_aes.h>
#include <rsl10_sys_asrc.h>
#include <rsl10_sys_audio.h>
#include <rsl10_sys_audiosink.h>
#include <rsl10_sys_bbif.h>
#include <rsl10_sys_clocks.h>
#include <rsl10_sys_cm3.h>
#include <rsl10_sys_crc.h>
#include <rsl10_sys_dma.h>
#include <rsl10_sys_flash.h>
#include <rsl10_sys_gpio.h>
#include <rsl10_sys_i2c.h>
#include <rsl10_sys_ip.h>
#include <rsl10_sys_lpdsp32.h>
#include <rsl10_sys_adc.h>
#include <rsl10_sys_pcm.h>
#include <rsl10_sys_power.h>
#include <rsl10_sys_power_modes.h>
#include <rsl10_sys_pwm.h>
#include <rsl10_sys_rffe.h>
#include <rsl10_sys_rtc.h>
#include <rsl10_sys_spi.h>
#include <rsl10_sys_timers.h>
#include <rsl10_sys_uart.h>
#include <rsl10_sys_watchdog.h>

/* ----------------------------------------------------------------------------
 * Generic error code definitions
 * ------------------------------------------------------------------------- */
/* No error */
#define ERRNO_NO_ERROR                  0x0000

/* General error */
#define ERRNO_GENERAL_FAILURE           0x0001

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_H */
