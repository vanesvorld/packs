/* -------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * -------------------------------------------------------------------------
 * rsl10_romvect.h
 * - Vectors for functions embedded in the Program ROM
 *
 * IMPORTANT: Do not include this file directly. Please use <rsl10.h>
 * -------------------------------------------------------------------------
 * $Revision: 1.15 $
 * $Date: 2017/07/14 20:42:32 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_ROMVECT_H
#define RSL10_ROMVECT_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C" {
#endif

/* ----------------------------------------------------------------------------
 * Error codes used by the functions in the ROM vector table
 * ----------------------------------------------------------------------------
 * Boot application error codes */
typedef enum
{
    BOOTROM_ERR_NONE = 0x0,
    BOOTROM_ERR_BAD_ALIGN = 0x1,
    BOOTROM_ERR_BAD_SP = 0x2,
    BOOTROM_ERR_BAD_RESET_VECT = 0x3,
    BOOTROM_ERR_FAILED_START_APP = 0x6,
    BOOTROM_ERR_BAD_CRC = 0x7
} BootROMStatus;

/* System initialization error codes */
typedef enum
{
    SYS_INIT_ERR_NONE = 0x0,
    SYS_INIT_ERR_INVALID_BLOCK_LENGTH = 0x10,
    SYS_INIT_ERR_BAD_CRC = 0x11
} SysInitStatus;

#define RESET_ERR_NONE                          0x00
#define RESET_ERR_WAKEUP_CONFIG_INVALID         0x21
#define RESET_ERR_WAKEUP_ADDR_INVALID           0x22
#define RESET_ERR_WAKEUP_ADDR_FAILED            0x23

/* ----------------------------------------------------------------------------
 * ROM Version
 * ------------------------------------------------------------------------- */
#define ROMVECT_VERSIONADDR                     0x0000001C
#define ROMVECT_VERSION                         (*(uint32_t *)ROMVECT_VERSIONADDR)

/* ----------------------------------------------------------------------------
 * ROM Vector (Function Table) Base
 * ------------------------------------------------------------------------- */
#define ROMVECT_BASEADDR                        0x00000020

/* ----------------------------------------------------------------------------
 * Soft Reset Vector
 * ------------------------------------------------------------------------- */
#define ROMVECT_RESET                           (ROMVECT_BASEADDR + 0x00)

/* ----------------------------------------------------------------------------
 * System Initialization
 * ------------------------------------------------------------------------- */
#define ROMVECT_INITIALIZE_BASE                 (ROMVECT_BASEADDR + 0x04)
#define ROMVECT_INITIALIZE                      (ROMVECT_BASEADDR + 0x08)
#define ROMVECT_GET_TRIM                        (ROMVECT_BASEADDR + 0x34)

/* ----------------------------------------------------------------------------
 * ROM-based System Delay
 * ------------------------------------------------------------------------- */
#define ROMVECT_PROGRAMROM_SYS_DELAY            (ROMVECT_BASEADDR + 0x0C)

/* ----------------------------------------------------------------------------
 * Application Boot Support Vectors
 * ------------------------------------------------------------------------- */
#define ROMVECT_BOOTROM_VALIDATE_APP            (ROMVECT_BASEADDR + 0x10)
#define ROMVECT_BOOTROM_START_APP               (ROMVECT_BASEADDR + 0x14)

/* ----------------------------------------------------------------------------
 * Memory Content Protection Support Vector
 * ------------------------------------------------------------------------- */
#define ROMVECT_UNLOCK_DEBUG                    (ROMVECT_BASEADDR + 0x18)

/* ----------------------------------------------------------------------------
 * Flash Write Support Vectors
 * ------------------------------------------------------------------------- */

#define ROMVECT_FLASH_WRITE_WORD_PAIR           (ROMVECT_BASEADDR + 0x1C)
#define ROMVECT_FLASH_WRITE_BUFFER              (ROMVECT_BASEADDR + 0x20)
#define ROMVECT_FLASH_ERASE_SECTOR              (ROMVECT_BASEADDR + 0x3C)
#define ROMVECT_FLASH_ERASE_ALL                 (ROMVECT_BASEADDR + 0x28)
#define ROMVECT_FLASH_WRITE_COMMAND             (ROMVECT_BASEADDR + 0x2C)
#define ROMVECT_FLASH_WRITE_INTERFACE_CONTROL   (ROMVECT_BASEADDR + 0x30)


/* ----------------------------------------------------------------------------
 * ROM-based NVR4 read
 * ------------------------------------------------------------------------- */
#define ROMVECT_READ_NVR4                       (ROMVECT_BASEADDR + 0x038)

/* ----------------------------------------------------------------------------
 * External Functions
 * ------------------------------------------------------------------------- */
extern BootROMStatus Sys_BootROM_StrictStartApp(uint32_t* vect_table);

/* ----------------------------------------------------------------------------
 * Inline Functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_BootROM_Reset(void)
 * ----------------------------------------------------------------------------
 * Description   : Reset the system by executing the reset vector in the Boot
 *                 ROM
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_BootROM_Reset(void)
{
    /* Call the reset vector */
    (*((void (**)(void)) ROMVECT_RESET))();
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Initialize_Base(void)
 * ----------------------------------------------------------------------------
 * Description   : Run the Program ROM based basic initialization function;
 *                 re-initialize all critical memory, clock, and power supply
 *                 components
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Initialize_Base(void)
{
    /* Call the base initialization vector */
    (*((void (**)(void)) ROMVECT_INITIALIZE_BASE))();
}

/* ----------------------------------------------------------------------------
 * Function      : SysInitStatus Sys_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Run the program ROM's extended initialization functions.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE SysInitStatus Sys_Initialize(void)
{
    /* Call the base initialization vector */
    return (*((SysInitStatus (**)(void)) ROMVECT_INITIALIZE))();
}

/* ----------------------------------------------------------------------------
 * Function      : BootROMStatus
 *                 Sys_BootROM_ValidateApp(uint32_t* vect_table)
 * ----------------------------------------------------------------------------
 * Description   : Validate an application using the Boot ROM application
 *                 checks.
 * Inputs        : vect_table   - Pointer to the vector table at the start of an
 *                                application that will be validated.
 * Outputs       : return value - Status code indicating whether a validation
 *                                error occurred or not; compare against
 *                                BOOTROM_ERR_*
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE BootROMStatus Sys_BootROM_ValidateApp(uint32_t* vect_table)
{
    return (*((BootROMStatus (**)(uint32_t*))
              ROMVECT_BOOTROM_VALIDATE_APP))(vect_table);
}

/* ----------------------------------------------------------------------------
 * Function      : BootROMStatus Sys_BootROM_StartApp(uint32_t* vect_table)
 * ----------------------------------------------------------------------------
 * Description   : Validate and start up an application using the Boot ROM.
 * Inputs        : vect_table   - Pointer to the vector table at the start of an
 *                                application that will be validated and then
 *                                run.
 * Outputs       : return value - Status code indicating application validation
 *                                error if application cannot be started. If not
 *                                returning, the status code is written to the
 *                                top of the started application's stack to
 *                                capture non-fatal validation issues.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE BootROMStatus Sys_BootROM_StartApp(uint32_t* vect_table)
{
    return (*((BootROMStatus (**)(uint32_t*))
              ROMVECT_BOOTROM_START_APP))(vect_table);
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ProgramROM_UnlockDebug(void)
 * ----------------------------------------------------------------------------
 * Description   : Run the unlock routine from the ProgramROM. 
 *                 WARNING: This will unlock the device by erasing the flash
 *                 and SRAM memories! 
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ProgramROM_UnlockDebug(void)
{
    /* Call the base initialization vector */
    (*((void (**)(void)) ROMVECT_UNLOCK_DEBUG))();
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Delay_ProgramROM(uint32_t cycles)
 * ----------------------------------------------------------------------------
 * Description   : Delay by at least the specified number of clock cycles
 * Inputs        : cycles   - Number of system clock cycles to delay
 * Outputs       : None
 * Assumptions   : - The requested delay is at least 32 cycles (32 us at 1 MHz)
 *                   and fits in a uint32_t (0xFFFFFFFF cycles is approximately
 *                   214.75 s at 20 MHz).
 *                 - A delay between cycles and (cycles + 3) provides a
 *                   sufficient delay resolution.
 *                 - The requested delay does not exceed the watchdog timeout.
 *                 - If the delay resolution is required to be exact, disable
 *                   interrupts.
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Delay_ProgramROM(uint32_t cycles)
{
    /* The overhead for this version of the function is greater than that of a
     * direct call to Sys_Delay. Decrement the cycle count before calling the
     * underlying function to account for this added delay. */
    if (cycles > 17)
    {
        (*((uint32_t (**)(uint32_t))
           ROMVECT_PROGRAMROM_SYS_DELAY))(cycles - 17);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Sys_ReadNVR4(unsigned int calib_info_ptr,
                                             unsigned int numReads,
                                             unsigned int *data)
 * ----------------------------------------------------------------------------
 * Description   : Read data from NVR4 using a function implemented in ROM.
 * Inputs        : - info_ptr          - The base register for the specified
 *                                       calibration information.
 *                 - numReads          - The number of words to be read.
 *                 - data              - A pointer to the variable that will
 *                                       hold the read data.
 * Outputs       : - return value      - A code indicating whether an error has
 *                                       occurred.
 *                 - data              - The data read from NVR4 will be
 *                                       contained here.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE unsigned int Sys_ReadNVR4(unsigned int info_ptr,
                                          unsigned int length,
                                          unsigned int *data)
{
    /* Call the NVR4 read with the right arguments */
    return (*((unsigned int (**)
              (unsigned int, unsigned int, unsigned int *))
              ROMVECT_READ_NVR4))((unsigned int)info_ptr, length, data);
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_ROMVECT_H */
