/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_profiles.h
 * - Top-level header file for support of shared profiles elements
 * ----------------------------------------------------------------------------
 * $Revision: 1.2 $
 * $Date: 2017/09/19 19:04:02 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_PROFILES_H
#define RSL10_PROFILES_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
 extern "C" {
#endif

/* ----------------------------------------------------------------------------
 * Profiles support header files
 * ------------------------------------------------------------------------- */
#include <ble/profiles/blp_common.h>
#include <ble/profiles/cpp_common.h>
#include <ble/profiles/cscp_common.h>
#include <ble/profiles/lan_common.h>
#include <ble/profiles/pasp_common.h>
#include <ble/profiles/find_common.h>
#include <ble/profiles/glp_common.h>
#include <ble/profiles/hogp_common.h>
#include <ble/profiles/hrp_common.h>
#include <ble/profiles/htp_common.h>
#include <ble/profiles/pasp_common.h>
#include <ble/profiles/rscp_common.h>
#include <ble/profiles/scpp_common.h>
#include <ble/profiles/tip_common.h>
#include <ble/profiles/anpc.h>
#include <ble/profiles/anps.h>
#include <ble/profiles/basc.h>
#include <ble/profiles/bass.h>
#include <ble/profiles/blpc.h>
#include <ble/profiles/blps.h>
#include <ble/profiles/cppc.h>
#include <ble/profiles/cpps.h>
#include <ble/profiles/cscpc.h>
#include <ble/profiles/cscps.h>
#include <ble/profiles/disc.h>
#include <ble/profiles/diss.h>
#include <ble/profiles/findl.h>
#include <ble/profiles/findt.h>
#include <ble/profiles/glpc.h>
#include <ble/profiles/glps.h>
#include <ble/profiles/hogpbh.h>
#include <ble/profiles/hogpd.h>
#include <ble/profiles/hogprh.h>
#include <ble/profiles/hrpc.h>
#include <ble/profiles/hrps.h>
#include <ble/profiles/htpc.h>
#include <ble/profiles/htpt.h>
#include <ble/profiles/lanc.h>
#include <ble/profiles/lans.h>
#include <ble/profiles/paspc.h>
#include <ble/profiles/pasps.h>
#include <ble/profiles/proxm.h>
#include <ble/profiles/proxr.h>
#include <ble/profiles/rscpc.h>
#include <ble/profiles/rscps.h>
#include <ble/profiles/scppc.h>
#include <ble/profiles/scpps.h>
#include <ble/profiles/tipc.h>
#include <ble/profiles/tips.h>
#include <ble/profiles/anpc_task.h>
#include <ble/profiles/anps_task.h>
#include <ble/profiles/basc_task.h>
#include <ble/profiles/bass_task.h>
#include <ble/profiles/blpc_task.h>
#include <ble/profiles/blps_task.h>
#include <ble/profiles/cppc_task.h>
#include <ble/profiles/cpps_task.h>
#include <ble/profiles/cscpc_task.h>
#include <ble/profiles/cscps_task.h>
#include <ble/profiles/disc_task.h>
#include <ble/profiles/diss_task.h>
#include <ble/profiles/findl_task.h>
#include <ble/profiles/findt_task.h>
#include <ble/profiles/glpc_task.h>
#include <ble/profiles/glps_task.h>
#include <ble/profiles/hogpbh_task.h>
#include <ble/profiles/hogpd_task.h>
#include <ble/profiles/hogprh_task.h>
#include <ble/profiles/hrpc_task.h>
#include <ble/profiles/hrps_task.h>
#include <ble/profiles/htpc_task.h>
#include <ble/profiles/htpt_task.h>
#include <ble/profiles/lanc_task.h>
#include <ble/profiles/lans_task.h>
#include <ble/profiles/paspc_task.h>
#include <ble/profiles/pasps_task.h>
#include <ble/profiles/proxm_task.h>
#include <ble/profiles/proxr_task.h>
#include <ble/profiles/rscpc_task.h>
#include <ble/profiles/rscps_task.h>
#include <ble/profiles/scppc_task.h>
#include <ble/profiles/scpps_task.h>
#include <ble/profiles/tipc_task.h>
#include <ble/profiles/tips_task.h>
#include <ble/profiles/wpt_common.h>
#include <ble/profiles/wptc_task.h>
#include <ble/profiles/wptc.h>
#include <ble/profiles/wpts_task.h>
#include <ble/profiles/wpts.h>

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_PROFILES_H */
