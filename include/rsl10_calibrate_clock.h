/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC
 * (d/b/a ON Semiconductor). All Rights Reserved.
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor. The
 * terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_calibrate_clock.h
 * - Calibration support functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.3 $
 * $Date: 2017/05/25 21:02:50 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_CALIBRATE_CLOCK_H
#define RSL10_CALIBRATE_CLOCK_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Internal library defines for clock calibration
 * ------------------------------------------------------------------------- */
#define TRIMMING_STEP                   0.015
#define AUDIO_SINK_PERIODS              16

#define CAL_32K_RCOSC                   1
#define CAL_START_OSC                   2

/* ----------------------------------------------------------------------------
 * Function Prototypes
 * ------------------------------------------------------------------------- */
void Calibrate_Clock_Initialize(void);
unsigned int Calibrate_Clock_32K_RCOSC(uint32_t target);
unsigned int Calibrate_Clock_Start_OSC(uint32_t target);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_CALIBRATE_CLOCK_H */
