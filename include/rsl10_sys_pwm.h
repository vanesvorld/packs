/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_pwm.h
 * - Pulse-Width Modulation(PWM) support functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.6 $
 * $Date: 2017/07/07 20:02:44 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_PWM_H
#define RSL10_SYS_PWM_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * PWM support functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_PWM_Enable(uint32_t num, uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Enable or disable a PWM
 * Inputs        : - num    - The PWM interface to enable or diable; use 0 or 1
 *                 - cfg    - The PWM bitband enable/disable setting; use
 *                            PWM*_[DISABLE | ENABLE]_BITBAND
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PWM_Enable(uint32_t num, uint32_t cfg)
{
    /* Calculate the word length offset from PWM_CTRL_BASE to the correct
     * bit-band alias that needs to be configured */
    *(&PWM_CTRL->PWM0_ENABLE_ALIAS + num * PWM_CTRL_PWM1_ENABLE_Pos) = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_PWM_Control(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Set the control configuration for the two PWM interfaces
 * Inputs        : cfg      - The PWM bitband enable/disable setting; use
 *                            PWM*_[DISABLE | ENABLE],
 *                            PWM_OFFSET_[DISABLE | ENABLE], and a constant
 *                            shifted to PWM_CTRL_PWM_OFFSET_Pos
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PWM_Control(uint32_t cfg)
{
    PWM->CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_PWM_Config(uint32_t num, uint32_t period,
 *                                     uint32_t duty)
 * ----------------------------------------------------------------------------
 * Description   : Configure a pulse-width modulator
 * Inputs        : - num    - The PWM interface to configure; use 0 or 1
 *                 - period - The period length for the PWM in cycles
 *                 - duty   - The high part of the period for the PWM in cycles
 * Outputs       : None
 * Assumptions   : - PWM period is (period + 1) cycles long
 *                 - PWM duty is high for (duty + 1) cycles
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PWM_Config(uint32_t num, uint32_t period,
                                    uint32_t duty)
{
    PWM->CFG[num] = (((duty << PWM_CFG_PWM_HIGH_Pos) & PWM_CFG_PWM_HIGH_Mask)   |
                  ((period<< PWM_CFG_PWM_PERIOD_Pos) & PWM_CFG_PWM_PERIOD_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_PWM_ConfigAll(uint32_t period, uint32_t duty)
 * ----------------------------------------------------------------------------
 * Description   : Configure both pulse-width modulators with the same
 *                 configuration
 * Inputs        : - period - The period length for the PWMs in cycles
 *                 - duty   - The high part of the period for the PWMs in cycles
 * Outputs       : None
 * Assumptions   : - PWM period is (period + 1) cycles long
 *                 - PWM duty is high for (duty + 1) cycles
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PWM_ConfigAll(uint32_t period, uint32_t duty)
{
    Sys_PWM_Config(0, period, duty);
    Sys_PWM_Config(1, period, duty);
}


/* ----------------------------------------------------------------------------
 * Function      : void Sys_PWM_DIOConfig(uint32_t numinv, uint32_t cfg,
 *                                      uint32_t pwm)
 * ----------------------------------------------------------------------------
 * Description   : Configure DIO for the specified PWM
 * Inputs        : - numinv - PWM number; use
 *                            DIO_MODE_[PWM0 | PWM0_INV | PWM1 | PWM1_INV]
 *                 - cfg    - DIO pin configuration for the PWM output
 *                 - pwm    - DIO to use as the PWM output pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PWM_DIOConfig(uint32_t numinv, uint32_t cfg,
                                        uint32_t pwm)
{
    Sys_DIO_Config(pwm, ((cfg) | (numinv)));
}
/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_PWM_H */
