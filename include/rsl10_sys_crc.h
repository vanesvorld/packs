/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_crc.2h
 * - Cyclic-Redundancy Check (CRC) peripheral support
 * ----------------------------------------------------------------------------
 * $Revision: 1.9 $
 * $Date: 2017/07/07 19:02:12 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_CRC_H
#define RSL10_SYS_CRC_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Cyclic-Redundancy Check (CRC) support function prototypes
 * ------------------------------------------------------------------------- */
extern uint32_t Sys_CRC_Check(uint32_t type, uint32_t base, uint32_t top);
extern uint32_t Sys_CRC_Calc(uint32_t type, uint32_t base, uint32_t top,
        uint32_t* crc_val);

/* ----------------------------------------------------------------------------
 * Cyclic-Redundancy Check (CRC) support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_CRC_Set_Config(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the CRC generator type, endianness of the
 *                 input data, and standard vs non-standard CRC behavior
 * Inputs        : cfg      - CRC generator configuration; use
 *                            CRC_[CCITT | 32],
 *                            CRC_[BIG | LITTLE]_ENDIAN,
 *                            CRC_BIT_ORDER_[STANDARD | NON_STANDARD],
 *                            CRC_FINAL_REVERSE_[STANDARD | NON_STANDARD], and
 *                            CRC_FINAL_XOR_[STANDARD | NON_STANDARD]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_CRC_Set_Config(uint32_t cfg)
{
    CRC->CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_CRC_Get_Config(void)
 * ----------------------------------------------------------------------------
 * Description   : Get the CRC generator configuration
 * Inputs        : None
 * Outputs       : return value  - CRC generator configuration; compare with
 *                                 CRC_[CCITT | 32],
 *                                 CRC_[BIG | LITTLE]_ENDIAN,
 *                                 CRC_BIT_ORDER_[STANDARD | NON_STANDARD],
 *                                 CRC_FINAL_REVERSE_[STANDARD | NON_STANDARD],
 *                                 and CRC_FINAL_XOR_[STANDARD | NON_STANDARD]
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_CRC_Get_Config(void)
{
    return CRC->CTRL;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_CRC_H */
