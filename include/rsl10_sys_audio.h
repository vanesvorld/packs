/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_audio.h
 * - AUDIO peripheral support
 * ----------------------------------------------------------------------------
 * $Revision: 1.15 $
 * $Date: 2017/11/13 10:15:51 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_AUDIO_H
#define RSL10_SYS_AUDIO_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * AUDIO peripheral support function prototypes
 * ------------------------------------------------------------------------- */
extern void Sys_Audio_ODDIOConfigMult(uint32_t cfg, uint32_t * od_p,
                uint32_t * od_n, uint32_t num);
/* ----------------------------------------------------------------------------
 * AUDIO support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audio_ODDIOConfig(uint32_t cfg, uint32_t od_p,
 *                                            uint32_t od_n)
 * ----------------------------------------------------------------------------
 * Description   : Configure two DIOs for the specified OD data output
 *                 selection
 * Inputs        : - cfg        - DIO pin configuration for the OD outputs
 *                 - od_p       - DIO to use as the OD positive pin
 *                 - od_n       - DIO to use as the OD negative pin
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Audio_ODDIOConfig(uint32_t cfg, uint32_t od_p,
                                           uint32_t od_n)
{
    Sys_DIO_Config(od_p, ((cfg) | DIO_MODE_OD_P));
    Sys_DIO_Config(od_n, ((cfg) | DIO_MODE_OD_N));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audio_Set_Config(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the audio block
 * Inputs        : cfg         -    The DMIC and OD configuration values; use
 *                                  OD_[AUDIOCLK | AUDIOSLOWCLK],
 *                                  DMIC_[AUDIOCLK | AUDIOSLOWCLK],
 *                                  DECIMATE_BY_*,
 *                                  OD_UNDERRUN_PROTECT_[ENABLE | DISABLE],
 *                                  OD_DMA_REQ_[ENABLE | DISABLE],
 *                                  OD_INT_GEN_[ENABLE | DISABLE],
 *                                  OD_DATA_[LSB | MSB]_ALIGNED,
 *                                  OD_[ENABLE | DISABLE],
 *                                  DMIC*_DMA_REQ_[ENABLE | DISABLE],
 *                                  DMIC*_INT_GEN_[ENABLE | DISABLE],
 *                                  DMIC*_DATA_[LSB | MSB]_ALIGNED, and
 *                                  DMIC*_[ENABLE | DISABLE]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Audio_Set_Config(uint32_t cfg)
{
    AUDIO->CFG = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audio_Set_DMICConfig(uint32_t cfg,
 *                                               uint32_t frac_delay)
 * ----------------------------------------------------------------------------
 * Description   : Configure the DMIC
 * Inputs        : - cfg        -   The DMIC configuration; use
 *                                  DMIC*_DCRM_CUTOFF_[*HZ | DISABLE],
 *                                  DMIC1_DELAY_[*P* | DISABLE], and
 *                                  DMIC*_[FALLING | RISING]_EDGE
 *                 - frac_delay -   DMIC1 fractional delay; use a 5- bit number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Audio_Set_DMICConfig(uint32_t cfg,
                                              uint32_t frac_delay)
{
    AUDIO->DMIC_CFG =
            ((cfg & ~(AUDIO_DMIC_CFG_DMIC1_FRAC_DELAY_Mask))       |
             (frac_delay << AUDIO_DMIC_CFG_DMIC1_FRAC_DELAY_Pos));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audio_Set_ODConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the OD block and sigma-delta modulator
 *                 for normal operation
 * Inputs        : cfg         -    The OD configuration; use
 *                                  DCRM_CUTOFF_[*HZ | DISABLE],
 *                                  DITHER_[ENABLE | DISABLE], and
 *                                  OD_[RISING | FALLING]_EDGE
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Audio_Set_ODConfig(uint32_t cfg)
{
    AUDIO->OD_CFG = cfg;
    AUDIO->SDM_CFG = SDM_CFG_NORMAL;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audio_DMICDIOConfig(uint32_t cfg, uint32_t clk,
 *                                              uint32_t data,
 *                                              uint32_t clk_ext)
 * ----------------------------------------------------------------------------
 * Description   : Configure two DIOs for the specified DMIC data input
 *                  selection
 * Inputs        : - cfg        -   DIO pin configuration for the DMIC input
 *                 - clk        -   DIO to use as the DMIC clock out pad
 *                 - data       -   DIO to use as the DMIC input pad
 *                 - clk_ext    -   Clock source for external clock on DMIC
 *                                  clock pad; use
 *                                  DIO_MODE_[AUDIOCLK | AUDIOSLOWCLK]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Audio_DMICDIOConfig(uint32_t cfg, uint32_t clk,
                                        uint32_t data, uint32_t clk_ext)
{
    Sys_DIO_Config(clk, ((cfg) | clk_ext));
    Sys_DIO_Config(data, ((cfg) | (DIO_MODE_INPUT)));
    DIO->DMIC_SRC = (((clk << DIO_DMIC_SRC_CLK_Pos)& DIO_DMIC_SRC_CLK_Mask)  |
                   ((data  << DIO_DMIC_SRC_DATA_Pos)& DIO_DMIC_SRC_DATA_Mask));
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_AUDIO_H */
