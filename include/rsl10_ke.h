/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_ke.h
 * - Top-level header file for support of shared kernel elements
 * ----------------------------------------------------------------------------
 * $Revision: 1.2 $
 * $Date: 2017/09/19 19:04:02 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_KE_H
#define RSL10_KE_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
 extern "C" {
#endif

/* ----------------------------------------------------------------------------
 * Kernel support header files
 * ------------------------------------------------------------------------- */
#include <rsl10_bb.h>
#include <kernel/ke.h>
#include <kernel/ke_config.h>
#include <kernel/ke_event.h>
#include <kernel/ke_mem.h>
#include <kernel/ke_misc.h>
#include <kernel/ke_msg.h>
#include <kernel/ke_task.h>
#include <kernel/ke_timer.h>

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_KE_H */
