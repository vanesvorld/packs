/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_lpdsp32.h
 * - LPDSP32 support functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.7 $
 * $Date: 2017/07/07 19:02:12 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_LPDSP32_H
#define RSL10_SYS_LPDSP32_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * LPDSP32 support macros
 * ------------------------------------------------------------------------- */

 /* ----------------------------------------------------------------------------
  * DMA DSP Data RAM 0/1 : 16 kBytes
  * ------------------------------------------------------------------------- */
#define LPDSP_DRAM01_BASE               0x000000
#define LPDSP_DRAM01_TOP                0x003FFF

 /* ----------------------------------------------------------------------------
  * DMA DSP Data RAM 2/3 : 16 kBytes
  * ------------------------------------------------------------------------- */
#define LPDSP_DRAM23_BASE               0x004000
#define LPDSP_DRAM23_TOP                0x007FFF

 /* ----------------------------------------------------------------------------
  * DMA DSP Data RAM 4/5 : 16 kBytes
  * ------------------------------------------------------------------------- */
#define LPDSP_DRAM45_BASE               0x008000
#define LPDSP_DRAM45_TOP                0x00BFFF

 /* ----------------------------------------------------------------------------
  * DMA DSP Data RAM 1/2 : 16 kBytes
  * ------------------------------------------------------------------------- */
#define LPDRAM12_BASE                   0x00C000
#define LPDRAM12_TOP                    0x00FFFF

/* ----------------------------------------------------------------------------
 * DSP Data RAM 1 :  8 kBytes
 * ------------------------------------------------------------------------- */
#define LPDRAM1_BASE                    0x804000
#define LPDRAM1_TOP                     0x805FFF

/* ----------------------------------------------------------------------------
  * DSP Data RAM 2 :  8 kBytes
  * ------------------------------------------------------------------------- */
#define LPDRAM2_BASE                    0x806000
#define LPDRAM2_TOP                     0x807FFF

 /* ----------------------------------------------------------------------------
  * DSP Data RAM 4 :  8 kBytes
  * ------------------------------------------------------------------------- */
#define LPDSP_DRAM4_BASE                0x800000
#define LPDSP_DRAM4_TOP                 0x801FFF

/* ----------------------------------------------------------------------------
 * DSP Data RAM 5 :  8 kBytes
 * ------------------------------------------------------------------------- */
#define LPDSP_DRAM5_BASE                0x802000
#define LPDSP_DRAM5_TOP                 0x803FFF

/* ----------------------------------------------------------------------------
 * Map Registers
 * ------------------------------------------------------------------------- */
#define MAP_REGISTERS_BASE              0xC00000
#define MAP_REGISTERS_TOP               0xC00007


/* ----------------------------------------------------------------------------
 * Macro      : Sys_LPDSP32_RuntimeAddr(Addr, prgdata)
 * ----------------------------------------------------------------------------
 * Description   : Calculate the equivalent LPDSP32 address to an ARM Cortex-M3
 *                 processor address.
 * Inputs        : - Addr         - the address in LPDSP32
 *                 - prgdata      - selection between program memory or data
 *                                  memory in LPDSP32. Value 1 indicates
 *                                  program section and 0 shows data section
 * Outputs       : return value   - equivalent address in ARM Cortex-M3
 *                                  processor
 * Assumptions   : For DSP_PROMx the output will contain the 32 bit LSB
 *                 locations
 * ------------------------------------------------------------------------- */
#define Sys_LPDSP32_RuntimeAddr(addr, prgdata) (prgdata==0)?(\
    (addr>= LPDSP_DRAM01_BASE && addr<= LPDSP_DRAM01_TOP) ? \
    ((addr%8> 3))?(DSP_DRAM1_BASE+ ((addr-4)>>1 + addr%0x2)):\
            (DSP_DRAM0_BASE+ (addr>>1 + addr % 0x2))        :\
            \
    (addr>= LPDSP_DRAM23_BASE && addr<= LPDSP_DRAM23_TOP) ? \
    ((addr%8> 3))?(DSP_DRAM3_BASE+ ((addr-4)>>1 + addr%0x2)):\
            (DSP_DRAM2_BASE+ (addr>>1 + addr % 0x2))        :\
            \
    (addr>= LPDSP_DRAM45_BASE && addr<= LPDSP_DRAM45_TOP) ? \
    ((addr%8> 3))?(DSP_DRAM5_BASE+ ((addr-4)>>1 + addr%0x2)):\
            (DSP_DRAM4_BASE+ (addr>>1 + addr % 0x2))        :\
            \
    (addr>= LPDRAM12_BASE && addr<= LPDRAM12_TOP)?\
    ((addr%8> 3))?(DRAM2_BASE+ ((addr-4)>>1 + addr%0x2)):\
            (DRAM1_BASE+ (addr>>1 + addr & 0x1))            :\
            \
    (addr>= LPDSP_DRAM4_BASE && addr<= LPDSP_DRAM5_TOP)?\
            (addr-LPDSP_DRAM4_BASE)+ DSP_DRAM4_BASE :\
            \
    (addr>= LPDRAM1_BASE && addr<= LPDRAM2_TOP)? \
            (addr-LPDRAM1_BASE)+ DRAM1_BASE :\
            \
    (addr>= MAP_REGISTERS_BASE && addr<= MAP_REGISTERS_TOP)? 0:\
            \
    0x00000000)/*Invalid address*/\
: addr*4 + DSP_PRAM0_BASE\

/* ----------------------------------------------------------------------------
 * LPDSP32 support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_LPDSP32_Reset(void)
 * ----------------------------------------------------------------------------
 * Description   : Reset LPDSP32
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_LPDSP32_Reset(void)
{
    SYSCTRL_DSS_CTRL->DSS_RESET_ALIAS = DSS_RESET;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_LPDSP32_Pause(void)
 * ----------------------------------------------------------------------------
 * Description   : Pause LPDSP32
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_LPDSP32_Pause(void)
{
    SYSCTRL_DSS_CTRL->LPDSP32_PAUSE_ALIAS = DSS_LPDSP32_PAUSE;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_LPDSP32_Run(void)
 * ----------------------------------------------------------------------------
 * Description   : Run LPDSP32
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_LPDSP32_Run(void)
{
    SYSCTRL_DSS_CTRL->LPDSP32_RESUME_ALIAS = DSS_LPDSP32_RESUME;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_LPDSP32_Run_Status(void)
 * ----------------------------------------------------------------------------
 * Description   : LPDSP32 running status
 * Inputs        : None
 * Outputs       : return value - LPDSP32 status; compare with
 *                                DSS_LPDSP32_STATE_[PAUSE | RUN]
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_LPDSP32_Run_Status(void)
{
    return SYSCTRL_DSS_CTRL->LPDSP32_RUNNING_ALIAS;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_LPDSP32_IntClear(void)
 * ----------------------------------------------------------------------------
 * Description   : Reset pending (DMA and ARM Cortex-M3) interrupts in the
 *                 LPDSP32 interrupt controller
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_LPDSP32_IntClear(void)
{
    SYSCTRL_DSS_CTRL->DSS_DMA_INT_RESET_ALIAS= DSS_DMA_INT_RESET;
    SYSCTRL_DSS_CTRL->DSS_CSS_INT_RESET_ALIAS= DSS_CSS_INT_RESET;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_LPDSP32_Set_DebugConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the LPDSP32 Debug Port
 * Inputs        : cfg      - debug port configuration; use
 *                            LPDSP32_DEBUG_IN_POWERDOWN_[DISABLED | ENABLE] |
 *                            LPDSP32_EXIT_POWERDOWN_WHEN_HALTED_[DISABLED |
 *                            ENABLE]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_LPDSP32_Set_DebugConfig(uint32_t cfg)
{
    SYSCTRL->LPDSP32_DEBUG_CFG= cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_LPDSP32_Get_ActivityCounter(void)
 * ----------------------------------------------------------------------------
 * Description   : Read LPDSP32 activity counter value
 * Inputs        : None
 * Outputs       : return value     - LPDSP32 activity counter value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_LPDSP32_Get_ActivityCounter(void)
{
    return SYSCTRL->LPDSP32_CNT;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_LPDSP32_Command(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure commands for LPDSP32
 * Inputs        : cfg       - Set LPDSP32 commands; use DSS_CMD_[0- 6]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_LPDSP32_Command(uint32_t cfg)
{
    SYSCTRL->DSS_CMD = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : Sys_LPDSP32_DIOJTAG(uint32_t cfg, uint32_t tdi,
 *                                               uint32_t tms, uint32_t tck,
 *                                               uint32_t tdo)
 * ----------------------------------------------------------------------------
 * Description   : Configure DIO pads connected to LPDSP32 JTAG. It causes the
 *                 LPDSP32 to be resumed.
 * Inputs        : - cfg   - DIO pin configuration for LPDSP32 JTAG pads
 *                 - tdi   - DIO to use as the JTAG TDI pad
 *                 - tms   - DIO to use as the JTAG TMS pad
 *                 - tck   - DIO to use as the JTAG TCK pad
 *                 - tdo   - DIO to use as the JTAG TDO pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_LPDSP32_DIOJTAG(uint32_t cfg, uint32_t tdi,
                                                 uint32_t tms, uint32_t tck,
                                                 uint32_t tdo)
{
    SYSCTRL_LPDSP32_DEBUG_CFG->LPDSP32_DEBUG_ENABLE_ALIAS = LPDSP32_DEBUG_ENABLED_BITBAND;
    Sys_DIO_Config(tdi, ((cfg) | (DIO_MODE_INPUT)));
    Sys_DIO_Config(tms, ((cfg) | (DIO_MODE_INPUT)));
    Sys_DIO_Config(tck, ((cfg) | (DIO_MODE_INPUT)));
    Sys_DIO_Config(tdo, (DIO_MODE_LPDSP32_TDO));
    DIO->LPDSP32_JTAG_SRC = ((tdi << DIO_LPDSP32_JTAG_SRC_TDI_Pos)  |
                        (tms << DIO_LPDSP32_JTAG_SRC_TMS_Pos)       |
                        (tck << DIO_LPDSP32_JTAG_SRC_TCK_Pos));
    SYSCTRL->DSS_CTRL = DSS_LPDSP32_RESUME;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_LPDSP32_H */
