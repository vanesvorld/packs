/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_power.h
 * - Power components support
 * ----------------------------------------------------------------------------
 * $Revision: 1.15 $
 * $Date: 2017/08/11 21:39:34 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_POWER_H
#define RSL10_SYS_POWER_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Error codes returned by the library
 * ------------------------------------------------------------------------- */
/* Power supply calibration errors */
#define ERRNO_POWER_LOAD_MARKER          0x10
#define ERRNO_VBG_LOAD_ERROR             (0x0001 | ERRNO_POWER_LOAD_MARKER)
#define ERRNO_VDDRF_LOAD_ERROR           (0x0002 | ERRNO_POWER_LOAD_MARKER)
#define ERRNO_VDDPA_LOAD_ERROR           (0x0003 | ERRNO_POWER_LOAD_MARKER)
#define ERRNO_DCDC_LOAD_ERROR            (0x0004 | ERRNO_POWER_LOAD_MARKER)
#define ERRNO_VDDC_LOAD_ERROR            (0x0005 | ERRNO_POWER_LOAD_MARKER)
#define ERRNO_VDDC_STANDBY_LOAD_ERROR    (0x0006 | ERRNO_POWER_LOAD_MARKER)
#define ERRNO_VDDM_LOAD_ERROR            (0x0007 | ERRNO_POWER_LOAD_MARKER)
#define ERRNO_VDDM_STANDBY_LOAD_ERROR    (0x0008 | ERRNO_POWER_LOAD_MARKER)

/* ----------------------------------------------------------------------------
 * Function prototypes
 * ------------------------------------------------------------------------- */
unsigned int Sys_Power_BandGapCalibratedConfig(uint8_t target);
unsigned int Sys_Power_VDDRFCalibratedConfig(uint8_t target);
unsigned int Sys_Power_VDDPACalibratedConfig(uint8_t target);
unsigned int Sys_Power_VDDCCalibratedConfig(uint8_t target);
unsigned int Sys_Power_VDDCStandbyCalibratedConfig(uint8_t target);
unsigned int Sys_Power_VDDMCalibratedConfig(uint8_t target);
unsigned int Sys_Power_VDDMStandbyCalibratedConfig(uint8_t target);
unsigned int Sys_Power_DCDCCalibratedConfig(uint8_t target);

/* ----------------------------------------------------------------------------
 * Power components wrapper support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_Power_Get_ResetAnalog(void)
 * ----------------------------------------------------------------------------
 * Description   : Read ACS reset source status
 * Inputs        : None
 * Outputs       : return value - read status of reset source; compare with
 *                 CLK_DET_RESET_FLAG_SET,
 *                 VDDM_RESET_FLAG_SET,
 *                 VDDC_RESET_FLAG_SET,
 *                 PAD_RESET_FLAG_SET, and
 *                 POR_RESET_FLAG_SET
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_Power_Get_ResetAnalog(void)
{
    return ACS->RESET_STATUS;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Power_ResetAnalogClearFlags(void)
 * ----------------------------------------------------------------------------
 * Description   : Clear all the analog reset flags
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Power_ResetAnalogClearFlags(void)
{
    ACS->RESET_STATUS =     CLK_DET_RESET_FLAG_CLEAR    |
                            VDDM_RESET_FLAG_CLEAR       |
                            VDDC_RESET_FLAG_CLEAR       |
                            PAD_RESET_FLAG_CLEAR        |
                            POR_RESET_FLAG_CLEAR;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_Power_Get_ResetDigital(void)
 * ----------------------------------------------------------------------------
 * Description   : Read digital reset source status
 * Inputs        : None
 * Outputs       : return value   - setting for reset source status; use
 *                                  ACS_RESET_[NOT_SET | SET],
 *                                  CM3_SW_RESET_[NOT_SET | SET],
 *                                  WATCHDOG_RESET_[NOT_SET | SET], and
 *                                  LOCKUP_NOT_[NOT_SET | SET]
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_Power_Get_ResetDigital(void)
{
    return DIG->RESET_STATUS;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Power_ResetDigitalClearFlags(void)
 * ----------------------------------------------------------------------------
 * Description   : Clear all the digital reset flags
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Power_ResetDigitalClearFlags(void)
{
    DIG->RESET_STATUS =     LOCKUP_FLAG_CLEAR           |
                            WATCHDOG_RESET_FLAG_CLEAR   |
                            CM3_SW_RESET_FLAG_CLEAR     |
                            ACS_RESET_FLAG_CLEAR;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Power_BandGapConfig(uint32_t cfg_slope,
 *                                              uint32_t cfg_vtrim)
 * ----------------------------------------------------------------------------
 * Description   : Configure the band gap supply voltage
 * Inputs        : - cfg_vtrim   - Reference voltage trimming; use
 *                                 BG_TRIM_0p*
 *                 - cfg_slope   - Temperature coefficient trimming; use a
 *                                 6-bit number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Power_BandGapConfig(uint32_t cfg_slope,
                                             uint32_t cfg_vtrim)
{
    ACS->BG_CTRL = ((cfg_vtrim & ACS_BG_CTRL_VTRIM_Mask)         |
                    (cfg_slope & ACS_BG_CTRL_SLOPE_TRIM_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_Power_BandGapStatus(void)
 * ----------------------------------------------------------------------------
 * Description   : Read Bandgap status
 * Inputs        : None
 * Outputs       : return value - content of the BG register; compare with
 *                                BG_NOT_READY | BG_READY, and
 *                                BG_TRIM_0p*V
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_Power_BandGapStatus(void)
{
    return ACS->BG_CTRL;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Power_VCCConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure DC-DC/ LDO supply
 * Inputs        : cfg            - DC-DC/ LDO supply value; use
 *                                  VCC_ICHTRIM_*MA,
 *                                  VCC_[MULTI | SINGLE]_PULSE,
 *                                  VCC_CONSTANT_[CHARGE | IMAX],
 *                                  VCC_[BUCK | VBAT], and
 *                                  VCC_TRIM_1p*V_BYTE
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Power_VCCConfig(uint32_t cfg)
{
    ACS->VCC_CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Power_VDDPAConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure power amplifier RF block regulator
 * Inputs        : cfg        - Power amplifier supply control, enable current
 *                              sensing circuit, enable control and output
 *                              voltage trimming configuration; use
 *                              VDDPA_[DISABLE | ENABLE,
 *                              VDDPA_TRIM_*V,
 *                              VDDPA_ISENSE_[DISABLE | ENABLE], and
 *                              VDDPA_SW_[HIZ | GND]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Power_VDDPAConfig(uint32_t cfg)
{
    ACS->VDDPA_CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Power_VDDRFConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure RF block regulator
 * Inputs        : cfg       - set the RF block regulator; use
 *                             VDDRF_TRIM_*V,
 *                             VDDRF_[DISABLE | ENABLE], and
 *                             VDDRF_DISABLE_[HIZ | GND]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Power_VDDRFConfig(uint32_t cfg)
{
    ACS->VDDRF_CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Power_VDDAConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure analog voltage maximum current and sleep mode
 *                  clamp control
 * Inputs        : cfg    - Configuration for output power trimming; use
 *                          VDDA_PTRIM_*MA
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Power_VDDAConfig(uint32_t cfg)
{
    ACS->VDDA_CP_CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Power_VDDCConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure digital core voltage regulator
 * Inputs        : cfg    - setting standby voltage trimming, low power mode
 *                          control, sleep mode clamp control and output
 *                          voltage trimming configuration; use
 *                          VDDC_TRIM_*V,
 *                          VDDC_SLEEP_[HIZ | GND],
 *                          VDDC_[LOW| NOMINAL]_BIAS, and
 *                          VDDC_STANDBY_TRIM_*V
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Power_VDDCConfig(uint32_t cfg)
{
    ACS->VDDC_CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Power_VDDMConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure memories' voltage regulator setting
 * Inputs        : cfg    - value of the memory voltage regulator; use
 *                          register
 *                          VDDM_TRIM_*V,
 *                          VDDM_SLEEP_[HIZ | GND],
 *                          VDDM_[LOW| NOMINAL]_BIAS, and
 *                          VDDM_STANDBY_TRIM_*V
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Power_VDDMConfig(uint32_t cfg)
{
    ACS->VDDM_CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * IFunction     : unsigned int Sys_GetTrim(uint32_t *calib_info_base,
 *                                          uint16_t calibration_target,
 *                                          uint16_t *trim_value)
 * ----------------------------------------------------------------------------
 * Description   : Search the calibration records for a trim value that matches
 *                 the specified target.
 * Inputs        : - calib_info_base    - The base address for the specified
 *                                        calibration information
 *                 - calibration_target - The expected frequency or voltage
 * Outputs       : - return value       - A code indicating whether an error
 *                                        has occurred
 *                 - trim_value         - The trim value corresponding to the
 *                                        target, if found
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE unsigned int Sys_GetTrim(uint32_t *calib_info_base,
                                         uint16_t calibration_target,
                                         uint16_t *trim_value)
{
    return (*((unsigned int (**)(uint32_t *, uint16_t, uint16_t *))
            ROMVECT_GET_TRIM))(calib_info_base, calibration_target, trim_value);
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_POWER_H */
