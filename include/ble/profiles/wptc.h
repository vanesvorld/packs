/**
 ****************************************************************************************
 *
 * @file wptc.h
 *
 * @brief Header file - Wireless Power Transfer Profile - Collector Role.
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 * $ Rev $
 *
 ****************************************************************************************
 */

#ifndef _WPTC_H_
#define _WPTC_H_

/**
 ****************************************************************************************
 * @addtogroup WPTC Wireless Power Transfer Profile Collector
 * @ingroup WSCP
 * @brief Wireless Power Transfer Profile Collector
 * @{
 ****************************************************************************************
 */

/*
 * DEFINES
 ****************************************************************************************
 */

/// Maximum number of Wireless Power Transfer Task instances
#define WPTC_IDX_MAX        (BLE_CONNECTION_MAX)

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "wpt_common.h"

#if (BLE_WPT_CLIENT)

#include "ke_task.h"
#include "prf_types.h"
#include "prf_utils.h"


/*
 * ENUMERATIONS
 ****************************************************************************************
 */


/// Possible states of the WPTC task
enum wptc_states
{
    /// Idle state
    WPTC_FREE,
    /// Connected state
    WPTC_IDLE,
    /// Discovering Services
    WPTC_DISCOVERING_SVC,
    /// Busy state
    WPTC_BUSY,
    /// Number of defined states.
    WPTC_STATE_MAX
};


/*
 * STRUCTURES
 ****************************************************************************************
 */

/**
 *  Stored Environment content/context information for a connection
 */
struct wptc_cnx_env
{
    uint8_t op_pending;

    /// Counter used to check service uniqueness
    uint8_t nb_svc;

    uint8_t registration;

    /// Wireless Power Transfer Service Characteristics
    struct wptc_wpt_content wpts;
};


/// Wireless Power Transfer environment variable
struct wptc_env_tag
{
    /// profile environment
    prf_env_t prf_env;
    /// Environment variable pointer for each connections
    struct wptc_cnx_env* env[WPTC_IDX_MAX];
    /// State of different task instances
    ke_state_t state[WPTC_IDX_MAX];
};



/*
 * GLOBAL VARIABLE DEFINITIONS
 ****************************************************************************************
 */

/// Pool of Wireless Power Transfer Collector task environments.
extern struct wptc_env_tag **wptc_envs;

/*
 * GLOBAL FUNCTION DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @brief Retrieve WSCP client profile interface
 *
 * @return WSCP client profile interface
 ****************************************************************************************
 */
const struct prf_task_cbs* wptc_prf_itf_get(void);

/**
 ****************************************************************************************
 * @brief Send Wireless Power Transfer ATT DB discovery results to WPTC host.
 ****************************************************************************************
 */
void wptc_enable_complete_send(struct wptc_env_tag *wptc_env, uint8_t conidx, uint8_t status);

/**
 ****************************************************************************************
 * @brief Send a WPTC_CMP_EVT message to the task which enabled the profile
 ****************************************************************************************
 */
void wptc_send_cmp_evt(struct wptc_env_tag *wptc_env, uint8_t conidx, uint8_t operation, uint8_t status);



/*
 * TASK DESCRIPTOR DECLARATIONS
 ****************************************************************************************
 */

extern const struct ke_state_handler wptc_default_handler;

#endif //(BLE_WPT_CLIENT)

/// @} WPTC

#endif //(_WPTC_H_)
