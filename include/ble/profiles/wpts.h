/**
 ****************************************************************************************
 *
 * @file wpts.h
 *
 * @brief Header file - Wireless Power Transfer Profile Service.
 *
 * Copyright (C) RivieraWaves 2009-2015
 *
 * $ Rev $
 *
 ****************************************************************************************
 */

#ifndef _WPTS_H_
#define _WPTS_H_

/**
 ****************************************************************************************
 * @addtogroup WPTS Wireless Power Transfer Profile Service
 * @ingroup WPTS
 * @brief Wireless Power Transfer Profile Service
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "rwip_config.h"

#if (BLE_WPT_SERVER)
#include "wpt_common.h"
#include "wpts_task.h"

#include "prf_types.h"
#include "prf.h"

/*
 * DEFINES
 ****************************************************************************************
 */

/// Maximum number of Wireless Power Transfer Profile Service role task instances
#define WPTS_IDX_MAX        (1)

/// Maximum sizes of the Characteristic Values
///
/// Max Value of the PRU Control Characteristic Value
#define WPTS_IDX_PRU_CONTROL_MAX (5)
/// Max Value of the PRU Static Characteristic Value
#define WPTS_IDX_PTU_STATIC_MAX  (17)
/// Max Value of the PRU ALERT Characteristic Value
#define WPTS_IDX_PRU_ALERT_MAX   (7)
/// Max Value of the PRU STATIC Characteristic Value
#define WPTS_IDX_PRU_STATIC_MAX  (20)
/// Max Value of the PRU DYNAMIC Characteristic Value
#define WPTS_IDX_PRU_DYNAMIC_MAX (20)


/*
 * ENUMERATIONS
 ****************************************************************************************
 */
 /// Possible states of the WPTS task
enum
{
    /// not connected state
    WPTS_FREE,
    /// idle state
    WPTS_IDLE,
    /// read request
    WPTS_OP_READ,
    /// notify 
    WPTS_OP_NOTIFY,
    /// indicate 
    WPTS_OP_INDICATE,
    /// Number of defined states.
    WPTS_STATE_MAX
};


/// Wireless Power Transfer Service - Attribute List
enum wpts_att_list
{
    /// Wireless Power Transfer Service
    WPTS_IDX_SVC = 0,
    /// WPT CHARGING PRU CONTROL Characteristic Declaration
    WPTS_IDX_PRU_CONTROL_CHAR,
    /// WPT CHARGING PRU CONTROL Characteristic Value
    WPTS_IDX_PRU_CONTROL_VAL,
    /// WPT CHARGING PTU STATIC Characteristic Declaration
    WPTS_IDX_PTU_STATIC_CHAR,
    /// WPT CHARGING PTU STATIC Characteristic Value
    WPTS_IDX_PTU_STATIC_VAL,
    /// WPT CHARGING PRU ALERT Characteristic Declaration
    WPTS_IDX_PRU_ALERT_CHAR,
    /// WPT CHARGING PRU ALERT Characteristic Value
    WPTS_IDX_PRU_ALERT_VAL,
    /// WPT ALERT CHARACTERISTIC Client Configuration Characteristic (CCC) Descriptor
    WPTS_IDX_PRU_ALERT_CCC,
    /// WPT CHARGING PRU STATIC Characteristic Declaration
    WPTS_IDX_PRU_STATIC_CHAR,
    /// WPT CHARGING PRU STATIC Characteristic Value
    WPTS_IDX_PRU_STATIC_VAL,
    /// WPT_ CHARGING PRU DYNAM Characteristic Declaration
    WPTS_IDX_PRU_DYNAMIC_CHAR,
    /// WPT_ CHARGING PRU DYNAM Characteristic Value
    WPTS_IDX_PRU_DYNAMIC_VAL,
    /// Number of attributes
    WPTS_IDX_NB,
};


/*
 * STRUCTURES
 ****************************************************************************************
 */


/// Wireless Power Transfer Profile Service environment variable
struct wpts_env_tag
{
    /// profile environment
    prf_env_t prf_env;
    /// Wireless Power Transfer Service Start Handle
    uint16_t shdl;
    /// Current connection Id.
    uint8_t conidx;
    /// CCC for Alert 
    uint8_t prfl_ind_cfg;
    /// State of different task instances
    ke_state_t state[WPTS_IDX_MAX];

};

/*
 * FUNCTION DECLARATIONS
 ****************************************************************************************
 */
 

/**
 ****************************************************************************************
 * @brief Retrieve WPTP service profile interface
 *
 * @return WPTP service profile interface
 ****************************************************************************************
 */
const struct prf_task_cbs* wpts_prf_itf_get(void);


/*
 * TASK DESCRIPTOR DECLARATIONS
 ****************************************************************************************
 */

extern const struct ke_state_handler wpts_default_handler;

#endif //(BLE_WPT_SERVER)

/// @} WPTS

#endif //(_WPTS_H_)
