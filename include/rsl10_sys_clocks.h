/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_clocks.h
 * - Clock support functions and macros
 * ----------------------------------------------------------------------------
 * $Revision: 1.22 $
 * $Date: 2017/08/11 21:39:34 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_CLOCKS_H
#define RSL10_SYS_CLOCKS_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Error codes returned by the library
 * ------------------------------------------------------------------------- */
/* Clock calibration errors */
#define ERRNO_CLOCK_LOAD_MARKER          0x20
#define ERRNO_RCOSC_32k_LOAD_ERROR       (0x0001 | ERRNO_CLOCK_LOAD_MARKER)
#define ERRNO_RC_LOAD_ERROR              (0x0002 | ERRNO_CLOCK_LOAD_MARKER)

/* ----------------------------------------------------------------------------
 * Function prototypes
 * ------------------------------------------------------------------------- */
unsigned int Sys_Clocks_Osc32kCalibratedConfig(uint16_t target);
unsigned int Sys_Clocks_OscRCCalibratedConfig(uint16_t target);

/* ----------------------------------------------------------------------------
 * Clock support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function     : void Sys_Clocks_SystemClkConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure System Clock
 * Inputs        : cfg      - Configuration of the system clock source and
 *                            prescale value; use
 *                            SYSCLK_CLKSRC_[RCCLK | STANDBYCLK | RFCLK |
 *                            EXTCLK | JTCK],
 *                            EXTCLK_PRESCALE_*, and
 *                            JTCK_PRESCALE_*
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Clocks_SystemClkConfig(uint32_t cfg)
{
    /* Configure flash delay ctrl initially to the max delay configuration
     * so following flash reads will return meaningful data. */
    FLASH->DELAY_CTRL = (FLASH->DELAY_CTRL & ~FLASH_DELAY_CTRL_SYSCLK_FREQ_Mask) |
                         FLASH_DELAY_FOR_SYSCLK_48MHZ;

    CLK->SYS_CFG = cfg;
    SystemCoreClockUpdate();
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Clocks_SystemClkPrescale0(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure prescale register number 0
 * Inputs        : cfg      - Configuration of the prescale value for the
 *                            slow, user and baseband peripheral clocks; use
 *                            SLOWCLK_PRESCALE_*,
 *                            BBCLK_PRESCALE_*, and
 *                            USRCLK_PRESCALE_*
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Clocks_SystemClkPrescale0(uint32_t cfg)
{
    CLK->DIV_CFG0 = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Clocks_SystemClkPrescale1(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure prescale register number 1
 * Inputs        : cfg      - Configuration of the prescale value for the
 *                            PWM0, PWM1, UART and AUDIO input peripheral
 *                            clocks; use
 *                            PWM0CLK_PRESCALE_*,
 *                            PWM1CLK_PRESCALE_*,
 *                            UARTCLK_PRESCALE_*, and
 *                            AUDIOCLK_PRESCALE_*
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Clocks_SystemClkPrescale1(uint32_t cfg)
{
    CLK->DIV_CFG1 = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Clocks_SystemClkPrescale2(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure prescale register number 2
 * Inputs        : cfg      - Configuration of the prescale value for the
 *                            charge pump and DC-DC converter clocks; use
 *                            CPCLK_PRESCALE_*, and
 *                            DCCLK_PRESCALE_*
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Clocks_SystemClkPrescale2(uint32_t cfg)
{
    CLK->DIV_CFG2 = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Clocks_Set_ClkDetConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the external clock detector
 * Inputs        : cfg      - The external clock detector configuration; use
 *                            CLK_DET_[DISABLE | ENABLE],
 *                            CLK_DET_SLOWCLK_DIV*,
 *                            CLK_DET_INT_[DISABLE | ACTIVATED |
 *                                         DEACTIVATED | ACTIVITY_CHANGE], and
 *                            CLK_DET_SEL_[EXT | SW]CLK
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Clocks_Set_ClkDetConfig(uint32_t cfg)
{
    CLK->DET_CFG = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Clocks_ClkDetEnable(void)
 * ----------------------------------------------------------------------------
 * Description   : Enable/Disable the external clock detector
 * Inputs        : cfg   - Configuration of  the clock detector enable value;
 *                         use CLK_DET_[DISABLE | ENABLE]_BITBAND
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Clocks_ClkDetEnable(uint32_t cfg)
{
    CLK_DET_CFG->CLK_DET_ENABLE_ALIAS = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Clocks_Osc(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the RC oscillator
 * Inputs        : cfg   - Configuration for 3 MHz/12 MHz RC oscillator; use
 *                         RC_START_OSC_[3 | 12]MHZ
 *                         RC_START_OSC_[M48 | M46P5 | NOM | P46P5]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Clocks_Osc(uint32_t cfg)
{
    ACS->RCOSC_CTRL = (ACS->RCOSC_CTRL &
                       ((1 << ACS_RCOSC_CTRL_RC_OSC_EN_Pos) |
                        (1 << ACS_RCOSC_CTRL_FTRIM_FLAG_Pos) |
                        (1 << ACS_RCOSC_CTRL_FTRIM_32K_ADJ_Pos) |
                        ACS_RCOSC_CTRL_FTRIM_32K_Mask)) |
                      (cfg &
                       ~((1 << ACS_RCOSC_CTRL_RC_OSC_EN_Pos) |
                         (1 << ACS_RCOSC_CTRL_FTRIM_FLAG_Pos) |
                         (1 << ACS_RCOSC_CTRL_FTRIM_32K_ADJ_Pos) |
                         ACS_RCOSC_CTRL_FTRIM_32K_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Clocks_Osc32kHz(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the 32 kHz RC oscillator
 * Inputs        : cfg   - Configuration for 32 kHz RC oscillator; use
 *                         RC_OSC_[DISABLE | ENABLE]
 *                         RC_OSC_RANGE_[NOM | M25]
 *                         RC_OSC_[M48 | M46P5 | NOM | P46P5]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Clocks_Osc32kHz(uint32_t cfg)
{
    ACS->RCOSC_CTRL = (ACS->RCOSC_CTRL &
                       ~((1 << ACS_RCOSC_CTRL_RC_OSC_EN_Pos) |
                         (1 << ACS_RCOSC_CTRL_FTRIM_32K_ADJ_Pos) |
                         ACS_RCOSC_CTRL_FTRIM_32K_Mask)) |
                      (cfg &
                       ((1 << ACS_RCOSC_CTRL_RC_OSC_EN_Pos) |
                        (1 << ACS_RCOSC_CTRL_FTRIM_32K_ADJ_Pos) |
                        ACS_RCOSC_CTRL_FTRIM_32K_Mask));
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_CLOCKS_H */
