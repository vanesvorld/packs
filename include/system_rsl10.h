/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * system_rsl10.h
 * - CMSIS Cortex-M3 Device Peripheral Access Layer Header File
 *
 *   Provides the required system_<Device>.h implementation for CMSIS
 *   compatibility
 * ------------------------------------------------------------------------- */

#ifndef SYSTEM_RSL10_H
#define SYSTEM_RSL10_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/* ----------------------------------------------------------------------------
 * System Initialization
 * ------------------------------------------------------------------------- */
extern void SystemInit(void);

/* ----------------------------------------------------------------------------
 * System Core Clock and Clock Configuration
 * ------------------------------------------------------------------------- */
#define DEFAULT_FREQ                    5000000
#define STANDBYCLK_DEFAULT_FREQ         32768
#define RFCLK_BASE_FREQ                 48000000
#define EXTCLK_MAX_FREQ                 48000000
#define JTCK_MAX_FREQ                   48000000
#define RCOSC_MAX_FREQ                  16000000

extern uint32_t SystemCoreClock;
extern void SystemCoreClockUpdate(void);
extern unsigned int System_GetTargetForTrim(uint32_t *calib_info_ptr,
                                            uint8_t trim,
                                            uint16_t *target);

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_RSL10_H */
