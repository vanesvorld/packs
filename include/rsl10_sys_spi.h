/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_spi.h
 * - Serial Peripheral Interface (SPI) support
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2017/07/10 14:52:29 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_SPI_H
#define RSL10_SYS_SPI_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * SPI support inline functions
 * ------------------------------------------------------------------------- */

#define SPI_BASE_DIFF    (SPI1_BASE - SPI0_BASE)
/* ----------------------------------------------------------------------------
 * Function      : void Sys_SPI_Config(uint32_t num, uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the specified SPI interface's operation and
 *                 controller information
 * Inputs        : - num    - SPI interface to configure; use 0 or 1
 *                 - cfg    - Interface operation configuration; use
 *                            SPI*_OVERRUN_INT_[DISABLE | ENABLE],
 *                            SPI*_UNDERRUN_INT_[DISABLE | ENABLE],
 *                            SPI*_CONTROLLER_[CM3 | DMA],
 *                            SPI*_SELECT_[MASTER | SLAVE],
 *                            SPI*_CLK_POLARITY_[NORMAL | INVERSE],
 *                            SPI*_MODE_SELECT_[MANUAL | AUTO],
 *                            SPI*_[DISABLE | ENABLE] and SPI*_PRESCALE_*
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_SPI_Config(uint32_t num, uint32_t cfg)
{
    ((SPI0_Type *)(SPI0_BASE + num * SPI_BASE_DIFF))->CTRL0 =
            (cfg & ((1U << SPI0_CTRL0_SPI0_OVERRUN_INT_ENABLE_Pos)   |
                    (1U << SPI0_CTRL0_SPI0_UNDERRUN_INT_ENABLE_Pos)  |
                    (1U << SPI0_CTRL0_SPI0_CONTROLLER_Pos)           |
                    (1U << SPI0_CTRL0_SPI0_SLAVE_Pos)                |
                    (1U << SPI0_CTRL0_SPI0_CLK_POLARITY_Pos)         |
                    (1U << SPI0_CTRL0_SPI0_MODE_SELECT_Pos)          |
                    (1U << SPI0_CTRL0_SPI0_ENABLE_Pos)               |
                     SPI0_CTRL0_SPI0_PRESCALE_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_SPI_TransferConfig(uint32_t num, uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the SPI transfer information for the specified
 *                 SPI interface
 * Inputs        : - num    - SPI interface to configure; use 0 or 1
 *                 - cfg    - Interface transfer configuration; use
 *                            SPI*_[IDLE | START],
 *                            SPI*_[WRITE | READ | RW]_DATA,
 *                            SPI*_CS_*, and SPI*_WORD_SIZE_* or a constant
 *                            shifted to SPI*_CTRL1_SPI0_WORD_SIZE_Pos
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_SPI_TransferConfig(uint32_t num, uint32_t cfg)
{
    ((SPI0_Type *)(SPI0_BASE + num * SPI_BASE_DIFF))->CTRL1 =
            (cfg & ((1U << SPI0_CTRL1_SPI0_START_BUSY_Pos)      |
                       SPI0_CTRL1_SPI0_RW_CMD_Mask              |
                       (1U << SPI0_CTRL1_SPI0_CS_Pos)           |
                       SPI0_CTRL1_SPI0_WORD_SIZE_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_SPI_MasterInit(uint32_t num)
 * ----------------------------------------------------------------------------
 * Description   : Initialize an SPI operation on a specified SPI interface
 *                 when running this interface in master mode
 * Inputs        : - num      - SPI interface to configure; use 0 or 1
 * Outputs       : None
 * Assumptions   : - The SPI interface is currently idle
 *                 - The SPI interface is configured for master mode operation
 *                 - If writing over the SPI interface, the data to be written
 *                   has been queued
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_SPI_MasterInit(uint32_t num)
{
    ((SPI0_CTRL1_Type *)
            (SPI0_CTRL1_BASE + num * \
            (SPI1_CTRL1_BASE - SPI0_CTRL1_BASE)))->SPI0_START_BUSY_ALIAS =
            SPI0_START_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_SPI_Read(uint32_t num, uint8_t bits)
 * ----------------------------------------------------------------------------
 * Description   : Configure the interface to read the specified number of bits
 *                 over the specified SPI interface
 * Inputs        : - num    - SPI interface to read from; use 0 or 1
 *                 - bits   - Word size used by the SPI interface; use
 *                            SPI*_WORD_SIZE_*
 * Outputs       : None
 * Assumptions   : - The SPI interface is currently idle
 *                 - The SPI interface is configured for master mode operation
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_SPI_Read(uint32_t num, uint8_t bits)
{
    ((SPI0_Type *)(SPI0_BASE + num * SPI_BASE_DIFF))->CTRL1 =
            (SPI0_START | SPI0_READ_DATA | SPI0_CS_0 | ((bits) - 1 ));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_SPI_Write(uint32_t num, uint8_t bits)
 * ----------------------------------------------------------------------------
 * Description   : Configure the interface to write the specified number of
 *                 bits over the specified SPI interface
 * Inputs        : - num    - SPI interface to read from; use 0 or 1
 *                 - bits   - Number of bits to transmit (between 1 and 32)
 * Outputs       : None
 * Assumptions   : - The SPI interface is currently idle
 *                 - The SPI interface is configured for master mode operation
 *                 - The data to be written has been queued
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_SPI_Write(uint32_t num, uint8_t bits)
{
    ((SPI0_Type *)(SPI0_BASE + num * SPI_BASE_DIFF))->CTRL1 =
            (SPI0_START | SPI0_WRITE_DATA | SPI0_CS_0 | ((bits) - 1 ));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_SPI_ReadWrite(uint32_t num, uint8_t bits)
 * ----------------------------------------------------------------------------
 * Description   : Configure the interface to read and write the specified
 *                 number of bits over the specified SPI interface (full-duplex)
 * Inputs        : - num    - SPI interface to read from; use 0 or 1
 *                 - bits   - Number of bits to transmit and receive (between
 *                            1 and 32)
 * Outputs       : None
 * Assumptions   : - The SPI interface is currently idle
 *                 - The SPI interface is configured for master mode operation
 *                 - The data to be written has been queued
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_SPI_ReadWrite(uint32_t num, uint8_t bits)
{
    ((SPI0_Type *)(SPI0_BASE + num * SPI_BASE_DIFF))->CTRL1 =
            (SPI0_START | SPI0_RW_DATA | SPI0_CS_0 | ((bits) - 1 ));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_SPI_DIOConfig(uint32_t num, uint32_t slave,
 *                                         uint32_t cfg,
 *                                         uint32_t clk, uint32_t cs,
 *                                         uint32_t seri, uint32_t sero)
 * ----------------------------------------------------------------------------
 * Description   : Configure four DIOs for the specified SPI interface
 * Inputs        : - num    - SPI interface to configure; use 0 or 1
 *                 - slave  - SPI master/slave configuration; use
 *                            SPI*_SELECT_[MASTER | SLAVE]
 *                 - cfg    - DIO pin configuration for the SPI pads
 *                 - clk    - DIO to use as the SPI clock pad
 *                 - cs     - DIO to use as the SPI chip select pad
 *                 - seri   - DIO to use as the SPI serial input pad
 *                 - sero   - DIO to use as the SPI serial output pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_SPI_DIOConfig(uint32_t num, uint32_t slave,
                                        uint32_t cfg, uint32_t clk, uint32_t cs,
                                        uint32_t seri, uint32_t sero)
{
    const int offset = num * (DIO_MODE_SPI1_SERO - DIO_MODE_SPI0_SERO);

    /* Check if the SPI interface is configured as a master or a slave mode
     * interface. Note: SPI1_SELECT_MASTER = SPI0_SELECT_MASTER */
    if ( slave == SPI1_SELECT_MASTER )
    {
        Sys_DIO_Config(clk, ((cfg) | (DIO_MODE_SPI0_CLK + offset)));
        Sys_DIO_Config(cs,  ((cfg) | (DIO_MODE_SPI0_CS + offset )));
        DIO->SPI_SRC[num] = ((seri << DIO_SPI_SRC_SERI_Pos) &
                        DIO_SPI_SRC_SERI_Mask);
    }
    else
    {
        Sys_DIO_Config(clk, ((cfg) | DIO_MODE_INPUT));
        Sys_DIO_Config(cs,  ((cfg) | DIO_MODE_INPUT));
        DIO->SPI_SRC[num] = (((seri << DIO_SPI_SRC_SERI_Pos) &
                                       DIO_SPI_SRC_SERI_Mask)    |
                             ((cs << DIO_SPI_SRC_CS_Pos) &
                                     DIO_SPI_SRC_CS_Mask)        |
                             ((clk << DIO_SPI_SRC_CLK_Pos) &
                                      DIO_SPI_SRC_CLK_Mask));
    }
    Sys_DIO_Config(seri, ((cfg) | DIO_MODE_INPUT));
    Sys_DIO_Config(sero, ((cfg) | (DIO_MODE_SPI0_SERO + offset)));
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_SPI_H */
