/* ----------------------------------------------------------------------------
 * Copyright (c) 2014-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_hw.h
 * - ARM Cortex-M3 accessible hardware registers and bit-field definitions
 * ---------------------------------------------------------------------------- 
 * $Revision: 1.4 $
 * $Date: 2017/09/19 03:19:44 $
 * ------------------------------------------------------------------------- */
#ifndef RSL10_CID
#error RSL10_CID must be defined
#endif

#if RSL10_CID == 101
#include <rsl10_hw_cid101.h>
#else
#error Invalid RSL10 chip identifier
#endif

