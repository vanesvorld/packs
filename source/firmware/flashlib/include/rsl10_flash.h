/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_flash.h
 * - Flash write support header
 * ----------------------------------------------------------------------------
 * $Revision: 1.16 $
 * $Date: 2017/06/29 15:38:58 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_FLASH_H
#define RSL10_FLASH_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * External include files
 * ------------------------------------------------------------------------- */
#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Firmware Flash Library Version Code
 * ------------------------------------------------------------------------- */
#define FLASH_FW_VER_MAJOR              0x02
#define FLASH_FW_VER_MINOR              0x01
#define FLASH_FW_VER_REVISION           0x03

#define FLASH_FW_VER                    ((FLASH_FW_VER_MAJOR << 12) | \
                                         (FLASH_FW_VER_MINOR << 8)  | \
                                          FLASH_FW_VER_REVISION)

extern const short RSL10_FlashLib_Version;

/* ----------------------------------------------------------------------------
 * Definitions
 * ------------------------------------------------------------------------- */

/* Flash error codes */
typedef enum
{
    FLASH_ERR_NONE                      = 0x0,
    FLASH_ERR_GENERAL_FAILURE           = 0x1,
    FLASH_ERR_WRITE_NOT_ENABLED         = 0x2,
    FLASH_ERR_BAD_ADDRESS               = 0x3,
    FLASH_ERR_ERASE_FAILED              = 0x4,
    FLASH_ERR_BAD_LENGTH                = 0x5,
    FLASH_ERR_INACCESSIBLE              = 0x6,
    FLASH_ERR_COPIER_BUSY               = 0x7,
    FLASH_ERR_PROG_FAILED               = 0x8
} FlashStatus;

/* Define the size of a sector of flash memory */
#define FLASH_SECTOR_SIZE               0x800
#define FLASH_SECTOR_ADDRESS            0xFFFFF800

/* ----------------------------------------------------------------------------
 * Function declarations
 * ------------------------------------------------------------------------- */
unsigned int Flash_WriteWordPair(unsigned int addr,
                                 unsigned int data0,
                                 unsigned int data1);
unsigned int Flash_WriteBuffer(unsigned int start_addr,
                               unsigned int length,
                               unsigned int* data);
unsigned int Flash_EraseSector(unsigned int addr);
unsigned int Flash_EraseAll(void);

unsigned int Flash_WriteCommand(uint32_t command);
unsigned int Flash_WriteInterfaceControl(uint32_t ctrl);

/* ----------------------------------------------------------------------------
 * Internal function declarations
 * ------------------------------------------------------------------------- */
unsigned int Flash_Setup(unsigned int addr,
                         unsigned int cmd);

unsigned int Flash_RunCommand(unsigned int addr,
                              unsigned int cmd);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_FLASH_H */
