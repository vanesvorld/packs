/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_flash_rom.h
 * - Flash write support header (ROM implementation)
 * ----------------------------------------------------------------------------
 * $Revision: 1.16 $
 * $Date: 2017/11/09 20:27:35 $
 * ------------------------------------------------------------------------- */

/* Use the same guard as the regular flash library to ensure they aren't both
 * included in the same application.
 */
#ifndef RSL10_FLASH_H
#define RSL10_FLASH_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * External include files
 * ------------------------------------------------------------------------- */
#include <rsl10_romvect.h>

/* ----------------------------------------------------------------------------
 * Definitions
 * ------------------------------------------------------------------------- */
/* Define the size of a sector of flash memory */
#define FLASH_SECTOR_SIZE               0x800
#define FLASH_SECTOR_ADDRESS            0xFFFFF800

/* Flash error codes */
typedef enum
{
    FLASH_ERR_NONE              = 0x0,
    FLASH_ERR_GENERAL_FAILURE   = 0x1,
    FLASH_ERR_WRITE_NOT_ENABLED = 0x2,
    FLASH_ERR_BAD_ADDRESS       = 0x3,
    FLASH_ERR_ERASE_FAILED      = 0x4,
    FLASH_ERR_BAD_LENGTH        = 0x5,
    FLASH_ERR_INACCESSIBLE      = 0x6,
    FLASH_ERR_COPIER_BUSY       = 0x7,
    FLASH_ERR_PROG_FAILED       = 0x8
} FlashStatus;

/* ----------------------------------------------------------------------------
 * Function declarations
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Flash_WriteWordPair(unsigned int addr,
 *                                                  unsigned int data0,
 *                                                  unsigned int data1)
 * ----------------------------------------------------------------------------
 * Description   : Write a word pair of flash at the specified address
 * Inputs        : - addr           - Address to write in the flash
 *                 - data0          - First data word to write to the specified
 *                                    address in flash
 *                 - data1          - Second data word to write to the
 *                                    specified address + 4 in flash
 * Outputs       : return value     - Status code indicating whether the
 *                                    requested flash operation succeeded
 * Assumptions   : - The calling application has unlocked the flash for write
 *                 - The area of flash to be written has been previously erased
 *                   (if necessary) and is not currently write protected
 *                 - If the flash is in sequential programming mode, it is
 *                   safe to exit this mode
 *                 - None of the RECALL, VREAD0_MODE and VREAD1_MODE bits are
 *                   set in FLASH_IF_CTRL
 * ------------------------------------------------------------------------- */
__STATIC_INLINE unsigned int Flash_WriteWordPair(unsigned int addr,
                                                 unsigned int data0,
                                                 unsigned int data1)
{
    return (*((unsigned int (**)(unsigned int, unsigned int, unsigned int))
            ROMVECT_FLASH_WRITE_WORD_PAIR))(addr, data0, data1);
}

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Flash_WriteBuffer(unsigned int start_addr,
 *                                                unsigned int length,
 *                                                unsigned int* data)
 * ----------------------------------------------------------------------------
 * Description   : Write a buffer of memory of the specified length, starting
 *                 at the specified address, to flash
 * Inputs        : - start_addr     - Start address for the write to flash
 *                 - length         - Number of words to write to flash
 *                 - data           - Pointer to the data to write to flash
 * Outputs       : return value     - Status code indicating whether the
 *                                    requested flash operation succeeded
 * Assumptions   : - The calling application has unlocked the flash for write
 *                 - The areas of flash to be written has been previously
 *                   erased (if necessary) and are not currently write
 *                   protected
 *                 - "data" points to a buffer of at least "length" words
 *                 - The address in flash is even word aligned
 *                 - The number of words to write is an even number
 *                 - If the flash is already in sequential programming mode,
 *                   it is safe to exit this mode to perform the buffered
 *                   write.
 *                 - None of the RECALL, VREAD0_MODE and VREAD1_MODE bits are
 *                   set in FLASH_IF_CTRL
 * ------------------------------------------------------------------------- */
__STATIC_INLINE unsigned int Flash_WriteBuffer(unsigned int start_addr,
                                               unsigned int length,
                                               unsigned int* data)
{
    return (*((unsigned int (**)(unsigned int, unsigned int, unsigned int*))
            ROMVECT_FLASH_WRITE_BUFFER))(start_addr, length, data);
}

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Flash_EraseSector(unsigned int addr)
 * ----------------------------------------------------------------------------
 * Description   : Erase the specified flash sector. This sector could be in
 *                 the main flash, one of the NVR sectors, or one of the
 *                 redundancy sectors. Verify the sector was erased,
 *                 progressively trying the different sector erase pulses
 *                 until one successfully erases the sector.
 * Inputs        : addr         - Address of data in the sector to be erased
 * Outputs       : return value - Status code indicating whether the
 *                                requested flash operation succeeded
 * Assumptions   : - The calling application has unlocked the flash for erase
 *                 - If the flash is in sequential programming mode, it is
 *                   safe to exit this mode
 *                 - None of the RECALL, VREAD0_MODE and VREAD1_MODE bits are
 *                   set in FLASH_IF_CTRL
 * ------------------------------------------------------------------------- */
__STATIC_INLINE unsigned int Flash_EraseSector(unsigned int addr)
{
    return (*((unsigned int (**)(unsigned int))
            ROMVECT_FLASH_ERASE_SECTOR))(addr);
}

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Flash_EraseAll(void)
 * ----------------------------------------------------------------------------
 * Description   : Erase all of the sectors in the main block of the flash
 * Inputs        : None
 * Outputs       : return value     - Status code indicating whether the
 *                                    requested flash operation succeeded
 * Assumptions   : - The calling application has unlocked all of the main
 *                   flash instance, and any NVR or redundancy sectors that
 *                   should be erased
 *                 - If the flash is in sequential programming mode, it is
 *                   safe to exit this mode
 * ------------------------------------------------------------------------- */
__STATIC_INLINE unsigned int Flash_EraseAll(void)
{
    return (*((unsigned int (**)(void)) ROMVECT_FLASH_ERASE_ALL))();
}

/* ----------------------------------------------------------------------------
 * Function      : void Flash_WriteCommand(uint32_t command)
 * ----------------------------------------------------------------------------
 * Description   : Safely issue a flash command; blocks waiting for the flash
 *                 to be idle before running the command and again before
 *                 returning.
 * Inputs        : command  - Command to be written to FLASH_CMD_CTRL; use
 *                            CMD_*
 * Outputs       : return value - Status code indicating whether the
 *                                flash interface can be written; returns
 *                                FLASH_ERR_INACCESSIBLE if the flash is 
 *                                isolated or not powered, otherwise returns
 *                                no error.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE unsigned int Flash_WriteCommand(uint32_t command)
{
    return (*((unsigned int (**)(unsigned int)) ROMVECT_FLASH_WRITE_COMMAND))(command);
}

/* ----------------------------------------------------------------------------
 * Function      : void Flash_WriteInterfaceControl(uint32_t ctrl)
 * ----------------------------------------------------------------------------
 * Description   : Safely write the interface control register; blocks waiting
 *                 for the flash to be idle before writing the interface
 *                 control register, and again before returning.
 * Inputs        : ctrl - Data to write to the FLASH_IF_CTRL register
 * Outputs       : return value - Status code indicating whether the
 *                                flash interface can be written; returns
 *                                FLASH_ERR_INACCESSIBLE if the flash is 
 *                                isolated or not powered and LP_MODE, RECALL,
 *                                VREAD0_MODE or VREAD1_MODE are being changed,
 *                                otherwise returns no error.
 * Assumptions   : - If the flash is in sequential programming mode, it is safe
 *                   to exit this mode
 *                 - No more than two of the LP_MODE, RECALL, VREAD0_MODE and
 *                   VREAD1_MODE bits are being updated in FLASH_IF_CTRL
 * ------------------------------------------------------------------------- */
__STATIC_INLINE unsigned int Flash_WriteInterfaceControl(uint32_t ctrl)
{
    return (*((unsigned int (**)(unsigned int)) ROMVECT_FLASH_WRITE_INTERFACE_CONTROL))(ctrl);
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_FLASH_H */
