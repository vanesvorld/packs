/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC
 * (d/b/a ON Semiconductor). All Rights Reserved.
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor. The
 * terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_flash.c
 * - Flash write support functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.23 $
 * $Date: 2017/11/03 15:49:50 $
 * ------------------------------------------------------------------------- */

#include <rsl10_flash.h>

/* ----------------------------------------------------------------------------
 * Firmware Flash Library Version
 * ------------------------------------------------------------------------- */
const short RSL10_FlashLib_Version = FLASH_FW_VER;

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Flash_WriteWordPair(unsigned int addr,
 *                                                  unsigned int data0,
 *                                                  unsigned int data1)
 * ----------------------------------------------------------------------------
 * Description   : Write a word pair of flash at the specified address
 * Inputs        : - addr           - Address to write in the flash
 *                 - data0          - First data word to write to the specified
 *                                    address in flash
 *                 - data1          - Second data word to write to the
 *                                    specified (address + 4) in flash
 * Outputs       : return value     - Status code indicating whether the
 *                                    requested flash operation succeeded
 * Assumptions   : - The calling application has unlocked the flash for write
 *                 - The area of flash to be written has been previously erased
 *                   (if necessary) and is not currently write protected
 *                 - If the flash is in sequential programming mode, it is
 *                   safe to exit this mode
 *                 - None of the RECALL, VREAD0_MODE and VREAD1_MODE bits are
 *                   set in FLASH_IF_CTRL
 * ------------------------------------------------------------------------- */
unsigned int Flash_WriteWordPair(unsigned int addr, unsigned int data0,
                                 unsigned int data1)
{
    unsigned int result;

    /* Check that address even word aligned */
    if ((addr % 8) != 0)
    {
        return FLASH_ERR_BAD_ADDRESS;
    }

    /* Check that flash is powered up and not isolated */
    if (FLASH_IF_STATUS->ISOLATE_STATUS_ALIAS != FLASH_ACCESSIBLE_BITBAND)
    {
        return FLASH_ERR_INACCESSIBLE;
    }

    /* Wait for the flash to be idle and end any outstanding modes if needed */
    while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND)
    {
        FLASH->CMD_CTRL = CMD_END;
    }

    /* Setting retry 4 as required for write operations */
    FLASH->IF_CTRL = ((FLASH->IF_CTRL & ~(FLASH_IF_CTRL_RETRY_Mask)) | FLASH_RETRY_4);

    /* Ensure ECC is enabled */
    FLASH_ECC_CTRL->CMD_ECC_CTRL_ALIAS = FLASH_CMD_ECC_ENABLE_BITBAND;

    /* Write one word pair */
    FLASH->DATA[0] = data0;
    FLASH->DATA[1] = data1;
    result = Flash_RunCommand(addr, CMD_PROGRAM_NOSEQ);
    if (result != FLASH_ERR_NONE)
    {
        return result;
    }

    /* Verify if programming was successful */
    FLASH->ADDR = addr;
    FLASH->CMD_CTRL = CMD_READ;
    while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);
    if ((FLASH->DATA[0] != data0) ||
        (FLASH->DATA[1] != data1))
    {
        return FLASH_ERR_PROG_FAILED;
    }

    return FLASH_ERR_NONE;
}

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Flash_WriteBuffer(unsigned int start_addr,
 *                                                unsigned int length,
 *                                                unsigned int* data)
 * ----------------------------------------------------------------------------
 * Description   : Write a buffer of memory of the specified length, starting
 *                 at the specified address, to flash
 * Inputs        : - start_addr     - Start address for the write to flash
 *                 - length         - Number of words to write to flash
 *                 - data           - Pointer to the data to write to flash
 * Outputs       : return value     - Status code indicating whether the
 *                                    requested flash operation succeeded
 * Assumptions   : - The calling application has unlocked the flash for write
 *                 - The areas of flash to be written have been previously
 *                   erased (if necessary) and are not currently write
 *                   protected
 *                 - "data" points to a buffer of at least "length" words
 *                 - The address in flash is even word aligned
 *                 - The number of words to write is an even number
 *                 - If the flash is already in sequential programming mode,
 *                   it is safe to exit this mode to perform the buffered
 *                   write.
 *                 - None of the RECALL, VREAD0_MODE and VREAD1_MODE bits are
 *                   set in FLASH_IF_CTRL
 * ------------------------------------------------------------------------- */
unsigned int Flash_WriteBuffer(unsigned int start_addr, unsigned int length,
                               unsigned int* data)
{
    uint32_t temp;
    uint32_t intStatus;
    unsigned int i;
    unsigned int result;
    unsigned int start_new_sequence;
    unsigned int length_copy;
    unsigned int* data_copy;

    if ((start_addr % 8) != 0)
    {
        return FLASH_ERR_BAD_ADDRESS;
    }

    if ((length % 2) != 0)
    {
        return FLASH_ERR_BAD_LENGTH;
    }

    /* Check that flash is powered up and not isolated */
    if (FLASH_IF_STATUS->ISOLATE_STATUS_ALIAS != FLASH_ACCESSIBLE_BITBAND)
    {
        return FLASH_ERR_INACCESSIBLE;
    }

    /* Wait for the flash to be idle and end any outstanding modes if needed */
    while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND)
    {
        FLASH->CMD_CTRL = CMD_END;
    }

    /* Setting retry 4 as required for write operations */
    FLASH->IF_CTRL = ((FLASH->IF_CTRL & ~(FLASH_IF_CTRL_RETRY_Mask)) |
                      FLASH_RETRY_4);

    /* Set the delays required to be able to erase or write the flash, and
     * check that the start, middle and end addresses are legal addresses in
     * flash */
    result = Flash_Setup(start_addr, CMD_PROGRAM_SEQ);
    if (result != FLASH_ERR_NONE)
    {
        return result;
    }
    result = Flash_Setup(start_addr + (length * 2), CMD_PROGRAM_SEQ);
    if (result != FLASH_ERR_NONE)
    {
        return result;
    }
    result = Flash_Setup(start_addr + ((length - 1) * 4), CMD_PROGRAM_SEQ);
    if (result != FLASH_ERR_NONE)
    {
        return result;
    }

    /* Disable interrupts during the write sequence */
    intStatus = __get_PRIMASK();

    /* Ensure ECC is enabled */
    FLASH_ECC_CTRL->CMD_ECC_CTRL_ALIAS = FLASH_CMD_ECC_ENABLE_BITBAND;

    /* Write the buffer at the specified starting address */
    FLASH->ADDR = start_addr;
    /* Always start new sequential programming sequence for the first write */
    start_new_sequence = 1;
    /* Backup length and data variables */
    length_copy = length;
    data_copy = data;
    while (length > 0)
    {
        /* Prepare the next word pair to be written */
        FLASH->DATA[0] = *data++;
        FLASH->DATA[1] = *data++;
        length = length - 2;

        /* Check if this is the start of the overall write or the start of a
         * row. If it is, start a sequential programming sequence. */
        if (start_new_sequence)
        {
            __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);
            FLASH->CMD_CTRL = CMD_PROGRAM_SEQ;
            start_new_sequence = 0;
        }

        /* Wait until the write is completed and new words can be provided */
        while (FLASH_IF_STATUS->PROG_SEQ_DATA_REQ_ALIAS == 0);

        /* Check if this is the end of the overall write (last word pair) or
         * the end of a row. If it is, terminate the sequential programming
         * sequence and start a one at the next write. */
        temp = FLASH->ADDR & 0xFF;
        if ((length == 0) || (temp == 0))
        {
            FLASH->CMD_CTRL = CMD_END;
            while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);
            if (temp == 0)
            {
                FLASH->ADDR = FLASH->ADDR + 0x100;
            }
            start_new_sequence = 1;

            /* Restore the previous interrupt setting */
            __set_PRIMASK(intStatus);
        }
    }

    /* Verify if programming was successful */
    FLASH->ADDR = start_addr;
    for (i = 0; i < length_copy; i = i+2)
    {
        FLASH->CMD_CTRL = CMD_READ;
        while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);
        if ((FLASH->DATA[0] != *(unsigned int *)data_copy++) ||
            (FLASH->DATA[1] != *(unsigned int *)data_copy++))
        {
            return FLASH_ERR_PROG_FAILED;
        }
    }

    return FLASH_ERR_NONE;
}

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Flash_EraseSector(unsigned int addr)
 * ----------------------------------------------------------------------------
 * Description   : Erase the specified flash sector. This sector could be in
 *                 the main flash, one of the NVR sectors, or one of the
 *                 redundancy sectors. Verify the sector was erased,
 *                 progressively trying the different sector erase pulses
 *                 until one successfully erases the sector.
 * Inputs        : addr         - Address of data in the sector to be erased
 * Outputs       : return value - Status code indicating whether the
 *                                requested flash operation succeeded
 * Assumptions   : - The calling application has unlocked the flash for erase
 *                 - If the flash is in sequential programming mode, it is
 *                   safe to exit this mode
 *                 - None of the RECALL, VREAD0_MODE and VREAD1_MODE bits are
 *                   set in FLASH_IF_CTRL
 * ------------------------------------------------------------------------- */
unsigned int Flash_EraseSector(unsigned int addr)
{
    unsigned int result;
    uint32_t i;
    uint32_t temp_IF_CTRL;
    uint32_t intStatus;

    /* 1.  Erase the sector with RETRY=FLASH_SE_RETRY_1.
     * 2.  Check with VREAD1_MODE=FLASH_VREAD1_ENABLE, the sector is properly
     *     erased.
     *  a. If the sector is erased, you could write it.
     *  b. If the sector is not fully erased, erase the sector with
     *     RETRY=FLASH_SE_RETRY_2.
     * 3.  Check with VREAD1_MODE=FLASH_VREAD1_ENABLE, the sector is properly
     *     erased.
     *  a. If the sector is erased, you could write it.
     *  b. If the sector is not fully erased, erase the sector with
     *     RETRY=FLASH_SE_RETRY_3.
     * 4.  Check with VREAD1_MODE=FLASH_VREAD1_ENABLE, the sector is properly
     *     erased.
     *  a. If the sector is erased, you could write it.
     *  b. If the sector is not fully erased, erase the sector with
     *     RETRY=FLASH_SE_RETRY_4.
     * 5.  Check with VREAD1_MODE=FLASH_VREAD1_DISABLE, the sector is properly
     *     erased.
     *  a. If the sector is erased, you could write it.
     *  b. If the sector is not fully erased, the sector cannot be used
     *     anymore and an error should be raised.
     */

    /* Check that flash copier is idle */
    if (FLASH_COPY_CTRL->BUSY_ALIAS == COPY_BUSY)
    {
        return FLASH_ERR_COPIER_BUSY;
    }

    /* Temporarily save the IF_CTRL settings before setting VREAD1 as required
     * for a verified sector erase. */
    temp_IF_CTRL = FLASH->IF_CTRL;

    /* Save interrupt status */
    intStatus = __get_PRIMASK();

    for (i = FLASH_RETRY_1;
         i <= FLASH_RETRY_4;
         i += (0x1U << FLASH_IF_CTRL_RETRY_Pos) )
    {
        /* Configure RETRY level */
        result = Flash_WriteInterfaceControl((temp_IF_CTRL &
                                             ~FLASH_IF_CTRL_RETRY_Mask) | i);

        /* Erase sector */
        result = Flash_RunCommand(addr, CMD_SECTOR_ERASE);
        if (result != FLASH_ERR_NONE)
        {
            /* Restore the previous FLASH_IF_CTRL settings */
            Flash_WriteInterfaceControl(temp_IF_CTRL);
            return result;
        }

        /* Use the flash copier to check the sector is erased */
        FLASH->COPY_SRC_ADDR_PTR = (uint32_t) (addr & 0xFFFFF800);
        FLASH->COPY_WORD_CNT = 512;
        FLASH->COPY_CFG = (COMPARATOR_MODE | COMP_MODE_CONSTANT | COMP_ADDR_UP
                           | COMP_ADDR_STEP_1 | COPY_TO_32BIT);
        FLASH->DATA[0] = 0xFFFFFFFF;
        FLASH->DATA[1] = 0x0000000F;

        if (i < FLASH_RETRY_4)
        {
            /* Disable interrupts before enabling VREAD1 mode */
            __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

            result = Flash_WriteInterfaceControl(temp_IF_CTRL
                                                 | FLASH_VREAD1_ENABLE);

            if (result != FLASH_ERR_NONE)
            {
                /* Restore the previous FLASH_IF_CTRL settings */
                Flash_WriteInterfaceControl(temp_IF_CTRL);
                __set_PRIMASK(intStatus);
                return result;
            }
        }

        FLASH->COPY_CTRL = COPY_START;
        while (FLASH_COPY_CTRL->BUSY_ALIAS == COPY_BUSY_BITBAND);

        /* Restore the previous FLASH_IF_CTRL settings */
        Flash_WriteInterfaceControl(temp_IF_CTRL);

        /* Restore interrupts */
        __set_PRIMASK(intStatus);

        /* Check result */
        if (FLASH->COPY_SRC_ADDR_PTR == ((addr & FLASH_SECTOR_ADDRESS)
                                         + FLASH_SECTOR_SIZE))
        {
            return FLASH_ERR_NONE;
        }
    }

    return FLASH_ERR_ERASE_FAILED;
}

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Flash_EraseAll(void)
 * ----------------------------------------------------------------------------
 * Description   : Erase all of the sectors in the main block of the flash
 * Inputs        : None
 * Outputs       : return value     - Status code indicating whether the
 *                                    requested flash operation succeeded
 * Assumptions   : - The calling application has unlocked all of the main
 *                   flash instance, and any NVR or redundancy sectors that
 *                   should be erased
 *                 - If the flash is in sequential programming mode, it is
 *                   safe to exit this mode
 * ------------------------------------------------------------------------- */
unsigned int Flash_EraseAll(void)
{
    /* Check that flash is powered up and not isolated */
    if (FLASH_IF_STATUS->ISOLATE_STATUS_ALIAS != FLASH_ACCESSIBLE_BITBAND)
    {
        return FLASH_ERR_INACCESSIBLE;
    }

    /* Wait for the flash to be idle and end any outstanding modes if needed */
    while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND)
    {
        FLASH->CMD_CTRL = CMD_END;
    }

    /* Setting retry 4 as required for a mass erase */
    FLASH->IF_CTRL = ((FLASH->IF_CTRL & ~(FLASH_IF_CTRL_RETRY_Mask)) |
                      FLASH_RETRY_4);

    return Flash_RunCommand(0, CMD_MASS_ERASE);
}

/* ----------------------------------------------------------------------------
 * Function      : void Flash_WriteCommand(uint32_t command)
 * ----------------------------------------------------------------------------
 * Description   : Safely issue a flash command; blocks waiting for the flash
 *                 to be idle before running the command and again before
 *                 returning.
 * Inputs        : command  - Command to be written to FLASH_CMD_CTRL; use
 *                            CMD_*
 * Outputs       : return value - Status code indicating whether the
 *                                flash interface can be written; returns
 *                                FLASH_ERR_INACCESSIBLE if the flash is 
 *                                isolated or not powered, otherwise returns
 *                                no error.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
unsigned int Flash_WriteCommand(uint32_t command)
{
    /* Check that flash is powered up and not isolated */
    if (FLASH_IF_STATUS->ISOLATE_STATUS_ALIAS != FLASH_ACCESSIBLE_BITBAND)
    {
        return FLASH_ERR_INACCESSIBLE;
    }

    /* Wait for the flash to be idle unless in sequential programming mode */
    if ((FLASH->CMD_CTRL & FLASH_CMD_CTRL_COMMAND_Mask) != CMD_PROGRAM_SEQ)
    {
        while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);
    }

    /* Write new command */
    FLASH->CMD_CTRL = command;

    /* Wait for the flash to be idle unless in sequential programming mode */
    if ((FLASH->CMD_CTRL & FLASH_CMD_CTRL_COMMAND_Mask) != CMD_PROGRAM_SEQ)
    {
        while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);
    }

    return FLASH_ERR_NONE;
}

/* ----------------------------------------------------------------------------
 * Function      : void Flash_WriteInterfaceControl(uint32_t ctrl)
 * ----------------------------------------------------------------------------
 * Description   : Safely write the interface control register; blocks waiting
 *                 for the flash to be idle before writing the interface
 *                 control register, and again before returning.
 * Inputs        : ctrl - Data to write to the FLASH_IF_CTRL register
 * Outputs       : return value - Status code indicating whether the
 *                                flash interface can be written; returns
 *                                FLASH_ERR_INACCESSIBLE if the flash is 
 *                                isolated or not powered and LP_MODE, RECALL,
 *                                VREAD0_MODE or VREAD1_MODE are being changed,
 *                                otherwise returns no error.
 * Assumptions   : - If the flash is in sequential programming mode, it is safe
 *                   to exit this mode
 *                 - No more than two of the LP_MODE, RECALL, VREAD0_MODE and
 *                   VREAD1_MODE bits are being updated in FLASH_IF_CTRL
 * ------------------------------------------------------------------------- */
unsigned int Flash_WriteInterfaceControl(uint32_t ctrl)
{
    uint32_t temp;
    uint32_t intStatus;
    const uint32_t mode_bits_mask = (1 << FLASH_IF_CTRL_VREAD1_MODE_Pos) |
                                    (1 << FLASH_IF_CTRL_VREAD0_MODE_Pos) |
                                    (1 << FLASH_IF_CTRL_RECALL_Pos) |
                                    (1 << FLASH_IF_CTRL_LP_MODE_Pos);

    /* Check if the mode bits are changed */
    temp = FLASH->IF_CTRL;
    if ((temp & mode_bits_mask) != (ctrl & mode_bits_mask))
    {
        /* Check that flash is powered up and not isolated */
        if (FLASH_IF_STATUS->ISOLATE_STATUS_ALIAS != FLASH_ACCESSIBLE_BITBAND)
        {
            return FLASH_ERR_INACCESSIBLE;
        }

        /* Wait for the flash to be idle and end any outstanding modes if
         * needed */
        while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND)
        {
            FLASH->CMD_CTRL = CMD_END;
        }

        /* Disable interrupts while executing flash commands */
        intStatus = __get_PRIMASK();
        __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

        /* Write new value */
        FLASH->IF_CTRL = ctrl;

        /* Wait for the flash to be idle */
        while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);

        /* Write new value a second time in case two bits have been updated */
        FLASH->IF_CTRL = ctrl;

        /* Wait for the flash to be idle */
        while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);

        __set_PRIMASK(intStatus);
    }
    else
    {
        /* Directly write new value as no mode bits are changed */
        FLASH->IF_CTRL = ctrl;
    }

    return FLASH_ERR_NONE;
}

/* ----------------------------------------------------------------------------
 * IFunction     : unsigned int Flash_Setup(unsigned int addr,
 *                                          unsigned int cmd)
 * ----------------------------------------------------------------------------
 * Description   : Ensure that the flash is ready for the specified command to
 *                 be executed, targeting the specified address. This includes
 *                 checking that the flash is not busy, before setting the
 *                 delays for the flash state machine using the supplied
 *                 system core clock frequency. Also check that the memory in
 *                 question is unlocked for write.
 * Inputs        : - addr       - Address in memory this command is executed
 *                                against
 *                 - cmd        - Flash command that will be executed; use
 *                                CMD_MASS_ERASE or another bit-setting for
 *                                the FLASH_CMD_CTRL_COMMAND bit-field from
 *                                FLASH_CMD_CTRL register
 * Outputs       : return value - Status code indicating whether the
 *                                requested setting was possible
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
unsigned int Flash_Setup(unsigned int addr, unsigned int cmd)
{
    unsigned int temp;

    /* Ensure the flash is currently idle before starting any operation */
    if (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND)
    {
        return FLASH_ERR_GENERAL_FAILURE;
    }

    /* Configure the flash delays required for proper flash operation */
    if (SystemCoreClock <= 3000000)
    {
        temp = FLASH_DELAY_FOR_SYSCLK_3MHZ;
    }
    else if (SystemCoreClock <= 5000000)
    {
        temp = FLASH_DELAY_FOR_SYSCLK_5MHZ;
    }
    else if (SystemCoreClock <= 8000000)
    {
        temp = FLASH_DELAY_FOR_SYSCLK_8MHZ;
    }
    else if (SystemCoreClock <= 12000000)
    {
        temp = FLASH_DELAY_FOR_SYSCLK_12MHZ;
    }
    else if (SystemCoreClock <= 16000000)
    {
        temp = FLASH_DELAY_FOR_SYSCLK_16MHZ;
    }
    else if (SystemCoreClock <= 20000000)
    {
        temp = FLASH_DELAY_FOR_SYSCLK_20MHZ;
    }
    else if (SystemCoreClock <= 24000000)
    {
        temp = FLASH_DELAY_FOR_SYSCLK_24MHZ;
    }
    else
    {
        temp = FLASH_DELAY_FOR_SYSCLK_48MHZ;
    }
    FLASH->DELAY_CTRL = temp;

    /* Check that the memories that should be write enabled are actually write
     * enabled */
    if (cmd == CMD_MASS_ERASE)
    {
        /* Only require that the main array of flash be unlocked */
        if ((FLASH->IF_STATUS & ((1 << FLASH_IF_STATUS_MAIN_HIGH_W_UNLOCK_Pos) |
                   (1 << FLASH_IF_STATUS_MAIN_MIDDLE_W_UNLOCK_Pos) |
                   (1 << FLASH_IF_STATUS_MAIN_LOW_W_UNLOCK_Pos))) != 
            (FLASH_MAIN_LOW_W_UNLOCKED |
             FLASH_MAIN_MIDDLE_W_UNLOCKED |
             FLASH_MAIN_HIGH_W_UNLOCKED))
        {
            return FLASH_ERR_WRITE_NOT_ENABLED;
        }
    }
    else if ((addr >= FLASH_MAIN_BASE) && (addr <= FLASH_MAIN_TOP))
    {
        if (addr < (FLASH_MAIN_BASE + FLASH_MAIN_SIZE/3))
        {
            if (FLASH_IF_STATUS->MAIN_LOW_W_UNLOCK_ALIAS ==
                 FLASH_MAIN_LOW_W_LOCKED_BITBAND)
            {
                return FLASH_ERR_WRITE_NOT_ENABLED;
            }
        }
        else if (addr >= (FLASH_MAIN_BASE + FLASH_MAIN_SIZE * 2/3))
        {
            if (FLASH_IF_STATUS->MAIN_HIGH_W_UNLOCK_ALIAS ==
                 FLASH_MAIN_HIGH_W_LOCKED_BITBAND)
            {
                return FLASH_ERR_WRITE_NOT_ENABLED;
            }
        }
        else if (FLASH_IF_STATUS->MAIN_MIDDLE_W_UNLOCK_ALIAS ==
                 FLASH_MAIN_MIDDLE_W_LOCKED_BITBAND)
        {
            return FLASH_ERR_WRITE_NOT_ENABLED;
        }
    }
    else if ((addr >= FLASH_NVR1_BASE) && (addr <= FLASH_NVR1_TOP))
    {
        if (FLASH_IF_STATUS->NVR1_W_UNLOCK_ALIAS == FLASH_NVR1_W_LOCKED_BITBAND)
        {
            return FLASH_ERR_WRITE_NOT_ENABLED;
        }
    }
    else if ((addr >= FLASH_NVR2_BASE) && (addr <= FLASH_NVR2_TOP))
    {
        if (FLASH_IF_STATUS->NVR2_W_UNLOCK_ALIAS == FLASH_NVR2_W_LOCKED_BITBAND)
        {
            return FLASH_ERR_WRITE_NOT_ENABLED;
        }
    }
    else if ((addr >= FLASH_NVR3_BASE) && (addr <= FLASH_NVR3_TOP))
    {
        if (FLASH_IF_STATUS->NVR3_W_UNLOCK_ALIAS == FLASH_NVR3_W_LOCKED_BITBAND)
        {
            return FLASH_ERR_WRITE_NOT_ENABLED;
        }
    }
    else if ((addr >= FLASH_REDUNDANCY_1_BASE) &&
             (addr <= FLASH_REDUNDANCY_1_TOP))
    {
        if (FLASH_IF_STATUS->RED1_W_UNLOCK_ALIAS == FLASH_RED1_W_LOCKED_BITBAND)
        {
            return FLASH_ERR_WRITE_NOT_ENABLED;
        }
    }
    else if ((addr >= FLASH_REDUNDANCY_2_BASE) &&
             (addr <= FLASH_REDUNDANCY_2_TOP))
    {
        if (FLASH_IF_STATUS->RED2_W_UNLOCK_ALIAS == FLASH_RED2_W_LOCKED_BITBAND)
        {
            return FLASH_ERR_WRITE_NOT_ENABLED;
        }
    }
    else
    {
        /* Not a valid flash memory address */
        return FLASH_ERR_BAD_ADDRESS;
    }
    return FLASH_ERR_NONE;
}

/* ----------------------------------------------------------------------------
 * IFunction     : unsigned int Flash_RunCommand(unsigned int addr,
 *                                               unsigned int cmd)
 * ----------------------------------------------------------------------------
 * Description   : Ensure the flash is setup for executing the specified erase
 *                 or write command. If the flash is setup correctly, execute
 *                 the command.
 * Inputs        : - addr       - Address in memory this command is executed
 *                                against
 *                 - cmd        - Flash command that will be executed; use a
 *                                bit-setting for the FLASH_CMD_CTRL_COMMAND
 *                                bit-field from FLASH_CMD_CTRL register
 * Outputs       : return value - Status code indicating whether the
 *                                requested flash operation could be executed
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
unsigned int Flash_RunCommand(unsigned int addr, unsigned int cmd)
{
    unsigned int result;
    uint32_t intStatus;

    /* Set the delays required to be able to erase or write the flash and
     * Check that the memory to be written is writable */
    result = Flash_Setup(addr, cmd);
    if (result == FLASH_ERR_NONE)
    {
        /* Perform the specified command */
        FLASH->ADDR = addr;

        /* Disable interrupts while executing flash commmand */
        intStatus = __get_PRIMASK();
        __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

        FLASH->CMD_CTRL = cmd;
        while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);

        __set_PRIMASK(intStatus);
    }
    return result;
}
