
/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 *  This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * ble_mfi.c
 * - Bluetooth MFI service functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/

#include <rsl10.h>
#include <ble_mfi.h>
#include <ble_gap.h>
#include <msg_handler.h>
#include <prf_utils.h>

/* ----------------------------------------------------------------------------
 * Static variables
 * --------------------------------------------------------------------------*/

/** MFI profile environment variable */
static MFI_Env_t mfi_env = {0};

/* ----------------------------------------------------------------------------
 * Function      : static MFI_DeviceState_t MFI_VerifyDevice(uint8_t conidx,
 *                                                       LEA_State_t lea_state)
 * ----------------------------------------------------------------------------
 * Description   : Verifies LEA device state:
 * Inputs        : conidx       - connection index
 *                 lea_state    - LEA state to be verified.
 * Outputs       : Device state.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static MFI_DeviceState_t MFI_VerifyDevice(uint8_t conidx, LEA_State_t lea_state)
{
    /* Set device state to invalid */
    MFI_DeviceState_t dev_state = MFI_DEV_INVALID;

    do
    {
        /* Check whether device is connected */
        if (!GAPC_IsConnectionActive(conidx)){
            /* Device not connected - break */
            break;
        }
        /* Check whether connection index matches with the one stored for LEA device */
        if (mfi_env.ctrlParams->control.device_indx != conidx){
            /* Return code: device connected */
            dev_state = MFI_DEV_CONNECTED;
            /* Break */
            break;
        }
        /* Check whether LEA state matches with the user specified state */
        if (mfi_env.ctrlParams->control.state == lea_state)
        {
            /* Return code: valid LEA state */
            dev_state = MFI_DEV_VALID_STATE;
            break;
        }
        else
        {
            /* Return code: valid device index */
            dev_state = MFI_DEV_VALID_IDX;
            break;
        }
    }
    while(0);

    /* Return device state */
    return dev_state;
}

/* ----------------------------------------------------------------------------
 * Function      : void MFI_SetPairingMode(uint8_t mode)
 * ----------------------------------------------------------------------------
 * Description   : Sets pairing mode.
 * Inputs        : mode     - enumerate value of pairing mode:
 *                            AM0_MODE_NON_PAIRABLE_MODE
 *                            AM0_MODE_PAIRABLE_MODE
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void MFI_SetPairingMode(MFI_PairingMode_t mode)
{
    /* Profile task */
    uint16_t prf_task = 0;

    /* Allocate message for setting the pairable mode */
    struct am0_has_set_pairable_mode_cmd *pairable =
            KE_MSG_ALLOC(AM0_HAS_SET_PAIRABLE_MODE_CMD,
                         prf_task, TASK_APP,
                         am0_has_set_pairable_mode_cmd);

    /* Set non-pairable mode */
    pairable->mode = mode;;

    /* Send the message */
    ke_msg_send(pairable);
}

/* ----------------------------------------------------------------------------
 * Function      : const MFI_Env_t* MFI_GetEnv(void)
 * ----------------------------------------------------------------------------
 * Description   : Returns a reference to the internal environment structure.
 * Inputs        : None
 * Outputs       : A constant pointer to MFI_Env_t.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
const MFI_Env_t* MFI_GetEnv(void)
{
    return &mfi_env;
}

/* ----------------------------------------------------------------------------
 * Function      : bool MFI_IsAdded(void)
 * ----------------------------------------------------------------------------
 * Description   : Returns a boolean indication whether the MFI accessory task
 *                 has been successfully added to the services database.
 * Inputs        : None
 * Outputs       : Return value: true if the service is added, false if not.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
bool MFI_IsAdded(void)
{
    return mfi_env.service_added;
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_HearingAid_ServiceAdd(void)
 * ----------------------------------------------------------------------------
 * Description   : Sends request to add Hearing Aid Service in kernel and DB.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_HearingAid_ServiceAdd(void)
{
    /* Database configuration */
    struct am0_has_db_cfg db_cfg =
    {
        .ava_progs_version  = mfi_env.readOnlyProp->ava_progs_version,
        .lea_version        = mfi_env.readOnlyProp->lea_version,
        .support_opt_char   = mfi_env.readOnlyProp->support_opt_char,
        .ear                = mfi_env.ctrlParams->audio.ear,
        .mic_volume         = mfi_env.ctrlParams->audio.mic_volume,
        .sec_stream_volume  = mfi_env.ctrlParams->audio.sec_stream_volume,
        .active_prog_id     = mfi_env.ctrlParams->audio.active_prog_id,
        .batt_lvl           = mfi_env.ctrlParams->battery.battery_lvl,
        .act_stream_prog_id = mfi_env.ctrlParams->audio.act_stream_prog_id,
        .substantially_diff = mfi_env.ctrlParams->audio.substantially_diff,
        .mic_sensitivity    = mfi_env.ctrlParams->audio.mic_sensitivity,
        .mic_preference     = mfi_env.ctrlParams->audio.mic_preference,
        .mic_presence       = mfi_env.ctrlParams->audio.mic_presence,
        .mic_vol_step       = mfi_env.ctrlParams->audio.mic_vol_step,
        .stream_vol_step    = mfi_env.ctrlParams->audio.stream_vol_step,
        .sensitivity_step   = mfi_env.ctrlParams->audio.sensitivity_step,
        .mixed_vol          = mfi_env.ctrlParams->audio.mixed_vol,
        .mixed_vol_step     = mfi_env.ctrlParams->audio.mic_vol_step,
        .bass               = mfi_env.ctrlParams->audio.bass,
        .treble             = mfi_env.ctrlParams->audio.treble,
    };

    /* Add task to the stack */
    GAPM_ProfileTaskAddCmd(PERM(SVC_AUTH, DISABLE), TASK_ID_AM0_HAS, TASK_APP,
            0, (uint8_t*) &db_cfg, sizeof(struct am0_has_db_cfg));
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_Send_Audio_Init(uint8_t conidx)
 * ----------------------------------------------------------------------------
 * Description   : Initiates the audio connection (the Accessory informs the
 *                 Device that it supports LEA audio, its capabilities and
 *                 supported codecs):
 * Inputs        : conidx   - connection index.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_Send_Audio_Init(uint8_t conidx)
{
    /* Allocate message for managing of the audio stream */
    struct am0_stream_ctrl_cmd *init;
    init = KE_MSG_ALLOC_DYN(AM0_STREAM_CTRL_CMD,
                            KE_BUILD_ID(TASK_AM0,
                            conidx),
                            APP_MAIN_TASK,
                            am0_stream_ctrl_cmd, 1);

    /* Operation type */
    init->operation = AM0_OP_AUDIO_INIT;

    /* Capabilities */
    init->param.init.capabilities = mfi_env.readOnlyProp->capabilities;

    /* Number of supported codecs */
    init->param.init.nb_codec = mfi_env.readOnlyProp->supported_codecs_len;

    /* Set Supported codec names */
    uint8_t cursor = 0;
    while(cursor < init->param.init.nb_codec)
    {
        init->param.init.codecs[cursor] = mfi_env.readOnlyProp->supported_codecs[cursor];
        cursor++;
    }

    /* Supported maximum packet size transferred over the air */
    init->param.init.max_pkt_size = MFI_SUP_MAX_PACKET_SIZE;

    /* Send the message */
    ke_msg_send(init);

    /* Configure the link synchronization */
    Sys_BBIF_SyncConfig(SYNC_ENABLE | SYNC_SOURCE_BLE_RX_AUDIO0, conidx,
                        SLAVE_CONNECT);
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_HearingAid_UpdateValue(uint8_t am0_has_char,
 *                                                              uint8_t conidx)
 * ----------------------------------------------------------------------------
 * Description   : Updates value of a given Hearing Aid Service characteristic:
 * Inputs        : am0_has_char - Hearing Aid Service characteristic,
 *                 conidx       - connection index.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_HearingAid_UpdateValue(uint8_t am0_has_char, uint8_t conidx)
{
    struct am0_has_env_tag *am0_has_env;
    struct am0_has_update_val_cmd *req;

    /* Get profile environment variable handle */
    am0_has_env = PRF_ENV_GET(AM0_HAS, am0_has);

    /* Allocate message for updating internal value */
    req = KE_MSG_ALLOC(AM0_HAS_UPDATE_VAL_CMD,
                       prf_src_task_get(&(am0_has_env->prf_env), conidx),
                       TASK_APP, am0_has_update_val_cmd);

    /* Value to be updated */
    req->value = am0_has_char;

    /* Select characteristic to be updated */
    switch (am0_has_char)
    {
        /* Available programs version */
        case AM0_HAS_AVA_PROGS_VERSION:
        {
            req->up.progs_version = mfi_env.readOnlyProp->ava_progs_version;
            break;
        }
        /* Update battery level information */
        case AM0_HAS_BATT_LVL:
        {
            req->up.batt_lvl = mfi_env.ctrlParams->battery.battery_lvl;
            break;
        }
        /* Microphone volume */
        case AM0_HAS_MIC_VOLUME:
        {
            req->up.volume = mfi_env.ctrlParams->audio.mic_volume;
            break;
        }
        /* 2nd Stream volume (range [0:255] 0 = mute) */
        case AM0_HAS_2ND_STREAM_VOLUME:
        {
            req->up.volume = mfi_env.ctrlParams->audio.sec_stream_volume;
            break;
        }
        /* Active program identifier */
        case AM0_HAS_ACTIVE_PROG_ID:
        {
            req->up.prog_id = mfi_env.ctrlParams->audio.active_prog_id;
            break;
        }
        /* Mixed Volume - Optional */
        case AM0_HAS_MIXED_VOL:
        {
            req->up.mixed_vol = mfi_env.ctrlParams->audio.mixed_vol;
            break;
        }
        /* Bass - Optional */
        case AM0_HAS_BASS:
        {
            req->up.bass = mfi_env.ctrlParams->audio.bass;
            break;
        }
        /* Treble - Optional */
        case AM0_HAS_TREBLE:
        {
            req->up.treble = mfi_env.ctrlParams->audio.treble;
            break;
        }
        /* Default */
        default:
        {
            /* Free allocated message */
            ke_msg_free((struct ke_msg const *)req);

            /* Return  */
            return;
        }
    }
    /* Send message */
    ke_msg_send(req);
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_CodecSelectReqInd
 *                                        (struct am0_codec_sel_req_ind *param,
 *                                         ke_task_id_t const dest_id,
 *                                         ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handles a received message indicating that peer device
 *                 has selected codec:
 * Inputs        : param      - message parameters in format
 *                              of struct am0_codec_sel_req_ind,
 *                 dest_id    - destination task ID number,
 *                 src_id     - source task ID number.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_CodecSelectReqInd(struct am0_codec_sel_req_ind *param,
                                 ke_task_id_t const dest_id,
                                 ke_task_id_t const src_id)
{
    /* Save data to the application environment variable */
    /* Selected codec identifier */
    mfi_env.ctrlParams->codec.selected_codec_id = param->codec_id;
    /* Selected max packet size supported */
    mfi_env.ctrlParams->transSet.selected_max_pkt_size = param->max_pkt_size;
    /* Selected connection interval */
    mfi_env.ctrlParams->transSet.selected_con_interval = param->con_interval;
    /* Selected audio content*/
    mfi_env.ctrlParams->transSet.selected_content = param->content;

    /* Allocate message to confirm that codec is correctly taken into account */
    struct am0_codec_sel_cfm* cfm = KE_MSG_ALLOC(AM0_CODEC_SEL_CFM, src_id,
                                                 dest_id, am0_codec_sel_cfm);

    /* Prepare confirmation message */
    /* Accept */
    cfm->accept = true;
    /* Codec delay */
    cfm->delay = mfi_env.ctrlParams->codec.codec_delay;
    /* Check whether bidirectional audio support is enabled */
    if (mfi_env.ctrlParams->transSet.bidirectional_sup)
    {
        /* Set audio transmitter size in bytes to the value received from peer */
        cfm->tx_size = param->max_pkt_size;
    }
    else
    {
        /* Set audio transmitter size in bytes to 0*/
        cfm->tx_size = 0;
    }
    /* Audio receiver size in bytes */
    cfm->rx_size = param->max_pkt_size;
    /* Audio configuration */
    cfm->audio_cfg = AM0_CFG_SYNC_P_GEN_EN;
    /* Mute pattern */
    cfm->mute_pattern = 0x00;

    /* Check whether selected codec is supported */
    cfm->accept = false;
    uint8_t cursor = 0;
    while(cursor < mfi_env.readOnlyProp->supported_codecs_len)
    {
        if (mfi_env.readOnlyProp->supported_codecs[cursor] == param->codec_id)
        {
            /* Set flag indicating that selected codec is supported */
            cfm->accept = true;
            break;
        }
        cursor++;
    }

    /* Send the message */
    ke_msg_send(cfm);
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_CodecReadyInd(uint8_t conidx)
 * ----------------------------------------------------------------------------
 * Description   : Handles a received message indicating that codec is ready:
 * Inputs        : conidx       - connection index.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_CodecReadyInd(uint8_t conidx)
{
    /* Check whether connection is active */
    if (MFI_VerifyDevice(conidx, LEA_DUMMY) != MFI_DEV_INVALID)
    {
        /* Indicate that LEA is ready for streaming */
        mfi_env.ctrlParams->control.state = LEA_READY;

        /* Save device index */
        mfi_env.ctrlParams->control.device_indx = conidx;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_StreamStartInd(uint8_t conidx,
 *                                                           uint16_t ch_delay)
 * ----------------------------------------------------------------------------
 * Description   : Handles a received message indicating that the stream
 *                 is started:
 * Inputs        : conidx       - connection index,
 *                 ch_delay     - channel delay.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_StreamStartInd(uint8_t conidx, uint16_t ch_delay)
{
    /* Check whether connection is active */
    if (MFI_VerifyDevice(conidx, LEA_READY) == MFI_DEV_VALID_STATE)
    {
        /* Perform actions required by application once the audio
         * streaming is started */
        mfi_env.mfiCallback(MFI_STREAM_START_IND);

        /* Indicate that LEA is streaming */
        mfi_env.ctrlParams->control.state = LEA_STREAMING;

        /* Set channel delay (in us) */
        mfi_env.ctrlParams->transSet.channel_delay = ch_delay;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_StreamStopInd(uint8_t conidx)
 * ----------------------------------------------------------------------------
 * Description   : Handles a received message indicating that the stream
 *                 is stopped:
 * Inputs        : conidx     - connection index.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_StreamStopInd(uint8_t conidx)
{
    /* Check whether connection is active */
    if (MFI_VerifyDevice(conidx, LEA_STREAMING) == MFI_DEV_VALID_STATE)
    {
        /* Perform actions required by the application when
         * the audio streaming is stopped */
        mfi_env.mfiCallback(MFI_STREAM_STOP_IND);

        /* Indicate that LEA gets back to ready */
        mfi_env.ctrlParams->control.state = LEA_READY;

        /* Allocate message to perform update of connection parameters */
        struct gapc_param_update_cmd *cmd =
            KE_MSG_ALLOC(GAPC_PARAM_UPDATE_CMD,
                         KE_BUILD_ID(TASK_GAPC, conidx),
                         TASK_APP, gapc_param_update_cmd);

        /* Operation */
        cmd->operation  = GAPC_UPDATE_PARAMS;
        /* Connection interval minimum */
        cmd->intv_min   = MFI_CONN_INTV_MIN;
        /* Connection interval maximum */
        cmd->intv_max   = MFI_CONN_INTV_MAX;
        /*  Latency */
        cmd->latency    = MFI_CONN_LATENCY;
        /*  Supervision timeout */
        cmd->time_out   = MFI_CONN_TIME_OUT;

        /* Minimum Connection Event Duration (not used by slave device) */
        cmd->ce_len_min = MFI_CONN_EVENT_DUR_MIN;
        /* Maximum Connection Event Duration (not used by slave device) */
        cmd->ce_len_max = MFI_CONN_EVENT_DUR_MAX;

        /* Send the message */
        ke_msg_send(cmd);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_ErrorInd(ke_task_id_t const dest_id,
 *                                                   ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handles a received message indicating an error:
 * Inputs        : dest_id      - destination task ID number,
 *                 src_id       - source task ID number.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_ErrorInd(ke_task_id_t const dest_id,
                            ke_task_id_t const src_id)
{
    /* Allocate message for managing of the audio stream */
    struct am0_stream_ctrl_cmd* restart;
    restart = KE_MSG_ALLOC(AM0_STREAM_CTRL_CMD,
                             src_id, dest_id, am0_stream_ctrl_cmd);

    /* Set operation: restart */
    restart->operation = AM0_OP_RESTART;

    /* Send message */
    ke_msg_send(restart);
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_RestartInd(uint8_t conidx)
 * ----------------------------------------------------------------------------
 * Description   : Handles a received message indicating that audio protocol
 *                 has been restarted:
 * Inputs        : conidx       - connection index.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_RestartInd(uint8_t conidx)
{
    /* Check whether the device index is valid */
    if(MFI_VerifyDevice(conidx, LEA_DUMMY) >= MFI_DEV_VALID_IDX)
    {
        /* Restart audio protocol */
        MFI_Send_Audio_Init(conidx);

        /* Indicate that LEA initialization is ongoing */
        mfi_env.ctrlParams->control.state = LEA_INIT;

        /* Notify connected device about battery level */
        MFI_HearingAid_UpdateValue(AM0_HAS_BATT_LVL, conidx);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_HA_ReadValReqInd(
 *                                   struct am0_has_read_val_req_ind *param,
                                     ke_task_id_t const dest_id,
                                     ke_task_id_t const src_id))
 * ----------------------------------------------------------------------------
 * Description   : Provides Hearing Aid Service information to response to
 *                 a read value request received from peer device:
 * Inputs        : param      - message parameters in format
 *                              of struct am0_has_read_val_req_ind,
 *                 dest_id    - destination task ID number,
 *                 src_id     - source task ID number.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_HA_ReadValReqInd(struct am0_has_read_val_req_ind *param,
                                     ke_task_id_t const dest_id,
                                     ke_task_id_t const src_id)
{
    switch (param->value)
    {
        /* Hearing Aid Identifier */
        case AM0_HAS_IDENTIFIER:
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *id_cfm = KE_MSG_ALLOC_DYN(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id,
                am0_has_read_val_cfm,
                MFI_AM0_IDENTIFIER_MAX_LEN);

            /* Set message data */
            id_cfm->value = param->value;
            id_cfm->status = GAP_ERR_NO_ERROR;
            id_cfm->res.array.len = mfi_env.readOnlyProp->id_len;
            memcpy(id_cfm->res.array.data, mfi_env.readOnlyProp->id,
                    mfi_env.readOnlyProp->id_len);

            /* Send message */
            ke_msg_send(id_cfm);
            break;
        }
        /* Other ear Hearing Aid Identifier */
        case AM0_HAS_OTHER_IDENTIFIER:
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *other_id_cfm = KE_MSG_ALLOC_DYN(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id,
                am0_has_read_val_cfm,
                MFI_AM0_IDENTIFIER_MAX_LEN);

            /* Set message data */
            other_id_cfm->value = param->value;
            other_id_cfm->status = GAP_ERR_NO_ERROR;
            other_id_cfm->res.array.len = mfi_env.readOnlyProp->other_id_len;
            memcpy(other_id_cfm->res.array.data, mfi_env.readOnlyProp->other_id,
                    mfi_env.readOnlyProp->other_id_len);

            /* Send message */
            ke_msg_send(other_id_cfm);
            break;
        }
        /* Available programs bit mask */
        case AM0_HAS_AVA_PROGS_BIT_MASK:
        {
            /* Set data length */
            uint8_t array_len = (mfi_env.ctrlParams->programs.avl_prog_num + 7) / 8;

            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *ava_progs_cfm = KE_MSG_ALLOC_DYN(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id,
                am0_has_read_val_cfm,
                array_len);

            /* Set message data */
            ava_progs_cfm->value = param->value;
            ava_progs_cfm->status = GAP_ERR_NO_ERROR;
            ava_progs_cfm->res.array.len = array_len;

            for (uint8_t cursor = 0; cursor < array_len; cursor++)
            {
                uint32_t value = ((1 << mfi_env.ctrlParams->programs.avl_prog_num) - 1) >> (cursor * 8);
                ava_progs_cfm->res.array.data[cursor] = value & 0xFF;
            }

            /* Send message */
            ke_msg_send(ava_progs_cfm);
            break;
        }
        /* Selected program identifier */
        case AM0_HAS_SELECTED_PROG_ID:
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *sel_prog = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            sel_prog->value = param->value;
            sel_prog->status = GAP_ERR_NO_ERROR;
            sel_prog->res.prog_id = mfi_env.ctrlParams->audio.selected_prog_id;

            /* Send message */
            ke_msg_send(sel_prog);
            break;
        }
        /* Selected program name */
        case AM0_HAS_SELECTED_PROG_NAME:
        {
            /* Set program identifier */
            uint8_t prog_id = mfi_env.ctrlParams->audio.selected_prog_id;

            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *sel_prog = KE_MSG_ALLOC_DYN(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm,
                AM0_PROG_NAME_MAX_LEN);

            /* Set message data */
            sel_prog->value = param->value;
            sel_prog->status = GAP_ERR_NO_ERROR;
            sel_prog->res.array.len =
                mfi_env.ctrlParams->programs.progs[prog_id].name_len;

            memcpy(sel_prog->res.array.data,
                mfi_env.ctrlParams->programs.progs[prog_id].name,
                AM0_PROG_NAME_MAX_LEN);

            /* Send message */
            ke_msg_send(sel_prog);
            break;
        }
        /* Selected program category */
        case AM0_HAS_SELECTED_PROG_CAT:
        {
            /* Set program identifier */
            uint8_t prog_id = mfi_env.ctrlParams->audio.selected_prog_id;

            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *prog_cat = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            prog_cat->value = param->value;
            prog_cat->status = GAP_ERR_NO_ERROR;
            prog_cat->res.category =
                mfi_env.ctrlParams->programs.progs[prog_id].category;

            /* Send message */
            ke_msg_send(prog_cat);
            break;
        }
        /* Sensitivity (optional) */
        case AM0_HAS_SENSITIVITY:
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *sensitivity = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            sensitivity->value = param->value;
            sensitivity->status = GAP_ERR_NO_ERROR;
            sensitivity->res.sensitivity = mfi_env.ctrlParams->audio.mic_sensitivity;

            /* Send message */
            ke_msg_send(sensitivity);
            break;
        }
        /* Mixed volume (optional) */
        case AM0_HAS_MIXED_VOL:
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *mixed_vol = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            mixed_vol->value = param->value;
            mixed_vol->status = GAP_ERR_NO_ERROR;
            mixed_vol->res.mixed_volume = mfi_env.ctrlParams->audio.mixed_vol;

            /* Send message */
            ke_msg_send(mixed_vol);
            break;
        }
        /* Bass (optional) */
        case AM0_HAS_BASS:
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *bass = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            bass->value = param->value;
            bass->status = GAP_ERR_NO_ERROR;
            bass->res.bass = mfi_env.ctrlParams->audio.bass;

            /* Send message */
            ke_msg_send(bass);
            break;
        }
        /* Treble (optional) */
        case AM0_HAS_TREBLE:
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *treble = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            treble->value = param->value;
            treble->status = GAP_ERR_NO_ERROR;
            treble->res.treble = mfi_env.ctrlParams->audio.treble;

            /* Send message */
            ke_msg_send(treble);
            break;
        }
        /* Substantially different (optional) */
        case AM0_HAS_SUB_DIFF :
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *treble = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            treble->value = param->value;
            treble->status = GAP_ERR_NO_ERROR;
            treble->res.sub_diff = mfi_env.ctrlParams->audio.substantially_diff;

            /* Send message */
            ke_msg_send(treble);
            break;
        }
        /* Mixed volume step size (optional) */
        case AM0_HAS_MIXED_VOL_STEP_SIZE :
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *treble = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            treble->value = param->value;
            treble->status = GAP_ERR_NO_ERROR;
            treble->res.mixed_vol_step_size = mfi_env.ctrlParams->audio.mixed_vol_step;

            /* Send message */
            ke_msg_send(treble);
            break;
        }
        /* Microphone volume step size (optional) */
        case AM0_HAS_MIC_VOL_STEP_SIZE :
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *treble = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            treble->value = param->value;
            treble->status = GAP_ERR_NO_ERROR;
            treble->res.mic_vol_step_size = mfi_env.ctrlParams->audio.mic_vol_step;

            /* Send message */
            ke_msg_send(treble);
            break;
        }
        /* Stream volume step size (optional) */
        case AM0_HAS_STREAM_VOL_STEP_SIZE :
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *treble = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            treble->value = param->value;
            treble->status = GAP_ERR_NO_ERROR;
            treble->res.stream_vol_step_size = mfi_env.ctrlParams->audio.stream_vol_step;

            /* Send message */
            ke_msg_send(treble);
            break;
        }
        /* Sensitivity step size (optional) */
        case AM0_HAS_SENSITIVITY_STEP_SIZE :
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *treble = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            treble->value = param->value;
            treble->status = GAP_ERR_NO_ERROR;
            treble->res.sensitivity_step_size = mfi_env.ctrlParams->audio.sensitivity_step;

            /* Send message */
            ke_msg_send(treble);
            break;
        }
        /* Active stream program identifier */
        case AM0_HAS_ACT_STREAM_PROG_ID:
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *prog_id = KE_MSG_ALLOC(
                AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            prog_id->value = param->value;
            prog_id->status = GAP_ERR_NO_ERROR;
            prog_id->res.act_stream_prog_id =
                mfi_env.ctrlParams->audio.act_stream_prog_id;

            /* Send message */
            ke_msg_send(prog_id);
            break;
        }
        case AM0_HAS_MICROPHONE_PREFERENCE :
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *mic_preference
                = KE_MSG_ALLOC(AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            mic_preference->value = param->value;
            mic_preference->status = GAP_ERR_NO_ERROR;
            mic_preference->res.mic_preference = mfi_env.ctrlParams->audio.mic_preference;

            /* Send message */
            ke_msg_send(mic_preference);
            break;
        }
        case AM0_HAS_MICROPHONE_PRESENCE :
        {
            /* Allocate message to send back to peer the requested service value*/
            struct am0_has_read_val_cfm *mic_presence
                = KE_MSG_ALLOC(AM0_HAS_READ_VAL_CFM, src_id, dest_id, am0_has_read_val_cfm);

            /* Set message data */
            mic_presence->value = param->value;
            mic_presence->status = GAP_ERR_NO_ERROR;
            mic_presence->res.mic_presence = mfi_env.ctrlParams->audio.mic_presence;

            /* Send message */
            ke_msg_send(mic_presence);
            break;
        }
        /* Default */
        default:
        {
            /* Do nothing */
            break;
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_HA_WriteValReqInd(
 *                                 struct am0_has_write_val_req_ind *param,
 *                                 ke_task_id_t const dest_id,
 *                                 ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Updates LEA application environment when receiving a write
 *                 value request received from peer device:
 * Inputs        : param      - message parameters in format
 *                              of struct gapm_cmp_evt,
 *                 dest_id    - destination task ID number,
 *                 src_id     - source task ID number.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_HA_WriteValReqInd(struct am0_has_write_val_req_ind *param,
                                  ke_task_id_t const dest_id,
                                  ke_task_id_t const src_id)
{
    /* Set initial value of the status to GAP no error */
    uint8_t status = GAP_ERR_NO_ERROR;
    MFI_Command_t mfi_command = 0;

    /* Select characteristic to be written */
    switch (param->value)
    {
        /* Selected program name */
        case AM0_HAS_SELECTED_PROG_NAME:
        {
            /* Check whether selected program name is valid */
            if (param->req.array.len
                              <= mfi_env.ctrlParams->programs.prog_name_len_max)
            {
                /* Get program identifier */
                uint8_t prog_id = mfi_env.ctrlParams->audio.selected_prog_id;

                /* Update program name */
                memcpy(mfi_env.ctrlParams->programs.progs[prog_id].name,
                    param->req.array.data, param->req.array.len);

                /* Update program name length */
                mfi_env.ctrlParams->programs.progs[prog_id].name_len
                    = param->req.array.len;

                /* Set notification event */
                mfi_command = MFI_HAS_SELECTED_PROG_NAME_UPD;
            }
            else
            {
                /* Set message status to application error */
                status = ATT_ERR_APP_ERROR;
            }
            break;
        }
        /* Mic volume */
        case AM0_HAS_MIC_VOLUME:
        {
            /* Notify application */
            struct am0_volume_ind *par = (struct am0_volume_ind *)(param);
            /* Perform volume level update */
            mfi_env.ctrlParams->audio.mic_volume = par->volume;
            /* Set notification event */
            mfi_command = MFI_HAS_MIC_VOLUME_UPD;
            break;
        }
        /* Second stream volume */
        case AM0_HAS_2ND_STREAM_VOLUME:
        {
            /* Missing information in AM0 HAS CEVA documentation */
            /* Set message status to application error */
            status = ATT_ERR_APP_ERROR;
            break;
        }
        /* Active program identifier */
        case AM0_HAS_ACTIVE_PROG_ID:
        {
            /* Check whether program identifier is valid */
            if (param->req.prog_id < mfi_env.ctrlParams->programs.avl_prog_num)
            {
                /* Update active program identifier */
                mfi_env.ctrlParams->audio.active_prog_id = param->req.prog_id;
                /* Set notification event */
                mfi_command = MFI_HAS_ACTIVE_PROG_ID_UPD;
            }
            else
            {
                /* Set message status to application error */
                status = ATT_ERR_APP_ERROR;
            }
            break;
        }
        /* Selected program identifier */
        case AM0_HAS_SELECTED_PROG_ID:
        {
            /* Check whether program identifier is valid */
            if (param->req.prog_id < mfi_env.ctrlParams->programs.avl_prog_num)
            {
                /* Set selected program identifier */
                mfi_env.ctrlParams->audio.selected_prog_id = param->req.prog_id;
                /* Set notification event */
                mfi_command = MFI_HAS_SELECTED_PROG_ID_UPD;
            }
            else
            {
                /* Set message status to application error */
                status = ATT_ERR_APP_ERROR;
            }
            break;
        }
        /* Selected program category */
        case AM0_HAS_SELECTED_PROG_CAT:
        {
            /* Get program identifier */
            uint8_t prog_id = mfi_env.ctrlParams->audio.selected_prog_id;

            /* Set program category */
            mfi_env.ctrlParams->programs.progs[prog_id].category
                                                        = param->req.category;
            /* Set notification event */
            mfi_command = MFI_HAS_SELECTED_PROG_CAT_UPD;
            break;
        }
        /* Mixed volume (optional) */
        case AM0_HAS_MIXED_VOL:
        {
            /* Update mixed volume level */
            mfi_env.ctrlParams->audio.mixed_vol = param->req.mixed_volume;
            /* Set notification event */
            mfi_command = MFI_HAS_MIXED_VOL_UPD;
            break;
        }
        /* Bass (optional) */
        case AM0_HAS_BASS:
        {
            /* Update bass level */
            mfi_env.ctrlParams->audio.bass = param->req.bass;
            /* Set notification event */
            mfi_command = MFI_HAS_BASS_UPD;
            break;
        }
        /* Treble (optional) */
        case AM0_HAS_TREBLE:
        {
            /* Update treble level */
            mfi_env.ctrlParams->audio.treble = param->req.treble;
            /* Set notification event */
            mfi_command = MFI_HAS_TREBLE_UPD;
            break;
        }
        /* Active stream program identifier */
        case AM0_HAS_ACT_STREAM_PROG_ID:
        {
            /* Update active stream program identifier */
            mfi_env.ctrlParams->audio.act_stream_prog_id
                                        = param->req.act_stream_prog_id;
            /* Set notification event */
            mfi_command = MFI_HAS_ACT_STREAM_PROG_ID_UPD;
            break;
        }
        case AM0_HAS_MICROPHONE_PREFERENCE :
        {
            mfi_env.ctrlParams->audio.mic_preference
                                    = (param->req.mic_preference & 0xFF);
            /* Set notification event */
            mfi_command = MFI_HAS_MICROPHONE_PREFERENCE_UPD;
            break;
        }
        /* Default */
        default:
        {
            /* Do nothing */
            break;
        }
    }

    if (status == GAP_ERR_NO_ERROR) {
        /* Notify application */
        mfi_env.mfiCallback(mfi_command);
    }

    /* Allocate confirmation message to send back the status of write operation */
    struct am0_has_write_val_cfm *cfm = KE_MSG_ALLOC(AM0_HAS_WRITE_VAL_CFM,
                                                     src_id, dest_id,
                                                     am0_has_write_val_cfm);

    /* Set message data */
    cfm->value = param->value;
    cfm->status = status;

    /* Send message */
    ke_msg_send(cfm);
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_HA_GetAuthInfoReqInd(
 *                                  struct am0_has_get_auth_info_req_ind *param,
                                    ke_task_id_t const dest_id,
                                    ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Provides Authentication Service information to response to
 *                 a read value request received from peer device:
 * Inputs        : param    - message parameters in format
 *                            of struct am0_has_get_auth_info_req_ind,
 *                 dest_id  - destination task ID number,
 *                 src_id   - source task ID number.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_HA_GetAuthInfoReqInd(struct am0_has_get_auth_info_req_ind *param,
                                         ke_task_id_t const dest_id,
                                         ke_task_id_t const src_id)
{
    /* Select Authentication Service information */
    switch (param->info)
    {
        /* First part of the X.509 certificate */
        case AM0_AS_CERTIFICATE_PART_A:
        {
            /* Determine the length of the first part of certificate data */
            uint16_t length = co_min(MFI_CERT_DATA_PART_LEN,
                                            mfi_env.readOnlyProp->cert_data_len);

            /* Allocate message to send authentication information */
            struct am0_has_get_auth_info_cfm *cfm_a = KE_MSG_ALLOC_DYN(
                AM0_HAS_GET_AUTH_INFO_CFM, src_id, dest_id,
                am0_has_get_auth_info_cfm,
                length);

            /* Set message data */
            cfm_a->info = param->info;
            cfm_a->status = GAP_ERR_NO_ERROR;
            cfm_a->array.len = length;
            memcpy(cfm_a->array.data,
                   &(mfi_env.readOnlyProp->cert_data[0]), length);

            /* Send message */
            ke_msg_send(cfm_a);
            break;
        }
        /* Second part of the X.509 certificate */
        case AM0_AS_CERTIFICATE_PART_B:
        {
            /* Pointer to message with authentication information*/
            struct am0_has_get_auth_info_cfm *cfm_b;

            /* Check whether second part of authentication information is available */
            if (mfi_env.readOnlyProp->cert_data_len >= MFI_CERT_DATA_PART_LEN)
            {
                /* Determine the length of the second part of certificate data */
                uint16_t length = co_min(MFI_CERT_DATA_PART_LEN,
                                            mfi_env.readOnlyProp->cert_data_len -
                                            MFI_CERT_DATA_PART_LEN);

                /* Allocate message to send authentication information */
                cfm_b = KE_MSG_ALLOC_DYN(AM0_HAS_GET_AUTH_INFO_CFM,
                                         src_id,
                                         dest_id,
                                         am0_has_get_auth_info_cfm,
                                         length);

                /* Set message data */
                cfm_b->info = param->info;
                cfm_b->status = GAP_ERR_NO_ERROR;
                cfm_b->array.len = length;
                memcpy(cfm_b->array.data,
                       &(mfi_env.readOnlyProp->cert_data[MFI_CERT_DATA_PART_LEN]),
                       length);
            }
            else
            {
                /* Allocate message to inform peer about application error */
                cfm_b = KE_MSG_ALLOC(AM0_HAS_GET_AUTH_INFO_CFM, src_id,
                                     dest_id,
                                     am0_has_get_auth_info_cfm);

                /* Set message data */
                cfm_b->info = param->info;
                cfm_b->status = ATT_ERR_APP_ERROR;
            }

            /* Send message */
            ke_msg_send(cfm_b);
            break;
        }
        /* Authentication private key to calculate the challenge response using RSA */
        case AM0_AS_PRIVATE_KEY:
        {
            /* Pointer to message with a authentication private key */
            struct am0_has_get_auth_info_cfm *cfm_p_key;

            /* Allocate message to send private key */
            cfm_p_key = KE_MSG_ALLOC_DYN(AM0_HAS_GET_AUTH_INFO_CFM,
                                         src_id,
                                         dest_id,
                                         am0_has_get_auth_info_cfm,
                                         mfi_env.readOnlyProp->cert_priv_key_len);

            /* Set message data */
            cfm_p_key->info = param->info;
            cfm_p_key->status = GAP_ERR_NO_ERROR;
            cfm_p_key->array.len = mfi_env.readOnlyProp->cert_priv_key_len;
            memcpy(cfm_p_key->array.data, &(mfi_env.readOnlyProp->cert_priv_key[0]),
                    mfi_env.readOnlyProp->cert_priv_key_len);

            /* Send message */
            ke_msg_send(cfm_p_key);
            break;
        }
        /* Default*/
        default:
        {
            /* Do nothing */
        }
        break;
    }
}

/* ----------------------------------------------------------------------------
static void MFI_HearingAid_RSAReqInd(const struct am0_has_init_rsa_req_ind *param,
                                  ke_task_id_t const dest_id,
                                  ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handles RSA request indicator:
 * Inputs        : param    - message parameters in a format
 *                            of struct am0_has_init_rsa_req_ind,
 *                 dest_id  - destination task ID number,
 *                 src_id   - source task ID number.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_HearingAid_RSAReqInd(const struct am0_has_init_rsa_req_ind *param,
                                  ke_task_id_t const dest_id,
                                  ke_task_id_t const src_id)
{
    /* Request RSA Challenge is initiated
       This can be done in either of two ways :
          1. Using RSA in underlying stack - as shown below
          2. Using Apple Auth Co-Processor to perform RSA */
    if (mfi_env.readOnlyProp->auth_type == MFI_SW_AUTHENTICATION)
    {
        struct am0_has_init_challenge_cmd *cmd_p_key;
        cmd_p_key = KE_MSG_ALLOC_DYN(AM0_HAS_INIT_CHALLENGE_CMD, src_id, dest_id,
                am0_has_init_challenge_cmd, mfi_env.readOnlyProp->cert_priv_key_len);
        cmd_p_key->priv_key.len = mfi_env.readOnlyProp->cert_priv_key_len;
        cmd_p_key->conidx = param->conidx;
        memcpy(&cmd_p_key->priv_key.data[0],  &mfi_env.readOnlyProp->cert_priv_key[0],
                                        mfi_env.readOnlyProp->cert_priv_key_len);
        if (param->rsa_challenge_length <= AM0_HAS_MAX_CHALLENGE_SIZE)
        {
          memcpy(&cmd_p_key->challenge[0],&param->rsa_challenge[0], param->rsa_challenge_length);
          cmd_p_key->challenge_length = param->rsa_challenge_length;
        }
        ke_msg_send(cmd_p_key);
    }
    else
    {
        /* TODO  Communicate with coprocessor */
    }
    /* Respond to the Write... */
    {
        struct am0_has_init_rsa_cfm *rsa_cfm;

        rsa_cfm = KE_MSG_ALLOC(AM0_HAS_INIT_RSA_CFM, src_id, dest_id, am0_has_init_rsa_cfm);
        rsa_cfm->status    = GAP_ERR_NO_ERROR;
        ke_msg_send(rsa_cfm);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_HearingAidChallengeComplete(uint8_t conidx,
                                                const uint8_t * challenge_rsp,
                                                uint16_t challenge_rsp_length)
 * ----------------------------------------------------------------------------
 * Description   : Handles challenge complete event:
 * Inputs        : conidx           - connection index,
 *                 challenge_rsp    - challenge response,
 *                 challenge_rsp    - challenge response length.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_HearingAidChallengeComplete(uint8_t conidx,
                                                const uint8_t * challenge_rsp,
                                                uint16_t challenge_rsp_length)
{
    struct am0_has_challenge_complete *cmpl;

    struct am0_has_env_tag *am0_has_env = PRF_ENV_GET(AM0_HAS, am0_has);

    cmpl = KE_MSG_ALLOC_DYN(AM0_HAS_CHALLENGE_COMPLETE,
              prf_src_task_get(&(am0_has_env->prf_env), conidx),
              APP_MAIN_TASK, am0_has_challenge_complete, challenge_rsp_length);

    cmpl->conidx = conidx;
    cmpl->status = GAP_ERR_NO_ERROR;
    cmpl->sign_len = challenge_rsp_length;
    memcpy(&cmpl->signature[0], challenge_rsp, challenge_rsp_length);

    ke_msg_send(cmpl);
}

/* ----------------------------------------------------------------------------
 * Function      : void MFI_HearingAidSignitureInd(ke_msg_id_t const msgid,
                                             struct am0_has_signature_ind *param,
                                             ke_task_id_t const dest_id,
                                             ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handles signature indication:
 * Inputs        : msg_id   - kernel message ID number,
 *                 param    - message parameter in a format
 *                            of struct am0_has_signature_ind,
 *                 dest_id  - destination task ID number,
 *                 src_id   - source task ID number.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_HearingAidSignitureInd(const struct am0_has_signature_ind *param)
{
    /* Once the App gets the signature Ind - It should call the API to inform the
       stack of the challenge response. */
    MFI_HearingAidChallengeComplete(param->conidx,
                        &param->challenge_rsp.data[0], param->challenge_rsp.len);
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_MsgHandler(ke_msg_id_t const msg_id,
 *                                            void const *param,
*                                             ke_task_id_t const dest_id,
 *                                            ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handles all events related to the MFI Accessory:
 * Inputs        : msg_id   - kernel message ID number,
 *                 param    - message parameter,
 *                 dest_id  - destination task ID number,
 *                 src_id   - source task ID number.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void MFI_MsgHandler(ke_msg_id_t const msg_id, void const *param,
        ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    /* Get connection index */
    uint8_t conidx = KE_IDX_GET(src_id);

    switch (msg_id)
    {
        /* GAPM complete event */
        case GAPM_CMP_EVT:
        {
            struct gapm_cmp_evt *gap_event = (struct gapm_cmp_evt*) param;

            /* Check whether set device configuration was selected */
            if (gap_event->operation == GAPM_SET_DEV_CONFIG) {

                /* Indicate that LEA is not connected */
                mfi_env.ctrlParams->control.state = LEA_UNCONNECTED;
                mfi_env.ctrlParams->control.lea_init_timer = 0;
                mfi_env.ctrlParams->control.device_indx = MFI_INVALID_DEV_IDX;

                /* Add profile task to BLE stack */
                MFI_HearingAid_ServiceAdd();

                break;
            }
            break;
        }
        /* Indication of added profile task */
        case GAPM_PROFILE_ADDED_IND:
        {
            struct gapm_profile_added_ind *ind =
                    (struct gapm_profile_added_ind*) param;

            /* Check added service ID */
            if (ind->prf_task_id == TASK_ID_AM0_HAS)
            {
                /* Set flag in environment variable if FMPT service was added successfully */
                mfi_env.service_added = true;

            }
            break;
        }
        /* Bonding information indication message */
        case GAPC_BOND_IND:
        {
            /* Get bond indication parameters */
            struct gapc_bond_ind const *par = (struct gapc_bond_ind const *) param;

            /* Check whether device is paired successfully */
            if(par->info == GAPC_PAIRING_SUCCEED)
            {
                /* Check whether device is bonded */
                if(GAPC_IsBonded(conidx))
                {
                    /* If LEA has not been initiated */
                    if (mfi_env.ctrlParams->control.state < LEA_INIT)
                    {
                        /* Initialize the audio protocol */
                        MFI_Send_Audio_Init(conidx);

                        /* Indicate that LEA initialization is ongoing */
                        mfi_env.ctrlParams->control.state = LEA_INIT;

                        /* Start timer for LEA initialization */
                        mfi_env.ctrlParams->control.lea_init_timer = 1;

                        /* Register the connected device */
                        mfi_env.ctrlParams->control.device_indx = conidx;

                        /* Update battery level to the peer device */
                        MFI_HearingAid_UpdateValue(AM0_HAS_BATT_LVL, conidx);
                    }
                }
            }
            break;
        }
        /* Encryption information indication message */
        case GAPC_ENCRYPT_IND:
        {
            /* Check whether device is connected */
            if (!GAPC_IsConnectionActive(conidx))
            {
                /* If device not connected - return */
                return;
            }

            /* If LEA has not been initiated */
            if (mfi_env.ctrlParams->control.state < LEA_INIT)
            {
                /* Initialize the audio protocol */
                MFI_Send_Audio_Init(conidx);

                /* Indicate that LEA initialization is ongoing */
                mfi_env.ctrlParams->control.state = LEA_INIT;

                /* Start timer for LEA initialization */
                mfi_env.ctrlParams->control.lea_init_timer = 1;

                /* Register the connected device */
                mfi_env.ctrlParams->control.device_indx = conidx;

                /* Update battery level to the peer device */
                MFI_HearingAid_UpdateValue(AM0_HAS_BATT_LVL, conidx);
            }
            break;
        }
        /* Disconnected link indication */
        case GAPC_DISCONNECT_IND:
        {
            /* Indicate that LEA is not connected */
            mfi_env.ctrlParams->control.state = LEA_UNCONNECTED;

            /* Stop timer */
            mfi_env.ctrlParams->control.lea_init_timer = 0;

            /* Clear device index */
            mfi_env.ctrlParams->control.device_indx = MFI_INVALID_DEV_IDX;

            /* Stop streaming */
            MFI_StreamStopInd(conidx);
            break;
        }
        /* Audio Mode 0 complete event */
        case AM0_CMP_EVT : {
            break;
        }
        /* Device being Audio Mode 0 Compliant indication */
        case AM0_DEVICE_CONNECT_IND:
        {
            /* Update LEA device state */
            mfi_env.ctrlParams->control.state = LEA_DEV_CONNECTED;
            break;
        }
        /* Selecting a specific codec by the Device indication */
        case AM0_CODEC_SEL_REQ_IND :
        {
            /* Handle codec select request indication */
            MFI_CodecSelectReqInd((struct am0_codec_sel_req_ind*)param, dest_id, src_id);
            break;
        }
        /* Accessory codec being ready to use indication */
        case AM0_CODEC_READY_IND :
        {
            /* Handle codec ready indication */
            MFI_CodecReadyInd(conidx);
            break;
        }
        /* Volume update by the remote Device indication */
        case AM0_VOLUME_IND :
        {
            struct am0_volume_ind *par = (struct am0_volume_ind *)(param);
            /* Perform volume level update using application callback function */
            mfi_env.ctrlParams->audio.volume = par->volume;
            /* Notify application */
            mfi_env.mfiCallback(MFI_VOLUME_UPD);
            break;
        }
        /* Device request to start streaming indication */
        case AM0_STREAM_START_IND :
        {
            struct am0_stream_start_ind *par = (struct am0_stream_start_ind *) param;
            /* Handle start streaming indication */
            MFI_StreamStartInd(conidx, par->delay);
            break;
        }
        /* Device request to stop streaming indication */
        case AM0_STREAM_STOP_IND :
        {
            /* Handle stop streaming indication */
            MFI_StreamStopInd(conidx);
            break;
        }
        /* Receiving the message through Device from another Accessory indication */
        case AM0_MSG_IND :
        {
            break;
        }
        /* Unexpected error triggered by a peer Device indication */
        case AM0_ERROR_IND :
        {
            /* Handle error indication */
            MFI_ErrorInd(dest_id, src_id);
            break;
        }
        /* Device request to restart audio connection indication */
        case AM0_RESTART_IND :
        {
            /* Handle restart indication */
            MFI_RestartInd(conidx);
            break;
        }
        /* Hearing Aid profile complete event */
        case AM0_HAS_CMP_EVT :
        {
            break;
        }
        /* Notification update for specific connection indication */
        case AM0_NTF_CFG_UPDATE_IND :
        {
            break;
        }
        /* Request for reading service value indication */
        case AM0_HAS_READ_VAL_REQ_IND :
        {
            /* Handle read value indication */
            MFI_HA_ReadValReqInd
                        ((struct am0_has_read_val_req_ind *)(param), dest_id, src_id);
            break;
        }
        /* Request for writing service value indication */
        case AM0_HAS_WRITE_VAL_REQ_IND :
        {
            /* Handle write value indication */
            MFI_HA_WriteValReqInd
                    ((struct am0_has_write_val_req_ind *) param, dest_id, src_id);
            break;
        }
        /* Request for retrieving Authentication Service information indication */
        case AM0_HAS_GET_AUTH_INFO_REQ_IND :
        {
            /* Handle get Authentication Service information indication */
            MFI_HA_GetAuthInfoReqInd((struct am0_has_get_auth_info_req_ind *)param, dest_id, src_id);
            break;
        }
        /* RSA request indicatgion */
        case AM0_HAS_INIT_RSA_REQ_IND :
        {
            /* Handle RSA request indication */
            MFI_HearingAid_RSAReqInd(param, dest_id, src_id);
            break;
        }
        /* Authentication signature indication */
        case AM0_HAS_SIGNATURE_IND :
        {
            /* Handles authentication signature indication */
            MFI_HearingAidSignitureInd(param);
            break;
        }
        /* Default */
        default:
            break;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : static void MFI_Initialize(void (*runAlertOnDevice)
 *                                                 (alertLevel_t alertLevel))
 * ----------------------------------------------------------------------------
 * Description   : Initializes MFI service environment and configure message
 *                 handlers:
 * Inputs        : mfiCallback -    pointer to user application callback
 *                                  function that allows to:
 *                                  - start/stop audio
 *                                  - update volume level.
 * Outputs       : MFI initialization status.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint8_t MFI_Initialize(MFI_Config_t *cfg,
                    void (*mfiCallback)(MFI_Command_t))
{
    /* Clear internal environment structure */
    memset(&mfi_env, 0, sizeof(MFI_Env_t));

    /* Set pointer to LEA read-only parameters */
    mfi_env.readOnlyProp = cfg->readOnlyProp;

    /* Set pointer to LEA control parameters */
    mfi_env.ctrlParams = cfg->controlParams;

    /* Check whether length of private key is correct */
    if (mfi_env.readOnlyProp->cert_priv_key_len > MFI_AM0_CERT_PRIV_KEY_MAX_LEN)
    {
        /* Return error code */
        return MFI_ERROR_INVALID_PARAMETER;
    }
    /* Check whether length of certificate data is correct */
    if (mfi_env.readOnlyProp->cert_data_len > MFI_AM0_CERT_DATA_MAX_LEN)
    {
        /* Return error code */
        return MFI_ERROR_INVALID_PARAMETER;
    }
    /* Check whether length of other identifier is correct */
    if (mfi_env.readOnlyProp->other_id_len > MFI_AM0_IDENTIFIER_MAX_LEN)
    {
        /* Return error code */
        return MFI_ERROR_INVALID_PARAMETER;
    }
    /* Check whether length of identifier is correct */
    if (mfi_env.readOnlyProp->id_len > MFI_AM0_IDENTIFIER_MAX_LEN)
    {
        /* Return error code */
        return MFI_ERROR_INVALID_PARAMETER;
    }

    /* Check whether MFI callback function is valid */
    if (mfiCallback != NULL)
    {
        /* Set pointer to MFI callback function */
        mfi_env.mfiCallback = mfiCallback;
    }
    else
    {
        /* Return error code */
        return MFI_ERROR_INVALID_PARAMETER;
    }

    /* Add MFI message handlers */
    MsgHandler_Add(TASK_ID_AM0, MFI_MsgHandler);
    MsgHandler_Add(TASK_ID_AM0_HAS, MFI_MsgHandler);
    MsgHandler_Add(GAPM_CMP_EVT, MFI_MsgHandler);
    MsgHandler_Add(GAPC_BOND_IND, MFI_MsgHandler);
    MsgHandler_Add(GAPC_ENCRYPT_IND, MFI_MsgHandler);
    MsgHandler_Add(GAPC_DISCONNECT_IND, MFI_MsgHandler);
    MsgHandler_Add(GAPM_PROFILE_ADDED_IND, MFI_MsgHandler);

    /* Return OK */
    return MFI_ERROR_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void MFI_NotifyAboutUpdatedValue(uint8_t am0_has_char)
 * ----------------------------------------------------------------------------
 * Description   : Function notifying connected devices about value update:
 * Inputs        : am0_has_char - characteristic type.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void MFI_NotifyAboutUpdatedValue(MFI_HasChar_t am0_has_char)
{
    for (unsigned int conidx = 0; conidx < BLE_CONNECTION_MAX; conidx++)
    {
        /* Check whether device is bonded */
        if (GAPC_IsBonded(conidx))
        {
            /* Notify peer device about updated characteristic value */
            MFI_HearingAid_UpdateValue(am0_has_char, conidx);
        }
    }
}

