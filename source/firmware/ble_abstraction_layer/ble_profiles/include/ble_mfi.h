/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * ble_mfi.h
 * - Bluetooth MFI service header
 * ------------------------------------------------------------------------- */

#ifndef BLE_MFI_H
#define BLE_MFI_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <ke_timer.h>
#include <gapm_task.h>
#include <am0_has_task.h>
#include <am0_has.h>
#include <am0_task.h>

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/

/** Invalid device index */
#define MFI_INVALID_DEV_IDX                    -1

/* Supported maximal packet size transferred over the air */
#define MFI_SUP_MAX_PACKET_SIZE                 60

/** Maximum length of Hearing Aid Service identifier */
#define MFI_AM0_IDENTIFIER_MAX_LEN              20

/** Maximum length of certificate data */
#define MFI_AM0_CERT_DATA_MAX_LEN               (1024)

/** Maximum length of private key */
#define MFI_AM0_CERT_PRIV_KEY_MAX_LEN           (64 * 5)

/** Length of each certificate data part */
#define MFI_CERT_DATA_PART_LEN                  512

/** Kernel timer coefficient to convert seconds to miliseconds */
#define MFI_KETIMER_S_TO_MS                     100

/** MFI Service UUID */
#define MFI_SERVICE_UUID                        HAS_HEARING_AID_SERVICE_UUID

/** MFI Service UUID length */
#define MFI_SERVICE_UUID_LEN                    16

/* MFI return codes */
#define MFI_ERROR_OK                            0   /** No error */
#define MFI_ERROR_INVALID_PARAMETER            -1   /** Invalid parameter */

/* MFI hearing aid left / right side */
#define MFI_HEARING_AID_LEFT                    0   /** Left side */
#define MFI_HEARING_AID_RIGHT                   1   /** Right side */

/* Audio stream  mode */
#define MFI_CAP_MONO                            0   /** Mono stream */
#define MFI_CAP_STEREO                          1   /** Stereo stream */

/* Minimal connection interval */
#define MFI_CONN_INTV_MIN                       24

/* Maximal connection interval */
#define MFI_CONN_INTV_MAX                       48

/* Latency */
#define MFI_CONN_LATENCY                        3

/* Supervision timeout */
#define MFI_CONN_TIME_OUT                       72

/* Minimum Connection Event Duration (not used by slave device) */
#define MFI_CONN_EVENT_DUR_MIN                  0xFFFF

/* Maximum Connection Event Duration (not used by slave device) */
#define MFI_CONN_EVENT_DUR_MAX                  0xFFFF

/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/

/** MFI Authentication */
typedef enum _MFI_Authentication_t
{
    MFI_SW_AUTHENTICATION = 0,          /* Software */
    MFI_HW_AUTHENTICATION,              /* Hardware */
} MFI_Authentication_t;

/* Read only properties of MFI */
typedef struct _MFI_ReadOnlyProperties_t
{
    const uint8_t *id;                  /* Hearing aid identifier */
    const uint16_t id_len;              /* Hearing aid identifier length */
    const uint8_t *other_id;            /* Other ear hearing aid identifier */
    const uint16_t other_id_len;        /* Other ear hearing aid identifier length */
    const uint8_t *cert_priv_key;       /* Private key */
    const uint16_t cert_priv_key_len;   /* Private key length */
    const uint8_t *cert_data;           /* Data for certificate A and certificate B */
    const uint16_t cert_data_len;       /* Length of data for certificate A and certificate B */
    const uint8_t *supported_codecs;    /* List of supported codecs */
    const uint8_t supported_codecs_len; /* Length of supported codecs list */
    uint32_t lea_version;               /* Current Version */
    uint32_t ava_progs_version;         /* Available programs version */
    uint16_t support_opt_char;          /* Optional characteristics support (1 - supported, 0 - not supported) */
    uint8_t  capabilities;              /* Capabilities */
    MFI_Authentication_t auth_type;     /* Authentication_type */
} MFI_ReadOnlyParameters_t;

/* Audio program information */
typedef struct _MFI_AudioProgramTag_t
{
    uint8_t name_len;                       /* Program name length */
    uint8_t name[AM0_PROG_NAME_MAX_LEN];    /* Program name */
    uint8_t category;                       /* Program category */
} MFI_AudioProgramTag_t;

/* Audio control parameters */
typedef struct _MFI_ControlParameters_t
{
    struct
    {
        uint16_t prog_name_len_max;     /* Program name maximum length */
        uint8_t sel_prog_num;           /* Selected program number */
        uint8_t avl_prog_num;           /* Number of available programs */
        MFI_AudioProgramTag_t *progs;   /* Programs information */
    } programs;
    struct
    {
        uint8_t  ear;                   /* Left or right ear */
        uint8_t  mic_volume;            /* Microphone volume (range [0:255] 0 = mute) */
        uint8_t  sec_stream_volume;     /* 2nd stream volume (range [0:255] 0 = mute) */
        uint8_t  active_prog_id;        /* Active program identifier */
        uint8_t  selected_prog_id;      /* Selected program identifeir */
        uint8_t  act_stream_prog_id;    /* Active streaming program identifier */
        uint8_t  substantially_diff;    /* Substantially different (if optional characteristics are supported) */
        uint8_t  mic_vol_step;          /* Microphone volume step size (if optional characteristics are supported) */
        uint8_t  stream_vol_step;       /* Stream volume vtep size (if optional characteristics are supported) */
        uint8_t  sensitivity_step;      /* Sensitivity step size (if optional characteristics are supported) */
        uint8_t  mixed_vol;             /* Mixed volume (if optional characteristics are supported) */
        uint8_t  mixed_vol_step;        /* Mixed volume Step (if optional characteristics are supported) */
        uint8_t  mic_sensitivity;       /* Microphone sensitivity (if optional characteristics are supported) */
        uint8_t  mic_preference;        /* Microphone preference */
        uint8_t  mic_presence;          /* Microphone presence */
        int8_t   bass;                  /* Bass (if optional characteristics are supported) */
        int8_t   treble;                /* Treble (if optional characteristics are supported) */
        uint8_t  volume;          /* Volume_level */
    } audio;
    struct
    {
        uint16_t codec_delay;           /* Audio codec delay (in us) */
        uint8_t  selected_codec_id;     /* Selected codec identifier */
    } codec;
    struct
    {
        bool     bidirectional_sup;     /* Bidirectional support */
        uint8_t  selected_max_pkt_size; /* Selected max packet size supported */
        uint8_t  selected_con_interval; /* Selected connection interval */
        uint8_t  selected_content;      /* Selected audio content*/
        uint16_t channel_delay;         /* Channel delay (in us) */
        bool     channel_cfg;           /* Channel configuration */
        bool     stream_mode;           /* Stream mode */
    } transSet;
    struct
    {
        uint8_t  lea_init_timer;        /* Timer for LEA initialization */
        uint8_t  state;                 /* LEA application state */
        int device_indx;                /* LEA device index */
    } control;
    struct
    {
        uint8_t battery_lvl;            /* Current Battery Level (range: 0-10) */
    } battery;
} MFI_ControlParameters_t;

/** MFI profile events that need to be handled by the application code */
typedef enum _MFI_Command_t
{
    MFI_STREAM_START_IND,               /* Stream start indicator */
    MFI_STREAM_STOP_IND,                /* Stream stop indicator */
    MFI_HAS_SELECTED_PROG_NAME_UPD,     /* Selected program name update */
    MFI_VOLUME_UPD,                     /* Volume update */
    MFI_HAS_MIC_VOLUME_UPD,             /* Mic volume update */
    MFI_HAS_2ND_STREAM_VOLUME_UPD,      /* Second stream volume update */
    MFI_HAS_ACTIVE_PROG_ID_UPD,         /* Active program identifier update */
    MFI_HAS_SELECTED_PROG_ID_UPD,       /* Selected program identifier update */
    MFI_HAS_SELECTED_PROG_CAT_UPD,      /* Selected program category update */
    MFI_HAS_MIXED_VOL_UPD,              /* Mixed volume update */
    MFI_HAS_BASS_UPD,                   /* Bass update */
    MFI_HAS_TREBLE_UPD,                 /* Treble update */
    MFI_HAS_ACT_STREAM_PROG_ID_UPD,     /* Active stream program identifier update */
    MFI_HAS_MICROPHONE_PREFERENCE_UPD,  /* Microphone preference update */
} MFI_Command_t;

/** MFI State */
typedef enum _LEA_state_t
{
    LEA_DUMMY = 0,                      /* LEA dummy state */
    LEA_UNCONNECTED,                    /* LEA is waiting for initialization */
    LEA_INIT,                           /* LEA initialization is ongoing */
    LEA_DEV_CONNECTED,                  /* LEA device is connected */
    LEA_READY,                          /* LEA is ready */
    LEA_STREAMING                       /* LEA is streaming */
} LEA_State_t;

/** MFI Device state */
typedef enum _MFI_deviceState_t
{
    MFI_DEV_INVALID = 0,                /* Invalid device */
    MFI_DEV_CONNECTED,                  /* Device connected */
    MFI_DEV_VALID_IDX,                  /* Valid device index */
    MFI_DEV_VALID_STATE,                /* Valid LEA state */
} MFI_DeviceState_t;

/* Hearing Aid Service configuration - redefinition */
typedef enum am0_has_char MFI_HasChar_t;

/* Hearing Aid Service pairing mode - redefinition */
typedef enum am0_has_mode MFI_PairingMode_t;

/* MFI configuration structure */
typedef struct _MFI_Config_t
{
    const MFI_ReadOnlyParameters_t *readOnlyProp;   /* Read only parameters pointer */
    MFI_ControlParameters_t *controlParams;         /* Contrlol parameters pointer */
} MFI_Config_t;

/* MFI application callback function */
typedef void (*MFI_Callback)(MFI_Command_t);

/* Service internal environment structure */
typedef struct _MFI_Env_t
{
    const MFI_ReadOnlyParameters_t *readOnlyProp;   /* MFI read only properties */
    MFI_ControlParameters_t *ctrlParams;            /* Control parameters */
    bool service_added;                             /* Flag indicating that a service has been added */
    MFI_Callback mfiCallback;                       /* Application callback function */
    struct gapm_start_advertise_cmd advertiseCmd;   /* Advertisement command configuration */

} MFI_Env_t;

/* ----------------------------------------------------------------------------
 * Function prototypes
 * --------------------------------------------------------------------------*/

/* Function initializing MFI profile */
uint8_t MFI_Initialize(MFI_Config_t *db, void (*mfiCallback)(MFI_Command_t));

/** Function returning environment variable */
const MFI_Env_t* MFI_GetEnv(void);

/** Function returning flag indicating if profile was added */
bool MFI_IsAdded(void);

/** Set pairing mode */
void MFI_SetPairingMode(MFI_PairingMode_t mode);

/** Function notifying connected devices about value update */
void MFI_NotifyAboutUpdatedValue(MFI_HasChar_t am0_has_char);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* BLE_MFI_H */
