# -*- coding: utf-8 -*-
"""This script updates the BLE abstraction layer files to all the
sample application folders that contain the 'ble' and 'ble_profiles'.
For a file to be updates, it first has to exist in the sample app.
It will also ignore folders that have 'Copy|copy' and '.BAK|.bak'
on their names.
"""

from __future__ import print_function, unicode_literals

import fnmatch
import os
import re
import argparse
import filecmp

from shutil import copyfile

def get_abstraction_files():
    abstraction_files = []
    for root, dirnames, filenames in os.walk('ble_profiles'):
        for filename in fnmatch.filter(filenames, '*.[ch]'):
            abstraction_files.append(os.path.join(root, filename))

    for root, dirnames, filenames in os.walk('ble'):
        for filename in fnmatch.filter(filenames, '*.[ch]'):
            abstraction_files.append(os.path.join(root, filename))

    return abstraction_files

def get_samples_folders(samples_names=[]):
    ble_matches = []
    for root, dirnames, filenames in os.walk('..\\samples\\'):
        for dirname in fnmatch.filter(dirnames, 'ble'):
            if root.startswith('..\\samples\\'):
                ble_matches.append(root[11:])

    for root, dirnames, filenames in os.walk('..\\samples\\'):
        for dirname in fnmatch.filter(dirnames, 'ble_profiles'):
            if root.startswith('..\\samples\\') and \
                    root[11:] not in ble_matches:
                ble_matches.append(root[11:])

    # re.IGNORECASE didn't work, so...
    regex = re.compile('(?:\\\\|[Cc]opy|\.bak|\.BAK)')
    # This filters out the Debug and Release folders
    list1 = filter(lambda i: not regex.search(i), ble_matches)

    if len(samples_names):
        return list(set(list1).intersection(samples_names))
    else:
        return list1

def update_samples(abstraction_files, samples_folders):
    for sample_folder in samples_folders:
        updated_files = []
        for abs_file in abstraction_files:
            dest_file = '..\\samples\\'+sample_folder+'\\'+abs_file
            if os.path.isfile(dest_file) and not filecmp.cmp(abs_file, dest_file, shallow=False):
                copyfile(abs_file, dest_file)
                updated_files.append(abs_file)
        if len(updated_files):
            print('Updated {0} : {1}'.format(sample_folder, [str(x) for x in updated_files]))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Updates the BLE abstraction \
            code in the sample applications that use it.')
    parser.add_argument('sample_apps', metavar='sample app', nargs='*',
                       help='optional list of sample apps to be updated.')

    args = parser.parse_args()

    abstraction_files = get_abstraction_files()

    samples_folders = get_samples_folders(args.sample_apps)

    update_samples(abstraction_files, samples_folders)
