/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 * ----------------------------------------------------------------------------
 * stubprf.c
 * - Empty definition of profile related functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2019/10/29 18:13:35 $
 * ------------------------------------------------------------------------- */

#ifndef CFG_PRF_HTPT
const struct prf_task_cbs* htpt_prf_itf_get(void);
const struct prf_task_cbs* htpt_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_HTPC
const struct prf_task_cbs* htpc_prf_itf_get(void);
const struct prf_task_cbs* htpc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_DISS
const struct prf_task_cbs* diss_prf_itf_get(void);
const struct prf_task_cbs* diss_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_DISC
const struct prf_task_cbs* disc_prf_itf_get(void);
const struct prf_task_cbs* disc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_BLPS
const struct prf_task_cbs* blps_prf_itf_get(void);
const struct prf_task_cbs* blps_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_BLPC
const struct prf_task_cbs* blpc_prf_itf_get(void);
const struct prf_task_cbs* blpc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_TIPS
const struct prf_task_cbs* tips_prf_itf_get(void);
const struct prf_task_cbs* tips_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_TIPC
const struct prf_task_cbs* tipc_prf_itf_get(void);
const struct prf_task_cbs* tipc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_HRPS
const struct prf_task_cbs* hrps_prf_itf_get(void);
const struct prf_task_cbs* hrps_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_HRPC
const struct prf_task_cbs* hrpc_prf_itf_get(void);
const struct prf_task_cbs* hrpc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_FMPL
const struct prf_task_cbs* findl_prf_itf_get(void);
const struct prf_task_cbs* findl_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_FMPT
const struct prf_task_cbs* findt_prf_itf_get(void);
const struct prf_task_cbs* findt_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_PXPM
const struct prf_task_cbs* proxm_prf_itf_get(void);
const struct prf_task_cbs* proxm_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_PXPR
const struct prf_task_cbs* proxr_prf_itf_get(void);
const struct prf_task_cbs* proxr_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_SCPPC
const struct prf_task_cbs* scppc_prf_itf_get(void);
const struct prf_task_cbs* scppc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_SCPPS
const struct prf_task_cbs* scpps_prf_itf_get(void);
const struct prf_task_cbs* scpps_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_BASC
const struct prf_task_cbs* basc_prf_itf_get(void);
const struct prf_task_cbs* basc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_BASS
const struct prf_task_cbs* bass_prf_itf_get(void);
const struct prf_task_cbs* bass_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_HOGPD
const struct prf_task_cbs* hogpd_prf_itf_get(void);
const struct prf_task_cbs* hogpd_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_HOGPDh
const struct prf_task_cbs* hogpbh_prf_itf_get(void);
const struct prf_task_cbs* hogpbh_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_HOGPRH
const struct prf_task_cbs* hogprh_prf_itf_get(void);
const struct prf_task_cbs* hogprh_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_GLPC
const struct prf_task_cbs* glpc_prf_itf_get(void);
const struct prf_task_cbs* glpc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_GLPS
const struct prf_task_cbs* glps_prf_itf_get(void);
const struct prf_task_cbs* glps_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_RSCPC
const struct prf_task_cbs* rscpc_prf_itf_get(void);
const struct prf_task_cbs* rscpc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_RSCPS
const struct prf_task_cbs* rscps_prf_itf_get(void);
const struct prf_task_cbs* rscps_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_CSCPC
const struct prf_task_cbs* cscpc_prf_itf_get(void);
const struct prf_task_cbs* cscpc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_CSCPS
const struct prf_task_cbs* cscps_prf_itf_get(void);
const struct prf_task_cbs* cscps_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_ANPC
const struct prf_task_cbs* anpc_prf_itf_get(void);
const struct prf_task_cbs* anpc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_ANPS
const struct prf_task_cbs* anps_prf_itf_get(void);
const struct prf_task_cbs* anps_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_PASPC
const struct prf_task_cbs* paspc_prf_itf_get(void);
const struct prf_task_cbs* paspc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_PASPS
const struct prf_task_cbs* pasps_prf_itf_get(void);
const struct prf_task_cbs* pasps_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_CPPC
const struct prf_task_cbs* cppc_prf_itf_get(void);
const struct prf_task_cbs* cppc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_CPPS
const struct prf_task_cbs* cpps_prf_itf_get(void);
const struct prf_task_cbs* cpps_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_LANC
const struct prf_task_cbs* lanc_prf_itf_get(void);
const struct prf_task_cbs* lanc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_LANS
const struct prf_task_cbs* lans_prf_itf_get(void);
const struct prf_task_cbs* lans_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_IPSS
const struct prf_task_cbs* ipss_prf_itf_get(void);
const struct prf_task_cbs* ipss_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_IPSC
const struct prf_task_cbs* ipsc_prf_itf_get(void);
const struct prf_task_cbs* ipsc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_ENVS
const struct prf_task_cbs* envs_prf_itf_get(void);
const struct prf_task_cbs* envs_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_ENVC
const struct prf_task_cbs* envc_prf_itf_get(void);
const struct prf_task_cbs* envc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_WSCS
const struct prf_task_cbs* wscs_prf_itf_get(void);
const struct prf_task_cbs* wscs_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_WSCC
const struct prf_task_cbs* wscc_prf_itf_get(void);
const struct prf_task_cbs* wscc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_BCSS
const struct prf_task_cbs* bcss_prf_itf_get(void);
const struct prf_task_cbs* bcss_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_BCSC
const struct prf_task_cbs* bcsc_prf_itf_get(void);
const struct prf_task_cbs* bcsc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_WPTS
const struct prf_task_cbs* wpts_prf_itf_get(void);
const struct prf_task_cbs* wpts_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_WPTC
const struct prf_task_cbs* wptc_prf_itf_get(void);
const struct prf_task_cbs* wptc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_PLXS
const struct prf_task_cbs* plxs_prf_itf_get(void);
const struct prf_task_cbs* plxs_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_PLXC
const struct prf_task_cbs* plxc_prf_itf_get(void);
const struct prf_task_cbs* plxc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_CGMS
const struct prf_task_cbs* cgms_prf_itf_get(void);
const struct prf_task_cbs* cgms_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_CGMC
const struct prf_task_cbs* cgmc_prf_itf_get(void);
const struct prf_task_cbs* cgmc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef BLE_AM0_HEARING_AID_SERV
const struct prf_task_cbs* am0_has_prf_itf_get(void);
const struct prf_task_cbs* am0_has_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_UDSS
const struct prf_task_cbs* udss_prf_itf_get(void);
const struct prf_task_cbs* udss_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_PRF_UDSC
const struct prf_task_cbs* udsc_prf_itf_get(void);
const struct prf_task_cbs* udsc_prf_itf_get(void)
{
    return 0;
}
#endif

#ifndef CFG_BLE_MESH
const struct prf_task_cbs* m_al_prf_itf_get(void);
const struct prf_task_cbs* m_al_prf_itf_get(void)
{
    return 0;
}
#endif
//To resolve issue CEVA-231
