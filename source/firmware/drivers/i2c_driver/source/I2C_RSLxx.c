/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * I2C_RSLxx.c
 * - I2C CMSIS-Driver implementation for RSLxx family of devices
 * ----------------------------------------------------------------------------
 * $Revision: 1.4 $
 * $Date: 2019/08/15 17:53:49 $
 * ------------------------------------------------------------------------- */

#include <GPIO_RSLxx.h>
#include <DMA_RSLxx.h>
#include <I2C_RSLxx.h>

#if RTE_I2C

#define ARM_I2C_DRV_VERSION    ARM_DRIVER_VERSION_MAJOR_MINOR(1, 0)

/* Driver Version */
static const ARM_DRIVER_VERSION DriverVersion =
{
    ARM_I2C_API_VERSION,
    ARM_I2C_DRV_VERSION
};

/* I2C0 runtime information */
static I2C_INFO_t I2C0_Info = {
    .default_cfg = {                                               /* i2c0 default configuration */
        .sda_pin             = RTE_I2C0_SDA_PIN_DEFAULT,           /* i2c0 default sda pin identifier */
        .scl_pin             = RTE_I2C0_SCL_PIN_DEFAULT,           /* i2c0 default scl pin identifier */
#if (I2C_DMA_CODE_EN)
        .dma_ch              = RTE_I2C0_DMA_CH_DEFAULT,            /* i2c0 default dma channel */
#endif /* if (I2C_DMA_CODE_EN) */
        .pri_cfg = {                                               /* i2c0 default priority configuration */
            .preempt_pri     = RTE_I2C_INT_PREEMPT_PRI_DEFAULT,    /* i2c0 default pre-empt priority value */
            .subgrp_pri      = RTE_I2C_INT_PREEMPT_PRI_DEFAULT     /* i2c0 default subgroup priority value */
        }                                                          
    },
    .status                  = { 0 },                              /* i2c0 current status */
    .state                   = 0,                                  /* i2c0 current flags */
    .prescale                = 0,                                  /* i2c0 current mode */
#if (I2C_DMA_CODE_EN)
    .dma_rx_cfg              = 0,                                  /* dma channel receiver configuration */
    .dma_tx_cfg              = 0,                                  /* dma channel transmitter configuration */
    .dma_busy                = 0                                   /* !!!WORKAROUND!!! for dma rx counter not being
                                                                    * reset before new transmission */
#endif /* if (I2C_DMA_CODE_EN) */
};

static I2C_INT_INFO_t I2C0_intInfo = {                             /* i2c0 interrupt info */
    .irqn                    = I2C_IRQn,                           /* i2c0 rx interrupt number */
    .cb                      = 0,                                  /* i2c0 callback function pointer */
};

/* I2C0 transfer information */
static I2C_TRANSFER_INFO_t I2C0_TransferInfo = { 0 };              /* transfer info */

/* Driver Capabilities */
static const ARM_I2C_CAPABILITIES DriverCapabilities = { 0 };

/* I2C0 Resources */
static const I2C_RESOURCES_t I2C0_Resources =                      /* i2c0 resources */
{
    .reg                     = I2C,                                /* i2c0 hw registers */
    .intInfo                 = &I2C0_intInfo,                      /* i2c0 interrupt info */
    .info                    = &I2C0_Info,                         /* i2c0 runtime info */
    .xfer                    = &I2C0_TransferInfo                  /* i2c0 transfer info*/
};

/* Prepare gpio driver pointer */
static DRIVER_GPIO_t *gpio = &Driver_GPIO;

/* Prepare sda pin configuration */
static GPIO_PAD_CFG_t gpioCfg = {
    .pull_mode     = GPIO_STRONG_PULL_UP,
    .drive_mode    = GPIO_6X,
    .lpf_en        = GPIO_LPF_ENABLE,
    .io_mode       = GPIO_MODE_I2C_SDA
};

#if (I2C_DMA_CODE_EN)
/* Prepare dma driver pointer */
static DRIVER_DMA_t *dma = &Driver_DMA;
#endif /* if (I2C_DMA_CODE_EN) */

#if (I2C_CM3_CODE_EN)
/* ----------------------------------------------------------------------------
 * Function      : void I2Cx_MasterIRQHandler (const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a byte is transferred.
 * Inputs        : i2c    - Pointer to i2c resources
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void I2Cx_MasterIRQHandler (const I2C_RESOURCES_t *i2c)
{
    uint32_t event = 0U;
    uint32_t i2c_status = Sys_I2C_Get_Status();

    /* Check if driver is busy */
    if (i2c->info->status.busy)
    {
        /* Check if error occured */
        if (i2c_status & I2C_BUS_ERROR)
        {
            /* Indicate error */
            i2c->info->status.bus_error = true;
            event |= ARM_I2C_EVENT_BUS_ERROR | ARM_I2C_EVENT_ARBITRATION_LOST;

            /* Reset the peripheral to a known state */
            Sys_I2C_Reset();
        }

        /* Check if stop was detected */
        else if (i2c_status & I2C_STOP_DETECTED)
        {
            /* Stop detected, finish transfer */
            if (i2c->xfer->cnt == i2c->xfer->num)
            {
                /* Indicate transfer done */
                event |= ARM_I2C_EVENT_TRANSFER_DONE;
            }
            else
            {
                /* Indicate transfer incomplete */
                event |= ARM_I2C_EVENT_TRANSFER_INCOMPLETE;

                if (i2c->xfer->cnt == 0)
                {
                    /* Indicate address nack */
                    event |= ARM_I2C_EVENT_ADDRESS_NACK;
                }
            }
            i2c->info->status.busy = I2C_FREE;
        }
        else if (i2c_status & I2C_IS_READ)
        {
            /* READ mode, If buffer full put a new data on RX buffer. When receive
             * the number of byte expected, send NACK and Stop. */
            if (i2c_status & I2C_BUFFER_FULL)
            {
                /* Check the data counter */
                if (i2c->xfer->cnt < (i2c->xfer->num - 1))
                {
                    /* Send ACK*/
                    Sys_I2C_ACK();
                }
                else
                {
                    /* Send NACK */
                    Sys_I2C_NACKAndStop();
                }

                /* Read new data */
                i2c->xfer->data[i2c->xfer->cnt++] = I2C->DATA;
            }

            /* If Data Event is set send an ACK to start the read */
            else if (i2c_status & I2C_DATA_EVENT)
            {
                /* Send ACK*/
                Sys_I2C_ACK();
            }
        }
        else
        {
            /* If WRITE mode, send the next byte.
             * If all bytes from the buffer are sent, set LAST_DATA bit */
            if ((i2c_status & (1 << I2C_STATUS_ACK_STATUS_Pos)) == I2C_HAS_ACK)
            {
                /* Check the data counter */
                if (i2c->xfer->cnt < i2c->xfer->num)
                {
                    /* Send new data */
                    I2C->DATA = i2c->xfer->data[i2c->xfer->cnt++];
                }

                /* Check the data counter */
                if (i2c->xfer->cnt == i2c->xfer->num)
                {
                    /* Set last data flag */
                    I2C_CTRL1->LAST_DATA_ALIAS = I2C_LAST_DATA_BITBAND;
                }
            }
        }
    }

    /* Check if an event happened and the application registered a callback */
    if (event && i2c->intInfo->cb)
    {
        /* Call the callback function */
        i2c->intInfo->cb(event);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void I2Cx_SlaveIRQHandler (const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a byte is transferred.
 * Inputs        : i2c    - Pointer to i2c resources
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void I2Cx_SlaveIRQHandler (const I2C_RESOURCES_t *i2c)
{
    uint32_t event = 0U;
    uint32_t i2c_status = Sys_I2C_Get_Status();

    /* Check if device is busy */
    if (i2c->info->status.busy)
    {
        /* Check if bus error occured */
        if (i2c_status & I2C_BUS_ERROR)
        {
            /* Indicate bust error */
            i2c->info->status.bus_error = true;
            event |= ARM_I2C_EVENT_BUS_ERROR;

            /* Reset the peripheral to a known state */
            Sys_I2C_Reset();
        }

        /* Check if address was send */
        else if (i2c_status & I2C_DATA_IS_ADDR)
        {
            /* If direction matches with the operation the application is waiting for */
            if ((i2c_status & (1 << I2C_STATUS_READ_WRITE_Pos)) ^
                (i2c->info->status.direction << I2C_STATUS_READ_WRITE_Pos))
            {
                /* Slave I2C selected. Send an ACK and reset the counter */
                Sys_I2C_ACK();

                /* If general call, notify application */
                if (i2c_status & I2C_ADDR_GEN_CALL)
                {
                    /* Indicate general call */
                    event |= ARM_I2C_EVENT_GENERAL_CALL;
                    i2c->info->status.general_call = true;
                }

                /* Reset data counter */
                i2c->xfer->cnt = 0;

                /*  Indicate transmit / receive event */
                event |= (i2c_status & I2C_IS_READ) ? ARM_I2C_EVENT_SLAVE_TRANSMIT : ARM_I2C_EVENT_SLAVE_RECEIVE;
            }
            else
            {
                /* Send NACK */
                Sys_I2C_NACK();
            }
        }

        /* Check if data event occured */
        else if (i2c_status & I2C_DATA_EVENT)
        {
            /* Data event:
             *  - When in write mode send an ACK and read data written by Master.
             *  - When in read mode and NACK status not set send a new byte. */

            /* Check data counter */
            if (i2c->xfer->cnt < i2c->xfer->num)
            {
                /* Check the write event */
                if ((i2c_status & (1 << I2C_STATUS_READ_WRITE_Pos)) == I2C_IS_WRITE)
                {
                    /* Send ACK */
                    Sys_I2C_ACK();

                    /* Fill buffer with received data */
                    i2c->xfer->data[i2c->xfer->cnt++] = I2C->DATA;
                }

                /* Check the ACK event */
                else if ((i2c_status & (1 << I2C_STATUS_ACK_STATUS_Pos)) == I2C_HAS_ACK)
                {
                    /* Send next byte */
                    I2C->DATA = i2c->xfer->data[i2c->xfer->cnt++];
                }
            }
            else
            {
                /* NACK and notify transfer done */
                Sys_I2C_NACK();
                event |= ARM_I2C_EVENT_TRANSFER_DONE;

                /* Indicate device is free */
                i2c->info->status.busy = I2C_FREE;
            }
        }

        /* Check if stop was detected */
        else if (i2c_status & I2C_STOP_DETECTED)
        {
            /* Stop detected, finish transfer */
            if (i2c->xfer->cnt == i2c->xfer->num)
            {
                /* Indicate transfer done event */
                event |= ARM_I2C_EVENT_TRANSFER_DONE;
            }
            else
            {
                /* Indicate transfer incomplete and bus error */
                event |= ARM_I2C_EVENT_TRANSFER_INCOMPLETE | ARM_I2C_EVENT_BUS_ERROR;
            }

            /* Indicate device is free */
            i2c->info->status.busy = I2C_FREE;
        }
    }

    /* Check if an event happened and the application registered a callback */
    if (event && i2c->intInfo->cb)
    {
        /* Call the callback function */
        i2c->intInfo->cb(event);
    }
}

#else  /* if (I2C_CM3_CODE_EN) */
/* ----------------------------------------------------------------------------
 * Function      : void I2C_DMAHandler(uint32_t event, I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Called by dma driver when a dma transfer is done.
 *                 Manage flags and counters. Application is notified via
 *                 callback function once the transmission is complete
 * Inputs        : i2c    - i2c instance
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void I2C_DMAHandler(uint32_t event, I2C_RESOURCES_t *i2c)
{
    /* Prepare status description */
    event = 0;

    /* Get the dma status */
    DMA_STATUS_t status = dma->GetStatus(i2c->info->default_cfg.dma_ch);

    if (status.completed)
    {
        /* Disable i2c interrupts */
        NVIC_DisableIRQ(I2C0_Resources.intInfo->irqn);

        /* Stop the dma */
        dma->Stop(i2c->info->default_cfg.dma_ch);

        /* Check if mode == master */
        if (i2c->info->status.mode == I2C_STATUS_MODE_MASTER)
        {
            /* Check if direction == RX */
            if (i2c->info->status.direction == I2C_STATUS_DIRECTION_RX)
            {
                /* !!!WORKAROUND!!! There is a problem with DMA <-> I2C synchronization */
                /* Set last data bit */
                I2C_CTRL1->LAST_DATA_ALIAS = I2C_LAST_DATA_BITBAND;

                /* Wait for next data to arrive */
                while ((i2c->reg->STATUS & I2C_DATA_EVENT)){}

                /* Get counter value */
                i2c->xfer->cnt = dma->GetCounterValue(i2c->info->default_cfg.dma_ch);

                /* Read last data */
                i2c->xfer->data[i2c->xfer->cnt] = i2c->reg->DATA;
            }
            else
            {
                /* Set last data bit */
                I2C_CTRL1->LAST_DATA_ALIAS = I2C_LAST_DATA_BITBAND;
            }
        }

        /* Clear send active flag */
        i2c->info->status.busy = I2C_FREE;

        /* Update status flag */
        event |= ARM_I2C_EVENT_TRANSFER_DONE;

        /* Clear pending i2c interrupts in nvic */
        NVIC_ClearPendingIRQ(i2c->intInfo->irqn);
    }

    /* Check if first word was received */
    if (status.started)
    {
        /* Indicate that first byte was received */
        i2c->info->dma_busy = 1;

        /* Check if i2c is in slave mode */
        if (i2c->info->status.mode == I2C_STATUS_MODE_SLAVE)
        {
            /* Check if i2c is in receive mode */
            if (i2c->info->status.direction == I2C_STATUS_DIRECTION_RX)
            {
                /* Update status flag */
                event |= ARM_I2C_EVENT_SLAVE_RECEIVE;
            }
            else
            {
                /* Update status flag */
                event |= ARM_I2C_EVENT_SLAVE_TRANSMIT;
            }
        }
    }

    /* Notify application */
    if (event && i2c->intInfo->cb)
    {
        i2c->intInfo->cb(event);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void I2Cx_MasterIRQHandler (const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a stop condition is detected.
 * Inputs        : i2c    - Pointer to i2c resources
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void I2Cx_MasterIRQHandler (const I2C_RESOURCES_t *i2c)
{
    uint32_t event = 0U;

    /* Indicate that i2c is free */
    i2c->info->status.busy = I2C_FREE;

    /* Stop the dma */
    dma->Stop(i2c->info->default_cfg.dma_ch);

    /* Get I2C status */
    uint32_t i2c_status = Sys_I2C_Get_Status();

    /* Get dma counter value */
    uint32_t dmaCounterValue = dma->GetCounterValue(i2c->info->default_cfg.dma_ch);

    /* Reset the peripheral to a known state */
    Sys_I2C_Reset();

    /* Check if error occured */
    if (i2c_status & I2C_BUS_ERROR)
    {
        /* Indicate error */
        i2c->info->status.bus_error = true;

        /* Set error in event return value */
        event |= ARM_I2C_EVENT_BUS_ERROR;

        /* Reset the peripheral to a known state */
        Sys_I2C_Reset();
    }
    else
    {
        /* Stop detected, finish transfer */
        if (dmaCounterValue == i2c->xfer->num)
        {
            /* Indicate transfer done */
            event |= ARM_I2C_EVENT_TRANSFER_DONE;
        }
        else
        {
            /* Indicate transfer incomplete */
            event |= ARM_I2C_EVENT_TRANSFER_INCOMPLETE;

            /* Check if dmaCounter == 0 */
            if (dmaCounterValue == 0)
            {
                /* Indicate adress nack event */
                event |= ARM_I2C_EVENT_ADDRESS_NACK;
            }
        }
    }

    /* Clear pending i2c interrupts in nvic */
    NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

    /* Check if callback function was registered */
    if (i2c->intInfo->cb)
    {
        /* Call the callback function */
        i2c->intInfo->cb(event);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void I2Cx_SlaveIRQHandler (const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a byte is transferred.
 * Inputs        : i2c    - Pointer to i2c resources
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void I2Cx_SlaveIRQHandler (const I2C_RESOURCES_t *i2c)
{
    uint32_t event = 0U;

    /* Stop the dma */
    dma->Stop(i2c->info->default_cfg.dma_ch);

    /* Get I2C status */
    uint32_t i2c_status = Sys_I2C_Get_Status();

    /* Reset the peripheral to a known state */
    Sys_I2C_Reset();

    /* Get dma counter value */
    uint32_t dmaCounterValue = dma->GetCounterValue(i2c->info->default_cfg.dma_ch);

    /* Check if error occured */
    if (i2c_status & I2C_BUS_ERROR)
    {
        /* Indicate error */
        i2c->info->status.bus_error = true;

        /* Set error in event return value */
        event |= ARM_I2C_EVENT_BUS_ERROR;

        /* Reset the peripheral to a known state */
        Sys_I2C_Reset();
    }
    else
    {
        /* Check if entire data was transferred */
        if (dmaCounterValue == i2c->xfer->num)
        {
            /* Indicate transfer done event */
            event |= ARM_I2C_EVENT_TRANSFER_DONE;
        }
        else
        {
            /* Indicate transfer incomplete */
            event |= ARM_I2C_EVENT_TRANSFER_INCOMPLETE;
        }
    }

    /* Indicate that i2c is free */
    i2c->info->status.busy = I2C_FREE;

    /* Clear pending i2c interrupts in nvic */
    NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

    /* Check if callback function was registered */
    if (i2c->intInfo->cb)
    {
        /* Call the callback function */
        i2c->intInfo->cb(event);
    }
}

#endif /* if (I2C_CM3_CODE_EN) */

/* ----------------------------------------------------------------------------
 * Function      : void I2Cx_GetVersion (void)
 * ----------------------------------------------------------------------------
 * Description   : Driver version
 * Inputs        : None
 * Outputs       : ARM_DRIVER_VERSION
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_DRIVER_VERSION I2Cx_GetVersion (void)
{
    /* Return driver version */
    return DriverVersion;
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_GetCapabilities (void)
 * ----------------------------------------------------------------------------
 * Description   : Get I2Cx driver capabilities
 * Inputs        : None
 * Outputs       : ARM_I2C_CAPABILITIES
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_I2C_CAPABILITIES I2Cx_GetCapabilities (void)
{
    /* Return i2c capabilities */
    return DriverCapabilities;
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_Initialize (ARM_I2C_SignalEvent_t cb,
 *                                      I2C_RESOURCES_t * i2c)
 * ----------------------------------------------------------------------------
 * Description   : Initialize i2c flags, gpio pins and reference to
 *                 callback function.
 * Inputs        : cb_event - pointer to callback function (optional)
 *                 i2c      - Pointer to i2c resources
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_Initialize (ARM_I2C_SignalEvent_t cb,
                               I2C_RESOURCES_t *i2c)
{
    /* Check if driver was initialized */
    if (i2c->info->state & I2C_INITIALIZED)
    {
        /* Driver is already initialized */
        return ARM_DRIVER_OK;
    }

    /* Change configuration to scl */
    gpioCfg.io_mode = GPIO_MODE_I2C_SDA;

    /* Configure sda pin */
    gpio->ConfigurePad(i2c->info->default_cfg.sda_pin, &gpioCfg);

    /* Change configuration to scl */
    gpioCfg.io_mode = GPIO_MODE_I2C_SCL;

    /* Configure scl pin */
    gpio->ConfigurePad(i2c->info->default_cfg.scl_pin, &gpioCfg);

    /* Reset runtime information structure */
    i2c->info->status   = (const ARM_I2C_STATUS) {
        0
    };
    i2c->info->state    = 0;
    i2c->info->prescale = 0;

    /* Initialize i2c runtime Resources */
    i2c->intInfo->cb = cb;
    i2c->info->state = I2C_INITIALIZED;

#if (I2C_DMA_CODE_EN)

    /* Prepare receiver mode dma configuration */
    DMA_CFG_t dmaCfgR = {
        .src_sel       = DMA_TRG_I2C,
        .src_step_mode = DMA_STEP_STATIC,
        .src_word_size = DMA_WORD_SIZE_8,
        .dst_sel       = DMA_TRG_MEM,
        .dst_step_mode = DMA_STEP_INC,
        .dst_word_size = DMA_WORD_SIZE_32,
        .byte_order    = DMA_ENDIANNESS_LITTLE,
        .ch_priority   = DMA_CH_PRI_0,
        .data_mode     = DMA_SINGLE
    };

    /* Configure the dma channel */
    dma->Configure(i2c->info->default_cfg.dma_ch, &dmaCfgR, I2C_DMA_Handler);

    /* Store the receiver dma configuration */
    i2c->info->dma_rx_cfg = dma->CreateConfigWord(&dmaCfgR);

    /* Prepare transmitter dma configuration */
    DMA_CFG_t dmaCfgT = {
        .src_sel       = DMA_TRG_MEM,
        .src_step_mode = DMA_STEP_INC,
        .src_word_size = DMA_WORD_SIZE_32,
        .dst_sel       = DMA_TRG_I2C,
        .dst_step_mode = DMA_STEP_STATIC,
        .dst_word_size = DMA_WORD_SIZE_8,
        .byte_order    = DMA_ENDIANNESS_LITTLE,
        .ch_priority   = DMA_CH_PRI_0,
        .data_mode     = DMA_SINGLE
    };

    /* Store the transmitter dma configuration */
    i2c->info->dma_tx_cfg = dma->CreateConfigWord(&dmaCfgT);
#endif /* if (I2C_DMA_CODE_EN) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_Uninitialize (const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Uninitialize i2c flags, gpio pins and removes reference to
 *                 callback function.
 * Inputs        : i2c - Pointer to i2c resources
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_Uninitialize (const I2C_RESOURCES_t *i2c)
{
#if (I2C_DMA_CODE_EN)

    /* Stop the dma data transfer */
    dma->Stop(i2c->info->default_cfg.dma_ch);
#endif /* if (I2C_DMA_CODE_EN) */

    /* Prepare sda / scl pin configuration */
    const GPIO_PAD_CFG_t gpioLocCfg = { .io_mode = GPIO_MODE_DISABLED };

    /* Reset sda pin */
    gpio->ConfigurePad(i2c->info->default_cfg.sda_pin, &gpioLocCfg);

    /* Reset scl pin */
    gpio->ConfigurePad(i2c->info->default_cfg.scl_pin, &gpioLocCfg);

    /* Reset i2c alternative function register */
    gpio->ResetAltFuncRegister(GPIO_FUNC_REG_I2C);

    /* Clear I2C state */
    i2c->info->state = 0U;

    /* Remove reference to callback function */
    i2c->intInfo->cb = NULL;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_PowerControl (ARM_POWER_STATE state,
 *                                       const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Operate the power modes of the I2C interface
 * Inputs        : state - ARM_POWER_FULL or ARM_POWER_OFF
 *                 i2c - Pointer to I2C resources
 * Outputs       : ARM_DRIVER_OK - if the operation is successful
 *                 ARM_DRIVER_ERROR_UNSUPPORTED - if argument is ARM_POWER_LOW
 *                                                or an invalid value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_PowerControl (ARM_POWER_STATE state,
                                 const I2C_RESOURCES_t *i2c)
{
    /* Check which operation is to be performed */
    switch (state)
    {
        case ARM_POWER_OFF:
        {
            /* Disable i2c interrupts */
            NVIC_DisableIRQ(i2c->intInfo->irqn);

            /* Clear pending i2c interrupts in nvic */
            NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

            /* Disable i2c clk */
            i2c->reg->CTRL0 = I2C_SAMPLE_CLK_DISABLE;

            /* Clear status flags */
            i2c->info->status = (const ARM_I2C_STATUS)
            {
                0
            };

            /* Clear powered flag */
            i2c->info->state &= ~I2C_POWERED;
            break;
        }

        case ARM_POWER_FULL:
        {
            /* Check if i2c was initialized */
            if ((i2c->info->state & I2C_INITIALIZED) == 0U)
            {
                /* Return error */
                return ARM_DRIVER_ERROR;
            }

            /* Check if i2c was already powered up */
            if ((i2c->info->state & I2C_POWERED) != 0U)
            {
                /* Return OK */
                return ARM_DRIVER_OK;
            }

            /* Clear pending i2c interrupts in nvic */
            NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

            /* Enable i2c interrupt */
            NVIC_EnableIRQ(i2c->intInfo->irqn);

#if (I2C_CM3_CODE_EN)

            /* Enable I2C clock and configure it to be controlled by CM3 */
            i2c->reg->CTRL0 = (I2C_CONTROLLER_CM3 | I2C_SAMPLE_CLK_ENABLE
                               | I2C_AUTO_ACK_DISABLE | I2C_STOP_INT_ENABLE);
#else  /* if (I2C_CM3_CODE_EN) */

            /* Enable I2C clock and configure it to be controlled by dma */
            i2c->reg->CTRL0 = (I2C_CONTROLLER_DMA | I2C_SAMPLE_CLK_ENABLE
                               | I2C_AUTO_ACK_ENABLE | I2C_STOP_INT_ENABLE);
#endif /* if (I2C_CM3_CODE_EN) */

            /* Reset the peripheral to a known state */
            Sys_I2C_Reset();

            /* Set powered flag */
            i2c->info->state |= I2C_POWERED;
            break;
        }

        case ARM_POWER_LOW:
        default:

            /* Return unsupported operation error */
            return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t I2C_SetInterruptPriority(I2C_RESOURCES_t * i2c)
 * ----------------------------------------------------------------------------
 * Description   : Configure i2c interrupts priority.
 * Inputs        : i2c - i2c instance
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_SetInterruptPriority(I2C_RESOURCES_t *i2c)
{
    /* Encoded priority value */
    uint32_t encodedPri = 0;

    /* Encode priority configuration word */
    encodedPri = NVIC_EncodePriority(NVIC_GetPriorityGrouping(), i2c->info->default_cfg.pri_cfg.preempt_pri,
                                     i2c->info->default_cfg.pri_cfg.subgrp_pri);

    /* Set the i2c interrupt priority */
    NVIC_SetPriority(i2c->intInfo->irqn, encodedPri);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t I2C_MasterTransmit (uint32_t addr,
 *                                             const uint8_t *data,
 *                                             uint32_t num, bool xfer_pending,
 *                                             const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Start transmitting data as I2C Master.
 * Inputs        : addr         - slave address (7-bit)
 *                 data         - pointer to data buffer to send to i2c slave
 *                 num          - number of data bytes to send
 *                 xfer_pending - transfer operation is pending (Stop condition
 *                                will not be generated)
 *                 i2c          - pointer to i2c resources
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_MasterTransmit (uint32_t addr, const uint8_t *data,
                                   uint32_t num, bool xfer_pending,
                                   const I2C_RESOURCES_t *i2c)
{
    /* Check input parameters */
    if ((data == NULL) || (num == 0U) ||
        ((addr & ~ARM_I2C_ADDRESS_GC) > 0x7FU))
    {
        /* Invalid parameters */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Check if driver was configured */
    if ((i2c->info->state & I2C_CONFIGURED) == 0U)
    {
        /* Driver not yet configured */
        return ARM_DRIVER_ERROR;
    }

    /* Check if i2c is busy */
    if (i2c->info->status.busy)
    {
        /* Transfer operation in progress */
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Configure status */
    i2c->info->status           = (const ARM_I2C_STATUS) {
        0
    };
    i2c->info->status.busy      = I2C_BUSY;
    i2c->info->status.mode      = I2C_STATUS_MODE_MASTER;
    i2c->info->status.direction = I2C_STATUS_DIRECTION_TX;

    /* Set transfer info */
    i2c->xfer->num     = num;
    i2c->xfer->cnt     = 0U;
    i2c->xfer->data    = (uint8_t *)data;
    i2c->xfer->addr    = (uint8_t)(addr);
    i2c->xfer->pending = xfer_pending;

    /* Disable slave mode and set clock prescaler */
    i2c->reg->CTRL0 = (((i2c->reg->CTRL0 & ~I2C_CTRL0_SPEED_Mask)   |
                        (i2c->info->prescale << I2C_CTRL0_SPEED_Pos)) &
                       ~I2C_SLAVE_ENABLE);

#if (I2C_DMA_CODE_EN)

    /* Reset dma busy flag */
    i2c->info->dma_busy = 0U;

    /* Clear pending i2c interrupts in nvic */
    NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

    /* Enable i2c interrupts */
    NVIC_EnableIRQ(I2C0_Resources.intInfo->irqn);

    /* Prepare dma for transmission */
    dma->SetConfigWord(i2c->info->default_cfg.dma_ch, i2c->info->dma_tx_cfg);

    /* Prepare dma buffer configuration */
    DMA_ADDR_CFG_t buffCfg = {
        .src_addr     = data,
        .dst_addr     = &i2c->reg->DATA,
        .counter_len  = num,
        .transfer_len = num
    };

    /* Configure the dma channel */
    dma->ConfigureAddr(i2c->info->default_cfg.dma_ch, &buffCfg);

    /* Reset the peripheral to a known state */
    Sys_I2C_Reset();

#endif /* if (I2C_DMA_CODE_EN) */

    /* Generate start condition for specified slave */
    Sys_I2C_StartWrite(i2c->xfer->addr);
#if (I2C_DMA_CODE_EN)

    /* Start the dma transfer */
    dma->Start(i2c->info->default_cfg.dma_ch);
#endif /* if (I2C_DMA_CODE_EN) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t I2C_MasterReceive (uint32_t addr,
 *                                            uint8_t *data,
 *                                            uint32_t num, bool xfer_pending,
 *                                            const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Start receiving data as i2c Master.
 * Inputs        : addr         - slave address (7-bit)
 *                 data         - pointer to data buffer to receive from i2c
 *                                slave
 *                 num          - number of data bytes to receive
 *                 xfer_pending - transfer operation is pending (stop condition
 *                                will not be generated)
 *                 i2c          - pointer to i2c resources
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_MasterReceive (uint32_t addr, uint8_t *data,
                                  uint32_t num, bool xfer_pending,
                                  const I2C_RESOURCES_t *i2c)
{
    /* Check input parameters */
    if ((data == NULL) || (num == 0U) ||
        ((addr & ~ARM_I2C_ADDRESS_GC) > 0x7FU))
    {
        /* Invalid parameters */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Check if driver was configured */
    if ((i2c->info->state & I2C_CONFIGURED) == 0U)
    {
        /* Driver not yet configured */
        return ARM_DRIVER_ERROR;
    }

    /* Check if i2c is busy */
    if (i2c->info->status.busy)
    {
        /* Transfer operation in progress */
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Reset status */
    i2c->info->status           = (const ARM_I2C_STATUS) { 0 };
    i2c->info->status.busy      = I2C_BUSY;
    i2c->info->status.mode      = I2C_STATUS_MODE_MASTER;
    i2c->info->status.direction = I2C_STATUS_DIRECTION_RX;

    /* Set transfer info */
    i2c->xfer->num     = num;
    i2c->xfer->cnt     = 0U;
    i2c->xfer->data    = (uint8_t *)data;
    i2c->xfer->addr    = (uint8_t)(addr);
    i2c->xfer->pending = xfer_pending;

    /* Disable slave mode and set clock prescaler */
    i2c->reg->CTRL0 = (((i2c->reg->CTRL0 & ~I2C_CTRL0_SPEED_Mask)   |
                        (i2c->info->prescale << I2C_CTRL0_SPEED_Pos)) &
                       ~I2C_SLAVE_ENABLE);

#if (I2C_DMA_CODE_EN)

    /* Reset dma busy flag */
    i2c->info->dma_busy = 0U;

    /* Clear pending i2c interrupts in nvic */
    NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

    /* Enable i2c interrupts */
    NVIC_EnableIRQ(I2C0_Resources.intInfo->irqn);

    /* Prepare dma for transmission */
    dma->SetConfigWord(i2c->info->default_cfg.dma_ch, i2c->info->dma_rx_cfg);

    /* Prepare dma buffer configuration */
    DMA_ADDR_CFG_t buffCfg = {
        .src_addr     = &i2c->reg->DATA,
        .dst_addr     = data,
        .counter_len  = num - 1,
        .transfer_len = num - 1
    };

    /* Configure the dma channel */
    dma->ConfigureAddr(i2c->info->default_cfg.dma_ch, &buffCfg);

    /* Reset the peripheral to a known state */
    Sys_I2C_Reset();

    /* Start the dma transfer */
    dma->Start(i2c->info->default_cfg.dma_ch);
#endif /* if (I2C_DMA_CODE_EN) */

    /* Generate start condition for specified slave */
    Sys_I2C_StartRead(i2c->xfer->addr);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t I2C_SlaveTransmit (const uint8_t * data,
 *                                            uint32_t num,
 *                                            const I2C_RESOURCES_t * i2c)
 * ----------------------------------------------------------------------------
 * Description   : Start transmitting data as I2C Slave.
 * Inputs        : data - Pointer to buffer for data to transmit to I2C Master
 *                 num  - Number of data bytes to transmit
 *                 i2c  - Pointer to I2C resources
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_SlaveTransmit (const uint8_t *data, uint32_t num,
                                  const I2C_RESOURCES_t *i2c)
{
    /* Check input parameters */
    if ((data == NULL) || (num == 0U))
    {
        /* Invalid parameters */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Check if driver was configured */
    if ((i2c->info->state & I2C_POWERED) == 0U)
    {
        /* Driver not yet configured */
        return ARM_DRIVER_ERROR;
    }

    /* Check if i2c is busy */
    if (i2c->info->status.busy)
    {
        /* Transfer operation in progress */
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Reset status */
    i2c->info->status           = (const ARM_I2C_STATUS) {
        0
    };
    i2c->info->status.busy      = I2C_BUSY;
    i2c->info->status.mode      = I2C_STATUS_MODE_SLAVE;
    i2c->info->status.direction = I2C_STATUS_DIRECTION_TX;

    /* Set transfer info */
    i2c->xfer->num  = num;
    i2c->xfer->cnt  = 0U;
    i2c->xfer->data = (uint8_t *)data;

#if (I2C_DMA_CODE_EN)

    /* Reset dma busy flag */
    i2c->info->dma_busy = 0U;

    /* Clear pending i2c interrupts in nvic */
    NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

    /* Enable i2c interrupts */
    NVIC_EnableIRQ(I2C0_Resources.intInfo->irqn);

    /* Prepare dma for transmission */
    dma->SetConfigWord(i2c->info->default_cfg.dma_ch, i2c->info->dma_tx_cfg);

    /* Prepare dma buffer configuration */
    DMA_ADDR_CFG_t buffCfg = {
        .src_addr     = data,
        .dst_addr     = &i2c->reg->DATA,
        .counter_len  = num,
        .transfer_len = num
    };

    /* Configure the dma channel */
    dma->ConfigureAddr(i2c->info->default_cfg.dma_ch, &buffCfg);

    /* Read the I2C data to clear the I2C flags */
    i2c->reg->DATA;

    /* / * Reset the peripheral to a known state * /
     * Sys_I2C_Reset(); */

    /* Start the dma transfer */
    dma->Start(i2c->info->default_cfg.dma_ch);

#endif /* if (I2C_DMA_CODE_EN) */

    /* Enable slave mode and set slave speed */
    i2c->reg->CTRL0 = ((i2c->reg->CTRL0 & ~I2C_CTRL0_SPEED_Mask) |
                       (I2C_SLAVE_SPEED_1 << I2C_CTRL0_SPEED_Pos) |
                       I2C_SLAVE_ENABLE);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_SlaveReceive (uint8_t *data, uint32_t num,
 *                                        const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Start receiving data as i2c Slave.
 * Inputs        : data - Pointer to buffer for data to receive from I2C Master
 *                 num  - Number of data bytes to receive
 *                 i2c  - Pointer to i2c resources
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_SlaveReceive (uint8_t *data, uint32_t num,
                                 const I2C_RESOURCES_t *i2c)
{
    /* Check input parameters */
    if ((data == NULL) || (num == 0U))
    {
        /* Invalid parameters */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Check if driver was configured */
    if ((i2c->info->state & I2C_POWERED) == 0U)
    {
        /* Driver not yet configured */
        return ARM_DRIVER_ERROR;
    }

    /* Check if i2c is busy */
    if (i2c->info->status.busy)
    {
        /* Transfer operation in progress */
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Reset status */
    i2c->info->status           = (const ARM_I2C_STATUS) {
        0
    };
    i2c->info->status.busy      = I2C_BUSY;
    i2c->info->status.mode      = I2C_STATUS_MODE_SLAVE;
    i2c->info->status.direction = I2C_STATUS_DIRECTION_RX;

    /* Set transfer info */
    i2c->xfer->num  = num;
    i2c->xfer->cnt  = 0U;
    i2c->xfer->data = data;

#if (I2C_DMA_CODE_EN)

    /* Reset dma busy flag */
    i2c->info->dma_busy = 0U;

    /* Clear pending i2c interrupts in nvic */
    NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

    /* Enable i2c interrupts */
    NVIC_EnableIRQ(I2C0_Resources.intInfo->irqn);

    /* Prepare dma for transmission */
    dma->SetConfigWord(i2c->info->default_cfg.dma_ch, i2c->info->dma_rx_cfg);

    /* Prepare dma buffer configuration */
    DMA_ADDR_CFG_t buffCfg = {
        .src_addr     = &i2c->reg->DATA,
        .dst_addr     = data,
        .counter_len  = num,
        .transfer_len = num
    };

    /* Configure the dma channel */
    dma->ConfigureAddr(i2c->info->default_cfg.dma_ch, &buffCfg);

    /* Reset the peripheral to a known state */
    Sys_I2C_Reset();

#endif /* if (I2C_DMA_CODE_EN) */

    /* Enable slave mode and set slave speed */
    i2c->reg->CTRL0 = ((i2c->reg->CTRL0 & ~I2C_CTRL0_SPEED_Mask) |
                       (I2C_SLAVE_SPEED_1 << I2C_CTRL0_SPEED_Pos) |
                       I2C_SLAVE_ENABLE);

#if (I2C_DMA_CODE_EN)

    /* Start the dma transfer */
    dma->Start(i2c->info->default_cfg.dma_ch);
#endif /* if (I2C_DMA_CODE_EN) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_GetDataCount (const I2C_RESOURCES_t * i2c)
 * ----------------------------------------------------------------------------
 * Description   : Get transferred data count.
 * Inputs        : i2c    - Pointer to i2c resources
 * Outputs       : number of data bytes transferred; -1 when slave is not
 *                 addressed by master
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_GetDataCount (const I2C_RESOURCES_t *i2c)
{
#if (I2C_CM3_CODE_EN)

    /* Return counter value */
    return ((int32_t)i2c->xfer->cnt);
#endif /* if (I2C_CM3_CODE_EN) */
#if (I2C_DMA_CODE_EN)

    /* Check if new transmission is ongoing */
    if (i2c->info->dma_busy)
    {
        /* Return counter value */
        return dma->GetCounterValue(i2c->info->default_cfg.dma_ch);
    }

    /* Return 0 */
    return 0;
#endif /* if (I2C_DMA_CODE_EN) */
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_Control (uint32_t control, uint32_t arg,
 *                                   const I2C_RESOURCES_t *i2c)
 * ----------------------------------------------------------------------------
 * Description   : Control i2c Interface
 * Inputs        : control - control operation
 *                 arg     - argument of operation (optional)
 *                 i2c     - pointer to I2C resources
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t I2C_Control (uint32_t control, uint32_t arg, const I2C_RESOURCES_t *i2c)
{
    uint32_t bus_speed, delay_cycles;

    /* Check if i2c is powered up */
    if ((i2c->info->state & I2C_POWERED) == 0U)
    {
        /* I2C not powered */
        return ARM_DRIVER_ERROR;
    }

    /* Check which operation should be performed */
    switch (control)
    {
        /* Check if i2c was powered up */
        case ARM_I2C_OWN_ADDRESS:
        {
            /* Check if i2c is busy */
            if (i2c->info->status.busy)
            {
                /* Transfer operation in progress */
                return ARM_DRIVER_ERROR_BUSY;
            }

            /* Wait until bus is free */
            while (!(Sys_I2C_Get_Status() & I2C_BUS_FREE))
            {
                Sys_Watchdog_Refresh();
            }

            /* Set address */
            i2c->reg->CTRL0 = ((i2c->reg->CTRL0 & ~I2C_CTRL0_SLAVE_ADDRESS_Mask) |
                               ((arg << I2C_CTRL0_SLAVE_ADDRESS_Pos) &
                                I2C_CTRL0_SLAVE_ADDRESS_Mask));
            break;
        }

        case ARM_I2C_BUS_SPEED:
        {
            /* Check if i2c is busy */
            if (i2c->info->status.busy)
            {
                /* Transfer operation in progress */
                return ARM_DRIVER_ERROR_BUSY;
            }

            /* Set Bus Speed */
            switch (arg)
            {
                case ARM_I2C_BUS_SPEED_STANDARD:
                {
                    /* Standard Speed (100kHz) */
                    bus_speed = I2C_SPEED_100KHZ;
                    break;
                }

                case ARM_I2C_BUS_SPEED_FAST:
                {
                    /* Fast Speed     (400kHz) */
                    bus_speed = I2C_SPEED_400KHZ;
                    break;
                }

                case ARM_I2C_BUS_SPEED_FAST_PLUS:
                {
                    /* Fast+ Speed    (  1MHz) */
                    bus_speed = I2C_SPEED_1MHZ;
                    break;
                }

                case ARM_I2C_BUS_SPEED_HIGH:

                /* High Speed     (3.4MHz) not supported */
                default:
                {
                    /* Return not supported error */
                    return ARM_DRIVER_ERROR_UNSUPPORTED;
                }
            }

            /* If system clock is not fast enough */
            if (SystemCoreClock < 3.125 * bus_speed)
            {
                /* Return not supported error */
                return ARM_DRIVER_ERROR_UNSUPPORTED;
            }

            /* Calculate I2C prescaler */
            i2c->info->prescale = SystemCoreClock / (I2C_CLOCK_MUL * bus_speed) - 1;

            /* Wait until bus is free */
            while (!(Sys_I2C_Get_Status() & I2C_BUS_FREE))
            {
                Sys_Watchdog_Refresh();
            }

            /* Set I2C prescaler */
            i2c->reg->CTRL0 = ((i2c->reg->CTRL0 & ~I2C_CTRL0_SPEED_Mask) |
                               (i2c->info->prescale << I2C_CTRL0_SPEED_Pos));

            /* Speed configured, I2C Master active */
            i2c->info->state |= I2C_CONFIGURED;
            break;
        }

        case ARM_I2C_BUS_CLEAR:
        {
            /* Disable I2C clock and interrupts temporarily */
            i2c->reg->CTRL0 &= ~I2C_SAMPLE_CLK_ENABLE;

            /* Disable i2c interrupts */
            NVIC_DisableIRQ(i2c->intInfo->irqn);

            /* Reset DIO_I2C_SRC configuration */
            DIO->I2C_SRC = (SDA_SRC_CONST_HIGH | SCL_SRC_CONST_HIGH);

            /* Prepare pins configuration */
            const GPIO_PAD_CFG_t gpioLocCfg = {
                .pull_mode     = GPIO_STRONG_PULL_UP,
                .drive_mode    = GPIO_6X,
                .lpf_en        = GPIO_LPF_DISABLE,
                .io_mode       = GPIO_MODE_GPIO_OUT_1
            };

            /* Configure pins as GPIO output */
            gpio->ConfigurePad(i2c->info->default_cfg.sda_pin, &gpioLocCfg);
            gpio->ConfigurePad(i2c->info->default_cfg.scl_pin, &gpioLocCfg);

            /* Delay cycles for the standard 100KHz speed */
            delay_cycles = SystemCoreClock / I2C_SPEED_100KHZ;

            /* Issue 9 clock pulses on SCL line */
            for (uint8_t i = 0U; i < 9U; i++)
            {
                /* Clock high */
                gpio->SetHigh(i2c->info->default_cfg.scl_pin);
                Sys_Delay_ProgramROM(delay_cycles);

                /* Clock low */
                gpio->SetLow(i2c->info->default_cfg.scl_pin);
                Sys_Delay_ProgramROM(delay_cycles);
            }

            /* Manually generates STOP condition - SDA goes high while SCL is
             * high to end transaction. */
            gpio->SetLow(i2c->info->default_cfg.sda_pin);
            Sys_Delay_ProgramROM(delay_cycles);
            gpio->SetHigh(i2c->info->default_cfg.scl_pin);
            Sys_Delay_ProgramROM(delay_cycles);
            gpio->SetHigh(i2c->info->default_cfg.sda_pin);

            /* Reconfigure SCL and SDA Pins as I2C peripheral pins */
            gpioCfg.io_mode = GPIO_MODE_I2C_SDA;

            /* Configure sda pin */
            gpio->ConfigurePad(i2c->info->default_cfg.sda_pin, &gpioCfg);

            /* Change configuration to scl */
            gpioCfg.io_mode = GPIO_MODE_I2C_SCL;

            /* Configure scl pin */
            gpio->ConfigurePad(i2c->info->default_cfg.scl_pin, &gpioCfg);

            /* Enable I2C clock and interrupt */
            i2c->reg->CTRL0 |= I2C_SAMPLE_CLK_ENABLE;

            /* Clear pending i2c interrupts */
            NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

            /* Enable i2c interrupts */
            NVIC_EnableIRQ(i2c->intInfo->irqn);

            /* Check if event callback was registered */
            if (i2c->intInfo->cb)
            {
                /* Send event */
                i2c->intInfo->cb(ARM_I2C_EVENT_BUS_CLEAR);
            }
            break;
        }

        case ARM_I2C_ABORT_TRANSFER:
        {
            if (i2c->info->status.busy)
            {
                /* Disable IRQ temporarily */
                NVIC_DisableIRQ(i2c->intInfo->irqn);

                /* If master, send stop */
                if (i2c->info->status.mode == I2C_STATUS_MODE_MASTER)
                {
                    i2c->reg->CTRL1 = I2C_STOP;

                    /* Wait for stop to be issued */
                    while (!(Sys_I2C_Get_Status() & I2C_STOP_DETECTED))
                    {
                        Sys_Watchdog_Refresh();
                    }
                }

                /* If slave */
                else
                {
                    /* Disable slave mode */
                    i2c->reg->CTRL0 &= ~I2C_SLAVE_ENABLE;

                    /* Reset the peripheral to a known state */
                    Sys_I2C_Reset();
                }

                /* Clear transfer info */
                i2c->xfer->num  = 0U;
                i2c->xfer->cnt  = 0U;
                i2c->xfer->data = NULL;
                i2c->xfer->addr = 0U;

                /* Clear statuses */
                i2c->info->status.busy             = 0U;
                i2c->info->status.general_call     = 0U;
                i2c->info->status.arbitration_lost = 0U;
                i2c->info->status.bus_error        = 0U;

                /* Clear pending i2c interrupts */
                NVIC_ClearPendingIRQ(i2c->intInfo->irqn);

                /* Enable i2c interrupts */
                NVIC_EnableIRQ(i2c->intInfo->irqn);
            }
            break;
        }

        default:

            /* Return error */
            return ARM_DRIVER_ERROR;
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_GetStatus(const I2C_RESOURCES_t * i2c)
 * ----------------------------------------------------------------------------
 * Description   : Get I2Cx status
 * Inputs        : i2c    - I2C resources
 * Outputs       : Return I2Cx status as an ARM_I2C_STATUS structure
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_I2C_STATUS I2C_GetStatus(const I2C_RESOURCES_t *i2c)
{
    return (i2c->info->status);
}

/* I2C0 Driver Wrapper functions. See functions above for usage */
static int32_t I2C0_Initialize (ARM_I2C_SignalEvent_t cb)
{
    /* Initialize usart */
    int32_t status = I2C_Initialize(cb, &I2C0_Resources);

    /* Set i2c interrupt priorities */
    status |= I2C_SetInterruptPriority(&I2C0_Resources);

#if (RTE_I2C0_CFG_EN_DEFAULT)

    /* Set default power mode */
    status |= I2C_PowerControl(ARM_POWER_FULL, &I2C0_Resources);

    /* Configure default bus speed */
    I2C_Control(ARM_I2C_BUS_SPEED, RTE_I2C0_SPEED_DEFAULT, &I2C0_Resources);

    /* Set default slave address */
    status |= I2C_Control(ARM_I2C_OWN_ADDRESS, RTE_I2C0_SLAVE_ADDR_DEFAULT,
                          &I2C0_Resources);
#endif /* if (RTE_I2C0_CFG_EN_DEFAULT) */

    /* Return initialization status */
    return status;
}

static int32_t I2C0_Uninitialize (void)
{
    return I2C_Uninitialize(&I2C0_Resources);
}

static int32_t I2C0_PowerControl (ARM_POWER_STATE state)
{
    return I2C_PowerControl(state, &I2C0_Resources);
}

static int32_t I2C0_MasterTransmit (uint32_t addr, const uint8_t *data, uint32_t num, bool xfer_pending)
{
    return I2C_MasterTransmit(addr, data, num, xfer_pending, &I2C0_Resources);
}

static int32_t I2C0_MasterReceive (uint32_t addr, uint8_t *data, uint32_t num, bool xfer_pending)
{
    return I2C_MasterReceive(addr, data, num, xfer_pending, &I2C0_Resources);
}

static int32_t I2C0_SlaveTransmit (const uint8_t *data, uint32_t num)
{
    return I2C_SlaveTransmit(data, num, &I2C0_Resources);
}

static int32_t I2C0_SlaveReceive (uint8_t *data, uint32_t num)
{
    return I2C_SlaveReceive(data, num, &I2C0_Resources);
}

static int32_t I2C0_GetDataCount (void)
{
    return I2C_GetDataCount(&I2C0_Resources);
}

static int32_t I2C0_Control (uint32_t control, uint32_t arg)
{
    return I2C_Control(control, arg, &I2C0_Resources);
}

static ARM_I2C_STATUS I2C0_GetStatus (void)
{
    return I2C_GetStatus(&I2C0_Resources);
}

#if (I2C_CM3_CODE_EN)
void I2C_IRQHandler(void)
{
    /* Check if driver is in master or slave mode */
    if (I2C0_Resources.info->status.mode == I2C_STATUS_MODE_MASTER)
    {
        /* Execute master interrupt handler */
        I2Cx_MasterIRQHandler(&I2C0_Resources);
    }
    else
    {
        /* Execute slave interrupt handler */
        I2Cx_SlaveIRQHandler(&I2C0_Resources);
    }
}

#else  /* if (I2C_CM3_CODE_EN) */
void I2C_IRQHandler(void)
{
    if (I2C0_Resources.info->status.mode == I2C_STATUS_MODE_MASTER)
    {
        I2Cx_MasterIRQHandler(&I2C0_Resources);
    }
    else
    {
        /* Execute slave interrupt handler */
        I2Cx_SlaveIRQHandler(&I2C0_Resources);
    }
}

void I2C_DMA_Handler(uint32_t event)
{
    if (I2C0_Resources.info->status.busy)
    {
        I2C_DMAHandler(event, &I2C0_Resources);
    }
}

#endif /* if (I2C_CM3_CODE_EN) */

/* I2C0 Driver Control Block */
ARM_DRIVER_I2C Driver_I2C0 =
{
    I2Cx_GetVersion,
    I2Cx_GetCapabilities,
    I2C0_Initialize,
    I2C0_Uninitialize,
    I2C0_PowerControl,
    I2C0_MasterTransmit,
    I2C0_MasterReceive,
    I2C0_SlaveTransmit,
    I2C0_SlaveReceive,
    I2C0_GetDataCount,
    I2C0_Control,
    I2C0_GetStatus
};
#endif    /* if RTE_I2C0 */
