/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * I2C_RSLxx.h
 * - I2C CMSIS-Driver header file for RSLxx family of devices
 * ----------------------------------------------------------------------------
 * $Revision: 1.3 $
 * $Date: 2019/06/10 21:57:16 $
 * ------------------------------------------------------------------------- */

#ifndef I2C_RSLXX_H
#define I2C_RSLXX_H

#include <rsl10.h>
#include <Driver_I2C.h>
#include <RTE_Device.h>
#include <Driver_DMA.h>
#include <Driver_GPIO.h>

#if (!RTE_I2C)
  #error "I2C0 not configured in RTE_Device.h!"
#endif    /* if (!RTE_I2C0) */

/* DMA / CM3 code switches */
#define I2C_DMA_CODE_EN (RTE_I2C0_DMA_EN_DEFAULT)
#define I2C_CM3_CODE_EN (!RTE_I2C0_DMA_EN_DEFAULT)

/* I2C interrupt handlers prototypes */
#if(I2C_DMA_CODE_EN)
void I2C_DMA_Handler(uint32_t event);
#endif
void I2C_IRQHandler(void);

/* Extern GPIO driver */
extern DRIVER_GPIO_t Driver_GPIO;

/* Extern DMA driver */
extern DRIVER_DMA_t Driver_DMA;

/* Driver status flag definition */
#define I2C_INITIALIZED           ((uint8_t)(1U))
#define I2C_POWERED               ((uint8_t)(1U << 1))
#define I2C_CONFIGURED            ((uint8_t)(1U << 2))

/* Constants used to improve code readability */
#define I2C_STATUS_MODE_MASTER    1U
#define I2C_STATUS_MODE_SLAVE     0U
#define I2C_STATUS_DIRECTION_TX   0U
#define I2C_STATUS_DIRECTION_RX   1U

/* I2C clock multipliers */
#define I2C_CLOCK_MUL             3
#define I2C_CLOCK_TEST_MUL        (3.5)

/* I2C statuses */
#define I2C_FREE                  0U
#define I2C_BUSY                  1U

/* I2C speed values */
#define I2C_SPEED_100KHZ      100000
#define I2C_SPEED_400KHZ      400000
#define I2C_SPEED_1MHZ        1000000

/* I2C Transfer Information (Run-Time) */
typedef struct _I2C_TRANSFER_INFO_t
{
    uint32_t                  num;               /* Total number of data to transfer */
    uint32_t                  cnt;               /* Number of data transferred */
    uint8_t                  *data;              /* Pointer to data buffer */
    uint8_t                   addr;              /* Device address */
    bool                      pending;           /* If transfer is pending */
} I2C_TRANSFER_INFO_t;

/* I2C interrupt priority configuration */
typedef struct _I2C_PRI_CFG_t
{
    uint32_t                  preempt_pri   :3;  /* preempt priority */
    uint32_t                                :13; /* reserved */
    uint32_t                  subgrp_pri    :3;  /* subgroup priority */
    uint32_t                                :13; /*reserved */
} I2C_PRI_CFG_t;

/* I2C default configuration */
typedef struct _I2C_DEFAULT_CFG_t
{
    GPIO_SEL_t               sda_pin        :4;  /* i2c sda pin identifier */
    GPIO_SEL_t               scl_pin        :4;  /* i2c scl pin identifier */
#if(I2C_DMA_CODE_EN)
    DMA_SEL_t                dma_ch         :3;  /* defines which dma channel should be used for data transmission */
    uint8_t                                 :1;  /* reserved */
#endif
    I2C_PRI_CFG_t            pri_cfg;            /* i2c interrupt priority default configuration */
} I2C_DEFAULT_CFG_t;

/* I2C runtime info */
typedef struct _I2C_INFO_t
{
    const I2C_DEFAULT_CFG_t   default_cfg;       /* i2c default configuration */
    ARM_I2C_STATUS            status;            /* status flags */
    uint8_t                   state;             /* current I2C state */
    uint32_t                  prescale;          /* i2c clock prescale speed */
#if(I2C_DMA_CODE_EN)
    uint32_t                  dma_rx_cfg;        /* dma channel receiver configuration */
    uint32_t                  dma_tx_cfg;        /* dma channel transmitter configuration */
    uint8_t                   dma_busy      :1;  /* !!!WORKAROUND!!! for dma rx counter not being reset before new transmission */
    uint8_t                                 :7;  /* reserved */
#endif
} I2C_INFO_t;

/* I2C interrupts info */
typedef struct _I2C_INT_INFO_t
{
    const IRQn_Type           irqn;              /* i2c IRQs number */
    ARM_I2C_SignalEvent_t     cb;                /* i2c event callback */
} I2C_INT_INFO_t;

/* I2C resources definition */
typedef const struct
{
    I2C_Type                 *reg;               /* i2c peripheral pointer */
    I2C_INT_INFO_t           *intInfo;           /* IRQs Info */
    I2C_INFO_t               *info;              /* runtime Information */
    I2C_TRANSFER_INFO_t      *xfer;              /* i2c transfer information */
} I2C_RESOURCES_t;

#endif    /* I2C_RSLXX_H */
