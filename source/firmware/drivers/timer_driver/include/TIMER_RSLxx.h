/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * TIMER_RSLxx.h
 * - TIMER driver header file for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#ifndef TIMER_RSLXX_H
#define TIMER_RSLXX_H

#include <RTE_Device.h>
#include <rsl10.h>
#include <Driver_TIMER.h>

#if (!RTE_TIMER)
  #error "TIMER not configured in RTE_Device.h!"
#endif    /* if (!RTE_TIMER) */

/* TIMER interrupt callbacks prototypes */
#if (RTE_TIMER0_EN)
void TIMER0_IRQHandler(void);
#endif
#if (RTE_TIMER1_EN)
void TIMER1_IRQHandler(void);
#endif
#if (RTE_TIMER2_EN)
void TIMER2_IRQHandler(void);
#endif
#if (RTE_TIMER3_EN)
void TIMER3_IRQHandler(void);
#endif
#if (RTE_SYSTICK_EN)
void SysTick_Handler(void);
#endif


/* TIMERs number */
#define TIMER_TIMERS_NUMBER  5

/* TIMER flags */
#define TIMER_FLAG_BIT_SET   1U

/* TIMER enabled driver mask */
#define TIMER_EN_MSK        ((RTE_TIMER0_EN  << TIMER_0) | \
                             (RTE_TIMER1_EN  << TIMER_1) | \
                             (RTE_TIMER2_EN  << TIMER_2) | \
                             (RTE_TIMER3_EN  << TIMER_3) | \
                             (RTE_SYSTICK_EN << TIMER_SYSTICK))

/* TIMER regular enabled driver mask */
#define TIMER_REG_EN_MSK    ((RTE_TIMER0_EN  << TIMER_0) | \
                             (RTE_TIMER1_EN  << TIMER_1) | \
                             (RTE_TIMER2_EN  << TIMER_2) | \
                             (RTE_TIMER3_EN  << TIMER_3))

/* TIMER interrupts info */
typedef struct _TIMER_INT_INFO_t
{
    const IRQn_Type          irqn[TIMER_TIMERS_NUMBER];              /* timer / systick irq numbers */
    TIMER_SignalEvent_t      cb;                                     /* timer / systick event callback */
} TIMER_INT_INFO_t;

/* TIMER runtime info */
typedef struct _TIMER_INFO_t
{
    const TIMER_CFG_t       *default_cfg[TIMER_TIMERS_NUMBER];       /* timer default configuration */
    const TIMER_PRI_CFG_t   *default_pri_cfg[TIMER_TIMERS_NUMBER];   /* timer priorities default configuration */
    uint8_t                  flags;                                  /* timer state */
} TIMER_INFO_t;

/* TIMER Resources definition */
typedef struct
{
    TIMER_INFO_t            *info;                                   /* run-time info */
    TIMER_INT_INFO_t         intInfo;                                /* IRQs Info */
} TIMER_RESOURCES_t;

#endif /* TIMER_RSLXX_H */
