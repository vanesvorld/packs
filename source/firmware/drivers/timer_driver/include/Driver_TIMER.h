/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * Driver_TIMER.h
 * - TIMER driver header file for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#ifndef DRIVER_TIMER_H_
#define DRIVER_TIMER_H_

#ifdef  __cplusplus
extern "C"
{
#endif

#include <Driver_Common.h>

#define ARM_TIMER_API_VERSION ARM_DRIVER_VERSION_MAJOR_MINOR(1,0)  /* API version */

/****** TIMER Control Codes *****/

/*----- TIMER Control Codes: TIMER Selection -----*/
typedef enum _TIMER_SEL_t {
    TIMER_0                       = 0,              ///< timer module 0
    TIMER_1                       = 1,              ///< timer module 1
    TIMER_2                       = 2,              ///< timer module 2
    TIMER_3                       = 3,              ///< timer module 3
    TIMER_SYSTICK                 = 4               ///< timer module systick
} TIMER_SEL_t;

/*----- TIMER Control Codes: Modes -----*/
typedef enum _TIMER_MODE_t {
     TIMER_MODE_SHOT              = 0x0U,           ///< timer mode = TIMER_SHOT_MODE_BITBAND
     TIMER_MODE_FREE_RUN          = 0x1U            ///< timer mode = TIMER_FREE_RUN_BITBAND
} TIMER_MODE_t;

/*----- TIMER Control Codes: Clock sources -----*/
typedef enum _TIMER_CLKSRC_t {
    TIMER_SLOWCLOCK_DIV32         = 0x0U,           ///< timer src = SLOWCLOCK DIV32
    TIMER_SLOWCLOCK_DIV2          = 0x1U            ///< timer src = SLOWCLOCK DIV2
} TIMER_CLKSRC_t;

/*----- TIMER Control Codes: Prescale values -----*/
typedef enum _TIMER_PRESCALE_t {
    TIMER_PRESCALE_VAL_1          = 0x0U,           ///< timer prescale = 1
    TIMER_PRESCALE_VAL_2          = 0x1U,           ///< timer prescale = 2
    TIMER_PRESCALE_VAL_4          = 0x2U,           ///< timer prescale = 4
    TIMER_PRESCALE_VAL_8          = 0x3U,           ///< timer prescale = 8
    TIMER_PRESCALE_VAL_16         = 0x4U,           ///< timer prescale = 16
    TIMER_PRESCALE_VAL_32         = 0x5U,           ///< timer prescale = 32
    TIMER_PRESCALE_VAL_64         = 0x6U,           ///< timer prescale = 64
    TIMER_PRESCALE_VAL_128        = 0x7U            ///< timer prescale = 128
} TIMER_PRESCALE_t;

/*----- TIMER Control Codes: Multi Count values -----*/
typedef enum _TIMER_MULTI_COUNT_t {
    TIMER_MULTI_COUNT_VAL_1       = 0x0U,           ///< timer multiCount = 1
    TIMER_MULTI_COUNT_VAL_2       = 0x1U,           ///< timer multiCount = 2
    TIMER_MULTI_COUNT_VAL_3       = 0x2U,           ///< timer multiCount = 3
    TIMER_MULTI_COUNT_VAL_4       = 0x3U,           ///< timer multiCount = 4
    TIMER_MULTI_COUNT_VAL_5       = 0x4U,           ///< timer multiCount = 5
    TIMER_MULTI_COUNT_VAL_6       = 0x5U,           ///< timer multiCount = 6
    TIMER_MULTI_COUNT_VAL_7       = 0x6U,           ///< timer multiCount = 7
    TIMER_MULTI_COUNT_VAL_8       = 0x7U            ///< timer multiCount = 8
} TIMER_MULTI_COUNT_t;

/*----- TIMER Control Codes: SYSTICK Clock sources -----*/
typedef enum _TIMER_SYSTICK_CLKSRC_t {
    SYSTICK_CLKSOURCE_EXTREFCLK   = 0x0U,           ///< systick timer clk src = external ref
    SYSTICK_CLKSOURCE_CORECLK     = 0x1U            ///< systick timer clk src = core clk
} TIMER_SYSTICK_CLKSRC_t;

/*----- TIMER Control Codes: timer interrupt events -----*/
typedef enum _ADC_EVENT_t {
    TIMER_TIMER0_EVENT   = 1 << TIMER_0,        ///< timer0 event
    TIMER_TIMER1_EVENT   = 1 << TIMER_1,        ///< timer1 event
    TIMER_TIMER2_EVENT   = 1 << TIMER_2,        ///< timer2 event
    TIMER_TIMER3_EVENT   = 1 << TIMER_3,        ///< timer3 event
    TIMER_SYSTICK_EVENT  = 1 << TIMER_SYSTICK   ///< systick event
} ADC_EVENT_t;

/*----- TIMER Control Codes: Error codes -----*/
#define TIMER_ERROR_UNCONFIGURED  (ARM_DRIVER_ERROR_SPECIFIC - 1) ///< Driver has not been configured yet

// Function documentation
/**
  \fn          ARM_DRIVER_VERSION TIMER_GetVersion (void)
  \brief       Get driver version.
  \return      \ref ARM_DRIVER_VERSION

  \fn          int32_t TIMER_Initialize (TIMER_SignalEvent_t cb_event)
  \brief       Initialize timer driver with default configuration.
  \param[in]   cb_event pointer to \ref TIMER_SignalEvent
  \return      \ref execution_status

  \fn          int32_t TIMER_Configure (TIMER_SEL_t sel, TIMER_CFG_t * cfg)
  \brief       Configure particular timer.
  \param[in]   sel TIMER to be configured (\ref TIMER_SEL_t)
  \param[in]   cfg pointer to \ref TIMER_CFG
  \return      \ref execution_status

  \fn          int32_t TIMER_SetInterruptPriority (TIMER_SEL_t sel, const TIMER_PRI_CFG_t * cfg)
  \brief       Configure the timer interrupt priority
  \param[in]   sel timer to be configured (\ref TIMER_SEL_t)
  \param[in]   cfg  Pointer to \ref TIMER_PRI_CFG_t
  \return      \ref execution_status

  \fn          int32_t TIMER_Start (TIMER_SEL_t sel)
  \brief       Starts the timer.
  \param[in]   sel timer number to be started (\ref TIMER_SEL_t)
  \return      \ref execution_status

  \fn          int32_t TIMER_Stop (TIMER_SEL_t sel)
  \brief       Stops the TIMER.
  \param[in]   sel TIMER number to be stopped (\ref TIMER_SEL_t)
  \return      \ref execution_status

  \fn          int32_t TIMER_SetValue (TIMER_SEL_t sel, uint32_t val)
  \brief       Sets the timeout / reload value of the selected TIMER
  \param[in]   sel timer value to be read (\ref TIMER_SEL_t)
  \param[in]   val timer value to be set
  \return      \ref execution_status of error status

  \fn          uint32_t TIMER_GetValue (TIMER_SEL_t sel)
  \brief       Returns the current value of TIMER
  \param[in]   sel timer value to be read (\ref TIMER_SEL_t)
  \return      \ref timer value or 0 if timer was not enabled

  \fn          uint32_t TIMER_GetSysTickState (void)
  \brief       Returns 1 if systick has already reached 0
  \param[in]   None
  \return      Systick status or 0 if systick was not enabled

  \fn          void TIMER_SignalEvent (uint32_t event)
  \brief       Signal timer events.
  \param[in]   event notification mask
  \return      none
*/

typedef void (*TIMER_SignalEvent_t) (uint32_t event);  ///< Pointer to \ref TIMER_SignalEvent : signal timer event.

/**
\brief TIMER Driver configuration.
*/
typedef struct _TIMER_t
{
    TIMER_MODE_t           mode         :1;   ///< timer mode to be used
    TIMER_CLKSRC_t         clk_src      :1;   ///< clockSource to be used
    uint32_t                            :6;   ///< reserved
    TIMER_PRESCALE_t       prescale_val :8;   ///< timer prescale value
    TIMER_MULTI_COUNT_t    multi_cnt    :4;   ///< multi count value
    uint32_t                            :4;   ///< reserved
    uint32_t               timeout_val;       ///< timer timeout value
} TIMER_t;

/**
\brief SYSTICK Driver configuration.
*/
typedef struct _SYSTICK_t
{
    TIMER_SYSTICK_CLKSRC_t clk_src      :1;   ///< clockSource to be used
    uint32_t                            :7;   ///< reserved
    uint32_t               reload_val   :24;  ///< systick value
} SYSTICK_t;

/**
\brief Common TIMER driver configuration.
*/
typedef union _TIMER_CFG_t
{
    TIMER_t                timer_cfg;         ///< timer configuration
    SYSTICK_t              systick_cfg;       ///< systick configuration
} TIMER_CFG_t;

/**
\brief TIMER interrupt priority configuration
*/
typedef struct _TIMER_PRI_CFG_t
{
    uint32_t               preempt_pri  :3;   ///< preempt priority
    uint32_t                            :13;  ///< reserved
    uint32_t               subgrp_pri   :3;   ///< subgroup priority
    uint32_t                            :13;  ///< reserved
} TIMER_PRI_CFG_t;

/**
\brief Access structure of the TIMER Driver.
*/
typedef struct _DRIVER_TIMER_t {
    ARM_DRIVER_VERSION (*GetVersion)           (void);                                         ///< Pointer to \ref TIMER_GetVersion : Get driver version.
    int32_t            (*Initialize)           (TIMER_SignalEvent_t cb);                       ///< Pointer to \ref TIMER_Initialize : Initialize timer driver.
    int32_t            (*Configure)            (TIMER_SEL_t sel, const TIMER_CFG_t * cfg);     ///< Pointer to \ref TIMER_Configure : Configure driver.
    int32_t            (*SetInterruptPriority) (TIMER_SEL_t sel, const TIMER_PRI_CFG_t * pri); ///< Pointer to \ref TIMER_SetInterruptPriority : Configure timer interrupt priority.
    int32_t            (*Start)                (TIMER_SEL_t sel);                              ///< Pointer to \ref TIMER_Start : Start particular timer.
    int32_t            (*Stop)                 (TIMER_SEL_t sel);                              ///< Pointer to \ref TIMER_Stop : Stop particular timer.
    int32_t            (*SetValue)             (TIMER_SEL_t sel, uint32_t value);              ///< Pointer to \ref TIMER_SetValue : Set the particular timer value.
    uint32_t           (*GetValue)             (TIMER_SEL_t sel);                              ///< Pointer to \ref TIMER_GetValue : Get the particular timer value.
    uint32_t           (*GetSysTickState)      (void);                                         ///< Pointer to \ref TIMER_GetSysTickState: Returns 1 if systick has already reached 0.
} const DRIVER_TIMER_t;

#ifdef  __cplusplus
}
#endif

#endif /* DRIVER_TIMER_H_ */
