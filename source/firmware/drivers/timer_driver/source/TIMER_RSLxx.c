/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * TIMER_RSLxx.c
 * - TIMER driver implementation for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#include <TIMER_RSLxx.h>

#if RTE_TIMER

/* Driver Version Macro */
#define ARM_TIMER_DRV_VERSION    ARM_DRIVER_VERSION_MAJOR_MINOR(0, 1)

/* Driver Version */
static const ARM_DRIVER_VERSION DriverVersion =
{
    ARM_TIMER_API_VERSION,
    ARM_TIMER_DRV_VERSION
};

#if (RTE_TIMER0_EN)

/* Set default timer0 configuration */
static const TIMER_CFG_t timer0_DefaultCfg = {                  /* timer 0 default configuration */
    .timer_cfg.mode          = RTE_TIMER0_MODE_DEFAULT,         /* timer 0 default mode */
    .timer_cfg.clk_src       = RTE_TIMER0_CLKSRC_DEFAULT,       /* timer 0 default clock source */
    .timer_cfg.prescale_val  = RTE_TIMER0_PRESCALE_DEFAULT,     /* timer 0 default prescale value */
    .timer_cfg.multi_cnt     = RTE_TIMER0_MULTI_COUNT_DEFAULT,  /* timer 0 default multi count value */
    .timer_cfg.timeout_val   = RTE_TIMER0_TIMEOUT_DEFAULT,      /* timer 0 default timeout value */
};
static const TIMER_PRI_CFG_t timer0_DefaultPri = {            /* timer 0 default interrupt priority configuration */
    .preempt_pri             = RTE_TIMER0_INT_PREEMPT_PRI,      /* timer 0 default pre-empt priority value */
    .subgrp_pri              = RTE_TIMER0_INT_SUBGRP_PRI        /* timer 0 default subgroup priority value */
};
#endif /* if (RTE_TIMER0_EN) */

#if (RTE_TIMER1_EN)

/* Set default timer1 configuration */
static const TIMER_CFG_t timer1_DefaultCfg = {                /* timer 1 default configuration */
    .timer_cfg.mode          = RTE_TIMER1_MODE_DEFAULT,         /* timer 1 default mode */
    .timer_cfg.clk_src       = RTE_TIMER1_CLKSRC_DEFAULT,       /* timer 1 default clock source */
    .timer_cfg.prescale_val  = RTE_TIMER1_PRESCALE_DEFAULT,     /* timer 1 default prescale value */
    .timer_cfg.multi_cnt     = RTE_TIMER1_MULTI_COUNT_DEFAULT,  /* timer 1 default multi count value */
    .timer_cfg.timeout_val   = RTE_TIMER1_TIMEOUT_DEFAULT,      /* timer 1 default timeout value */
};
static const TIMER_PRI_CFG_t timer1_DefaultPri = {            /* timer 1 default interrupt priority configuration */
    .preempt_pri             = RTE_TIMER1_INT_PREEMPT_PRI,      /* timer 1 default pre-empt priority value */
    .subgrp_pri              = RTE_TIMER1_INT_SUBGRP_PRI        /* timer 1 default subgroup priority value */
};
#endif /* if (RTE_TIMER1_EN) */

#if (RTE_TIMER2_EN)

/* Set default timer2 configuration */
static const TIMER_CFG_t timer2_DefaultCfg = {                /* timer 2 default configuration */
    .timer_cfg.mode          = RTE_TIMER2_MODE_DEFAULT,         /* timer 2 default mode */
    .timer_cfg.clk_src       = RTE_TIMER2_CLKSRC_DEFAULT,       /* timer 2 default clock source */
    .timer_cfg.prescale_val  = RTE_TIMER2_PRESCALE_DEFAULT,     /* timer 2 default prescale value */
    .timer_cfg.multi_cnt     = RTE_TIMER2_MULTI_COUNT_DEFAULT,  /* timer 2 default multi count value */
    .timer_cfg.timeout_val   = RTE_TIMER2_TIMEOUT_DEFAULT,      /* timer 2 default timeout value */
};
static const TIMER_PRI_CFG_t timer2_DefaultPri = {            /* timer 2 default interrupt priority configuration */
    .preempt_pri             = RTE_TIMER2_INT_PREEMPT_PRI,      /* timer 2 default pre-empt priority value */
    .subgrp_pri              = RTE_TIMER2_INT_SUBGRP_PRI        /* timer 2 default subgroup priority value */
};
#endif /* if (RTE_TIMER2_EN) */

#if (RTE_TIMER3_EN)

/* Set default timer3 configuration */
static const TIMER_CFG_t timer3_DefaultCfg = {                /* timer 3 default configuration */
    .timer_cfg.mode          = RTE_TIMER3_MODE_DEFAULT,         /* timer 3 default mode */
    .timer_cfg.clk_src       = RTE_TIMER3_CLKSRC_DEFAULT,       /* timer 3 default clock source */
    .timer_cfg.prescale_val  = RTE_TIMER3_PRESCALE_DEFAULT,     /* timer 3 default prescale value */
    .timer_cfg.multi_cnt     = RTE_TIMER3_MULTI_COUNT_DEFAULT,  /* timer 3 default multi count value */
    .timer_cfg.timeout_val   = RTE_TIMER3_TIMEOUT_DEFAULT,      /* timer 3 default timeout value */
};
static const TIMER_PRI_CFG_t timer3_DefaultPri = {            /* timer 3 default interrupt priority configuration */
    .preempt_pri             = RTE_TIMER3_INT_PREEMPT_PRI,      /* timer 3 default pre-empt priority value */
    .subgrp_pri              = RTE_TIMER3_INT_SUBGRP_PRI        /* timer 3 default subgroup priority value */
};
#endif /* if (RTE_TIMER3_EN) */

#if (RTE_SYSTICK_EN)

/* Set default SYSTICK configuration */
static const TIMER_CFG_t systick_DefaultCfg = {               /* systick default configuration */
    .systick_cfg.clk_src          = RTE_SYSTICK_CLKSRC_DEFAULT, /* systick default clock source */
    .systick_cfg.reload_val       = RTE_SYSTICK_RELOAD_DEFAULT, /* systick default reload value */
};
static const TIMER_PRI_CFG_t systick_DefaultPri = {           /* systick default interrupt priorities */
    .preempt_pri             = RTE_SYSTICK_INT_PREEMPT_PRI,     /* systick default pre-empt priority value */
    .subgrp_pri              = RTE_SYSTICK_INT_SUBGRP_PRI       /* systick default subgroup priority value */
};
#endif /* if (RTE_SYSTICK_EN) */

/* Timer run-time information */
static TIMER_INFO_t TIMER_Info = {
    .flags = 0,                                                 /* timer flags */
#if (RTE_TIMER0_EN)
    .default_cfg[0]     = &timer0_DefaultCfg,                   /* timer0 default configuration */
    .default_pri_cfg[0] = &timer0_DefaultPri,                   /* timer0 default interrupt priorities */
#endif /* if (RTE_TIMER0_EN) */
#if (RTE_TIMER1_EN)
    .default_cfg[1]     = &timer1_DefaultCfg,                   /* timer1 default configuration */
    .default_pri_cfg[1] = &timer1_DefaultPri,                   /* timer1 default interrupt priorities */
#endif /* if (RTE_TIMER1_EN) */
#if (RTE_TIMER2_EN)
    .default_cfg[2]     = &timer2_DefaultCfg,                   /* timer2 default configuration */
    .default_pri_cfg[2] = &timer2_DefaultPri,                   /* timer2 default interrupt priorities */
#endif /* if (RTE_TIMER2_EN) */
#if (RTE_TIMER3_EN)
    .default_cfg[3]     = &timer3_DefaultCfg,                   /* timer3 default configuration */
    .default_pri_cfg[3] = &timer3_DefaultPri,                   /* timer3 default interrupt priorities */
#endif /* if (RTE_TIMER3_EN) */
#if (RTE_SYSTICK_EN)
    .default_cfg[4]     = &systick_DefaultCfg,                  /* systick default configuration */
    .default_pri_cfg[4] = &systick_DefaultPri,                  /* timer4 default interrupt priorities */
#endif /* if (RTE_SYSTICK_EN) */
};

/* Timer resources */
static TIMER_RESOURCES_t TIMER_Resources =
{
    .info = &TIMER_Info,                                        /* timer run-time information */
    .intInfo = {                                                /* timer interrupt info */
        .irqn[TIMER_0]       = TIMER0_IRQn,                 /* timer0 interrupt number */
        .irqn[TIMER_1]       = TIMER1_IRQn,                 /* timer1 interrupt number */
        .irqn[TIMER_2]       = TIMER2_IRQn,                 /* timer2 interrupt number */
        .irqn[TIMER_3]       = TIMER3_IRQn,                 /* timer3 interrupt number */
        .irqn[TIMER_SYSTICK] = SysTick_IRQn,                /* systick interrupt number */
        .cb = 0                                                 /* timer interrupt handler */
    }
};

/* ----------------------------------------------------------------------------
 * Function      : void TIMER_GetVersion (void)
 * ----------------------------------------------------------------------------
 * Description   : Get driver version
 * Inputs        : None
 * Outputs       : ARM_DRIVER_VERSION
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_DRIVER_VERSION TIMER_GetVersion (void)
{
    /* Return driver version */
    return DriverVersion;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t TIMER_Configure (TIMER_SEL_t sel, const TIMER_CONFIG_t * cfg)
 * ----------------------------------------------------------------------------
 * Description   : Initialize timer
 * Inputs        : sel      - timer to be configured
 *               : cfg      - timer configuration
 * Outputs       : ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t TIMER_Configure (TIMER_SEL_t sel, const TIMER_CFG_t *cfg)
{
    /* Check if correct timer was selected */
    if (!((TIMER_FLAG_BIT_SET << sel) & TIMER_EN_MSK))
    {
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

#if (RTE_SYSTICK_EN)

    /* Configure SYSTICK if SYSTICK was selected */
    if (sel == TIMER_SYSTICK)
    {
        /* Set the clock source */
        SysTick->CTRL = cfg->systick_cfg.clk_src << SysTick_CTRL_CLKSOURCE_Pos;

        /* Set the reload value */
        SysTick->LOAD = cfg->systick_cfg.reload_val;
    }
#endif /* if (RTE_SYSTICK_EN) */

#if (TIMER_REG_EN_MSK)

    /* Configure timer if timer was selected */
    if (sel != TIMER_SYSTICK)
    {
        /* Prepare config Word */
        uint32_t cfgWord = (cfg->timer_cfg.multi_cnt    << TIMER_CFG_MULTI_COUNT_Pos) |
                           (cfg->timer_cfg.mode         << TIMER_CFG_MODE_Pos)        |
                           (cfg->timer_cfg.clk_src      << TIMER_CFG_CLK_SRC_Pos)     |
                           (cfg->timer_cfg.prescale_val << TIMER_CFG_PRESCALE_Pos)    |
                           (cfg->timer_cfg.timeout_val  << TIMER_CFG_TIMEOUT_VALUE_Pos);

        /* Set the configuration word */
        Sys_Timer_Set_Control(sel, cfgWord);
    }
#endif /* if (TIMER_REG_EN_MSK) */

    /* Indicate that TIMERx was Initialized */
    TIMER_Resources.info->flags |= TIMER_FLAG_BIT_SET << sel;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_tER_SetInterruptPriority(TIMER_SEL_t sel, const TIMER_PRI_CFG * cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure timer interrupt priority.
 * Inputs        : sel - timer to be configured
 *                 cfg - interrupt priority configuration
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t TIMER_SetInterruptPriority(TIMER_SEL_t sel, const TIMER_PRI_CFG_t *cfg)
{
    /* Encoded priority value */
    uint32_t encodedPri = 0;

    /* Encode priority configuration word */
    encodedPri = NVIC_EncodePriority(NVIC_GetPriorityGrouping(), cfg->preempt_pri, cfg->subgrp_pri);

    /* Set the timer priority */
    NVIC_SetPriority(TIMER_Resources.intInfo.irqn[sel], encodedPri);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t TIMER_Initialize(TIMER_SignalEvent_t cb)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the timer driver
 * Inputs        : cb - callback function to be called on timer event
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t TIMER_Initialize(TIMER_SignalEvent_t cb)
{
    /* Configure each enabled timer module */
    for (int i = 0; i < TIMER_TIMERS_NUMBER; ++i)
    {
        /* Check if particular timer should be initialized */
        if ((TIMER_FLAG_BIT_SET << i) & TIMER_EN_MSK)
        {
            /* Configure particular timer */
            TIMER_Configure(i, TIMER_Resources.info->default_cfg[i]);

            /* Set the particuar timer interrupts priority */
            TIMER_SetInterruptPriority(i, TIMER_Resources.info->default_pri_cfg[i]);

            /* Indicate that timer was Initialized */
            TIMER_Resources.info->flags |= TIMER_FLAG_BIT_SET << i;
        }
    }

    /* Set the callback function */
    TIMER_Resources.intInfo.cb = cb;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * static int32_t TIMER_Start (TIMER_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Start the particular timer
 * Inputs        : sel - timer to be started
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t TIMER_Start(TIMER_SEL_t sel)
{
    /* Check if correct timer was selected */
    if (!((TIMER_FLAG_BIT_SET << sel) & TIMER_EN_MSK))
    {
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

#if (RTE_SYSTICK_EN)
    /* Check if systick was selected */
    if (sel == TIMER_SYSTICK)
    {
        /* Enable systick interrupt and systick Timer*/
        SysTick->CTRL |= (1 << SysTick_CTRL_TICKINT_Pos) | (1 << SysTick_CTRL_ENABLE_Pos);
    }
#endif /* if (RTE_SYSTICK_EN) */

#if (TIMER_REG_EN_MSK)
    /* Check if systick was selected */
    if (sel != TIMER_SYSTICK)
    {
        /* Clear pending flag */
        NVIC_ClearPendingIRQ(TIMER_Resources.intInfo.irqn[sel]);

        /* Enable the interrupt */
        NVIC_EnableIRQ(TIMER_Resources.intInfo.irqn[sel]);

        /* Start the timer */
        Sys_Timers_Start((1U << sel));
    }
#endif /* if (TIMER_REG_EN_MSK) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * static int32_t TIMER_Stop (TIMER_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Stop the particular timer
 * Inputs        : sel - timer to be stopped
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t TIMER_Stop(TIMER_SEL_t sel)
{
    /* Check if correct timer was selected */
    if (!((TIMER_FLAG_BIT_SET << sel) & TIMER_EN_MSK))
    {
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

#if (RTE_SYSTICK_EN)
    /* Check if systick was selected */
    if (sel == TIMER_SYSTICK)
    {
        /* Disable the systick interrupt and disable the systick timer */
        SysTick->CTRL &= ~((1 << SysTick_CTRL_TICKINT_Pos) | (1 << SysTick_CTRL_ENABLE_Pos));
    }
#endif /* if (RTE_SYSTICK_EN) */

#if (TIMER_REG_EN_MSK)
    /* Check if systick was selected */
    if (sel != TIMER_SYSTICK)
    {
        /* Disable the interrupt */
        NVIC_DisableIRQ(TIMER_Resources.intInfo.irqn[sel]);

        /* Clear pending flag */
        NVIC_ClearPendingIRQ(TIMER_Resources.intInfo.irqn[sel]);

        /* Stop the timer */
        Sys_Timers_Stop((1U << sel));
    }
#endif /* if (TIMER_REG_EN_MSK) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t TIMER_SetValue(uint32_t sel, uint32_t val)
 * ----------------------------------------------------------------------------
 * Description   : Set value of the timer / systick
 * Inputs        : sel - Number of the timer to be read.
 *               : val - timer / systick value
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t TIMER_SetValue(TIMER_SEL_t sel, uint32_t val)
{
    /* Check if correct timer was selected */
    if (!((TIMER_FLAG_BIT_SET << sel) & TIMER_EN_MSK))
    {
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

#if (RTE_SYSTICK_EN)
    /* Check if systick was selected */
    if (sel == TIMER_SYSTICK)
    {
        /* Set systick reload value */
        SysTick->VAL = val;
    }
#endif /* if (RTE_SYSTICK_EN) */

#if (TIMER_REG_EN_MSK)
    /* Check if systick was selected */
    if (sel != TIMER_SYSTICK)
    {
        /* Set systick timer value */
        TIMER->CFG[sel] &= ~TIMER_CFG_TIMEOUT_VALUE_Mask;
        TIMER->CFG[sel] |= val << TIMER_CFG_TIMEOUT_VALUE_Pos;
    }
#endif /* if (TIMER_REG_EN_MSK) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t TIMER_GetValue(uint32_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Get value of the timer
 * Inputs        : sel - Number of the timer to be read.
 * Outputs       : timer value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t TIMER_GetValue(TIMER_SEL_t sel)
{
    /* Check if correct timer was selected */
    if (!((TIMER_FLAG_BIT_SET << sel) & TIMER_EN_MSK))
    {
        return 0;
    }

#if (RTE_SYSTICK_EN)
    /* Check if systick was selected */
    if (sel == TIMER_SYSTICK)
    {
        /* Return current systick value */
        return SysTick->VAL;
    }
#endif /* if (RTE_SYSTICK_EN) */

#if (TIMER_REG_EN_MSK)
    /* Check if systick was selected */
    if (sel != TIMER_SYSTICK)
    {
        /* Return the current timer value */
        return TIMER->VAL[sel];
    }
#endif /* if (TIMER_REG_EN_MSK) */

    /* To avoid no return warning - should never be here */
    return 0;
}

/* ----------------------------------------------------------------------------
 * static uint32_t TIMER_GetSysTickState (void)
 * ----------------------------------------------------------------------------
 * Description   : Get the current systick value
 * Inputs        : None
 * Outputs       : 1 if systick has already reached 0, otherwise 0
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t TIMER_GetSysTickState(void)
{
#if (RTE_SYSTICK_EN)

    /* Return 1 if SYSTICK has already reached 0 */
    return (SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) >> SysTick_CTRL_COUNTFLAG_Pos;
#else  /* if (RTE_SYSTICK_EN) */

    /* Return 0 as timer is not supported */
    return 0;
#endif /* if (RTE_SYSTICK_EN) */
}

#if (RTE_TIMER0_EN)
/* ----------------------------------------------------------------------------
 * Function      : void TIMER0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a timer event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER0_IRQHandler(void)
{
    /* Disable TIMER interrupt */
    NVIC_DisableIRQ(TIMER_Resources.intInfo.irqn[TIMER_0]);

    /* Check if callback was set */
    if (TIMER_Resources.intInfo.cb)
    {
        /* Execute application callback */
        TIMER_Resources.intInfo.cb(TIMER_TIMER0_EVENT);
    }

    /* Clear pending flag */
    NVIC_ClearPendingIRQ(TIMER_Resources.intInfo.irqn[TIMER_0]);

    /* Enable the interrupt */
    NVIC_EnableIRQ(TIMER_Resources.intInfo.irqn[TIMER_0]);
}

#endif /* if (RTE_TIMER0_EN) */

#if (RTE_TIMER1_EN)
/* ----------------------------------------------------------------------------
 * Function      : void TIMER1_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a timer event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER1_IRQHandler(void)
{
    /* Disable TIMER interrupt */
    NVIC_DisableIRQ(TIMER_Resources.intInfo.irqn[TIMER_1]);

    /* Check if callback was set */
    if (TIMER_Resources.intInfo.cb)
    {
        /* Execute application callback */
        TIMER_Resources.intInfo.cb(TIMER_TIMER1_EVENT);
    }

    /* Clear pending flag */
    NVIC_ClearPendingIRQ(TIMER_Resources.intInfo.irqn[TIMER_1]);

    /* Enable the interrupt */
    NVIC_EnableIRQ(TIMER_Resources.intInfo.irqn[TIMER_1]);
}

#endif /* if (RTE_TIMER1_EN) */

#if (RTE_TIMER2_EN)
/* ----------------------------------------------------------------------------
 * Function      : void TIMER2_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a timer event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER2_IRQHandler(void)
{
    /* Disable TIMER interrupt */
    NVIC_DisableIRQ(TIMER_Resources.intInfo.irqn[TIMER_2]);

    /* Check if callback was set */
    if (TIMER_Resources.intInfo.cb)
    {
        /* Execute application callback */
        TIMER_Resources.intInfo.cb(TIMER_TIMER2_EVENT);
    }

    /* Clear pending flag */
    NVIC_ClearPendingIRQ(TIMER_Resources.intInfo.irqn[TIMER_2]);

    /* Enable the interrupt */
    NVIC_EnableIRQ(TIMER_Resources.intInfo.irqn[TIMER_2]);
}

#endif /* if (RTE_TIMER2_EN) */

#if (RTE_TIMER3_EN)
/* ----------------------------------------------------------------------------
 * Function      : void TIMER3_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a timer event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER3_IRQHandler(void)
{
    /* Disable TIMER interrupt */
    NVIC_DisableIRQ(TIMER_Resources.intInfo.irqn[TIMER_3]);

    /* Check if callback was set */
    if (TIMER_Resources.intInfo.cb)
    {
        /* Execute application callback */
        TIMER_Resources.intInfo.cb(TIMER_TIMER3_EVENT);
    }

    /* Clear pending flag */
    NVIC_ClearPendingIRQ(TIMER_Resources.intInfo.irqn[TIMER_3]);

    /* Enable the interrupt */
    NVIC_EnableIRQ(TIMER_Resources.intInfo.irqn[TIMER_3]);
}

#endif /* if (RTE_TIMER3_EN) */

#if (RTE_SYSTICK_EN)
/* ----------------------------------------------------------------------------
 * Function      : void SysTick_Handler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a systick event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SysTick_Handler(void)
{
    /* Check if callback was set */
    if (TIMER_Resources.intInfo.cb)
    {
        /* Execute application callback */
        TIMER_Resources.intInfo.cb(TIMER_SYSTICK_EVENT);
    }
}

#endif /* if (RTE_SYSTICK_EN) */

/* TIMER Driver Control Block */
DRIVER_TIMER_t Driver_TIMER =
{
    TIMER_GetVersion,
    TIMER_Initialize,
    TIMER_Configure,
    TIMER_SetInterruptPriority,
    TIMER_Start,
    TIMER_Stop,
    TIMER_SetValue,
    TIMER_GetValue,
    TIMER_GetSysTickState
};

#endif    /* RTE_TIMER */
