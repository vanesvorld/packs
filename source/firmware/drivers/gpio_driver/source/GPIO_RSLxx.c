/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * GPIO_RSLxx.c
 * - GPIO driver implementation for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#include <GPIO_RSLxx.h>

#if RTE_GPIO

#define ARM_GPIO_DRV_VERSION    ARM_DRIVER_VERSION_MAJOR_MINOR(0, 1)

/* Driver Version */
static const ARM_DRIVER_VERSION DriverVersion =
{
    ARM_GPIO_API_VERSION,
    ARM_GPIO_DRV_VERSION
};

/* Set default gpio main configuration */
static const GPIO_CFG_t gpio_DefaultCfg = {                      /* gpio default main configuration */
    .drive_strengths         = RTE_GPIO_DRIVE_STRENGTHS_DEFAULT, /* gpio default pads drive strength */
    .debounce_cfg.count      = RTE_GPIO_INT_DBC_CNT_DEFAULT,     /* gpio default debounce filter counter value */
    .debounce_cfg.clk_source = RTE_GPIO_INT_DBC_CLK_SRC_DEFAULT  /* gpio default debounce filter clock source */
};

#if (RTE_GPIO_EXTCLK_EN)

/* Set default gpio external clock configuration */
static const GPIO_EXTCLK_CFG_t gpio_ExtClkDefaultCfg = {         /* gpio default external clock configuration */
    .pull_mode               = RTE_GPIO_EXTCLK_PULL_DEFAULT,     /* gpio default external clock pull control */
    .lpf_en                  = RTE_GPIO_EXTCLK_LPF_DEFAULT       /* gpio default external clock low pass filter config */
};
#endif /* if (RTE_GPIO_EXTCLK_EN) */

#if (RTE_GPIO_NMI_EN)

/* Set default gpio nmi configuration */
static const GPIO_NMI_CFG_t gpio_NmiDefaultCfg = {               /* gpio default nmi configuration */
    .nmi_pol                 = RTE_GPIO_NMI_POL_DEFAULT          /* gpio default nmi polarity */
};
#endif /* if (RTE_GPIO_NMI_EN) */

#if (RTE_GPIO_JTAG_EN)

/* Set default gpio jtag configuration */
static const GPIO_JTAG_CFG_t gpio_JtagDefaultCfg = {             /* gpio default jtag configuration */
    .jtck_lpf_en             = RTE_GPIO_JTAG_JTCK_LPF_DEFAULT,   /* gpio default jtck low pass filter enable */
    .jtms_lpf_en             = RTE_GPIO_JTAG_JTMS_LPF_DEFAULT,   /* gpio default jtms low pass filter enable */
    .jtag_data_en            = RTE_GPIO_JTAG_DATA_DEFAULT,       /* gpio default jtag data on DIO[14:15] available */
    .jtag_trst_en            = RTE_GPIO_JTAG_TRST_DEFAULT,       /* gpio default jtag trst on DIO13 available */
    .jtck_pull               = RTE_GPIO_JTAG_JTCK_PULL_DEFAULT,  /* gpio default jtck pull mode */
    .jtms_pull               = RTE_GPIO_JTAG_JTMS_PULL_DEFAULT,  /* gpio default jtms pull mode */
    .jtms_drive              = RTE_GPIO_JTAG_JTMS_DRIVE_DEFAULT  /* gpio default jtms drive mode */
};
#endif /* if (RTE_GPIO_JTAG_EN) */

#if (RTE_GPIO0_EN)

/* Set default gpio 0 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio0DefaultCfg = {             /* gpio 0 default configuration */
    .pull_mode               = RTE_GPIO0_PULL_DEFAULT,           /* gpio 0 default pull mode */
    .drive_mode              = RTE_GPIO0_DRIVE_DEFAULT,          /* gpio 0 default drive mode */
    .lpf_en                  = RTE_GPIO0_LPF_DEFAULT,            /* gpio 0 default low pass filter enable */
    .io_mode                 = RTE_GPIO0_IOMODE_DEFAULT,         /* gpio 0 default io mode */
};
#endif /* if (RTE_GPIO0_EN) */

#if (RTE_GPIO1_EN)

/* Set default gpio 1 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio1DefaultCfg = {             /* gpio 1 default configuration */
    .pull_mode               = RTE_GPIO1_PULL_DEFAULT,           /* gpio 1 default pull mode */
    .drive_mode              = RTE_GPIO1_DRIVE_DEFAULT,          /* gpio 1 default drive mode */
    .lpf_en                  = RTE_GPIO1_LPF_DEFAULT,            /* gpio 1 default low pass filter enable */
    .io_mode                 = RTE_GPIO1_IOMODE_DEFAULT,         /* gpio 1 default io mode */
};
#endif /* if (RTE_GPIO1_EN) */

#if (RTE_GPIO2_EN)

/* Set default gpio 2 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio2DefaultCfg = {             /* gpio 2 default configuration */
    .pull_mode               = RTE_GPIO2_PULL_DEFAULT,           /* gpio 2 default pull mode */
    .drive_mode              = RTE_GPIO2_DRIVE_DEFAULT,          /* gpio 2 default drive mode */
    .lpf_en                  = RTE_GPIO2_LPF_DEFAULT,            /* gpio 2 default low pass filter enable */
    .io_mode                 = RTE_GPIO2_IOMODE_DEFAULT,         /* gpio 2 default io mode */
};
#endif /* if (RTE_GPIO2_EN) */

#if (RTE_GPIO3_EN)

/* Set default gpio 3 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio3DefaultCfg = {             /* gpio 3 default configuration */
    .pull_mode               = RTE_GPIO3_PULL_DEFAULT,           /* gpio 3 default pull mode */
    .drive_mode              = RTE_GPIO3_DRIVE_DEFAULT,          /* gpio 3 default drive mode */
    .lpf_en                  = RTE_GPIO3_LPF_DEFAULT,            /* gpio 3 default low pass filter enable */
    .io_mode                 = RTE_GPIO3_IOMODE_DEFAULT,         /* gpio 3 default io mode */
};
#endif /* if (RTE_GPIO3_EN) */

#if (RTE_GPIO4_EN)

/* Set default gpio 4 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio4DefaultCfg = {             /* gpio 4 default configuration */
    .pull_mode               = RTE_GPIO4_PULL_DEFAULT,           /* gpio 4 default pull mode */
    .drive_mode              = RTE_GPIO4_DRIVE_DEFAULT,          /* gpio 4 default drive mode */
    .lpf_en                  = RTE_GPIO4_LPF_DEFAULT,            /* gpio 4 default low pass filter enable */
    .io_mode                 = RTE_GPIO4_IOMODE_DEFAULT,         /* gpio 4 default io mode */
};
#endif /* if (RTE_GPIO4_EN) */

#if (RTE_GPIO5_EN)

/* Set default gpio 5 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio5DefaultCfg = {             /* gpio 5 default configuration */
    .pull_mode               = RTE_GPIO5_PULL_DEFAULT,           /* gpio 5 default pull mode */
    .drive_mode              = RTE_GPIO5_DRIVE_DEFAULT,          /* gpio 5 default drive mode */
    .lpf_en                  = RTE_GPIO5_LPF_DEFAULT,            /* gpio 5 default low pass filter enable */
    .io_mode                 = RTE_GPIO5_IOMODE_DEFAULT,         /* gpio 5 default io mode */
};
#endif /* if (RTE_GPIO5_EN) */

#if (RTE_GPIO6_EN)

/* Set default gpio 6 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio6DefaultCfg = {             /* gpio 6 default configuration */
    .pull_mode               = RTE_GPIO6_PULL_DEFAULT,           /* gpio 6 default pull mode */
    .drive_mode              = RTE_GPIO6_DRIVE_DEFAULT,          /* gpio 6 default drive mode */
    .lpf_en                  = RTE_GPIO6_LPF_DEFAULT,            /* gpio 6 default low pass filter enable */
    .io_mode                 = RTE_GPIO6_IOMODE_DEFAULT,         /* gpio 6 default io mode */
};
#endif /* if (RTE_GPIO6_EN) */

#if (RTE_GPIO7_EN)

/* Set default gpio 7 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio7DefaultCfg = {             /* gpio 7 default configuration */
    .pull_mode               = RTE_GPIO7_PULL_DEFAULT,           /* gpio 7 default pull mode */
    .drive_mode              = RTE_GPIO7_DRIVE_DEFAULT,          /* gpio 7 default drive mode */
    .lpf_en                  = RTE_GPIO7_LPF_DEFAULT,            /* gpio 7 default low pass filter enable */
    .io_mode                 = RTE_GPIO7_IOMODE_DEFAULT,         /* gpio 7 default io mode */
};
#endif /* if (RTE_GPIO7_EN) */

#if (RTE_GPIO8_EN)

/* Set default gpio 8 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio8DefaultCfg = {             /* gpio 8 default configuration */
    .pull_mode               = RTE_GPIO8_PULL_DEFAULT,           /* gpio 8 default pull mode */
    .drive_mode              = RTE_GPIO8_DRIVE_DEFAULT,          /* gpio 8 default drive mode */
    .lpf_en                  = RTE_GPIO8_LPF_DEFAULT,            /* gpio 8 default low pass filter enable */
    .io_mode                 = RTE_GPIO8_IOMODE_DEFAULT,         /* gpio 8 default io mode */
};
#endif /* if (RTE_GPIO8_EN) */

#if (RTE_GPIO9_EN)

/* Set default gpio 9 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio9DefaultCfg = {             /* gpio 9 default configuration */
    .pull_mode               = RTE_GPIO9_PULL_DEFAULT,           /* gpio 9 default pull mode */
    .drive_mode              = RTE_GPIO9_DRIVE_DEFAULT,          /* gpio 9 default drive mode */
    .lpf_en                  = RTE_GPIO9_LPF_DEFAULT,            /* gpio 9 default low pass filter enable */
    .io_mode                 = RTE_GPIO9_IOMODE_DEFAULT,         /* gpio 9 default io mode */
};
#endif /* if (RTE_GPIO9_EN) */

#if (RTE_GPIO10_EN)

/* Set default gpio 10 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio10DefaultCfg = {            /* gpio 10 default configuration */
    .pull_mode               = RTE_GPIO10_PULL_DEFAULT,          /* gpio 10 default pull mode */
    .drive_mode              = RTE_GPIO10_DRIVE_DEFAULT,         /* gpio 10 default drive mode */
    .lpf_en                  = RTE_GPIO10_LPF_DEFAULT,           /* gpio 10 default low pass filter enable */
    .io_mode                 = RTE_GPIO10_IOMODE_DEFAULT,        /* gpio 10 default io mode */
};
#endif /* if (RTE_GPIO10_EN) */

#if (RTE_GPIO11_EN)

/* Set default gpio 11 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio11DefaultCfg = {            /* gpio 11 default configuration */
    .pull_mode               = RTE_GPIO11_PULL_DEFAULT,          /* gpio 11 default pull mode */
    .drive_mode              = RTE_GPIO11_DRIVE_DEFAULT,         /* gpio 11 default drive mode */
    .lpf_en                  = RTE_GPIO11_LPF_DEFAULT,           /* gpio 11 default low pass filter enable */
    .io_mode                 = RTE_GPIO11_IOMODE_DEFAULT,        /* gpio 11 default io mode */
};
#endif /* if (RTE_GPIO11_EN) */

#if (RTE_GPIO12_EN)

/* Set default gpio 12 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio12DefaultCfg = {            /* gpio 12 default configuration */
    .pull_mode               = RTE_GPIO12_PULL_DEFAULT,          /* gpio 12 default pull mode */
    .drive_mode              = RTE_GPIO12_DRIVE_DEFAULT,         /* gpio 12 default drive mode */
    .lpf_en                  = RTE_GPIO12_LPF_DEFAULT,           /* gpio 12 default low pass filter enable */
    .io_mode                 = RTE_GPIO12_IOMODE_DEFAULT,        /* gpio 12 default io mode */
};
#endif /* if (RTE_GPIO12_EN) */

#if (RTE_GPIO13_EN)

/* Set default gpio 13 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio13DefaultCfg = {            /* gpio 13 default configuration */
    .pull_mode               = RTE_GPIO13_PULL_DEFAULT,          /* gpio 13 default pull mode */
    .drive_mode              = RTE_GPIO13_DRIVE_DEFAULT,         /* gpio 13 default drive mode */
    .lpf_en                  = RTE_GPIO13_LPF_DEFAULT,           /* gpio 13 default low pass filter enable */
    .io_mode                 = RTE_GPIO13_IOMODE_DEFAULT,        /* gpio 13 default io mode */
};
#endif /* if (RTE_GPIO13_EN) */

#if (RTE_GPIO14_EN)

/* Set default gpio 14 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio14DefaultCfg = {            /* gpio 14 default configuration */
    .pull_mode               = RTE_GPIO14_PULL_DEFAULT,          /* gpio 14 default pull mode */
    .drive_mode              = RTE_GPIO14_DRIVE_DEFAULT,         /* gpio 14 default drive mode */
    .lpf_en                  = RTE_GPIO14_LPF_DEFAULT,           /* gpio 14 default low pass filter enable */
    .io_mode                 = RTE_GPIO14_IOMODE_DEFAULT,        /* gpio 14 default io mode */
};
#endif /* if (RTE_GPIO14_EN) */

#if (RTE_GPIO15_EN)

/* Set default gpio 15 pad configuration */
static const GPIO_PAD_CFG_t gpio_Gpio15DefaultCfg = {            /* gpio 15 default configuration */
    .pull_mode               = RTE_GPIO15_PULL_DEFAULT,          /* gpio 15 default pull mode */
    .drive_mode              = RTE_GPIO15_DRIVE_DEFAULT,         /* gpio 15 default drive mode */
    .lpf_en                  = RTE_GPIO15_LPF_DEFAULT,           /* gpio 15 default low pass filter enable */
    .io_mode                 = RTE_GPIO15_IOMODE_DEFAULT,        /* gpio 15 default io mode */
};
#endif /* if (RTE_GPIO15_EN) */

#if (RTE_GPIO_DIO0_INT_EN)

/* Set default gpio int 0 configuration */
static const GPIO_INT_CFG_t gpio_GpioInt0DefaultCfg = {          /* gpio int 0 default configuration */
    .src_sel                 = RTE_GPIO_DIO0_SRC_DEFAULT,        /* gpio int 0 default source */
    .event                   = RTE_GPIO_DIO0_TRG_DEFAULT,        /* gpio int 0 default event */
    .debounce_en             = RTE_GPIO_DIO0_DBC_DEFAULT,        /* gpio int 0 default debounce enable */
    .interrup_en             = GPIO_ENABLE,                      /* gpio int 0 default enable flag */
};
static const GPIO_PRI_CFG_t gpio_GpioPri0DefaultCfg = {          /* gpio int 0 default priority configuration */
    .preempt_pri             = RTE_GPIO_DIO0_INT_PREEMPT_PRI,    /* gpio int 0 default preempt priority */
    .subgrp_pri              = RTE_GPIO_DIO0_INT_SUBGRP_PRI,     /* gpio int 0 default subgroup priority */
};
#endif /* if (RTE_GPIO_DIO0_INT_EN) */

#if (RTE_GPIO_DIO1_INT_EN)

/* Set default gpio int 1 configuration */
static const GPIO_INT_CFG_t gpio_GpioInt1DefaultCfg = {          /* gpio int 1 default configuration */
    .src_sel                 = RTE_GPIO_DIO1_SRC_DEFAULT,        /* gpio int 1 default source */
    .event                   = RTE_GPIO_DIO1_TRG_DEFAULT,        /* gpio int 1 default event */
    .debounce_en             = RTE_GPIO_DIO1_DBC_DEFAULT,        /* gpio int 1 default debounce enable */
    .interrup_en             = GPIO_ENABLE,                      /* gpio int 1 default enable flag */
};
static const GPIO_PRI_CFG_t gpio_GpioPri1DefaultCfg = {          /* gpio int 1 default priority configuration */
    .preempt_pri             = RTE_GPIO_DIO1_INT_PREEMPT_PRI,    /* gpio int 1 default preempt priority */
    .subgrp_pri              = RTE_GPIO_DIO1_INT_SUBGRP_PRI,     /* gpio int 1 default subgroup priority */
};
#endif /* if (RTE_GPIO_DIO1_INT_EN) */

#if (RTE_GPIO_DIO2_INT_EN)

/* Set default gpio int 2 configuration */
static const GPIO_INT_CFG_t gpio_GpioInt2DefaultCfg = {          /* gpio int 2 default configuration */
    .src_sel                 = RTE_GPIO_DIO2_SRC_DEFAULT,        /* gpio int 2 default source */
    .event                   = RTE_GPIO_DIO2_TRG_DEFAULT,        /* gpio int 2 default event */
    .debounce_en             = RTE_GPIO_DIO2_DBC_DEFAULT,        /* gpio int 2 default debounce enable */
    .interrup_en             = GPIO_ENABLE,                      /* gpio int 2 default enable flag */
};
static const GPIO_PRI_CFG_t gpio_GpioPri2DefaultCfg = {          /* gpio int 2 default priority configuration */
    .preempt_pri             = RTE_GPIO_DIO2_INT_PREEMPT_PRI,    /* gpio int 2 default preempt priority */
    .subgrp_pri              = RTE_GPIO_DIO2_INT_SUBGRP_PRI,     /* gpio int 2 default subgroup priority */
};
#endif /* if (RTE_GPIO_DIO2_INT_EN) */

#if (RTE_GPIO_DIO3_INT_EN)

/* Set default gpio int 3 configuration */
static const GPIO_INT_CFG_t gpio_GpioInt3DefaultCfg = {          /* gpio int 3 default configuration */
    .src_sel                 = RTE_GPIO_DIO3_SRC_DEFAULT,        /* gpio int 3 default source */
    .event                   = RTE_GPIO_DIO3_TRG_DEFAULT,        /* gpio int 3 default event */
    .debounce_en             = RTE_GPIO_DIO3_DBC_DEFAULT,        /* gpio int 3 default debounce enable */
    .interrup_en             = GPIO_ENABLE,                      /* gpio int 3 default enable flag */
};
static const GPIO_PRI_CFG_t gpio_GpioPri3DefaultCfg = {          /* gpio int 3 default priority configuration */
    .preempt_pri             = RTE_GPIO_DIO3_INT_PREEMPT_PRI,    /* gpio int 3 default preempt priority */
    .subgrp_pri              = RTE_GPIO_DIO3_INT_SUBGRP_PRI,     /* gpio int 3 default subgroup priority */
};
#endif /* if (RTE_GPIO_DIO3_INT_EN) */

/* Gpio run-time information */
static GPIO_INFO_t GPIO_Info = {
    .default_cfg = &gpio_DefaultCfg,
#if (RTE_GPIO_EXTCLK_EN)
    .default_extclk_cfg         = &gpio_ExtClkDefaultCfg,         /* Set default gpio external clock configuration */
#endif /* if (RTE_GPIO_EXTCLK_EN) */
#if (RTE_GPIO_NMI_EN)
    .default_nmi_cfg             = &gpio_NmiDefaultCfg,           /* Set default gpio nmi configuration */
#endif /* if (RTE_GPIO_NMI_EN) */
#if (RTE_GPIO_JTAG_EN)
    .default_jtag_cfg            = &gpio_JtagDefaultCfg,          /* Set default gpio jtag configuration */
#endif /* if (RTE_GPIO_JTAG_EN) */
#if (RTE_GPIO0_EN)
    .default_pad_cfg[GPIO_0]     = &gpio_Gpio0DefaultCfg,         /* Set default gpio 0 pad configuration */
#endif /* if (RTE_GPIO0_EN) */
#if (RTE_GPIO1_EN)
    .default_pad_cfg[GPIO_1]     = &gpio_Gpio1DefaultCfg,         /* Set default gpio 1 pad configuration */
#endif /* if (RTE_GPIO1_EN) */
#if (RTE_GPIO2_EN)
    .default_pad_cfg[GPIO_2]     = &gpio_Gpio2DefaultCfg,         /* Set default gpio 2 pad configuration */
#endif /* if (RTE_GPIO2_EN) */
#if (RTE_GPIO3_EN)
    .default_pad_cfg[GPIO_3]     = &gpio_Gpio3DefaultCfg,         /* Set default gpio 3 pad configuration */
#endif /* if (RTE_GPIO3_EN) */
#if (RTE_GPIO4_EN)
    .default_pad_cfg[GPIO_4]     = &gpio_Gpio4DefaultCfg,         /* Set default gpio 4 pad configuration */
#endif /* if (RTE_GPIO4_EN) */
#if (RTE_GPIO5_EN)
    .default_pad_cfg[GPIO_5]     = &gpio_Gpio5DefaultCfg,         /* Set default gpio 5 pad configuration */
#endif /* if (RTE_GPIO5_EN) */
#if (RTE_GPIO6_EN)
    .default_pad_cfg[GPIO_6]     = &gpio_Gpio6DefaultCfg,         /* Set default gpio 6 pad configuration */
#endif /* if (RTE_GPIO6_EN) */
#if (RTE_GPIO7_EN)
    .default_pad_cfg[GPIO_7]     = &gpio_Gpio7DefaultCfg,         /* Set default gpio 7 pad configuration */
#endif /* if (RTE_GPIO7_EN) */
#if (RTE_GPIO8_EN)
    .default_pad_cfg[GPIO_8]     = &gpio_Gpio8DefaultCfg,         /* Set default gpio 8 pad configuration */
#endif /* if (RTE_GPIO8_EN) */
#if (RTE_GPIO9_EN)
    .default_pad_cfg[GPIO_9]     = &gpio_Gpio9DefaultCfg,         /* Set default gpio 9 pad configuration */
#endif /* if (RTE_GPIO9_EN) */
#if (RTE_GPIO10_EN)
    .default_pad_cfg[GPIO_10]    = &gpio_Gpio10DefaultCfg,        /* Set default gpio 10 pad configuration */
#endif /* if (RTE_GPIO10_EN) */
#if (RTE_GPIO11_EN)
    .default_pad_cfg[GPIO_11]    = &gpio_Gpio11DefaultCfg,        /* Set default gpio 11 pad configuration */
#endif /* if (RTE_GPIO11_EN) */
#if (RTE_GPIO12_EN)
    .default_pad_cfg[GPIO_12]    = &gpio_Gpio12DefaultCfg,        /* Set default gpio 12 pad configuration */
#endif /* if (RTE_GPIO12_EN) */
#if (RTE_GPIO13_EN)
    .default_pad_cfg[GPIO_13]    = &gpio_Gpio13DefaultCfg,        /* Set default gpio 13 pad configuration */
#endif /* if (RTE_GPIO13_EN) */
#if (RTE_GPIO14_EN)
    .default_pad_cfg[GPIO_14]    = &gpio_Gpio14DefaultCfg,        /* Set default gpio 14 pad configuration */
#endif /* if (RTE_GPIO14_EN) */
#if (RTE_GPIO15_EN)
    .default_pad_cfg[GPIO_15]    = &gpio_Gpio15DefaultCfg,        /* Set default gpio 15 pad configuration */
#endif /* if (RTE_GPIO15_EN) */
#if (RTE_GPIO_DIO0_INT_EN)
    .default_int_cfg[GPIO_INT_0] = &gpio_GpioInt0DefaultCfg,      /* Set default gpio int 0 configuration */
    .default_pri_cfg[GPIO_INT_0] = &gpio_GpioPri0DefaultCfg,      /* Set default gpio int 0 priority configuration */
#endif /* if (RTE_GPIO_DIO0_INT_EN) */
#if (RTE_GPIO_DIO1_INT_EN)
    .default_int_cfg[GPIO_INT_1] = &gpio_GpioInt1DefaultCfg,      /* Set default gpio int 1 configuration */
    .default_pri_cfg[GPIO_INT_1] = &gpio_GpioPri1DefaultCfg,      /* Set default gpio int 1 priority configuration */
#endif /* if (RTE_GPIO_DIO1_INT_EN) */
#if (RTE_GPIO_DIO2_INT_EN)
    .default_int_cfg[GPIO_INT_2] = &gpio_GpioInt2DefaultCfg,      /* Set default gpio int 2 configuration */
    .default_pri_cfg[GPIO_INT_2] = &gpio_GpioPri2DefaultCfg,      /* Set default gpio int 2 priority configuration */
#endif /* if (RTE_GPIO_DIO2_INT_EN) */
#if (RTE_GPIO_DIO3_INT_EN)
    .default_int_cfg[GPIO_INT_3] = &gpio_GpioInt3DefaultCfg,      /* Set default gpio int 3 configuration */
    .default_pri_cfg[GPIO_INT_3] = &gpio_GpioPri3DefaultCfg,      /* Set default gpio int 3 priority configuration */
#endif /* if (RTE_GPIO_DIO3_INT_EN) */
};

/* GPIO resources */
static GPIO_RESOURCES GPIO_Resources =
{
    .info = &GPIO_Info,                                           /* gpio run-time information */
#if (GPIO_INT_EN_MSK)
    .intInfo = {                                                  /* gpio interrupt info */
        .irqn[GPIO_INT_0]        = DIO0_IRQn,                     /* gpio 0 interrupt number */
        .irqn[GPIO_INT_1]        = DIO1_IRQn,                     /* gpio 1 interrupt number */
        .irqn[GPIO_INT_2]        = DIO2_IRQn,                     /* gpio 2 interrupt number */
        .irqn[GPIO_INT_3]        = DIO3_IRQn,                     /* gpio 3 interrupt number */
        .cb = 0                                                   /* gpio interrupt handler */
    }

#endif /* if (GPIO_INT_EN_MSK) */
};

/* ----------------------------------------------------------------------------
 * Function      : ARM_DRIVER_VERSION GPIO_GetVersion (void)
 * ----------------------------------------------------------------------------
 * Description   : Driver version
 * Inputs        : None
 * Outputs       : ARM_DRIVER_VERSION
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_DRIVER_VERSION GPIO_GetVersion (void)
{
    /* Return the driver version */
    return DriverVersion;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_Configure (const GPIO_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure common gpio settings
 * Inputs        : cfg - Pointer to gpio configuration structure
 * Outputs       : int32_t  - ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_Configure (const GPIO_CFG_t *cfg)
{
    /* Set the debounce configuration */
    DIO->INT_DEBOUNCE = (cfg->debounce_cfg.clk_source << DIO_INT_DEBOUNCE_DEBOUNCE_CLK_Pos) |
                        (cfg->debounce_cfg.count      << DIO_INT_DEBOUNCE_DEBOUNCE_COUNT_Pos);

    /* Set the gpio drive strengths */
    DIO->PAD_CFG = cfg->drive_strengths << DIO_PAD_CFG_DRIVE_Pos;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_ConfigureExtClk (const GPIO_EXTCLK_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the gpio external clock mode
 * Inputs        : cfg - Pointer to extclk configuration structure
 * Outputs       : ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_ConfigureExtClk (const GPIO_EXTCLK_CFG_t *cfg)
{
    /* Set the extclk configuration */
    DIO->EXTCLK_CFG = (cfg->lpf_en    << DIO_EXTCLK_CFG_LPF_Pos) |
                      (cfg->pull_mode << DIO_EXTCLK_CFG_PULL_CTRL_Pos);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_ConfigureNmi (GPIO_NMI_POL_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the nmi polarity
 * Inputs        : cfg - Nmi polarity to be set
 * Outputs       : ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_ConfigureNmi (GPIO_NMI_POL_t cfg)
{
    /* Set the nmi polarity */
    DIO_NMI_SRC->NMI_POLARITY_ALIAS = DIO_NMI_SRC_NMI_POLARITY_Pos;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_ConfigureJtag (const GPIO_JTAG_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the gpio jtag mode
 * Inputs        : cfg - Pointer to jtag configuration structure
 * Outputs       : ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_ConfigureJtag (const GPIO_JTAG_CFG_t *cfg)
{
    /* Set the jtag configuration */
    DIO->JTAG_SW_PAD_CFG = (cfg->jtag_data_en << DIO_JTAG_SW_PAD_CFG_CM3_JTAG_DATA_EN_Pos) |
                           (cfg->jtag_trst_en << DIO_JTAG_SW_PAD_CFG_CM3_JTAG_TRST_EN_Pos) |
                           (cfg->jtck_lpf_en  << DIO_JTAG_SW_PAD_CFG_JTCK_LPF_Pos)         |
                           (cfg->jtck_pull    << DIO_JTAG_SW_PAD_CFG_JTCK_PULL_Pos)        |
                           (cfg->jtms_drive   << DIO_JTAG_SW_PAD_CFG_JTMS_DRIVE_Pos)       |
                           (cfg->jtms_lpf_en  << DIO_JTAG_SW_PAD_CFG_JTMS_LPF_Pos)         |
                           (cfg->jtms_pull    << DIO_JTAG_SW_PAD_CFG_JTMS_PULL_Pos);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_ConfigurePad (GPIO_SEL_t sel, const GPIO_PAD_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the gpio pad
 * Inputs        : sel - Pad selection
 *               : cfg - Pointer to pad configuration structure
 * Outputs       : ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_ConfigurePad (GPIO_SEL_t sel, const GPIO_PAD_CFG_t *cfg)
{
    /* Prepare gpio pad config word */
    uint32_t padCfg = (cfg->drive_mode << DIO_CFG_DRIVE_Pos)   |
                      (cfg->lpf_en     << DIO_CFG_LPF_Pos)     |
                      (cfg->pull_mode  << DIO_CFG_PULL_CTRL_Pos);

    /* Check if input IO has been selected */
    if (cfg->io_mode & GPIO_INPUT_MODE_MASK)
    {
        /* Set the io mode to input */
        DIO->CFG[sel] = padCfg | DIO_MODE_INPUT;

        /* Check which input mode has been selected */
        switch (cfg->io_mode)
        {
            case GPIO_MODE_PCM_SERI_IN:
            {
                /* Set the io mode to gpio in 0 */
                DIO->CFG[sel] = padCfg | DIO_MODE_GPIO_IN_0;

                /* Set pcm seri src */
                DIO_PCM_SRC->SERI_BYTE = sel;
                break;
            }            

            case GPIO_MODE_PCM_FRAME_IN:
            {
                /* Set pcm frame src */
                DIO_PCM_SRC->FRAME_BYTE = sel;
                break;
            }           

            case GPIO_MODE_PCM_FRAME_OUT:
            {
                /* Set the io mode to frame */
                DIO->CFG[sel] = padCfg | DIO_MODE_PCM_FRAME;

                /* Set pcm frame src */
                DIO_PCM_SRC->FRAME_BYTE = sel;
                break;
            }            

            case GPIO_MODE_PCM_CLK_IN:
            {
                /* Set pcm clk src */
                DIO_PCM_SRC->CLK_BYTE = sel;
                break;
            }            

            case GPIO_MODE_SPI0_SERI_IN:
            {
                /* Set spi0 seri src */
                DIO_SPI_SRC->SERI_BYTE = sel;
                break;
            }            

            case GPIO_MODE_SPI0_CS_IN:
            {
                /* Set spi0 cs src */
                DIO_SPI_SRC->CS_BYTE = sel;
                break;
            }            

            case GPIO_MODE_SPI0_CLK_IN:
            {
                /* Set spi0 clk src */
                DIO_SPI_SRC->CLK_BYTE = sel;
                break;
            }          

            case GPIO_MODE_SPI1_SERI_IN:
            {
                /* Set spi1 seri src */
                DIO_SPI_SRC1->SERI_BYTE = sel;
                break;
            }          

            case GPIO_MODE_SPI1_CS_IN:
            {
                /* Set spi1 cs src */
                DIO_SPI_SRC1->CS_BYTE = sel;
                break;
            }            

            case GPIO_MODE_SPI1_CLK_IN:
            {
                /* Set spi1 clk src */
                DIO_SPI_SRC1->CLK_BYTE = sel;
                break;
            }           

            case GPIO_MODE_UART_RX_IN:
            {
                /* Set uart rx src */
                DIO->UART_SRC &= ~(DIO_UART_SRC_RX_Mask << DIO_UART_SRC_RX_Pos);
                DIO->UART_SRC |= sel << DIO_UART_SRC_RX_Pos;
                break;
            }            

            case GPIO_MODE_I2C_SCL:
            {
                /* Set i2c scl mode */
                DIO->CFG[sel] = padCfg | DIO_MODE_SCL;

                /* Set i2c scl src */
                DIO_I2C_SRC->SCL_BYTE = sel;
                break;
            }           

            case GPIO_MODE_I2C_SDA:
            {
                /* Set i2c sda mode */
                DIO->CFG[sel] = padCfg | DIO_MODE_SDA;

                /* Set i2c sda src */
                DIO_I2C_SRC->SDA_BYTE = sel;
                break;
            }

            case GPIO_MODE_LPDSP32_TMS_IN:
            {
                /* Set ldpsp32 tdi src */
                DIO_LPDSP32_JTAG_SRC->TMS_BYTE = sel;
                break;
            }            

            case GPIO_MODE_LPDSP32_TDI_IN:
            {
                /* Set ldpsp32 tms src */
                DIO_LPDSP32_JTAG_SRC->TDI_BYTE = sel;
                break;
            }          

            case GPIO_MODE_LPDSP32_TCK_IN:
            {
                /* Set ldpsp32 tck src */
                DIO_LPDSP32_JTAG_SRC->TCK_BYTE = sel;
                break;
            }

            case GPIO_MODE_AUDIOSINK_IN:
            {
                /* Set audiosink src */
                DIO->AUDIOSINK_SRC &= ~(DIO_AUDIOSINK_SRC_CLK_Mask << DIO_AUDIOSINK_SRC_CLK_Pos);
                DIO->AUDIOSINK_SRC |= sel << DIO_AUDIOSINK_SRC_CLK_Pos;
                break;
            }

            case GPIO_MODE_NMI_IN:
            {
                /* Set nmi src */
                DIO->NMI_SRC &= ~(DIO_NMI_SRC_NMI_Mask << DIO_NMI_SRC_NMI_Pos);
                DIO->NMI_SRC |= sel << DIO_NMI_SRC_NMI_Pos;
                break;
            }            

            case GPIO_MODE_BB_RX_CLK_IN:
            {
                /* Set bb rx clk src */
                DIO_BB_RX_SRC->CLK_BYTE = sel;
                break;
            }            

            case GPIO_MODE_BB_RX_DATA_IN:
            {
                /* Set bb rx data src */
                DIO_BB_RX_SRC->DATA_BYTE = sel;
                break;
            }            

            case GPIO_MODE_BB_SYNC_P_IN:
            {
                /* Set bb rx data src */
                DIO_BB_RX_SRC->RF_SYNC_P_BYTE = sel;
                break;
            }            

            case GPIO_MODE_BB_SPI_MISO_IN:
            {
                /* Set bb spi miso src */
                DIO->BB_SPI_SRC &= ~(DIO_BB_SPI_SRC_MISO_Mask << DIO_BB_SPI_SRC_MISO_Pos);
                DIO->BB_SPI_SRC |= sel << DIO_BB_SPI_SRC_MISO_Pos;
                break;
            }            

            case GPIO_MODE_RF_SPI_MOSI_IN:
            {
                /* Set rf spi mosi src */
                DIO_RF_SPI_SRC->MOSI_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_SPI_CSN_IN:
            {
                /* Set rf spi csn src */
                DIO_RF_SPI_SRC->CSN_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_SPI_CLK_IN:
            {
                /* Set rf spi clk src */
                DIO_RF_SPI_SRC->CLK_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_GPIO0_IN:
            {
                /* Set rf gpio 0 src */
                DIO_RF_GPIO03_SRC->GPIO0_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_GPIO1_IN:
            {
                /* Set rf gpio 1 src */
                DIO_RF_GPIO03_SRC->GPIO1_BYTE = sel;
                break;
            }          

            case GPIO_MODE_RF_GPIO2_IN:
            {
                /* Set rf gpio 2 src */
                DIO_RF_GPIO03_SRC->GPIO2_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_GPIO3_IN:
            {
                /* Set rf gpio 3 src */
                DIO_RF_GPIO03_SRC->GPIO3_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_GPIO4_IN:
            {
                /* Set rf gpio 4 src */
                DIO_RF_GPIO47_SRC->GPIO4_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_GPIO5_IN:
            {
                /* Set rf gpio 5 src */
                DIO_RF_GPIO47_SRC->GPIO5_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_GPIO6_IN:
            {
                /* Set rf gpio 6 src */
                DIO_RF_GPIO47_SRC->GPIO6_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_GPIO7_IN:
            {
                /* Set rf gpio 7 src */
                DIO_RF_GPIO47_SRC->GPIO7_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_GPIO8_IN:
            {
                /* Set rf gpio 8 src */
                DIO_RF_GPIO89_SRC->GPIO8_BYTE = sel;
                break;
            }            

            case GPIO_MODE_RF_GPIO9_IN:
            {
                /* Set rf gpio 9 src */
                DIO_RF_GPIO89_SRC->GPIO9_BYTE = sel;
                break;
            }            

            case GPIO_MODE_DMIC_CLK_AUDIOCLK:
            {
                /* Set the io mode to audioclk */
                DIO->CFG[sel] = padCfg | DIO_MODE_AUDIOCLK;

                /* Set dmic clk src */
                DIO_DMIC_SRC->CLK_BYTE = sel;
                break;
            }            

            case GPIO_MODE_DMIC_CLK_AUDIOSLOWCLK:
            {
                /* Set the io mode to audioslowclk */
                DIO->CFG[sel] = padCfg | DIO_MODE_AUDIOSLOWCLK;

                /* Set dmic clk src */
                DIO_DMIC_SRC->CLK_BYTE = sel;
                break;
            }            

            case GPIO_MODE_DMIC_DATA_IN:
            {
                /* Set dmic data src */
                DIO_DMIC_SRC->DATA_BYTE = sel;
                break;
            }            

            case GPIO_MODE_ADC_IN:
            {
                /* Set adc in src */
                break;
            }            

            default:
            {
                break;
            }
        }
    }
    else
    {
        /* Set the io mode */
        DIO->CFG[sel] = padCfg | (cfg->io_mode << DIO_CFG_IO_MODE_Pos);
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_ConfigureInterrupt (GPIO_INT_SEL_t sel, const GPIO_INT_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the gpio interrupt
 * Inputs        : sel - Interrupt selection
 *               : cfg - Pointer to interrupt configuration structure
 * Outputs       : ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_ConfigureInterrupt (GPIO_INT_SEL_t sel, const GPIO_INT_CFG_t *cfg)
{
    /* Check if selected interrupt was enabled */
    if (!((GPIO_FLAG_BIT_SET << sel) & GPIO_INT_EN_MSK))
    {
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Configure interrupt settings */
    DIO->INT_CFG[sel] = (cfg->debounce_en << DIO_INT_CFG_DEBOUNCE_ENABLE_Pos) |
                        (cfg->event << DIO_INT_CFG_EVENT_Pos)                 |
                        (cfg->src_sel << DIO_INT_CFG_SRC_Pos);

    /* Check if interrupt should be enabled */
    if (cfg->interrup_en)
    {
        /* Clear pending flag */
        NVIC_ClearPendingIRQ(GPIO_Resources.intInfo.irqn[sel]);

        /* Enable the interrupt */
        NVIC_EnableIRQ(GPIO_Resources.intInfo.irqn[sel]);
    }
    else
    {
        /* Disable the interrupt */
        NVIC_DisableIRQ(GPIO_Resources.intInfo.irqn[sel]);

        /* Clear pending flag */
        NVIC_ClearPendingIRQ(GPIO_Resources.intInfo.irqn[sel]);
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_SetInterruptPriority(GPIO_INT_SEL_t sel, const GPIO_PRI_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure gpio interrupt priority.
 *               : sel  - interrupt seelction
 * Inputs        : cfg - interrupt priority configuration
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_SetInterruptPriority(GPIO_INT_SEL_t sel, const GPIO_PRI_CFG_t *cfg)
{
    /* Encoded priority value */
    uint32_t encodedPri = 0;

    /* Encode priority configuration word */
    encodedPri = NVIC_EncodePriority(NVIC_GetPriorityGrouping(), cfg->preempt_pri, cfg->subgrp_pri);

    /* Set the gpio priority */
    NVIC_SetPriority(GPIO_Resources.intInfo.irqn[sel], encodedPri);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_SetAltFuncRegister (GPIO_FUNC_REGISTERS_t reg, uint32_t val)
 * ----------------------------------------------------------------------------
 * Description   : Sets the alternative function register.
 * Inputs        : reg - Register selection
 *               : val - Register value
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_SetAltFuncRegister (GPIO_FUNC_REGISTERS_t reg, uint32_t val)
{
    /* Check which register was selected */
    switch (reg)
    {
        case GPIO_FUNC_REG_PCM:
        {
            /* Set pcm register */
            DIO->PCM_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_SPI0:
        {
            /* Set spi0 register */
            DIO->SPI_SRC[0] = val;
            break;
        }

        case GPIO_FUNC_REG_SPI1:
        {
            /* Set spi1 register */
            DIO->SPI_SRC[1] = val;
            break;
        }

        case GPIO_FUNC_REG_UART:
        {
            /* Set uart register */
            DIO->UART_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_I2C:
        {
            /* Set i2c register */
            DIO->I2C_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_AUDIOSINK:
        {
            /* Set audiosink register */
            DIO->AUDIOSINK_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_NMI:
        {
            /* Set nmi register */
            DIO->NMI_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_BB_RX:
        {
            /* Set bb rx register */
            DIO->BB_RX_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_BB_SPI:
        {
            /* Set bb spi register */
            DIO->BB_SPI_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_RF_SPI:
        {
            /* Set rf spi register */
            DIO->RF_SPI_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_RF_GPIO03:
        {
            /* Set rf gpio03 register */
            DIO->RF_GPIO03_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_RF_GPIO47:
        {
            /* Set rf gpio47 register */
            DIO->RF_GPIO47_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_RF_GPIO89:
        {
            /* Set rf gpio89 register */
            DIO->RF_GPIO89_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_DMIC:
        {
            /* Set dmic register */
            DIO->DMIC_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_LPDSP32_JTAG:
        {
            /* Set lpdsp32 jtag register */
            DIO->LPDSP32_JTAG_SRC = val;
            break;
        }

        case GPIO_FUNC_REG_JTAG_SW_PAD:
        {
            /* Set lpdsp32 jtag register */
            DIO->LPDSP32_JTAG_SRC = val;
            break;
        }

        default:
        {
            break;
        }
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_ResetAltFuncRegister (GPIO_FUNC_REGISTERS_t reg)
 * ----------------------------------------------------------------------------
 * Description   : Reset the selected alternative function register.
 * Inputs        : reg - Register selection
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_ResetAltFuncRegister (GPIO_FUNC_REGISTERS_t reg)
{
    /* Check which register was selected */
    switch (reg)
    {
        case GPIO_FUNC_REG_PCM:
        {
            /* Reset pcm register */
            DIO->PCM_SRC = PCM_CLK_SRC_CONST_HIGH | PCM_FRAME_SRC_CONST_HIGH | PCM_SERI_SRC_CONST_HIGH;
            break;
        }

        case GPIO_FUNC_REG_SPI0:
        {
            /* Reset spi0 register */
            DIO->SPI_SRC[0] = SPI_CLK_SRC_CONST_HIGH | SPI_CS_SRC_CONST_HIGH | SPI_SERI_SRC_CONST_HIGH;
            break;
        }

        case GPIO_FUNC_REG_SPI1:
        {
            /* Reset spi1 register */
            DIO->SPI_SRC[1] = SPI_CLK_SRC_CONST_HIGH | SPI_CS_SRC_CONST_HIGH | SPI_SERI_SRC_CONST_HIGH;
            break;
        }

        case GPIO_FUNC_REG_UART:
        {
            /* Reset uart register */
            DIO->UART_SRC = UART_RX_SRC_CONST_HIGH;
            break;
        }

        case GPIO_FUNC_REG_I2C:
        {
            /* Reset i2c register */
            DIO->I2C_SRC = SCL_SRC_CONST_HIGH | SDA_SRC_CONST_HIGH;
            break;
        }

        case GPIO_FUNC_REG_AUDIOSINK:
        {
            /* Reset audiosink register */
            DIO->AUDIOSINK_SRC = AUDIOSINK_CLK_SRC_CONST_HIGH;
            break;
        }

        case GPIO_FUNC_REG_NMI:
        {
            /* Reset nmi register */
            DIO->NMI_SRC = NMI_SRC_CONST_LOW | NMI_ACTIVE_HIGH;
            break;
        }

        case GPIO_FUNC_REG_BB_RX:
        {
            /* Reset bb rx register */
            DIO->BB_RX_SRC = BB_RX_DATA_SRC_RF_GPIO0 | BB_RX_CLK_SRC_RF_GPIO1 | BB_RF_SYNC_P_SRC_RF;
            break;
        }

        case GPIO_FUNC_REG_BB_SPI:
        {
            /* Reset bb spi register */
            DIO->BB_SPI_SRC = BB_SPI_MISO_SRC_RF_SPI_MISO;
            break;
        }

        case GPIO_FUNC_REG_RF_SPI:
        {
            /* Reset rf spi register */
            DIO->RF_SPI_SRC = RF_SPI_CLK_SRC_BB_SPI_CLK | RF_SPI_CSN_SRC_BB_SPI_CSN | RF_SPI_MOSI_SRC_BB_SPI_MOSI;
            break;
        }

        case GPIO_FUNC_REG_RF_GPIO03:
        {
            /* Reset rf gpio03 register */
            DIO->RF_GPIO03_SRC = RF_GPIO0_SRC_CONST_LOW | RF_GPIO1_SRC_CONST_LOW |
                                 RF_GPIO2_SRC_BB_SYNC_P | RF_GPIO3_SRC_BB_TX_DATA;
            break;
        }

        case GPIO_FUNC_REG_RF_GPIO47:
        {
            /* Reset rf gpio47 register */
            DIO->RF_GPIO47_SRC = RF_GPIO4_SRC_BB_TX_DATA_VALID | RF_GPIO5_SRC_CONST_LOW |
                                 RF_GPIO6_SRC_CONST_LOW | RF_GPIO7_SRC_CONST_LOW;
            break;
        }

        case GPIO_FUNC_REG_RF_GPIO89:
        {
            /* Reset rf gpio89 register */
            DIO->RF_GPIO89_SRC = RF_GPIO8_SRC_CONST_LOW | RF_GPIO9_SRC_CONST_LOW;
            break;
        }

        case GPIO_FUNC_REG_DMIC:
        {
            /* Reset dmic register */
            DIO->DMIC_SRC = DMIC_DATA_SRC_CONST_HIGH | DMIC_CLK_SRC_CONST_HIGH;
            break;
        }

        case GPIO_FUNC_REG_LPDSP32_JTAG:
        {
            /* Reset lpdsp32 jtag register */
            DIO->LPDSP32_JTAG_SRC = LPDSP32_TCK_SRC_CONST_HIGH | LPDSP32_TMS_SRC_CONST_HIGH |
                                    LPDSP32_TDI_SRC_CONST_HIGH;
            break;
        }

        case GPIO_FUNC_REG_JTAG_SW_PAD:
        {
            /* Reset lpdsp32 jtag register */
            DIO->LPDSP32_JTAG_SRC = CM3_JTAG_TRST_DISABLED | CM3_JTAG_DATA_DISABLED;
            break;
        }

        default:
        {
            break;
        }
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t GPIO_Initialize (GPIO_SignalEvent_t cb)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the gpio driver
 * Inputs        : cb  - callback function
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t GPIO_Initialize (GPIO_SignalEvent_t cb)
{
    /* Reset all alternative function registers */
    for (int i = 0; i <= GPIO_ALT_FUNC_REG_NUMBER; i++)
    {
        GPIO_ResetAltFuncRegister(i);
    }

    /* Configure common settings */
    GPIO_Configure(GPIO_Resources.info->default_cfg);

#if (RTE_GPIO_EXTCLK_EN)

    /* Configure gpio extclk settings */
    GPIO_ConfigureExtClk(GPIO_Resources.info->default_extclk_cfg);
#endif /* if (RTE_GPIO_EXTCLK_EN) */
#if (RTE_GPIO_NMI_EN)

    /* Configure gpio nmi settings */
    GPIO_ConfigureNmi(GPIO_Resources.info->default_nmi_cfg->nmi_pol);
#endif /* if (RTE_GPIO_NMI_EN) */
#if (RTE_GPIO_JTAG_EN)

    /* Configure gpio jtag settings */
    GPIO_ConfigureJtag(GPIO_Resources.info->default_jtag_cfg);
#endif /* if (RTE_GPIO_JTAG_EN) */

    /* Configure each gpio pad which was set to be initialized */
    for (int i = 0; i < GPIO_PADS_NUMBER; ++i)
    {
        /* Check if particular gpio pad should be initialized */
        if ((GPIO_FLAG_BIT_SET << i) & GPIO_EN_MSK)
        {
            /* Configure particular gpio pad */
            GPIO_ConfigurePad(i, GPIO_Resources.info->default_pad_cfg[i]);
        }
    }

#if (GPIO_INT_EN_MSK)

    /* Configure each gpio interrupt which was set to be initialized */
    for (int i = 0; i < GPIO_INT_NUMBER; ++i)
    {
        /* Check if particular gpio interrupt should be initialized */
        if ((GPIO_FLAG_BIT_SET << i) & GPIO_INT_EN_MSK)
        {
            /* Configure particular gpio */
            GPIO_ConfigureInterrupt(i, GPIO_Resources.info->default_int_cfg[i]);

            /* Set the particuar gpio interrupts priority */
            GPIO_SetInterruptPriority(i, GPIO_Resources.info->default_pri_cfg[i]);
        }
    }

    /* Set the callback function */
    GPIO_Resources.intInfo.cb = cb;
#endif /* if (GPIO_INT_EN_MSK) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void GPIO_SetDir (GPIO_SEL sel, GPIO_DIR dir)
 * ----------------------------------------------------------------------------
 * Description   : Set particular gpio pad direction
 * Inputs        : sel - gpio pad selection
 *               : dir - gpio dir selection
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void GPIO_SetDir(GPIO_SEL_t sel, GPIO_DIR_t dir)
{
    /* Set selected pad direction */
    DIO->DIR &= ~(GPIO_FLAG_BIT_SET << sel);
    DIO->DIR |= (dir & GPIO_FLAG_BIT_SET) << sel;
}

/* ----------------------------------------------------------------------------
 * Function      : void GPIO_SetHigh (GPIO_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Set particular gpio pad
 * Inputs        : sel - gpio pad selection
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void GPIO_SetHigh(GPIO_SEL_t sel)
{
    /* Set selected pad */
    Sys_GPIO_Set_High(sel);
}

/* ----------------------------------------------------------------------------
 * Function      : void GPIO_ToggleValue (GPIO_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Toggle particular gpio pad
 * Inputs        : sel - gpio pad selection
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void GPIO_ToggleValue(GPIO_SEL_t sel)
{
    /* Toggle selected pad */
    Sys_GPIO_Toggle(sel);
}

/* ----------------------------------------------------------------------------
 * Function      : void GPIO_SetLow (GPIO_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Reset particular gpio pad
 * Inputs        : sel - gpio pad selection
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void GPIO_SetLow(GPIO_SEL_t sel)
{
    /* Reset selected pad */
    Sys_GPIO_Set_Low(sel);
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t GPIO_GetValue (GPIO_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Return the selected gpio value
 * Inputs        : sel - gpio pad selection
 * Outputs       : Gpio pad value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t GPIO_GetValue(GPIO_SEL_t sel)
{
    /* Reset selected pad */
    return DIO_DATA->ALIAS[sel];
}

#if (RTE_GPIO_DIO0_INT_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DIO0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a gpio event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO0_IRQHandler(void)
{
    /* Check if callback was set */
    if (GPIO_Resources.intInfo.cb)
    {
        /* Execute application callback */
        GPIO_Resources.intInfo.cb(GPIO_EVENT_0_IRQ);
    }
}

#endif /* if (RTE_GPIO_DIO0_INT_EN) */

#if (RTE_GPIO_DIO1_INT_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DIO1_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a gpio event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO1_IRQHandler(void)
{
    /* Check if callback was set */
    if (GPIO_Resources.intInfo.cb)
    {
        /* Execute application callback */
        GPIO_Resources.intInfo.cb(GPIO_EVENT_1_IRQ);
    }
}

#endif /* if (RTE_GPIO_DIO1_INT_EN) */

#if (RTE_GPIO_DIO2_INT_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DIO2_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a gpio event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO2_IRQHandler(void)
{
    /* Check if callback was set */
    if (GPIO_Resources.intInfo.cb)
    {
        /* Execute application callback */
        GPIO_Resources.intInfo.cb(GPIO_EVENT_2_IRQ);
    }
}

#endif /* if (RTE_GPIO_DIO2_INT_EN) */

#if (RTE_GPIO_DIO3_INT_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DIO3_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a gpio event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO3_IRQHandler(void)
{
    /* Check if callback was set */
    if (GPIO_Resources.intInfo.cb)
    {
        /* Execute application callback */
        GPIO_Resources.intInfo.cb(GPIO_EVENT_3_IRQ);
    }
}

#endif /* if (RTE_GPIO_DIO3_INT_EN) */

/* GPIO Driver Control Block */
DRIVER_GPIO_t Driver_GPIO =
{
    GPIO_GetVersion,
    GPIO_Initialize,
    GPIO_Configure,
    GPIO_ConfigurePad,
    GPIO_ConfigureInterrupt,
    GPIO_SetInterruptPriority,
    GPIO_ConfigureExtClk,
    GPIO_ConfigureNmi,
    GPIO_ConfigureJtag,
    GPIO_SetDir,
    GPIO_SetHigh,
    GPIO_ToggleValue,
    GPIO_SetLow,
    GPIO_GetValue,
    GPIO_SetAltFuncRegister,
    GPIO_ResetAltFuncRegister
};

#endif    /* RTE_GPIO */
