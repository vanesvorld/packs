/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * Driver_GPIO.h
 * - GPIO driver header file for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#ifndef DRIVER_GPIO_H_
#define DRIVER_GPIO_H_

#ifdef  __cplusplus
extern "C"
{
#endif

#include <Driver_Common.h>

#define ARM_GPIO_API_VERSION ARM_DRIVER_VERSION_MAJOR_MINOR(1,0)  /* API version */

/****** GPIO Control Codes *****/

/*----- GPIO Control Codes: GPIO Selection -----*/
typedef enum _GPIO_SEL_t {
    GPIO_0                          = 0x0,            ///< gpio pad 0
    GPIO_1                          = 0x1,            ///< gpio pad 1
    GPIO_2                          = 0x2,            ///< gpio pad 2
    GPIO_3                          = 0x3,            ///< gpio pad 3
    GPIO_4                          = 0x4,            ///< gpio pad 4
    GPIO_5                          = 0x5,            ///< gpio pad 5
    GPIO_6                          = 0x6,            ///< gpio pad 6
    GPIO_7                          = 0x7,            ///< gpio pad 7
    GPIO_8                          = 0x8,            ///< gpio pad 8
    GPIO_9                          = 0x9,            ///< gpio pad 9
    GPIO_10                         = 0xA,            ///< gpio pad 10
    GPIO_11                         = 0xB,            ///< gpio pad 11
    GPIO_12                         = 0xC,            ///< gpio pad 12
    GPIO_13                         = 0xD,            ///< gpio pad 13
    GPIO_14                         = 0xE,            ///< gpio pad 14
    GPIO_15                         = 0xF             ///< gpio pad 15
} GPIO_SEL_t;

/*----- GPIO Control Codes: GPIO INT Selection -----*/
typedef enum _GPIO_INT_SEL_t {
    GPIO_INT_0                      = 0x0,            ///< gpio interrupt 0
    GPIO_INT_1                      = 0x1,            ///< gpio interrupt 1
    GPIO_INT_2                      = 0x2,            ///< gpio interrupt 2
    GPIO_INT_3                      = 0x3,            ///< gpio interrupt 3
} GPIO_INT_SEL_t;

/*----- GPIO Control Codes: Drive strength -----*/
typedef enum _GPIO_DRIVE_t {
    GPIO_2X                         = 0x0,           ///< 2x drive strength
    GPIO_3X                         = 0x1,           ///< 3x drive strength
    GPIO_5X                         = 0x2,           ///< 5x drive strength
    GPIO_6X                         = 0x3            ///< 6x drive strength
} GPIO_DRIVE_t;

/*----- GPIO Control Codes: Low pass filter -----*/
typedef enum _GPIO_LPF_t {
    GPIO_LPF_DISABLE                = 0x0,           ///< low pass filter disabled
    GPIO_LPF_ENABLE                 = 0x1            ///< low pass filter enabled
} GPIO_LPF_t;

/*----- GPIO Control Codes: Pull control -----*/
typedef enum _GPIO_PULL_t {
    GPIO_NO_PULL                    = 0x0,           ///< no pull selected
    GPIO_WEAK_PULL_UP               = 0x1,           ///< weak pull-up selected
    GPIO_WEAK_PULL_DOWN             = 0x2,           ///< weak pull-down selected
    GPIO_STRONG_PULL_UP             = 0x3            ///< strong pull-up selected
} GPIO_PULL_t;

/*----- GPIO Control Codes: IO Mode -----*/
typedef enum _GPIO_MODE_t {
    GPIO_MODE_GPIO_IN_0             = 0x000,         ///< gpio input mode 0
    GPIO_MODE_GPIO_IN_1             = 0x001,         ///< gpio input mode 1
    GPIO_MODE_GPIO_OUT_0            = 0x002,         ///< gpio output low mode
    GPIO_MODE_GPIO_OUT_1            = 0x003,         ///< gpio output high mode
    GPIO_MODE_USRCLK_OUT            = 0x004,         ///< user clock signal
    GPIO_MODE_SLOWCLK_OUT           = 0x005,         ///< slow clock signal
    GPIO_MODE_SYSCLK_OUT            = 0x006,         ///< system clock signal
    GPIO_MODE_PCM_SERO_OUT          = 0x007,         ///< pcm sero interface signal
    GPIO_MODE_PCM_SERI_IN           = 0x100,         ///< pcm seri interface signal
    GPIO_MODE_PCM_FRAME_IN          = 0x101,         ///< pcm frame input interface signal
    GPIO_MODE_PCM_CLK_IN            = 0x102,         ///< pcm clock input interface signal
    GPIO_MODE_PCM_FRAME_OUT         = 0x103,         ///< pcm frame output interface signal
    GPIO_MODE_SPI0_SERO_OUT         = 0x009,         ///< spi0 sero interface signal
    GPIO_MODE_SPI0_SERI_IN          = 0x200,         ///< spi0 seri interface signal
    GPIO_MODE_SPI0_CS_OUT           = 0x00A,         ///< spi0 cs output interface signal
    GPIO_MODE_SPI0_CLK_OUT          = 0x00B,         ///< spi0 clock output interface signal
    GPIO_MODE_SPI0_CS_IN            = 0x201,         ///< spi0 cs input interface signal
    GPIO_MODE_SPI0_CLK_IN           = 0x202,         ///< spi0 clock input interface signal
    GPIO_MODE_SPI1_SERO_OUT         = 0x00C,         ///< spi1 sero interface signal
    GPIO_MODE_SPI1_SERI_IN          = 0x203,         ///< spi1 seri interface signal
    GPIO_MODE_SPI1_CS_OUT           = 0x00D,         ///< spi1 cs output interface signal
    GPIO_MODE_SPI1_CLK_OUT          = 0x00E,         ///< spi1 clock output interface signal
    GPIO_MODE_SPI1_CS_IN            = 0x204,         ///< spi1 cs input interface signal
    GPIO_MODE_SPI1_CLK_IN           = 0x205,         ///< spi1 clock input interface signal
    GPIO_MODE_UART_RX_IN            = 0x300,         ///< uart rx interface signal
    GPIO_MODE_UART_TX_OUT           = 0x00F,         ///< uart tx interface signal
    GPIO_MODE_I2C_SCL               = 0x400,         ///< i2c scl signal
    GPIO_MODE_I2C_SDA               = 0x401,         ///< i2c sda signal
    GPIO_MODE_PWM0_OUT              = 0x012,         ///< pwm0 interface signal
    GPIO_MODE_PWM0_INV_OUT          = 0x013,         ///< pwm0 inverted interface signal
    GPIO_MODE_PWM1_OUT              = 0x014,         ///< pwm1 interface signal
    GPIO_MODE_PWM1_INV_OUT          = 0x015,         ///< pwm1 inverted interface signal
    GPIO_MODE_LPDSP32_TDO_OUT       = 0x016,         ///< LPDSP32 TDO interface signal
    GPIO_MODE_LPDSP32_TDI_IN        = 0x500,         ///< LPDSP32 TDI interface signal
    GPIO_MODE_LPDSP32_TMS_IN        = 0x501,         ///< LPDSP32 TMS interface signal
    GPIO_MODE_LPDSP32_TCK_IN        = 0x502,         ///< LPDSP32 TCK interface signal
    GPIO_MODE_RFCLK_OUT             = 0x017,         ///< rfclk clock signal
    GPIO_MODE_RCCLK_OUT             = 0x018,         ///< rcclk clock signal
    GPIO_MODE_JTCK_DIV_OUT          = 0x019,         ///< jtck divider signal
    GPIO_MODE_EXTCLK_DIV_OUT        = 0x01A,         ///< extclk divider signal
    GPIO_MODE_STANDBYCLK_OUT        = 0x01B,         ///< standby clock signal
    GPIO_MODE_AUDIOSINK_IN          = 0x600,         ///< audiosink source
    GPIO_MODE_NMI_IN                = 0x700,         ///< nmi input signal
    GPIO_MODE_BB_TX_DATA_OUT        = 0x01C,         ///< output baseband controller tx data signal
    GPIO_MODE_BB_TX_DATA_VALID_OUT  = 0x01D,         ///< output baseband controller tx data valid signal
    GPIO_MODE_BB_RX_CLK_IN          = 0x800,         ///< output baseband controller input clock signal
    GPIO_MODE_BB_RX_DATA_IN         = 0x801,         ///< output baseband controller input data signal
    GPIO_MODE_BB_SYNC_P_IN          = 0x802,         ///< output baseband controller rf_sync_p input signal
    GPIO_MODE_BB_SYNC_P_OUT         = 0x01E,         ///< output baseband controller rf_sync_p output signal
    GPIO_MODE_BB_AUDIO0_SYNC_P_OUT  = 0x01F,         ///< output baseband controller ble audio0 synchronization signal
    GPIO_MODE_BB_AUDIO1_SYNC_P_OUT  = 0x020,         ///< output baseband controller ble audio1 synchronization signal
    GPIO_MODE_BB_AUDIO2_SYNC_P_OUT  = 0x021,         ///< output baseband controller ble audio2 synchronization signal
    GPIO_MODE_BB_SPI_CSN_OUT        = 0x022,         ///< output baseband controller spi csn signal
    GPIO_MODE_BB_SPI_CLK_OUT        = 0x023,         ///< output baseband controller spi clk signal
    GPIO_MODE_BB_SPI_MOSI_OUT       = 0x024,         ///< output baseband controller spi mosi signal
    GPIO_MODE_BB_SPI_MISO_IN        = 0x900,         ///< output baseband controller spi miso signal
    GPIO_MODE_BB_DBG0_0_OUT         = 0x025,         ///< output baseband controller diagnostic port 0
    GPIO_MODE_BB_DBG0_1_OUT         = 0x026,         ///< output baseband controller diagnostic port 1
    GPIO_MODE_BB_DBG0_2_OUT         = 0x027,         ///< output baseband controller diagnostic port 2
    GPIO_MODE_BB_DBG0_3_OUT         = 0x028,         ///< output baseband controller diagnostic port 3
    GPIO_MODE_BB_DBG0_4_OUT         = 0x029,         ///< output baseband controller diagnostic port 4
    GPIO_MODE_BB_DBG0_5_OUT         = 0x02A,         ///< output baseband controller diagnostic port 5
    GPIO_MODE_BB_DBG0_6_OUT         = 0x02B,         ///< output baseband controller diagnostic port 6
    GPIO_MODE_BB_DBG0_7_OUT         = 0x02C,         ///< output baseband controller diagnostic port 7
    GPIO_MODE_RF_SPI_MISO_OUT       = 0x02D,         ///< rf front-end spi miso signal
    GPIO_MODE_RF_SPI_MOSI_IN        = 0xA00,         ///< rf front-end spi mosi signal
    GPIO_MODE_RF_SPI_CSN_IN         = 0xA01,         ///< rf front-end spi csn signal
    GPIO_MODE_RF_SPI_CLK_IN         = 0xA02,         ///< rf front-end spi clk signal
    GPIO_MODE_RF_GPIO0_OUT          = 0x02E,         ///< rf front-end gpio0 output
    GPIO_MODE_RF_GPIO1_OUT          = 0x02F,         ///< rf front-end gpio1 output
    GPIO_MODE_RF_GPIO2_OUT          = 0x030,         ///< rf front-end gpio2 output
    GPIO_MODE_RF_GPIO3_OUT          = 0x031,         ///< rf front-end gpio3 output
    GPIO_MODE_RF_GPIO4_OUT          = 0x032,         ///< rf front-end gpio4 output
    GPIO_MODE_RF_GPIO5_OUT          = 0x033,         ///< rf front-end gpio5 output
    GPIO_MODE_RF_GPIO6_OUT          = 0x034,         ///< rf front-end gpio6 output
    GPIO_MODE_RF_GPIO7_OUT          = 0x035,         ///< rf front-end gpio7 output
    GPIO_MODE_RF_GPIO8_OUT          = 0x036,         ///< rf front-end gpio8 output
    GPIO_MODE_RF_GPIO9_OUT          = 0x037,         ///< rf front-end gpio9 output
    GPIO_MODE_RF_GPIO0_IN           = 0xB00,         ///< rf front-end gpio0 input
    GPIO_MODE_RF_GPIO1_IN           = 0xB01,         ///< rf front-end gpio1 input
    GPIO_MODE_RF_GPIO2_IN           = 0xB02,         ///< rf front-end gpio2 input
    GPIO_MODE_RF_GPIO3_IN           = 0xB03,         ///< rf front-end gpio3 input
    GPIO_MODE_RF_GPIO4_IN           = 0xB04,         ///< rf front-end gpio4 input
    GPIO_MODE_RF_GPIO5_IN           = 0xB05,         ///< rf front-end gpio5 input
    GPIO_MODE_RF_GPIO6_IN           = 0xB06,         ///< rf front-end gpio6 input
    GPIO_MODE_RF_GPIO7_IN           = 0xB07,         ///< rf front-end gpio7 input
    GPIO_MODE_RF_GPIO8_IN           = 0xB08,         ///< rf front-end gpio8 input
    GPIO_MODE_RF_GPIO9_IN           = 0xB09,         ///< rf front-end gpio9 input
    GPIO_MODE_AUDIOCLK_OUT          = 0x038,         ///< audio clock signal
    GPIO_MODE_AUDIOSLOWCLK_OUT      = 0x039,         ///< audio slow clock signal
    GPIO_MODE_OD_P_OUT              = 0x03A,         ///< output od_p signal
    GPIO_MODE_OD_N_OUT              = 0x03B,         ///< output od_n signal
    GPIO_MODE_DMIC_CLK_AUDIOCLK     = 0xC00,         ///< dmic audio clk signal
    GPIO_MODE_DMIC_CLK_AUDIOSLOWCLK = 0xC01,         ///< dmic audioslow clk signal
    GPIO_MODE_DMIC_DATA_IN          = 0xC02,         ///< dmic data signal
    GPIO_MODE_AUDIO_SYNC_PULSE_OUT  = 0x03C,         ///< output audio synchronization pulse
    GPIO_MODE_AUDIO_SYNC_MISSED_OUT = 0x03D,         ///< output audio synchronization missed pulse
    GPIO_MODE_ADC_IN                = 0xD00,         ///< adc input mode
    GPIO_MODE_INPUT                 = 0x03E,         ///< input mode
    GPIO_MODE_DISABLED              = 0x03F          ///< disabled
} GPIO_MODE_t;

/*----- GPIO Control Codes: GPIO alternative function registers -----*/
typedef enum _GPIO_FUNC_REGISTERS_t {
    GPIO_FUNC_REG_PCM               = 0x00,          ///< pcm register
    GPIO_FUNC_REG_SPI0              = 0x01,          ///< spi register
    GPIO_FUNC_REG_SPI1              = 0x02,          ///< spi register
    GPIO_FUNC_REG_UART              = 0x03,          ///< uart register
    GPIO_FUNC_REG_I2C               = 0x04,          ///< i2c register
    GPIO_FUNC_REG_AUDIOSINK         = 0x05,          ///< audiosink register
    GPIO_FUNC_REG_NMI               = 0x06,          ///< nmi register
    GPIO_FUNC_REG_BB_RX             = 0x07,          ///< bb rx register
    GPIO_FUNC_REG_BB_SPI            = 0x08,          ///< bb spi register
    GPIO_FUNC_REG_RF_SPI            = 0x09,          ///< rf spi register
    GPIO_FUNC_REG_RF_GPIO03         = 0x0A,          ///< rf gpio03 register
    GPIO_FUNC_REG_RF_GPIO47         = 0x0B,          ///< rf gpio47 register
    GPIO_FUNC_REG_RF_GPIO89         = 0x0C,          ///< rf gpio89 register
    GPIO_FUNC_REG_DMIC              = 0x0D,          ///< dmic register
    GPIO_FUNC_REG_LPDSP32_JTAG      = 0x0E,          ///< lpdsp32 jtag register
    GPIO_FUNC_REG_JTAG_SW_PAD       = 0x0F           ///< jtag sw pad register
} GPIO_FUNC_REGISTERS_t;

/*----- GPIO Control Codes: Enable / disable values -----*/
typedef enum _GPIO_EN_DIS_t {
    GPIO_DISABLE                    = 0,             ///< gpio disable value
    GPIO_ENABLE                     = 1              ///< gpio enable value
} GPIO_EN_DIS_t;

/*----- GPIO Control Codes: GPIO direction -----*/
typedef enum _GPIO_DIR_t {
    GPIO_IN                         = 0,             ///< gpio direction input
    GPIO_OUT                        = 1              ///< gpio direction output
} GPIO_DIR_t;

/*----- GPIO Control Codes: Interrupts events -----*/
typedef enum _GPIO_EVENT_t {
    GPIO_EVENT_NONE                 = 0,             ///< interrupt event none
    GPIO_EVENT_HIGH_LEVEL           = 1,             ///< interrupt event high level
    GPIO_EVENT_LOW_LEVEL            = 2,             ///< interrupt event low level
    GPIO_EVENT_RISING_EDGE          = 3,             ///< interrupt event rising edge
    GPIO_EVENT_FALLING_EDGE         = 4,             ///< interrupt event falling edge
    GPIO_EVENT_TRANSITION           = 5              ///< interrupt event transition
} GPIO_EVENT_t;

/*----- GPIO Control Codes: Debounce clock source -----*/
typedef enum _GPIO_DBC_CLK_t {
    GPIO_DBC_CLK_SLOWCLK_DIV32      = 0,             ///< debounce clock source = slow clock / 32
    GPIO_DBC_CLK_SLOWCLK_DIV1024    = 1              ///< debounce clock source = slow clock / 1024
} GPIO_DBC_CLK_t;

/*----- GPIO Control Codes: Pads strength -----*/
typedef enum _GPIO_DRIVE_STRENGTHS_t {
    GPIO_LOW_DRIVE                  = 0,             ///< regular drive strengths
    GPIO_HIGH_DRIVE                 = 1              ///< drive strengths increased by ~50%
} GPIO_DRIVE_STRENGTHS_t;

/*----- GPIO Control Codes: NMI polarity strength -----*/
typedef enum _GPIO_NMI_POL_t {
    GPIO_NMI_ACTIVE_LOW             = 0,             ///< nmi active low
    GPIO_NMI_ACTIVE_HIGH            = 1              ///< nmi active high
} GPIO_NMI_POL_t;

/*----- GPIO Event -----*/
#define GPIO_EVENT_0_IRQ   (1UL << 0)                ///< DIO0 interrupt event value
#define GPIO_EVENT_1_IRQ   (1UL << 1)                ///< DIO1 interrupt event value
#define GPIO_EVENT_2_IRQ   (1UL << 2)                ///< DIO2 interrupt event value
#define GPIO_EVENT_3_IRQ   (1UL << 3)                ///< DIO3 interrupt event value

// Function documentation
/**
  \fn          ARM_DRIVER_VERSION ARM_GPIO_GetVersion (void)
  \brief       Get driver version.
  \return      \ref ARM_DRIVER_VERSION

  \fn          int32_t GPIO_Initialize (GPIO_SignalEvent_t cb)
  \brief       Initialize the gpio driver.
  \param[in]   cb  Pointer to \ref GPIO_SignalEvent
  \return      \ref execution_status

  \fn          int32_t GPIO_Configure (const GPIO_CFG_t * cfg)
  \brief       Configure common gpio settings.
  \param[in]   cfg  Pointer to \ref GPIO_CFG_t
  \return      \ref execution_status

  \fn          int32_t GPIO_ConfigurePad (GPIO_SEL_t sel, const GPIO_PAD_CFG_t *cfg)
  \brief       Configure the gpio pad.
  \param[in]   sel  Pad selection \ref GPIO_SEL_t
  \param[in]   cfg  Pointer to \ref GPIO_PAD_CFG_t
  \return      \ref execution_status

  \fn          int32_t GPIO_ConfigureInterrupt (GPIO_INT_SEL_t sel, const GPIO_INT_CFG_t *cfg)
  \brief       Configure the gpio interrupt.
  \param[in]   sel  Interrupt selection \ref GPIO_INT_SEL_t
  \param[in]   cfg  Pointer to \ref GPIO_INT_CFG_t
  \return      \ref execution_status

  \fn          int32_t GPIO_SetInterruptPriority(GPIO_INT_SEL_t sel, const GPIO_PRI_CFG_t *cfg)
  \brief       Configure gpio interrupt priority.
  \param[in]   sel  Interrupt selection \ref GPIO_INT_SEL_t
  \param[in]   cfg  Pointer to \ref GPIO_PRI_CFG_t
  \return      \ref execution_status

  \fn          int32_t GPIO_ConfigureExtClk (const GPIO_EXTCLK_CFG_t * cfg)
  \brief       Configure the gpio external clock mode.
  \param[in]   cfg  Pointer to \ref GPIO_EXTCLK_CFG_t
  \return      \ref execution_status

  \fn          int32_t GPIO_ConfigureNmi (GPIO_NMI_POL_t cfg)
  \brief       Configure the nmi polarity.
  \param[in]   cfg  \ref GPIO_NMI_POL_t value
  \return      \ref execution_status

  \fn          int32_t GPIO_ConfigureJtag (const GPIO_JTAG_CFG_t *cfg)
  \brief       Configure the gpio jtag mode.
  \param[in]   cfg  Pointer to \ref GPIO_JTAG_CFG_t
  \return      \ref execution_status

  \fn          void GPIO_SetDir (GPIO_SEL_t sel, GPIO_DIR_t dir)
  \brief       Set particular gpio pad direction.
  \param[in]   sel  Pad selection \ref GPIO_SEL_t
  \param[in]   dir  Pad direction \ref GPIO_DIR_t
  \return      None

  \fn          void GPIO_SetHigh (GPIO_SEL_t sel)
  \brief       Set particular gpio pad.
  \param[in]   sel  Pad selection \ref GPIO_SET_t
  \return      \ref None

  \fn          void GPIO_ToggleValue (GPIO_SEL_t sel)
  \brief       Toggle particular gpio pad.
  \param[in]   sel  Pad selection \ref GPIO_SET_t
  \return      \ref None

  \fn          void GPIO_SetLow (GPIO_SEL_t sel)
  \brief       Reset particular gpio pad.
  \param[in]   sel  Pad selection \ref GPIO_SET_t
  \return      \ref None

  \fn          uint32_t GPIO_GetValue (GPIO_SEL_t sel)
  \brief       Returns the selected gpio pad value.
  \param[in]   sel  Pad selection \ref GPIO_SET_t
  \return      \ref Gpio pad value

  \fn          int32_t GPIO_SetAltFuncRegister (GPIO_FUNC_REGISTERS_t reg, uint32_t val)
  \brief       Set the particular alternative function register.
  \param[in]   reg  Register selection \ref GPIO_FUNC_REGISTERS_t
  \param[in]   val  Register value
  \return      \ref execution_status

  \fn          int32_t GPIO_ResetAltFuncRegister (GPIO_FUNC_REGISTERS_t reg)
  \brief       Reset the particular alternative function register.
  \param[in]   reg  Register selection \ref GPIO_FUNC_REGISTERS_t
  \return      \ref execution_status

  \fn          void GPIO_SignalEvent (uint32_t event)
  \brief       Signal gpio events.
  \param[in]   event notification mask
  \return      none
*/

typedef void (*GPIO_SignalEvent_t) (uint32_t event);  ///< Pointer to \ref GPIO_SignalEvent : Signal GPIO Event.

/**
\brief Debounce filter configuration.
*/
typedef struct _GPIO_DBF_CFG_t
{
    uint8_t                count           :8;   ///< debounce filter count value
    GPIO_DBC_CLK_t         clk_source      :1;   ///< debounce filter clock source
    uint8_t                                :7;   ///< reserved
} GPIO_DBF_CFG_t;

/**
\brief GPIO interrupt priority configuration
*/
typedef struct _GPIO_PRI_CFG_t
{
    uint32_t               preempt_pri     :3;   ///< preempt priority
    uint32_t                               :13;  ///< reserved
    uint32_t               subgrp_pri      :3;   ///< subgroup priority
    uint32_t                               :13;  ///< reserved
} GPIO_PRI_CFG_t;

/**
\brief GPIO Driver configuration.
*/
typedef struct _GPIO_CFG_t
{
    GPIO_DRIVE_STRENGTHS_t drive_strengths :1;   ///< drive strengths configuration
    uint8_t                                :7;   ///< reserved
    GPIO_DBF_CFG_t         debounce_cfg;         ///< debounce filter configuration
} GPIO_CFG_t;

/**
\brief GPIO PAD configuration.
*/
typedef struct _GPIO_PAD_CFG_t
{
    GPIO_PULL_t            pull_mode       :2;   ///< pull control
    uint8_t                                :6;   ///< reserved
    GPIO_DRIVE_t           drive_mode      :2;   ///< drive mode
    uint8_t                                :6;   ///< reserved
    GPIO_LPF_t             lpf_en          :1;   ///< low pass filter enable
    uint16_t                               :3;   ///< reserved
    GPIO_MODE_t            io_mode         :12;  ///< io mode
} GPIO_PAD_CFG_t;

/**
\brief GPIO INT configuration.
*/
typedef struct _GPIO_INT_CFG_t
{
    GPIO_SEL_t             src_sel         :4;   ///< interrupt source selection
    GPIO_EVENT_t           event           :3;   ///< event selection
    GPIO_EN_DIS_t          debounce_en     :1;   ///< debouce filter enable
    GPIO_EN_DIS_t          interrup_en     :1;   ///< interrupt enable flag
    uint8_t                                :7;   ///< reserved
} GPIO_INT_CFG_t;

/**
\brief External clock pad configuration.
*/
typedef struct _GPIO_EXTCLK_CFG_t
{
    GPIO_PULL_t            pull_mode       :2;   ///< pull control
    GPIO_LPF_t             lpf_en          :1;   ///< low pass filter enable
    uint8_t                                :6;   ///< reserved
} GPIO_EXTCLK_CFG_t;

/**
\brief JTAG configuration.
*/
typedef struct _GPIO_JTAG_CFG_t
{
    GPIO_LPF_t             jtck_lpf_en     :1;   ///< jtck low pass filter enable
    GPIO_LPF_t             jtms_lpf_en     :1;   ///< jtms low pass filter enable
    GPIO_EN_DIS_t          jtag_data_en    :1;   ///< jtag data on DIO[14:15] available
    GPIO_EN_DIS_t          jtag_trst_en    :1;   ///< jtag trst on DIO13 available
    GPIO_PULL_t            jtck_pull       :2;   ///< jtck pul mode
    GPIO_PULL_t            jtms_pull       :2;   ///< jtms pul mode
    GPIO_DRIVE_t           jtms_drive      :2;   ///< jtms drive mode
    uint8_t                                :6;   ///< reserved
} GPIO_JTAG_CFG_t;

/**
\brief Access structure of the GPIO Driver.
*/
typedef struct _DRIVER_GPIO_t {
    ARM_DRIVER_VERSION  (*GetVersion)            (void);                                          ///< Pointer to \ref GPIO_GetVersion : Get driver version.
    int32_t             (*Initialize)            (GPIO_SignalEvent_t cb);                         ///< Pointer to \ref GPIO_Initialize : Initialize the gpio driver.
    int32_t             (*Configure)             (const GPIO_CFG_t *cfg);                         ///< Pointer to \ref GPIO_Configure : Configure common gpio settings.
    int32_t             (*ConfigurePad)          (GPIO_SEL_t sel, const GPIO_PAD_CFG_t *cfg);     ///< Pointer to \ref GPIO_ConfigurePad : Configure the gpio pad.
    int32_t             (*ConfigureInterrupt)    (GPIO_INT_SEL_t sel, const GPIO_INT_CFG_t *cfg); ///< Pointer to \ref GPIO_ConfigureInterrupt : Configure the gpio interrupt.
    int32_t             (*SetInterruptPriority)  (GPIO_INT_SEL_t sel, const GPIO_PRI_CFG_t *pri); ///< Pointer to \ref GPIO_SetInterruptPriority : Configure gpio interrupt priority.
    int32_t             (*ConfigureExtClk)       (const GPIO_EXTCLK_CFG_t *cfg);                  ///< Pointer to \ref GPIO_ConfigureExtClk: Configure the gpio external clock mode.
    int32_t             (*ConfigureNmi)          (GPIO_NMI_POL_t cfg);                            ///< Pointer to \ref GPIO_ConfigureNmi : Configure the nmi polarity.
    int32_t             (*ConfigureJtag)         (const GPIO_JTAG_CFG_t *cfg);                    ///< Pointer to \ref GPIO_ConfigureJtag : Configure the gpio jtag mode.
    void                (*SetDir)                (GPIO_SEL_t sel, GPIO_DIR_t dir);                ///< Pointer to \ref GPIO_SetDir : Set particular gpio pad direction.
    void                (*SetHigh)               (GPIO_SEL_t sel);                                ///< Pointer to \ref GPIO_SetHigh : Set particular gpio pad.
    void                (*ToggleValue)           (GPIO_SEL_t sel);                                ///< Pointer to \ref GPIO_ToggleValue : Toggle particular gpio pad.
    void                (*SetLow)                (GPIO_SEL_t sel);                                ///< Pointer to \ref GPIO_SetLow : Reset particular gpio pad.
    uint32_t            (*GetValue)              (GPIO_SEL_t sel);                                ///< Pointer to \ref GPIO_GetValue : Return the selected gpio value.
    int32_t             (*SetAltFuncRegister)    (GPIO_FUNC_REGISTERS_t sel, uint32_t val);       ///< Pointer to \ref GPIO_SetAltFuncRegister : Set gpio alternative function register.
    int32_t             (*ResetAltFuncRegister)  (GPIO_FUNC_REGISTERS_t sel);                     ///< Pointer to \ref GPIO_ResetAltFuncRegister : Reset gpio alternative function register.
} const DRIVER_GPIO_t;

#ifdef  __cplusplus
}
#endif

#endif /* DRIVER_GPIO_H_ */
