/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * SPI_RSLxx.c
 * - CMSIS-Driver implementation for RSLxx SPI
 * ------------------------------------------------------------------------- */

#include <GPIO_RSLxx.h>
#include <DMA_RSLxx.h>
#include <SPI_RSLxx.h>

#define ARM_SPI_DRV_VERSION    ARM_DRIVER_VERSION_MAJOR_MINOR(1, 0)

/* Driver Version */
static const ARM_DRIVER_VERSION DriverVersion =
{
    ARM_SPI_API_VERSION,
    ARM_SPI_DRV_VERSION
};

/* Driver Capabilities */
static const ARM_SPI_CAPABILITIES DriverCapabilities =
{
    0,    /* Simplex Mode (Master and Slave) */
    0,    /* TI Synchronous Serial Interface */
    0,    /* Microwire Interface */
    0     /* Signal Mode Fault event */
};

#if RTE_SPI0

/* SPI0 Run-Time Information */
static SPI_INFO_t SPI0_Info = {
    .default_cfg = {                                               /* spi0 default configuration */
        .sclk_pin            = RTE_SPI0_SCLK_PIN_DEFAULT,          /* spi0 default sclk pin */
        .ssel_pin            = RTE_SPI0_SSEL_PIN_DEFAULT,          /* spi0 default ssel pin */
        .miso_pin            = RTE_SPI0_MISO_PIN_DEFAULT,          /* spi0 default miso pin */
        .mosi_pin            = RTE_SPI0_MOSI_PIN_DEFAULT,          /* spi0 default mosi pin */
#if (SPI_DMA_CODE_EN)
        .rx_dma_ch           = RTE_SPI0_RX_DMA_CH_DEFAULT,         /* spi0 default rx dma channel */
        .tx_dma_ch           = RTE_SPI0_TX_DMA_CH_DEFAULT,         /* spi0 default tx dma channel */
#if (RTE_SPI0_DMA_EN_DEFAULT)
        .dma_rx_cb           = SPI0_RX_DMAHandler,                 /* spi0 default rx callback function */
        .dma_tx_cb           = SPI0_TX_DMAHandler,                 /* spi0 default tx callback function */
#endif /* if (RTE_SPI0_DMA_EN_DEFAULT) */
        .dma_en              = RTE_SPI0_DMA_EN_DEFAULT,            /* spi0 default dma enable flag */
        .dma_trg             = DMA_TRG_SPI0,                   /* spi0 default dma target selection */
        .dma_rx_busy         = 0,                                  /* !!!WORKAROUND!!! for dma rx counter not being
                                                                    * reset before new transmission */
        .dma_tx_busy         = 0,                                  /* !!!WORKAROUND!!! for dma tx counter not being
                                                                    * reset before new transmission */
#endif /* if (SPI_DMA_CODE_EN) */
        .pri_cfg = {                                               /* spi0 default priority configuration */
            .preempt_pri     = RTE_SPI0_INT_PREEMPT_PRI_DEFAULT,   /* spi0 default pre-empt priority value */
            .subgrp_pri      = RTE_SPI0_INT_SUBGRP_PRI_DEFAULT     /* spi0 default subgroup priority value */
        }                                                          
    },
    .status                  = { 0 },                              /* spi0 current status */
    .state                   = 0,                                  /* spi0 current state */
    .mode                    = 0,                                  /* spi0 current mode */
    .word_size               = 0                                   /* spi0 current word size */
};

/* SPI0 interrupt information */
static SPI_INT_INFO_t SPI0_intInfo = {                             /* spi0 interrupt info */
#if (SPI_CM3_CODE_EN)
    .rx_irqn                 = SPI0_RX_IRQn,                       /* spi0 rx interrupt number */
    .tx_irqn                 = SPI0_TX_IRQn,                       /* spi0 tx interrupt number */
#endif /* if (SPI_CM3_CODE_EN) */
#if (SPI_DMA_CODE_EN)
    .err_irqn                = SPI0_ERROR_IRQn,                    /* spi0 error interrupt number */
#endif /* if (SPI_DMA_CODE_EN) */
    .cb                      = 0,                                  /* spi0 callback function pointer */
};

/* SPI0 transfer information */
static SPI_TRANSFER_INFO_t SPI0_TransferInfo = { 0 };              /* transfer info */

/* SPI0 Resources */
static const SPI_RESOURCES_t SPI0_Resources =
{
    .reg                     = (SPI1_Type *)SPI0,                  /* spi0 hw registers */
    .intInfo                 = &SPI0_intInfo,                      /* spi0 interrupt info */
    .info                    = &SPI0_Info,                         /* spi0 runtime info */
    .xfer                    = &SPI0_TransferInfo                  /* spi0 transfer info*/
};

#endif    /* RTE_SPI0 */

#if RTE_SPI1

/* SPI0 Run-Time Information */
static SPI_INFO_t SPI1_Info = {
    .default_cfg = {                                               /* spi1 default configuration */
        .sclk_pin            = RTE_SPI1_SCLK_PIN_DEFAULT,          /* spi1 default sclk pin */
        .ssel_pin            = RTE_SPI1_SSEL_PIN_DEFAULT,          /* spi1 default ssel pin */
        .miso_pin            = RTE_SPI1_MISO_PIN_DEFAULT,          /* spi1 default miso pin */
        .mosi_pin            = RTE_SPI1_MOSI_PIN_DEFAULT,          /* spi1 default mosi pin */
#if (SPI_DMA_CODE_EN)
        .rx_dma_ch           = RTE_SPI1_RX_DMA_CH_DEFAULT,         /* spi1 default rx dma channel */
        .tx_dma_ch           = RTE_SPI1_TX_DMA_CH_DEFAULT,         /* spi1 default tx dma channel */
#if (RTE_SPI1_DMA_EN_DEFAULT)
        .dma_rx_cb           = SPI1_RX_DMAHandler,                 /* spi1 default rx callback function */
        .dma_tx_cb           = SPI1_TX_DMAHandler,                 /* spi1 default tx callback function */
#endif /* if (RTE_SPI1_DMA_EN_DEFAULT) */
        .dma_en              = RTE_SPI1_DMA_EN_DEFAULT,            /* spi1 default dma enable flag */
        .dma_trg             = DMA_TRG_SPI1,                   /* spi1 default dma target selection */
        .dma_rx_busy         = 0,                                  /* !!!WORKAROUND!!! for dma rx counter not being
                                                                    * reset before new transmission */
        .dma_tx_busy         = 0,                                  /* !!!WORKAROUND!!! for dma tx counter not being
                                                                    * reset before new transmission */
#endif /* if (SPI_DMA_CODE_EN) */
        .pri_cfg = {                                               /* spi1 default priority configuration */
            .preempt_pri     = RTE_SPI1_INT_PREEMPT_PRI_DEFAULT,   /* spi1 default pre-empt priority value */
            .subgrp_pri      = RTE_SPI1_INT_SUBGRP_PRI_DEFAULT     /* spi1 default subgroup priority value */
        }                                                          
    },
    .status                  = { 0 },                              /* spi1 current status */
    .state                   = 0,                                  /* spi1 current state */
    .mode                    = 0,                                  /* spi1 current mode */
    .word_size               = 0                                   /* spi1 current word size */
};

/* SPI1 interrupt information */
static SPI_INT_INFO_t SPI1_intInfo = {                             /* spi1 interrupt info */
#if (SPI_CM3_CODE_EN)
    .rx_irqn                 = SPI1_RX_IRQn,                       /* spi1 rx interrupt number */
    .tx_irqn                 = SPI1_TX_IRQn,                       /* spi1 tx interrupt number */
#endif /* if (SPI_CM3_CODE_EN) */
#if (SPI_DMA_CODE_EN)
    .err_irqn                = SPI1_ERROR_IRQn,                    /* spi1 error interrupt number */
#endif /* if (SPI_DMA_CODE_EN) */
    .cb                      = 0,                                  /* spi1 callback function pointer */
};

/* SPI1 transfer information */
static SPI_TRANSFER_INFO_t SPI1_TransferInfo = { 0 };              /* transfer info */

/* SPI1 Resources */
static const SPI_RESOURCES_t SPI1_Resources =
{
    .reg                     = (SPI1_Type *)SPI1,                  /* spi1 hw registers */
    .intInfo                 = &SPI1_intInfo,                      /* spi1 interrupt info */
    .info                    = &SPI1_Info,                         /* spi1 runtime info */
    .xfer                    = &SPI1_TransferInfo                  /* spi1 transfer info*/
};

#endif    /* RTE_SPI1 */

/* Prepare gpio driver pointer */
static DRIVER_GPIO_t *gpio = &Driver_GPIO;

#if (SPI_DMA_CODE_EN)

/* Prepare dma driver pointer */
static DRIVER_DMA_t *dma = &Driver_DMA;
#endif /* if (SPI_DMA_CODE_EN) */

/* ----------------------------------------------------------------------------
 * Function      : void SPI_PrepareNextTXData(SPI_RESOURCES* spi)
 * ----------------------------------------------------------------------------
 * Description   : Support function internal to the driver. Called by
 *                 SPI_Transfer at the start of a transfer and by
 *                 SPI_TX_IRQ_Handler for each new data transfer. This function
 *                 prepares the data for transfer by converting the byte array
 *                 into the configured word size (8, 16, 24 or 32).
 * Inputs        : spi              - Pointer to SPI resources
 * Outputs       : TX_Data ready for next transfer. If tx_buf is NULL, returns
 * Assumptions   : none
 * ------------------------------------------------------------------------- */
static uint32_t SPI_PrepareNextTXData(const SPI_RESOURCES_t *spi)
{
    uint32_t data = spi->xfer->def_val;
    uint8_t i, ws, max;

    /* Get word size configuration (in bytes) */
    ws = spi->info->word_size;

    /* Check if tx buffer is not null */
    if (spi->xfer->tx_buf)
    {
        /* Check if all bytes should be send */
        if (spi->xfer->num - spi->xfer->tx_cnt < ws)
        {
            /* Set number of bytes left to be send */
            max = spi->xfer->num - spi->xfer->tx_cnt;
        }
        else
        {
            /* Set specifiec word size */
            max = ws;
        }

        /* Convert byte array into 32bit integer */
        for (i = 0, data = 0; i < max; i++)
        {
            data |= spi->xfer->tx_buf[i] << (i * SPI_BYTE_SIZE);
        }

        /* Update the tx buffer */
        spi->xfer->tx_buf += ws;
    }

    /* Return prepared tx data */
    return data;
}

#if (SPI_CM3_CODE_EN)
/* ----------------------------------------------------------------------------
 * Function      : void SPI_TransferCompleted(SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Support function internal to the driver. Called after a
 *                 transfer is completed. Clear flags and set operation to idle
 * Inputs        : spi              - Pointer to SPI resources
 * Outputs       : none
 * Assumptions   : none
 * ------------------------------------------------------------------------- */
static void SPI_TransferCompleted(const SPI_RESOURCES_t *spi)
{
    /* Clear busy flag */
    spi->info->status.busy = SPI_FREE;

    /* Deactivate SSEL line, if in master mode/SS_HW_OUT */
    if ((spi->info->mode & ARM_SPI_MODE_MASTER) &&
        (spi->info->mode & ARM_SPI_SS_MASTER_HW_OUTPUT))
    {
        spi->reg->CTRL1 = (spi->reg->CTRL1 & ~SPI0_CS_1) | SPI0_CS_1;
    }

    /* Stop transfer and set RW operation to Idle */
    spi->reg->CTRL1 &= (~SPI0_START & ~SPI0_CTRL1_SPI0_RW_CMD_Mask);
}

#endif /* if (SPI_CM3_CODE_EN) */

/* ----------------------------------------------------------------------------
 * Function      : void SPI_CalculatePrescale(uint32_t bus_speed,
 *                                            SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : SPI clock is a divided form of System Clock. This function
 *                 calculates the SPI prescaler according to the desired bus
 *                 speed and current System Clock.
 * Inputs        : bus_speed              - desired bus speed in bps
 *                 system_clock           - current system clock
 * Outputs       : An integer in the range [0,9]
 * Assumptions   : The system clock can be divided into the required bus speed.
 *                 For precise speeds, make sure: system_clock = bus_speed << x
 *                 Where x is in the range [1,10]
 * ------------------------------------------------------------------------- */
static uint8_t SPI_CalculatePrescale(uint32_t bus_speed, uint32_t system_clock)
{
    uint8_t spi_prescale;

    /* SPI Clock is sourced from SysClk. Find closest prescale value */
    for (spi_prescale = 0U; spi_prescale < SPI_MAX_PRESCALE; spi_prescale++)
    {
        /* SPI prescale condition */
        if (bus_speed >= (system_clock >> (spi_prescale + 1U)))
        {
            break;
        }
    }
    return spi_prescale;
}

#if (SPI_CM3_CODE_EN)
/* ----------------------------------------------------------------------------
 * Function      : void SPI_TX_IRQHandler(const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a byte is transmitted. Place
 *                 transmit data into the SPI, sequentially, byte per byte.
 *                 Manage flags and counters. Application is notified via
 *                 callback function once the transmission is complete
 * Inputs        : spi    - Pointer to SPI resources
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SPI_TX_IRQHandler(const SPI_RESOURCES_t *spi)
{
    uint32_t event = 0U;

    /* Check if device is busy */
    if (spi->info->status.busy)
    {
        /* If data lost occurred on last transmission */
        if ((spi->reg->STATUS & SPI0_OVERRUN_TRUE)  ||
            (spi->reg->STATUS & SPI0_UNDERRUN_TRUE))
        {
            /* Indicate data was lost */
            spi->info->status.data_lost = 1U;
            event = ARM_SPI_EVENT_DATA_LOST;

            /* Clear error status */
            spi->reg->STATUS |= SPI0_UNDERRUN_CLEAR | SPI0_OVERRUN_CLEAR;
        }

        /* Update the tx counter */
        spi->xfer->tx_cnt += spi->info->word_size;

        /* Check if tx buffer is full */
        if (spi->xfer->tx_cnt > spi->xfer->num)
        {
            /* Update the tx counter */
            spi->xfer->tx_cnt = spi->xfer->num;
        }

        /* If there is more data to transfer */
        if (spi->xfer->tx_cnt < spi->xfer->num)
        {
            /* Fill spi tx buffer */
            spi->reg->TX_DATA = SPI_PrepareNextTXData(spi);

            /* Start next transfer (if master) */
            spi->reg->CTRL1 |= SPI0_START;
        }
        else if (spi->xfer->tx_cnt == spi->xfer->rx_cnt)
        {
            /* Clear flags and stop transfer. */
            SPI_TransferCompleted(spi);

            event = ARM_SPI_EVENT_TRANSFER_COMPLETE;
        }

        /* If an event happened and the application registered a callback */
        if (event && spi->intInfo->cb)
        {
            spi->intInfo->cb(event);
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI_RX_IRQHandler(const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a byte is received. Store
 *                 received data in the buffer, manage flags and counters.
 *                 Application is notified via callback function once the
 *                 receive is complete
 * Inputs        : spi    - Pointer to SPI resources
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SPI_RX_IRQHandler(const SPI_RESOURCES_t *spi)
{
    uint32_t data, i, ws;
    uint32_t event = 0U;

    /* Check if device is busy and if there is space for new data */
    if (spi->info->status.busy && spi->xfer->rx_cnt < spi->xfer->num)
    {
        /* If data lost occurred on last transmission */
        if ((spi->reg->STATUS & SPI0_OVERRUN_TRUE)  ||
            (spi->reg->STATUS & SPI0_UNDERRUN_TRUE))
        {
            spi->info->status.data_lost = 1U;
            event = ARM_SPI_EVENT_DATA_LOST;

            /* Clear error status */
            spi->reg->STATUS |= SPI0_UNDERRUN_CLEAR | SPI0_OVERRUN_CLEAR;
        }

        /* Read received data from SPI RX_DATA register */
        data = spi->reg->RX_DATA;

        /* Get word size configuration (in bytes) */
        ws = spi->info->word_size;

        /* Split received data into bytes and store into rx_buf */
        for (i = 0; i < ws; i++)
        {
            /* Check if rx buffer is not null */
            if (spi->xfer->rx_buf != NULL)
            {
                /* Get single byte */
                *(spi->xfer->rx_buf++) = (uint8_t)(data >> (i * SPI_BYTE_SIZE));
            }

            /* Update the rx counter */
            spi->xfer->rx_cnt++;

            /* Check if rx buffer is full */
            if (spi->xfer->rx_cnt == spi->xfer->num)
            {
                break;
            }
        }

        /* If transfer has finished */
        if ((spi->xfer->rx_cnt == spi->xfer->num) &&
            (spi->xfer->tx_cnt == spi->xfer->rx_cnt))
        {
            /* Clear flags and stop transfer. */
            SPI_TransferCompleted(spi);

            event = ARM_SPI_EVENT_TRANSFER_COMPLETE;
        }

        /* If an event happened and the application registered a callback */
        if (event && spi->intInfo->cb)
        {
            spi->intInfo->cb(event);
        }
    }
}

#endif /* if (SPI_CM3_CODE_EN) */

#if (SPI_DMA_CODE_EN)
/* ----------------------------------------------------------------------------
 * Function      : void SPI_TX_DMAHandler(uint32_t event, SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Called by dma driver when a dma transfer is done.
 *                 Manage flags and counters. Application is notified via
 *                 callback function once the transmission is complete
 * Inputs        : spi    - spi instance
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SPI_TX_DMAHandler(uint32_t event, SPI_RESOURCES_t *spi)
{
    /* Prepare status description */
    event = 0;

    /* Check if device is busy */
    if (spi->info->status.busy)
    {
        /* Stop the dma */
        DMA_STATUS_t status = dma->GetStatus(spi->info->default_cfg.tx_dma_ch);

        /* Check if dma transfer has finished */
        if (status.completed)
        {
            /* Stop the dma */
            dma->Stop(spi->info->default_cfg.tx_dma_ch);

            /* Clear tx part of busy flag */
            spi->info->status.busy &= SPI_TX_BUSY;

            /* Check if full spi transfer has finished */
            if (!spi->info->status.busy)
            {
                /* Indicate transfer complete */
                event = ARM_SPI_EVENT_TRANSFER_COMPLETE;

                /* Deactivate SSEL line, if in master mode/SS_HW_OUT */
                if ((spi->info->mode & ARM_SPI_MODE_MASTER) &&
                    (spi->info->mode & ARM_SPI_SS_MASTER_HW_OUTPUT))
                {
                    spi->reg->CTRL1 = (spi->reg->CTRL1 & ~SPI0_CS_1) | SPI0_CS_1;
                }

                /* Stop transfer and set RW operation to Idle */
                spi->reg->CTRL1 &= (~SPI0_START & ~SPI0_CTRL1_SPI0_RW_CMD_Mask);
            }
        }

        /* Check if first word was transmitted */
        if (status.started)
        {
            /* Indicate that first byte was transmitted */
            spi->info->dma_tx_busy = 1;
        }

        /* Check if dma error occured. */
        if (status.error)
        {
            /* Indicate data was lost */
            event = ARM_SPI_EVENT_DATA_LOST;
        }
    }

    /* If an event happened and the application registered a callback */
    if (event && spi->intInfo->cb)
    {
        spi->intInfo->cb(event);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI_DMAHandler(uint32_t event, SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Called by dma driver when a dma transfer is done.
 *                 Manage flags and counters. Application is notified via
 *                 callback function once the transmission is complete
 * Inputs        : spi    - spi instance
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SPI_RX_DMAHandler(uint32_t event, SPI_RESOURCES_t *spi)
{
    /* Prepare status description */
    event = 0;

    /* Check if device is busy */
    if (spi->info->status.busy)
    {
        /* Stop the dma */
        DMA_STATUS_t status = dma->GetStatus(spi->info->default_cfg.rx_dma_ch);

        /* Check if dma transfer has finished */
        if (status.completed)
        {
            /* Stop the dma */
            dma->Stop(spi->info->default_cfg.rx_dma_ch);

            /* Clear tx part of busy flag */
            spi->info->status.busy &= SPI_RX_BUSY;

            /* Check if full spi transfer has finished */
            if (!spi->info->status.busy)
            {
                /* Indicate transfer complete */
                event = ARM_SPI_EVENT_TRANSFER_COMPLETE;

                /* Deactivate SSEL line, if in master mode/SS_HW_OUT */
                if ((spi->info->mode & ARM_SPI_MODE_MASTER) &&
                    (spi->info->mode & ARM_SPI_SS_MASTER_HW_OUTPUT))
                {
                    spi->reg->CTRL1 = (spi->reg->CTRL1 & ~SPI0_CS_1) | SPI0_CS_1;
                }

                /* Stop transfer and set RW operation to Idle */
                spi->reg->CTRL1 &= (~SPI0_START & ~SPI0_CTRL1_SPI0_RW_CMD_Mask);
            }
        }

        /* Check if first word was transmitted */
        if (status.started)
        {
            /* Indicate that first byte was transmitted */
            spi->info->dma_rx_busy = 1;
        }

        /* Check if dma error occured. */
        if (status.error)
        {
            /* Indicate data loss */
            event = ARM_SPI_EVENT_DATA_LOST;
        }
    }

    /* If an event happened and the application registered a callback */
    if (event && spi->intInfo->cb)
    {
        spi->intInfo->cb(event);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI_ERR_IRQHandler(const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when an overrun or underrun error
 *                    occurs. Application is notified via callback function
 *                    that the transmission error occured.
 * Inputs        : spi    - Pointer to SPI resources
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SPI_ERR_IRQHandler(const SPI_RESOURCES_t *spi)
{
    uint32_t event = 0U;

    if (spi->info->status.busy)
    {
        /* If data lost occurred on last transmission */
        if ((spi->reg->STATUS & SPI0_OVERRUN_TRUE)  ||
            (spi->reg->STATUS & SPI0_UNDERRUN_TRUE))
        {
            spi->info->status.data_lost = 1U;
            event = ARM_SPI_EVENT_DATA_LOST;

            /* Clear error status */
            spi->reg->STATUS |= SPI0_UNDERRUN_CLEAR | SPI0_OVERRUN_CLEAR;
        }


        /* If an event happened and the application registered a callback */
        if (event && spi->intInfo->cb)
        {
            /* Call the callback function */
            spi->intInfo->cb(event);
        }
    }
}

#endif /* if (SPI_DMA_CODE_EN) */

/* ----------------------------------------------------------------------------
 * Function      : void SPIx_GetVersion(void)
 * ----------------------------------------------------------------------------
 * Description   : Get driver version
 * Inputs        : None
 * Outputs       : ARM_DRIVER_VERSION
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_DRIVER_VERSION SPIx_GetVersion(void)
{
    return DriverVersion;
}

/* ----------------------------------------------------------------------------
 * Function      : void SPIx_GetCapabilities(void)
 * ----------------------------------------------------------------------------
 * Description   : Get spi driver capabilities
 * Inputs        : None
 * Outputs       : ARM_SPI_CAPABILITIES
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_SPI_CAPABILITIES SPIx_GetCapabilities(void)
{
    return DriverCapabilities;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t SPI_SetInterruptPriority(SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Configure spi interrupts priority.
 * Inputs        : spi - spi instance
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SPI_SetInterruptPriority(SPI_RESOURCES_t *spi)
{
    /* Encoded priority value */
    uint32_t encodedPri = 0;

    /* Encode priority configuration word */
    encodedPri = NVIC_EncodePriority(NVIC_GetPriorityGrouping(), spi->info->default_cfg.pri_cfg.preempt_pri,
                                     spi->info->default_cfg.pri_cfg.subgrp_pri);

#if (SPI_DMA_CODE_EN)

    /* Check if dma was enabled */
    if (!spi->info->default_cfg.dma_en)
#endif /* if (SPI_DMA_CODE_EN) */
    {
#if (SPI_CM3_CODE_EN)

        /* Set the priority of spi rx interrupt */
        NVIC_SetPriority(spi->intInfo->rx_irqn, encodedPri);

        /* Set the priority of spi tx interrupt */
        NVIC_SetPriority(spi->intInfo->tx_irqn, encodedPri);
#endif /* if (SPI_CM3_CODE_EN) */
    }
#if (SPI_DMA_CODE_EN)
    else
    {
        /* Set the priority of spi error interrupt */
        NVIC_SetPriority(spi->intInfo->err_irqn, encodedPri);
    }
#endif /* if (SPI_DMA_CODE_EN) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI_Initialize(ARM_SPI_SignalEvent_t cb_event,
 *                                     const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Initialize SPI flags, GPIO pins and reference to
 *                 callback function.
 * Inputs        : cb_event - pointer to callback function (optional)
 *                 spi    - Pointer to SPI resources
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SPI_Initialize(ARM_SPI_SignalEvent_t cb_event,
                              const SPI_RESOURCES_t *spi)
{
    /* Check if driver is already initialized */
    if (spi->info->state & SPI_INITIALIZED)
    {
        /* Driver is already initialized */
        return ARM_DRIVER_OK;
    }

    /* Initialize SPI Run-Time Resources */
    spi->intInfo->cb             = cb_event;
    spi->info->status.busy       = SPI_FREE;
    spi->info->status.data_lost  = 0U;
    spi->info->status.mode_fault = 0U;

    /* Clear transfer information */
    memset(spi->xfer, 0, sizeof(SPI_TRANSFER_INFO_t));

    /* Indicate that driver is initialized */
    spi->info->state = SPI_INITIALIZED;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI_Uninitialize(const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Uninitialize SPI flags, GPIO pins and remove reference to
 *                 callback function.
 * Inputs        : spi - Pointer to SPI resources
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SPI_Uninitialize(const SPI_RESOURCES_t *spi)
{
    /* Prepare pins configuration */
    const GPIO_PAD_CFG_t gpioLocCfg = {
        .pull_mode     = GPIO_WEAK_PULL_UP,
        .drive_mode    = GPIO_6X,
        .lpf_en        = GPIO_LPF_DISABLE,
        .io_mode       = GPIO_MODE_DISABLED
    };

    /* Reset pin configuration */
    gpio->ConfigurePad(spi->info->default_cfg.sclk_pin, &gpioLocCfg);
    gpio->ConfigurePad(spi->info->default_cfg.ssel_pin, &gpioLocCfg);
    gpio->ConfigurePad(spi->info->default_cfg.miso_pin, &gpioLocCfg);
    gpio->ConfigurePad(spi->info->default_cfg.mosi_pin, &gpioLocCfg);

    /* Reset DIO_SPI_SRC configuration */
    if (spi->reg == (SPI1_Type *)SPI0)
    {
        gpio->ResetAltFuncRegister(GPIO_FUNC_REG_SPI0);
    }
    else
    {
        gpio->ResetAltFuncRegister(GPIO_FUNC_REG_SPI1);
    }

    /* Clear SPI state */
    spi->info->state = 0U;

    /* Remove reference to callback function */
    spi->intInfo->cb = NULL;

    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI_PowerControl(ARM_POWER_STATE state,
 *                                       const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Operate the power modes of the SPI interface
 * Inputs        : state - ARM_POWER_FULL or ARM_POWER_OFF
 *                 spi - Pointer to SPI resources
 * Outputs       : ARM_DRIVER_OK - if the operation is successful
 *                 ARM_DRIVER_ERROR_UNSUPPORTED - if argument is ARM_POWER_LOW
 *                                                or an invalid value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SPI_PowerControl(ARM_POWER_STATE state,
                                const SPI_RESOURCES_t *spi)
{
    /* Check driver state */
    switch (state)
    {
        case ARM_POWER_OFF:
        {
#if (SPI_DMA_CODE_EN)

            /* Check if dma was enabled */
            if (!spi->info->default_cfg.dma_en)
#endif /* if (SPI_DMA_CODE_EN) */
            {
#if (SPI_CM3_CODE_EN)

                /* Disable spi interrupts */
                NVIC_DisableIRQ(spi->intInfo->rx_irqn);
                NVIC_DisableIRQ(spi->intInfo->tx_irqn);

                /* Clear pending spi interrupts in nvic */
                NVIC_ClearPendingIRQ(spi->intInfo->rx_irqn);
                NVIC_ClearPendingIRQ(spi->intInfo->tx_irqn);
#endif /* if (SPI_CM3_CODE_EN) */
            }
#if (SPI_DMA_CODE_EN)
            else
            {
                /* Disable spi err interrupt */
                NVIC_DisableIRQ(spi->intInfo->err_irqn);

                /* Clear pending spi err interrupt in nvic */
                NVIC_ClearPendingIRQ(spi->intInfo->err_irqn);
            }
#endif /* if (SPI_DMA_CODE_EN) */

            /* Disable SPI */
            spi->reg->CTRL0 = SPI0_DISABLE;

            /* Clear status flags */
            spi->info->status.busy       = SPI_FREE;
            spi->info->status.data_lost  = 0U;
            spi->info->status.mode_fault = 0U;

            spi->info->state &= ~SPI_POWERED;
            break;
        }        

        case ARM_POWER_FULL:
        {
            /* Check if driver is already initialized */
            if ((spi->info->state & SPI_INITIALIZED) == 0U)
            {
                /* Return driver error */
                return ARM_DRIVER_ERROR;
            }

            /* Check if driver is already powered */
            if ((spi->info->state & SPI_POWERED) != 0U)
            {
                /* Return OK */
                return ARM_DRIVER_OK;
            }

            /* Clear Status flags */
            spi->info->status.busy       = SPI_FREE;
            spi->info->status.data_lost  = 0U;
            spi->info->status.mode_fault = 0U;
            spi->xfer->def_val           = 0U;

#if (SPI_DMA_CODE_EN)

            /* Check if dma was enabled */
            if (!spi->info->default_cfg.dma_en)
#endif /* if (SPI_DMA_CODE_EN) */
            {
#if (SPI_CM3_CODE_EN)

                /* Clear pending spi interrupts in nvic */
                NVIC_ClearPendingIRQ(spi->intInfo->tx_irqn);
                NVIC_ClearPendingIRQ(spi->intInfo->rx_irqn);

                /* Enable spi interrupts in nvic */
                NVIC_EnableIRQ(spi->intInfo->tx_irqn);
                NVIC_EnableIRQ(spi->intInfo->rx_irqn);
#endif /* if (SPI_CM3_CODE_EN) */
            }
#if (SPI_DMA_CODE_EN)
            else
            {
                /* Clear pending spi err interrupt in nvic */
                NVIC_ClearPendingIRQ(spi->intInfo->err_irqn);

                /* Enable spi err interrupt in nvic */
                NVIC_EnableIRQ(spi->intInfo->err_irqn);
            }
#endif /* if (SPI_DMA_CODE_EN) */

            /* Set powered flag */
            spi->info->state |= SPI_POWERED;
            break;
        }        

        case ARM_POWER_LOW:
        default:
        {
            /* Return unsupported error */
            return ARM_DRIVER_ERROR_UNSUPPORTED;
        }
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI_Control(uint32_t control, uint32_t arg,
 *                                  const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Control SPI Interface
 * Inputs        : control - Control operation
 *                 arg     - Argument of operation (optional)
 *                 spi     - Pointer to SPI resources
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SPI_Control(uint32_t control, uint32_t arg,
                           const SPI_RESOURCES_t *spi)
{
    uint32_t mode, ctrl0, ctrl1, spi_prescale;
    mode  = 0U;
#if (SPI_DMA_CODE_EN)
    DMA_CFG_t dmaCfgR, dmaCfgT;
#endif /* if (SPI_DMA_CODE_EN) */

    /* Prepare ctrl0 configuration */
    ctrl0 = (SPI0_OVERRUN_INT_DISABLE  |
             SPI0_UNDERRUN_INT_DISABLE |
             SPI0_CONTROLLER_CM3       |
             SPI0_MODE_SELECT_MANUAL   |
             SPI0_ENABLE);

#if (SPI_DMA_CODE_EN)
    /* Check if DMA should be used */
    if (spi->info->default_cfg.dma_en == SPI_DMA_ENABLE)
    {
        /* Update the ctrl0 configuration so auto mode is used */
        ctrl0 &= ~SPI0_CTRL0_SPI0_MODE_SELECT_Pos;
        ctrl0 |= SPI0_MODE_SELECT_AUTO;

        /* Prepare receiver mode dma configuration */
        dmaCfgR.src_sel       = spi->info->default_cfg.dma_trg;
        dmaCfgR.src_step_mode = DMA_STEP_STATIC;
        dmaCfgR.src_word_size = DMA_WORD_SIZE_8;
        dmaCfgR.dst_sel       = DMA_TRG_MEM;
        dmaCfgR.dst_step_mode = DMA_STEP_INC;
        dmaCfgR.dst_word_size = DMA_WORD_SIZE_32;
        dmaCfgR.byte_order    = DMA_ENDIANNESS_LITTLE;
        dmaCfgR.ch_priority   = DMA_CH_PRI_0;
        dmaCfgR.data_mode     = DMA_SINGLE;
        dmaCfgR.dst_step_size = DMA_STEP_SIZE_1;
        dmaCfgR.src_step_size = DMA_STEP_SIZE_1;

        /* Prepare transmitter dma configuration */
        dmaCfgT.src_sel       = DMA_TRG_MEM;
        dmaCfgT.src_step_mode = DMA_STEP_INC;
        dmaCfgT.src_word_size = DMA_WORD_SIZE_32;
        dmaCfgT.dst_sel       = spi->info->default_cfg.dma_trg;
        dmaCfgT.dst_step_mode = DMA_STEP_STATIC;
        dmaCfgT.dst_word_size = DMA_WORD_SIZE_8;
        dmaCfgT.byte_order    = DMA_ENDIANNESS_LITTLE;
        dmaCfgT.ch_priority   = DMA_CH_PRI_0;
        dmaCfgT.data_mode    = DMA_SINGLE;
        dmaCfgT.dst_step_size = DMA_STEP_SIZE_1;
        dmaCfgT.src_step_size = DMA_STEP_SIZE_1;
    }
#endif /* if (SPI_DMA_CODE_EN) */

    /* Prepare the ctrl1 configuration*/
    ctrl1 = SPI0_IDLE | SPI0_NOP | SPI0_CS_1;

    /* Check if driver is powered */
    if ((spi->info->state & SPI_POWERED) == 0U)
    {
        /* SPI not powered */
        return ARM_DRIVER_ERROR;
    }

    /* Abort transfer */
    if ((control & ARM_SPI_CONTROL_Msk) == ARM_SPI_ABORT_TRANSFER)
    {
        /* Stop transfer and set RW operation to Idle */
        spi->reg->CTRL1 &= (~SPI0_START & ~SPI0_CTRL1_SPI0_RW_CMD_Mask);

#if (SPI_CM3_CODE_EN)
        /* Clear pending spi interrupts in nvic */
        NVIC_ClearPendingIRQ(spi->intInfo->rx_irqn);
        NVIC_ClearPendingIRQ(spi->intInfo->tx_irqn);
#else
        /* Check if dma is being used */
        if (spi->info->default_cfg.dma_en == SPI_DMA_ENABLE)
        {
            /* Stop the dma transfer */
            dma->Stop(spi->info->default_cfg.rx_dma_ch);
            dma->Stop(spi->info->default_cfg.tx_dma_ch);
        }
#endif /* if (SPI_CM3_CODE_EN) */

        /* Clear transfer information and status */
        memset(spi->xfer, 0, sizeof(SPI_TRANSFER_INFO_t));
        spi->xfer->rx_cnt = 0U;
        spi->xfer->tx_cnt = 0U;
        spi->info->status.busy = SPI_FREE;
        return ARM_DRIVER_OK;
    }

    /* Check for busy flag */
    if (spi->info->status.busy)
    {
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Check the spi control command */
    switch (control & ARM_SPI_CONTROL_Msk)
    {
        /* Enter inactive mode */
        case ARM_SPI_MODE_INACTIVE:
        {
            /* Disable SPI */
            spi->reg->CTRL0   = 0U;
            spi->info->state &= ~SPI_CONFIGURED;
            spi->info->mode   = ((spi->info->mode & ~ARM_SPI_CONTROL_Msk) |
                                 ARM_SPI_MODE_INACTIVE);

            /* Return OK */
            return ARM_DRIVER_OK;
        }

        /* Enter master mode */
        case ARM_SPI_MODE_MASTER:
        {
            /* Set master mode */
            mode  |= ARM_SPI_MODE_MASTER;
            ctrl0 |= SPI0_SELECT_MASTER;

            /* Prepare pins configuration */
            GPIO_PAD_CFG_t gpioLocCfg = {
                .pull_mode     = GPIO_WEAK_PULL_UP,
                .drive_mode    = GPIO_6X,
                .lpf_en        = GPIO_LPF_DISABLE
            };

            /* Prepare offset for configuration */
            uint32_t offset  = 0;

            /* Check if spi1 was selected */
            if (spi->reg == SPI1)
            {
                /* Change offset to spi1 */
                offset  = SPI1_OFFSET;

                /* Reset SPI1 register */
                gpio->ResetAltFuncRegister(GPIO_FUNC_REG_SPI1);
            }
            else
            {
                /* Reset SPI0 register */
                gpio->ResetAltFuncRegister(GPIO_FUNC_REG_SPI0);
            }

            /* Configure sclk pin */
            gpioLocCfg.io_mode = GPIO_MODE_SPI0_CLK_OUT + offset;
            gpio->ConfigurePad(spi->info->default_cfg.sclk_pin, &gpioLocCfg);

            /* Configure ssel pin */
            gpioLocCfg.io_mode = GPIO_MODE_SPI0_CS_OUT + offset;
            gpio->ConfigurePad(spi->info->default_cfg.ssel_pin, &gpioLocCfg);

            /* Configure miso pin */
            gpioLocCfg.io_mode = GPIO_MODE_SPI0_SERI_IN + offset;
            gpio->ConfigurePad(spi->info->default_cfg.miso_pin, &gpioLocCfg);

            /* Configure mosi pin */
            gpioLocCfg.io_mode = GPIO_MODE_SPI0_SERO_OUT + offset;
            gpio->ConfigurePad(spi->info->default_cfg.mosi_pin, &gpioLocCfg);

            switch (control & ARM_SPI_SS_MASTER_MODE_Msk)
            {
                case ARM_SPI_SS_MASTER_SW:
                case ARM_SPI_SS_MASTER_HW_OUTPUT:
                {
                    /* SSEL pin already configured above */
                    mode |= (control & ARM_SPI_SS_MASTER_MODE_Msk);
                    break;
                }                

                case ARM_SPI_SS_MASTER_UNUSED:
                {
                    /* Unconfigure SSEL Pin */
                    gpioLocCfg.io_mode = GPIO_MODE_DISABLED;
                    gpio->ConfigurePad(spi->info->default_cfg.ssel_pin, &gpioLocCfg);
                    mode |= ARM_SPI_SS_MASTER_UNUSED;
                    break;
                }                

                case ARM_SPI_SS_MASTER_HW_INPUT:
                default:
                {
                    /* Return SS error */
                    return ARM_SPI_ERROR_SS_MODE;
                }
            }
            break;
        }   

        /* Enter slave mode */
        case ARM_SPI_MODE_SLAVE:
        {
            /* Set slave mode */
            mode  |= ARM_SPI_MODE_SLAVE;
            ctrl0 |= SPI0_SELECT_SLAVE;

            /* Prepare pins configuration */
            GPIO_PAD_CFG_t gpioLocCfg = {
                .pull_mode     = GPIO_WEAK_PULL_UP,
                .drive_mode    = GPIO_6X,
                .lpf_en        = GPIO_LPF_DISABLE
            };

            /* Prepare offset for configuration */
            uint32_t offset  = 0;

            /* Check if spi1 was selected */
            if (spi->reg == SPI1)
            {
                /* Change offset to spi1 */
                offset  = SPI1_OFFSET;
            }

            /* Configure sclk pin */
            gpioLocCfg.io_mode = GPIO_MODE_SPI0_CLK_IN + offset;
            gpio->ConfigurePad(spi->info->default_cfg.sclk_pin, &gpioLocCfg);

            /* Configure ssel pin */
            gpioLocCfg.io_mode = GPIO_MODE_SPI0_CS_IN + offset;
            gpio->ConfigurePad(spi->info->default_cfg.ssel_pin, &gpioLocCfg);

            /* Configure mosi pin */
            gpioLocCfg.io_mode = GPIO_MODE_SPI0_SERI_IN + offset;
            gpio->ConfigurePad(spi->info->default_cfg.mosi_pin, &gpioLocCfg);

            /* Configure miso pin */
            gpioLocCfg.io_mode = GPIO_MODE_SPI0_SERO_OUT + offset;
            gpio->ConfigurePad(spi->info->default_cfg.miso_pin, &gpioLocCfg);

            /* Return ss slave mode */
            switch (control & ARM_SPI_SS_SLAVE_MODE_Msk)
            {
                case ARM_SPI_SS_SLAVE_HW:
                {
                    /* Already configured above */
                    break;
                }                

                case ARM_SPI_SS_SLAVE_SW:
                {
                    /* Unconfigure SSEL Pin */
                    gpioLocCfg.io_mode = GPIO_MODE_DISABLED;
                    gpio->ConfigurePad(spi->info->default_cfg.ssel_pin, &gpioLocCfg);
                    break;
                }                

                default:
                {
                    /* Return ss error */
                    return ARM_SPI_ERROR_SS_MODE;
                }
            }
            break;
        }        

        /* Simplex mode not supported */
        case ARM_SPI_MODE_MASTER_SIMPLEX:
        case ARM_SPI_MODE_SLAVE_SIMPLEX:
        {
            /* Return unsupported mode error */
            return ARM_SPI_ERROR_MODE;
        }

        /* Set bus speed command */
        case ARM_SPI_SET_BUS_SPEED:
        {
            /* If system clock is not fast enough */
            if (SystemCoreClock < SPI_CLOCK_LIMIT_MUL * arg)
            {
                /* Return unsupported error */
                return ARM_DRIVER_ERROR_UNSUPPORTED;
            }

            /* Set SPI prescale */
            spi_prescale = SPI_CalculatePrescale(arg, SystemCoreClock);
            spi->reg->CTRL0 = ((spi->reg->CTRL0 &
                                ~SPI0_CTRL0_SPI0_PRESCALE_Mask) |
                               spi_prescale);

            /* Return OK */
            return ARM_DRIVER_OK;
        }

        /* Get bus speed command */
        case ARM_SPI_GET_BUS_SPEED:
        {
            /* Read SPI prescale value */
            spi_prescale = ((spi->reg->CTRL0 >> SPI0_CTRL0_SPI0_PRESCALE_Pos) &
                            SPI0_CTRL0_SPI0_PRESCALE_Mask);

            /* Return current bus speed */
            return (SystemCoreClock >> (spi_prescale + 1U));
        }

        /* Set default tx value command */
        case ARM_SPI_SET_DEFAULT_TX_VALUE:
        {
            spi->xfer->def_val = (uint16_t)(arg & 0xFFFFU);
            return ARM_DRIVER_OK;
        }

        /* SS control commnad */
        case ARM_SPI_CONTROL_SS:
        {
            if (((spi->info->mode & ARM_SPI_CONTROL_Msk) !=
                 ARM_SPI_MODE_MASTER) ||
                ((spi->info->mode & ARM_SPI_SS_MASTER_MODE_Msk) !=
                 ARM_SPI_SS_MASTER_SW))
            {
                /* SSEL line can only be controlled in master/SW mode */
                return ARM_DRIVER_ERROR;
            }

            /* Check ss active mode */
            if (arg == ARM_SPI_SS_ACTIVE)
            {
                /* Set SSEL pin low */
                spi->reg->CTRL1 = (spi->reg->CTRL1 & ~SPI0_CS_1) | SPI0_CS_0;
            }
            else
            {
                /* Set SSEL pin high */
                spi->reg->CTRL1 = (spi->reg->CTRL1 & ~SPI0_CS_1) | SPI0_CS_1;
            }

            /* Return OK */
            return ARM_DRIVER_OK;
        }

        default:
        {
            /* Return unsupported error */
            return ARM_DRIVER_ERROR_UNSUPPORTED;
        }
    }

    /* Frame format */
    switch (control & ARM_SPI_FRAME_FORMAT_Msk)
    {
        /* Polarity normal */
        case ARM_SPI_CPOL0_CPHA0:
        {
            /* Set polarity normal*/
            ctrl0 |= SPI0_CLK_POLARITY_NORMAL;
            break;
        }        

        /* Polarity inverse */
        case ARM_SPI_CPOL1_CPHA0:
        {
            /* Set polarity inverse */
            ctrl0 |= SPI0_CLK_POLARITY_INVERSE;
            break;
        }        

        /* Not supported modes */
        case ARM_SPI_CPOL1_CPHA1:
        case ARM_SPI_CPOL0_CPHA1:
        case ARM_SPI_TI_SSI:
        case ARM_SPI_MICROWIRE:
        default:
        {
            /* Return frame format error */
            return ARM_SPI_ERROR_FRAME_FORMAT;
        }
    }

    /* Set data bits */
    switch (control & ARM_SPI_DATA_BITS_Msk)
    {
        case ARM_SPI_DATA_BITS(8U):
        {
            /* Update the word size */
            ctrl1 |= SPI0_WORD_SIZE_8;

            /* Store the word size */
            spi->info->word_size = SPI_WORDSIZE_8;

#if (SPI_DMA_CODE_EN)

            /* Check if dma is being used */
            if (spi->info->default_cfg.dma_en == SPI_DMA_ENABLE)
            {
                /* Update the dma word size */
                dmaCfgR.src_word_size = DMA_WORD_SIZE_8;
                dmaCfgT.dst_word_size = DMA_WORD_SIZE_8;
            }
#endif /* if (SPI_DMA_CODE_EN) */
            break;
        }

        case ARM_SPI_DATA_BITS(16U):
        {
            /* Update the word size */
            ctrl1 |= SPI0_WORD_SIZE_16;

            /* Store the word size */
            spi->info->word_size = SPI_WORDSIZE_16;

#if (SPI_DMA_CODE_EN)

            /* Check if dma is being used */
            if (spi->info->default_cfg.dma_en == SPI_DMA_ENABLE)
            {
                /* Update the dma word size */
                dmaCfgR.src_word_size = DMA_WORD_SIZE_16;
                dmaCfgT.dst_word_size = DMA_WORD_SIZE_16;
            }
#endif /* if (SPI_DMA_CODE_EN) */
            break;
        }

        case ARM_SPI_DATA_BITS(24U):
        {
            /* Update the word size */
            ctrl1 |= SPI0_WORD_SIZE_24;

            /* Store the word size */
            spi->info->word_size = SPI_WORDSIZE_24;
            break;
        }

        case ARM_SPI_DATA_BITS(32U):
        {
            /* Update the word size */
            ctrl1 |= SPI0_WORD_SIZE_32;

            /* Store the word size */
            spi->info->word_size = SPI_WORDSIZE_32;

#if (SPI_DMA_CODE_EN)

            /* Check if dma is being used */
            if (spi->info->default_cfg.dma_en == SPI_DMA_ENABLE)
            {
                /* Update the dma word size */
                dmaCfgR.src_word_size = DMA_WORD_SIZE_32;
                dmaCfgT.dst_word_size = DMA_WORD_SIZE_32;
            }
#endif /* if (SPI_DMA_CODE_EN) */
            break;
        }

        default:
        {
            return ARM_SPI_ERROR_DATA_BITS;
        }
    }

    /* Bit order supports MSB to LSB only */
    if ((control & ARM_SPI_BIT_ORDER_Msk) != ARM_SPI_MSB_LSB)
    {
        /* Return bit order error */
        return ARM_SPI_ERROR_BIT_ORDER;
    }

    /* Set SPI Bus Speed */
    spi_prescale = SPI_CalculatePrescale(arg, SystemCoreClock);
    ctrl0 |= (spi_prescale << SPI0_CTRL0_SPI0_PRESCALE_Pos);

#if (SPI_DMA_CODE_EN)

    /* Check if dma is being used */
    if (spi->info->default_cfg.dma_en == SPI_DMA_ENABLE)
    {
        /* Configure the rx dma channel */
        dma->Configure(spi->info->default_cfg.rx_dma_ch, &dmaCfgR, spi->info->default_cfg.dma_rx_cb);

        /* Store the rx dma configuration */
        spi->info->dma_rx_cfg = dma->CreateConfigWord(&dmaCfgR);

        /* Configure the tx dma channel */
        dma->Configure(spi->info->default_cfg.tx_dma_ch, &dmaCfgT, spi->info->default_cfg.dma_tx_cb);

        /* Store the tx dma configuration */
        spi->info->dma_tx_cfg = dma->CreateConfigWord(&dmaCfgT);

        /* !!!WORKAROUND!!! for read only mode issues */
        dmaCfgT.src_step_mode = DMA_STEP_STATIC;

        /* Store the tx dma configuration for read only mode */
        spi->info->dma_ro_cfg = dma->CreateConfigWord(&dmaCfgT);

        /* Update the ctrl0 configuration */
        ctrl0 |= SPI0_OVERRUN_INT_ENABLE  | SPI0_UNDERRUN_INT_ENABLE;
    }
#endif /* if (SPI_DMA_CODE_EN) */

    /* Save mode */
    spi->info->mode = mode;

    /* Set SPI registers */
    Sys_SPI_Config((spi->reg == SPI1), ctrl0);
    Sys_SPI_TransferConfig((spi->reg == SPI1), ctrl1);

    spi->info->state |= SPI_CONFIGURED;

    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t SPI_Transfer(const void * data_out, void * data_in,
 *                                      uint32_t num, const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Non-blocking transfer function. If a callback was previously
 *                 registered using the Initialize function, the caller is
 *                 notified when 'num' bytes are transferred.
 * Inputs        : data_out              - pointer to tx buffer
 *                 data_in               - pointer to rx buffer
 *                 num                   - number of bytes to send
 *                 spi                   - Pointer to SPI resources
 * Outputs       : ARM_DRIVER_OK         - if it can successfully start the
 *                                         operation
 *                 ARM_DRIVER_ERROR_BUSY - if it's already busy handling
 *                                         a previous transfer
 * Assumptions   : The data buffers point to a memory area with 'num'
 *                 bytes of size
 * ------------------------------------------------------------------------- */
static int32_t SPI_Transfer(const void *data_out, void *data_in, uint32_t num,
                            const SPI_RESOURCES_t *spi)
{
    /* Check if params are correct */
    if (((data_out == NULL) && (data_in == NULL)) || (num == 0U))
    {
        /* Invalid parameters */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Check if driver is configured */
    if ((spi->info->state & SPI_CONFIGURED) == 0U)
    {
        /* SPI is not configured */
        return ARM_DRIVER_ERROR;
    }

    /* Check if driver is busy */
    if (spi->info->status.busy)
    {
        /* Previous send is not completed yet */
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Clear status */
    spi->reg->STATUS = SPI0_UNDERRUN_CLEAR | SPI0_OVERRUN_CLEAR |
                       SPI0_RECEIVE_CLEAR  | SPI0_TRANSMIT_CLEAR;

    /* Dummy read to clear registers */
    spi->reg->RX_DATA;
    spi->reg->TX_DATA;

    /* Prepare busy flag */
    uint8_t busy = SPI_BUSY;

#if (SPI_DMA_CODE_EN)

    /* Prepare transfer type*/
    uint32_t transferType = SPI0_RW_DATA;

    /* Check if transfer is write only */
    if (data_in == NULL)
    {
        /* Update the transfer type */
        transferType = SPI0_WRITE_DATA;

        /* Update busy flag */
        busy = SPI_TX_BUSY;
    }
#endif /* if (SPI_DMA_CODE_EN) */

    /* Updates statuses */
    spi->info->status.busy       = busy;
    spi->info->status.data_lost  = 0U;
    spi->info->status.mode_fault = 0U;

    /* Save transmit buffer info */
    spi->xfer->rx_buf = (uint8_t *)data_in;
    spi->xfer->tx_buf = (uint8_t *)data_out;
    spi->xfer->num    = num;
    spi->xfer->rx_cnt = 0U;
    spi->xfer->tx_cnt = 0U;

    /* Activate SSEL line, if HW controlled */
    if ((spi->info->mode & ARM_SPI_MODE_MASTER) &&
        (spi->info->mode & ARM_SPI_SS_MASTER_HW_OUTPUT))
    {
        spi->reg->CTRL1 &= ~SPI0_CS_1;
    }

#if (SPI_DMA_CODE_EN)

    /* Check if dma is being used */
    if (spi->info->default_cfg.dma_en == SPI_DMA_ENABLE)
    {
        /* !!!WORKAROUND!!! for dma issue when spi is in idle mode */
        spi->reg->CTRL1 |= transferType;

        /* Prepare dma word number */
        uint32_t wordNum;
        wordNum = (num / spi->info->word_size) + (num % spi->info->word_size > 0 ? 1 : 0);

        /* Check if rx transmission is enabled */
        if (data_in != NULL)
        {
            /* Reset dma busy flag */
            spi->info->dma_rx_busy = 0U;

            /* Prepare dma for transmission */
            dma->SetConfigWord(spi->info->default_cfg.rx_dma_ch, spi->info->dma_rx_cfg);

            /* Prepare dma rx buffer configuration */
            DMA_ADDR_CFG_t buffRCfg = {
                .src_addr     = &spi->reg->RX_DATA,
                .dst_addr     = spi->xfer->rx_buf,
                .counter_len  = wordNum,
                .transfer_len = wordNum
            };

            /* Configure the rx dma channel */
            dma->ConfigureAddr(spi->info->default_cfg.rx_dma_ch, &buffRCfg);

            /* Start the rx dma channel */
            dma->Start(spi->info->default_cfg.rx_dma_ch);
        }

        /* Prepare dma tx buffer configuration */
        DMA_ADDR_CFG_t buffTCfg = {
            .src_addr     = spi->xfer->tx_buf,
            .dst_addr     = &spi->reg->TX_DATA,
            .counter_len  = wordNum,
            .transfer_len = wordNum
        };

        /* Prepare dma for transmission */
        dma->SetConfigWord(spi->info->default_cfg.tx_dma_ch, spi->info->dma_tx_cfg);

        /* Check if tx transmission is enabled */
        if (data_out == NULL)
        {
            /* Reset dma busy flag */
            spi->info->dma_tx_busy = 0U;

            /* !!!WORKAROUND!!! for read only mode */
            /* Prepare dma for transmission */
            dma->SetConfigWord(spi->info->default_cfg.tx_dma_ch, spi->info->dma_ro_cfg);

            /* Dummy buffer for read only mode */
            static uint8_t dummyTxBuff[4] = { 0 };

            /* Prepare dma tx buffer configuration */
            buffTCfg.src_addr = dummyTxBuff;
        }

        /* Configure the tx dma channel */
        dma->ConfigureAddr(spi->info->default_cfg.tx_dma_ch, &buffTCfg);

        /* Start the tx dma channel */
        dma->Start(spi->info->default_cfg.tx_dma_ch);

        /* !!!WORKAROUND!!! set the RW transfer before setting the dma control to properly send first byte */
        spi->reg->CTRL0 &= ~SPI0_CTRL0_SPI0_CONTROLLER_Pos;
        spi->reg->CTRL1 |= (SPI0_START | transferType);
        spi->reg->CTRL0 |= SPI0_CONTROLLER_DMA;
    }
    else
    {
        /* Fill SPI TX_DATA buffer for first transmission.
         * Remaining transmissions will occur via the SPI Interrupts. */
        spi->reg->TX_DATA = SPI_PrepareNextTXData(spi);

        /* Configure SPI to RW operation and start transfer (master) */
        spi->reg->CTRL1 |= (SPI0_START | SPI0_RW_DATA);
    }

#else  /* if (SPI_DMA_CODE_EN) */
    /* Fill SPI TX_DATA buffer for first transmission.
     * Remaining transmissions will occur via the SPI Interrupts. */
    spi->reg->TX_DATA = SPI_PrepareNextTXData(spi);

    /* Configure SPI to RW operation and start transfer (master) */
    spi->reg->CTRL1 |= (SPI0_START | SPI0_RW_DATA);
#endif /* if (SPI_DMA_CODE_EN) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI_GetDataCount(const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Get SPIx data count. This count is reset for every receive
 *                 operation.
 * Inputs        : spi    - Pointer to SPI resources
 * Outputs       : Number of bytes received by SPIx
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t SPI_GetDataCount(const SPI_RESOURCES_t *spi)
{
#if (SPI_DMA_CODE_EN)

    /* Check if DMA is being used */
    if (spi->info->default_cfg.dma_en)
    {
        /* Check if new transmission is ongoing */
        if (spi->info->dma_rx_busy)
        {
            /* Return counter value */
            return dma->GetCounterValue(spi->info->default_cfg.rx_dma_ch) *spi->info->word_size;
        }

        /* Return 0 */
        return 0;
    }
    else
#endif /* if (SPI_DMA_CODE_EN) */
    {
        /* Return rx counter */
        return spi->xfer->rx_cnt;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI_GetStatus(const SPI_RESOURCES_t *spi)
 * ----------------------------------------------------------------------------
 * Description   : Get SPIx status
 * Inputs        : spi    - Pointer to SPI resources
 * Outputs       : Return SPIx status as an ARM_SPI_STATUS structure
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_SPI_STATUS SPI_GetStatus(const SPI_RESOURCES_t *spi)
{
    ARM_SPI_STATUS status;

    /* Prepare spi status */
    status.busy       = spi->info->status.busy?1U:0U;
    status.data_lost  = spi->info->status.data_lost;
    status.mode_fault = 0U;

    /* Return spi status */
    return status;
}

/* SPI0 Driver Wrapper functions. See functions above for usage */
#if RTE_SPI0
static int32_t SPI0_Initialize(ARM_SPI_SignalEvent_t cb_event)
{
    /* Initialize spi0 */
    int32_t status = SPI_Initialize(cb_event, &SPI0_Resources);

    /* Set spi0 interrupt priorities */
    status |= SPI_SetInterruptPriority(&SPI0_Resources);

#if (RTE_SPI0_CFG_EN_DEFAULT)

    /* Set default power mode */
    status |= SPI_PowerControl(ARM_POWER_FULL, &SPI0_Resources);

    /* Set default spi0 configuration (inactive mode) */
    status |= SPI_Control(SPI0_DEFAULT_CONTROL_CONFIG, RTE_SPI0_SPEED_DEFAULT, &SPI0_Resources);
#endif /* if (RTE_SPI0_CFG_EN_DEFAULT) */

    /* Return initialization status */
    return status;
}

static int32_t SPI0_Uninitialize(void)
{
    return SPI_Uninitialize(&SPI0_Resources);
}

static int32_t SPI0_PowerControl(ARM_POWER_STATE state)
{
    return SPI_PowerControl(state, &SPI0_Resources);
}

static int32_t SPI0_Send(const void *data, uint32_t num)
{
    return SPI_Transfer(data, NULL, num, &SPI0_Resources);
}

static int32_t SPI0_Receive(void *data, uint32_t num)
{
    return SPI_Transfer(NULL, data, num, &SPI0_Resources);
}

static int32_t SPI0_Transfer(const void *data_out, void *data_in, uint32_t num)
{
    return SPI_Transfer(data_out, data_in, num, &SPI0_Resources);
}

static uint32_t SPI0_GetDataCount(void)
{
    return SPI_GetDataCount(&SPI0_Resources);
}

static int32_t SPI0_Control(uint32_t control, uint32_t arg)
{
    return SPI_Control(control, arg, &SPI0_Resources);
}

static ARM_SPI_STATUS SPI0_GetStatus(void)
{
    return SPI_GetStatus(&SPI0_Resources);
}

#if (!RTE_SPI0_DMA_EN_DEFAULT)
void SPI0_TX_IRQHandler(void)
{
    SPI_TX_IRQHandler(&SPI0_Resources);
}

void SPI0_RX_IRQHandler(void)
{
    SPI_RX_IRQHandler(&SPI0_Resources);
}

#endif /* if (!RTE_SPI0_DMA_EN_DEFAULT) */

#if (RTE_SPI0_DMA_EN_DEFAULT)
void SPI0_TX_DMAHandler(uint32_t event)
{
    SPI_TX_DMAHandler(event, &SPI0_Resources);
}

void SPI0_RX_DMAHandler(uint32_t event)
{
    SPI_RX_DMAHandler(event, &SPI0_Resources);
}

void SPI0_ERROR_IRQHandler(void)
{
    SPI_ERR_IRQHandler(&SPI0_Resources);
}

#endif /* if (RTE_SPI0_DMA_EN_DEFAULT) */

/* SPI0 Driver Control Block */
ARM_DRIVER_SPI Driver_SPI0 =
{
    SPIx_GetVersion,
    SPIx_GetCapabilities,
    SPI0_Initialize,
    SPI0_Uninitialize,
    SPI0_PowerControl,
    SPI0_Send,
    SPI0_Receive,
    SPI0_Transfer,
    SPI0_GetDataCount,
    SPI0_Control,
    SPI0_GetStatus
};
#endif    /* if RTE_SPI0 */

/* SPI1 Driver Wrapper functions. See functions above for usage */
#if RTE_SPI1
static int32_t SPI1_Initialize(ARM_SPI_SignalEvent_t cb_event)
{
    /* Initialize spi1 */
    int32_t status = SPI_Initialize(cb_event, &SPI1_Resources);

    /* Set spi1 interrupt priorities */
    status |= SPI_SetInterruptPriority(&SPI1_Resources);

#if (RTE_SPI1_CFG_EN_DEFAULT)

    /* Set default power mode */
    status |= SPI_PowerControl(ARM_POWER_FULL, &SPI1_Resources);

    /* Set default spi1 configuration (inactive mode) */
    status |= SPI_Control(SPI1_DEFAULT_CONTROL_CONFIG, RTE_SPI1_SPEED_DEFAULT, &SPI1_Resources);
#endif /* if (RTE_SPI1_CFG_EN_DEFAULT) */

    /* Return initialization status */
    return status;
}

static int32_t SPI1_Uninitialize(void)
{
    return SPI_Uninitialize(&SPI1_Resources);
}

static int32_t SPI1_PowerControl(ARM_POWER_STATE state)
{
    return SPI_PowerControl(state, &SPI1_Resources);
}

static int32_t SPI1_Send(const void *data, uint32_t num)
{
    return SPI_Transfer(data, NULL, num, &SPI1_Resources);
}

static int32_t SPI1_Receive(void *data, uint32_t num)
{
    return SPI_Transfer(NULL, data, num, &SPI1_Resources);
}

static int32_t SPI1_Transfer(const void *data_out, void *data_in, uint32_t num)
{
    return SPI_Transfer(data_out, data_in, num, &SPI1_Resources);
}

static uint32_t SPI1_GetDataCount(void)
{
    return SPI_GetDataCount(&SPI1_Resources);
}

static int32_t SPI1_Control(uint32_t control, uint32_t arg)
{
    return SPI_Control(control, arg, &SPI1_Resources);
}

static ARM_SPI_STATUS SPI1_GetStatus(void)
{
    return SPI_GetStatus(&SPI1_Resources);
}

#if (!RTE_SPI1_DMA_EN_DEFAULT)
void SPI1_TX_IRQHandler(void)
{
    SPI_TX_IRQHandler(&SPI1_Resources);
}

void SPI1_RX_IRQHandler(void)
{
    SPI_RX_IRQHandler(&SPI1_Resources);
}

#endif /* if (!RTE_SPI1_DMA_EN_DEFAULT) */

#if (RTE_SPI1_DMA_EN_DEFAULT)
void SPI1_TX_DMAHandler(uint32_t event)
{
    SPI_TX_DMAHandler(event, &SPI1_Resources);
}

void SPI1_RX_DMAHandler(uint32_t event)
{
    SPI_RX_DMAHandler(event, &SPI1_Resources);
}

void SPI1_ERROR_IRQHandler(void)
{
    SPI_ERR_IRQHandler(&SPI1_Resources);
}

#endif /* if (RTE_SPI1_DMA_EN_DEFAULT) */

/* SPI1 Driver Control Block */
ARM_DRIVER_SPI Driver_SPI1 =
{
    SPIx_GetVersion,
    SPIx_GetCapabilities,
    SPI1_Initialize,
    SPI1_Uninitialize,
    SPI1_PowerControl,
    SPI1_Send,
    SPI1_Receive,
    SPI1_Transfer,
    SPI1_GetDataCount,
    SPI1_Control,
    SPI1_GetStatus
};
#endif    /* if RTE_SPI1 */
