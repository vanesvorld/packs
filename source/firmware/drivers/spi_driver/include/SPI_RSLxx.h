/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * SPI_RSLxx.h
 * - CMSIS-Driver header file for RSLXX SPI
 * ------------------------------------------------------------------------- */

#ifndef SPI_RSLXX_H
#define SPI_RSLXX_H

#include <rsl10.h>
#include <Driver_SPI.h>
#include <RTE_Device.h>
#include <Driver_DMA.h>
#include <Driver_GPIO.h>

#if ((!RTE_SPI0) && (!RTE_SPI1))
  #error "SPI not configured in RTE_Device.h!"
#endif    /* if ((!RTE_SPI0) && (!RTE_SPI1)) */

/* DMA / CM3 code switches */
#define SPI_DMA_CODE_EN (RTE_SPI0_DMA_EN_DEFAULT || RTE_SPI1_DMA_EN_DEFAULT)
#define SPI_CM3_CODE_EN ((RTE_SPI0 && !RTE_SPI0_DMA_EN_DEFAULT) || (RTE_SPI1 && !RTE_SPI1_DMA_EN_DEFAULT))

/* SPI interrupt handlers prototypes */
#if(SPI_DMA_CODE_EN)
void SPI_DMA_Handler(uint32_t event);
#endif
#if(RTE_SPI0)
#if(RTE_SPI0_DMA_EN_DEFAULT)
void SPI0_RX_DMAHandler(uint32_t event);
void SPI0_TX_DMAHandler(uint32_t event);
void SPI0_ERROR_IRQHandler(void);
#else
void SPI0_RX_IRQHandler(void);
void SPI0_TX_IRQHandler(void);
#endif
#endif
#if(RTE_SPI1)
#if(RTE_SPI1_DMA_EN_DEFAULT)
void SPI1_RX_DMAHandler(uint32_t event);
void SPI1_TX_DMAHandler(uint32_t event);
void SPI1_ERROR_IRQHandler(void);
#else
void SPI1_RX_IRQHandler(void);
void SPI1_TX_IRQHandler(void);
#endif
#endif

/* Check if correct word size is used */
#if(RTE_SPI0_DMA_EN_DEFAULT)
#if(RTE_SPI0_WS_DEFAULT == 24)
#error "DMA does not support 24 bit long words"
#endif
#endif
#if(RTE_SPI1_DMA_EN_DEFAULT)
#if(RTE_SPI1_WS_DEFAULT == 24)
#error "DMA does not support 24 bit long words"
#endif
#endif

/* Extern GPIO driver */
extern DRIVER_GPIO_t Driver_GPIO;

/* Extern DMA driver */
extern DRIVER_DMA_t Driver_DMA;

/* Default driver configuration */
#if(!RTE_SPI0_MODE_DEFAULT)
#define SPI0_DEFAULT_CONTROL_CONFIG (ARM_SPI_MODE_INACTIVE                   | \
                                     ARM_SPI_DATA_BITS(RTE_SPI0_WS_DEFAULT)  | \
                                     ARM_SPI_SS_MASTER_UNUSED                | \
                                     RTE_SPI0_POL_DEFAULT << ARM_SPI_FRAME_FORMAT_Pos)

#elif((RTE_SPI0_MODE_DEFAULT & 0xF0) == 0x10)
#define SPI0_DEFAULT_CONTROL_CONFIG (ARM_SPI_MODE_MASTER                                       | \
                                     ARM_SPI_DATA_BITS(RTE_SPI0_WS_DEFAULT)                    | \
                                   ((RTE_SPI0_MODE_DEFAULT & 3) << ARM_SPI_SS_MASTER_MODE_Pos) | \
                                     RTE_SPI0_POL_DEFAULT << ARM_SPI_FRAME_FORMAT_Pos)

#else
#define SPI0_DEFAULT_CONTROL_CONFIG (ARM_SPI_MODE_SLAVE                                       | \
                                     ARM_SPI_DATA_BITS(RTE_SPI0_WS_DEFAULT)                   | \
                                   ((RTE_SPI0_MODE_DEFAULT & 1) << ARM_SPI_SS_SLAVE_MODE_Pos) | \
                                     RTE_SPI0_POL_DEFAULT << ARM_SPI_FRAME_FORMAT_Pos)
#endif
#if(!RTE_SPI1_MODE_DEFAULT)
#define SPI1_DEFAULT_CONTROL_CONFIG (ARM_SPI_MODE_INACTIVE                   | \
                                     ARM_SPI_DATA_BITS(RTE_SPI1_WS_DEFAULT)  | \
                                     ARM_SPI_SS_MASTER_UNUSED                | \
                                     RTE_SPI1_POL_DEFAULT << ARM_SPI_FRAME_FORMAT_Pos)

#elif((RTE_SPI1_MODE_DEFAULT & 0xF0) == 0x10)
#define SPI1_DEFAULT_CONTROL_CONFIG (ARM_SPI_MODE_MASTER                                       | \
                                     ARM_SPI_DATA_BITS(RTE_SPI1_WS_DEFAULT)                    | \
                                   ((RTE_SPI1_MODE_DEFAULT & 3) << ARM_SPI_SS_MASTER_MODE_Pos) | \
                                     RTE_SPI1_POL_DEFAULT << ARM_SPI_FRAME_FORMAT_Pos)

#else
#define SPI1_DEFAULT_CONTROL_CONFIG (ARM_SPI_MODE_SLAVE                                       | \
                                     ARM_SPI_DATA_BITS(RTE_SPI1_WS_DEFAULT)                   | \
                                   ((RTE_SPI1_MODE_DEFAULT & 1) << ARM_SPI_SS_SLAVE_MODE_Pos) | \
                                     RTE_SPI1_POL_DEFAULT << ARM_SPI_FRAME_FORMAT_Pos)
#endif

/* Constants */
#define SPI_MAX_PRESCALE          9U
#define SPI_BYTE_SIZE             8
#define SPI1_OFFSET               3
#define SPI_CLOCK_LIMIT_MUL       4

/* SPI Word sizes */
#define SPI_WORDSIZE_8            1
#define SPI_WORDSIZE_16           2
#define SPI_WORDSIZE_24           3
#define SPI_WORDSIZE_32           4

/* SPI module state defines */
#define SPI_BUSY                  3U
#define SPI_RX_BUSY               1U
#define SPI_TX_BUSY               2U
#define SPI_FREE                  0U

/* Driver status flag definition */
#define SPI_INITIALIZED           ((uint8_t)(1U))
#define SPI_POWERED               ((uint8_t)(1U << 1))
#define SPI_CONFIGURED            ((uint8_t)(1U << 2))
#define SPI_DATA_LOST             ((uint8_t)(1U << 3))

/* SPI module number */
typedef enum
{
    SPI_0            = 0,                       /* dma module 0 */
    SPI_1            = 1                        /* dma module 1 */
} SPI_NUM_t;

/* SPI dma enable */
typedef enum
{
    SPI_DMA_DISABLE  = 0,                       /* dma disabled */
    SPI_DMA_ENABLE   = 1                        /* dma enabled */
} SPI_DMA_EN_t;

/* SPI interrupt priority configuration */
typedef struct _SPI_PRI_CFG_t
{
    uint32_t                 preempt_pri   :3;  /* preempt priority */
    uint32_t                               :13; /* reserved */
    uint32_t                 subgrp_pri    :3;  /* subgroup priority */
    uint32_t                               :13; /* reserved */
} SPI_PRI_CFG_t;

/* SPI0 default configuration */
typedef struct _SPI_DEFAULT_CFG_t
{
    GPIO_SEL_t               sclk_pin      :4;  /* spi sclk pin identifier */
    GPIO_SEL_t               ssel_pin      :4;  /* spi ssel pin identifier */
    GPIO_SEL_t               miso_pin      :4;  /* spi miso pin identifier */
    GPIO_SEL_t               mosi_pin      :4;  /* spi mosi pin identifier */
#if(SPI_DMA_CODE_EN)
    DMA_SignalEvent_t        dma_rx_cb;         /* dma rx callback funcion */
    DMA_SignalEvent_t        dma_tx_cb;         /* dma tx callback funcion */
    DMA_SEL_t                rx_dma_ch     :3;  /* spi rx transmission dma channel */
    DMA_SEL_t                tx_dma_ch     :3;  /* spi tx transmission dma channel */
    SPI_DMA_EN_t             dma_en        :1;  /* spi dma enable */
    uint8_t                  dma_rx_busy   :1;  /* !!!WORKAROUND!!! for dma rx counter not being reset before new transmission */
    uint8_t                  dma_tx_busy   :1;  /* !!!WORKAROUND!!! for dma tx counter not being reset before new transmission */
    uint8_t                                :5;  /* reserved */
    DMA_TRG_t                dma_trg;           /* dma target selection */
#endif
    SPI_PRI_CFG_t            pri_cfg;           /* spi interrupt priority default configuration */
} SPI_DEFAULT_CFG_t;

/* SPI Transfer Information (Run-Time) */
typedef struct _SPI_TRANSFER_INFO_t
{
    uint32_t                 num;               /* Total number of data to transfer */
    uint8_t                 *rx_buf;            /* Pointer to in data buffer */
    uint8_t                 *tx_buf;            /* Pointer to out data buffer */
    uint32_t                 rx_cnt;            /* Number of data received */
    uint32_t                 tx_cnt;            /* Number of data sent */
    uint32_t                 def_val;           /* Default transfer value */
} SPI_TRANSFER_INFO_t;

/* SPI Status */
typedef struct _SPI_MODEM_STATUS_t
{
    uint8_t                  busy;              /* Transmitter/Receiver busy flag */
    uint8_t                  data_lost;         /* Data lost: Receive overflow/Transmit underflow */
    uint8_t                  mode_fault;        /* Not supported */
} SPI_MODEM_STATUS_t;

/* SPI Information (Run-time) */
typedef struct _SPI_INFO_t
{
    const SPI_DEFAULT_CFG_t  default_cfg;       /* spi default configuration */
    SPI_MODEM_STATUS_t       status;            /* status flags */
    uint8_t                  word_size;         /* current word size */
    uint8_t                  state;             /* current SPI state */
    uint32_t                 mode;              /* current SPI mode */
#if(SPI_DMA_CODE_EN)
    uint32_t                 dma_rx_cfg;        /* dma channel receiver configuration */
    uint32_t                 dma_tx_cfg;        /* dma channel transmitter configuration */
    uint32_t                 dma_ro_cfg;        /* dma channel transmitter for read mode only configuration */
    uint8_t                  dma_rx_busy  :1;   /* !!!WORKAROUND!!! for dma rx counter not being reset before new transmission */
    uint8_t                  dma_tx_busy  :1;   /* !!!WORKAROUND!!! for dma tx counter not being reset before new transmission */
    uint8_t                               :6;   /* reserved */
#endif
} SPI_INFO_t;

/* SPI interrupts info */
typedef struct _SPI_INT_INFO_t
{
#if(SPI_CM3_CODE_EN)
    const IRQn_Type          rx_irqn;           /* spi rx IRQ Number */
    const IRQn_Type          tx_irqn;           /* spi tx IRQ Number */
#endif
#if(SPI_DMA_CODE_EN)
    const IRQn_Type          err_irqn;          /* spi errors IRQ Number */
#endif
    ARM_SPI_SignalEvent_t    cb;                /* spi event callback */
} SPI_INT_INFO_t;

/* SPI resources definition */
typedef const struct
{
    SPI1_Type               *reg;               /* spi peripheral pointer */
    SPI_INT_INFO_t          *intInfo;           /* IRQs Info */
    SPI_INFO_t              *info;              /* runtime Information */
    SPI_TRANSFER_INFO_t     *xfer;              /* spi transfer information */
} SPI_RESOURCES_t;

#endif    /* SPI_RSLXX_H */
