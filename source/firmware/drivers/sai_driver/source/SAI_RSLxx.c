/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * SAI_RSLxx.c
 * - SAI cmsis-driver implementation for RSLxx family of devices
 * ------------------------------------------------------------------------- */
#include <GPIO_RSLxx.h>
#include <DMA_RSLxx.h>
#include <SAI_RSLxx.h>

#define ARM_SAI_DRV_VERSION    ARM_DRIVER_VERSION_MAJOR_MINOR(0, 1)

/* Driver Version */
static const ARM_DRIVER_VERSION DriverVersion =
{
    ARM_SAI_API_VERSION,
    ARM_SAI_DRV_VERSION
};

/* Driver Capabilities */
static const ARM_SAI_CAPABILITIES DriverCapabilities =
{
    0,         /* supports asynchronous Transmit/Receive */
    0,         /* supports synchronous Transmit/Receive */
    0,         /* supports user defined Protocol */
    1,         /* supports I2S Protocol */
    0,         /* supports MSB/LSB justified Protocol */
    1,         /* supports PCM short/long frame Protocol */
    0,         /* supports AC'97 Protocol */
    0,         /* supports Mono mode */
    0,         /* supports Companding */
    0,         /* supports MCLK (Master Clock) pin */
    0,         /* supports Frame error event: ARM_SAI_EVENT_FRAME_ERROR */
    0,         /* Reserved (must be zero)} */
};

#if RTE_SAI_PCM

/* SAI Run-Time Information */
static SAI_INFO_t SAI_Info = {
    .default_cfg = {                                               /* sai default configuration */
        .mclk_pin            = RTE_SAI_PCM_MCLK_PIN_DEFAULT,       /* sai default mclk pin */
        .frame_pin           = RTE_SAI_PCM_FRAME_PIN_DEFAULT,      /* sai default frame pin */
        .seri_pin            = RTE_SAI_PCM_SERI_PIN_DEFAULT,       /* sai default seri pin */
        .sero_pin            = RTE_SAI_PCM_SERO_PIN_DEFAULT,       /* sai default sero pin */
#if (RTE_SAI_DMA_EN_DEFAULT)
        .dma_rx_cb           = SAI0_RX_DMAHandler,                 /* sai default rx callback function */
        .dma_tx_cb           = SAI0_TX_DMAHandler,                 /* sai default tx callback function */
        .rx_dma_ch           = RTE_SAI_RX_DMA_CH_DEFAULT,          /* sai default rx dma channel */
        .tx_dma_ch           = RTE_SAI_TX_DMA_CH_DEFAULT,          /* sai default tx dma channel */
#endif /* if (RTE_SAI_DMA_EN_DEFAULT) */
        .pri_cfg = {                                               /* sai default priority configuration */
            .preempt_pri     = RTE_SAI_INT_PREEMPT_PRI_DEFAULT,    /* sai default pre-empt priority value */
            .subgrp_pri      = RTE_SAI_INT_SUBGRP_PRI_DEFAULT      /* sai default subgroup priority value */
        }                                                          
    },
    .status                  = { 0 },                              /* sai current status */
    .flags                   = 0,                                  /* sai current state */
    .mode                    = 0,                                  /* sai current mode */
    .word_size               = 0,                                  /* sai current word size */
#if (RTE_SAI_DMA_EN_DEFAULT)
    .dma_rx_cfg              = 0,                                  /* dma channel receiver configuration */
    .dma_tx_cfg              = 0,                                  /* dma channel transmitter configuration */
    .dma_rx_busy             = 0,                                  /* !!!WORKAROUND!!! for dma rx counter not being
                                                                    * reset before new transmission */
    .dma_tx_busy             = 0,                                  /* !!!WORKAROUND!!! for dma tx counter not being
                                                                    * reset before new transmission */
#endif /* if (RTE_SAI_DMA_EN_DEFAULT) */
};

/* SAI interrupt information */
static SAI_INT_INFO_t SAI_intInfo = {                            /* sai interrupt info */
#if (!RTE_SAI_DMA_EN_DEFAULT)
    .rx_irqn                 = PCM_RX_IRQn,                        /* sai rx interrupt number */
    .tx_irqn                 = PCM_TX_IRQn,                        /* sai tx interrupt number */
#endif /* if (!RTE_SAI_DMA_EN_DEFAULT) */
#if (RTE_SAI_DMA_EN_DEFAULT)
    .err_irqn                = PCM_ERROR_IRQn,                     /* sai error interrupt number */
#endif /* if (RTE_SAI_DMA_EN_DEFAULT) */
    .cb                      = 0,                                  /* sai callback function pointer */
};

/* SAI transfer information */
static SAI_TRANSFER_INFO_t SAI_TransferInfo = { 0 };               /* sai transfer info */

/* SAI Resources */
static const SAI_RESOURCES_t SAI_Resources =
{
    .reg                     = PCM,                                /* sai hw registers */
    .intInfo                 = &SAI_intInfo,                       /* sai interrupt info */
    .info                    = &SAI_Info,                          /* sai runtime info */
    .xfer                    = &SAI_TransferInfo                   /* sai transfer info*/
};

#endif /* if RTE_SAI_PCM */

/* Prepare gpio driver pointer */
static DRIVER_GPIO_t *gpio = &Driver_GPIO;

#if (SAI_DMA_CODE_EN)

/* Prepare dma driver pointer */
static DRIVER_DMA_t *dma = &Driver_DMA;
#endif /* if (SAI_DMA_CODE_EN) */

/* ----------------------------------------------------------------------------
 * Function      : uint32_t SAIx_PrepareWord(SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Function to prepare word to be sent
 * Inputs        : buf       - buffer pointer
 * Outputs       : Prepared data
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t SAIx_PrepareWord(SAI_RESOURCES_t *sai)
{
    uint32_t word = 0;

    /* Prepare data word to be sent */
    for (int i = 0; i < sai->info->word_size; i++)
    {
        word |= sai->xfer->tx_buf[i] << (SAI_BYTE_SHIFT * i);
    }

    /* Update the tx buffer pointer */
    sai->xfer->tx_buf += sai->info->word_size;

    /* Return data word */
    return word;
}

#if (SAI_DMA_CODE_EN)
/* ----------------------------------------------------------------------------
 * Function      : void SAI_TX_DMAHandler(uint32_t event, SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Called by dma driver when a dma transfer is done.
 *                 Manage flags and counters. Application is notified via
 *                 callback function once the transmission is complete
 * Inputs        : spi    - spi instance
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SAI_TX_DMAHandler(uint32_t event, SAI_RESOURCES_t *sai)
{
    /* Prepare status description */
    event = 0;

    if (sai->info->status.tx_busy)
    {
        /* Stop the dma */
        DMA_STATUS_t status = dma->GetStatus(sai->info->default_cfg.tx_dma_ch);

        /* Check if dma transfer has finished */
        if (status.completed)
        {
            /* Stop the dma */
            dma->Stop(sai->info->default_cfg.tx_dma_ch);

            /* Clear tx busy flag */
            sai->info->status.tx_busy = 0;

            /* Indicate transfer complete */
            event |= ARM_SAI_EVENT_SEND_COMPLETE;

            /* Disable PCM */
            Sys_PCM_Disable();

            /* Notify application */
            if (sai->intInfo->cb)
            {
                /* Call the callback function */
                sai->intInfo->cb(event);
            }
        }

        /* Check if first word was transmitted */
        if (status.started)
        {
            /* Indicate that first byte was transmitted */
            sai->info->dma_tx_busy = 1;
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_RX_DMAHandler(uint32_t event, SAI_RESOURCES_t * spi)
 * ----------------------------------------------------------------------------
 * Description   : Called by dma driver when a dma transfer is done.
 *                 Manage flags and counters. Application is notified via
 *                 callback function once the transmission is complete
 * Inputs        : spi    - spi instance
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SAI_RX_DMAHandler(uint32_t event, SAI_RESOURCES_t *sai)
{
    /* Prepare status description */
    event = 0;

    if (sai->info->status.rx_busy)
    {
        /* Stop the dma */
        DMA_STATUS_t status = dma->GetStatus(sai->info->default_cfg.rx_dma_ch);

        /* Check if dma transfer has finished */
        if (status.completed)
        {
            /* Stop the dma */
            dma->Stop(sai->info->default_cfg.rx_dma_ch);

            /* Clear rx busy flag */
            sai->info->status.rx_busy = 0;

            /* Indicate transfer complete */
            event |= ARM_SAI_EVENT_RECEIVE_COMPLETE;

            /* Disable PCM */
            Sys_PCM_Disable();

            /* Notify application */
            if (sai->intInfo->cb)
            {
                sai->intInfo->cb(event);
            }
        }

        /* Check if first word was received */
        if (status.started)
        {
            /* Indicate that first byte was received */
            sai->info->dma_rx_busy = 1;
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_ERROR_IRQHandler(SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when DMA is selected as a controller
 *                    and a overrun/underrun error occur. Application is notified
 *                    via callback function about the transfer status.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SAI_ERROR_IRQHandler(SAI_RESOURCES_t *sai)
{
    uint32_t event = 0U;

    /* Check if receive transmission is ongoing  */
    if (sai->info->status.rx_busy)
    {
        /* Check if overrun occured */
        if (sai->reg->STATUS & PCM_OVERRUN_TRUE)
        {
            /* Indicate that overflow occured */
            sai->info->status.rx_overflow = 1U;
            event |= ARM_SAI_EVENT_RX_OVERFLOW;
        }
    }

    /* Check if send transmission is ongoing  */
    if (sai->info->status.tx_busy)
    {
        /* Check if underrun occured */
        if (sai->reg->STATUS & PCM_UNDERRUN_TRUE)
        {
            /* Indicate that underrun occured */
            sai->info->status.tx_underflow = 1U;
            event |= ARM_SAI_EVENT_TX_UNDERFLOW;
        }

        /* Clear error status */
        sai->reg->STATUS |= PCM_OVERRUN_CLEAR | PCM_UNDERRUN_CLEAR;
    }

    /* If an event happened and the application registered a callback */
    if (event && sai->intInfo->cb)
    {
        /* Call the callback function */
        sai->intInfo->cb(event);
    }

    /* Clear pending sai err interrupt in nvic */
    NVIC_ClearPendingIRQ(sai->intInfo->err_irqn);
}

#endif /* if (SAI_DMA_CODE_EN) */

#if (SAI_CM3_CODE_EN)
/* ----------------------------------------------------------------------------
 * Function      : uint32_t SAIx_PrepareWord(uint32_t word, SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Translate data word to receive buffer
 * Inputs        : word      - received data word
 *                    buf       - buffer pointer
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SAIx_GetData(uint32_t word, SAI_RESOURCES_t *sai)
{
    /* Calculate word size */
    uint32_t ws = ((sai->xfer->rx_num - sai->xfer->rx_cnt) > sai->info->word_size) ?
                  sai->info->word_size                     :
                  (sai->xfer->rx_num - sai->xfer->rx_cnt);

    /* Split received data to uint8_t * buffer */
    for (int i = 0; i < ws; i++)
    {
        sai->xfer->rx_buf[sai->xfer->rx_cnt++] = (word >> (SAI_BYTE_SHIFT * i)) & SAI_BYTE_MASK;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_TX_IRQHandler(SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a byte is transmitted. Place
 *                 transmit data into the PCM, sequentially, byte per byte.
 *                 Manage flags and counters. Application is notified via
 *                 callback function once the transmission is complete
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SAI_TX_IRQHandler(SAI_RESOURCES_t *sai)
{
    if (sai->info->status.tx_busy)
    {
        /* Increment tx count */
        sai->xfer->tx_cnt += sai->info->word_size;

        /* Check if transfer is complete */
        if (sai->xfer->tx_cnt >= sai->xfer->tx_num)
        {
            /* Clear send active flag */
            sai->info->status.tx_busy = 0U;

            /* Disable sai tx interrupt */
            NVIC_DisableIRQ(sai->intInfo->tx_irqn);

            /* Disable pcm */
            Sys_PCM_Disable();

            /* Prepare event word */
            uint32_t event = ARM_SAI_EVENT_SEND_COMPLETE;

            /* Check if underrun occured */
            if (sai->reg->STATUS & PCM_UNDERRUN_TRUE)
            {
                /* Indicate that underrun occured */
                sai->info->status.tx_underflow = 1U;
                event |= ARM_SAI_EVENT_TX_UNDERFLOW;
            }

            /* Notify application */
            if (sai->intInfo->cb)
            {
                /* Call callback function */
                sai->intInfo->cb(event);
            }
        }
        else
        {
            /* Transfer next byte */
            sai->reg->TX_DATA = SAIx_PrepareWord(sai);
        }

        /* Clear pending sai tx interrupt */
        NVIC_ClearPendingIRQ(sai->intInfo->tx_irqn);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_RX_IRQHandler(SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a byte is received. Store
 *                 received data in the buffer, manage flags and counters.
 *                 Application is notified via callback function once the
 *                 receive is complete
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void SAI_RX_IRQHandler(SAI_RESOURCES_t *sai)
{
    /* Check if device is busy and receive counter value */
    if (sai->info->status.rx_busy && (sai->xfer->rx_cnt < sai->xfer->rx_num))
    {
        /* Receive byte */
        SAIx_GetData(sai->reg->RX_DATA, sai);

        /* Check if receive is complete */
        if (sai->xfer->rx_cnt >= sai->xfer->rx_num)
        {
            /* Clear rx busy flag */
            sai->info->status.rx_busy = 0U;

            /* Disable sai rx interrupt */
            NVIC_DisableIRQ(sai->intInfo->rx_irqn);

            /* Disable pcm */
            Sys_PCM_Disable();

            /* Prepare event word */
            uint32_t event = ARM_SAI_EVENT_RECEIVE_COMPLETE;

            /* Check if overrun occured */
            if (sai->reg->STATUS & PCM_OVERRUN_TRUE)
            {
                /* Indicate that overflow occured */
                sai->info->status.rx_overflow = 1U;
                event |= ARM_SAI_EVENT_RX_OVERFLOW;
            }

            /* Notify application */
            if (sai->intInfo->cb)
            {
                /* Call callback function */
                sai->intInfo->cb(event);
            }
        }

        /* Clear pending sai rx interrupt */
        NVIC_ClearPendingIRQ(sai->intInfo->rx_irqn);
    }
}

#endif /* if (SAI_CM3_CODE_EN) */

/* ----------------------------------------------------------------------------
 * Function      : void SAIx_GetVersion(void)
 * ----------------------------------------------------------------------------
 * Description   : Get driver version
 * Inputs        : None
 * Outputs       : ARM_DRIVER_VERSION
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_DRIVER_VERSION SAIx_GetVersion(void)
{
    /* Return driver version */
    return DriverVersion;
}

/* ----------------------------------------------------------------------------
 * Function      : void SAIx_GetCapabilities(void)
 * ----------------------------------------------------------------------------
 * Description   : Get SAI driver capabilities
 * Inputs        : None
 * Outputs       : ARM_PCM_CAPABILITIES
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_SAI_CAPABILITIES SAIx_GetCapabilities(void)
{
    /* Return driver capabilities */
    return DriverCapabilities;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t SAI_SetInterruptPriority(SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Configure sai interrupts priority.
 * Inputs        : sai - sai instance
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SAI_SetInterruptPriority(SAI_RESOURCES_t *sai)
{
    /* Encoded priority value */
    uint32_t encodedPri = 0;

    /* Encode priority configuration word */
    encodedPri = NVIC_EncodePriority(NVIC_GetPriorityGrouping(), sai->info->default_cfg.pri_cfg.preempt_pri,
                                     sai->info->default_cfg.pri_cfg.subgrp_pri);
#if (SAI_DMA_CODE_EN)

    /* Set the priority of sai err interrupt */
    NVIC_SetPriority(sai->intInfo->err_irqn, encodedPri);
#else  /* if (SAI_DMA_CODE_EN) */

    /* Set the priority of sai rx interrupt */
    NVIC_SetPriority(sai->intInfo->rx_irqn, encodedPri);

    /* Set the priority of sai tx interrupt */
    NVIC_SetPriority(sai->intInfo->tx_irqn, encodedPri);
#endif /* if (SAI_DMA_CODE_EN) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_Initialize(ARM_SAI_SignalEvent_t cb, SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Initialize PCM flags and reference to callback function.
 * Inputs        : cb_event - pointer to callback function (optional)
 *                 sai      - PCM instance
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SAI_Initialize(ARM_SAI_SignalEvent_t cb, SAI_RESOURCES_t *sai)
{
    /* Check if driver was already initialized */
    if (sai->info->flags & SAI_FLAG_INITIALIZED)
    {
        /* Driver is already initialized */
        return ARM_DRIVER_OK;
    }

    /* Initialize pcm runtime Resources */
    sai->intInfo->cb = cb;

    /* Clear transfer information */
    memset(sai->xfer, 0, sizeof(SAI_TRANSFER_INFO_t));

    /*Set flag initialized */
    sai->info->flags = SAI_FLAG_INITIALIZED;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_Uninitialize(SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Uninitialize PCM flags, GPIO pins and removes reference to
 *                 callback function.
 * Inputs        : PCM - PCM instance
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SAI_Uninitialize(SAI_RESOURCES_t *sai)
{
    /* Prepare pins configuration */
    const GPIO_PAD_CFG_t gpioLocCfg = {
        .pull_mode     = GPIO_WEAK_PULL_UP,
        .drive_mode    = GPIO_6X,
        .lpf_en        = GPIO_LPF_DISABLE,
        .io_mode       = GPIO_MODE_DISABLED
    };

    /* Reset pin configuration */
    gpio->ConfigurePad(sai->info->default_cfg.mclk_pin,  &gpioLocCfg);
    gpio->ConfigurePad(sai->info->default_cfg.frame_pin, &gpioLocCfg);
    gpio->ConfigurePad(sai->info->default_cfg.seri_pin,  &gpioLocCfg);
    gpio->ConfigurePad(sai->info->default_cfg.sero_pin,  &gpioLocCfg);

    /* Reset DIO_PCM_SRC configuration */
    gpio->ResetAltFuncRegister(GPIO_FUNC_REG_PCM);

    /* Reset PCM status flags */
    sai->info->flags = 0U;

    /* Remove reference to callback function */
    sai->intInfo->cb = NULL;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_PowerControl(ARM_POWER_STATE state,
 *                                         SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Operates the power modes of the PCM interface
 * Inputs        : state - ARM_POWER_FULL or ARM_POWER_OFF
 *                 sai - PCM instance
 * Outputs       : ARM_DRIVER_OK - if the operation is successful
 *                 ARM_DRIVER_ERROR_UNSUPPORTED - if argument is ARM_POWER_LOW
 *                                                or an invalid value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SAI_PowerControl(ARM_POWER_STATE state, SAI_RESOURCES_t *sai)
{
    /* Check what power state should be applied */
    switch (state)
    {
        case ARM_POWER_OFF:
        {
#if (SAI_CM3_CODE_EN)

            /* Disable sai interrupts */
            NVIC_DisableIRQ(sai->intInfo->rx_irqn);
            NVIC_DisableIRQ(sai->intInfo->tx_irqn);

            /* Clear pending sai interrupts in NVIC */
            NVIC_ClearPendingIRQ(sai->intInfo->rx_irqn);
            NVIC_ClearPendingIRQ(sai->intInfo->tx_irqn);
#else  /* if (SAI_CM3_CODE_EN) */

            /* Disable sai error interrupt */
            NVIC_DisableIRQ(sai->intInfo->err_irqn);

            /* Clear pending sai error interrupt */
            NVIC_ClearPendingIRQ(sai->intInfo->err_irqn);
#endif /* if (SAI_CM3_CODE_EN) */

            /* Disable PCM */
            Sys_PCM_Disable();

            /* Clear flags */
            sai->info->status.tx_busy = 0U;
            sai->info->status.rx_busy = 0U;
            sai->info->flags &= ~SAI_FLAG_POWERED;
            break;
        }

        case ARM_POWER_LOW:
        {
            /* Return unsupported*/
            return ARM_DRIVER_ERROR_UNSUPPORTED;
        }

        case ARM_POWER_FULL:
        {
            /* Check if driver was already initialized */
            if ((sai->info->flags & SAI_FLAG_INITIALIZED) == 0U)
            {
                /* Return error */
                return ARM_DRIVER_ERROR;
            }

            /* Check if driver was powered up */
            if ((sai->info->flags & SAI_FLAG_POWERED) != 0U)
            {
                /* Return OK */
                return ARM_DRIVER_OK;
            }

            /* Clear Status flags */
            sai->info->status.tx_busy = 0U;
            sai->info->status.rx_busy = 0U;

            /* Set powered flag */
            sai->info->flags |= SAI_FLAG_POWERED;

#if (SAI_DMA_CODE_EN)

            /* Clear pending sai err interrupt in nvic */
            NVIC_ClearPendingIRQ(sai->intInfo->err_irqn);

            /* Enable sai err interrupt in nvic */
            NVIC_EnableIRQ(sai->intInfo->err_irqn);
#endif /* if (SAI_DMA_CODE_EN) */
            break;
        }

        default:
        {
            /* Return unsupported error */
            return ARM_DRIVER_ERROR_UNSUPPORTED;
        }
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_Control(uint32_t control, uint32_t arg,
 *                                    SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Set mode or execute operation for PCM0. Examples:
 *                 ARM_PCM_ABORT_RECEIVE, ARM_PCM_ABORT_SEND or
 *                 ARM_PCM_MODE_ASYNCHRONOUS.
 * Inputs        : arg1 - Argument 1 of operation (optional)
 *                 arg2 - Argument 2 of operation (optional)
 *                 sai - sai instance
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t SAI_Control(uint32_t control, uint32_t arg1, uint32_t arg2, SAI_RESOURCES_t *sai)
{
    /* Check if driver was powered up */
    if ((sai->info->flags & SAI_FLAG_POWERED) == 0U)
    {
        /* Driver not powered */
        return ARM_DRIVER_ERROR;
    }

    /* Check if any control operation should be executed */
    if (control & ARM_SAI_CONTROL_Msk)
    {
        /* Check which control operation should be executed */
        switch (control & ARM_SAI_CONTROL_Msk)
        {
            case ARM_SAI_CONTROL_TX:
            case ARM_SAI_CONTROL_RX:
            {
                if (arg1)
                {
                    /* Enable PCM */
                    Sys_PCM_Enable();
                }
                else
                {
#if (SAI_DMA_CODE_EN)

                    /* Stop the dma transfer */
                    dma->Stop(sai->info->default_cfg.rx_dma_ch);
                    dma->Stop(sai->info->default_cfg.tx_dma_ch);
#endif /* if (SAI_DMA_CODE_EN) */

                    /* Disable PCM */
                    Sys_PCM_Disable();
                }

                /* Return OK */
                return ARM_DRIVER_OK;
            }

            case ARM_SAI_ABORT_SEND:
            {
#if (SAI_CM3_CODE_EN)

                /* Disable PCM TX interrupt */
                NVIC_DisableIRQ(sai->intInfo->tx_irqn);
#endif /* if (SAI_CM3_CODE_EN) */
#if (SAI_DMA_CODE_EN)

                /* Stop the tx dma transfer */
                dma->Stop(sai->info->default_cfg.tx_dma_ch);
#endif /* if (SAI_DMA_CODE_EN) */

                /* Clear Send active flag */
                sai->info->status.tx_busy = 0U;

                /* Disable PCM interface */
                Sys_PCM_Disable();

                /* Return OK */
                return ARM_DRIVER_OK;
            }

            case ARM_SAI_ABORT_RECEIVE:
            {
#if (SAI_CM3_CODE_EN)

                /* Disable PCM RX interrupt */
                NVIC_DisableIRQ(sai->intInfo->rx_irqn);
#endif /* if (SAI_CM3_CODE_EN) */
#if (SAI_DMA_CODE_EN)

                /* Stop the rx dma transfer */
                dma->Stop(sai->info->default_cfg.rx_dma_ch);
#endif /* if (SAI_DMA_CODE_EN) */

                /* Clear RX busy flag */
                sai->info->status.rx_busy = 0U;
                sai->xfer->rx_cnt = 0U;

                /* Disable PCM interface */
                Sys_PCM_Disable();

                /* Return OK */
                return ARM_DRIVER_OK;
            }

            default:
            {
                /* Return unsupported error */
                return ARM_DRIVER_ERROR_UNSUPPORTED;
            }
        }
    }

    /* Set all pcm predefined parameters not included in CMSIS interface */
    uint32_t sai_cfg = SAI_CFG_INI;

    /* Configure clock, gpio pins and operation mode */
    if ((arg2 != 0)
        && ((arg2 - 1) <= (CLK_DIV_CFG0_USRCLK_PRESCALE_Mask >> CLK_DIV_CFG0_USRCLK_PRESCALE_Pos)))
    {
        /* Set clock and GPIO configuration */
        if (control & ARM_SAI_MODE_Msk)
        {
            /* Prepare pins configuration */
            GPIO_PAD_CFG_t gpioLocCfg = {
                .pull_mode     = GPIO_WEAK_PULL_DOWN,
                .drive_mode    = GPIO_6X,
                .lpf_en        = GPIO_LPF_ENABLE
            };

            /* Disable mclk pin during reconfiguration*/
            gpioLocCfg.io_mode = GPIO_MODE_DISABLED;
            gpio->ConfigurePad(sai->info->default_cfg.mclk_pin, &gpioLocCfg);

            /* Disable frame pin */
            gpioLocCfg.io_mode = GPIO_MODE_DISABLED;
            gpio->ConfigurePad(sai->info->default_cfg.frame_pin, &gpioLocCfg);

            /* Disable seri pin */
            gpioLocCfg.io_mode = GPIO_MODE_DISABLED;
            gpio->ConfigurePad(sai->info->default_cfg.seri_pin, &gpioLocCfg);

            /* Disable sero pin */
            gpioLocCfg.io_mode = GPIO_MODE_DISABLED;
            gpio->ConfigurePad(sai->info->default_cfg.sero_pin, &gpioLocCfg);
        }
        else
        {
            /* Set pcm mode to: slave */
            uint32_t mask = 1 << PCM_CTRL_SLAVE_Pos;
            sai_cfg |= (sai_cfg & (~mask)) | PCM_SELECT_SLAVE;

            /* Prepare pins configuration */
            GPIO_PAD_CFG_t gpioLocCfg = {
                .pull_mode     = GPIO_NO_PULL,
                .drive_mode    = GPIO_6X,
                .lpf_en        = GPIO_LPF_ENABLE
            };

            /* Configure mclk pin */
            gpioLocCfg.io_mode = GPIO_MODE_PCM_CLK_IN;
            gpio->ConfigurePad(sai->info->default_cfg.mclk_pin, &gpioLocCfg);

            /* Configure frame pin */
            gpioLocCfg.io_mode = GPIO_MODE_PCM_FRAME_IN;
            gpio->ConfigurePad(sai->info->default_cfg.frame_pin, &gpioLocCfg);

            /* Configure seri pin */
            gpioLocCfg.io_mode = GPIO_MODE_PCM_SERI_IN;
            gpio->ConfigurePad(sai->info->default_cfg.seri_pin, &gpioLocCfg);

            /* Configure sero pin */
            gpioLocCfg.io_mode = GPIO_MODE_PCM_SERO_OUT;
            gpio->ConfigurePad(sai->info->default_cfg.sero_pin, &gpioLocCfg);
        }
    }
    else
    {
        /* Reset configured flag */
        sai->info->flags &= ~SAI_FLAG_CONFIGURED;

        /* Return unsupported error */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Prepare frame width setting */
    uint32_t mask = (1 << PCM_CTRL_FRAME_ALIGN_Pos) | (1 << PCM_CTRL_FRAME_WIDTH_Pos);

    /* Check which protocol should be applied */
    switch (control & ARM_SAI_PROTOCOL_Msk)
    {
        case ARM_SAI_PROTOCOL_PCM_SHORT:
        {
            /* Set pcm short frame settings */
            sai_cfg &= (~mask);
            sai_cfg |= (PCM_FRAME_ALIGN_LAST | PCM_FRAME_WIDTH_SHORT) & mask;
            break;
        }

        case ARM_SAI_PROTOCOL_I2S:
        case ARM_SAI_PROTOCOL_PCM_LONG:
        {
            /* Set i2s / pcm long frame settings */
            sai_cfg &= (~mask);
            sai_cfg |= (PCM_FRAME_ALIGN_LAST | PCM_FRAME_WIDTH_LONG) & mask;

            break;
        }

        default:
        {
            /* Reset configured flag */
            sai->info->flags &= ~SAI_FLAG_CONFIGURED;

            /* Return unsupported error */
            return ARM_DRIVER_ERROR_UNSUPPORTED;
        }
    }

#if (SAI_DMA_CODE_EN)

    /* Prepare receiver mode dma configuration */
    DMA_CFG_t dmaCfgR = {
        .src_sel       = DMA_TRG_PCM,
        .src_step_mode = DMA_STEP_STATIC,
        .src_word_size = DMA_WORD_SIZE_8,
        .dst_sel       = DMA_TRG_MEM,
        .dst_step_mode = DMA_STEP_INC,
        .dst_word_size = DMA_WORD_SIZE_32,
        .byte_order    = DMA_ENDIANNESS_LITTLE,
        .ch_priority   = DMA_CH_PRI_0,
        .data_mode     = DMA_SINGLE
    };

    /* Prepare transmitter dma configuration */
    DMA_CFG_t dmaCfgT = {
        .src_sel       = DMA_TRG_MEM,
        .src_step_mode = DMA_STEP_INC,
        .src_word_size = DMA_WORD_SIZE_32,
        .dst_sel       = DMA_TRG_PCM,
        .dst_step_mode = DMA_STEP_STATIC,
        .dst_word_size = DMA_WORD_SIZE_8,
        .byte_order    = DMA_ENDIANNESS_LITTLE,
        .ch_priority   = DMA_CH_PRI_0,
        .data_mode     = DMA_SINGLE
    };
#endif /* if (SAI_DMA_CODE_EN) */

    /* Prepare word size (I2S, PCM short frame 8:32, PCM long frame 16:32)*/
    uint32_t word_size = ((control & ARM_SAI_DATA_SIZE_Msk) >> ARM_SAI_DATA_SIZE_Pos) + 1;

    /* Check if incorrect word size has been selected */
    if (((control & ARM_SAI_PROTOCOL_Msk) == ARM_SAI_PROTOCOL_PCM_LONG) && (word_size < 16))
    {
        /* Reset configured flag */
        sai->info->flags &= ~SAI_FLAG_CONFIGURED;

        /* Return unsupported error */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }
    else
    {
        /* Set proper word size */
        sai_cfg &= ~PCM_CTRL_WORD_SIZE_Mask;
        switch (word_size)
        {
            case 8:
            {
                /* Set word size = 8 bits */
                sai_cfg |= PCM_WORD_SIZE_8;

                /* Store the word size */
                sai->info->word_size = 1;

                break;
            }

            case 16:
            {
#if (SAI_DMA_CODE_EN)

                /* Update dma word size */
                dmaCfgR.src_word_size = DMA_WORD_SIZE_16;
                dmaCfgT.dst_word_size = DMA_WORD_SIZE_16;
#endif /* if (SAI_DMA_CODE_EN) */

                /* Set word size = 16 bits */
                sai_cfg |= PCM_WORD_SIZE_16;

                /* Store the word size */
                sai->info->word_size = 2;

                break;
            }

            case 24:
            {
#if (SAI_DMA_CODE_EN)

                /* Return unsupported error */
                return ARM_DRIVER_ERROR_UNSUPPORTED;
#endif /* if (SAI_DMA_CODE_EN) */

                /* Set word size = 24 bits */
                sai_cfg |= PCM_WORD_SIZE_24;

                /* Store the word size */
                sai->info->word_size = 3;

                break;
            }

            case 32:
            {
#if (SAI_DMA_CODE_EN)

                /* Update dma word size */
                dmaCfgR.src_word_size = DMA_WORD_SIZE_32;
                dmaCfgT.dst_word_size = DMA_WORD_SIZE_32;
#endif /* if (SAI_DMA_CODE_EN) */

                /* Set word size = 32 bits */
                sai_cfg |= PCM_WORD_SIZE_32;

                /* Store the word size */
                sai->info->word_size = 4;

                break;
            }

            default:
            {
                /* Reset configured flag */
                sai->info->flags &= ~SAI_FLAG_CONFIGURED;

                /* Return unsupported error */
                return ARM_DRIVER_ERROR_UNSUPPORTED;
            }
        }
    }

    /* Select slot size */
    switch (control & ARM_SAI_PROTOCOL_Msk)
    {
        /* Select slot for pcm short / long protocols */
        case ARM_SAI_PROTOCOL_PCM_SHORT:
        case ARM_SAI_PROTOCOL_PCM_LONG:
        {
            /* Check slot size */
            switch (arg1 & ARM_SAI_SLOT_SIZE_Msk)
            {
                /* Set default slot size */
                case ARM_SAI_SLOT_SIZE_DEFAULT:
                {
                    sai_cfg &= (~(1 << PCM_CTRL_FRAME_SUBFRAMES_Pos) | PCM_CTRL_FRAME_LENGTH_Mask);
                    sai_cfg |= (PCM_SUBFRAME_ENABLE | PCM_MULTIWORD_2);
                    break;
                }

                /* Set slot size = 16 */
                case ARM_SAI_SLOT_SIZE_16:
                {
                    sai_cfg &= (~(1 << PCM_CTRL_FRAME_SUBFRAMES_Pos) | PCM_CTRL_FRAME_LENGTH_Mask);
                    if (word_size == 8)
                    {
                        sai_cfg |= (PCM_SUBFRAME_DISABLE | PCM_MULTIWORD_2);
                    }
                    else if (word_size == 16)
                    {
                        sai_cfg |= (PCM_SUBFRAME_ENABLE | PCM_MULTIWORD_2);
                    }
                    else
                    {
                        /* Reset configured flag */
                        sai->info->flags &= ~SAI_FLAG_CONFIGURED;

                        /* Return unsupported error */
                        return ARM_DRIVER_ERROR_UNSUPPORTED;
                    }
                    break;
                }

                /* Set slot size = 32 */
                case ARM_SAI_SLOT_SIZE_32:
                {
                    sai_cfg &= (~(1 << PCM_CTRL_FRAME_SUBFRAMES_Pos) | PCM_CTRL_FRAME_LENGTH_Mask);
                    if (word_size == 8)
                    {
                        sai_cfg |= (PCM_SUBFRAME_DISABLE | PCM_MULTIWORD_4);
                    }
                    else if (word_size == 16)
                    {
                        sai_cfg |= (PCM_SUBFRAME_DISABLE | PCM_MULTIWORD_2);
                    }
                    else if (word_size == 32)
                    {
                        sai_cfg |= (PCM_SUBFRAME_ENABLE | PCM_MULTIWORD_2);
                    }
                    else
                    {
                        /* Reset configured flag */
                        sai->info->flags &= ~SAI_FLAG_CONFIGURED;

                        /* Return unsupported error */
                        return ARM_DRIVER_ERROR_UNSUPPORTED;
                    }
                    break;
                }

                default:
                {
                    /* Reset configured flag */
                    sai->info->flags &= ~SAI_FLAG_CONFIGURED;

                    /* Return unsupported error */
                    return ARM_DRIVER_ERROR_UNSUPPORTED;
                }
            }
            break;
        }

        /* Select slot for i2s protocols */
        case ARM_SAI_PROTOCOL_I2S:
        {
            /* Check slot size */
            switch (arg1 & ARM_SAI_SLOT_SIZE_Msk)
            {
                /* Set default slot size */
                case ARM_SAI_SLOT_SIZE_DEFAULT:
                {
                    break;
                }

                /* Set slot size = 16 */
                case ARM_SAI_SLOT_SIZE_16:
                {
                    if (word_size != 16)
                    {
                        /* Reset configured flag */
                        sai->info->flags &= ~SAI_FLAG_CONFIGURED;

                        /* Return unsupported error */
                        return ARM_DRIVER_ERROR_UNSUPPORTED;
                    }
                    break;
                }

                /* Set slot size = 32 */
                case ARM_SAI_SLOT_SIZE_32:
                {
                    if (word_size != 32)
                    {
                        /* Reset configured flag */
                        sai->info->flags &= ~SAI_FLAG_CONFIGURED;

                        /* Return unsupported error */
                        return ARM_DRIVER_ERROR_UNSUPPORTED;
                    }
                    break;
                }

                default:
                {
                    /* Reset configured flag */
                    sai->info->flags &= ~SAI_FLAG_CONFIGURED;

                    /* Return unsupported error */
                    return ARM_DRIVER_ERROR_UNSUPPORTED;
                }
            }

            /* Prepare slot size configuration */
            sai_cfg &= (~(1 << PCM_CTRL_FRAME_SUBFRAMES_Pos) | PCM_CTRL_FRAME_LENGTH_Mask);
            sai_cfg |= (PCM_SUBFRAME_DISABLE | PCM_MULTIWORD_2);
            break;
        }

        default:

            /* Reset configured flag */
            sai->info->flags &= ~SAI_FLAG_CONFIGURED;

            /* Return unsupported error */
            return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Set clock polarity - for PCM and I2S only ARM_SAI_CLOCK_POLARITY_0 */
    if ((control & ARM_SAI_CLOCK_POLARITY_Msk) == ARM_SAI_CLOCK_POLARITY_0)
    {
        sai_cfg |= PCM_SAMPLE_RISING_EDGE;
    }
    else
    {
        /* Reset configured flag */
        sai->info->flags &= ~SAI_FLAG_CONFIGURED;

        /* Return unsupported error */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Set bit order - for PCM and I2S only MSB */
    if ((control & ARM_SAI_BIT_ORDER_Msk) == ARM_SAI_MSB_FIRST)
    {
        sai_cfg |= PCM_BIT_ORDER_MSB_FIRST;
    }
    else
    {
        /* Reset configured flag */
        sai->info->flags &= ~SAI_FLAG_CONFIGURED;

        /* Return unsupported error */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

#if (SAI_DMA_CODE_EN)

    /* Configure the rx dma channel */
    dma->Configure(sai->info->default_cfg.rx_dma_ch, &dmaCfgR, sai->info->default_cfg.dma_rx_cb);

    /* Store the rx dma configuration */
    sai->info->dma_rx_cfg = dma->CreateConfigWord(&dmaCfgR);

    /* Configure the tx dma channel */
    dma->Configure(sai->info->default_cfg.tx_dma_ch, &dmaCfgT, sai->info->default_cfg.dma_tx_cb);

    /* Store the tx dma configuration */
    sai->info->dma_tx_cfg = dma->CreateConfigWord(&dmaCfgT);

#endif /* if (SAI_DMA_CODE_EN) */

    /* Configure the pcm */
    Sys_PCM_Config(sai_cfg);

    /* Finish the pin configuration for master mode */
    if ((arg2 != 0)
        && ((arg2 - 1) <= (CLK_DIV_CFG0_USRCLK_PRESCALE_Mask >> CLK_DIV_CFG0_USRCLK_PRESCALE_Pos)))
    {
        /* Set clock and GPIO configuration */
        if (control & ARM_SAI_MODE_Msk)
        {
            /* Set pcm configuration to: master */
            uint32_t mask = 1 << PCM_CTRL_SLAVE_Pos;
            sai_cfg |= (sai_cfg & (~mask)) | PCM_SELECT_MASTER;

            /* configure user clock prescaler */
            uint32_t prescaler =  ((arg2 - 1) << CLK_DIV_CFG0_USRCLK_PRESCALE_Pos) & CLK_DIV_CFG0_USRCLK_PRESCALE_Mask;
            CLK->DIV_CFG0 = (CLK->DIV_CFG0 & ~CLK_DIV_CFG0_USRCLK_PRESCALE_Mask) | prescaler;

            /* Prepare pins configuration */
            GPIO_PAD_CFG_t gpioLocCfg = {
                .pull_mode     = GPIO_WEAK_PULL_DOWN,
                .drive_mode    = GPIO_6X,
                .lpf_en        = GPIO_LPF_ENABLE
            };

            /* Configure mclk pin */
            gpioLocCfg.io_mode = GPIO_MODE_PCM_CLK_IN;
            gpio->ConfigurePad(sai->info->default_cfg.mclk_pin, &gpioLocCfg);

            /* Overwrite the mclk pin mode with the user clock configuration */
            gpioLocCfg.io_mode = GPIO_MODE_USRCLK_OUT;
            gpio->ConfigurePad(sai->info->default_cfg.mclk_pin, &gpioLocCfg);

            /* !!!WORKAROUND!!! additional frame is being send right after reconfiguration. Waiting 1ms
             * after reconfiguration and after setting the clock pin to skip the frame */
            Sys_Delay_ProgramROM(0.001 * SystemCoreClock);

            /* Configure frame pin */
            gpioLocCfg.io_mode = GPIO_MODE_PCM_FRAME_OUT;
            gpio->ConfigurePad(sai->info->default_cfg.frame_pin, &gpioLocCfg);

            /* Configure seri pin */
            gpioLocCfg.io_mode = GPIO_MODE_PCM_SERI_IN;
            gpio->ConfigurePad(sai->info->default_cfg.seri_pin, &gpioLocCfg);

            /* Configure sero pin */
            gpioLocCfg.io_mode = GPIO_MODE_PCM_SERO_OUT;
            gpio->ConfigurePad(sai->info->default_cfg.sero_pin, &gpioLocCfg);
        }
    }

    /* Set configured flag */
    sai->info->flags |= SAI_FLAG_CONFIGURED;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t SAI_Send(const void *data, uint32_t num,
 *                                    SAI_RESOURCES *sai)
 * ----------------------------------------------------------------------------
 * Description   : Non-blocking send function. If a callback was previously
 *                 registered using the Initialize function, the caller is
 *                 notified when 'num' bytes are sent.
 * Inputs        : data                  - pointer to data buffer
 *                 num                   - number of bytes to send
 *                 sai                   - sai instance
 * Outputs       : ARM_DRIVER_OK         - if it can successfully start the
 *                                         send operation
 *                 ARM_DRIVER_ERROR_BUSY - if it's already busy handling
 *                                         other send operation
 * Assumptions   : The data buffer argument points to a memory area with 'num'
 *                 bytes of size
 * ------------------------------------------------------------------------- */
static int32_t SAI_Send(const void *data, uint32_t num, SAI_RESOURCES_t *sai)
{
    /* Check if parameters are correct */
    if ((data == NULL) || (num == 0U))
    {
        /* Return invalid parameter error */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Check if pcm was configured */
    if ((sai->info->flags & SAI_FLAG_CONFIGURED) == 0U)
    {
        /* Return error */
        return ARM_DRIVER_ERROR;
    }

    /* Check if driver is busy */
    if (sai->info->status.tx_busy != 0U)
    {
        /* Return driver busy error */
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Set send active flag */
    sai->info->status.tx_busy = 1U;

    /* Save transmit buffer info */
    sai->xfer->tx_buf = (uint8_t *)data;
    sai->xfer->tx_num = num;
    sai->xfer->tx_cnt = 0U;

    /* Clear the pcm status register */
    sai->reg->STATUS = 0xFFU;

#if (SAI_CM3_CODE_EN)

    /* Clear pending pcm tx interrupt */
    NVIC_ClearPendingIRQ(sai->intInfo->tx_irqn);

    /* Enable pcm tx interrupt */
    NVIC_EnableIRQ(sai->intInfo->tx_irqn);

#endif /* if (SAI_CM3_CODE_EN) */

    /* Store first data */
    sai->reg->TX_DATA = SAIx_PrepareWord(sai);

#if (SAI_DMA_CODE_EN)

    /* Reset dma busy flag */
    sai->info->dma_tx_busy = 0U;

    /* Prepare dma for transmission */
    dma->SetConfigWord(sai->info->default_cfg.tx_dma_ch, sai->info->dma_tx_cfg);

    /* Prepare dma data count */
    uint32_t dataCount = num / sai->info->word_size + ((num % sai->info->word_size != 0) ? 1 : 0);

    /* !!!WORKAROUND!!! first word must be manually send therefore buffer pointer is increased and transfer len is
     * decreased */
    /* Prepare dma tx buffer configuration */
    DMA_ADDR_CFG_t buffTCfg = {
        .src_addr     = ++sai->xfer->tx_buf,
        .dst_addr     = &sai->reg->TX_DATA,
        .counter_len  = dataCount - 1,
        .transfer_len = dataCount - 1
    };

    /* Configure the tx dma channel */
    dma->ConfigureAddr(sai->info->default_cfg.tx_dma_ch, &buffTCfg);

    /* Start the tx dma channel */
    dma->Start(sai->info->default_cfg.tx_dma_ch);

#endif /* if (SAI_DMA_CODE_EN) */

    /* Enable PCM interface */
    Sys_PCM_Enable();

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_Receive(void *data, uint32_t num,
 *                                    SAI_RESOURCES *sai)
 * ----------------------------------------------------------------------------
 * Description   : Non-blocking receive function. If a callback was previously
 *                 registered using the Initialize function, the caller is
 *                 notified when 'num' bytes are received.
 * Inputs        : data                  - pointer to data buffer
 *                 num                   - number of bytes to receive
 *                 sai                   - sai instance
 * Outputs       : ARM_DRIVER_OK         - if it can successfully start the
 *                                         receive operation
 *                 ARM_DRIVER_ERROR_BUSY - if it's already busy handling
 *                                         other receive operation
 * Assumptions   : The data buffer argument points to a memory area with at
 *                 least 'num' bytes of available space
 * ------------------------------------------------------------------------- */
static int32_t SAI_Receive(void *data, uint32_t num, SAI_RESOURCES_t *sai)
{
    /* Check if parameters are correct */
    if ((data == NULL) || (num == 0U))
    {
        /* Return invalid parameter error */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Check if sai was configured */
    if ((sai->info->flags & SAI_FLAG_CONFIGURED) == 0U)
    {
        /* Return error */
        return ARM_DRIVER_ERROR;
    }

    /* Check if driver is busy */
    if (sai->info->status.rx_busy == 1U)
    {
        /* Return driver busy error */
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Set rx busy flag */
    sai->info->status.rx_busy = 1U;

    /* Save number of data to be received */
    sai->xfer->rx_num = num;

    /* Save receive buffer info */
    sai->xfer->rx_buf = (uint8_t *)data;
    sai->xfer->rx_cnt = 0U;

    /* Clear the pcm status register */
    sai->reg->STATUS = 0xFFU;

    /* !!!WORKAROUND!!! dummy read to clear the rx buffer */
    sai->reg->RX_DATA;

#if (SAI_CM3_CODE_EN)

    /* Clear pending pcm rx interrupt */
    NVIC_ClearPendingIRQ(sai->intInfo->rx_irqn);

    /* Enable pcm rx interrupt */
    NVIC_EnableIRQ(sai->intInfo->rx_irqn);
#endif /* if (SAI_CM3_CODE_EN) */

#if (SAI_DMA_CODE_EN)

    /* Reset dma busy flag */
    sai->info->dma_rx_busy = 0U;

    /* Prepare dma for transmission */
    dma->SetConfigWord(sai->info->default_cfg.rx_dma_ch, sai->info->dma_rx_cfg);

    /* Prepare dma data count */
    uint32_t dataCount = num / sai->info->word_size + ((num % sai->info->word_size != 0) ? 1 : 0);

    /* Prepare dma rx buffer configuration */
    DMA_ADDR_CFG_t buffRCfg = {
        .src_addr     = &sai->reg->RX_DATA,
        .dst_addr     = sai->xfer->rx_buf,
        .counter_len  = dataCount,
        .transfer_len = dataCount
    };

    /* Configure the rx dma channel */
    dma->ConfigureAddr(sai->info->default_cfg.rx_dma_ch, &buffRCfg);

    /* Start the rx dma channel */
    dma->Start(sai->info->default_cfg.rx_dma_ch);

#endif /* if (SAI_DMA_CODE_EN) */

    /* Enable PCM */
    Sys_PCM_Enable();

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_GetTxCount(SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Get PCM TX count. This count is reset for every send
 *                 operation.
 * Inputs        : sai    - PCM instance
 * Outputs       : Number of bytes transmitted by PCM
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t SAI_GetTxCount(SAI_RESOURCES_t *sai)
{
#if (SAI_CM3_CODE_EN)

    /* Return counter value */
    return sai->xfer->tx_cnt;
#endif /* if (SAI_CM3_CODE_EN) */
#if (SAI_DMA_CODE_EN)

    /* Check if new transmission is ongoing */
    if (sai->info->dma_tx_busy)
    {
        /* Return counter value */
        return dma->GetCounterValue(sai->info->default_cfg.tx_dma_ch);
    }

    /* Return 0 */
    return 0;
#endif /* if (SAI_DMA_CODE_EN) */
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_GetRxCount(SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Get PCM RX count. This count is reset for every receive
 *                 operation.
 * Inputs        : sai    - PCM instance
 * Outputs       : Number of bytes received by PCM
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t SAI_GetRxCount(SAI_RESOURCES_t *sai)
{
#if (SAI_CM3_CODE_EN)

    /* Return counter value */
    return sai->xfer->rx_cnt;
#endif /* if (SAI_CM3_CODE_EN) */
#if (SAI_DMA_CODE_EN)

    /* Check if new transmission is ongoing */
    if (sai->info->dma_rx_busy)
    {
        /* Return counter value */
        return dma->GetCounterValue(sai->info->default_cfg.rx_dma_ch);
    }

    /* Return 0 */
    return 0;
#endif /* if (SAI_DMA_CODE_EN) */
}

/* ----------------------------------------------------------------------------
 * Function      : void SAI_GetStatus(SAI_RESOURCES_t *sai)
 * ----------------------------------------------------------------------------
 * Description   : Get PCM status
 * Inputs        : sai    - PCM instance
 * Outputs       : Return PCMX status as an ARM_SAI_STATUS structure
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_SAI_STATUS SAI_GetStatus(SAI_RESOURCES_t *sai)
{
    /* Return sai status */
    return sai->info->status;
}

/* wrapper part */
#if RTE_SAI_PCM

/* SAI Driver Wrapper functions. See functions above for usage */
static int32_t SAI0_Initialize(ARM_SAI_SignalEvent_t cb_event)
{
    /* Initialize sai */
    int32_t status = SAI_Initialize(cb_event, &SAI_Resources);

    /* Set sai interrupt priorities */
    status |= SAI_SetInterruptPriority(&SAI_Resources);

#if (RTE_SAI_CFG_EN_DEFAULT)

    /* Set default power mode */
    status |= SAI_PowerControl(ARM_POWER_FULL, &SAI_Resources);

    /* Set default sai configuration */
    status |=
        SAI_Control(SAI_CFG_DEFAULT, ARM_SAI_SLOT_SIZE_DEFAULT, RTE_SAI_MCLK_PRESCALE_DEFAULT, &SAI_Resources);
#endif /* if (RTE_SAI_CFG_EN_DEFAULT) */

    /* Return initialization status */
    return status;
}

static int32_t SAI0_Uninitialize(void)
{
    return SAI_Uninitialize(&SAI_Resources);
}

static int32_t SAI0_PowerControl(ARM_POWER_STATE state)
{
    return SAI_PowerControl(state, &SAI_Resources);
}

static int32_t SAI0_Send(const void *data, uint32_t num)
{
    return SAI_Send(data, num, &SAI_Resources);
}

static int32_t SAI0_Receive(void *data, uint32_t num)
{
    return SAI_Receive(data, num, &SAI_Resources);
}

static uint32_t SAI0_GetTxCount(void)
{
    return SAI_GetTxCount(&SAI_Resources);
}

static uint32_t SAI0_GetRxCount(void)
{
    return SAI_GetRxCount(&SAI_Resources);
}

static int32_t SAI0_Control(uint32_t control, uint32_t arg1, uint32_t arg2)
{
    return SAI_Control(control, arg1, arg2, &SAI_Resources);
}

static ARM_SAI_STATUS SAI0_GetStatus(void)
{
    return SAI_GetStatus(&SAI_Resources);
}

#if (SAI_CM3_CODE_EN)
void PCM_RX_IRQHandler(void)
{
    SAI_RX_IRQHandler(&SAI_Resources);
}

void PCM_TX_IRQHandler(void)
{
    SAI_TX_IRQHandler(&SAI_Resources);
}

#endif /* if (SAI_CM3_CODE_EN) */

#if (SAI_DMA_CODE_EN)
void PCM_ERROR_IRQHandler(void)
{
    SAI_ERROR_IRQHandler(&SAI_Resources);
}

void SAI0_TX_DMAHandler(uint32_t event)
{
    SAI_TX_DMAHandler(event, &SAI_Resources);
}

void SAI0_RX_DMAHandler(uint32_t event)
{
    SAI_RX_DMAHandler(event, &SAI_Resources);
}

#endif /* if (SAI_DMA_CODE_EN) */

/* PCM Driver Control Block */
ARM_DRIVER_SAI Driver_SAI =
{
    SAIx_GetVersion,
    SAIx_GetCapabilities,
    SAI0_Initialize,
    SAI0_Uninitialize,
    SAI0_PowerControl,
    SAI0_Send,
    SAI0_Receive,
    SAI0_GetTxCount,
    SAI0_GetRxCount,
    SAI0_Control,
    SAI0_GetStatus,
};
#endif    /* if RTE_SAI_PCMs */
