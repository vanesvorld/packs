/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * SAI_RSLxx.h
 * - SAI cmsis-driver header file for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#ifndef PCM_RSLXX_H
#define PCM_RSLXX_H

#include <rsl10.h>
#include <Driver_SAI.h>
#include <RTE_Device.h>
#include <Driver_DMA.h>
#include <Driver_GPIO.h>

#if (!RTE_SAI_PCM)
#error "PCM not configured in RTE_Device.h!"
#endif    /* if (!RTE_PCM) */

/* Extern GPIO driver */
extern DRIVER_GPIO_t Driver_GPIO;

/* Extern DMA driver */
extern DRIVER_DMA_t Driver_DMA;

/* SAI flags */
#define SAI_FLAG_INITIALIZED      ((uint8_t)(1U))
#define SAI_FLAG_POWERED          ((uint8_t)(1U << 1))
#define SAI_FLAG_CONFIGURED       ((uint8_t)(1U << 2))

/* DMA / CM3 code switches */
#define SAI_DMA_CODE_EN (RTE_SAI_DMA_EN_DEFAULT)
#define SAI_CM3_CODE_EN (!RTE_SAI_DMA_EN_DEFAULT)

/* Byte mask */
#define SAI_BYTE_MASK (0xFFU)

/* Byte shift */
#define SAI_BYTE_SHIFT (0x8U)

/* SPI interrupt handlers prototypes */
#if(SAI_DMA_CODE_EN)
void PCM_ERROR_IRQHandler(void);
void SAI0_RX_DMAHandler(uint32_t event);
void SAI0_TX_DMAHandler(uint32_t event);
#else
void PCM_RX_IRQHandler(void);
void PCM_TX_IRQHandler(void);
#endif

/* Initial configuration of PCM CMSIS interface */
#if(SAI_CM3_CODE_EN)
#define SAI_CFG_INI           (PCM_TX_ALIGN_LSB   | \
                               PCM_CONTROLLER_CM3 | \
                               PCM_DISABLE)
#else
#define SAI_CFG_INI           (PCM_TX_ALIGN_LSB   | \
                               PCM_CONTROLLER_DMA | \
                               PCM_DISABLE)
#endif

/* SAI auto configuration */
#if(RTE_SAI_CFG_EN_DEFAULT == 1)
#if(SAI_DMA_CODE_EN && (RTE_SAI_WORD_SIZE_DEFAULT != 32))
#error "DMA does not support 24 bit long words"
#endif
#if(RTE_SAI_PROTOCOL_DEFAULT == 1)
#define SAI_CFG_DEFAULT    ((RTE_SAI_MODE_DEFAULT << ARM_SAI_MODE_Pos)    | \
                             ARM_SAI_PROTOCOL_I2S                         | \
                             ARM_SAI_DATA_SIZE(RTE_SAI_WORD_SIZE_DEFAULT) | \
                             ARM_SAI_MSB_FIRST                            | \
                             ARM_SAI_CLOCK_POLARITY_0)
#elif(RTE_SAI_PROTOCOL_DEFAULT == 4)
#define SAI_CFG_DEFAULT    ((RTE_SAI_MODE_DEFAULT << ARM_SAI_MODE_Pos)    | \
                             ARM_SAI_PROTOCOL_PCM_SHORT                   | \
                             ARM_SAI_DATA_SIZE(RTE_SAI_WORD_SIZE_DEFAULT) | \
                             ARM_SAI_MSB_FIRST                            | \
                             ARM_SAI_CLOCK_POLARITY_0)
#elif(RTE_SAI_PROTOCOL_DEFAULT == 5)
#if(RTE_SAI_WORD_SIZE_DEFAULT == 8)
#error "PCM LONG protocol does not support 8 bit long words"
#endif
#define SAI_CFG_DEFAULT    ((RTE_SAI_MODE_DEFAULT << ARM_SAI_MODE_Pos)    | \
                             ARM_SAI_PROTOCOL_PCM_LONG                    | \
                             ARM_SAI_DATA_SIZE(RTE_SAI_WORD_SIZE_DEFAULT) | \
                             ARM_SAI_MSB_FIRST                            | \
                             ARM_SAI_CLOCK_POLARITY_0)
#else
#error "Unsupported protocol selected"
#endif
#endif

/* SAI dma enable */
typedef enum
{
    SAI_DMA_DISABLE  = 0,                    /* dma disabled */
    SAI_DMA_ENABLE   = 1                     /* dma enabled */
} SAI_DMA_EN_t;

/* SAI interrupt priority configuration */
typedef struct _SAI_PRI_CFG_t
{
    uint32_t                preempt_pri  :3;  /* preempt priority */
    uint32_t                             :13; /* reserved */
    uint32_t                subgrp_pri   :3;  /* subgroup priority */
    uint32_t                             :13; /* reserved */
} SAI_PRI_CFG_t;

/* SAI0 default configuration */
typedef struct _SAI_DEFAULT_CFG_t
{
    GPIO_SEL_t              mclk_pin     :4;  /* sai mclk pin identifier */
    GPIO_SEL_t              frame_pin    :4;  /* sai frame pin identifier */
    GPIO_SEL_t              seri_pin     :4;  /* sai seri pin identifier */
    GPIO_SEL_t              sero_pin     :4;  /* sai sero pin identifier */
#if(SAI_DMA_CODE_EN)
    DMA_SignalEvent_t       dma_rx_cb;        /* dma rx callback funcion */
    DMA_SignalEvent_t       dma_tx_cb;        /* dma tx callback funcion */
    DMA_SEL_t               rx_dma_ch    :3;  /* sai rx transmission dma channel */
    DMA_SEL_t               tx_dma_ch    :3;  /* sai tx transmission dma channel */
    uint8_t                              :2;  /* reserved */
#endif
#if(RTE_SAI_CFG_EN_DEFAULT)
    uint32_t                conf         :32; /* sai configuration */
    uint32_t                pre          :32; /* sai prescale */
#endif
    SAI_PRI_CFG_t           pri_cfg;          /* sai interrupt priority default configuration */
} SAI_DEFAULT_CFG_t;

/* SAI Transfer Information (Run-Time) */
typedef struct _SAI_TRANSFER_INFO_t {
    uint32_t rx_num;                          /* Total number of receive data */
    uint32_t tx_num;                          /* Total number of transmit data */
    uint8_t *rx_buf;                          /* Pointer to in data buffer */
    uint8_t *tx_buf;                          /* Pointer to out data buffer */
    uint32_t rx_cnt;                          /* Number of data received */
    uint32_t tx_cnt;                          /* Number of data sent */
} SAI_TRANSFER_INFO_t;

/* SAI Information (Run-time) */
typedef struct _SAI_INFO_t
{
    const SAI_DEFAULT_CFG_t default_cfg;      /* sai default configuration */
    ARM_SAI_STATUS          status;           /* status flags */
    uint8_t                 flags;            /* current sai flags */
    uint8_t                 word_size;        /* current word size */
    uint32_t                mode;             /* current sai mode */
#if(RTE_SAI_DMA_EN_DEFAULT)
    uint32_t                dma_rx_cfg;       /* dma channel receiver configuration */
    uint32_t                dma_tx_cfg;       /* dma channel transmitter configuration */
    uint8_t                 dma_rx_busy  :1;  /* !!!WORKAROUND!!! for dma rx counter not being reset before new transmission */
    uint8_t                 dma_tx_busy  :1;  /* !!!WORKAROUND!!! for dma tx counter not being reset before new transmission */
    uint8_t                              :6;  /* reserved */
#endif
} SAI_INFO_t;

/* SAI interrupts info */
typedef struct _SAI_INT_INFO_t
{
#if(!RTE_SAI_DMA_EN_DEFAULT)
    const IRQn_Type         rx_irqn;          /* sai rx IRQ Number */
    const IRQn_Type         tx_irqn;          /* sai tx IRQ Number */
#endif
#if(RTE_SAI_DMA_EN_DEFAULT)
    const IRQn_Type         err_irqn;         /* sai errors IRQ Number */
#endif
    ARM_SAI_SignalEvent_t   cb;               /* sai event callback */
} SAI_INT_INFO_t;

/* SAI resources definition */
typedef const struct
{
    PCM_Type               *reg;              /* sai peripheral pointer */
    SAI_INT_INFO_t         *intInfo;          /* IRQs Info */
    SAI_INFO_t             *info;             /* runtime Information */
    SAI_TRANSFER_INFO_t    *xfer;             /* sai transfer information */
} SAI_RESOURCES_t;

#endif    /* PCM_RSLXX_H */
