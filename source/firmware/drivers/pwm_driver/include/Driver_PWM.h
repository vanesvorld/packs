/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * Driver_PWM.h
 * - PWM driver header file for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#ifndef DRIVER_PWM_H_
#define DRIVER_PWM_H_

#ifdef  __cplusplus
extern "C"
{
#endif

#include <Driver_Common.h>

#define ARM_PWM_API_VERSION ARM_DRIVER_VERSION_MAJOR_MINOR(1,0)  /* API version */

/****** PWM Control Codes *****/

/*----- PWM Control Codes: PWM Selection -----*/
typedef enum _PWM_SEL_t {
    PWM_0 = 0,                                     ///< pwm module 0
    PWM_1 = 1                                      ///< pwm module 0
} PWM_SEL_t;

/*----- PWM Control Codes: PWM Prescale values -----*/
typedef enum _PWM_PRESCALE_t {
    PWM_PRESCALE_1  = 0x0U,                        ///< pwm Prescale val = 1
    PWM_PRESCALE_2  = 0x1U,                        ///< pwm Prescale val = 2
    PWM_PRESCALE_63 = 0x3EU,                       ///< pwm Prescale val = 63
    PWM_PRESCALE_64 = 0x3FU                        ///< pwm Prescale val = 64
} PWM_PRESCALE_t;

/*----- PWM Control Codes: PWM Offset values -----*/
typedef enum _PWM_OFFSET_EN_t {
    PWM_OFFSET_DIS  = 0x0,                         ///< pwm Offset disabled
    PWM_OFFSET_EN   = 0x1,                         ///< pwm Offset enabled
} PWM_OFFSET_EN_t;

/*----- PWM Control Codes: Error codes -----*/
#define PWM_ERROR_UNCONFIGURED  (ARM_DRIVER_ERROR_SPECIFIC - 1)     ///< Driver has not been configured yet

// Function documentation
/**
  \fn          ARM_DRIVER_VERSION PWM_GetVersion (void)
  \brief       Get driver version.
  \return      \ref ARM_DRIVER_VERSION

  \fn          int32_t PWM_Initialize (void)
  \brief       Initialize pwm driver with default configuration.
  \return      \ref execution_status

  \fn          int32_t PWM_Configure (PWM_SEL_t sel, PWM_CFG_t *pwm_cfg)
  \brief       Configure PWM common setting.
  \param[in]   sel PWM to be configured \ref PWM_SEL_t
  \param[in]   pwm_cfg Pointer to \ref PWM_CFG_t
  \return      \ref execution_status

  \fn          int32_t PWM_SetPeriod (PWM_SEL_t sel, uint8_t period)
  \brief       Set the PWM period.
  \param[in]   sel PWM to be configured \ref PWM_SEL
  \param[in]   period Period value
  \return      \ref execution_status

  \fn          int32_t PWM_SetDutyCycle (PWM_SEL_t sel, uint8_t duty_cycle)
  \brief       Set the PWM duty cycle (expressed in percentage)
  \param[in]   sel PWM to be configured \ref PWM_SEL_t
  \param[in]   duty_cycle Duty cycle value (expressed in percentage)
  \return      \ref execution_status

  \fn          int32_t PWM_SetHighPeriod (PWM_SEL_t sel, uint8_t high_period)
  \brief       Set the PWM duty cycle (expressed in cycles).
  \param[in]   sel PWM to be configured \ref PWM_SEL_t
  \param[in]   high_period Duty cycle value (expressed in cycles)
  \return      \ref execution_status

  \fn          int32_t PWM_SetOffset (PWM_OFFSET_CFG_t *cfg)
  \brief       Set the offset between pwms.
  \param[in]   cfg Pointer to offset configuration \ref PWM1_OFFSET_CFG
  \return      \ref execution_status

  \fn          int32_t PWM_Start (PWM_SEL_t sel)
  \brief       Start the PWM.
  \param[in]   sel PWM to be started \ref PWM_SEL_t
  \return      \ref execution_status

  \fn          int32_t PWM_Stop (PWM_SEL_t sel)
  \brief       Stop the PWM.
  \param[in]   sel PWM to be stopped \ref PWM_SEL
  \return      \ref execution_status
*/

/**
\brief PWM1 Driver configuration.
*/
typedef struct _PWM_OFFSET_CFG_t
{
    PWM_OFFSET_EN_t   offset_en    :1;    ///< enable / disable offset
    uint32_t          offset_val   :8;    ///< offset value
    uint32_t                       :23;   ///< reserved
} PWM_OFFSET_CFG_t;

/**
\brief PWM Driver configuration.
*/
typedef struct _PWM_CFG_t
{
    uint32_t          period       :8;    ///< period value
    uint32_t          high_cycle   :8;    ///< high cycle value
    uint32_t                       :16;   ///< reserved
} PWM_CFG_t;

/**
\brief Access structure of the PWM Driver.
*/
typedef struct _DRIVER_PWM_t {
    ARM_DRIVER_VERSION (*GetVersion)    (void);                                       ///< Pointer to \ref PWM_GetVersion : Get driver version.
    int32_t            (*Initialize)    (void);                                       ///< Pointer to \ref PWM_Initialize : Initialize pwm driver.
    int32_t            (*Configure)     (PWM_SEL_t sel, const PWM_CFG_t * pwm_cfg);   ///< Pointer to \ref PWM_Configure : Configure pwm common setting.
    int32_t            (*SetPeriod)     (PWM_SEL_t sel, uint8_t period);              ///< Pointer to \ref PWM_SetPeriod : Set the period.
    int32_t            (*SetDutyCycle)  (PWM_SEL_t sel, uint8_t duty_cycle);          ///< Pointer to \ref PWM_SetDutyCycle : Set the duty cycle (percentage).
    int32_t            (*SetHighPeriod) (PWM_SEL_t sel, uint8_t high_period);         ///< Pointer to \ref PWM_SetHighPeriod : Set the high cycle period.
    int32_t            (*PWM_SetOffset) (PWM_OFFSET_CFG_t *cfg);                      ///< Pointer to \ref PWM_SetOffset : Set offset between pwms.
    int32_t            (*Start)         (PWM_SEL_t sel);                              ///< Pointer to \ref PWM_Start : Start the pwm.
    int32_t            (*Stop)          (PWM_SEL_t sel);                              ///< Pointer to \ref PWM_Stop : Stop the pwm.
} const DRIVER_PWM_t;

#ifdef  __cplusplus
}
#endif

#endif /* DRIVER_PWM_H_ */
