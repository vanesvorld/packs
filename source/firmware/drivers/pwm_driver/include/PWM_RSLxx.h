/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * PWM_RSLxx.h
 * - PWM driver header file for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#ifndef PWM_RSLXX_H
#define PWM_RSLXX_H

#include <rsl10.h>
#include <RTE_Device.h>
#include <GPIO_RSLxx.h>
#include <Driver_PWM.h>

#if (!RTE_PWM)
  #error "PWM not configured in RTE_Device.h!"
#endif    /* if (!RTE_PWM) */

/* Extern GPIO driver */
extern DRIVER_GPIO_t Driver_GPIO;

/* PWM flags */
#define PWM_FLAG_BIT_SET      1U

/* PWM enabled driver mask */
#define PWM_EN_MSK            ((RTE_PWM0_EN << PWM_0) | \
                               (RTE_PWM1_EN << PWM_1))

/* PWM max values */
#define PWM_MAX_PERCENTAGE     100UL

/* PWM number */
#define PWM_PWMS_NUMBER        2

/* PWM module dio default configuration */
typedef struct _PWM_DIO_DEFAULT_CFG_t
{
    uint8_t                   dio_pin     :8;                      /* dio selection */
} PWM_DIO_DEFAULT_CFG_t;

/* PWM default configuration */
typedef const struct _PWM_DEFAULT_CFG_t
{
    PWM_CFG_t                 pwm_default_cfg;                     /* pwm default configuration */
    PWM_DIO_DEFAULT_CFG_t     pwm_default_dio_cfg;                 /* pwm dio default configuration */
} PWM_DEFAULT_CFG_t;

/* PWM interrupt info */
typedef struct _PWM_INFO_t
{
    uint8_t                   flags;                               /* current pwm flags */
    PWM_OFFSET_CFG_t          default_offset_cfg;                  /* pwm default configuration */
    const PWM_DEFAULT_CFG_t  *const default_cfg[PWM_PWMS_NUMBER];  /* pwm default configuration */
} PWM_INFO_t;

/* PWM Resources definition */
typedef struct
{
    PWM_INFO_t               *info;                                /* run-time information */
} PWM_RESOURCES_t;

#endif /* PWM_RSLXX_H */
