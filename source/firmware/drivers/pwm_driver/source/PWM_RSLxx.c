/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * PWM_RSLxx.c
 * - PWM driver implementation for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#include <PWM_RSLxx.h>
#include <GPIO_RSLxx.h>

#if RTE_PWM

/* Driver Version Macro */
#define ARM_PWM_DRV_VERSION    ARM_DRIVER_VERSION_MAJOR_MINOR(0, 1)

/* Driver Version */
static const ARM_DRIVER_VERSION DriverVersion =
{
    ARM_PWM_API_VERSION,
    ARM_PWM_DRV_VERSION
};

/* Set default pwm0 configuration */
#if (RTE_PWM0_EN)
static const PWM_DEFAULT_CFG_t pwm0_DefaultCfg = {
    .pwm_default_cfg = {                                  /* pwm 0 default configuration */
        .period              = RTE_PWM0_PER_DEFAULT,      /* pwm 0 default period */
        .high_cycle          = RTE_PWM0_DTC_DEFAULT       /* pwm 0 default duty cycle */
    },
    .pwm_default_dio_cfg = {                              /* pwm 0 default dio configuration */
        .dio_pin             = RTE_PWM0_DIO_PIN           /* pwm 0 default dio pad number */
    }
};
#endif /* if (RTE_PWM0_EN) */

/* Set default pwm1 configuration */
#if (RTE_PWM1_EN)
static const PWM_DEFAULT_CFG_t pwm1_DefaultCfg = {
    .pwm_default_cfg = {                                  /* pwm 1 default configuration */
        .period              = RTE_PWM1_PER_DEFAULT,      /* pwm 1 default period */
        .high_cycle          = RTE_PWM1_DTC_DEFAULT,      /* pwm 1 default duty cycle */
    },
    .pwm_default_dio_cfg = {                              /* pwm 1 default dio configuration */
        .dio_pin             = RTE_PWM1_DIO_PIN           /* pwm 1 default dio pad number */
    }
};
#endif /* if (RTE_PWM1_EN) */

/* Pwm run-time information */
static PWM_INFO_t PWM_Info = {
    .flags = 0,                                           /* pwms offset */
    .default_offset_cfg = {
        .offset_en  = RTE_PWM1_OFF_EN_DEFAULT,            /* pwm default offset enable */
        .offset_val = RTE_PWM1_OFF_VAL_DEFAULT            /* pwm default offset value */
    },
#if (RTE_PWM0_EN)
    .default_cfg[0]          = &pwm0_DefaultCfg,          /* pwm 0 default configuration */
#endif /* if (RTE_PWM0_EN) */
#if (RTE_PWM1_EN)
    .default_cfg[1]          = &pwm1_DefaultCfg,          /* pwm 1 default configuration */
#endif /* if (RTE_PWM1_EN) */
};

/* Pwm resources */
static PWM_RESOURCES_t PWM_Resources =
{
    &PWM_Info,                                            /* pwm run-time information */
};

/* ----------------------------------------------------------------------------
 * Function      : void PWM_DC_WORKAROUND(PWM_SEL_t sel, uint32_t duty_cycle)
 * ----------------------------------------------------------------------------
 * Description   : Workaround for pwm spikes when duty cycle is set to 0
 * Inputs        : sel        - pwm to be configured
 *               : duty_cycle - duty cycle to be set
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void PWM_DC_WORKAROUND(PWM_SEL_t sel, uint32_t duty_cycle)
{
    /* Flags indicating if workaround was already applied */
    static uint32_t flags = 0;

    /* Prepare gpio modes matrix */
    const uint32_t gpioModes[2][2] = {
        { GPIO_MODE_PWM0_OUT, GPIO_MODE_GPIO_OUT_0 },
        { GPIO_MODE_PWM1_OUT, GPIO_MODE_GPIO_OUT_0 }
    };

    /* Check if gpio mode should be chenged */
    if (!(!duty_cycle != !(flags & (PWM_FLAG_BIT_SET << sel))))
    {
        /* Pointer to gpio driver */
        DRIVER_GPIO_t *gpio = &Driver_GPIO;

        /* Prepare gpio configuration */
        GPIO_PAD_CFG_t gpioCfg = {
            .pull_mode     = GPIO_NO_PULL,
            .drive_mode    = GPIO_6X,
            .lpf_en        = GPIO_LPF_DISABLE,
            .io_mode       = gpioModes[sel][!duty_cycle]
        };

        /* Update the workaround flag status */
        flags ^= PWM_FLAG_BIT_SET << sel;

        /* Set pin low */
        gpio->SetLow(PWM_Resources.info->default_cfg[sel]->pwm_default_dio_cfg.dio_pin);

        /* Configure gpio pad */
        gpio->ConfigurePad(PWM_Resources.info->default_cfg[sel]->pwm_default_dio_cfg.dio_pin, &gpioCfg);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void PWM_GetVersion (void)
 * ----------------------------------------------------------------------------
 * Description   : Get driver version
 * Inputs        : None
 * Outputs       : ARM_DRIVER_VERSION
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_DRIVER_VERSION PWM_GetVersion (void)
{
    /* Return driver version */
    return DriverVersion;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t PWM_Configure(PWM_SEL_t sel, PWM_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure pwm common setting
 * Inputs        : sel     - pwm to be configured
 *               : cfg     - pointer to pwm configuration structure
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t PWM_Configure(PWM_SEL_t sel, const PWM_CFG_t *cfg)
{
    /* Pointer to gpio driver */
    DRIVER_GPIO_t *gpio = &Driver_GPIO;

    /* Check if correct pwm was selected */
    if (!((PWM_FLAG_BIT_SET << sel) & PWM_EN_MSK))
    {
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Prepare gpio configuration */
    GPIO_PAD_CFG_t gpioCfg = {
        .pull_mode     = GPIO_NO_PULL,
        .drive_mode    = GPIO_6X,
        .lpf_en        = GPIO_LPF_DISABLE
    };

#if (RTE_PWM0_EN)

    /* Configure pwm0 */
    if (sel == PWM_0)
    {
        /* Set the gpio config as pwm0 config */
        gpioCfg.io_mode = GPIO_MODE_PWM0_OUT;
    }
#endif /* if (RTE_PWM0_EN) */

#if (RTE_PWM1_EN)

    /* Configure pwm1 */
    if (sel == PWM_1)
    {
        /* Set the gpio config as pwm0 config */
        gpioCfg.io_mode = GPIO_MODE_PWM1_OUT;
    }
#endif /* if (RTE_PWM1_EN) */

    /* Configure gpio pad */
    gpio->ConfigurePad(PWM_Resources.info->default_cfg[sel]->pwm_default_dio_cfg.dio_pin, &gpioCfg);

    /* Configure pwm period */
    ((PWM_CFG_Type *)&PWM->CFG[sel])->PWM_PERIOD_BYTE = cfg->period;

    /* Configure pwm high period */
    ((PWM_CFG_Type *)&PWM->CFG[sel])->PWM_HIGH_BYTE = cfg->high_cycle;

    /* Indicate that pwm was Initialized */
    PWM_Resources.info->flags |= PWM_FLAG_BIT_SET << sel;

    /* Apply pwm workaround for spikes */
    PWM_DC_WORKAROUND(sel, cfg->high_cycle);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void PWM_SetOffset(PWM1_OFFSET_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Set the pwm1 offset value.
 * Inputs        : cfg     - pointer to pwm configuration structure
 * Outputs       : execution_status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t PWM_SetOffset(PWM_OFFSET_CFG_t *cfg)
{
    /* Configure pwm1 offset */
    PWM->CTRL &= ~(PWM_CTRL_PWM_OFFSET_ENABLE_Pos | PWM_CTRL_PWM_OFFSET_Mask);
    PWM->CTRL |= cfg->offset_en  << PWM_CTRL_PWM_OFFSET_ENABLE_Pos |
                 cfg->offset_val << PWM_CTRL_PWM_OFFSET_Pos;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t PWM_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initializes the pwm driver
 * Inputs        : None
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t PWM_Initialize(void)
{
    /* Configure each enabled pwm module */
    for (int i = 0; i < PWM_PWMS_NUMBER; ++i)
    {
        /* Check if particular pwm should be initialized */
        if ((PWM_FLAG_BIT_SET << i) & PWM_EN_MSK)
        {
            /* Configure particular pwm */
            PWM_Configure(i, &PWM_Resources.info->default_cfg[i]->pwm_default_cfg);

            /* Indicate that particular pwm was Initialized */
            PWM_Resources.info->flags |= PWM_FLAG_BIT_SET << i;
        }
    }

    /* Configure pwms offset */
    PWM_SetOffset(&PWM_Resources.info->default_offset_cfg);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t PWM_SetPeriod(PWM_SEL_t sel, uint8_t period)
 * ----------------------------------------------------------------------------
 * Description   : Set the pwm period.
 * Inputs        : sel    - pwm to be configured
 *               : period - period value
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t PWM_SetPeriod (PWM_SEL_t sel, uint8_t period)
{
    /* Check if correct pwm was selected */
    if (!((PWM_FLAG_BIT_SET << sel) & PWM_EN_MSK))
    {
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Check if pwm has been already configured */
    if (!(PWM_Resources.info->flags & (PWM_FLAG_BIT_SET << sel)))
    {
        return PWM_ERROR_UNCONFIGURED;
    }

    /* Configure pwm period */
    ((PWM_CFG_Type *)&PWM->CFG[sel])->PWM_PERIOD_BYTE = period;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void PWM_SetDutyCycle(PWM_SEL sel, uint8_t duty_cycle)
 * ----------------------------------------------------------------------------
 * Description   : Set the pwm duty cycle (expressed in percentage).
 * Inputs        : sel        - pwm to be configured
 *               : duty_cycle - duty cycle value (expressed in percentage)
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t PWM_SetDutyCycle(PWM_SEL_t sel, uint8_t duty_cycle)
{
    /* Check if correct pwm was selected */
    if (!((PWM_FLAG_BIT_SET << sel) & PWM_EN_MSK))
    {
        /* Return unsupported error */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Check if pwm has been already configured */
    if (!(PWM_Resources.info->flags & (PWM_FLAG_BIT_SET << sel)))
    {
        /* Return driver unconfigured error */
        return PWM_ERROR_UNCONFIGURED;
    }

    /* Check if correct values were passed */
    if (duty_cycle > PWM_MAX_PERCENTAGE)
    {
        /* Return param error */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Configure pwm duty cycle */
    uint32_t cfg = ((PWM_CFG_Type *)&PWM->CFG[sel])->PWM_PERIOD_BYTE * duty_cycle / 100;
    ((PWM_CFG_Type *)&PWM->CFG[sel])->PWM_HIGH_BYTE = cfg;

    /* Apply pwm workaround for spikes */
    PWM_DC_WORKAROUND(sel, duty_cycle);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void PWM_SetHighPeriod(PWM_SEL_t sel, uint8_t high_period)
 * ----------------------------------------------------------------------------
 * Description   : Set the pwm duty cycle (expressed in cycles).
 * Inputs        : sel         - pwm to be configured
 *               : high_period - duty cycle value (expressed in cycles)
 * Outputs       : execution_status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t PWM_SetHighPeriod(PWM_SEL_t sel, uint8_t high_period)
{
    /* Check if correct pwm was selected */
    if (!((PWM_FLAG_BIT_SET << sel) & PWM_EN_MSK))
    {
        /* Return unsupported error */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Check if pwm has been already configured */
    if (!(PWM_Resources.info->flags & (PWM_FLAG_BIT_SET << sel)))
    {
        /* Return driver unconfigured error */
        return PWM_ERROR_UNCONFIGURED;
    }

    /* Check if high period value is not too high */
    if (high_period > ((PWM_CFG_Type *)&PWM->CFG[sel])->PWM_PERIOD_BYTE)
    {
        /* Return param error */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Configure pwm high period */
    ((PWM_CFG_Type *)&PWM->CFG[sel])->PWM_HIGH_BYTE = high_period;

    /* Apply pwm workaround for spikes */
    PWM_DC_WORKAROUND(sel, high_period);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void PWM_Start(PWM_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Start the pwm.
 * Inputs        : sel - pwm to be started
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t PWM_Start(PWM_SEL_t sel)
{
    /* Check if correct pwm was selected */
    if (!((PWM_FLAG_BIT_SET << sel) & PWM_EN_MSK))
    {
        /* Return unsupported error */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Check if pwm has been already configured */
    if (!(PWM_Resources.info->flags & (PWM_FLAG_BIT_SET << sel)))
    {
        /* Return driver unconfigured error */
        return PWM_ERROR_UNCONFIGURED;
    }

    /* Enable pwm */
    Sys_PWM_Enable(sel, 1);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void PWM_Stop(PWM_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Stop the pwm.
 * Inputs        : sel - pwm to be stopped
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t PWM_Stop(PWM_SEL_t sel)
{
    /* Check if correct pwm was selected */
    if (!((PWM_FLAG_BIT_SET << sel) & PWM_EN_MSK))
    {
        /* Return unsupported error */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Check if pwm has been already configured */
    if (!(PWM_Resources.info->flags & (PWM_FLAG_BIT_SET << sel)))
    {
        /* Return driver unconfigured error */
        return PWM_ERROR_UNCONFIGURED;
    }

    /* Enable pwm */
    Sys_PWM_Enable(sel, 0);

    return ARM_DRIVER_OK;
}

/* Pwm driver control block */
DRIVER_PWM_t Driver_PWM =
{
    PWM_GetVersion,
    PWM_Initialize,
    PWM_Configure,
    PWM_SetPeriod,
    PWM_SetDutyCycle,
    PWM_SetHighPeriod,
    PWM_SetOffset,
    PWM_Start,
    PWM_Stop
};

#endif    /* RTE_PWM */
