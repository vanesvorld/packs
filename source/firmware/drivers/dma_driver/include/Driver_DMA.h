/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * Driver_DMA.h
 * - DMA driver header file for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#ifndef DRIVER_DMA_H_
#define DRIVER_DMA_H_

#ifdef  __cplusplus
extern "C"
{
#endif

#include <Driver_Common.h>

#define ARM_DMA_API_VERSION ARM_DRIVER_VERSION_MAJOR_MINOR(1,0)  /* API version */

/****** DMA Control Codes *****/

/*----- DMA Control Codes: DMA Channel Selection -----*/
typedef enum _DMA_SEL_t {
    DMA_CH_0          = 0,                ///< dma channel 0
    DMA_CH_1          = 1,                ///< dma channel 1
    DMA_CH_2          = 2,                ///< dma channel 2
    DMA_CH_3          = 3,                ///< dma channel 3
    DMA_CH_4          = 4,                ///< dma channel 4
    DMA_CH_5          = 5,                ///< dma channel 5
    DMA_CH_6          = 6,                ///< dma channel 6
    DMA_CH_7          = 7,                ///< dma channel 7
} DMA_SEL_t;

/*----- DMA Control Codes: Src/Dst Target -----*/
typedef enum _DMA_TRG_t {
    DMA_TRG_I2C       = 0,                ///< src / dst target = I2C
    DMA_TRG_SPI0      = 1,                ///< src / dst target = SPI0
    DMA_TRG_SPI1      = 2,                ///< src / dst target = SPI1
    DMA_TRG_PCM       = 3,                ///< src / dst target = PCM
    DMA_TRG_UART      = 4,                ///< src / dst target = UART
    DMA_TRG_ASRC      = 5,                ///< src / dst target = ASRC
    DMA_TRG_PBUS      = 6,                ///< src / dst target = PBUS
    DMA_TRG_OD_DMIC   = 7,                ///< src / dst target = OD(destination) or DMIC(source)
    DMA_TRG_MEM       = 8                 ///< src / dst target = I2C
} DMA_TRG_t;

/*----- DMA Control Codes: Step Size -----*/
typedef enum _DMA_STEP_SIZE_t {
     DMA_STEP_SIZE_1  = 0x0U,             ///< step size = 1 x 32-bit word
     DMA_STEP_SIZE_2  = 0x1U,             ///< step size = 2 x 32-bit word
     DMA_STEP_SIZE_3  = 0x2U,             ///< step size = 3 x 32-bit word
     DMA_STEP_SIZE_4  = 0x3U,             ///< step size = 4 x 32-bit word
} DMA_STEP_SIZE_t;

/*----- DMA Control Codes: Clock Sources -----*/
typedef enum _DMA_STEP_MODE_t {
    DMA_STEP_INC      = 0x0U,             ///< increment address with step size
    DMA_STEP_DEC      = 0x1U,             ///< decrement address with step size
    DMA_STEP_STATIC   = 0x2U              ///< keep the address value constant
} DMA_STEP_MODE_t;

/*----- DMA Control Codes: Transfer Mode -----*/
typedef enum _DMA_DATA_MODE_t {
    DMA_REPEAT        = 0x0U,             ///< data to be transfered repeatedly
    DMA_SINGLE        = 0x1U,             ///< single data transfer
} DMA_DATA_MODE_t;

/*----- DMA Control Codes: Byte Order-----*/
typedef enum _DMA_BYTE_ORDER_t {
    DMA_ENDIANNESS_LITTLE = 0x0U,         ///< little endian to be used
	DMA_ENDIANNESS_BIG    = 0x1U          ///< big endian to be used
} DMA_BYTE_ORDER_t;

/*----- DMA Control Codes: Word Size -----*/
typedef enum _DMA_WORD_SIZE_t {
    DMA_WORD_SIZE_4   = 0x3U,             ///< word size = 4bits
    DMA_WORD_SIZE_8   = 0x0U,             ///< word size = 8bits
    DMA_WORD_SIZE_16  = 0x1U,             ///< word size = 16bits
    DMA_WORD_SIZE_32  = 0x2U              ///< word size = 32bits
} DMA_WORD_SIZE_t;

/*----- DMA Control Codes: Channel Priority -----*/
typedef enum _DMA_CH_PRI_t {
    DMA_CH_PRI_0      = 0x0U,             ///< channel priority = 0
    DMA_CH_PRI_1      = 0x1U,             ///< channel priority = 1
    DMA_CH_PRI_2      = 0x2U,             ///< channel priority = 2
    DMA_CH_PRI_3      = 0x3U              ///< channel priority = 3
} DMA_CH_PRI_t;

/*----- DMA Control Codes: Interrupt Channel Source -----*/
typedef enum _ADC_EVENT_SRC_t {
    DMA_DMA0_EVENT     = 1 << DMA_CH_0,   ///< dma channel 0 event
    DMA_DMA1_EVENT     = 1 << DMA_CH_1,   ///< dma channel 1 event
    DMA_DMA2_EVENT     = 1 << DMA_CH_2,   ///< dma channel 2 event
    DMA_DMA3_EVENT     = 1 << DMA_CH_3,   ///< dma channel 3 event
    DMA_DMA4_EVENT     = 1 << DMA_CH_4,   ///< dma channel 4 event
    DMA_DMA5_EVENT     = 1 << DMA_CH_5,   ///< dma channel 5 event
    DMA_DMA6_EVENT     = 1 << DMA_CH_6,   ///< dma channel 6 event
    DMA_DMA7_EVENT     = 1 << DMA_CH_7    ///< dma channel 7 event
} ADC_EVENT_SRC_t;

/*----- DMA Control Codes: Error codes -----*/
#define DMA_ERROR_UNCONFIGURED  (ARM_DRIVER_ERROR_SPECIFIC - 1) ///< DMA channel has not been configured yet

// Function documentation
/**
  \fn          ARM_DRIVER_VERSION ARM_DMA_GetVersion (void)
  \brief       Get driver version.
  \return      \ref ARM_DRIVER_VERSION

  \fn          int32_t DMA_Initialize (DMA_SignalEvent_t cb_event)
  \brief       Initialize dma driver with default configuration.
  \param[in]   cb_event pointer to \ref DMA_SignalEvent
  \return      \ref execution_status

  \fn          int32_t DMA_Configure (DMA_SEL_t sel, DMA_CFG_t *cfg, DMA_SignalEvent_t cb)
  \brief       Configure particular dma channel.
  \param[in]   sel DMA channel to be configured (\ref DMA_SEL_t)
  \param[in]   cfg Pointer to \ref DMA_CFG_t
  \param[in]   cb_event Pointer to \ref DMA_SignalEvent_t
  \return      \ref execution_status

  \fn          int32_t DMA_ConfigureWord (DMA_SEL_t sel, uint32_t cfg, DMA_SignalEvent_t cb)
  \brief       Configure particular dma channel.
  \param[in]   sel DMA channel to be configured (\ref DMA_SEL_t)
  \param[in]   cfg Configuration word
  \param[in]   cb_event Pointer to \ref DMA_SignalEvent_t
  \return      \ref execution_status


  \fn          int32_t DMA_ConfigureAddr (DMA_SEL_t sel, const DMA_ADDR_CFG_t *cfg)
  \brief       Configure dma channel source and destination addresses.
  \param[in]   sel DMA to be configured (\ref DMA_SEL_t)
  \param[in]   cfg  Pointer to \ref DMA_ADDR_CFG_t
  \return      \ref execution_status

  \fn          int32_t DMA_SetInterruptPriority (DMA_SEL sel, const DMA_PRI_CFG *cfg)
  \brief       Configure the dma interrupt priority.
  \param[in]   sel DMA channel to be configured (\ref DMA_SEL_t)
  \param[in]   cfg  Pointer to \ref DMA_PRI_CFG
  \return      \ref execution_status

  \fn          uint32_t DMA_CreateConfigWord (const DMA_CFG_t *cfg)
  \brief       Create dma channel configuration word
  \param[in]   cfg  Pointer to \ref DMA_CFG_t
  \return      \ref configuration word

  \fn          int32_t DMA_SetConfigWord (DMA_SEL_t sel, uint32_t cfg)
  \brief       Quickly updates the dma channel configuration
  \param[in]   sel DMA channel to be configured (\ref DMA_SEL_t)
  \param[in]   cfg  configuration word
  \return      none

  \fn          int32_t DMA_Start (DMA_SEL_t sel)
  \brief       Starts the dma transfer.
  \param[in]   sel DMA channel number to be used (\ref DMA_SEL_t)
  \return      \ref execution_status

  \fn          int32_t DMA_Stop (DMA_SEL_t sel)
  \brief       Stops the dma transfer.
  \param[in]   sel DMA channel number to be used(\ref DMA_SEL_t)
  \return      \ref execution_status

  \fn          unt32_t DMA_GetCounterValue (DMA_SEL_t sel)
  \brief       Returns the current counter value of dma channel.
  \param[in]   sel DMA channel value to be read (\ref DMA_SEL_t)
  \return      \ref dma channel counter value

  \fn          DMA_STATE_t DMA_GetStatus (DMA_SEL_t sel)
  \brief       Returns the dma channel status. Clears the status register
               on read.
  \param[in]   sel DMA channel value to be read (\ref DMA_SEL_t)
  \return      dma channel status (\ref DMA_STATUS_t)

  \fn          void DMA_SignalEvent (DMA_EVENT_t event)
  \brief       Signal DMA events.
  \param[in]   event notification mask (\ref DMA_EVENT)
  \return      none
*/

typedef void (*DMA_SignalEvent_t) (uint32_t event);  ///< Pointer to \ref DMA_SignalEvent : signal timer event.

/**
\brief DMA channel configuration.
*/
typedef struct _DMA_CFG_t
{
    DMA_TRG_t            src_sel       :4;     ///< dma source target
    DMA_STEP_MODE_t      src_step_mode :2;     ///< source step mode
    DMA_STEP_SIZE_t      src_step_size :2;     ///< source step size
    DMA_WORD_SIZE_t      src_word_size :2;     ///< source word size
    DMA_TRG_t            dst_sel       :4;     ///< dma destination target
    DMA_STEP_MODE_t      dst_step_mode :2;     ///< destination step mode
    DMA_STEP_SIZE_t      dst_step_size :2;     ///< destination step size
    DMA_WORD_SIZE_t      dst_word_size :2;     ///< destination word size
    DMA_BYTE_ORDER_t     byte_order    :1;     ///< byte order
    DMA_CH_PRI_t         ch_priority   :2;     ///< channel priority
    DMA_DATA_MODE_t      data_mode     :1;     ///< data transfer mode
    uint32_t                           :10;    ///< reserved
} DMA_CFG_t;

/**
\brief DMA src / dst address configuration.
*/
typedef struct _DMA_ADDR_CFG_t
{
    volatile const void *src_addr;             ///< source address
    volatile void       *dst_addr;             ///< destination address
    uint32_t             counter_len    :16;   ///< value which when reached triggers the counter event
    uint32_t             transfer_len   :16;   ///< dma transfer length
} DMA_ADDR_CFG_t;

/**
\brief DMA interrupt priority configuration.
*/
typedef struct _DMA_PRI_CFG_t
{
    uint32_t             preempt_pri     :3;   ///< preempt priority
    uint32_t                             :13;  ///< reserved
    uint32_t             subgrp_pri      :3;   ///< subgroup priority
    uint32_t                             :13;  ///< reserved
} DMA_PRI_CFG_t;

/**
\brief DMA status.
*/
typedef struct _DMA_STATUS_t
{
    uint32_t             started         :1;   ///< transfer was started
    uint32_t             completed       :1;   ///< transfer was completed
    uint32_t             counter_reached :1;   ///< counter value was reached
    uint32_t             disabled        :1;   ///< dma disabled
    uint32_t             error           :1;   ///< error occured
    uint32_t                             :27;  ///< reserved
} DMA_STATUS_t;

/**
\brief Access structure of the DMA Driver.
*/
typedef struct _DRIVER_DMA_t {
    ARM_DRIVER_VERSION (*GetVersion)           (void);                                      ///< Pointer to \ref DMA_GetVersion : Get driver version.
    int32_t            (*Initialize)           (DMA_SignalEvent_t cb_event);                ///< Pointer to \ref DMA_Initialize : Initialize dma driver.
    int32_t            (*Configure)            (DMA_SEL_t sel, const DMA_CFG_t * cfg,
                                                DMA_SignalEvent_t cb);                      ///< Pointer to \ref DMA_Configure : Configure dma channel.
    int32_t            (*ConfigureWord)        (DMA_SEL_t sel, uint32_t cfg,
                                                DMA_SignalEvent_t cb);                      ///< Pointer to \ref DMA_ConfigureWord : Configure dma channel.
    int32_t            (*ConfigureAddr)        (DMA_SEL_t sel, const DMA_ADDR_CFG_t * pri); ///< Pointer to \ref DMA_ConfigureAddr : Configure dma interrupt priority.
    int32_t            (*SetInterruptPriority) (DMA_SEL_t sel, const DMA_PRI_CFG_t * pri);  ///< Pointer to \ref DMA_SetInterruptPriority : Configure dma interrupt priority.
    uint32_t           (*CreateConfigWord)     (const DMA_CFG_t * cfg);                     ///< Pointer to \ref DMA_CreateConfigWord : Create dma channel configuration word..
    void               (*SetConfigWord)        (DMA_SEL_t sel, uint32_t cfg);               ///< Pointer to \ref DMA_SetConfigWord : Quickly update dma channel configuration word.
    int32_t            (*Start)                (DMA_SEL_t sel);                             ///< Pointer to \ref DMA_Start : Start dma transfer.
    int32_t            (*Stop)                 (DMA_SEL_t sel);                             ///< Pointer to \ref DMA_Stop : Stop dma transfer.
    uint32_t           (*GetCounterValue)      (DMA_SEL_t sel);                             ///< Pointer to \ref DMA_GetCounterValue : Get the current channel transfer counter.
    DMA_STATUS_t       (*GetStatus)            (DMA_SEL_t sel);                             ///< Pointer to \ref DMA_GetStatus: Returns dma channel status.
} const DRIVER_DMA_t;

#ifdef  __cplusplus
}
#endif

#endif /* DRIVER_DMA_H_ */
