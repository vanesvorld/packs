/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * DMA_RSLxx.h
 * - DMA driver header file for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#ifndef DMA_RSLXX_H
#define DMA_RSLXX_H

#include <RTE_Device.h>
#include <rsl10.h>
#include <Driver_DMA.h>

#if (!RTE_DMA)
  #error "DMA not configured in RTE_Device.h!"
#endif    /* if (!RTE_DMA) */

/* DMA interrupt handlers prototypes */
#if (RTE_DMA0_EN)
void DMA0_IRQHandler(void);
#endif
#if (RTE_DMA1_EN)
void DMA1_IRQHandler(void);
#endif
#if (RTE_DMA2_EN)
void DMA2_IRQHandler(void);
#endif
#if (RTE_DMA3_EN)
void DMA3_IRQHandler(void);
#endif
#if (RTE_DMA4_EN)
void DMA4_IRQHandler(void);
#endif
#if (RTE_DMA5_EN)
void DMA5_IRQHandler(void);
#endif
#if (RTE_DMA6_EN)
void DMA6_IRQHandler(void);
#endif
#if (RTE_DMA7_EN)
void DMA7_IRQHandler(void);
#endif

/* DMA channels number */
#define DMA_CHANNELS_NUMBER     8

/* DMA flags */
#define DMA_FLAG_BIT_SET       1U
#define DMA_FLAG_BIT_CLR       0U
#define DMA_STATUS_REG_CLR    ((1U << DMA_STATUS_ERROR_INT_CLEAR_Pos)    | \
                               (1U << DMA_STATUS_COMPLETE_INT_CLEAR_Pos) | \
                               (1U << DMA_STATUS_COUNTER_INT_CLEAR_Pos)  | \
                               (1U << DMA_STATUS_START_INT_CLEAR_Pos)    | \
                               (1U << DMA_STATUS_DISABLE_INT_CLEAR_Pos))

/* DMA address inc mode mask */
#define DMA_ADDR_INC_MSK       1U

/* DMA enabled driver mask */
#define DMA_EN_MSK            ((RTE_DMA0_EN  << DMA_CH_0) | \
                               (RTE_DMA1_EN  << DMA_CH_1) | \
                               (RTE_DMA2_EN  << DMA_CH_2) | \
                               (RTE_DMA3_EN  << DMA_CH_3) | \
                               (RTE_DMA4_EN  << DMA_CH_4) | \
                               (RTE_DMA5_EN  << DMA_CH_5) | \
                               (RTE_DMA6_EN  << DMA_CH_6) | \
                               (RTE_DMA7_EN  << DMA_CH_7))

/* DMA auto configuration enabled driver mask */
#define DMA_EN_AUTO_MSK      (((RTE_DMA0_AUTO_EN  << DMA_CH_0)  | \
                               (RTE_DMA1_AUTO_EN  << DMA_CH_1)  | \
                               (RTE_DMA2_AUTO_EN  << DMA_CH_2)  | \
                               (RTE_DMA3_AUTO_EN  << DMA_CH_3)  | \
                               (RTE_DMA4_AUTO_EN  << DMA_CH_4)  | \
                               (RTE_DMA5_AUTO_EN  << DMA_CH_5)  | \
                               (RTE_DMA6_AUTO_EN  << DMA_CH_6)  | \
                               (RTE_DMA7_AUTO_EN  << DMA_CH_7)) & \
                                DMA_EN_MSK)

/* DMA interrupts info */
typedef struct _DMA_INT_INFO_t
{
    const IRQn_Type       irqn[DMA_CHANNELS_NUMBER];                  /* dma irqs numbers */
    DMA_SignalEvent_t     cb[DMA_CHANNELS_NUMBER];                    /* dma channels handler functions */
} DMA_INT_INFO_t;

/* DMA runtime info */
typedef struct _DMA_INFO_t
{
    const DMA_CFG_t      *const default_cfg[DMA_CHANNELS_NUMBER];     /* dma default configuration */
    const DMA_PRI_CFG_t  *const default_pri_cfg[DMA_CHANNELS_NUMBER]; /* dma priorities default configuration */
    uint8_t               flags;                                      /* dma state */
} DMA_INFO_t;

/* DMA Resources definition */
typedef struct
{
    DMA_INFO_t           *info;                                       /* run-time info */
    DMA_INT_INFO_t        intInfo;                                    /* IRQs Info */
} DMA_RESOURCES_t;

#endif /* DMA_RSLXX_H */
