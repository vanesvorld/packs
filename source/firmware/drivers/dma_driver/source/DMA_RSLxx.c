/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * DMA_RSLxx.c
 * - DMA driver implementation for RSLxx family of devices
 * ------------------------------------------------------------------------- */

#include <DMA_RSLxx.h>

#if RTE_DMA

/* Driver Version Macro */
#define ARM_DMA_DRV_VERSION    ARM_DRIVER_VERSION_MAJOR_MINOR(0, 1)

/* Driver Version */
static const ARM_DRIVER_VERSION DriverVersion =
{
    ARM_DMA_API_VERSION,
    ARM_DMA_DRV_VERSION
};

#if (RTE_DMA0_EN & RTE_DMA0_AUTO_EN)

/* Set default dma channel 0 configuration */
static const DMA_CFG_t DMA0_DefaultCfg = {                  /* dma channel 0 default configuration */
    .src_sel         = RTE_DMA0_SRC_SEL_DEFAULT,            /* dma channel 0 default src target */
    .src_step_mode   = RTE_DMA0_SRC_STEP_MODE_DEFAULT,      /* dma channel 0 default src step mode */
    .src_step_size   = RTE_DMA0_SRC_STEP_SIZE_DEFAULT,      /* dma channel 0 default src step size */
    .src_word_size   = RTE_DMA0_SRC_WORD_SIZE_DEFAULT,      /* dma channel 0 default src step word */
    .dst_sel         = RTE_DMA0_DST_SEL_DEFAULT,            /* dma channel 0 default dst target */
    .dst_step_mode   = RTE_DMA0_DST_STEP_MODE_DEFAULT,      /* dma channel 0 default dst step mode */
    .dst_step_size   = RTE_DMA0_DST_STEP_SIZE_DEFAULT,      /* dma channel 0 default dst step size */
    .dst_word_size   = RTE_DMA0_DST_WORD_SIZE_DEFAULT,      /* dma channel 0 default dst step word */
    .byte_order      = RTE_DMA0_BYTE_ORDER_DEFAULT,         /* dma channel 0 default endianness */
    .ch_priority     = RTE_DMA0_CHANNEL_PRIORITY_DEFAULT,   /* dma channel 0 default priority */
    .data_mode       = RTE_DMA0_DATA_MODE_DEFAULT           /* dma channel 0 default data transfer mode */
};
static const DMA_PRI_CFG_t DMA0_DefaultPri = {              /* dma channel 0 default interrupt priority configuration */
    .preempt_pri     = RTE_DMA0_INT_PREEMPT_PRI,            /* dma channel 0 default pre-empt priority value */
    .subgrp_pri      = RTE_DMA0_INT_SUBGRP_PRI              /* dma channel 0 default subgroup priority value */
};
#endif /* if (RTE_DMA0_EN & RTE_DMA0_AUTO_EN) */

#if (RTE_DMA1_EN & RTE_DMA1_AUTO_EN)

/* Set default dma channel 1 configuration */
static const DMA_CFG_t DMA1_DefaultCfg = {                    /* dma channel 1 default configuration */
    .src_sel         = RTE_DMA1_SRC_SEL_DEFAULT,            /* dma channel 1 default src target */
    .src_step_mode   = RTE_DMA1_SRC_STEP_MODE_DEFAULT,      /* dma channel 1 default src step mode */
    .src_step_size   = RTE_DMA1_SRC_STEP_SIZE_DEFAULT,      /* dma channel 1 default src step size */
    .src_word_size   = RTE_DMA1_SRC_WORD_SIZE_DEFAULT,      /* dma channel 1 default src step word */
    .dst_sel         = RTE_DMA1_DST_SEL_DEFAULT,            /* dma channel 1 default dst target */
    .dst_step_mode   = RTE_DMA1_DST_STEP_MODE_DEFAULT,      /* dma channel 1 default dst step mode */
    .dst_step_size   = RTE_DMA1_DST_STEP_SIZE_DEFAULT,      /* dma channel 1 default dst step size */
    .dst_word_size   = RTE_DMA1_DST_WORD_SIZE_DEFAULT,      /* dma channel 1 default dst step word */
    .byte_order      = RTE_DMA1_BYTE_ORDER_DEFAULT,         /* dma channel 1 default endianness */
    .ch_priority     = RTE_DMA1_CHANNEL_PRIORITY_DEFAULT,   /* dma channel 1 default priority */
    .data_mode       = RTE_DMA1_DATA_MODE_DEFAULT           /* dma channel 1 default data transfer mode */
};
static const DMA_PRI_CFG_t DMA1_DefaultPri = {                /* dma channel 1 default interrupt priority configuration */
    .preempt_pri     = RTE_DMA1_INT_PREEMPT_PRI,            /* dma channel 1 default pre-empt priority value */
    .subgrp_pri      = RTE_DMA1_INT_SUBGRP_PRI              /* dma channel 1 default subgroup priority value */
};
#endif /* if (RTE_DMA1_EN & RTE_DMA1_AUTO_EN) */

#if (RTE_DMA2_EN & RTE_DMA2_AUTO_EN)

/* Set default dma channel 2 configuration */
static const DMA_CFG_t DMA2_DefaultCfg = {                    /* dma channel 2 default configuration */
    .src_sel         = RTE_DMA2_SRC_SEL_DEFAULT,            /* dma channel 2 default src target */
    .src_step_mode   = RTE_DMA2_SRC_STEP_MODE_DEFAULT,      /* dma channel 2 default src step mode */
    .src_step_size   = RTE_DMA2_SRC_STEP_SIZE_DEFAULT,      /* dma channel 2 default src step size */
    .src_word_size   = RTE_DMA2_SRC_WORD_SIZE_DEFAULT,      /* dma channel 2 default src step word */
    .dst_sel         = RTE_DMA2_DST_SEL_DEFAULT,            /* dma channel 2 default dst target */
    .dst_step_mode   = RTE_DMA2_DST_STEP_MODE_DEFAULT,      /* dma channel 2 default dst step mode */
    .dst_step_size   = RTE_DMA2_DST_STEP_SIZE_DEFAULT,      /* dma channel 2 default dst step size */
    .dst_word_size   = RTE_DMA2_DST_WORD_SIZE_DEFAULT,      /* dma channel 2 default dst step word */
    .byte_order      = RTE_DMA2_BYTE_ORDER_DEFAULT,         /* dma channel 2 default endianness */
    .ch_priority     = RTE_DMA2_CHANNEL_PRIORITY_DEFAULT,   /* dma channel 2 default priority */
    .data_mode       = RTE_DMA2_DATA_MODE_DEFAULT           /* dma channel 2 default data transfer mode */
};
static const DMA_PRI_CFG_t DMA2_DefaultPri = {                /* dma channel 2 default interrupt priority configuration */
    .preempt_pri     = RTE_DMA2_INT_PREEMPT_PRI,            /* dma channel 2 default pre-empt priority value */
    .subgrp_pri      = RTE_DMA2_INT_SUBGRP_PRI              /* dma channel 2 default subgroup priority value */
};
#endif /* if (RTE_DMA2_EN & RTE_DMA2_AUTO_EN) */

#if (RTE_DMA3_EN & RTE_DMA3_AUTO_EN)

/* Set default dma channel 3 configuration */
static const DMA_CFG_t DMA3_DefaultCfg = {                    /* dma channel 3 default configuration */
    .src_sel         = RTE_DMA3_SRC_SEL_DEFAULT,            /* dma channel 3 default src target */
    .src_step_mode   = RTE_DMA3_SRC_STEP_MODE_DEFAULT,      /* dma channel 3 default src step mode */
    .src_step_size   = RTE_DMA3_SRC_STEP_SIZE_DEFAULT,      /* dma channel 3 default src step size */
    .src_word_size   = RTE_DMA3_SRC_WORD_SIZE_DEFAULT,      /* dma channel 3 default src step word */
    .dst_sel         = RTE_DMA3_DST_SEL_DEFAULT,            /* dma channel 3 default dst target */
    .dst_step_mode   = RTE_DMA3_DST_STEP_MODE_DEFAULT,      /* dma channel 3 default dst step mode */
    .dst_step_size   = RTE_DMA3_DST_STEP_SIZE_DEFAULT,      /* dma channel 3 default dst step size */
    .dst_word_size   = RTE_DMA3_DST_WORD_SIZE_DEFAULT,      /* dma channel 3 default dst step word */
    .byte_order      = RTE_DMA3_BYTE_ORDER_DEFAULT,         /* dma channel 3 default endianness */
    .ch_priority     = RTE_DMA3_CHANNEL_PRIORITY_DEFAULT,   /* dma channel 3 default priority */
    .data_mode       = RTE_DMA3_DATA_MODE_DEFAULT           /* dma channel 3 default data transfer mode */
};
static const DMA_PRI_CFG_t DMA3_DefaultPri = {                /* dma channel 3 default interrupt priority configuration */
    .preempt_pri     = RTE_DMA3_INT_PREEMPT_PRI,            /* dma channel 3 default pre-empt priority value */
    .subgrp_pri      = RTE_DMA3_INT_SUBGRP_PRI              /* dma channel 3 default subgroup priority value */
};
#endif /* if (RTE_DMA3_EN & RTE_DMA3_AUTO_EN) */

#if (RTE_DMA4_EN & RTE_DMA4_AUTO_EN)

/* Set default dma channel 4 configuration */
static const DMA_CFG_t DMA4_DefaultCfg = {                    /* dma channel 4 default configuration */
    .src_sel         = RTE_DMA4_SRC_SEL_DEFAULT,            /* dma channel 4 default src target */
    .src_step_mode   = RTE_DMA4_SRC_STEP_MODE_DEFAULT,      /* dma channel 4 default src step mode */
    .src_step_size   = RTE_DMA4_SRC_STEP_SIZE_DEFAULT,      /* dma channel 4 default src step size */
    .src_word_size   = RTE_DMA4_SRC_WORD_SIZE_DEFAULT,      /* dma channel 4 default src step word */
    .dst_sel         = RTE_DMA4_DST_SEL_DEFAULT,            /* dma channel 4 default dst target */
    .dst_step_mode   = RTE_DMA4_DST_STEP_MODE_DEFAULT,      /* dma channel 4 default dst step mode */
    .dst_step_size   = RTE_DMA4_DST_STEP_SIZE_DEFAULT,      /* dma channel 4 default dst step size */
    .dst_word_size   = RTE_DMA4_DST_WORD_SIZE_DEFAULT,      /* dma channel 4 default dst step word */
    .byte_order      = RTE_DMA4_BYTE_ORDER_DEFAULT,         /* dma channel 4 default endianness */
    .ch_priority     = RTE_DMA4_CHANNEL_PRIORITY_DEFAULT,   /* dma channel 4 default priority */
    .data_mode       = RTE_DMA4_DATA_MODE_DEFAULT           /* dma channel 4 default data transfer mode */
};
static const DMA_PRI_CFG_t DMA4_DefaultPri = {                /* dma channel 4 default interrupt priority configuration */
    .preempt_pri     = RTE_DMA4_INT_PREEMPT_PRI,            /* dma channel 4 default pre-empt priority value */
    .subgrp_pri      = RTE_DMA4_INT_SUBGRP_PRI              /* dma channel 4 default subgroup priority value */
};
#endif /* if (RTE_DMA4_EN & RTE_DMA4_AUTO_EN) */

#if (RTE_DMA5_EN & RTE_DMA5_AUTO_EN)

/* Set default dma channel 5 configuration */
static const DMA_CFG_t DMA5_DefaultCfg = {                    /* dma channel 5 default configuration */
    .src_sel         = RTE_DMA5_SRC_SEL_DEFAULT,            /* dma channel 5 default src target */
    .src_step_mode   = RTE_DMA5_SRC_STEP_MODE_DEFAULT,      /* dma channel 5 default src step mode */
    .src_step_size   = RTE_DMA5_SRC_STEP_SIZE_DEFAULT,      /* dma channel 5 default src step size */
    .src_word_size   = RTE_DMA5_SRC_WORD_SIZE_DEFAULT,      /* dma channel 5 default src step word */
    .dst_sel         = RTE_DMA5_DST_SEL_DEFAULT,            /* dma channel 5 default dst target */
    .dst_step_mode   = RTE_DMA5_DST_STEP_MODE_DEFAULT,      /* dma channel 5 default dst step mode */
    .dst_step_size   = RTE_DMA5_DST_STEP_SIZE_DEFAULT,      /* dma channel 5 default dst step size */
    .dst_word_size   = RTE_DMA5_DST_WORD_SIZE_DEFAULT,      /* dma channel 5 default dst step word */
    .byte_order      = RTE_DMA5_BYTE_ORDER_DEFAULT,         /* dma channel 5 default endianness */
    .ch_priority     = RTE_DMA5_CHANNEL_PRIORITY_DEFAULT,   /* dma channel 5 default priority */
    .data_mode       = RTE_DMA5_DATA_MODE_DEFAULT           /* dma channel 5 default data transfer mode */
};
static const DMA_PRI_CFG_t DMA5_DefaultPri = {                /* dma channel 5 default interrupt priority configuration */
    .preempt_pri     = RTE_DMA5_INT_PREEMPT_PRI,            /* dma channel 5 default pre-empt priority value */
    .subgrp_pri      = RTE_DMA5_INT_SUBGRP_PRI              /* dma channel 5 default subgroup priority value */
};
#endif /* if (RTE_DMA5_EN & RTE_DMA5_AUTO_EN) */

#if (RTE_DMA6_EN & RTE_DMA6_AUTO_EN)

/* Set default dma channel 6 configuration */
static const DMA_CFG_t DMA6_DefaultCfg = {                    /* dma channel 6 default configuration */
    .src_sel         = RTE_DMA6_SRC_SEL_DEFAULT,            /* dma channel 6 default src target */
    .src_step_mode   = RTE_DMA6_SRC_STEP_MODE_DEFAULT,      /* dma channel 6 default src step mode */
    .src_step_size   = RTE_DMA6_SRC_STEP_SIZE_DEFAULT,      /* dma channel 6 default src step size */
    .src_word_size   = RTE_DMA6_SRC_WORD_SIZE_DEFAULT,      /* dma channel 6 default src step word */
    .dst_sel         = RTE_DMA6_DST_SEL_DEFAULT,            /* dma channel 6 default dst target */
    .dst_step_mode   = RTE_DMA6_DST_STEP_MODE_DEFAULT,      /* dma channel 6 default dst step mode */
    .dst_step_size   = RTE_DMA6_DST_STEP_SIZE_DEFAULT,      /* dma channel 6 default dst step size */
    .dst_word_size   = RTE_DMA6_DST_WORD_SIZE_DEFAULT,      /* dma channel 6 default dst step word */
    .byte_order      = RTE_DMA6_BYTE_ORDER_DEFAULT,         /* dma channel 6 default endianness */
    .ch_priority     = RTE_DMA6_CHANNEL_PRIORITY_DEFAULT,   /* dma channel 6 default priority */
    .data_mode       = RTE_DMA6_DATA_MODE_DEFAULT           /* dma channel 6 default data transfer mode */
};
static const DMA_PRI_CFG_t DMA6_DefaultPri = {                /* dma channel 6 default interrupt priority configuration */
    .preempt_pri     = RTE_DMA6_INT_PREEMPT_PRI,            /* dma channel 6 default pre-empt priority value */
    .subgrp_pri      = RTE_DMA6_INT_SUBGRP_PRI              /* dma channel 6 default subgroup priority value */
};
#endif /* if (RTE_DMA6_EN & RTE_DMA6_AUTO_EN) */

#if (RTE_DMA7_EN & RTE_DMA7_AUTO_EN)

/* Set default dma channel 7 configuration */
static const DMA_CFG_t DMA7_DefaultCfg = {                    /* dma channel 7 default configuration */
    .src_sel         = RTE_DMA7_SRC_SEL_DEFAULT,            /* dma channel 7 default src target */
    .src_step_mode   = RTE_DMA7_SRC_STEP_MODE_DEFAULT,      /* dma channel 7 default src step mode */
    .src_step_size   = RTE_DMA7_SRC_STEP_SIZE_DEFAULT,      /* dma channel 7 default src step size */
    .src_word_size   = RTE_DMA7_SRC_WORD_SIZE_DEFAULT,      /* dma channel 7 default src step word */
    .dst_sel         = RTE_DMA7_DST_SEL_DEFAULT,            /* dma channel 7 default dst target */
    .dst_step_mode   = RTE_DMA7_DST_STEP_MODE_DEFAULT,      /* dma channel 7 default dst step mode */
    .dst_step_size   = RTE_DMA7_DST_STEP_SIZE_DEFAULT,      /* dma channel 7 default dst step size */
    .dst_word_size   = RTE_DMA7_DST_WORD_SIZE_DEFAULT,      /* dma channel 7 default dst step word */
    .byte_order      = RTE_DMA7_BYTE_ORDER_DEFAULT,         /* dma channel 7 default endianness */
    .ch_priority     = RTE_DMA7_CHANNEL_PRIORITY_DEFAULT,   /* dma channel 7 default priority */
    .data_mode       = RTE_DMA7_DATA_MODE_DEFAULT           /* dma channel 7 default data transfer mode */
};
static const DMA_PRI_CFG_t DMA7_DefaultPri = {                /* dma channel 7 default interrupt priority configuration */
    .preempt_pri     = RTE_DMA7_INT_PREEMPT_PRI,            /* dma channel 7 default pre-empt priority value */
    .subgrp_pri      = RTE_DMA7_INT_SUBGRP_PRI              /* dma channel 7 default subgroup priority value */
};
#endif /* if (RTE_DMA7_EN & RTE_DMA7_AUTO_EN) */

/* DMA run-time information */
static DMA_INFO_t DMA_Info = {
    .flags = 0,                                             /* dma flags */
#if (RTE_DMA0_EN & RTE_DMA0_AUTO_EN)
    .default_cfg[DMA_CH_0]     = &DMA0_DefaultCfg,          /* dma channel 0 default configuration */
    .default_pri_cfg[DMA_CH_0] = &DMA0_DefaultPri,          /* dma channel 0 default interrupt priorities */
#endif /* if (RTE_DMA0_EN & RTE_DMA0_AUTO_EN) */
#if (RTE_DMA1_EN & RTE_DMA1_AUTO_EN)
    .default_cfg[DMA_CH_1]     = &DMA1_DefaultCfg,          /* dma channel 1 default configuration */
    .default_pri_cfg[DMA_CH_1] = &DMA1_DefaultPri,          /* dma channel 1 default interrupt priorities */
#endif /* if (RTE_DMA1_EN & RTE_DMA1_AUTO_EN) */
#if (RTE_DMA2_EN & RTE_DMA2_AUTO_EN)
    .default_cfg[DMA_CH_2]     = &DMA2_DefaultCfg,          /* dma channel 2 default configuration */
    .default_pri_cfg[DMA_CH_2] = &DMA2_DefaultPri,          /* dma channel 2 default interrupt priorities */
#endif /* if (RTE_DMA2_EN & RTE_DMA2_AUTO_EN) */
#if (RTE_DMA3_EN & RTE_DMA3_AUTO_EN)
    .default_cfg[DMA_CH_3]     = &DMA3_DefaultCfg,          /* dma channel 3 default configuration */
    .default_pri_cfg[DMA_CH_3] = &DMA3_DefaultPri,          /* dma channel 3 default interrupt priorities */
#endif /* if (RTE_DMA3_EN & RTE_DMA3_AUTO_EN) */
#if (RTE_DMA4_EN & RTE_DMA4_AUTO_EN)
    .default_cfg[DMA_CH_4]     = &DMA4_DefaultCfg,          /* dma channel 4 default configuration */
    .default_pri_cfg[DMA_CH_4] = &DMA4_DefaultPri,          /* dma channel 4 default interrupt priorities */
#endif /* if (RTE_DMA4_EN & RTE_DMA4_AUTO_EN) */
#if (RTE_DMA5_EN & RTE_DMA5_AUTO_EN)
    .default_cfg[DMA_CH_5]     = &DMA5_DefaultCfg,          /* dma channel 5 default configuration */
    .default_pri_cfg[DMA_CH_5] = &DMA5_DefaultPri,          /* dma channel 5 default interrupt priorities */
#endif /* if (RTE_DMA5_EN & RTE_DMA5_AUTO_EN) */
#if (RTE_DMA6_EN & RTE_DMA6_AUTO_EN)
    .default_cfg[DMA_CH_6]     = &DMA6_DefaultCfg,          /* dma channel 6 default configuration */
    .default_pri_cfg[DMA_CH_6] = &DMA6_DefaultPri,          /* dma channel 6 default interrupt priorities */
#endif /* if (RTE_DMA6_EN & RTE_DMA6_AUTO_EN) */
#if (RTE_DMA7_EN & RTE_DMA7_AUTO_EN)
    .default_cfg[DMA_CH_7]     = &DMA7_DefaultCfg,          /* dma channel 7 default configuration */
    .default_pri_cfg[DMA_CH_7] = &DMA7_DefaultPri,          /* dma channel 7 default interrupt priorities */
#endif /* if (RTE_DMA7_EN & RTE_DMA7_AUTO_EN) */
};

/* DMA resources */
static DMA_RESOURCES_t DMA_Resources =
{
    .info = &DMA_Info,                                      /* dma run-time information */
    .intInfo = {                                            /* dma interrupt info */
        .irqn[DMA_CH_0]    = DMA0_IRQn,                     /* dma channel 0 interrupt number */
        .irqn[DMA_CH_1]    = DMA1_IRQn,                     /* dma channel 1 interrupt number */
        .irqn[DMA_CH_2]    = DMA2_IRQn,                     /* dma channel 2 interrupt number */
        .irqn[DMA_CH_3]    = DMA3_IRQn,                     /* dma channel 3 interrupt number */
        .irqn[DMA_CH_4]    = DMA4_IRQn,                     /* dma channel 4 interrupt number */
        .irqn[DMA_CH_5]    = DMA5_IRQn,                     /* dma channel 5 interrupt number */
        .irqn[DMA_CH_6]    = DMA6_IRQn,                     /* dma channel 6 interrupt number */
        .irqn[DMA_CH_7]    = DMA7_IRQn,                     /* dma channel 7 interrupt number */
        .cb[DMA_CH_0]      = 0,                             /* dma channel 0 interrupt handler */
        .cb[DMA_CH_1]      = 0,                             /* dma channel 1 interrupt handler */
        .cb[DMA_CH_2]      = 0,                             /* dma channel 2 interrupt handler */
        .cb[DMA_CH_3]      = 0,                             /* dma channel 3 interrupt handler */
        .cb[DMA_CH_4]      = 0,                             /* dma channel 4 interrupt handler */
        .cb[DMA_CH_5]      = 0,                             /* dma channel 5 interrupt handler */
        .cb[DMA_CH_6]      = 0,                             /* dma channel 6 interrupt handler */
        .cb[DMA_CH_7]      = 0,                             /* dma channel 7 interrupt handler */
    }
};

/* ----------------------------------------------------------------------------
 * Function      : void DMA_GetVersion (void)
 * ----------------------------------------------------------------------------
 * Description   : Driver version
 * Inputs        : None
 * Outputs       : ARM_DRIVER_VERSION
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_DRIVER_VERSION DMA_GetVersion (void)
{
    return DriverVersion;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t DMA_CreateConfigWord (const DMA_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Prepare configuration word which can be used for quick dma
 *               : configuration update.
 * Inputs        : cfg      - dma channel configuration
 * Outputs       : configuration word
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t DMA_CreateConfigWord (const DMA_CFG_t *cfg)
{
    /* Configuration word to be returned */
    uint32_t cfgWord;

    /* Check the transfer type */
    if ((cfg->src_sel == DMA_TRG_MEM) && (cfg->dst_sel == DMA_TRG_MEM))
    {
        /* Set memory to memory transfer */
        cfgWord = DMA_TRANSFER_M_TO_M;
    }
    else if (cfg->src_sel == DMA_TRG_MEM)
    {
        /* Set memory to peripheral transfer */
        cfgWord = DMA_TRANSFER_M_TO_P;
    }
    else if (cfg->dst_sel == DMA_TRG_MEM)
    {
        /* Set peripheral to memory transfer */
        cfgWord = DMA_TRANSFER_P_TO_M;
    }
    else
    {
        /* Set peripheral to peripheral transfer */
        cfgWord = DMA_TRANSFER_P_TO_P;
    }

    /* Set the rest of the configuration word */
    cfgWord |= ((cfg->src_sel << DMA_CTRL0_SRC_SELECT_Pos) & DMA_CTRL0_SRC_SELECT_Mask)       |
               (cfg->src_step_mode == 2 ? DMA_SRC_ADDR_STATIC : DMA_SRC_ADDR_INC)             |
               ((cfg->src_step_mode & DMA_ADDR_INC_MSK) << DMA_CTRL0_SRC_ADDR_STEP_MODE_Pos)  |
               (cfg->src_step_size << DMA_CTRL0_SRC_ADDR_STEP_SIZE_Pos)                       |
               (cfg->src_word_size << DMA_CTRL0_SRC_WORD_SIZE_Pos)                            |
               ((cfg->dst_sel << DMA_CTRL0_DEST_SELECT_Pos) & DMA_CTRL0_DEST_SELECT_Mask)     |
               (cfg->dst_step_mode == 2 ? DMA_DEST_ADDR_STATIC : DMA_DEST_ADDR_INC)           |
               ((cfg->dst_step_mode & DMA_ADDR_INC_MSK) << DMA_CTRL0_DEST_ADDR_STEP_MODE_Pos) |
               (cfg->dst_step_size << DMA_CTRL0_DEST_ADDR_STEP_SIZE_Pos)                      |
               (cfg->dst_word_size << DMA_CTRL0_DEST_WORD_SIZE_Pos)                           |
               (cfg->byte_order    << DMA_CTRL0_BYTE_ORDER_Pos)                               |
               (cfg->ch_priority   << DMA_CTRL0_CHANNEL_PRIORITY_Pos)                         |
               (cfg->data_mode     << DMA_CTRL0_ADDR_MODE_Pos)                                |
               (DMA_FLAG_BIT_CLR   << DMA_CTRL0_DISABLE_INT_ENABLE_Pos)                       |
               (DMA_FLAG_BIT_SET   << DMA_CTRL0_START_INT_ENABLE_Pos)                         |
               (DMA_FLAG_BIT_SET   << DMA_CTRL0_ERROR_INT_ENABLE_Pos)                         |
               (DMA_FLAG_BIT_SET   << DMA_CTRL0_COMPLETE_INT_ENABLE_Pos)                      |
               (DMA_FLAG_BIT_SET   << DMA_CTRL0_COUNTER_INT_ENABLE_Pos);

    /* Return configuration word */
    return cfgWord;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t DMA_SetConfigWord (DMA_SEL_t sel, uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Quickly update the dma configuration.
 * Inputs        : sel      - dma channel to be configured
 *               : cfg      - dma channel configuration
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void DMA_SetConfigWord (DMA_SEL_t sel, uint32_t cfg)
{
    /* Set the configuration word */
    DMA->CTRL0[sel] = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t DMA_Configure (DMA_SEL_t sel, const DMA_CFG_t *cfg, DMA_SignalEvent_t cb)
 * ----------------------------------------------------------------------------
 * Description   : Configure dma channel
 * Inputs        : sel      - dma channel to be configured
 *               : cfg      - dma channel configuration
 *               : cb       - dma channel event callback function
 * Outputs       : ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t DMA_Configure (DMA_SEL_t sel, const DMA_CFG_t *cfg, DMA_SignalEvent_t cb)
{
    /* Check if correct dma channel was selected */
    if (!((DMA_FLAG_BIT_SET << sel) & DMA_EN_MSK))
    {
        /* Return error - channel is unsupported */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Set the configuration word */
    DMA_SetConfigWord(sel, DMA_CreateConfigWord(cfg));

    /* Set the callback function */
    DMA_Resources.intInfo.cb[sel] = cb;

    /* Indicate that DMAx was Initialized */
    DMA_Resources.info->flags |= DMA_FLAG_BIT_SET << sel;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t DMA_ConfigureWord (DMA_SEL_t sel, uint32_t cfg, DMA_SignalEvent_t cb)
 * ----------------------------------------------------------------------------
 * Description   : Configure dma channel using prepared configuration word
 * Inputs        : sel      - dma channel to be configured
 *               : cfg      - dma channel configuration word
 *               : cb       - dma channel event callback function
 * Outputs       : ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t DMA_ConfigureWord (DMA_SEL_t sel, uint32_t cfg, DMA_SignalEvent_t cb)
{
    /* Check if correct dma channel was selected */
    if (!((DMA_FLAG_BIT_SET << sel) & DMA_EN_MSK))
    {
        /* Return error - channel is unsupported */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Set the configuration word */
    DMA_SetConfigWord(sel, cfg);

    /* Set the callback function */
    DMA_Resources.intInfo.cb[sel] = cb;

    /* Indicate that DMAx was Initialized */
    DMA_Resources.info->flags |= DMA_FLAG_BIT_SET << sel;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t DMA_ConfigureAddr (DMA_SEL_t sel, const DMA_ADDR_CFG_t * cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure dma channel src / dst address configuration
 * Inputs        : sel      - dma channel to be configured
 *               : cfg      - src / dst address configuration configuration
 * Outputs       : ARM Driver return code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t DMA_ConfigureAddr (DMA_SEL_t sel, const DMA_ADDR_CFG_t *cfg)
{
    /* Check if correct dma channel was selected */
    if (!((DMA_FLAG_BIT_SET << sel) & DMA_EN_MSK))
    {
        /* Return error - channel is unsupported */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Configuration word to be used */
    uint32_t cfgWord;

    /* Prepare the configuration word */
    cfgWord = (cfg->counter_len  << DMA_CTRL1_COUNTER_INT_VALUE_Pos) |
              (cfg->transfer_len << DMA_CTRL1_TRANSFER_LENGTH_Pos);

    /* Set the configuration word */
    DMA->CTRL1[sel] = cfgWord;

    /* Set the source address */
    DMA->SRC_BASE_ADDR[sel] = (uint32_t)cfg->src_addr;

    /* Set the destination address */
    DMA->DEST_BASE_ADDR[sel] = (uint32_t)cfg->dst_addr;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t DMA_SetInterruptPriority(DMA_SEL_t sel, const DMA_PRI_CFG_t *cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure dma interrupt priority.
 *                 sel      - dma channel to be configured
 * Inputs        : cfg      - interrupt priority configuration
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t DMA_SetInterruptPriority(DMA_SEL_t sel, const DMA_PRI_CFG_t *cfg)
{
    /* Encoded priority value */
    uint32_t encodedPri = 0;

    /* Encode priority configuration word */
    encodedPri = NVIC_EncodePriority(NVIC_GetPriorityGrouping(), cfg->preempt_pri, cfg->subgrp_pri);

    /* Set the dma priority */
    NVIC_SetPriority(DMA_Resources.intInfo.irqn[sel], encodedPri);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : int32_t DMA_Initialize(DMA_SignalEvent_t cb)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the DMA driver
 * Inputs        : cb_event - callback function to be called on dma event
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t DMA_Initialize(DMA_SignalEvent_t cb)
{
    /* Configure each enabled dma channel */
    for (int i = 0; i < DMA_CHANNELS_NUMBER; ++i)
    {
        /* Check if particular dma channel should be initialized */
        if ((DMA_FLAG_BIT_SET << i) & DMA_EN_AUTO_MSK)
        {
            /* Check if dma channel was already configured */
            if (DMA_Resources.info->flags & (DMA_FLAG_BIT_SET << i))
            {
                continue;
            }

            /* Configure particular DMA */
            DMA_Configure(i, DMA_Resources.info->default_cfg[i], cb);

            /* Set the particuar DMA interrupts priority */
            DMA_SetInterruptPriority(i, DMA_Resources.info->default_pri_cfg[i]);

            /* Set the callback function */
            DMA_Resources.intInfo.cb[i] = cb;

            /* Indicate that DMA was Initialized */
            DMA_Resources.info->flags |= DMA_FLAG_BIT_SET << i;
        }
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * static int32_t DMA_Start (DMA_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Start the particular dma channel data transfer
 * Inputs        : sel - dma channel to be used
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t DMA_Start(DMA_SEL_t sel)
{
    /* Check if correct dma channel was selected */
    if (!((DMA_FLAG_BIT_SET << sel) & DMA_EN_MSK))
    {
        /* Return unsupported error */
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Check if dma channel was already configured */
    if (!(DMA_Resources.info->flags & (DMA_FLAG_BIT_SET << sel)))
    {
        /* Return unconfigured error */
        return DMA_ERROR_UNCONFIGURED;
    }

    /* Clear dma status register */
    DMA->STATUS[sel] = DMA_STATUS_REG_CLR;

    /* Clear pending flag */
    NVIC_ClearPendingIRQ(DMA_Resources.intInfo.irqn[sel]);

    /* Enable the interrupt */
    NVIC_EnableIRQ(DMA_Resources.intInfo.irqn[sel]);

    /* Start the DMA */
    Sys_DMA_ChannelEnable(sel);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * static int32_t DMA_Stop (DMA_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Stop the particular dma channel data transfer
 * Inputs        : sel - dma channel to be used
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t DMA_Stop(DMA_SEL_t sel)
{
    /* Check if correct DMA was selected */
    if (!((DMA_FLAG_BIT_SET << sel) & DMA_EN_MSK))
    {
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }

    /* Disable the interrupt */
    NVIC_DisableIRQ(DMA_Resources.intInfo.irqn[sel]);

    /* Clear pending flag */
    NVIC_ClearPendingIRQ(DMA_Resources.intInfo.irqn[sel]);

    /* Stop the DMA */
    Sys_DMA_ChannelDisable(sel);

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t DMA_GetCounterValue(DMA_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Return the current counter value of dma channel
 * Inputs        : sel - channel to be read.
 * Outputs       : Channel counter value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t DMA_GetCounterValue(DMA_SEL_t sel)
{
    /* Check if correct DMA was selected */
    if (!((DMA_FLAG_BIT_SET << sel) & DMA_EN_MSK))
    {
        /* Return 0 as incorrect dma channel was selected */
        return 0;
    }

    /* Return dma channel counter value */
    return DMA->WORD_CNT[sel];
}

/* ----------------------------------------------------------------------------
 * static DMA_STATUS_t DMA_GetStatus (DMA_SEL_t sel)
 * ----------------------------------------------------------------------------
 * Description   : Return the dma channel status. Clear the status register
 *                 on read
 * Inputs        : Channel number to be read
 * Outputs       : DMA channel status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static DMA_STATUS_t DMA_GetStatus(DMA_SEL_t sel)
{
    /* Get dma register status value */
    uint32_t reg = DMA->STATUS[sel];

    /* Prepare dma status */
    DMA_STATUS_t status = {
        .started         = (reg >> DMA_STATUS_START_INT_STATUS_Pos)
                           & DMA_FLAG_BIT_SET,
        .completed       = (reg >> DMA_STATUS_COMPLETE_INT_STATUS_Pos)
                           & DMA_FLAG_BIT_SET,
        .counter_reached = (reg >> DMA_STATUS_COUNTER_INT_STATUS_Pos)
                           & DMA_FLAG_BIT_SET,
        .error           = (reg >> DMA_STATUS_ERROR_INT_STATUS_Pos)
                           & DMA_FLAG_BIT_SET,
        .disabled        = (reg >> DMA_STATUS_DISABLE_INT_STATUS_Pos)
                           & DMA_FLAG_BIT_SET
    };

    /* Return dma status */
    return status;
}

#if (RTE_DMA0_EN | RTE_DMA1_EN | RTE_DMA2_EN | RTE_DMA3_EN | RTE_DMA4_EN | \
     RTE_DMA5_EN | RTE_DMA6_EN | RTE_DMA7_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DMA_IRQHandler(DMA_SEL_t dmaSel)
 * ----------------------------------------------------------------------------
 * Description   : Function called by interrup handler. Perform reguested operations
 *                 when interrupt occurs.
 * Inputs        : dmaSel - channel selection
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void DMA_IRQHandler(DMA_SEL_t dmaSel)
{
    /* Clear pending flag */
    NVIC_ClearPendingIRQ(DMA_Resources.intInfo.irqn[dmaSel]);

    /* Check if callback was set */
    if (DMA_Resources.intInfo.cb[dmaSel])
    {
        /* Execute application callback */
        DMA_Resources.intInfo.cb[dmaSel] (1 << dmaSel);
    }
}
#endif

#if (RTE_DMA0_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DMA0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a DMA event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA0_IRQHandler(void)
{
    /* Call the dma irq handler */
    DMA_IRQHandler(DMA_CH_0);
}

#endif /* if (RTE_DMA0_EN) */

#if (RTE_DMA1_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DMA1_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a DMA event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA1_IRQHandler(void)
{
    /* Call the dma irq handler */
    DMA_IRQHandler(DMA_CH_1);
}

#endif /* if (RTE_DMA1_EN) */

#if (RTE_DMA2_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DMA2_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a DMA event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA2_IRQHandler(void)
{
    /* Call the dma irq handler */
    DMA_IRQHandler(DMA_CH_2);
}

#endif /* if (RTE_DMA2_EN) */

#if (RTE_DMA3_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DMA3_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a DMA event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA3_IRQHandler(void)
{
    /* Call the dma irq handler */
    DMA_IRQHandler(DMA_CH_3);
}

#endif /* if (RTE_DMA3_EN) */

#if (RTE_DMA4_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DMA4_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a DMA event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA4_IRQHandler(void)
{
    /* Call the dma irq handler */
    DMA_IRQHandler(DMA_CH_4);
}

#endif /* if (RTE_DMA4_EN) */

#if (RTE_DMA5_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DMA5_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a DMA event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA5_IRQHandler(void)
{
    /* Call the dma irq handler */
    DMA_IRQHandler(DMA_CH_5);
}

#endif /* if (RTE_DMA5_EN) */

#if (RTE_DMA6_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DMA6_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a DMA event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA6_IRQHandler(void)
{
    /* Call the dma irq handler */
    DMA_IRQHandler(DMA_CH_6);
}

#endif /* if (RTE_DMA6_EN) */

#if (RTE_DMA7_EN)
/* ----------------------------------------------------------------------------
 * Function      : void DMA7_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a DMA event occurs. Application
 *                 is notified via callback function once the interrupt occurs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA7_IRQHandler(void)
{
    /* Call the dma irq handler */
    DMA_IRQHandler(DMA_CH_7);
}

#endif /* if (RTE_DMA7_EN) */

/* DMA Driver Control Block */
DRIVER_DMA_t Driver_DMA =
{
    DMA_GetVersion,
    DMA_Initialize,
    DMA_Configure,
    DMA_ConfigureWord,
    DMA_ConfigureAddr,
    DMA_SetInterruptPriority,
    DMA_CreateConfigWord,
    DMA_SetConfigWord,
    DMA_Start,
    DMA_Stop,
    DMA_GetCounterValue,
    DMA_GetStatus
};

#endif    /* RTE_DMA */
