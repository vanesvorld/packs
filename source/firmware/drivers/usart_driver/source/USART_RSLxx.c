/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * USART_RSLXX.c
 * - CMSIS-Driver implementation for RSLXX UART
 * ----------------------------------------------------------------------------
 * $Revision: 1.3 $
 * $Date: 2019/06/10 21:57:25 $
 * ------------------------------------------------------------------------- */

#include <GPIO_RSLxx.h>
#include <DMA_RSLxx.h>
#include <USART_RSLxx.h>

#if RTE_USART

#define ARM_USART_DRV_VERSION    ARM_DRIVER_VERSION_MAJOR_MINOR(1, 0)

/* Driver Version */
static const ARM_DRIVER_VERSION DriverVersion =
{
    ARM_USART_API_VERSION,
    ARM_USART_DRV_VERSION
};

/* USART0 runtime information */
static USART_INFO_t USART0_Info = {
    .default_cfg = {                                               /* usart0 default configuration */
        .rx_pin               = RTE_USART0_RX_PIN_DEFAULT,         /* usart0 default rx pin identifier */
        .tx_pin               = RTE_USART0_TX_PIN_DEFAULT,         /* usart0 default rx pin */
#if (USART_DMA_CODE_EN)
        .rx_dma_ch            = RTE_USART0_RX_DMA_CH_DEFAULT,      /* usart0 default rx dma channel */
        .tx_dma_ch            = RTE_USART0_TX_DMA_CH_DEFAULT,      /* usart0 default tx dma channel */
#endif /* if (USART_DMA_CODE_EN) */
#if (USART_CM3_CODE_EN)
        .pri_cfg = {                                               /* usart0 default priority configuration */
            .preempt_pri      = RTE_USART_INT_PREEMPT_PRI_DEFAULT, /* usart0 default pre-empt priority value */
            .subgrp_pri       = RTE_USART_INT_PREEMPT_PRI_DEFAULT
        }                                                          /* usart0 default subgroup priority value */

#endif /* if (USART_CM3_CODE_EN) */
    },
    .status                   = { 0 },                             /* usart0 current status */
    .flags                    = 0,                                 /* usart0 current flags */
    .mode                     = 0,                                 /* usart0 current mode */
    .baudrate                 = 0,                                 /* usart0 current baudrate */
#if (USART_DMA_CODE_EN)
    .dma_rx_cfg               = 0,                                 /* dma channel receiver configuration */
    .dma_tx_cfg               = 0,                                 /* dma channel transmitter configuration */
    .dma_rx_busy              = 0,                                 /* !!!WORKAROUND!!! for dma rx counter not being
                                                                    * reset before new transmission */
    .dma_tx_busy              = 0                                  /* !!!WORKAROUND!!! for dma tx counter not being
                                                                    * reset before new transmission */
#endif /* if (USART_DMA_CODE_EN) */
};

static USART_INT_INFO_t USART0_intInfo = {                         /* usart0 interrupt info */
#if (USART_CM3_CODE_EN)
    .irqn[USART_RX_INT]   = UART_RX_IRQn,                          /* usart0 rx interrupt number */
    .irqn[USART_TX_INT]   = UART_TX_IRQn,                          /* usart0 tx interrupt number */
#endif /* if (USART_CM3_CODE_EN) */
    .cb = 0,                                                       /* usart0 callback function pointer */
};

#if (USART_CM3_CODE_EN)

/* USART0 transfer information */
static USART_TRANSFER_INFO_t USART0_TransferInfo = { 0 };          /* transfer info */
#endif /* if (USART_CM3_CODE_EN) */

/* Driver Capabilities */
static const ARM_USART_CAPABILITIES DriverCapabilities =
{
    .asynchronous         = 1,                                     /* supports UART (Asynchronous) mode */
    .synchronous_master   = 0,                                     /* supports Synchronous Master mode */
    .synchronous_slave    = 0,                                     /* supports Synchronous Slave mode */
    .single_wire          = 0,                                     /* supports UART Single-wire mode */
    .irda                 = 0,                                     /* supports UART IrDA mode */
    .smart_card           = 0,                                     /* supports UART Smart Card mode */
    .smart_card_clock     = 0,                                     /* smart Card Clock generator available */
    .flow_control_rts     = 0,                                     /* RTS Flow Control available */
    .flow_control_cts     = 0,                                     /* CTS Flow Control available */
    .event_tx_complete    = 0,                                     /* transmit completed event: \ref
                                                                    * ARM_USART_EVENT_TX_COMPLETE */
    .event_rx_timeout     = 0,                                     /* signal rx char timeout evt: \ref
                                                                    * ARM_USART_EVENT_RX_TIMEOUT */
    .rts                  = 0,                                     /* RTS Line: 0=not available, 1=available */
    .cts                  = 0,                                     /* CTS Line: 0=not available, 1=available */
    .dtr                  = 0,                                     /* DTR Line: 0=not available, 1=available */
    .dsr                  = 0,                                     /* DSR Line: 0=not available, 1=available */
    .dcd                  = 0,                                     /* DCD Line: 0=not available, 1=available */
    .ri                   = 0,                                     /* RI Line: 0=not available, 1=available */
    .event_cts            = 0,                                     /* signal CTS change event: \ref ARM_USART_EVENT_CTS
                                                                    * */
    .event_dsr            = 0,                                     /* signal DSR change event: \ref ARM_USART_EVENT_DSR
                                                                    * */
    .event_dcd            = 0,                                     /* signal DCD change event: \ref ARM_USART_EVENT_DCD
                                                                    * */
    .event_ri             = 0,                                     /* signal RI change event: \ref ARM_USART_EVENT_RI */
    .reserved             = 0                                      /* reserved (must be zero) */
};

/* USART0 Resources */
static const USART_RESOURCES_t USART0_Resources = {                /* usart0 resources */
    .reg                      = UART,                              /* usart0 hw registers */
    .intInfo                  = &USART0_intInfo,                   /* usart0 interrupt info */
    .info                     = &USART0_Info,                      /* usart0 runtime info */
#if (USART_CM3_CODE_EN)
    .xfer                     = &USART0_TransferInfo               /* usart0 transfer info*/
#endif /* if (USART_CM3_CODE_EN) */
};

/* Prepare gpio driver pointer */
static DRIVER_GPIO_t *gpio = &Driver_GPIO;

#if (USART_DMA_CODE_EN)

/* Prepare dma driver pointer */
static DRIVER_DMA_t *dma = &Driver_DMA;
#endif /* if (USART_DMA_CODE_EN) */

#if (USART_CM3_CODE_EN)
/* ----------------------------------------------------------------------------
 * Function      : void USART_TX_IRQHandler(USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a byte is transmitted. Place
 *                 transmit data into the UART, sequentially, byte per byte.
 *                 Manage flags and counters. Application is notified via
 *                 callback function once the transmission is complete
 * Inputs        : usart    - usart instance
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void USART_TX_IRQHandler(USART_RESOURCES_t *usart)
{
    /* Disable usart tx interrupt */
    NVIC_DisableIRQ(usart->intInfo->irqn[USART_TX_INT]);

    /* Clear pending usart tx interrupt */
    NVIC_ClearPendingIRQ(usart->intInfo->irqn[USART_TX_INT]);

    /* Increment tx count */
    usart->xfer->tx_cnt++;

    /* Check if transfer is complete */
    if (usart->xfer->tx_cnt == usart->xfer->tx_num)
    {
        /* Clear send active flag */
        usart->info->status.tx_busy = USART_FREE;

        /* Notify application */
        if (usart->intInfo->cb)
        {
            usart->intInfo->cb(ARM_USART_EVENT_SEND_COMPLETE);
        }
    }
    else
    {
        /* Enable usart tx interrupt */
        NVIC_EnableIRQ(usart->intInfo->irqn[USART_TX_INT]);

        /* Transfer next byte */
        usart->reg->TX_DATA = usart->xfer->tx_buf[usart->xfer->tx_cnt];
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_RX_IRQHandler(USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Called by hardware ISR when a byte is received. Store
 *                 received data in the buffer, manage flags and counters.
 *                 Application is notified via callback function once the
 *                 receive is complete
 * Inputs        : usart    - usart instance
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void USART_RX_IRQHandler(USART_RESOURCES_t *usart)
{
    /* Disable usart rx interrupt */
    NVIC_DisableIRQ(usart->intInfo->irqn[USART_RX_INT]);

    /* Clear pending usart rx interrupt */
    NVIC_ClearPendingIRQ(usart->intInfo->irqn[USART_RX_INT]);

    /* Receive byte */
    usart->xfer->rx_buf[usart->xfer->rx_cnt++] = usart->reg->RX_DATA;

    /* Check if receive is complete */
    if (usart->xfer->rx_cnt == usart->xfer->rx_num)
    {
        /* Clear rx busy flag */
        usart->info->status.rx_busy = USART_FREE;

        /* Disable usart tx interrupt */
        NVIC_DisableIRQ(usart->intInfo->irqn[USART_RX_INT]);

        /* Notify application */
        if (usart->intInfo->cb)
        {
            usart->intInfo->cb(ARM_USART_EVENT_RECEIVE_COMPLETE);
        }
    }
    else
    {
        /* Enable usart rx interrupt */
        NVIC_EnableIRQ(usart->intInfo->irqn[USART_RX_INT]);
    }
}

#else  /* if (USART_CM3_CODE_EN) */

/* ----------------------------------------------------------------------------
 * Function      : void USART_DMAHandler(uint32_t event, USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Called by dma driver when a dma transfer is done.
 *                 Manage flags and counters. Application is notified via
 *                 callback function once the transmission is complete
 * Inputs        : usart    - usart instance
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void USART_DMA_Handler(uint32_t event, USART_RESOURCES_t *usart)
{
    /* Prepare status description */
    uint32_t status = 0;

    /* Check if tx dma channel triggered the event */
    if (event & (USART_FLAG_BIT_SET << usart->info->default_cfg.tx_dma_ch))
    {
        /* Get dma status */
        DMA_STATUS_t dmaStatus = dma->GetStatus(usart->info->default_cfg.tx_dma_ch);

        /* Check if data was fully transmitted */
        if (dmaStatus.completed)
        {
            /* Clear send active flag */
            usart->info->status.tx_busy = USART_FREE;

            /* Update status flag */
            status |= ARM_USART_EVENT_SEND_COMPLETE;
        }

        /* Check if first word was transmitted */
        if (dmaStatus.started)
        {
            /* Indicate that first byte was transmitted */
            usart->info->dma_tx_busy = 1;
        }
    }

    /* Check if rx dma channel triggered the event */
    if (event & (USART_FLAG_BIT_SET << usart->info->default_cfg.rx_dma_ch))
    {
        /* Get dma status */
        DMA_STATUS_t dmaStatus = dma->GetStatus(usart->info->default_cfg.rx_dma_ch);

        /* Check if buffer was fully filled */
        if (dmaStatus.completed)
        {
            /* Clear send active flag */
            usart->info->status.rx_busy = USART_FREE;

            /* Update status flag */
            status |= ARM_USART_EVENT_RECEIVE_COMPLETE;
        }

        /* Check if first word was transmitted */
        if (dmaStatus.started)
        {
            /* Indicate that first byte was transmitted */
            usart->info->dma_rx_busy = 1;
        }
    }

    /* Notify application */
    if (status && usart->intInfo->cb)
    {
        usart->intInfo->cb(status);
    }
}

#endif /* if (USART_CM3_CODE_EN) */

/* ----------------------------------------------------------------------------
 * Function      : void USARTx_GetVersion(void)
 * ----------------------------------------------------------------------------
 * Description   : Get driver version
 * Inputs        : None
 * Outputs       : ARM_DRIVER_VERSION
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_DRIVER_VERSION USARTx_GetVersion(void)
{
    /* Return driver version */
    return DriverVersion;
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_GetCapabilities(void)
 * ----------------------------------------------------------------------------
 * Description   : Get USARTX driver capabilities
 * Inputs        : usart    - USART instance
 * Outputs       : ARM_USART_CAPABILITIES
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_USART_CAPABILITIES USARTx_GetCapabilities(void)
{
    /* Return usart capabilities */
    return DriverCapabilities;
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_Initialize(ARM_USART_SignalEvent_t cb_event,
 *                                       USART_RESOURCES_t* usart)
 * ----------------------------------------------------------------------------
 * Description   : Initialize usart flags, gpio pins and reference to
 *                 callback function.
 * Inputs        : cb_event - pointer to callback function (optional)
 *                 usart    - usart instance
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t USART_Initialize(ARM_USART_SignalEvent_t cb_event,
                                USART_RESOURCES_t *usart)
{
    /* Check if driver is already initialized */
    if (usart->info->flags & USART_FLAG_INITIALIZED)
    {
        /* Driver is already initialized */
        return ARM_DRIVER_OK;
    }

    /* Initialize USART Run-time Resources */
    usart->intInfo->cb = cb_event;

#if (USART_CM3_CODE_EN)

    /* Clear transfer information */
    memset(usart->xfer, 0, sizeof(USART_TRANSFER_INFO_t));
#endif /* if (USART_CM3_CODE_EN) */

    /* Prepare rx pin configuration */
    GPIO_PAD_CFG_t gpioCfg = {
        .pull_mode     = GPIO_WEAK_PULL_UP,
        .drive_mode    = GPIO_6X,
        .lpf_en        = GPIO_LPF_DISABLE,
        .io_mode       = GPIO_MODE_UART_RX_IN
    };

    /* Configure rx pin */
    gpio->ConfigurePad(usart->info->default_cfg.rx_pin, &gpioCfg);

    /* Change configuration to tx */
    gpioCfg.io_mode = GPIO_MODE_UART_TX_OUT;

    /* Configure tx pin */
    gpio->ConfigurePad(usart->info->default_cfg.tx_pin, &gpioCfg);

#if (USART_DMA_CODE_EN)

    /* Prepare rx dma configuration */
    const DMA_CFG_t dmaCfgR = {
        .src_sel       = DMA_TRG_UART,
        .src_step_mode = DMA_STEP_STATIC,
        .src_word_size = DMA_WORD_SIZE_8,
        .dst_sel       = DMA_TRG_MEM,
        .dst_step_mode = DMA_STEP_INC,
        .dst_word_size = DMA_WORD_SIZE_32,
        .byte_order    = DMA_ENDIANNESS_LITTLE,
        .ch_priority   = DMA_CH_PRI_0,
        .data_mode     = DMA_SINGLE
    };

    /* Configure the rx dma channel */
    dma->Configure(usart->info->default_cfg.rx_dma_ch, &dmaCfgR, UART_DMA_Handler);

    /* Store the rx dma configuration */
    usart->info->dma_rx_cfg = dma->CreateConfigWord(&dmaCfgR);

    /* Prepare tx dma configuration */
    const DMA_CFG_t dmaCfgT = {
        .src_sel       = DMA_TRG_MEM,
        .src_step_mode = DMA_STEP_INC,
        .src_word_size = DMA_WORD_SIZE_32,
        .dst_sel       = DMA_TRG_UART,
        .dst_step_mode = DMA_STEP_STATIC,
        .dst_word_size = DMA_WORD_SIZE_8,
        .byte_order    = DMA_ENDIANNESS_LITTLE,
        .ch_priority   = DMA_CH_PRI_0,
        .data_mode     = DMA_SINGLE
    };

    /* Configure the tx dma channel */
    dma->Configure(usart->info->default_cfg.tx_dma_ch, &dmaCfgT, UART_DMA_Handler);

    /* Store the tx dma configuration */
    usart->info->dma_tx_cfg = dma->CreateConfigWord(&dmaCfgT);
#endif /* if (USART_DMA_CODE_EN) */

    /* Indicate that initialization is done */
    usart->info->flags = USART_FLAG_INITIALIZED;

    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_Uninitialize(USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Uninitialize USART flags, gpio pins and remove reference to
 *                 callback function.
 * Inputs        : usart - usart instance
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t USART_Uninitialize(USART_RESOURCES_t *usart)
{
#if (USART_DMA_CODE_EN)

    /* Stop the dma data transfer */
    dma->Stop(usart->info->default_cfg.rx_dma_ch);
    dma->Stop(usart->info->default_cfg.tx_dma_ch);
#endif /* if (USART_DMA_CODE_EN) */

    /* Prepare rx / tx pin configuration */
    const GPIO_PAD_CFG_t gpioCfg = { .io_mode = GPIO_MODE_DISABLED };

    /* Reset rx pin */
    gpio->ConfigurePad(usart->info->default_cfg.rx_pin, &gpioCfg);

    /* Reset tx pin */
    gpio->ConfigurePad(usart->info->default_cfg.tx_pin, &gpioCfg);

    /* Reset uart alternative function register */
    gpio->ResetAltFuncRegister(GPIO_FUNC_REG_UART);

    /* Reset usart status flags */
    usart->info->flags = 0U;

    /* Remove reference to callback function */
    usart->intInfo->cb = NULL;

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_PowerControl(ARM_POWER_STATE state,
 *                                         USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Operate the power modes of the USART interface
 * Inputs        : state - ARM_POWER_FULL or ARM_POWER_OFF
 *                 usart - USART instance
 * Outputs       : ARM_DRIVER_OK - if the operation is successful
 *                 ARM_DRIVER_ERROR_UNSUPPORTED - if argument is ARM_POWER_LOW
 *                                                or an invalid value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t USART_PowerControl(ARM_POWER_STATE state,
                                  USART_RESOURCES_t *usart)
{
    /* Check which operation should be performed */
    switch (state)
    {
        case ARM_POWER_OFF:
        {
#if (USART_CM3_CODE_EN)

            /* Disable UART interrupts */
            NVIC_DisableIRQ(usart->intInfo->irqn[USART_RX_INT]);
            NVIC_DisableIRQ(usart->intInfo->irqn[USART_TX_INT]);

            /* Clear pending USART interrupts in NVIC */
            NVIC_ClearPendingIRQ(usart->intInfo->irqn[USART_RX_INT]);
            NVIC_ClearPendingIRQ(usart->intInfo->irqn[USART_TX_INT]);
#endif /* if (USART_CM3_CODE_EN) */

            /* Disable UART */
            Sys_UART_Disable();

            /* Clear flags */
            usart->info->status.tx_busy          = USART_FREE;
            usart->info->status.rx_busy          = USART_FREE;
            usart->info->flags &= ~USART_FLAG_POWERED;
            break;
        }

        case ARM_POWER_LOW:
        {
            /* Return unsupported error */
            return ARM_DRIVER_ERROR_UNSUPPORTED;
        }

        case ARM_POWER_FULL:
        {
            /* Check if usart was initialized */
            if ((usart->info->flags & USART_FLAG_INITIALIZED) == 0U)
            {
                /* Return error */
                return ARM_DRIVER_ERROR;
            }

            /* Check if usart was already powered up */
            if ((usart->info->flags & USART_FLAG_POWERED) != 0U)
            {
                /* Return OK */
                return ARM_DRIVER_OK;
            }

            /* Clear Status flags */
            usart->info->status.tx_busy = 0U;
            usart->info->status.rx_busy = 0U;

            /* Set powered flag */
            usart->info->flags = USART_FLAG_POWERED | USART_FLAG_INITIALIZED;
            break;
        }

        default:
        {
            /* Return unsupported error */
            return ARM_DRIVER_ERROR_UNSUPPORTED;
        }
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_Control(uint32_t control, uint32_t arg,
 *                                    USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Set mode or execute operation for USART0. Examples:
 *                 ARM_USART_ABORT_RECEIVE, ARM_USART_ABORT_SEND or
 *                 ARM_USART_MODE_ASYNCHRONOUS.
 * Inputs        : arg   - Argument of operation (optional)
 *                 usart - USART instance
 * Outputs       : Execution status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t USART_Control(uint32_t control, uint32_t arg,
                             USART_RESOURCES_t *usart)
{
    /* Check if usart is powered up */
    if ((usart->info->flags & USART_FLAG_POWERED) == 0U)
    {
        /* Return usart not powered error */
        return ARM_DRIVER_ERROR;
    }

    /* Check which operation should be performed */
    switch (control & ARM_USART_CONTROL_Msk)
    {
        case ARM_USART_MODE_ASYNCHRONOUS:
        {
#if (USART_DMA_CODE_EN)

            /* Configure usart */
            Sys_UART_Enable(SystemCoreClock, arg, UART_DMA_MODE_ENABLE);
#else  /* if (USART_DMA_CODE_EN) */

            /* Configure usart */
            Sys_UART_Enable(SystemCoreClock, arg, UART_DMA_MODE_DISABLE);
#endif /* if (USART_DMA_CODE_EN) */

            /* Store baudrate*/
            usart->info->baudrate = arg;
            break;
        }

        case ARM_USART_ABORT_SEND:
        {
#if (USART_DMA_CODE_EN)

            /* Stop the dma data transfer */
            dma->Stop(usart->info->default_cfg.tx_dma_ch);
#else  /* if (USART_DMA_CODE_EN) */

            /* Disable usart tx interrupt */
            NVIC_DisableIRQ(usart->intInfo->irqn[USART_TX_INT]);
#endif /* if (USART_DMA_CODE_EN) */

            /* Clear send active flag */
            usart->info->status.tx_busy = USART_FREE;
            return ARM_DRIVER_OK;
        }

        case ARM_USART_ABORT_RECEIVE:
        {
#if (USART_DMA_CODE_EN)

            /* Stop the dma data transfer */
            dma->Stop(usart->info->default_cfg.rx_dma_ch);
#else  /* if (USART_DMA_CODE_EN) */

            /* Disable usart rx interrupt */
            NVIC_DisableIRQ(usart->intInfo->irqn[USART_RX_INT]);
#endif /* if (USART_DMA_CODE_EN) */

            /* Clear rx busy flag */
            usart->info->status.rx_busy = USART_FREE;
            return ARM_DRIVER_OK;
        }

        default:
        {
            /* Return unsupported operation error */
            return ARM_DRIVER_ERROR_UNSUPPORTED;
        }
    }

    /* Check usart data bits */
    if ((control & ARM_USART_DATA_BITS_Msk) != ARM_USART_DATA_BITS_8)
    {
        /* Return data_bits error */
        return ARM_USART_ERROR_DATA_BITS;
    }

    /* Check usart parity */
    if ((control & ARM_USART_PARITY_Msk) != ARM_USART_PARITY_NONE)
    {
        /* Return parity error */
        return ARM_USART_ERROR_PARITY;
    }

    /* Check usart stop bits */
    if ((control & ARM_USART_STOP_BITS_Msk) != ARM_USART_STOP_BITS_1)
    {
        /* Return stop_bits error */
        return ARM_USART_ERROR_STOP_BITS;
    }

    /* Check flow control */
    if ((control & ARM_USART_FLOW_CONTROL_Msk) != ARM_USART_FLOW_CONTROL_NONE)
    {
        /* Return flow error */
        return ARM_USART_ERROR_FLOW_CONTROL;
    }

    /* Check polarity */
    if ((control & ARM_USART_CPOL_Msk) != ARM_USART_CPOL0)
    {
        /* Return cpol error */
        return ARM_USART_ERROR_CPOL;
    }

    /* Check clock phase */
    if ((control & ARM_USART_CPHA_Msk) != ARM_USART_CPHA0)
    {
        /* Return cpha error */
        return ARM_USART_ERROR_CPHA;
    }

    /* Set configured flag */
    usart->info->flags |= USART_FLAG_CONFIGURED;

    /* Return OK */
    return ARM_DRIVER_OK;
}

#if (USART_CM3_CODE_EN)
/* ----------------------------------------------------------------------------
 * Function      : int32_t USART_SetInterruptPriority(USART_RESOURCES_t * usart)
 * ----------------------------------------------------------------------------
 * Description   : Configure usart interrupts priority.
 * Inputs        : usart - usart instance
 * Outputs       : ARM_DRIVER_OK
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t USART_SetInterruptPriority(USART_RESOURCES_t *usart)
{
    /* Encoded priority value */
    uint32_t encodedPri = 0;

    /* Encode priority configuration word */
    encodedPri = NVIC_EncodePriority(NVIC_GetPriorityGrouping(), usart->info->default_cfg.pri_cfg.preempt_pri,
                                     usart->info->default_cfg.pri_cfg.subgrp_pri);

    /* Set the priority for each usart interrupt */
    for (int i = 0; i < USART_INT_NUM; ++i)
    {
        /* Set the particular usart interrupt priority */
        NVIC_SetPriority(usart->intInfo->irqn[i], encodedPri);
    }

    /* Return OK */
    return ARM_DRIVER_OK;
}

#endif /* if (USART_CM3_CODE_EN) */

/* ----------------------------------------------------------------------------
 * Function      : int32_t USART_Send(const void *data, uint32_t num,
 *                                    USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Non-blocking send function. If a callback was previously
 *                 registered using the Initialize function, the caller is
 *                 notified when 'num' bytes are sent.
 * Inputs        : data                  - pointer to data buffer
 *                 num                   - number of bytes to send
 *                 usart                 - USART instance
 * Outputs       : ARM_DRIVER_OK         - if it can successfully start the
 *                                         send operation
 *                 ARM_DRIVER_ERROR_BUSY - if it's already busy handling
 *                                         other send operation
 * Assumptions   : The data buffer argument points to a memory area with 'num'
 *                 bytes of size
 * ------------------------------------------------------------------------- */
static int32_t USART_Send(const void *data, uint32_t num,
                          USART_RESOURCES_t *usart)
{
    /* Check input parameters */
    if ((data == NULL) || (num == 0U))
    {
        /* Invalid parameters */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    /* Check if driver was configured */
    if ((usart->info->flags & USART_FLAG_CONFIGURED) == 0U)
    {
        /* USART is not configured (mode not selected) */
        return ARM_DRIVER_ERROR;
    }

    /* Check if tx is busy */
    if (usart->info->status.tx_busy != USART_FREE)
    {
        /* Send is not completed yet */
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Set Send active flag */
    usart->info->status.tx_busy = USART_BUSY;

#if (USART_DMA_CODE_EN)

    /* Reset dma busy flag */
    usart->info->dma_tx_busy = 0U;

    /* Prepare dma for transmission */
    dma->SetConfigWord(usart->info->default_cfg.tx_dma_ch, usart->info->dma_tx_cfg);

    /* Prepare dma buffer configuration */
    DMA_ADDR_CFG_t buffCfg = {
        .src_addr     = data,
        .dst_addr     = &usart->reg->TX_DATA,
        .counter_len  = num,
        .transfer_len = num
    };

    /* Configure the dma channel */
    dma->ConfigureAddr(usart->info->default_cfg.tx_dma_ch, &buffCfg);

    /* Start the dma data transfer */
    dma->Start(usart->info->default_cfg.tx_dma_ch);

#else  /* if (USART_DMA_CODE_EN) */

    /* Save transmit buffer info */
    usart->xfer->tx_buf = (uint8_t *)data;
    usart->xfer->tx_num = num;
    usart->xfer->tx_cnt = 0U;

    /* Enable UART TX interrupt */
    NVIC_ClearPendingIRQ(usart->intInfo->irqn[USART_TX_INT]);
    NVIC_EnableIRQ(usart->intInfo->irqn[USART_TX_INT]);

    /* Transmit first byte (remaining will be transmitted
     * in interrupt handler) */
    usart->reg->TX_DATA = usart->xfer->tx_buf[usart->xfer->tx_cnt];
#endif /* if (USART_DMA_CODE_EN) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_Receive(void *data, uint32_t num,
 *                                    USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Non-blocking receive function. If a callback was previously
 *                 registered using the Initialize function, the caller is
 *                 notified when 'num' bytes are received.
 * Inputs        : data                  - pointer to data buffer
 *                 num                   - number of bytes to receive
 *                 usart                 - USART instance
 * Outputs       : ARM_DRIVER_OK         - if it can successfully start the
 *                                         receive operation
 *                 ARM_DRIVER_ERROR_BUSY - if it's already busy handling
 *                                         other receive operation
 * Assumptions   : The data buffer argument points to a memory area with at
 *                 least 'num' bytes of available space
 * ------------------------------------------------------------------------- */
static int32_t USART_Receive(void *data, uint32_t num, USART_RESOURCES_t *usart)
{
    if ((data == NULL) || (num == 0U))
    {
        /* Invalid parameters */
        return ARM_DRIVER_ERROR_PARAMETER;
    }

    if ((usart->info->flags & USART_FLAG_CONFIGURED) == 0U)
    {
        /* USART is not configured (mode not selected) */
        return ARM_DRIVER_ERROR;
    }

    if (usart->info->status.rx_busy != USART_FREE)
    {
        /* Receiver is busy */
        return ARM_DRIVER_ERROR_BUSY;
    }

    /* Set RX busy flag */
    usart->info->status.rx_busy = USART_BUSY;

    /* Clear the rx buffer if already full */
    usart->reg->RX_DATA;

#if (USART_DMA_CODE_EN)

    /* Reset dma busy flag */
    usart->info->dma_rx_busy = 0U;

    /* Prepare dma for transmission */
    dma->SetConfigWord(usart->info->default_cfg.rx_dma_ch, usart->info->dma_rx_cfg);

    /* Prepare dma buffer configuration */
    DMA_ADDR_CFG_t buffCfg = {
        .src_addr     = &usart->reg->RX_DATA,
        .dst_addr     = data,
        .counter_len  = num,
        .transfer_len = num
    };

    /* Configure the dma channel */
    dma->ConfigureAddr(usart->info->default_cfg.rx_dma_ch, &buffCfg);

    /* Start the dma data transfer */
    dma->Start(usart->info->default_cfg.rx_dma_ch);

#else  /* if (USART_DMA_CODE_EN) */

    /* Save number of data to be received */
    usart->xfer->rx_num = num;

    /* Save receive buffer info */
    usart->xfer->rx_buf = (uint8_t *)data;
    usart->xfer->rx_cnt = 0U;

    /* Enable UART RX interrupt */
    NVIC_ClearPendingIRQ(usart->intInfo->irqn[USART_RX_INT]);
    NVIC_EnableIRQ(usart->intInfo->irqn[USART_RX_INT]);
#endif /* if (USART_DMA_CODE_EN) */

    /* Return OK */
    return ARM_DRIVER_OK;
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_GetTxCount(USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Get USARTX TX count. This count is reset for every send
 *                 operation.
 * Inputs        : usart    - USART instance
 * Outputs       : Number of bytes transmitted by USARTX
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t USART_GetTxCount(USART_RESOURCES_t *usart)
{
#if (USART_DMA_CODE_EN)

    /* Check if new transmission is ongoing */
    if (usart->info->dma_tx_busy)
    {
        /* Return counter value */
        return dma->GetCounterValue(usart->info->default_cfg.tx_dma_ch);
    }

    /* Return 0 */
    return 0;
#else  /* if (USART_DMA_CODE_EN) */

    /* Return tx count */
    return usart->xfer->tx_cnt;
#endif /* if (USART_DMA_CODE_EN) */
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_GetRxCount(USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Get USARTX RX count. This count is reset for every receive
 *                 operation.
 * Inputs        : usart    - USART instance
 * Outputs       : Number of bytes received by USARTX
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t USART_GetRxCount(USART_RESOURCES_t *usart)
{
#if (USART_DMA_CODE_EN)

    /* Check if new transmission is ongoing */
    if (usart->info->dma_rx_busy)
    {
        /* Return counter value */
        return dma->GetCounterValue(usart->info->default_cfg.rx_dma_ch);
    }

    /* Return 0 */
    return 0;
#else  /* if (USART_DMA_CODE_EN) */

    /* Return rx count */
    return usart->xfer->rx_cnt;
#endif /* if (USART_DMA_CODE_EN) */
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_GetStatus(USART_RESOURCES_t *usart)
 * ----------------------------------------------------------------------------
 * Description   : Get USARTX status
 * Inputs        : usart    - USART instance
 * Outputs       : Return USARTX status as an ARM_USART_STATUS structure
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_USART_STATUS USART_GetStatus(USART_RESOURCES_t *usart)
{
    /* Status structure */
    ARM_USART_STATUS status;

    /* Prepare status structure */
    status.tx_busy          = usart->info->status.tx_busy;
    status.rx_busy          = usart->info->status.rx_busy;
    status.rx_overflow      = 0U;
    status.tx_underflow     = 0U;
    status.rx_break         = 0U;
    status.rx_framing_error = 0U;
    status.rx_parity_error  = 0U;

    /* Return status */
    return status;
}

/* ----------------------------------------------------------------------------
 * Function      : void USART_GetModemStatus(void)
 * ----------------------------------------------------------------------------
 * Description   : Not supported
 * Inputs        : None
 * Outputs       : Return status as 0
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static ARM_USART_MODEM_STATUS USART_GetModemStatus(USART_RESOURCES_t *usart)
{
    /* Modem status structure */
    ARM_USART_MODEM_STATUS modem_status;

    /* Prepare modem status structure */
    modem_status.cts = 0U;
    modem_status.dsr = 0U;
    modem_status.ri  = 0U;
    modem_status.dcd = 0U;

    /* Return modem status */
    return modem_status;
}

/* USART0 driver Wrapper functions. See functions above for usage */
static int32_t USART0_Initialize(ARM_USART_SignalEvent_t cb_event)
{
    /* Initialize usart */
    int32_t status = USART_Initialize(cb_event, &USART0_Resources);

#if (USART_CM3_CODE_EN)

    /* Set usart interrupt priorities */
    status |= USART_SetInterruptPriority(&USART0_Resources);
#endif /* if (USART_CM3_CODE_EN) */

#if (RTE_USART_CFG_EN_DEFAULT)

    /* Set default power mode */
    status |= USART_PowerControl(ARM_POWER_FULL, &USART0_Resources);

    /* Set default uart mode, baudrate */
    status |= USART_Control(ARM_USART_MODE_ASYNCHRONOUS, RTE_USART0_BAUDRATE_DEFAULT,
                            &USART0_Resources);
#endif /* if (RTE_USART_CFG_EN_DEFAULT) */

    /* Return initialization status */
    return status;
}

static int32_t USART0_Uninitialize(void)
{
    return USART_Uninitialize(&USART0_Resources);
}

static int32_t USART0_PowerControl(ARM_POWER_STATE state)
{
    return USART_PowerControl(state, &USART0_Resources);
}

static int32_t USART0_Send(const void *data, uint32_t num)
{
    return USART_Send(data, num, &USART0_Resources);
}

static int32_t USART0_Receive(void *data, uint32_t num)
{
    return USART_Receive(data, num, &USART0_Resources);
}

/* ----------------------------------------------------------------------------
 * Function      : void USART0_Transfer(const void *data_out, void *data_in,
 *                                      uint32_t num)
 * ----------------------------------------------------------------------------
 * Description   : Not supported
 * Inputs        : None
 * Outputs       : ARM_DRIVER_ERROR_UNSUPPORTED
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t USART0_Transfer(const void *data_out, void *data_in, uint32_t num)
{
    return ARM_DRIVER_ERROR_UNSUPPORTED;
}

static uint32_t USART0_GetTxCount(void)
{
    return USART_GetTxCount(&USART0_Resources);
}

static uint32_t USART0_GetRxCount(void)
{
    return USART_GetRxCount(&USART0_Resources);
}

static int32_t USART0_Control(uint32_t control, uint32_t arg)
{
    return USART_Control(control, arg, &USART0_Resources);
}

static ARM_USART_STATUS USART0_GetStatus(void)
{
    return USART_GetStatus(&USART0_Resources);
}

/* ----------------------------------------------------------------------------
 * Function      : void USART0_SetModemControl(ARM_USART_MODEM_CONTROL control)
 * ----------------------------------------------------------------------------
 * Description   : Not supported
 * Inputs        : None
 * Outputs       : ARM_DRIVER_ERROR_UNSUPPORTED
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static int32_t USART0_SetModemControl(ARM_USART_MODEM_CONTROL control)
{
    return ARM_DRIVER_ERROR_UNSUPPORTED;
}

static ARM_USART_MODEM_STATUS USART0_GetModemStatus(void)
{
    return USART_GetModemStatus(&USART0_Resources);
}

#if (USART_CM3_CODE_EN)
void UART_TX_IRQHandler(void)
{
    USART_TX_IRQHandler(&USART0_Resources);
}

void UART_RX_IRQHandler(void)
{
    USART_RX_IRQHandler(&USART0_Resources);
}

#else  /* if (USART_CM3_CODE_EN) */
void UART_DMA_Handler(uint32_t event)
{
    USART_DMA_Handler(event, &USART0_Resources);
}

#endif /* if (USART_CM3_CODE_EN) */

/* USART0 Driver Control Block */
ARM_DRIVER_USART Driver_USART0 =
{
    USARTx_GetVersion,
    USARTx_GetCapabilities,
    USART0_Initialize,
    USART0_Uninitialize,
    USART0_PowerControl,
    USART0_Send,
    USART0_Receive,
    USART0_Transfer,
    USART0_GetTxCount,
    USART0_GetRxCount,
    USART0_Control,
    USART0_GetStatus,
    USART0_SetModemControl,
    USART0_GetModemStatus
};
#endif    /* if RTE_USART0 */
