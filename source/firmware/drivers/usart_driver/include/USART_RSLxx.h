/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * USART_RSLXX.h
 * - CMSIS-Driver header file for RSLXX UART
 * ----------------------------------------------------------------------------
 * $Revision: 1.3 $
 * $Date: 2019/06/10 21:57:25 $
 * ------------------------------------------------------------------------- */

#ifndef USART_RSLXX_H
#define USART_RSLXX_H

#include <rsl10.h>
#include <Driver_USART.h>
#include <RTE_Device.h>
#include <Driver_DMA.h>
#include <Driver_GPIO.h>

#if !RTE_USART
  #error "USART not configured in RTE_Device.h!"
#endif    /* if !RTE_USART0 */

/* DMA / CM3 code switches */
#define USART_DMA_CODE_EN (RTE_USART0_DMA_EN_DEFAULT)
#define USART_CM3_CODE_EN (!RTE_USART0_DMA_EN_DEFAULT)

/* USART interrupt handlers prototypes */
#if(USART_DMA_CODE_EN)
void UART_DMA_Handler(uint32_t event);
#else
void UART_RX_IRQHandler(void);
void UART_TX_IRQHandler(void);
#endif

/* Extern GPIO driver */
extern DRIVER_GPIO_t Driver_GPIO;

/* Extern DMA driver */
extern DRIVER_DMA_t Driver_DMA;

/* USART flags */
#define USART_FLAG_INITIALIZED      ((uint8_t)(1U))
#define USART_FLAG_POWERED          ((uint8_t)(1U << 1))
#define USART_FLAG_CONFIGURED       ((uint8_t)(1U << 2))
#define USART_FLAG_BIT_SET          0x1U

/* USART interrupt number */
#define USART_INT_NUM               2U

/* USART interrupts*/
#define USART_RX_INT                0U
#define USART_TX_INT                1U

/* USART statuses*/
#define USART_FREE                  0U
#define USART_BUSY                  1U

/* USART runtime transfer information */
typedef struct _USART_TRANSFER_INFO_t
{
    uint32_t                   rx_num;              /* Total number of receive data */
    uint32_t                   tx_num;              /* Total number of transmit data */
    uint8_t                   *rx_buf;              /* Pointer to in data buffer */
    uint8_t                   *tx_buf;              /* Pointer to out data buffer */
    uint32_t                   rx_cnt;              /* Number of data received */
    uint32_t                   tx_cnt;              /* Number of data sent */
} USART_TRANSFER_INFO_t;

/* USART Status */
typedef struct _USART_TRANSFER_STATUS_t
{
    uint8_t                    tx_busy;             /* transmitter busy flag */
    uint8_t                    rx_busy;             /* receiver busy flag */
} USART_TRANSFER_STATUS_t;

/* USART interrupt priority configuration */
typedef struct _USART_PRI_CFG_t
{
    uint32_t                   preempt_pri :3;      /* preempt priority */
    uint32_t                               :13;     /* reserved */
    uint32_t                   subgrp_pri  :3;      /* subgroup priority */
    uint32_t                               :13;     /* reserved */
} USART_PRI_CFG_t;

/* USART default configuration */
typedef struct _USART_DEFAULT_CFG_t
{
    GPIO_SEL_t                 tx_pin      :4;      /* usart tx pin identifier */
    GPIO_SEL_t                 rx_pin      :4;      /* usart rx pin identifier */
#if(USART_DMA_CODE_EN)
    DMA_SEL_t                  rx_dma_ch   :3;      /* defines which dma channel should be used for rx */
    DMA_SEL_t                  tx_dma_ch   :3;      /* defines which dma channel should be used for tx */
    uint8_t                                :2;      /* reserved */
#endif
#if(USART_CM3_CODE_EN)
    USART_PRI_CFG_t            pri_cfg;             /* usart interrupt priorrity default configuration */
#endif
} USART_DEFAULT_CFG_t;

/* USART runtime info */
typedef struct _USART_INFO_t
{
    const USART_DEFAULT_CFG_t  default_cfg;         /* usart default configuration */
    USART_TRANSFER_STATUS_t    status;              /* status flags */
    uint8_t                    flags;               /* current usart flags */
    uint32_t                   mode;                /* current usart mode */
    uint32_t                   baudrate;            /* baudrate */
#if(USART_DMA_CODE_EN)
    uint32_t                   dma_rx_cfg;          /* dma channel receiver configuration */
    uint32_t                   dma_tx_cfg;          /* dma channel transmitter configuration */
    uint8_t                    dma_rx_busy :1;      /* !!!WORKAROUND!!! for dma rx counter not being reset before new transmission */
    uint8_t                    dma_tx_busy :1;      /* !!!WORKAROUND!!! for dma tx counter not being reset before new transmission */
    uint8_t                                :6;      /* reserved */
#endif
} USART_INFO_t;

/* USART interrupts info */
typedef struct _USART_INT_INFO_t
{
#if(USART_CM3_CODE_EN)
    const IRQn_Type            irqn[USART_INT_NUM]; /* usart IRQs number */
#endif
    ARM_USART_SignalEvent_t    cb;                  /* usart event callback */
} USART_INT_INFO_t;

/* USART resources definition */
typedef const struct
{
    UART_Type                 *reg;                 /* usart peripheral pointer */
    USART_INT_INFO_t          *intInfo;             /* IRQs Info */
    USART_INFO_t              *info;                /* runtime Information */
#if(USART_CM3_CODE_EN)
    USART_TRANSFER_INFO_t     *xfer;                /* usart transfer information */
#endif
} USART_RESOURCES_t;

#endif    /* USART_RSLXX_H */
