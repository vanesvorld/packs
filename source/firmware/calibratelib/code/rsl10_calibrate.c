/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC
 * (d/b/a ON Semiconductor). All Rights Reserved.
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor. The
 * terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_calibrate.c
 * - Calibration support functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.2 $
 * $Date: 2017/05/25 21:02:50 $
 * ------------------------------------------------------------------------- */

#include <rsl10_calibrate.h>

/* ----------------------------------------------------------------------------
 * Firmware Calibration Library Version
 * ------------------------------------------------------------------------- */
const short RSL10_CalibrateLib_Version = CALIBRATE_FW_VER;
