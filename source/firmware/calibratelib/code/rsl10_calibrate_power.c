/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC
 * (d/b/a ON Semiconductor). All Rights Reserved.
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor. The
 * terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_calibrate_power.c
 * - Power calibration support functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.19 $
 * $Date: 2017/06/22 17:21:19 $
 * ------------------------------------------------------------------------- */

#include <rsl10_calibrate.h>

/* ----------------------------------------------------------------------------
 * IMacro: CONVERT(x)
 * ----------------------------------------------------------------------------
 * Description   : Converts an adc code to a voltage, calculated as follows:
 *                  voltage = adc_code * (2 V * 1000 [mV*10]/1 V / 2^14 steps.)
 * Inputs        : x               - the ADC code input
 * Outputs       : return value    - the voltage output in mV*10
 * Assumptions   : Low frequency mode for the ADC is used, meaning that the
 *                 resolution of the ADC is 14-bits. CONVERT provides voltage 
 *                 level as a milliVolt value based on the input ADC code.
 * ------------------------------------------------------------------------- */
#define CONVERT(x)                      ((uint32_t)((x * 1000) >> 13))

 /* ----------------------------------------------------------------------------
 * IMacro: SWAP(a, b, t)
 * ----------------------------------------------------------------------------
 * Description   : Swap the values in variables a and b
 * Inputs        : - a             - holds the value that must go to b
 *                 - b             - holds the value that must go to a
 *                                   measured cycle counts
 *                 - t             - a temporary buffer for the swap
 * Outputs       : - a             - holds the value previously in b
 *                 - b             - holds the value previously in a
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#define SWAP(a, b, t)                   ((t) = (a), (a) = (b), (b) = (t))

/* ----------------------------------------------------------------------------
 * IFunction      : static void Calibrate_Clock_InitializeXTAL48M(void)
 * ----------------------------------------------------------------------------
 * Description   : Initializes the 48MHz XTAL oscillator and configures the
 *                 system clock to RF clock oscillator.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void Calibrate_Clock_InitializeXTAL48M(void)

{
    /* Start 48 MHz XTAL oscillator */

    /* Enable VDDRF supply without changing trimming settings */
    *((volatile uint8_t *)&ACS->VDDRF_CTRL+1) = (VDDRF_ENABLE_BITBAND <<
                                                (ACS_VDDRF_CTRL_ENABLE_Pos - 8)) |
                                                (VDDRF_DISABLE_HIZ_BITBAND <<
                                                (ACS_VDDRF_CTRL_CLAMP_Pos - 8));

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_3_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS != ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to (divided 48 MHz) oscillator clock */
    Sys_Clocks_SystemClkConfig(SYSCLK_CLKSRC_RFCLK |
                               EXTCLK_PRESCALE_1   |
                               JTCK_PRESCALE_1);


    /* Configure clock dividers */
    CLK->DIV_CFG0 =  SLOWCLK_PRESCALE_16 | BBCLK_PRESCALE_1 | USRCLK_PRESCALE_1;
}

/* ----------------------------------------------------------------------------
 * IFunction     : static uint32_t Calibrate_Power_GetMedian
 *                                               (uint32_t a,
 *                                                uint32_t b,
 *                                                uint32_t c)
 * ----------------------------------------------------------------------------
 * Description   : Determines the median of 3 supply measurements
 * Inputs        : - a             - Supply measurement #1
 *                 - b             - Supply measurement #2
 *                 - c             - Supply measurement #3
 * Outputs       : return value    - Returns the median value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static uint32_t Calibrate_Power_GetMedian(uint32_t a, uint32_t b, uint32_t c)
{

    uint32_t temp = 0;

    /* Put smallest in a, median in b, and max in c */
    if (a > b)
    {
        /* Swap a and b */
        SWAP(a, b, temp);
    }

    if (b > c)
    {
        /* Swap b and c */
        SWAP(b, c, temp);

        if (a > b)
        {
            /* Swap a and b */
            SWAP(a, b, temp);
        }
    }

    /* b always contains the median value */
    return (b);
}

/* ----------------------------------------------------------------------------
 * IFunction     : static uint32_t Calibrate_Power_MeasureSupply
 *                                                 (uint32_t *adc_ptr)
 * ----------------------------------------------------------------------------
 * Description   : Measure a supply voltage; returns the median measurement of
 *                 3 measurements to ensure that we're rejecting sampling noise
 * Inputs        : *adc_ptr            - Pointer to ADC data register
 * Outputs       : return value        - Median measurement from the ADC
 *                                       data register
 * Assumptions   : Calibrate_Power_Initialize() has been called.
 * ------------------------------------------------------------------------- */
static uint32_t Calibrate_Power_MeasureSupply(uint32_t *adc_ptr)
{

    uint32_t supply1 = 0;
    uint32_t supply2 = 0;
    uint32_t supply3 = 0;

    /* Define a short stabilization delay to ensure that we
     * allow the ADC measured parameter to stabilize. */
    Sys_Delay_ProgramROM(ADC_STABILIZATION_DELAY);
    supply1 = *adc_ptr;

    Sys_Delay_ProgramROM(ADC_MEASUREMENT_DELAY);
    supply2 = *adc_ptr;

    Sys_Delay_ProgramROM(ADC_MEASUREMENT_DELAY);
    supply3 = *adc_ptr;

    /* Return median of the 3 measurements */
    return (Calibrate_Power_GetMedian(supply1, supply2, supply3));
}

/*-----------------------------------------------------------------------------
 * IFunction     : static unsigned int Calibrate_Power_BinarySearch
 *                                               (uint32_t target,
 *                                                uint8_t max,
 *                                                uint8_t min,
 *                                                uint32_t *adc_ptr,
 *                                                uint8_t *supply_ptr)
 * ----------------------------------------------------------------------------
 * Description   : Perform a binary search to set the data read from an ADC
 *                 to match a specified target voltage.
 * Inputs        : - target        - The specified target voltage [mV]
 *                 - max           - The setting that gives the maximum
 *                                   measured output
 *                 - min           - The setting that gives the minimum
 *                                   measured output
 *                 - adc_ptr       - Pointer to the ADC data register
 *                 - supply_ptr    - Pointer to the power supply control
 *                                   register
 * Outputs       : return value    - Status value indicating whether the
 *                                   calibration succeeded
 * Assumptions   : None
 * Clock cycles  : N/A
 * ------------------------------------------------------------------------- */
static unsigned int Calibrate_Power_BinarySearch(uint32_t target,
                                                 uint8_t max,
                                                 uint8_t min,
                                                 uint32_t *adc_ptr,
                                                 uint8_t *supply_ptr)
{
    uint8_t current_setting = 0;
    uint32_t current_readback_voltage = 0;
    uint8_t previous_setting = min;
    uint32_t error = 0;

    do
    {
        /* Set the middle to half of difference between min and max */
        current_setting = ((max - min) / 2) + min;

        /* Makes it possible to reach the maximum value, as it would not
         * otherwise be possible because of flooring present in integer
         * division. */
        if ((current_setting == (max - 1)) &&
            (current_setting == previous_setting))
        {
            current_setting = max;
        }

        /* Set power supply's VTRIM_BYTE register to current setting */
        *supply_ptr = (current_setting);

        /* Measure the power supply from the ADC register and convert the analog
         * value
         */
        current_readback_voltage = CONVERT(Calibrate_Power_MeasureSupply(adc_ptr));

        /* Exit if the target voltage == the current measured voltage */
        if (target == current_readback_voltage)
        {
            return ERRNO_NO_ERROR;
        }

        /* Determine the error in the ADC measurement and target
         * readback voltage.
         */
        if (target < current_readback_voltage)
        {
            error = (current_readback_voltage - target);
        }
        else
        {
            error = (target - current_readback_voltage);
        }

        /* Exit if the current measurement is within allowed
         * ADC measurement error */
        if (error <= ADC_MEASUREMENT_ERROR)
        {
            /* Calibration successful */
            return (ERRNO_NO_ERROR);
        }

        /* Exit if the current setting is the same as the previous setting
         * This means that the search cannot proceed further. */
        if (previous_setting == current_setting)
        {
            /* Calibration not successful */
            return ERRNO_GENERAL_FAILURE;
        }

        /* If current measurement is less than target,
         * set minimum value to middle */
        if (current_readback_voltage < target)
        {
            min = current_setting;
        }
        /* If current measurement is greater than target,
         * set maximum value to middle */
        else if (current_readback_voltage > target)
        {
            max = current_setting;
        }

        previous_setting = current_setting;

    } while (max != min);

    return (ERRNO_GENERAL_FAILURE);
}

/* ----------------------------------------------------------------------------
 * Function      : void Calibrate_Power_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : The initialization function does the following tasks:
 *                 1) Changes settings in all power supply control registers
 *                    to their default values.
 *                 2) Sets the system clock source to RFCLK/3 (16 MHz).
 *                 3) Configures the ADC to enable measurement at 100 Hz
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : VBAT must be less than or equal to 1.3 V
 * ------------------------------------------------------------------------- */
void Calibrate_Power_Initialize(void)
{

    /* Reset the power supply configuration and modes to default settings. */
    /* VBG */
    ACS->BG_CTRL = (BG_SLOPE_TRIM_VALUE | BG_TRIM_0P750V);

    /* VCC */
    ACS->VCC_CTRL = (VCC_ICHTRIM_80MA | VCC_DCM_MODE | VCC_SINGLE_PULSE |
                     VCC_CONSTANT_IMAX | VCC_LDO | VCC_TRIM_1P31V);

    /* VDDA */
    ACS->VDDA_CP_CTRL = VDDA_PTRIM_4MA;

    /* VDDC */
    ACS->VDDC_CTRL = (VDDC_STANDBY_TRIM_1P10V | VDDC_NOMINAL_BIAS |
                      VDDC_SLEEP_HIZ | VDDC_TRIM_1P10V);

    /* VDDM */
    ACS->VDDM_CTRL = (VDDM_STANDBY_TRIM_1P10V | VDDM_NOMINAL_BIAS |
                      VDDM_SLEEP_HIZ | VDDM_TRIM_1P10V);

    /* VDDPA */
    ACS->VDDPA_CTRL = (VDDPA_SW_HIZ | VDDPA_TRIM_1P60V |
                       VDDPA_DISABLE | VDDPA_ISENSE_DISABLE);

    /* Startup 48 MHz XTAL oscillator, set as system oscillator */
    Calibrate_Clock_InitializeXTAL48M();

    /* VDDRET */
    ACS->VDDRET_CTRL = (VDDMRET_TRIM_VALUE | VDDMRET_DISABLE |
                        VDDTRET_TRIM_VALUE | VDDTRET_DISABLE |
                        VDDCRET_TRIM_VALUE | VDDCRET_DISABLE);

    /* Set VBG to calibrated value found in NVR4 */

    /* Configure ADC */
    Sys_ADC_Set_Config(ADC_VBAT_DIV2_NORMAL | ADC_NORMAL |
                        ADC_PRESCALE_200);

    /* Configure ADC_OFFSET to channel 6 - reduces error in ADC measurements */
    Sys_ADC_InputSelectConfig(6, ADC_POS_INPUT_GND | ADC_NEG_INPUT_GND);
}

/* ----------------------------------------------------------------------------
 * Function     : unsigned int Calibrate_Power_VBG(unsigned int adc_num,
 *                                                 uint32_t *adc_ptr,
 *                                                 uint32_t target)
 * ----------------------------------------------------------------------------
 * Description   : Calibrate the bandgap voltage (VBG) against a specified
 *                 VBAT supply voltage. VBG is the reference voltage for the
 *                 ADC, so it can be calibrated based on the ADC output
 *                 for a known voltage, which is VBAT.
 * Inputs        : - adc_num             - ADC channel number [0-7]
 *                 - adc_ptr             - Pointer to the ADC data register
 *                 - target              - Target voltage readback [10*mV]
 * Outputs       : return value          - Status code indicating whether the
 *                                         calibration succeeded
 * Assumptions   : - The target band-gap is calibrated by reading the
 *                   current VBAT supply using the ADC. The assumed VBAT
 *                   supply voltage is 1.25 V.
 *                 - Calibrate_Power_Initialize() has been called.
 * ------------------------------------------------------------------------- */
unsigned int Calibrate_Power_VBG(unsigned int adc_num,
                                 uint32_t *adc_ptr,
                                 uint32_t target)
{

    unsigned int result = 0;

    /* The minimum and maximum are swapped because we're calibrating to VBAT
     * which goes down relative to VBG, when VBG rises (eg. inversely
     * proportional). The maximum setting here is the setting that will give
     * the maximum measured output. The minimum setting here is the setting
     * that will give the minimum measured output.
     */
    uint8_t max_setting = BG_TRIM_0P675V_BYTE;
    uint8_t min_setting = BG_TRIM_0P833V_BYTE;

    /* Configure ADC channel to measure VBAT/2 */
    Sys_ADC_InputSelectConfig(adc_num, ADC_POS_INPUT_VBAT_DIV2 |
                               ADC_NEG_INPUT_GND);

    /* Convert target to mV. */
    target *= 10;

    /* Perform Binary Search to find calibration setting */
    result = Calibrate_Power_BinarySearch(target,
                                          max_setting,
                                          min_setting,
                                          adc_ptr,
                                          (uint8_t *)&ACS_BG_CTRL->VTRIM_BYTE);

    /* Exit with error code if Binary Search failed */
    if (result == ERRNO_GENERAL_FAILURE)
    {
        return (ERRNO_VBG_CAL_ERROR);
    }

    return ERRNO_NO_ERROR;
}

/* ----------------------------------------------------------------------------
 * Function     : unsigned int Calibrate_Power_VDDRF(unsigned int adc_num,
 *                                                   uint32_t *adc_ptr,
 *                                                   uint32_t target)
 * ----------------------------------------------------------------------------
 * Description   : Calibrate the radio front-end power supply (VDDRF).
 * Inputs        : - adc_num               - ADC channel number [0-7]
 *                 - adc_ptr               - Pointer to the ADC data register
 *                 - target                - Target voltage readback [10*mV]
 * Outputs       : return value            - Status code indicating whether the
 *                                           calibration succeeded
 * Assumptions   : - VBG has been calibrated.
 *                 - Calibrate_Power_Initialize() has been called.
 *                 - VCC is sufficiently high to trim VDDRF to the desired
 *                   value. This is because VCC supplies VDDRF.
 * ------------------------------------------------------------------------- */
unsigned int Calibrate_Power_VDDRF(unsigned int adc_num,
                                   uint32_t *adc_ptr,
                                   uint32_t target)
{
    unsigned int result = 0;

    /* Enter min and max trim settings, found from Product Spec */
    uint8_t max_setting = VDDRF_TRIM_1P38V_BYTE;
    uint8_t min_setting = VDDRF_TRIM_0P75V_BYTE;

    /* Configure ADC channel to measure AOUT */
    Sys_ADC_InputSelectConfig(adc_num, ADC_POS_INPUT_AOUT |
                               ADC_NEG_INPUT_GND);

    /* Enable VDDRF */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;

    /* Output VDDRF on AOUT */
    ACS->AOUT_CTRL = AOUT_VDDRF_SENSE;

    /* Convert target to mV. */
    target *= 10;

    /* Perform Binary Search to find appropriate trim setting */
    result = Calibrate_Power_BinarySearch(target,
                                          max_setting,
                                          min_setting,
                                          adc_ptr,
                                          (uint8_t *)&ACS_VDDRF_CTRL->VTRIM_BYTE);

    /* Exit with error code if Binary Search failed */
    if (result == ERRNO_GENERAL_FAILURE)
    {
        return (ERRNO_VDDRF_CAL_ERROR);
    }

    return (ERRNO_NO_ERROR);
}

/* ----------------------------------------------------------------------------
 * Function     : unsigned int Calibrate_Power_VDDPA(unsigned int adc_num,
 *                                                   uint32_t *adc_ptr,
 *                                                   uint32_t target)
 * ----------------------------------------------------------------------------
 * Description   : Calibrate the radio power amplifier power supply (VDDPA).
 * Inputs        : - adc_num               - ADC channel number [0-7]
 *                 - adc_ptr               - Pointer to the ADC data register
 *                 - target                - Target voltage readback [10*mV]
 * Outputs       : return value            - Status code indicating whether the
 *                                           calibration succeeded
 * Assumptions   : - VBG has been calibrated.
 *                 - Calibrate_Power_Initialize() has been called.
 * ------------------------------------------------------------------------- */
unsigned int Calibrate_Power_VDDPA(unsigned int adc_num,
                                   uint32_t *adc_ptr,
                                   uint32_t target)
{
    unsigned int result = 0;

    /* Enter min and max trim settings, found from Product Spec */
    uint8_t max_setting = VDDPA_TRIM_1P68V_BYTE;
    uint8_t min_setting = VDDPA_TRIM_1P05V_BYTE;
    /* Configure ADC channel to measure AOUT */
    Sys_ADC_InputSelectConfig(adc_num, ADC_POS_INPUT_AOUT |
                               ADC_NEG_INPUT_GND);

    /* Enable VDDPA */
    ACS->VDDPA_CTRL = VDDPA_ENABLE | VDDPA_ISENSE_ENABLE;

    /* Output VDDPA on AOUT */
    ACS->AOUT_CTRL = AOUT_VDDPA_SENSE;

    /* Convert target to mV. */
    target *= 10;

    /* Perform Binary Search to find calibration setting */
    result = Calibrate_Power_BinarySearch(target,
                                          max_setting,
                                          min_setting,
                                          adc_ptr,
                                          (uint8_t*)&ACS_VDDPA_CTRL->VTRIM_BYTE);

    /* Exit with error code if Binary Search failed */
    if (result == ERRNO_GENERAL_FAILURE)
    {
        return (ERRNO_VDDPA_CAL_ERROR);
    }

    return (ERRNO_NO_ERROR);
}

/* ----------------------------------------------------------------------------
 * Function     : unsigned int Calibrate_Power_DCDC(unsigned int adc_num,
 *                                                  uint32_t *adc_ptr,
 *                                                  uint32_t target)
 * ----------------------------------------------------------------------------
 * Description   : Calibrate the DC-DC converter (DCDC).
 * Inputs        : - adc_num               - ADC channel number [0-7]
 *                 - adc_ptr               - Pointer to the ADC data register
 *                 - target                - Target voltage readback [10*mV]
 * Outputs       : return value            - Status code indicating whether the
 *                                           calibration succeeded
 * Assumptions   : - VBG has been calibrated.
 *                 - Calibrate_Power_Initialize() has been called.
 * ------------------------------------------------------------------------- */
unsigned int Calibrate_Power_DCDC(unsigned int adc_num,
                                  uint32_t *adc_ptr,
                                  uint32_t target)
{
    unsigned int result = 0;

    /* Enter min and max trim settings, found from Product Spec */
    uint8_t max_setting = VCC_TRIM_1P31V_BYTE;
    uint8_t min_setting = VCC_TRIM_1P00V_BYTE;

    /* Configure ADC channel to measure AOUT */
    Sys_ADC_InputSelectConfig(adc_num, ADC_POS_INPUT_AOUT |
                               ADC_NEG_INPUT_GND);

    /* Output DCDC on AOUT */
    ACS->AOUT_CTRL = AOUT_VCC_SENSE;

    /* Convert target to mV. */
    target *= 10;

    /* Perform Binary Search to find calibration setting */
    result = Calibrate_Power_BinarySearch(target,
                                          max_setting,
                                          min_setting,
                                          adc_ptr,
                                          (uint8_t*)&ACS_VCC_CTRL->VTRIM_BYTE);

    /* Exit with error code if Binary Search failed */
    if (result == ERRNO_GENERAL_FAILURE)
    {
        return (ERRNO_DCDC_CAL_ERROR);
    }

    return (ERRNO_NO_ERROR);
}

/* ----------------------------------------------------------------------------
 * Function     : unsigned int Calibrate_Power_VDDC(unsigned int adc_num,
 *                                                  uint32_t *adc_ptr,
 *                                                  uint32_t target)
 * ----------------------------------------------------------------------------
 * Description   : Calibrate the digital core voltage power supply (VDDC).
 * Inputs        : - adc_num               - ADC channel number [0-7]
 *                 - adc_ptr               - Pointer to the ADC data register
 *                 - target                - Target voltage readback [10*mV]
 * Outputs       : return value            - Status code indicating whether the
 *                                           calibration succeeded
 * Assumptions   : - VBG has been calibrated.
 *                 - Calibrate_Power_Initialize() has been called.
 * ------------------------------------------------------------------------- */
unsigned int Calibrate_Power_VDDC(unsigned int adc_num,
                                  uint32_t *adc_ptr,
                                  uint32_t target)
{
    unsigned int result = 0;

    /* Enter min and max trim settings, found from Product Spec */
    uint8_t max_setting = VDDC_TRIM_1P38V_BYTE;
    uint8_t min_setting = VDDC_TRIM_0P75V_BYTE;

    /* Configure ADC channel to measure VDDC */
    Sys_ADC_InputSelectConfig(adc_num, ADC_POS_INPUT_VDDC |
                               ADC_NEG_INPUT_GND);

    /* Convert target to mV. */
    target *= 10;

    /* Increment the target by 8 mV to account for calibration error
     * and guarantee we are above the target, as defined in the spec */
    target += 8;

    /* Perform Binary Search to find calibration setting */
    result = Calibrate_Power_BinarySearch(target,
                                          max_setting,
                                          min_setting,
                                          adc_ptr,
                                          (uint8_t*)&ACS_VDDC_CTRL->VTRIM_BYTE);

    /* Exit with error code if Binary Search failed */
    if (result == ERRNO_GENERAL_FAILURE)
    {
        return (ERRNO_VDDC_CAL_ERROR);
    }

    return (ERRNO_NO_ERROR);
}

/* ----------------------------------------------------------------------------
 * Function     : unsigned int Calibrate_Power_VDDM(unsigned int adc_num,
 *                                                  uint32_t *adc_ptr,
 *                                                  uint32_t target)
 * ----------------------------------------------------------------------------
 * Description   : Calibrate the digital memory voltage (VDDM)
 * Inputs        : - adc_num               - ADC channel number [0-7]
 *                 - adc_ptr               - Pointer to the ADC data register
 *                 - target                - Target voltage readback [10*mV]
 * Outputs       : return value            - Status code indicating whether the
 *                                           calibration succeeded
 * Assumptions   : - VBG has been calibrated.
 *                 - Calibrate_Power_Initialize() has been called.
 * ------------------------------------------------------------------------- */
unsigned int Calibrate_Power_VDDM(unsigned int adc_num,
                                  uint32_t *adc_ptr,
                                  uint32_t target)
{
    unsigned int result = 0;

/* Enter min and max trim settings, found from Product Spec */
    uint8_t max_setting = VDDM_TRIM_1P38V_BYTE;
    uint8_t min_setting = VDDM_TRIM_0P75V_BYTE;

    /* Configure ADC channel to measure AOUT */
    Sys_ADC_InputSelectConfig(adc_num, ADC_POS_INPUT_AOUT |
                               ADC_NEG_INPUT_GND);

    /* Output VDDM on AOUT */
    ACS->AOUT_CTRL = AOUT_VDDM_SENSE;

    /* Convert target to mV. */
    target *= 10;

    /* Increment the target by 8 mV to account for the calibration error
     * and guarantee we are above the target, as defined in the spec */
    target += 8;

    /* Perform Binary Search to find calibration setting */
    result = Calibrate_Power_BinarySearch(target,
                                          max_setting,
                                          min_setting,
                                          adc_ptr,
                                          (uint8_t*)&ACS_VDDM_CTRL->VTRIM_BYTE);

    /* Exit with error code if Binary Search failed */
    if (result == ERRNO_GENERAL_FAILURE)
    {
        return (ERRNO_VDDM_CAL_ERROR);
    }

    return (ERRNO_NO_ERROR);
}
