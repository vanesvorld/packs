/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC
 * (d/b/a ON Semiconductor). All Rights Reserved.
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor. The
 * terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_calibrate_clock.c
 * - Clock calibration support functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.24 $
 * $Date: 2019/01/23 18:02:19 $
 * ------------------------------------------------------------------------- */

#include <rsl10_calibrate.h>

 /* ----------------------------------------------------------------------------
 * IMacro        : CONVERT_HZ_TO_CYCLES(x, y, z)   (((y) * (z) * 1000000)/(x))
 * ----------------------------------------------------------------------------
 * Description   : Calculates the number of cycles that will be returned by
 *                 the ASCC. The ASCC returns the number of SYSCLK cycles
 *                 between n number of periods in the input clock.
 * Inputs        : - x             - The value in Hz that must be
 *                                   converted to a cycle count
 *                 - y             - The frequency of the SYSCLK (MHz)
 *                 - z             - The number of periods that the ASCC
 *                                   measures
 * Outputs       : return value    - Number of system clock cycles between n
 *                                   periods of the input clock.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#define CONVERT_HZ_TO_CYCLES(x, y, z)   (((y) * (z) * 1000000)/(x))

 /* ----------------------------------------------------------------------------
 * IMacro        : CONVERT_MHZ_TO_CYCLES(x, y, z)  (((y) * (z))/(x))
 * ----------------------------------------------------------------------------
 * Description   : Calculates the number of cycles that will be returned by
 *                 the ASCC. The ASCC returns the number of SYSCLK cycles
 *                 between n number of periods in the input clock.
 * Inputs        : - x             - The value in MHz that must be
 *                                   converted to a cycle count
 *                 - y             - The frequency of the SYSCLK (Hz)
 *                 - z             - The number of periods that the ASCC
 *                                   measures.
 * Outputs       : return value    - Number of system clock cycles between n
 *                                   periods of the input clock.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#define CONVERT_MHZ_TO_CYCLES(x, y, z)  (((y) * (z))/(x))

 /* ----------------------------------------------------------------------------
 * IMacro        : CONVERT_CYCLES_TO_HZ(x, y, z)  (((y) * (z))/(x))
 * ----------------------------------------------------------------------------
 * Description   : Calculates the number of Hz that correspond to a certain
 *                 number of cycles as returned by the ASCC.
 * Inputs        : - x             - The cycle value from the ASCC that must be
 *                                   converted to Hz
 *                 - y             - The frequency of the SYSCLK (MHz)
 *                 - z             - The number of periods that the ASCC
 *                                   measures
 * Outputs       : return value    - Number of system clock cycles between n
 *                                   periods of the input clock.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#define CONVERT_CYCLES_TO_HZ(x, y, z)  (((y) * (z) * 1000000)/(x))

/* ----------------------------------------------------------------------------
 * IFunction      : static void Calibrate_Clock_InitializeXTAL48M(void)
 * ----------------------------------------------------------------------------
 * Description   : Initializes the 48MHz XTAL oscillator and configures the
 *                 system clock to RF clock oscillator.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void Calibrate_Clock_InitializeXTAL48M(void)
{
    /* Start 48 MHz XTAL oscillator */

    /* Enable VDDRF supply without changing trimming settings */
    *((volatile uint8_t *)&ACS->VDDRF_CTRL+1) = (VDDRF_ENABLE_BITBAND <<
                                                (ACS_VDDRF_CTRL_ENABLE_Pos - 8)) |
                                                (VDDRF_DISABLE_HIZ_BITBAND <<
                                                (ACS_VDDRF_CTRL_CLAMP_Pos - 8));

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_3_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS != ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to (divided 48 MHz) oscillator clock */
    Sys_Clocks_SystemClkConfig(SYSCLK_CLKSRC_RFCLK |
                               EXTCLK_PRESCALE_1   |
                               JTCK_PRESCALE_1);
    /* Configure clock dividers */
    CLK->DIV_CFG0 =  SLOWCLK_PRESCALE_16 | BBCLK_PRESCALE_1 | USRCLK_PRESCALE_1;

}

/* ----------------------------------------------------------------------------
 * IFunction     : static void Calibrate_Clock_InitializeASCC(void)
 * ----------------------------------------------------------------------------
 * Description   : Initializes the Audio Sink Clock Counter and configures it
 *                 to measure 16 audio sink clock periods.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : Calibrate_Clock_Initialize() has been called.
 *                 DIO0 is available to use as a source for the ADCC.
 * ------------------------------------------------------------------------- */
static void Calibrate_Clock_InitializeASCC(void)
{
    SYSCTRL->RF_ACCESS_CFG = RF_ACCESS_ENABLE_BITBAND | RF_IRQ_ACCESS_ENABLE;
    Sys_Audiosink_ResetCounters();
    Sys_Audiosink_InputClock(0, AUDIOSINK_CLK_SRC_STANDBYCLK);
    Sys_Audiosink_Config(AUDIO_SINK_PERIODS_16, 0, 0);

    AUDIOSINK->PHASE_CNT = 0;
    AUDIOSINK_CTRL->PHASE_CNT_START_ALIAS = 1;
    AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = 1;
}

/* ----------------------------------------------------------------------------
 * IFunction      : static void Calibrate_Clock_InitializeRCOSC(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the RC oscillator, resets the baseband timer and
 *                 configures RC oscillator as the real-time clock source.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void Calibrate_Clock_IntializeRCOSC(void)
{
    /* Start the RC oscillator */
    ACS->RCOSC_CTRL = (RC_OSC_ENABLE | RC_OSC_NOM);

    /* Reset the baseband timer */
    ACS->BB_TIMER_CTRL = BB_TIMER_RESET;

    /* Select the RC oscillator as the clock source */
    ACS->RTC_CTRL = (RTC_CLK_SRC_RC_OSC | RTC_ENABLE);
}

/* ----------------------------------------------------------------------------
 * IFunction      : static uint32_t Calibrate_Clock_GetNumPeriodCycles(void)
 * ----------------------------------------------------------------------------
 * Description   : Gets the number of cycles in a period of reference
 *                 signal using the audio sink clock counter.
 * Inputs        : None
 * Outputs       : return value   - Average number of clock cycles over 10 times
 *                                  16 audio sink clock periods
 * Assumptions   : Calibrate_Clock_Initialize() has been called.
 *                 Calibrate_Clock_InitializeASCC() has been called.
 *                 Trigger has been sent to audio sync clock counters.
 * ------------------------------------------------------------------------- */
static unsigned int Calibrate_Clock_GetNumPeriodCycles(void)
{
    uint32_t audio_sink_period_cnt = 0;

    while (AUDIOSINK_CTRL->PHASE_CNT_MISSED_STATUS_ALIAS ==
               PHASE_CNT_NORMAL_BITBAND)
    {
        Sys_Watchdog_Refresh();
    }

    audio_sink_period_cnt = Sys_Audiosink_PeriodCounter();

    AUDIOSINK->PERIOD_CNT = 0;

    return (audio_sink_period_cnt);
}

/* ----------------------------------------------------------------------------
 * IFunction      : static unsigned int Calibrate_Clock_BinarySearch
 *                                               (uint16_t target,
 *                                                uint16_t max,
 *                                                uint16_t min,
 *                                                uint32_t error,
 *                                                uint16_t *rcosc_ptr,
 *                                                unsigned int rcosc_sel)
 * ----------------------------------------------------------------------------
 * Description   : Perform a binary search to set the measured cycle counts
 *                 to match a specified target clock frequency.
 * Inputs        : - target        - The specified target clock cycles
 *                 - max           - The setting that gives the maximum
 *                                   measured cycle counts
 *                 - min           - The setting that gives the minimum
 *                                   measured cycle counts
 *                 - error         - The measurement error allowed for
 *                                   specified target frequency
 *                 - rcosc_ptr     - Pointer to the RC oscillator control
 *                                   register
 *                 - rcosc_sel     - The clock being selected for trimming
 * Outputs       : return value    - Status value indicating whether the
 *                                   calibration succeeded
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static unsigned int Calibrate_Clock_BinarySearch(uint16_t target,
                                                 uint16_t max,
                                                 uint16_t min,
                                                 uint32_t error,
                                                 uint16_t *rcosc_ptr,
                                                 unsigned int rcosc_sel)
{
    uint16_t current_setting = 0;
    uint16_t previous_setting = min;
    uint32_t cycle_count_readback = 0;
    uint32_t measured_error = 0;

    do
    {
        /* Set the middle to half of difference between min and max */
        current_setting = ((max - min) / 2) + min;

        if (rcosc_sel == CAL_32K_RCOSC)
        {
            /* Set FTRIM_SHORT register's FTRIM_32K bit-field to current setting */
            *rcosc_ptr = (current_setting);
        }
        else if (rcosc_sel == CAL_START_OSC)
        {
            /* Set FTRIM_SHORT register's FTRIM_START bit-field to current setting */
            *rcosc_ptr &= ~(ACS_RCOSC_CTRL_FTRIM_START_Mask);
            *rcosc_ptr = ((current_setting << ACS_RCOSC_CTRL_FTRIM_START_Pos)
                           | *rcosc_ptr);
        }
        else
        {
            return (ERRNO_GENERAL_FAILURE);
        }

        /* Determine the cycle count for this oscillator configuration*/
        Calibrate_Clock_InitializeASCC();

        /* ~10 mSec delay required between initialization and read
         * Assumes system clock of 16 MHz. Calculated as follows:
         * 10 [mSec] * 16*10^6 [cycles/sec] = 160000 [cycles] -> = 0x27100 */
        Sys_Delay_ProgramROM(0x27100U);

        /*Send trigger to audio sync clock counters */
        BBIF->SYNC_CFG = (ACTIVE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
        BBIF->SYNC_CFG = (ACTIVE | RX_ACTIVE   | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
        DIO->BB_RX_SRC &= (BB_RX_DATA_SRC_CONST_HIGH | ~DIO_BB_RX_SRC_RF_SYNC_P_Mask);
        DIO->BB_RX_SRC &= (BB_RX_DATA_SRC_CONST_LOW | ~DIO_BB_RX_SRC_RF_SYNC_P_Mask);
        BBIF->SYNC_CFG = (ACTIVE | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
        BBIF->SYNC_CFG = (IDLE   | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
        cycle_count_readback = Calibrate_Clock_GetNumPeriodCycles();

        /* Exit if the target frequency == cycle count */
        if (target == cycle_count_readback)
        {
            /* Calibration successful */
            return (ERRNO_NO_ERROR);
        }

        /* Determine the error in the measurement cycle count.*/
        if (target < cycle_count_readback)
        {
            measured_error = (cycle_count_readback - target);
        }
        else
        {
            measured_error = (target - cycle_count_readback);
        }

        /* Exit if the current measurement is within allowed
         * measurement error */
        if (measured_error <= error)
        {
            /* Calibration successful */
            return (ERRNO_NO_ERROR);
        }

        /* Exit if the current setting is the same as the previous setting
         * This means that the search cannot proceed further. The exception to
         * this exit condition is if the previous setting was (max-1)
         * because max cannot actually be reached due integer division. */
        if (previous_setting == current_setting)
        {
            if (current_setting == (max - 1))
            {
                current_setting = max;
            }
            else{
                /* Calibration not successful */
                return ERRNO_GENERAL_FAILURE;
            }
        }
        /* If current cycle count is greater than target,
         * set maximum value to middle */
        if (cycle_count_readback > target)
        {
            max = current_setting;
        }
        /* If current cycle count is less than target,
         * set minimum value to middle */
        else if (cycle_count_readback < target)
        {
            min = current_setting;
        }

        /* Set the previous setting to the current setting */
        previous_setting = current_setting;

        /* Reset measured cycle count to 0 */
        cycle_count_readback = 0;

    } while (max != min);

    return (ERRNO_GENERAL_FAILURE);
}

/* ----------------------------------------------------------------------------
 * Function      : void Calibrate_Clock_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system to support the clock calibration,
 *                 consisting of the 48 MHz XTAL oscillator and RC oscillator.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Calibrate_Clock_Initialize(void)
{
    /* Startup 48 MHz XTAL oscillator */
    Calibrate_Clock_InitializeXTAL48M();

    /* Initialize RC Oscillator*/
    Calibrate_Clock_IntializeRCOSC();
}

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Calibrate_Clock_32K_RCOSC(uint32_t target)
 * ----------------------------------------------------------------------------
 * Description   : Used to calibrate the 32K RC oscillator to a
 *                 specified frequency.
 * Inputs        : target         - Number of cycles required to achieve the
 *                                  Desired clock frequency in Hz
 * Outputs       : return value   - Status code indicating whether the
 *                                  RCOSC calibration succeeded
 * Assumptions   : Calibrate_Clock_Initialize() has been called.
 * ------------------------------------------------------------------------- */
unsigned int Calibrate_Clock_32K_RCOSC(uint32_t target)
{
    unsigned int result = 0;
    uint32_t clock_cycles = 0;
    uint32_t error = 0;
    uint32_t measured_error = 0;

    /* Initialize the max and min settings */
    /* The min and max settings are flipped because the clock frequency is
     * inversely proportional to the trim percentage present in the spec */
    uint16_t max_setting = RC_OSC_M48_BYTE;
    uint16_t min_setting = RC_OSC_P46P5_BYTE;

    /* Convert to number of cycles required to achieve the desired
     * calibrated frequency
     */
    target = CONVERT_HZ_TO_CYCLES(target, 16, AUDIO_SINK_PERIODS);

    /* Determine the cycle count for this oscillator configuration */
    Calibrate_Clock_InitializeASCC();
    BBIF->SYNC_CFG = (ACTIVE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    BBIF->SYNC_CFG = (ACTIVE | RX_ACTIVE   | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    DIO->BB_RX_SRC &= (BB_RX_DATA_SRC_CONST_HIGH | ~DIO_BB_RX_SRC_RF_SYNC_P_Mask);
    DIO->BB_RX_SRC &= (BB_RX_DATA_SRC_CONST_LOW | ~DIO_BB_RX_SRC_RF_SYNC_P_Mask);
    BBIF->SYNC_CFG = (ACTIVE | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    BBIF->SYNC_CFG = (IDLE   | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    clock_cycles = Calibrate_Clock_GetNumPeriodCycles();

    /* The error is scaled according to the target by multiplying it with the
     * minimum trimming step and multiplying that by 2 to achieve the
     * theoretical max precision and multiplied by 1.5 as a safety margin */
    error = (uint32_t)(target * TRIMMING_STEP*0.75);

    /* Compare the cycle count with the requested cycle count, and
     * exit if they are the equal*/
    if (target < clock_cycles)
    {
       measured_error = (clock_cycles - target);
    }
    else
    {
       measured_error = (target - clock_cycles);
    }

    /* Exit if the current measurement is within allowed
     * measurement error */
    if (measured_error <= error)
    {
        return (ERRNO_NO_ERROR);
    }

    /* Perform Binary Search to find calibration setting */
    result = Calibrate_Clock_BinarySearch(target,
                                          max_setting,
                                          min_setting,
                                          error,
                                          (uint16_t *)&ACS_RCOSC_CTRL->FTRIM_32K_BYTE,
                                          CAL_32K_RCOSC);

    /* Exit with error code if Binary Search failed */
    if (result == ERRNO_GENERAL_FAILURE)
    {
        return (ERRNO_RCOSC_CAL_ERROR);
    }

    return (ERRNO_NO_ERROR);
}

/* ----------------------------------------------------------------------------
 * Function      : unsigned int Calibrate_Clock_Start_OSC(uint32_t target)
 * ----------------------------------------------------------------------------
 * Description   : Used to calibrate the startup oscillator to a specified
 *                 frequency.
 * Inputs        : target         - Desired clock frequency in kHz
 * Outputs       : return value   - Status code indicating whether the
 *                                  clock succeeded
 * Assumptions   : Calibrate_Clock_Initialize() has been called.
 * ------------------------------------------------------------------------- */
unsigned int Calibrate_Clock_Start_OSC(uint32_t target)
{
    unsigned int result = 0;
    uint32_t clock_cycles = 0;
    uint32_t error = 0;
    uint32_t measured_error = 0;
    unsigned int standbyclk_freq;

    /* Initialize the max and min settings */
    uint16_t max_setting = RC_START_OSC_P46P5 >> ACS_RCOSC_CTRL_FTRIM_START_Pos;
    uint16_t min_setting = RC_START_OSC_M48 >> ACS_RCOSC_CTRL_FTRIM_START_Pos;

    /* Switch to (divided 48 MHz) oscillator clock */
    Sys_Clocks_SystemClkConfig(SYSCLK_CLKSRC_RFCLK |
                               EXTCLK_PRESCALE_1   |
                               JTCK_PRESCALE_1);

    /* Determine the cycle count for this oscillator configuration */
    Calibrate_Clock_InitializeASCC();
    /* ~10 mSec delay required between initialization and read
     * Assumes system clock of 16 MHz. Calculated as follows:
     * 10 [mSec] * 16*10^6 [cycles/sec] = 160000 [cycles] -> = 0x27100 */
    Sys_Delay_ProgramROM(0x27100U);
    BBIF->SYNC_CFG = (ACTIVE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    BBIF->SYNC_CFG = (ACTIVE | RX_ACTIVE   | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    DIO->BB_RX_SRC &= (BB_RX_DATA_SRC_CONST_HIGH | ~DIO_BB_RX_SRC_RF_SYNC_P_Mask);
    DIO->BB_RX_SRC &= (BB_RX_DATA_SRC_CONST_LOW | ~DIO_BB_RX_SRC_RF_SYNC_P_Mask);
    BBIF->SYNC_CFG = (ACTIVE | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    BBIF->SYNC_CFG = (IDLE   | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    clock_cycles = Calibrate_Clock_GetNumPeriodCycles();

    standbyclk_freq = CONVERT_CYCLES_TO_HZ(clock_cycles, 16, AUDIO_SINK_PERIODS);

    /* Switch to RC clock */
    Sys_Clocks_SystemClkConfig(SYSCLK_CLKSRC_RCCLK |
                               EXTCLK_PRESCALE_1   |
                               JTCK_PRESCALE_1);


    ACS_RCOSC_CTRL->FTRIM_32K_BYTE = (RC_START_OSC_NOM |
                                     ACS_RCOSC_CTRL->FTRIM_32K_BYTE);
    /* Convert target from kHz to Hz */
    target *= 1000;

    /* Convert to number of cycles required to achieve the desired
     * calibrated frequency. standbyclk_freq is the frequency we will measure
     * using the system clock in order to calibrate the system clock. */
    target = CONVERT_MHZ_TO_CYCLES(standbyclk_freq, target, AUDIO_SINK_PERIODS);

    /* Determine the cycle count for this oscillator configuration */
    Calibrate_Clock_InitializeASCC();
    /* ~10 mSec delay required between initialization and read
     * Assumes system clock of 16 MHz. Calculated as follows:
     * 10 [mSec] * 16*10^6 [cycles/sec] = 160000 [cycles] -> = 0x27100 */
    Sys_Delay_ProgramROM(0x27100U);
    BBIF->SYNC_CFG = (ACTIVE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    BBIF->SYNC_CFG = (ACTIVE | RX_ACTIVE   | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    DIO->BB_RX_SRC &= (BB_RX_DATA_SRC_CONST_HIGH | ~DIO_BB_RX_SRC_RF_SYNC_P_Mask);
    DIO->BB_RX_SRC &= (BB_RX_DATA_SRC_CONST_LOW | ~DIO_BB_RX_SRC_RF_SYNC_P_Mask);
    BBIF->SYNC_CFG = (ACTIVE | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    BBIF->SYNC_CFG = (IDLE   | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    clock_cycles = Calibrate_Clock_GetNumPeriodCycles();

    /* The 0.75 is calculated by dividing the trim step by 2, which gives the
     * max possible accuracy, and multiplying by 1.5 as a safety margin */
    error = (uint32_t)(target * TRIMMING_STEP *0.75);

    /* Compare the cycle count with the requested cycle count, and
     * exit if they are the equal*/
    if (target < clock_cycles)
    {
       measured_error = (clock_cycles - target);
    }
    else
    {
       measured_error = (target - clock_cycles);
    }

    /* Exit if the current measurement is within allowed
     * measurement error */
    if (measured_error <= error)
    {
        /* Calibration successful */
        return (ERRNO_NO_ERROR);
    }

    /* Perform Binary Search to find calibration setting */
    result = Calibrate_Clock_BinarySearch(target,
                                          max_setting,
                                          min_setting,
                                          error,
                                          (uint16_t *)&ACS_RCOSC_CTRL->FTRIM_32K_BYTE,
                                          CAL_START_OSC);

    /* Exit with error code if binary search failed */
    if (result == ERRNO_GENERAL_FAILURE)
    {
        return (ERRNO_START_OSC_CAL_ERROR);
    }

    return (ERRNO_NO_ERROR);
}
