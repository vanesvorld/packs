/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_calibrate.h
 * - Calibration support header
 * ----------------------------------------------------------------------------
 * $Revision: 1.10 $
 * $Date: 2017/05/25 21:02:50 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_CALIBRATE_H
#define RSL10_CALIBRATE_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * External include files
 * ------------------------------------------------------------------------- */
#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Internal include files
 * ------------------------------------------------------------------------- */
#include <rsl10_calibrate_power.h>
#include <rsl10_calibrate_clock.h>

/* ----------------------------------------------------------------------------
 * Firmware Calibration Library Version Code
 * ------------------------------------------------------------------------- */
#define CALIBRATE_FW_VER_MAJOR          0x01
#define CALIBRATE_FW_VER_MINOR          0x03
#define CALIBRATE_FW_VER_REVISION       0x02

#define CALIBRATE_FW_VER                ((CALIBRATE_FW_VER_MAJOR << 12) | \
                                         (CALIBRATE_FW_VER_MINOR << 8)  | \
                                          CALIBRATE_FW_VER_REVISION)

extern const short RSL10_CalibrateLib_Version;

/* ----------------------------------------------------------------------------
 * Definitions for possible calibration library error conditions
 * ------------------------------------------------------------------------- */
/* Power supply calibration related errors */
#define ERRNO_POWER_CAL_MARKER          0x10
#define ERRNO_VBG_CAL_ERROR             (0x0001 | ERRNO_POWER_CAL_MARKER)
#define ERRNO_VDDRF_CAL_ERROR           (0x0002 | ERRNO_POWER_CAL_MARKER)
#define ERRNO_VDDPA_CAL_ERROR           (0x0003 | ERRNO_POWER_CAL_MARKER)
#define ERRNO_DCDC_CAL_ERROR            (0x0004 | ERRNO_POWER_CAL_MARKER)
#define ERRNO_VDDC_CAL_ERROR            (0x0005 | ERRNO_POWER_CAL_MARKER)
#define ERRNO_VDDM_CAL_ERROR            (0x0006 | ERRNO_POWER_CAL_MARKER)

/* Clock calibration related errors */
#define ERRNO_CLK_CAL_MARKER            0x20
#define ERRNO_RCOSC_CAL_ERROR           (0x0001 | ERRNO_CLK_CAL_MARKER)
#define ERRNO_START_OSC_CAL_ERROR       (0x0002 | ERRNO_CLK_CAL_MARKER)

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_CALIBRATE_H */
