/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_romvect.c
 * - Wrapper functions for function vectors embedded in the Program ROM
 * ----------------------------------------------------------------------------
 * $Revision: 1.7 $
 * $Date: 2017/07/14 20:42:32 $
 * ------------------------------------------------------------------------- */

#include <rsl10.h>
#include <rsl10_flash_rom.h>

/* ----------------------------------------------------------------------------
 * Function      : BootROMStatus
 *                 Sys_BootROM_StrictStartApp(uint32_t* vect_table)
 * ----------------------------------------------------------------------------
 * Description   : Validate and start up an application using the Boot ROM.
 *                 Only start the application if application validation returns
 *                 BOOTROM_ERR_NONE.
 * Inputs        : vect_table - Pointer to the vector table at the start of an
 *                              application that will be validated and then
 *                              run.
 * Outputs       : return value - Status code indicating application validation
 *                                error if application cannot be started. If not
 *                                returning, the status code is written to the
 *                                top of the started application's stack to
 *                                capture non-fatal validation issues.
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
BootROMStatus Sys_BootROM_StrictStartApp(uint32_t* vect_table)
{
    BootROMStatus result;
    result = (*((BootROMStatus (**)(uint32_t*))
                ROMVECT_BOOTROM_VALIDATE_APP))(vect_table);

    /* Check to ensure that the BootROM error code indicated no errors */
    if (result != BOOTROM_ERR_NONE)
    {
        return result;
    }
    else
    {
        /* No errors were observed, use the regular start app function to boot
         * the validated application. */
        result = (*((BootROMStatus (**)(uint32_t*))
                    ROMVECT_BOOTROM_START_APP))(vect_table);

        /* If returning here, an error occurred that was unexpected. Return the
         * error code, but if no error was detected return
         * BOOTROM_ERR_FAILED_START_APP. */
        if (result == BOOTROM_ERR_NONE)
        {
            return BOOTROM_ERR_FAILED_START_APP;
        }
        else
        {
            return result;
        }
    }
}
