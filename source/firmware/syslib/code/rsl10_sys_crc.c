/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_crc.c
 * - Cyclic-Redundancy Check (CRC) peripheral support
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2017/07/05 16:26:49 $
 * ------------------------------------------------------------------------- */

#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_CRC_Check(uint32_t type, uint32_t base,
 *                                        uint32_t top)
 * ----------------------------------------------------------------------------
 * Description   : Check the CRC over the specified range assuming the last
 *                 bytes of the defined block contain the previously calculated
 *                 CRC
 * Inputs        : - type       - CRC mode; use CRC_32 or CRC_CCITT
 *                 - base       - Base of the range to be verified
 *                 - top        - Last byte in the range to be verified
 * Outputs       : return value - 0 if CRC check failed, 1 if CRC check passed,
 *                                2 if there's an error
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint32_t Sys_CRC_Check(uint32_t type, uint32_t base, uint32_t top)
{
    uint32_t result;
    uint32_t crc_val;

    if (type == CRC_32)
    {
        /* Pass the parameters for CRC calculation excluding the bytes
         * containing the block's CRC */
        result = Sys_CRC_Calc(type, base , top - 4, &crc_val);

        /* Check if the calculation function returned an error */
        if(result == 0)
        {
            return 2;
        }
        /* Compare the calculated CRC with the block's CRC */
        else if (*(uint32_t *)(top - 3) == crc_val)
        {
            return 1;
        }
    }
    else
    {
        result = Sys_CRC_Calc(type, base , top - 2, &crc_val);

        if(result == 0)
        {
            return 2;
        }
        else if (*(uint16_t *)(top - 1) == crc_val)
        {
            return 1;
        }
    }
    return 0;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_CRC_Calc(uint32_t type, uint32_t base,
 *                                        uint32_t top, uint32_t* crc_val)
 * ----------------------------------------------------------------------------
 * Description   : Calculate the CRC over the specified range
 * Inputs        : - type       - CRC mode; use CRC_32 or CRC_CCITT
 *                 - base       - Base of the range to be verified
 *                 - top        - Last byte in the range to be verified
 *                 - crc_val    - Address to store the calculated value at
 * Outputs       : return value - 0 if invalid input; 1 otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint32_t Sys_CRC_Calc(uint32_t type, uint32_t base, uint32_t top, uint32_t* crc_val)
{
    uint32_t i;

    /* Check if the CRC verification range is valid */
    if (base > top || base == top)
    {
        return 0;
    }
    else if (base > FLASH_NVR1_BASE && base < FLASH_NVR4_3_TOP)
    {
        if (top > FLASH_NVR4_3_TOP) return 0;
    }
    else if (base > FLASH_MAIN_BASE && base < FLASH_MAIN_TOP)
    {
        if (top > FLASH_MAIN_TOP) return 0;
    }
    else if (base > PRAM0_BASE && base < DSP_PRAM0_REMAPPED_TOP)
    {
        if (top > DSP_PRAM0_REMAPPED_TOP) return 0;
    }
    else if (base > DSP_PRAM0_BASE && base < DSP_PRAM3_TOP)
    {
        if (top > DSP_PRAM3_TOP) return 0;
    }
    else if (base > DSP_PRAM0_EXT_BASE && base < DSP_PRAM3_EXT_TOP)
    {
        if (top > DSP_PRAM3_EXT_TOP) return 0;
    }
    else if (base > DRAM0_BASE && base < BB_DRAM1_TOP)
    {
        if (top > BB_DRAM1_TOP) return 0;
    }
    else if (base > DSP_DRAM01_BASE && base < DRAM12_TOP)
    {
        if (top > DRAM12_TOP) return 0;
    }
    else if (base > DSP_DRAM4_REMAPPED_BASE && base < DRAM2_REMAPPED_TOP)
    {
        if (top > DRAM2_REMAPPED_TOP) return 0;
    }
    else
    {
        return 0;
    }

    /* Configure for the correct CRC type */
    if (type == CRC_32)
    {
        CRC->CTRL = CRC_32                      | \
                    CRC_LITTLE_ENDIAN           | \
                    CRC_BIT_ORDER_STANDARD      | \
                    CRC_FINAL_REVERSE_STANDARD  | \
                    CRC_FINAL_XOR_STANDARD;
        CRC->VALUE = CRC_32_INIT_VALUE;
    }
    else
    {
        CRC->CTRL = CRC_CCITT                   | \
                    CRC_LITTLE_ENDIAN           | \
                    CRC_BIT_ORDER_STANDARD      | \
                    CRC_FINAL_REVERSE_STANDARD  | \
                    CRC_FINAL_XOR_STANDARD;
        CRC->VALUE = CRC_CCITT_INIT_VALUE;
    }

    /* Add all the data to the CRC */
    for(i = base; i <= top; i++)
    {
        CRC->ADD_8 = *(uint8_t *)i;
    }

    /* Return the calculated CRC*/
    *crc_val = CRC->FINAL;

    /* Return 1 for Success */
    return 1;
}
