/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_uart.c
 * - UART interface support functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.4 $
 * $Date: 2017/07/05 16:26:49 $
 * ------------------------------------------------------------------------- */

#include <rsl10.h>

/*-----------------------------------------------------------------------------
 * Function      : void Sys_UART_Enable(uint32_t clk_speed, uint32_t baud,
 *                  uint32_t dma)
 *-----------------------------------------------------------------------------
 * Description   : Enable a UART at the specified baud rate.
 * Inputs        : - clk_speed  - clock speed
 *                 - baud       - Baud rate to which UART* is configured
 *                 - dma        - DMA mode enable; use
 *                                UART_DMA_MODE_[ DISABLE | ENABLE]
 * Outputs       : None
 * Assumptions   : None
 * -------------------------------------------------------------------------- */
void Sys_UART_Enable(uint32_t clk_speed, uint32_t baud, uint32_t dma)
{
    uint32_t arg;
    int prescaleEnableFlag;

    /* Enable the prescalar if the clock speed is greater than 2^10 times
     * faster than the baud rate */
    if (clk_speed / baud > 1024)
    {
        prescaleEnableFlag = 1;
    }
    else
    {
        prescaleEnableFlag = 0;
    }

    /* Calculate the speed based using a multiplier of 2^18, reduced
     * appropriately to extend the valid range of baud_rate */
    if (baud < 16384)
    {
        arg = ((baud << 14) * (1 + 11 * prescaleEnableFlag)) /
               (clk_speed >> (18 - 14));
    }
    else
    {
        arg = ((baud << 8) * (1 + 11 * prescaleEnableFlag)) /
               (clk_speed >> (18 - 8));
    }

    UART->CFG = (((arg << UART_CFG_PRESCALE_Pos) & UART_CFG_PRESCALE_Mask)  |
                   (prescaleEnableFlag << UART_CFG_PRESCALE_ENABLE_Pos)     |
                   (dma & (1 << UART_CFG_DMA_ENABLE_Pos))                   |
                   UART_ENABLE);
}
