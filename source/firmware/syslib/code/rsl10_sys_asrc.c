/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_asrc.c
 * - Asynchronous Sample Rate Converter block support
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2017/07/05 16:26:49 $
 * ------------------------------------------------------------------------- */

#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ASRC_ConfigRunTime(uint32_t cfg,
 *                                             uint32_t f_src, uint32_t f_sink)
 * ----------------------------------------------------------------------------
 * Description   : Configure the phase increment value according to the WDF
 *                 selection and the input frequencies. The mode is
 *                 calculated automatically.
 * Inputs        : - cfg        - The WDF type and the ASRC mode; use
 *                                [LOW_DELAY | WIDE_BAND],
 *                                [ASRC_INT_MODE | ASRC_DEC_MODE*]
 *                 - f_src      - Source frequency or source sample number
 *                 - f_sink     - Sink frequency or sink sample number
 *                 - diff_bit   - Number of shifts on the numerator of the
 *                                ASRC formula after the subtraction of (f_src)
 *                                and (x*f_sink). It must be calculated by the
 *                                user to prevent overflow and have the maximum
 *                                precision
 * Outputs       : None
 * Assumptions   : The ASRC mode must be selected according to f_src and f_sink
 *                 mode == ASRC_INT_MODE where (f_sink > f_src)
 *                 mode == ASRC_DEC_MODE1 where
 *                      (f_sink < f_src * 1.20 && f_sink > f_src * 0.8)
 *                 mode == ASRC_DEC_MODE2 where
 *                      (f_sink > f_src * 0.4 && f_sink < f_src))
 *                 mode == ASRC_DEC_MODE2 where f_sink < f_src * 0.4
 * ------------------------------------------------------------------------- */
void Sys_ASRC_ConfigRunTime(uint32_t cfg, int64_t f_src, int64_t f_sink,
                            int8_t diff_bit)
{
    uint8_t mode;
    int64_t asrc_phase_inc;
    mode = (cfg & ASRC_CFG_ASRC_MODE_Mask) >> ASRC_CFG_ASRC_MODE_Pos;

    /* Convert the input to an m8p24 format, multiplying by the required power
     * of 2 (given the mode selected and the number of bits of difference
     * between the source and sink frequencies). */
    asrc_phase_inc =
    /* If using decimation mode 2, use 4 left shifts (24 + 4) minus
    * the diff_bit */
    (mode == ASRC_DEC_MODE2) ?
    ((f_src - f_sink) << (28 - diff_bit) / f_sink) << diff_bit :
    /* If using decimation mode 3, use 3 left shifts (24 + 3) minus
     * the diff_bit */
    (mode == ASRC_DEC_MODE3) ?
    ((f_src - (f_sink<<1)) << (27 - diff_bit) / f_sink) << diff_bit :
    /* If using decimation mode 1, use 5 left shifts (24 + 5) minus
     * the diff_bit */
    ((f_src - f_sink) << (29 - diff_bit) / f_sink) << diff_bit;

    asrc_phase_inc &= 0xFFFFFFFF;

    Sys_ASRC_Config(asrc_phase_inc , cfg);
}
