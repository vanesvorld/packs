/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_version.c
 * - RSL10 system version constant
 * ----------------------------------------------------------------------------
 * $Revision: 1.3 $
 * $Date: 2017/05/25 21:02:58 $
 * ------------------------------------------------------------------------- */

#include <rsl10.h>

const short RSL10_Sys_Version = RSL10_SYS_VER;
