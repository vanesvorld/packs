/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_audio.c
 * - AUDIO peripheral support
 * ----------------------------------------------------------------------------
 * $Revision: 1.11 $
 * $Date: 2017/08/11 15:38:41 $
 * ------------------------------------------------------------------------- */

#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Audio_ODDIOConfigMult(uint32_t cfg,
 *                          uint32_t * od_p, uint32_t * od_n, uint32_t num)
 * ----------------------------------------------------------------------------
 * Description   : Configure multiple sets of DIOs for the specified OD data
 *                 output selection
 * Inputs        : - cfg        - DIO pin configuration for the OD outputs
 *                 - od_p       - Pointer to the DIOs array to use as the OD
 *                                positive pins
 *                 - od_n       - Pointer to the DIOs array to use as the OD
 *                                negative pins
 *                 - num        - Number of pairs of DIO pins used for the OD
 * Outputs       : None
 * Assumptions   : The arrays od_p and od_n are the same length
 * ------------------------------------------------------------------------- */
void Sys_Audio_ODDIOConfigMult(uint32_t cfg, uint32_t * od_p,
                               uint32_t * od_n, uint32_t num)
{
    uint8_t i;
    for (i = 0; i < num; i++)
    {
        Sys_Audio_ODDIOConfig(cfg, *od_p++ , *od_n++);
    }
}
