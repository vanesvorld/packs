/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_gpio.h
 * - General purpose input/output interface support
 * ----------------------------------------------------------------------------
 * $Revision: 1.6 $
 * $Date: 2017/07/26 21:01:33 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_GPIO_H
#define RSL10_SYS_GPIO_H

#define DIO_CFG_IO_MODE_GPIO_Mask           (DIO_CFG_IO_MODE_Mask & ~0x3)

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * GPIO interface support functions
 * ------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------
 * Function      : void Sys_GPIO_Set_High(uint32_t gpio_pin)
 * ----------------------------------------------------------------------------
 * Description   : Set the specified GPIO output value to high
 * Inputs        : gpio_pin - GPIO pin to set high
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_GPIO_Set_High(uint32_t gpio_pin)
{
    if(((uint8_t)DIO->CFG[gpio_pin] & DIO_CFG_IO_MODE_GPIO_Mask) == 0)
    {
        DIO->CFG[gpio_pin] =  DIO->CFG[gpio_pin] | 0x1;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_GPIO_Set_Low(uint32_t gpio_pin)
 * ----------------------------------------------------------------------------
 * Description   : Set the specified GPIO output value to low
 * Inputs        : gpio_pin - GPIO pin to set low
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_GPIO_Set_Low( uint32_t gpio_pin )
{
    if(((uint8_t)DIO->CFG[gpio_pin] & DIO_CFG_IO_MODE_GPIO_Mask) == 0)
    {
        DIO->CFG[gpio_pin] =  DIO->CFG[gpio_pin] & ~0x1;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_GPIO_Toggle(uint32_t gpio_pin)
 * ----------------------------------------------------------------------------
 * Description   : Toggle the current value of the specified GPIO output
 * Inputs        : gpio_pin - GPIO pin to toggle
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_GPIO_Toggle(uint32_t gpio_pin)
{
    if(((uint8_t)DIO->CFG[gpio_pin] & DIO_CFG_IO_MODE_GPIO_Mask) == 0)
    {
        DIO->CFG[gpio_pin] =  DIO->CFG[gpio_pin] ^ 0x1;
    }
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_GPIO_H */
