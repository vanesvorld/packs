/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_cm3.h
 * - Additions to the NVIC support provided in core_cmFunc.h
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2018/09/20 18:05:36 $
 * ------------------------------------------------------------------------- */ 

#ifndef RSL10_SYS_CM3_H
#define RSL10_SYS_CM3_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 *   ARM Cortex-M3 processor support macros
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Macro         : SYS_WAIT_FOR_EVENT
 * ----------------------------------------------------------------------------
 * Description   : Hold the ARM Cortex-M3 core waiting for an event, interrupt
 *                 request, abort or debug entry request (ARM Thumb-2 WFE
 *                 instruction)
 * Inputs        : None
 * Outputs       : None
 * ------------------------------------------------------------------------- */
#define SYS_WAIT_FOR_EVENT              __ASM("wfe")

/* ----------------------------------------------------------------------------
 * Macro         : SYS_WAIT_FOR_INTERRUPT
 * ----------------------------------------------------------------------------
 * Description   : Hold the ARM Cortex-M3 core waiting for an interrupt
 *                 request, abort or debug entry request (ARM Thumb-2 WFI
 *                 instruction)
 * Inputs        : None
 * Outputs       : None
 * ------------------------------------------------------------------------- */
#define SYS_WAIT_FOR_INTERRUPT          __ASM("wfi")

/* ----------------------------------------------------------------------------
 * Macro         : SYS_BOOTROM_STARTAPP_RETURN
 * ----------------------------------------------------------------------------
 * Description   : Read the start application return code from the application
 *                 stack for the current application
 * Inputs        : None
 * Outputs       : return value - The value stored on the top of the stack
 *                                (uint32_t); compare against BOOTROM_ERR_* or
 *                                SYS_INIT_ERR_*
 * ------------------------------------------------------------------------- */
#define SYS_BOOTROM_STARTAPP_RETURN     *(*((uint32_t **)SCB->VTOR) - 1)

/* ----------------------------------------------------------------------------
 * Additions to the NVIC support provided in core_cmFunc.h
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_NVIC_DisableAllInt(void)
 * ----------------------------------------------------------------------------
 * Description   : Disable all external interrupts
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_NVIC_DisableAllInt(void)
{
    NVIC->ICER[0] = 0xFFFFFFFFU;
    NVIC->ICER[1] = 0xFFFFFFFFU;
    NVIC->ICER[2] = 0xFFFFFFFFU;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_NVIC_ClearAllPendingInt(void)
 * ----------------------------------------------------------------------------
 * Description   : Clear the pending status for all external interrupts
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_NVIC_ClearAllPendingInt(void)
{
    NVIC->ICPR[0] = 0xFFFFFFFFU;
    NVIC->ICPR[1] = 0xFFFFFFFFU;
    NVIC->ICPR[2] = 0xFFFFFFFFU;    
} 

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_CM3_H */
