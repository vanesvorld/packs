/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_dma.h
 * - Direct Memory Access (DMA) peripheral support
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2018/02/26 19:10:27 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_DMA_H
#define RSL10_SYS_DMA_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Direct Memory Access (DMA) peripheral support defines
 * ------------------------------------------------------------------------- */
/* Use this define as the source address for a DMA when initializing a memory
 * to zero. */
#define DMA_ZERO_INIT                   (&SYSCTRL->DSS_CMD)

/* ----------------------------------------------------------------------------
 * Direct Memory Access (DMA) peripheral support function prototypes
 * ------------------------------------------------------------------------- */
extern void Sys_DMA_ChannelConfig(uint32_t num, uint32_t cfg,
                        uint32_t transferLength, uint32_t counterInt,
                        uint32_t srcAddr, uint32_t destAddr);


/* ----------------------------------------------------------------------------
 * Direct Memory Access (DMA) peripheral support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : uint8_t Sys_DMA_Get_ChannelStatus(uint32_t num)
 * ----------------------------------------------------------------------------
 * Description   : Get the current status of the DMA channel
 * Inputs        : num          - The DMA channel number
 * Outputs       : return value - The current status of the specified DMA
 *                                channel
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint16_t Sys_DMA_Get_ChannelStatus(uint32_t num)
{
    return DMA->STATUS[num];
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DMA_ClearChannelStatus(uint32_t num)
 * ----------------------------------------------------------------------------
 * Description   : Clear the current status for the specified DMA channel
 * Inputs        : num      - The DMA channel number; use 0- 7
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DMA_ClearChannelStatus(uint32_t num)
{
    DMA->STATUS[num] =
            (DMA_ERROR_INT_CLEAR           |
             DMA_COMPLETE_INT_CLEAR        |
             DMA_COUNTER_INT_CLEAR         |
             DMA_START_INT_CLEAR           |
             DMA_DISABLE_INT_CLEAR);
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DMA_ClearAllChannelStatus(void)
 * ----------------------------------------------------------------------------
 * Description   : Clear the current status for the DMA on all channels
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DMA_ClearAllChannelStatus(void)
{
    uint32_t num;
    for(num=0; num<7; num++)
         Sys_DMA_ClearChannelStatus(num);
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DMA_Set_ChannelSourceAddress(uint32_t num,
 *                                                       uint32_t srcAddr)
 * ----------------------------------------------------------------------------
 * Description   : Set the base source address for the specified DMA channel
 * Inputs        : - num     - The DMA channel number
 *                 - srcAddr - Base source address for the DMA transfer
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DMA_Set_ChannelSourceAddress(uint32_t num, uint32_t srcAddr)
{
    DMA->SRC_BASE_ADDR[num] = srcAddr;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DMA_Set_ChannelDestAddress(uint32_t num,
 *                                                     uint32_t destAddr)
 * ----------------------------------------------------------------------------
 * Description   : Set the base destination address for the specified DMA
 *                 channel
 * Inputs        : - num      - The DMA channel number
 *                 - destAddr - Base destination address for the DMA transfer
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DMA_Set_ChannelDestAddress(uint32_t num, uint32_t destAddr)
{
    DMA->DEST_BASE_ADDR[num] = destAddr;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DMA_ChannelEnable(uint32_t num)
 * ----------------------------------------------------------------------------
 * Description   : Enable the DMA channel
 * Inputs        : num              - The DMA channel number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DMA_ChannelEnable(uint32_t num)
{
    DMA_CTRL0[num].ENABLE_ALIAS = DMA_ENABLE_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_DMA_ChannelDisable(uint32_t num)
 * ----------------------------------------------------------------------------
 * Description   : Disable the DMA channel
 * Inputs        : num             - The DMA channel number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_DMA_ChannelDisable(uint32_t num)
{
    DMA_CTRL0[num].ENABLE_ALIAS = DMA_DISABLE_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_DMA_H */
