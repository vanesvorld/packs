/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_protocol.h
 * - BLE protocol support
 * ----------------------------------------------------------------------------
 * $Revision: 1.10 $
 * $Date: 2019/05/06 19:56:46 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_PROTOCOL_H
#define RSL10_PROTOCOL_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/

typedef enum
{
    FLASH_PROVIDED_or_DFLT,
    APP_PROVIDED,
} DEVICE_PARAM_SRC_TYPE;

typedef enum
{
    PARAM_ID_PUBLIC_BLE_ADDRESS,
    PARAM_ID_IRK,
    PARAM_ID_CSRK,
    PARAM_ID_PRIVATE_KEY,
    PARAM_ID_PUBLIC_KEY,

    PARAM_ID_BLE_CA_TIMER_DUR,
    PARAM_ID_BLE_CRA_TIMER_CNT,
    PARAM_ID_BLE_CA_MIN_THR,
    PARAM_ID_BLE_CA_MAX_THR,
    PARAM_ID_BLE_CA_NOISE_THR,

} DEVICE_PARAM_ID;

typedef struct
{
    uint8_t device_param_src_type;
    uint8_t bleAddress[6];
    uint8_t irk[16];
    uint8_t csrk[16];
    uint8_t privateKey[32];
    uint8_t publicKey_x[32];
    uint8_t publicKey_y[32];

    uint8_t chnlAsses_param_src_type;
    uint16_t chnlAsses_timer_duration;
    uint8_t chnlAsses_timer_cnt;
    uint8_t chnlAsses_min_thr;
    uint8_t chnlAsses_max_thr;
    uint8_t chnlAsses_noise_thr;

} app_device_param_t;

typedef struct
{
    uint32_t adv_ifs;
    uint32_t forcedClockAccuracy;
    uint16_t clockAccuracy;
    uint8_t maxNumRAL;
    uint8_t max_rx_octets;
    uint16_t max_rx_time;
    uint8_t fixedAdvIntervalDelayEnable;
    uint8_t slaveLatencyDelay;

} ble_deviceParam_t;

extern ble_deviceParam_t ble_deviceParam;

/* ----------------------------------------------------------------------------
 * Function prototypes
 * ------------------------------------------------------------------------- */

extern void Device_Param_Prepare(app_device_param_t *param);
extern uint8_t Device_Param_Read(uint8_t requestedId, uint8_t *buf);
extern void BLE_DeviceParam_Set_ADV_IFS(uint32_t adv_ifs);
extern void BLE_DeviceParam_Set_ClockAccuracy(uint16_t clockAccuracy);
extern void BLE_DeviceParam_Set_ForcedClockAccuracy(uint32_t forcedClockAccuracy);
extern void BLE_DeviceParam_Set_MaxNumRAL(uint8_t maxNumRAL);
extern uint8_t BLE_DeviceParam_Set_MaxRxOctet(uint8_t maxRxOctet, uint16_t maxRxTime);
extern void BLE_DeviceParam_Set_AdvDelay(uint8_t fixedDelayEnable);
extern void BLE_DeviceParam_Set_SlaveLatencyDelay(uint8_t latencyDelay);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_PROTOCOL_H */
