/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_rffe.h
 * - RF front-end functions and macros
 * ----------------------------------------------------------------------------
 * $Revision: 1.31 $
 * $Date: 2018/10/15 19:42:51 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_RFFE_H
#define RSL10_SYS_RFFE_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/
#define ERRNO_RFFE_MARKER                0x300
#define ERRNO_RFFE_INVALIDSETTING_ERROR  (0x0010 | ERRNO_RFFE_MARKER)
#define ERRNO_RFFE_MISSINGSETTING_ERROR  (0x0020 | ERRNO_RFFE_MARKER)
#define ERRNO_RFFE_INSUFFICIENTVCC_ERROR (0x0030 | ERRNO_RFFE_MARKER)

#define RFFE_DCDC_1DBM_TARGET            120    /* 1.20V   */
#define RFFE_DCDC_2DBM_TARGET            125    /* 1.25V   */
#define RFFE_DCDC_DEFAULT_VDDRF_TARGET   115    /* 1.12V   */
#define RFFE_DCDC_THRESHOLD              112    /* 1.12V   */
#define RFFE_VDDRF_SENSITIVITY_TARGET    110    /* 1.10V   */
#define RFFE_VDDRF_LOWVCC_TARGET         105    /* 1.05V   */
#define RFFE_VDDRF_TOLERANCE             975    /* 97.5%  */
#define RFFE_VDDRF_TRIM_STEP             0x1    /* 1.00V   */

/* Max MANU_INFO_VCC Trim Version is 22*/
#define VCC_VERSION_THRESHOLD            22

/* Define a few helper contants used in the calculation of interpolated values
 * for TX power in the range 0 to -20, and PAPWR settings in the range -1 to 11:
 * - RFFE_PAPWR_LICOEFF_ALPHA = (y1 - y0) / (x1 - x0),
 * - RFFE_PAPWR_LICOEFF_BETA =  y0- ALPHA * x0 */
#define RFFE_PAPWR_LICOEFF_ALPHA         (int)(((11 + 1) * 10) / (0 + 20))
#define RFFE_PAPWR_LICOEFF_BETA          (int)((-1 * 10) + (RFFE_PAPWR_LICOEFF_ALPHA * 20))

/* Corresponds to sample rate of the ADC as configured (100 Hz) */
#define ADC_MEASUREMENT_DELAY            0x2710

/* Obsolete defines, renamed in rsl10_hw_*.h */
#define RF_REG2E_RSSI_DETECT_EN_ABS_RSSI_DETECT_Pos 1
#define RF_REG2E_RSSI_DETECT_EN_DIFF_RSSI_DETECT_Pos 0

#define RSSI_DETECT_EN_DIFF_RSSI_DETECT_DEFAULT ((uint32_t)(0x0U << RF_REG2E_RSSI_DETECT_EN_DIFF_RSSI_DETECT_Pos))
#define RSSI_DETECT_EN_ABS_RSSI_DETECT_DEFAULT ((uint32_t)(0x0U << RF_REG2E_RSSI_DETECT_EN_ABS_RSSI_DETECT_Pos))

/* Handy trim values necessary for uncalibrated boards */
#define VCC_TRIM_1P12V_BYTE                 ((uint8_t)(0x0CU << ACS_VCC_CTRL_VTRIM_BYTE_Pos))
#define VDDRF_TRIM_1P05V_BYTE               ((uint8_t)(0x1EU << ACS_VDDRF_CTRL_VTRIM_BYTE_Pos))
#define VDDRF_TRIM_1P07V_BYTE               ((uint8_t)(0x20U << ACS_VDDRF_CTRL_VTRIM_BYTE_Pos))
#define VDDRF_TRIM_1P13V_BYTE               ((uint8_t)(0x26U << ACS_VDDRF_CTRL_VTRIM_BYTE_Pos))
#define VDDPA_TRIM_1P26V_BYTE               ((uint8_t)(0x15U << ACS_VDDPA_CTRL_VTRIM_BYTE_Pos))
#define VDDPA_TRIM_1P35V_BYTE               ((uint8_t)(0x21U << ACS_VDDPA_CTRL_VTRIM_BYTE_Pos))
#define VDDPA_TRIM_1P45V_BYTE               ((uint8_t)(0x28U << ACS_VDDPA_CTRL_VTRIM_BYTE_Pos))

/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/
extern const uint8_t targetVoltage[];

/* ----------------------------------------------------------------------------
 * Function prototypes
 * --------------------------------------------------------------------------*/
extern unsigned int Sys_RFFE_SetTXPower(int target);

/* ----------------------------------------------------------------------------
 * RF front-end inline functions
 * ------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------
 * Function      : void Sys_RFFE_SPIDIOConfig(uint32_t cfg,
 *                                  uint32_t mosi, uint32_t csn, uint32_t clk,
 *                                  uint32_t miso)
 * ----------------------------------------------------------------------------
 * Description   : Configure the SPI slave for the RF front-end to use DIOs as
 *                 the SPI master source
 * Inputs        : - cfg       - DIO pin configuration for the output pads
 *                 - mosi      - DIO to use as the MOSI pad
 *                 - csn       - DIO to use as the CSN pad
 *                 - clk       - DIO to use as the CLK pad
 *                 - miso      - DIO to use as the MISO pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_RFFE_SPIDIOConfig(uint32_t cfg,
                                           uint32_t mosi, uint32_t csn, uint32_t clk,
                                           uint32_t miso)
{
    Sys_DIO_Config(mosi, ((cfg) | (DIO_MODE_INPUT)));
    Sys_DIO_Config(csn, ((cfg) | (DIO_MODE_INPUT)));
    Sys_DIO_Config(clk, ((cfg) | (DIO_MODE_INPUT)));
    DIO->RF_SPI_SRC = (((mosi << DIO_RF_SPI_SRC_MOSI_Pos) &
                        DIO_RF_SPI_SRC_MOSI_Mask)       |
                       ((csn << DIO_RF_SPI_SRC_CSN_Pos) &
                        DIO_RF_SPI_SRC_MOSI_Mask)       |
                       ((clk << DIO_RF_SPI_SRC_CLK_Pos) &
                        DIO_RF_SPI_SRC_MOSI_Mask));
    Sys_DIO_Config(miso, ((cfg) | (DIO_MODE_RF_SPI_MISO)));
}

/* ----------------------------------------------------------------------------
 * Function      :  void Sys_RFFE_OutputDIOConfig(uint32_t num,
 *                           uint32_t cfg, uint32_t gpio)
 * ----------------------------------------------------------------------------
 * Description   : Configure DIO pads connected to RF front-end GPIO
 * Inputs        : - num       - GPIO interface pad to configure; use 0 to 9
 *                 - cfg       - DIO pin configuration for the output pads
 *                 - gpo       - DIO to use as the GPO pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_RFFE_OutputDIOConfig(uint32_t num,
                                              uint32_t cfg, uint32_t gpio)
{
    Sys_DIO_Config(gpio, ((cfg) | (DIO_MODE_RF_GPIO0 +
                                   (uint32_t)(((num << DIO_CFG_IO_MODE_Pos) &
                                               DIO_CFG_IO_MODE_Mask)))));
}

/* ----------------------------------------------------------------------------
 * Function      :  void Sys_RFFE_InputDIOConfig(uint32_t num, uint32_t cfg,
 *                      uint32_t gpi)
 * ----------------------------------------------------------------------------
 * Description   : Configure a DIO pad as an RF front-end general-purpose input
 * Inputs        : - num       - GPIO interface pad to configure; use 0 to 9
 *                 - cfg       - DIO pin configuration for the output pads
 *                 - gpi       - DIO to use as the GPI pad; use
 *                               RF_GPIO[0:9]_SRC_DIO_[0:15], and
 *                               RF_GPIO[0:9]_SRC_CONST_[LOW | HIGH]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_RFFE_InputDIOConfig(uint32_t num, uint32_t cfg,
                                             uint32_t gpi)
{
    Sys_DIO_Config(gpi, ((cfg) | (DIO_MODE_INPUT)));
    *(uint8_t *)(&(DIO->RF_GPIO03_SRC) + num) = (gpi & 0xFF);
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif /* ifdef __cplusplus */

#endif    /* RSL10_SYS_RFFE_H */
