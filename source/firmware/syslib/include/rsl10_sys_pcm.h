/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_pcm.h
 * - PCM interface support
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2017/08/03 15:08:38 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_PCM_H
#define RSL10_SYS_PCM_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * PCM interface support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_PCM_Config(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the PCM interface
 * Inputs        : cfg      - Interface operation configuration; use
 *                            PCM_SAMPLE_[FALLING | RISING]_EDGE,
 *                            PCM_BIT_ORDER_[MSB | LSB]_FIRST,
 *                            PCM_TX_ALIGN_[MSB | LSB],
 *                            PCM_WORD_SIZE_*,
 *                            PCM_FRAME_ALIGN_[LAST | FIRST],
 *                            PCM_FRAME_WIDTH_[SHORT | LONG],
 *                            PCM_MULTIWORD_*,
 *                            PCM_SUBFRAME_[ENABLE | DISABLE],
 *                            PCM_CONTROLLER_[CM3 | DMA],
 *                            PCM_[DISABLE | ENABLE],
 *                            and PCM_SELECT_[MASTER | SLAVE]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PCM_Config(uint32_t cfg)
{
    PCM->CTRL = (cfg    &  ((1U << PCM_CTRL_PCM_CLK_POL_Pos)            |
                            (1U << PCM_CTRL_BIT_ORDER_Pos)              |
                            (1U << PCM_CTRL_TX_ALIGN_Pos)               |
                            PCM_CTRL_WORD_SIZE_Mask                     |
                            (1U << PCM_CTRL_FRAME_ALIGN_Pos)            |
                            (1U << PCM_CTRL_FRAME_WIDTH_Pos)            |
                            PCM_CTRL_FRAME_LENGTH_Mask                  |
                            (1U << PCM_CTRL_FRAME_SUBFRAMES_Pos)        |
                            (1U << PCM_CTRL_CONTROLLER_Pos)             |
                            (1U << PCM_CTRL_ENABLE_Pos)                 |
                            (1U << PCM_CTRL_SLAVE_Pos)));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_PCM_Enable(void)
 * ----------------------------------------------------------------------------
 * Description   : Enable the PCM interface without changing other PCM
 *                 configuration settings
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PCM_Enable(void)
{
    PCM_CTRL->ENABLE_ALIAS = PCM_ENABLE_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_PCM_Disable(void)
 * ----------------------------------------------------------------------------
 * Description   : Disable the PCM interface without changing other PCM
 *                 configuration settings
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PCM_Disable(void)
{
    PCM_CTRL->ENABLE_ALIAS = PCM_DISABLE_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_PCM_Get_Status(void)
 * ----------------------------------------------------------------------------
 * Description   : Get the current PCM interface status
 * Inputs        : None
 * Outputs       : Return value - The current PCM interface status
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_PCM_Get_Status(void)
{
    return PCM->STATUS;
}

/* ----------------------------------------------------------------------------
 * Function      :  void Sys_PCM_ClearStatus(void)
 * ----------------------------------------------------------------------------
 * Description   : Clear the current PCM interface status
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PCM_ClearStatus(void)
{
    PCM->STATUS = (PCM_UNDERRUN_CLEAR   | PCM_OVERRUN_CLEAR |
                   PCM_RECEIVE_CLEAR    | PCM_TRANSMIT_CLEAR);
}


/* ----------------------------------------------------------------------------
 * Function      : void Sys_PCM_ConfigClk(uint32_t slave, uint32_t cfg,
 *                                               uint32_t clk, uint32_t frame,
 *                                               uint32_t seri, uint32_t sero,
 *                                               uint32_t clksrc)
 * ----------------------------------------------------------------------------
 * Description   : Configure four DIOs for the PCM interface and clock source
 *                 selection
 * Inputs        : - slave    - PCM master/slave configuration; use
 *                              PCM_SELECT_[MASTER | SLAVE]
 *                 - cfg      - DIO pin configuration for the PCM pads
 *                 - clk      - DIO to use as the PCM clock pad
 *                 - frame    - DIO to use as the PCM frame pad
 *                 - seri     - DIO to use as the PCM serial input pad
 *                 - sero     - DIO to use as the PCM serial output pad
 *                 - clksrc   - Clock source for PCM; use
 *                              DIO_MODE_[*CLK | INPUT]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PCM_ConfigClk(uint32_t slave, uint32_t cfg,
                                              uint32_t clk, uint32_t frame,
                                              uint32_t seri, uint32_t sero,
                                              uint32_t clksrc)
{
    if ( slave == PCM_SELECT_MASTER)
    {
        Sys_DIO_Config(frame, ((cfg) | DIO_MODE_PCM_FRAME));
    }
    else
    {
        Sys_DIO_Config(frame, ((cfg) | DIO_MODE_INPUT));
    }
    DIO->PCM_SRC = (((seri << DIO_PCM_SRC_SERI_Pos) &
                            DIO_PCM_SRC_SERI_Mask)      |
                        ((frame << DIO_PCM_SRC_FRAME_Pos) &
                            DIO_PCM_SRC_FRAME_Mask)     |
                        ((clk << DIO_PCM_SRC_CLK_Pos) &
                            DIO_PCM_SRC_CLK_Mask));
    Sys_DIO_Config(clk, ((cfg) | clksrc));
    Sys_DIO_Config(seri, ((cfg) | DIO_MODE_GPIO_IN_0));
    Sys_DIO_Config(sero, ((cfg) | DIO_MODE_PCM_SERO));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_PCM_DIOConfig(uint32_t slave, uint32_t cfg,
 *                                         uint32_t clk, uint32_t frame,
 *                                         uint32_t seri, uint32_t sero)
 * ----------------------------------------------------------------------------
 * Description   : Configure four DIOs for the PCM interface
 * Inputs        : - slave  - PCM master/slave configuration; use
 *                            PCM_SELECT_[MASTER | SLAVE]
 *                 - cfg    - DIO pin configuration for the PCM pads
 *                 - clk    - DIO to use as the PCM clock pad
 *                 - frame  - DIO to use as the PCM frame pad
 *                 - seri   - DIO to use as the PCM serial input pad
 *                 - sero   - DIO to use as the PCM serial output pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_PCM_DIOConfig(uint32_t slave, uint32_t cfg,
                                        uint32_t clk, uint32_t frame,
                                        uint32_t seri, uint32_t sero)
{
    Sys_PCM_ConfigClk(slave, cfg, clk, frame, seri, sero,
                        DIO_MODE_INPUT);
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_PCM_H */
