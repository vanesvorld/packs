/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_watchdog.h
 * - Watchdog timer hardware support functions and macros
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2017/09/20 17:26:01 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_WATCHDOG_H
#define RSL10_SYS_WATCHDOG_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Watchdog timer support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Watchdog_Refresh(void)
 * ----------------------------------------------------------------------------
 * Description   : Refresh the watchdog timer count
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Watchdog_Refresh(void)
{
    WATCHDOG->CTRL = WATCHDOG_REFRESH;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_Watchdog_Set_Timeout(uint32_t timeout)
 * ----------------------------------------------------------------------------
 * Description   : Set the watchdog timeout period
 * Inputs        : timeout - Timeout value for watchdog; use WATCHDOG_TIMEOUT_*
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_Watchdog_Set_Timeout(uint32_t timeout)
{
    WATCHDOG->CFG = timeout;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_WATCHDOG_H */
