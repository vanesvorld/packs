/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_uart.h
 * - Support firmware for the UART interface
 * ----------------------------------------------------------------------------
 * $Revision: 1.6 $
 * $Date: 2017/07/07 20:02:44 $
 * --------------------------------------------------------------------------*/

#ifndef RSL10_SYS_UART_H
#define RSL10_SYS_UART_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Universal Asynchronous Receiver-Transmitter (UART) peripheral support
 * function prototypes
 * ------------------------------------------------------------------------- */
extern void Sys_UART_Enable(uint32_t clk_speed, uint32_t baud, uint32_t dma);

/* ----------------------------------------------------------------------------
 * Function      : void Sys_UART_Disable(void)
 * ----------------------------------------------------------------------------
 * Description   : Disable the UART
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * -------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_UART_Disable(void)
{
    UART_CFG->ENABLE_ALIAS = UART_DISABLE_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_UART_DIOConfig(uint32_t cfg,
 *                                          uint32_t tx, uint32_t rx)
 * ----------------------------------------------------------------------------
 * Description   : Configure two DIOs for the specified UART interface
 * Inputs        : - cfg    - DIO pin configuration for the UART pads
 *                 - tx     - DIO to use as the UART transmit pad
 *                 - rx     - DIO to use as the UART receive pad
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_UART_DIOConfig(uint32_t cfg,
                                         uint32_t tx, uint32_t rx)
{
    Sys_DIO_Config(tx, ((cfg) | (DIO_MODE_UART_TX)));
    Sys_DIO_Config(rx, ((cfg) | DIO_MODE_INPUT));
    DIO->UART_SRC = (rx << DIO_UART_SRC_RX_Pos);
}
/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_UART_H */
