/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_adc.h
 * - Analog-to-Digital (ADC) component support
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2017/07/31 20:53:01 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_ADC_H
#define RSL10_SYS_ADC_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * ADC support inline functions
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ADC_Set_Config(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Set the ADC configuration
 * Inputs        : cfg - The ADC configuration; use
 *                       ADC_VBAT_DIV2_[NORMAL | DUTY],
 *                       ADC_[NORMAL | CONTINUOUS], and
 *                       ADC_[DISABLE | PRESCALE_*]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ADC_Set_Config(uint32_t cfg)
{
    ADC->CFG = (cfg & ((1U << ADC_CFG_DUTY_DIVIDER_Pos)           |
                      (1U << ADC_CFG_CONTINUOUS_MODE_Pos )        |
                      ADC_CFG_FREQ_Mask));
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_ADC_Get_Config(void)
 * ----------------------------------------------------------------------------
 * Description   : Get the control register values from ADC_CFG
 * Inputs        : None
 * Outputs       : return value - Current ADC control setting; compare with
 *                                ADC_VBAT_DIV2_[NORMAL | DUTY],
 *                                ADC_[NORMAL | CONTINUOUS], and
 *                                ADC_[DISABLE | PRESCALE_*]
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_ADC_Get_Config(void)
{
    return ADC->CFG;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ADC_Set_BATMONConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Set the battery monitor configuration.
 * Inputs        : cfg - ADC_BATMON configuration; use
 *                       BATMON_ALARM_[NONE | COUNT1 | COUNT255]
 *                       or other values shifted to
 *                       ADC_BATMON_CFG_ALARM_COUNT_VALUE_Pos,
 *                       SUPPLY_THRESHOLD_[LOW | MID | HIGH]
 *                       or other values shifted to
 *                       ADC_BATMON_CFG_SUPPLY_THRESHOLD_Pos, and
 *                       BATMON_CH[6 | 7]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ADC_Set_BATMONConfig(uint32_t cfg)
{
    ADC->BATMON_CFG = (cfg & (ADC_BATMON_CFG_ALARM_COUNT_VALUE_Mask |
                                 ADC_BATMON_CFG_SUPPLY_THRESHOLD_Mask  |
                                 (1U << ADC_BATMON_CFG_SUPPLY_SRC_Pos)));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ADC_Set_BATMONIntConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Set the battery monitor interrupt configuration
 * Inputs        : cfg - ADC_BATMON_INT_ENABLE configuration; use
 *                       INT_[DIS | EBL]_ADC
 *                       ADC_INT_CH*, and
 *                       INT_[DIS | EBL]_BATMON_ALARM,
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ADC_Set_BATMONIntConfig(uint32_t cfg)
{
    ADC->BATMON_INT_ENABLE = (cfg &
            ((1U << ADC_BATMON_INT_ENABLE_BATMON_ALARM_INT_ENABLE_Pos)     |
             ADC_BATMON_INT_ENABLE_ADC_INT_CH_NUM_Mask                     |
             (1U << ADC_BATMON_INT_ENABLE_ADC_INT_ENABLE_Pos)));
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ADC_Clear_BATMONStatus(void)
 * ----------------------------------------------------------------------------
 * Description   : Clear the battery monitor status
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ADC_Clear_BATMONStatus(void)
{
    ADC->BATMON_STATUS = (((1U << ADC_BATMON_STATUS_BATMON_ALARM_CLEAR_Pos)  |
                           (1U << ADC_BATMON_STATUS_ADC_OVERRUN_CLEAR_Pos)   |
                           (1U << ADC_BATMON_STATUS_ADC_READY_CLEAR_Pos)));
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_ADC_Get_BATMONStatus(void)
 * ----------------------------------------------------------------------------
 * Description   : Get the battery monitor status
 * Inputs        : None
 * Outputs       : return value   - Current ADC_BATMON_STATUS status;
 *                                  compare with
 *                                  BATMON_ALARM_[FALSE | TRUE],
 *                                  ADC_OVERRUN_[FALSE | TRUE], and
 *                                  ADC_READY_[FALSE | TRUE]
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_ADC_Get_BATMONStatus(void)
{
    return ADC->BATMON_STATUS;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ADC_InputSelectConfig(uint32_t num,
 *                                                  uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the input selection for an ADC channel
 * Inputs        : - num     - Channel number; use [0 to 7]
 *                 - cfg     - Input selection configuration; use
 *                             ADC_POS_INPUT_* | ADC_NEG_INPUT_*
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ADC_InputSelectConfig(uint32_t num, uint32_t cfg)
{
    ADC->INPUT_SEL[num] = cfg;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_ADC_H */
