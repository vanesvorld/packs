/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a ON
 * Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_sys_asrc.h
 * - Asynchronous Sample Rate Converter functions and macros
 * ----------------------------------------------------------------------------
 * $Revision: 1.9 $
 * $Date: 2019/05/31 15:22:05 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_SYS_ASRC_H
#define RSL10_SYS_ASRC_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Asynchronous Sample Rate Converter support function prototypes
 * ------------------------------------------------------------------------- */
extern void Sys_ASRC_ConfigRunTime(uint32_t cfg, int64_t f_src,
        int64_t f_sink, int8_t diff_bit);

/* ----------------------------------------------------------------------------
 * Asynchronous Sample Rate Converter support macros
 * ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
 * Macro      : Sys_ASRC_CalcPhaseCnt(mode, f_src, f_sink)
 * ----------------------------------------------------------------------------
 * Description   : Calculate the phase increment value according to the mode
 *                  and the input frequencies
 * Inputs        : - mode         - Configuration of the ASRC mode value; use
 *                                  ASRC_INT_MODE | ASRC_DEC_MODE*
 *                 - f_src        - Source frequency or source sample number
 *                 - f_sink       - Sink frequency or sink sample number
 * Outputs       : return value   - The phase increment value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#define Sys_ASRC_CalcPhaseCnt(mode, f_src, f_sink)\
    (\
        (mode==ASRC_INT_MODE | mode == ASRC_DEC_MODE1)? \
                                32 * (f_src - f_sink)/ f_sink :\
        (mode==ASRC_DEC_MODE2)? 16 * (f_src - f_sink)/ f_sink :\
                                8 * (f_src - 2*f_sink)/ f_sink\
    )

/* ----------------------------------------------------------------------------
 * Macro      : Sys_ASRC_CheckInputConfig(mode, f_src, f_sink)
 * ----------------------------------------------------------------------------
 * Description   : Check that the input frequencies or sample numbers are valid
 * in the range depending on the selected mode
 * Inputs        : - mode         - Configuration of the mode value; use
 *                                  ASRC_INT_MODE | ASRC_DEC_MODE*
 *                 - f_src        - Source frequency or source sample number
 *                 - f_sink       - Sink frequency or sink sample number
 * Outputs       : return value   - 0 if frequency range check failed;
 *                                  1 otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#define Sys_ASRC_CheckInputConfig(mode, f_src, f_sink)\
    (\
        (mode==ASRC_INT_MODE)? (!(f_sink>f_src)? 0 : 1):\
        (mode==ASRC_DEC_MODE1)? ((!((f_sink<(1.125*f_src)) && \
                                (f_sink>(0.875*f_src)))) ? 0 : 1):\
        (mode==ASRC_DEC_MODE2)? ((!((f_sink<f_src) && (f_sink>0.4*f_src)))? \
                                0 : 1):\
        (!((f_sink>0.2*f_src) && (f_sink<0.5*f_src)))? 0 : 1\
    )

/* ----------------------------------------------------------------------------
 * Macro      : Sys_ASRC_PhaseIncConfig(mode, f_src, f_sink)
 * ----------------------------------------------------------------------------
 * Description   : Calculate the phase increment value in m8p24
 * Inputs        : - mode       - Configuration value; use
 *                                ASRC_INT_MODE | ASRC_DEC_MODE*
 *                 - f_src      - Source frequency or source samples
 *                 - f_sink     - Sink frequency or sink samples
 * Outputs       : return value - 0 if frequency range check failed;
 *                                otherwise return the phase increment value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#define Sys_ASRC_PhaseIncConfig(mode, f_src, f_sink)\
/*The output will be left-shifted 24 bits to fit in m8p24 format*/\
    (\
        (Sys_ASRC_CheckInputConfig(mode, f_src, f_sink) == 1)?\
                (1<<24)* Sys_ASRC_CalcPhaseCnt(mode, f_src, f_sink): 0\
    )

/* ----------------------------------------------------------------------------
 * Asynchronous Sample Rate Converter support inline functions
 * ------------------------------------------------------------------------- */
 /* ----------------------------------------------------------------------------
 * Function      : void Sys_ASRC_Enable(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Enable or disable the ASRC block
 * Inputs        : cfg       - Enable or disable the ASRC block; use
 *                             ASRC_[DISABLE | ENABLE]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ASRC_Enable(uint32_t cfg)
{
    ASRC->CTRL = (ASRC->CTRL & ~0x3)|cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_ASRC_Status(void)
 * ----------------------------------------------------------------------------
 * Description   : Read status of the ASRC block
 * Inputs        : None
 * Outputs       : return value - The value of the ASRC_CTRL register
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_ASRC_Status(void)
{
    return ASRC->CTRL;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ASRC_StatusConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the status of the ASRC block
 * Inputs        : cfg        - The value of the ASRC_CTRL register
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ASRC_StatusConfig(uint32_t cfg)
{
    ASRC->CTRL = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ASRC_InputData(uint16_t data)
 * ----------------------------------------------------------------------------
 * Description   : Send a sample to the ASRC block
 * Inputs        : data        - Value of the input sample
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ASRC_InputData(uint16_t data)
{
    ASRC->IN = data;
}

/* ----------------------------------------------------------------------------
 * Function      : uint16_t Sys_ASRC_OutputData(void)
 * ----------------------------------------------------------------------------
 * Description   : Read a sample from the ASRC block
 * Inputs        : None
 * Outputs       : return value -   The output value of the block
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint16_t Sys_ASRC_OutputData(void)
{
    return ASRC->OUT;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ASRC_IntEnableConfig(uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the interrupt enable register of the ASRC block
 * Inputs        : cfg     - Interrupt register value; use
 *                           INT_EBL_ASRC_IN,
 *                           INT_EBL_ASRC_OUT,
 *                           INT_EBL_ASRC_IN_ERR, and
 *                           INT_EBL_ASRC_UPDATE_ERR
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ASRC_IntEnableConfig(uint32_t cfg)
{
    ASRC->INT_ENABLE= cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ASRC_Reset(void)
 * ----------------------------------------------------------------------------
 * Description   : Reset the ASRC block
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ASRC_Reset(void)
{
    ASRC_CTRL->ASRC_RESET_ALIAS = ASRC_RESET_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ASRC_Config(uint32_t phase_inc, uint32_t cfg)
 * ----------------------------------------------------------------------------
 * Description   : Configure the ASRC block
 * Inputs        : - phase_inc  - The phase increment value
 *                 - cfg        - The WDF type and ASRC mode; use
 *                                ASRC_[INT_MODE | DEC_MODE*], and
 *                                [LOW_DELAY | WIDE_BAND]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ASRC_Config(uint32_t phase_inc, uint32_t cfg)
{
    ASRC->PHASE_INC = phase_inc;
    ASRC->CFG = cfg;
}

/* ----------------------------------------------------------------------------
 * Function      : uint32_t Sys_ASRC_OutputCount(void)
 * ----------------------------------------------------------------------------
 * Description   : Read the ASRC output counter value
 * Inputs        : None
 * Outputs       : return value -   The output counter value
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE uint32_t Sys_ASRC_OutputCount(void)
{
    return ASRC->OUTPUT_CNT;
}

/* ----------------------------------------------------------------------------
 * Function      : void Sys_ASRC_ResetOutputCount(void)
 * ----------------------------------------------------------------------------
 * Description   : Reset the ASRC block output counter
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__STATIC_INLINE void Sys_ASRC_ResetOutputCount(void)
{
    ASRC->OUTPUT_CNT = 0;
}

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_SYS_ASRC_H */
