/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * printf.h
 * Header of PRINTF utility functions and definitions to allow the choice
 * between SEGGER RTT or UART output.
 * ------------------------------------------------------------------------- */

#ifndef PRINTF_H
#define PRINTF_H

#define OUTPUT_DISABLED                -1
#define OUTPUT_UART                    0 /* Note: if application uses UART or DIO5, PRINTF over UART will be a conflict.  */
#define OUTPUT_RTT                     1 /* Note: for RTT PRINTF, add the SEGGER RTT component on your .rteconfig file */

 #ifndef OUTPUT_INTERFACE
     #define OUTPUT_INTERFACE           OUTPUT_DISABLED
 #endif

#if OUTPUT_INTERFACE == OUTPUT_UART
    #define THREE_BLOCK_APPN(x, y, z)       x##y##z
    #define DMA_IRQn(x)                     THREE_BLOCK_APPN(DMA, x, _IRQn)
    #define DMA_IRQ_FUNC(x)                 THREE_BLOCK_APPN(DMA, x, _IRQHandler)
    #define VA_NARGS_IMPL(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, N, ...) N
    #define VA_NARGS(...) VA_NARGS_IMPL(__VA_ARGS__, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
    int UART_printf(const int narg, const char *sFormat, ...);
    #define PRINTF(...) UART_printf(VA_NARGS(__VA_ARGS__), __VA_ARGS__)
    void printf_init(void);
    void DMA_IRQ_FUNC(DMA_UART_TX)(void);

#elif OUTPUT_INTERFACE == OUTPUT_RTT
#include <SEGGER_RTT.h>
    void printf_init(void);
    #define PRINTF(...) SEGGER_RTT_printf(0, __VA_ARGS__)
#else
    #define PRINTF(...)
    #define printf_init()
#endif

#endif /* PRINTF_H */
