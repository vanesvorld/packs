/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * printf.c
 * Implementation of PRINTF utility functions and definitions to allow the
 * choice between SEGGER RTT or UART output.
 * ------------------------------------------------------------------------- */

#include <printf.h>
#include <stdio.h>
#include <stdarg.h>
#include <rsl10.h>

#if OUTPUT_INTERFACE == OUTPUT_UART

#define UART_BAUD_RATE             115200
#define TX_BUFFER_SIZE             200
#define DMA_UART_TX                7
#define UART_TX                    5
#define UART_RX                    4
#define UART_TX_CFG                (DMA_DISABLE                | \
                                    DMA_ADDR_LIN               | \
                                    DMA_TRANSFER_M_TO_P        | \
                                    DMA_PRIORITY_0             | \
                                    DMA_DISABLE_INT_DISABLE    | \
                                    DMA_ERROR_INT_DISABLE      | \
                                    DMA_COMPLETE_INT_ENABLE    | \
                                    DMA_COUNTER_INT_DISABLE    | \
                                    DMA_START_INT_DISABLE      | \
                                    DMA_LITTLE_ENDIAN          | \
                                    DMA_SRC_ADDR_INC           | \
                                    DMA_SRC_WORD_SIZE_32       | \
                                    DMA_SRC_ADDR_STEP_SIZE_1   | \
                                    DMA_DEST_ADDR_STATIC       | \
                                    DMA_DEST_UART              | \
                                    DMA_DEST_WORD_SIZE_8)

static uint8_t UARTTXBuffer[TX_BUFFER_SIZE];
static volatile uint8_t tx_busy = 0;

void printf_init(void)
{
    Sys_UART_DIOConfig(DIO_6X_DRIVE | DIO_WEAK_PULL_UP | DIO_LPF_ENABLE,
                       UART_TX, UART_RX);
    Sys_UART_Enable(SystemCoreClock, UART_BAUD_RATE, UART_DMA_MODE_ENABLE);

    Sys_DMA_ChannelConfig(DMA_UART_TX, UART_TX_CFG, 1, 0,
                          (uint32_t)UARTTXBuffer, (uint32_t)&UART->TX_DATA);
    NVIC_SetPriority(DMA_IRQn(DMA_UART_TX), 0x4);
    NVIC_EnableIRQ(DMA_IRQn(DMA_UART_TX));
}

/* ----------------------------------------------------------------------------
 * Function      : void DMA_IRQ_FUNC(DMA_UART_TX)(void)
 * ----------------------------------------------------------------------------
 * Description   : DMA interrupt service routing to clear busy flag.
 * Inputs        : None
 * Outputs       : None
 * ------------------------------------------------------------------------- */
void DMA_IRQ_FUNC(DMA_UART_TX)(void)
{
    tx_busy = 0;
    Sys_DMA_ClearChannelStatus(DMA_UART_TX);
}

/* ----------------------------------------------------------------------------
 * Function      : int UART_printf(int narg, const char *sFormat, ...)
 * ----------------------------------------------------------------------------
 * Description   : Printf function for the UART debug port
 * Inputs        : - narg    - Number of argument to print
 *                 - sFormat - Pointer to the argument list
 * Outputs       : None
 * Assumptions   : The UART port and the DMA has been initialized.
 * ------------------------------------------------------------------------- */
int UART_printf(const int narg, const char *sFormat, ...)
{
    va_list arg;
    int done = 0;

    /* Check until the DMA is not busy */
    while (tx_busy == 1)
    {
        Sys_Watchdog_Refresh();
    }

    if (narg > 1)
    {
        /* Use sprint if there is more than one argument */
        va_start(arg, sFormat);
        done = vsprintf((char *)UARTTXBuffer, sFormat, arg);
        va_end(arg);
        DMA_CTRL1[DMA_UART_TX].TRANSFER_LENGTH_SHORT = strlen((const char *)UARTTXBuffer);
    }
    else
    {
        /* Use simple memcpy if there is one argument */
        DMA_CTRL1[DMA_UART_TX].TRANSFER_LENGTH_SHORT = strlen(sFormat);
        memcpy(UARTTXBuffer, sFormat, DMA_CTRL1[DMA_UART_TX].TRANSFER_LENGTH_SHORT);
    }

    /* The DMA is busy */
    tx_busy = 1;
    Sys_DMA_ChannelEnable(DMA_UART_TX);

    return done;
}

#elif OUTPUT_INTERFACE == OUTPUT_RTT
void printf_init(void)
{
    SEGGER_RTT_Init();
}
#endif


