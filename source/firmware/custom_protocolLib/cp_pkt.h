/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * cp_pkt.h
 * - Low latency audio streaming protocol header file
 * ----------------------------------------------------------------------------
 * $Revision: 1.13 $
 * $Date: 2017/11/28 13:46:21 $
 * ------------------------------------------------------------------------- */

#ifndef CP_PKT_H
#define CP_PKT_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/
#define RFREG_BASE                      0x40010000

/* GCS1 register addresses */
#define MODE                            0x00
#define MODE_2                          0x01
#define FOURFSK_CODING                  0x02
#define DATAWHITE_BTLE                  0x03
#define FIFO                            0x0B
#define FIFO_2                          0x0C
#define IRQ_CONF                        0x0D
#define PAD_CONF_1                      0x0E
#define PAD_CONF_2                      0x0F
#define PAD_CONF_3                      0x10
#define PAD_CONF_4                      0x11
#define PAD_CONF_5                      0x12
#define MAC_CONF                        0x13
#define RX_MAC_TIMER                    0x14
#define TX_MAC_TIMER                    0x15
#define BANK                            0x16
#define CENTER_FREQ                     0x18
#define CHANNELS_2                      0x20
#define CODING                          0x21
#define PACKET_HANDLING                 0x22
#define PACKET_LENGTH                   0x23
#define PACKET_LENGTH_OPTS              0x24
#define PREAMBLE                        0x25
#define PREAMBLE_LENGTH                 0x26
#define ADDRESS_CONF                    0x27
#define ADDRESS                         0x28
#define ADDRESS_BROADCAST               0x2A
#define PATTERN                         0x2C
#define PACKET_EXTRA                    0x30
#define CONV_CODES_CONF                 0x31
#define CONV_CODES_POLY                 0x32
#define CRC_POLYNOMIAL                  0x34
#define CRC_RST                         0x38
#define CONV_CODES_PUNCT                0x3C
#define PA_PWR                          0x65
#define RSSI_CTRL                       0x6A
#define TIMEOUT                         0x8C
#define SWCAP_FSM                       0xA2
#define XTAL_TRIM                       0xA7
#define SUBBAND_CONF                    0xB1
#define SUBBAND_CORR                    0xB7
#define RSSI_DETECT                     0xB8
#define FSM_MODE                        0xC0
#define FSM_STATUS                      0xC1
#define TXFIFO_STATUS                   0xC2
#define RXFIFO_STATUS                   0xC3
#define TXFIFO_COUNT                    0xC4
#define RXFIFO_COUNT                    0xC5
#define RSSI_MIN                        0xC6
#define RSSI_MAX                        0xC7
#define RSSI_PKT                        0xC8
#define RSSI_AVG                        0xCA
#define TXFIFO                          0xCC
#define RXFIFO                          0xD0
#define DESER_STATUS                    0xD4
#define IRQ_STATUS                      0xD8
#define CARRIER_RECOVERY_EXTRA          0x5C

/* Protocol state machine */
#define CP_IDLE                         0
#define CP_READY                        1
#define CP_WAIT_FOR_TX_START_FIRST      2
#define CP_WAIT_FOR_RX_START_FIRST      3
#define CP_TX_RX_FIRST                  4
#define CP_RX_TX_FIRST                  5
#define CP_WAIT_FOR_TX_START_SECOND     6
#define CP_WAIT_FOR_RX_START_SECOND     7
#define CP_TX_RX_SECOND                 8
#define CP_RX_TX_SECOND                 9
#define CP_TX_SECOND                    10
#define CP_RX_SECOND                    11

#define RF_REG_READ(addr)               (*(volatile uint8_t *)(RFREG_BASE + addr))
#define RF_REG_WRITE(addr, value)       (*(volatile uint8_t *)(RFREG_BASE + addr)) = (value)

/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/
struct pkt_type
{
    uint8_t seq_num           :   1;
    uint8_t acked_seq_num     :   1;
    uint8_t data_control      :   1;
    uint8_t acking            :   1;
    uint8_t hopClusterNum     :   4;
    uint8_t length;
    uint8_t payload[160];
};

struct hop_status
{
    uint8_t state; // 0: slow, 1: fast
    uint8_t maxClustNum;

    /* For slow: for master it increments per interval up to
     * (maxClustNum*numSlowHop) and then go back to zero for beginning of
     * fast hopping, for fast: (increment per interval %16) */
    uint8_t hopNum;

    /* master: pre-saved hop number for current channel in slow hopping,
     * for fast hopping track the current hop number */
    uint8_t tempHopNum;
    uint8_t tempHopState;
    uint8_t numSlowHop;
    uint8_t numFastHop;
    uint8_t slowHopList_first[4];
    uint8_t slowHopList_second[4];
    uint8_t fastHopList_first[40];
    uint8_t fastHopList_second[40];
};

/* Protocol device role */
enum cp_device_role
{
    CP_SLAVE_ROLE = 0,
    CP_MASTER_ROLE,
};

/* Unidirectional and bidirectional audio definitions */
enum cp_audio_direction
{
    CP_MONODIRECTION = 0,
    CP_BIDIRECTION,
};

/* Link status to be used by application */
enum cp_status_update
{
    LINK_DISCONNECTED = 0,
    LINK_ESTABLISHMENT_UNSUCCESS,
    LINK_ESTABLISHED,
};

/* Event types used in APIs to notify application */
enum cp_app_trx_event_type
{
    TX_START = 0,
    TX_SUCCESS,
    TX_RETRANSMIT_DONE,
    RX_START,
    RX_SUCCESS,
    RX_RETRANSMIT_RECEIVED,
    RX_TIMEOUT,
};

/* Event types for the protocol event handler */
enum cp_event_handle_type
{
	CP_TX_START = 0,
	CP_TX_SUCCESS,
	CP_TX_RETRANSMIT_DONE,
	CP_RX_START,
	CP_RX_SUCCESS,
	CP_RX_RETRANSMIT_RECEIVED,
	CP_RX_TIMEOUT,
    CP_PACKET_TRANSMIT,
    CP_PACKET_RECEIVE
};

/* Protocol parameters data structure to be used by application */
struct cp_param_tag
{
    /* Configuration for uni/bi directional audio */
    uint8_t bidirectional;

    /* Configuration for role of protocol in device */
    uint8_t master;

    /* Times in us */
    uint16_t interval_time;
    uint16_t retrans_time;
    uint32_t scan_time;
    uint16_t render_time;
    uint16_t ifs;

    /* Rate in 1000 bits per second */
    uint16_t audio_rate;
    uint16_t radio_rate;

    /* Configuration of preamble and access word */
    uint8_t preamble;
    uint32_t accessword;

    /* Link budget thresholds */
    uint16_t pktLostLowThrshld;
    uint16_t pktLostHighThrshld;
    uint16_t pktLostLowThrshldSlow;

    /* Configuration parametrs for hopping */
    uint8_t numSlowHop;
    uint8_t numFastHop;
    uint8_t maxClustNum;
    uint8_t slowHopList_first[4];
    uint8_t slowHopList_second[4];
    uint8_t fastHopList_first[40];
    uint8_t fastHopList_second[40];
};

/* Call back function structure to be in APIs */
struct cp_callback
{
    uint8_t (* trx_event)(uint8_t type, uint8_t *length, uint8_t *ptr);
    uint8_t (* status_update)(uint8_t status);

};

extern struct cp_callback cp_env;
extern uint32_t cp_pre_anchor_timer_value;
extern uint32_t cp_anchor_timer_value;
extern uint32_t cp_runtime_timer_value;
extern uint32_t master_anchor_point_instant_basetime;
extern uint32_t slave_anchor_point_instant_basetime;
extern uint32_t slave_anchor_point_instant_finetime;
extern uint32_t cp_interval_time;
extern uint32_t cp_retrans_time;
extern uint32_t cp_rendertime;
extern uint16_t cp_packet_length;
extern uint8_t cp_bidirectional;
extern uint8_t cp_master;
extern uint8_t cp_errorHndl;
extern uint16_t cp_audio_rate;
extern uint16_t cp_radio_rate;
extern uint16_t cp_ifs;
extern uint32_t cp_interval_counter;
extern uint32_t cp_ble_offset;
extern uint32_t cp_scan_time;
extern uint32_t cp_eventProcessTime;
extern uint8_t cp_state;
extern uint8_t cp_gi;
extern struct pkt_type cp_rxBuff;
extern struct pkt_type cp_txBuff;
extern struct hop_status cp_hopStatus;
extern uint8_t *cp_ptr;
extern uint8_t cp_ble_request;
extern uint32_t cp_gtmp;
extern uint8_t cp_rxLen;
extern uint8_t cp_txLen;
extern uint8_t cp_rxStatus;
extern uint32_t cp_rxErrCnt;
extern uint32_t cp_crcErrCnt;
extern uint32_t cp_pktLenErrCnt;
extern uint32_t cp_adrsErrCnt;
extern uint32_t cp_proErrCnt;
extern uint32_t cp_rxPktCnt;
extern uint32_t cp_txPktCnt;
extern uint32_t cp_rxScndPktCnt;
extern uint32_t cp_txScndPktCnt;
extern uint32_t cp_inputCnt;
extern uint16_t cp_linkLost_count;
extern uint8_t cp_inputBuffer[];
extern uint8_t cp_outputBuffer[];
extern uint8_t cp_event;
extern uint8_t cp_eventType;
extern uint8_t cp_statusChange;
extern uint8_t cp_oldLinkStatus;
extern uint8_t cp_linkStatus;
extern uint8_t cp_preamble;
extern uint32_t cp_accessword;
extern uint32_t cp_crc_poly;
extern uint8_t cp_gchnl;
extern uint8_t cp_oldChnl;
extern uint8_t cp_currentChnlIndx;
extern uint8_t cp_bleChnl;
extern uint16_t cp_pktLostCnt;
extern uint16_t cp_pktLostHighThrshld;
extern uint16_t cp_pktLostLowThrshld;
extern uint16_t cp_pktLostLowThrshldSlow;
extern uint8_t rf_conf_2Mbps[];
extern uint8_t rf_conf_1Mbps[];
extern uint8_t rf_conf_250Kbps[];
extern uint8_t rf_conf_500Kbps[];
extern uint32_t rf_freq_table[];

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/
extern void RF_Reg_WriteBurst (uint16_t addr, uint8_t size, uint8_t *data);
extern void CP_TransmitPacket(void);
extern void CP_ReceivePacket(void);
extern void CustomProtocol_Init(void);
extern uint8_t CP_Configure(struct cp_param_tag *, struct cp_callback);
extern uint8_t CP_Enable(uint16_t offset);
extern uint8_t CP_Disable(void);
extern uint8_t CP_EventHandler(void);
extern void RF_TX_IRQHandler(void);
extern void RF_RXSTOP_IRQHandler(void);
extern void RF_InitRegistersCustomMode (void);
extern uint8_t BleWhitening(uint8_t chan);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* CP_PKT_H */
