/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * cp_pkt_hdl.c
 * - Low latency audio streaming protocol packet and state machine handler
 * ----------------------------------------------------------------------------
 * $Revision: 1.27 $
 * $Date: 2018/03/05 20:17:41 $
 * ------------------------------------------------------------------------- */

#include "cp_pkt.h"

#define CP_DEBUG    1

/* Global variables */
uint32_t cp_pre_anchor_timer_value;
uint32_t cp_anchor_timer_value;
uint32_t cp_runtime_timer_value;
uint32_t master_anchor_point_instant_basetime;
uint32_t slave_anchor_point_instant_basetime;
uint32_t slave_anchor_point_instant_finetime;

/* In us */
uint32_t cp_interval_time;
uint32_t cp_retrans_time;
uint32_t cp_rendertime;
uint16_t cp_ifs;
uint32_t cp_ble_offset;
uint32_t cp_scan_time;
uint32_t cp_eventProcessTime;

uint16_t cp_packet_length;
uint8_t cp_bidirectional;
uint8_t cp_master;
uint8_t cp_errorHndl;
uint32_t cp_interval_counter;

uint16_t cp_audio_rate;
uint16_t cp_radio_rate;

uint8_t cp_state;
uint8_t cp_gi;
struct pkt_type cp_rxBuff;
struct pkt_type cp_txBuff;
struct hop_status cp_hopStatus;
struct cp_callback cp_env;

uint8_t *cp_ptr;
uint8_t cp_ble_request;
uint32_t cp_gtmp;
uint8_t cp_rxLen;
uint8_t cp_txLen;
uint8_t cp_rxStatus;
uint32_t cp_rxErrCnt;
uint32_t cp_crcErrCnt;
uint32_t cp_pktLenErrCnt;
uint32_t cp_adrsErrCnt;
uint32_t cp_proErrCnt;
uint32_t cp_rxPktCnt;
uint32_t cp_txPktCnt;
uint32_t cp_rxScndPktCnt;
uint32_t cp_txScndPktCnt;
uint32_t cp_inputCnt;

uint8_t cp_event;
uint8_t cp_eventType;
uint8_t cp_statusChange;
uint8_t cp_oldLinkStatus;
uint8_t cp_linkStatus;

uint8_t cp_preamble;
uint32_t cp_accessword;
uint32_t cp_crc_poly;
uint8_t cp_gchnl;
uint8_t cp_oldChnl;
uint8_t cp_currentChnlIndx;
uint8_t cp_bleChnl;
uint16_t cp_pktLostCnt;
uint16_t cp_pktLostHighThrshld;
uint16_t cp_pktLostLowThrshld;
uint16_t cp_pktLostLowThrshldSlow;
uint16_t cp_linkLost_count;


/* ----------------------------------------------------------------------------
 * Function      : void RF_RXSTOP_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Interrupt routine when receiver stops
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void RF_RXSTOP_IRQHandler(void)
{
    cp_rxStatus = RF_REG_READ(DESER_STATUS);
    cp_errorHndl = 1;

    if(cp_master == CP_MASTER_ROLE)
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {
            if((cp_rxStatus & 0xf) == 0x01)
            {
                cp_rxLen = RF_REG_READ(RXFIFO_COUNT);
                cp_rxPktCnt++;
                cp_errorHndl=0;
                cp_ptr = (uint8_t *) &cp_rxBuff;
                for(cp_gi = 0; cp_gi < cp_rxLen; cp_gi++)
                    cp_ptr[cp_gi] = RF_REG_READ(RXFIFO);

                /* it could be control or data, but here just acking and seq_num
                 *  is important, it is possible to have a data or
                 *  in-band signaling payload */
                if(!cp_rxBuff.acked_seq_num && cp_rxBuff.acking
                   && cp_state == CP_TX_RX_FIRST)
                {
                    Sys_Timers_Stop(SELECT_TIMER1);

#if CP_DEBUG
                    //Sys_GPIO_Set_Low(9);
#endif
                     if(cp_pktLostCnt < cp_pktLostHighThrshld)
                        cp_pktLostCnt+=1;

                     cp_linkStatus = LINK_ESTABLISHED;
                     if(cp_linkStatus != cp_oldLinkStatus)
                     {
                    	 cp_statusChange = 1;
                     }
                }
                else
                {
                    /* Integrity failure */
                    cp_proErrCnt++;
                    cp_errorHndl=1;
                }
            }
            if(cp_errorHndl)
            {
                if((cp_rxStatus & 0xf) == 0x0)
                {
                    /* could be timeout */
                    cp_rxErrCnt++;
                }
                else if((cp_rxStatus & 0xf) == 0x02)
                {
                    cp_crcErrCnt++;
                }
                else if((cp_rxStatus & 0xf) == 0x08)
                {
                    cp_pktLenErrCnt++;
                }
                else if((cp_rxStatus & 0xf) == 0x04)
                {
                    /* Address error, should be zero */
                    cp_adrsErrCnt++;
                }
#if CP_DEBUG
                    //Sys_GPIO_Set_Low(9);
#endif

                if(cp_state == CP_TX_RX_FIRST)
                {
                    cp_state = CP_TX_SECOND;
                    cp_event = 1;
                    cp_eventType = CP_PACKET_TRANSMIT;
                 }
            }
        }
        else
        {

        }
    }
    else
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {

            if((cp_rxStatus & 0xf) == 0x01)
            {
                cp_rxLen = RF_REG_READ(RXFIFO_COUNT);

                cp_errorHndl = 0;
                cp_rxPktCnt++;

                cp_ptr = (uint8_t *) &cp_rxBuff;
                cp_ptr[0] = RF_REG_READ(RXFIFO);

                    if(!cp_rxBuff.seq_num && cp_rxBuff.data_control &&
                       (cp_state == CP_RX_TX_FIRST || cp_state == CP_READY))
                    {
                        Sys_Timers_Stop(SELECT_TIMER1);

                  DIO->BB_RX_SRC = (BB_RF_SYNC_P_SRC_CONST_HIGH | (DIO->BB_RX_SRC & ~DIO_BB_RX_SRC_RF_SYNC_P_Mask));
                  DIO->BB_RX_SRC = (BB_RF_SYNC_P_SRC_CONST_LOW | (DIO->BB_RX_SRC & ~DIO_BB_RX_SRC_RF_SYNC_P_Mask));
                  BBIF->SYNC_CFG = (ACTIVE | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
                  BBIF->SYNC_CFG = (IDLE   | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);

#if CP_DEBUG
                        //Sys_GPIO_Set_Low(9);
#endif

                        if(cp_state == CP_READY)
                        {
                            cp_pktLostCnt = cp_pktLostHighThrshld;

                            /* cp_hopStatus.hopNum is pointing to next channel */
                            if(!cp_hopStatus.hopNum && ((cp_rxBuff.hopClusterNum + 1) == cp_hopStatus.maxClustNum))
                            {
                                {
                                    cp_hopStatus.state = 1;
                                    cp_hopStatus.hopNum = 0;
                                }
                            }
                            else
                            {
                                cp_hopStatus.hopNum = ((cp_rxBuff.hopClusterNum * cp_hopStatus.numSlowHop)
                                                        + cp_hopStatus.hopNum);
                            }

                            cp_linkStatus = LINK_ESTABLISHED;
                            if(cp_linkStatus != cp_oldLinkStatus)
                            {
                            	cp_statusChange = 1;
                            }
                        }
                        else
                        {
                            if(cp_pktLostCnt < cp_pktLostHighThrshld)
                                cp_pktLostCnt+=1;
                        }

                        cp_txBuff.seq_num = 0;
                        cp_txBuff.acked_seq_num = 0;
                        cp_txBuff.acking = 1;
                        cp_txBuff.data_control = 0;
                        cp_txBuff.length = 0;
                        RF_REG_WRITE(TXFIFO, *((uint8_t *)&cp_txBuff));
                        RF_REG_WRITE(TXFIFO, cp_txBuff.length);

                        cp_rxBuff.length = RF_REG_READ(RXFIFO);
                        cp_event = 1;
                        cp_eventType = CP_RX_SUCCESS;

                        cp_state = CP_RX_TX_FIRST;
                    }
                    else if(cp_rxBuff.seq_num && cp_rxBuff.data_control &&
                           (cp_state == CP_RX_SECOND))
                    {
                        RF_REG_WRITE(FSM_MODE, 0x8);

#if CP_DEBUG
                        Sys_GPIO_Set_Low(11);
#endif
                        cp_rxBuff.length = RF_REG_READ(RXFIFO);
                        cp_event = 1;
                        cp_eventType = CP_RX_RETRANSMIT_RECEIVED;

#if CP_DEBUG
                        if(cp_rxBuff.length!=cp_packet_length)
                            cp_rxBuff.length=cp_packet_length;
#endif
                        cp_rxScndPktCnt++;
                        if(cp_pktLostCnt < cp_pktLostHighThrshld)
                            cp_pktLostCnt+=2;
                    }
                    else
                    {
                        cp_proErrCnt++;
                        cp_errorHndl=1;
                    }
            }
            if(cp_errorHndl)
            {
                RF_REG_WRITE(FSM_MODE, 0x8);

                BBIF->SYNC_CFG = (ACTIVE | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
                BBIF->SYNC_CFG = (IDLE   | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);

                cp_rxLen = 0;

                if((cp_rxStatus & 0xf) == 0x0)
                {
                    /* Could be timeout */
                    cp_rxErrCnt++;
                }
                else if((cp_rxStatus & 0xf) == 0x02)
                {
                    cp_crcErrCnt++;
                }
                else if((cp_rxStatus & 0xf) == 0x08)
                {
                    cp_pktLenErrCnt++;
                }
                else if((cp_rxStatus & 0xf) == 0x04)
                {
                    /* address_err, should be zero */
                    cp_adrsErrCnt++;
                }

                if(cp_state == CP_READY)
                {
                    cp_linkStatus = LINK_ESTABLISHMENT_UNSUCCESS;
                 	cp_statusChange = 1;
                }
                else if(cp_state == CP_RX_TX_FIRST || cp_state == CP_RX_SECOND)
                {
                    if(cp_pktLostCnt)
                        cp_pktLostCnt-=1;

                    if(cp_pktLostCnt < cp_pktLostLowThrshld)
                    {
                        cp_linkStatus = LINK_DISCONNECTED;
                        if(cp_linkStatus != cp_oldLinkStatus)
                        {
                        	cp_statusChange = 1;
                        }

                        cp_linkLost_count++;
                        CustomProtocol_Init();
                        return;
                    }

                    if(cp_state == CP_RX_SECOND)
                    {
                        cp_event = 1;
                        cp_eventType = CP_RX_TIMEOUT;
#if CP_DEBUG
                        //Sys_GPIO_Set_Low(11);
#endif

                    }
                }
            }

        }
        else
        {

        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void RF_TX_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Interrupt routine for the end of transmission
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void RF_TX_IRQHandler(void)
{
    cp_gtmp = RF_REG_READ(IRQ_STATUS);

#if CP_DEBUG

  if(cp_state==CP_TX_RX_FIRST)
      Sys_GPIO_Set_Low(9);
  if(cp_state==CP_TX_SECOND)
      Sys_GPIO_Set_Low(11);
#endif

    cp_txPktCnt++;

    if(cp_master == CP_MASTER_ROLE)
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {
            if(cp_state == CP_TX_RX_FIRST)
            {
                cp_event = 1;
                cp_eventType = CP_TX_SUCCESS;
            }
            if(cp_state == CP_TX_SECOND)
            {
                cp_event = 1;
                cp_eventType = CP_TX_RETRANSMIT_DONE;
                cp_txScndPktCnt++;
            }
        }
        else
        {

        }
    }
    else
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {
        	Sys_Timers_Stop(SELECT_TIMER0);
        	Sys_Timer_Set_Control(0, TIMER_SHOT_MODE | TIMER_PRESCALE_1 |
                                  TIMER_SLOWCLK_DIV2 |
                                  (((cp_interval_time -
                                  (((2*10*8 +
                                     cp_packet_length*8)*1000)/cp_radio_rate)
                                     - cp_ifs - 110) / 2) - 1));
            Sys_Timers_Start(SELECT_TIMER0);

#if CP_DEBUG
            Sys_GPIO_Set_Low(9);
#endif
        }
        else
        {

        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void CP_TransmitPacket(void)
 * ----------------------------------------------------------------------------
 * Description   : Pre-transmission processing and enable Transmitter
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CP_TransmitPacket(void)
{
    if(cp_master == CP_MASTER_ROLE)
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {
             RF_REG_WRITE(FSM_MODE, 0x8);

             /* Flush FIFO */
             RF_REG_WRITE(TXFIFO_STATUS, 0x1);

             if(cp_state == CP_TX_RX_FIRST)
             {
                 cp_interval_counter++;

                 cp_hopStatus.tempHopNum = cp_hopStatus.hopNum;
                 cp_hopStatus.tempHopState = cp_hopStatus.state;

                 if(!cp_hopStatus.state)
                 {
                     /* (timeout value + 1) * 16us */
                     RF_REG_WRITE(TIMEOUT, (0x8f));

                     cp_gchnl = cp_hopStatus.slowHopList_first[(cp_hopStatus.tempHopNum % cp_hopStatus.numSlowHop)];
                 }
                 else
                 {
                     /* (timeout value + 1) * 16us */
                     if(cp_radio_rate == 2000)
                     {
                         RF_REG_WRITE(TIMEOUT, 0x81);
                     }
                     else
                     {
                         RF_REG_WRITE(TIMEOUT, 0x83);
                     }

                     cp_currentChnlIndx = (cp_hopStatus.tempHopNum % cp_hopStatus.numFastHop);
                     cp_gchnl = cp_hopStatus.fastHopList_first[cp_currentChnlIndx];
                 }

                 cp_txBuff.seq_num = 0;
                 cp_txBuff.acked_seq_num = 0;
                 cp_txBuff.acking = 0;
                 cp_txBuff.data_control = 1;
                 if(!cp_hopStatus.state)
                     cp_txBuff.hopClusterNum = (cp_hopStatus.tempHopNum / cp_hopStatus.maxClustNum);
                 else
                     cp_txBuff.hopClusterNum = (cp_hopStatus.tempHopNum & 0x0f);
                 cp_txBuff.length = cp_packet_length;

                 RF_REG_WRITE(MAC_CONF, 0x84);


                 RF_REG_WRITE(TX_MAC_TIMER, 7);

                 RF_REG_WRITE(IRQ_CONF, 0x3);

                 cp_inputCnt++;
            }
            if(cp_state == CP_TX_SECOND)
            {
                if(cp_pktLostCnt)
                    cp_pktLostCnt-=1;

                if(!cp_hopStatus.state)
                {
                    if(cp_pktLostCnt <= cp_pktLostLowThrshldSlow)
                    {
                        cp_linkStatus = LINK_ESTABLISHMENT_UNSUCCESS;
                      	cp_statusChange = 1;

                        CustomProtocol_Init();
                        return;
                    }
                }
                else
                {
                    if(cp_pktLostCnt < cp_pktLostLowThrshld)
                    {
                        cp_linkStatus = LINK_DISCONNECTED;
                        if(cp_linkStatus != cp_oldLinkStatus)
                        {
                        	cp_statusChange = 1;
                        }

                        cp_linkLost_count++;
                        CustomProtocol_Init();
                        return;
                    }
                }

                cp_txBuff.seq_num = 1;
                cp_txBuff.acked_seq_num = 0;
                cp_txBuff.acking = 0;
                cp_txBuff.data_control = 1;
                if(!cp_hopStatus.state)
                    cp_txBuff.hopClusterNum = (cp_hopStatus.tempHopNum / cp_hopStatus.maxClustNum);
                else
                    cp_txBuff.hopClusterNum = (cp_hopStatus.tempHopNum & 0x0f);
                cp_txBuff.length = cp_packet_length;

                RF_REG_WRITE(MAC_CONF, 0x80);
                RF_REG_WRITE(IRQ_CONF, 0x1);

                if(!cp_hopStatus.tempHopState)
                    cp_gchnl = cp_hopStatus.slowHopList_second[(cp_hopStatus.tempHopNum % cp_hopStatus.numSlowHop)];
                else
                {
                    cp_gchnl = cp_hopStatus.fastHopList_second[cp_currentChnlIndx];
                }
            }

            RF_REG_WRITE(TXFIFO, *((uint8_t *)&cp_txBuff));
            RF_REG_WRITE(TXFIFO, cp_txBuff.length);

            RF_REG_WRITE(DATAWHITE_BTLE,(0x80 | (cp_gchnl>>1)));

            RF_Reg_WriteBurst(CENTER_FREQ, 4, (uint8_t *)&rf_freq_table[cp_gchnl]);

            if(cp_state == CP_TX_RX_FIRST)
            {
                /* Slow hop */
                if(!cp_hopStatus.state)
                {
                    if(cp_hopStatus.hopNum < (cp_hopStatus.maxClustNum * cp_hopStatus.numSlowHop))
                    {
                        cp_hopStatus.hopNum++;
                        if(cp_hopStatus.hopNum == (cp_hopStatus.maxClustNum * cp_hopStatus.numSlowHop))
                        {
                            if(cp_pktLostCnt <= cp_pktLostLowThrshldSlow)
                            {
                                cp_linkStatus = LINK_ESTABLISHMENT_UNSUCCESS;
                               	cp_statusChange = 1;

                                CustomProtocol_Init();
                                    return;
                            }
                            cp_hopStatus.state = 1;
                            cp_hopStatus.hopNum = 0;
                            cp_pktLostCnt = cp_pktLostHighThrshld;
                        }
                    }
                }
                /* Fast hop */
                else
                {
                    cp_hopStatus.hopNum = ((cp_hopStatus.hopNum + 1) % cp_hopStatus.numFastHop);
                }
            }
        }
        else
        {

        }

    }
}

/* ----------------------------------------------------------------------------
 * Function      : void CP_ReceivePacket(void)
 * ----------------------------------------------------------------------------
 * Description   : Pre-process and enable receiver
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CP_ReceivePacket(void)
{
    RF_REG_WRITE(FSM_MODE, 0x8);

    if(!cp_master)
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {

            cp_event = 1;
            cp_eventType = CP_RX_START;

            if(cp_state == CP_RX_TX_FIRST || cp_state == CP_READY)
            {
                RF_REG_WRITE(MAC_CONF, 0xb0);

                /* TX rampup should be considered */
                RF_REG_WRITE(RX_MAC_TIMER, 13);

                cp_interval_counter++;

                if(cp_state == CP_READY)
                {
                    /* it should be more than timer0 value for this state, > cp_scan_time */
                    /* 1ms * (timeout value+1) */
                    RF_REG_WRITE(TIMEOUT, (0xb7));

                    cp_gchnl = cp_hopStatus.slowHopList_first[cp_hopStatus.hopNum];
                    cp_hopStatus.hopNum = ((cp_hopStatus.hopNum + 1) % cp_hopStatus.numSlowHop);
                }
                else
                {
                    /* (timeout value + 1) * 64us */
                    RF_REG_WRITE(TIMEOUT, (0x92));


                    cp_hopStatus.tempHopNum = cp_hopStatus.hopNum;
                    cp_hopStatus.tempHopState = cp_hopStatus.state;

                    if(!cp_hopStatus.state)
                        cp_gchnl = cp_hopStatus.slowHopList_first[(cp_hopStatus.tempHopNum % cp_hopStatus.numSlowHop)];
                    else
                    {
                        cp_currentChnlIndx = (cp_hopStatus.tempHopNum % cp_hopStatus.numFastHop);
                        cp_gchnl = cp_hopStatus.fastHopList_first[cp_currentChnlIndx];
                    }

                    BBIF->SYNC_CFG = (ACTIVE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
                    BBIF->SYNC_CFG = (ACTIVE | RX_ACTIVE   | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
                }

                RF_REG_WRITE(IRQ_CONF, 0x3);
            }
            if(cp_state == CP_RX_SECOND)
            {
            	RF_REG_WRITE(MAC_CONF, 0x0);

                /* TX rampup should be considered */
                /* (timeout value + 1) * 64us */
                RF_REG_WRITE(TIMEOUT, 0x90);

                RF_REG_WRITE(IRQ_CONF, 0x2);

                if(!cp_hopStatus.tempHopState)
                    cp_gchnl = cp_hopStatus.slowHopList_second[(cp_hopStatus.tempHopNum % cp_hopStatus.numSlowHop)];
                else
                {
                    cp_gchnl = cp_hopStatus.fastHopList_second[cp_currentChnlIndx];
                }
            }

            RF_REG_WRITE(DATAWHITE_BTLE,(0x80 | (cp_gchnl>>1)));
            RF_Reg_WriteBurst(CENTER_FREQ, 4, (uint8_t *)&rf_freq_table[cp_gchnl]);

            RF_REG_WRITE(FSM_MODE, 0x3);

#if CP_DEBUG
            if(cp_state==CP_RX_TX_FIRST || cp_state==CP_READY)
                Sys_GPIO_Set_High(9);
            if(cp_state==CP_RX_SECOND)
                Sys_GPIO_Set_High(11);
#endif

            if(cp_state == CP_RX_TX_FIRST)
            {
                /* Slow hop */
                if(!cp_hopStatus.state)
                {
                    if(cp_hopStatus.hopNum < (cp_hopStatus.maxClustNum * cp_hopStatus.numSlowHop))
                    {
                        cp_hopStatus.hopNum++;
                        if(cp_hopStatus.hopNum == (cp_hopStatus.maxClustNum * cp_hopStatus.numSlowHop))
                        {
                            cp_hopStatus.state = 1;
                            cp_hopStatus.hopNum = 0;
                        }
                    }
                }
                /* Fast hop */
                else
                {
                    cp_hopStatus.hopNum = ((cp_hopStatus.hopNum + 1) % cp_hopStatus.numFastHop);
                }
            }

        }
        else
        {

        }
    }
    else
    {

    }
}

/* ----------------------------------------------------------------------------
 * Function      : void CustomProtocol_Init(void)
 * ----------------------------------------------------------------------------
 * Description   : Reset state machine and the protocol global variables and
 *                 start the protocol
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : Parameters for the protocol have been set
 * ------------------------------------------------------------------------- */
void CustomProtocol_Init(void)
{

    RF_REG_WRITE(FSM_MODE, 0x8);
    cp_hopStatus.tempHopNum = 0;
    cp_hopStatus.tempHopState = 0;

    cp_hopStatus.hopNum = 0;
    cp_hopStatus.state = 0;

    cp_interval_counter = 0xffffffff;
    cp_ble_offset = 500;

    cp_oldChnl=0;

    cp_rxErrCnt=0;
    cp_crcErrCnt=0;
    cp_pktLenErrCnt=0;
    cp_proErrCnt=0;
    cp_rxPktCnt=0;
    cp_txPktCnt=0;
    cp_adrsErrCnt=0;
    cp_rxScndPktCnt=0;
    cp_txScndPktCnt=0;
    cp_inputCnt=0;

    cp_event = 0;

    if(cp_master == CP_MASTER_ROLE)
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {
            cp_pktLostCnt = (cp_hopStatus.maxClustNum * cp_hopStatus.numSlowHop);
        }
        else
        {

        }
    }
    else
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {
            cp_pktLostCnt = cp_pktLostHighThrshld;
            cp_rxStatus = 0;
            cp_state = CP_IDLE;

            cp_scan_time = (cp_hopStatus.numSlowHop * cp_interval_time + 500);

            Sys_Timers_Stop(SELECT_TIMER0);
            Sys_Timers_Stop(SELECT_TIMER1);

            NVIC_ClearPendingIRQ(TIMER0_IRQn);
            NVIC_EnableIRQ(TIMER0_IRQn);

            /* It should be called once ble event interrupt happens */
            /* 100us is to have rx sooner than tx, wider rx window */
            Sys_Timer_Set_Control(0, TIMER_FREE_RUN | TIMER_PRESCALE_1 |
                                  TIMER_SLOWCLK_DIV2 |
                                  ((cp_ble_offset - 100 / 2 ) - 1));
            Sys_Timers_Start(SELECT_TIMER0);
        }
        else
        {

        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void TIMER0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : First timer interrupt routine for the first transmit
 *                 or receive
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER0_IRQHandler(void)
{
    if(cp_master == CP_MASTER_ROLE)
    {
        if(cp_state == CP_IDLE)
        {
            NVIC_ClearPendingIRQ(TIMER0_IRQn);
            NVIC_EnableIRQ(TIMER0_IRQn);

            NVIC_ClearPendingIRQ(TIMER1_IRQn);
            NVIC_EnableIRQ(TIMER1_IRQn);

            Sys_Timer_Set_Control(0, TIMER_FREE_RUN | TIMER_PRESCALE_1 |
                                  TIMER_SLOWCLK_DIV2 |
                                  ((cp_interval_time / 2 ) - 1));
            Sys_Timers_Start(SELECT_TIMER0);

            cp_state = CP_READY;
        }
        else
        {
            if(cp_bidirectional == CP_MONODIRECTION )
            {
                Sys_Timers_Stop(SELECT_TIMER1);

                  Sys_Timer_Set_Control(1, TIMER_SHOT_MODE | TIMER_PRESCALE_1 |
                                        TIMER_SLOWCLK_DIV2 |
                                        ((cp_eventProcessTime / 2 ) - 1));

                Sys_Timers_Start(SELECT_TIMER1);


                 BBIF->SYNC_CFG = (ACTIVE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
                 BBIF->SYNC_CFG = (ACTIVE | RX_ACTIVE   | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
                 DIO->BB_RX_SRC = (BB_RF_SYNC_P_SRC_CONST_HIGH | (DIO->BB_RX_SRC & ~DIO_BB_RX_SRC_RF_SYNC_P_Mask));
                 DIO->BB_RX_SRC = (BB_RF_SYNC_P_SRC_CONST_LOW | (DIO->BB_RX_SRC & ~DIO_BB_RX_SRC_RF_SYNC_P_Mask));
                 BBIF->SYNC_CFG = (ACTIVE | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
                 BBIF->SYNC_CFG = (IDLE   | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);

                cp_state = CP_TX_RX_FIRST;
                cp_event = 1;
                cp_eventType = CP_PACKET_TRANSMIT;
            }
            else
            {

            }
        }
    }
    else
    {
        if(cp_state == CP_IDLE)
        {
            NVIC_ClearPendingIRQ(TIMER0_IRQn);
            NVIC_EnableIRQ(TIMER0_IRQn);

            NVIC_ClearPendingIRQ(TIMER1_IRQn);
            NVIC_EnableIRQ(TIMER1_IRQn);

            Sys_Timers_Stop(SELECT_TIMER0);

            Sys_Timer_Set_Control(0, TIMER_FREE_RUN | TIMER_PRESCALE_1 |
                                  TIMER_SLOWCLK_DIV2 |
                                  ((cp_scan_time / 2 ) - 1));
            Sys_Timers_Start(SELECT_TIMER0);

            cp_state = CP_READY;

            cp_event = 1;
            cp_eventType = CP_PACKET_RECEIVE;

        }
        else
        {
            if(cp_bidirectional == CP_MONODIRECTION )
            {
                if(cp_state != CP_READY)
                {
                    Sys_Timers_Stop(SELECT_TIMER0);

                    /* Interval - length of packet - windowing - ramp-up */
                    Sys_Timer_Set_Control(0, TIMER_FREE_RUN | TIMER_PRESCALE_1 |
                                          TIMER_SLOWCLK_DIV2 |
                                          (((cp_interval_time) / 2) - 1));
                    Sys_Timers_Start(SELECT_TIMER0);

                    /* Interval - length of packet - windowing - ramp-up + cp_retrans_time */
                    Sys_Timers_Stop(SELECT_TIMER1);
                    Sys_Timer_Set_Control(1, TIMER_SHOT_MODE | TIMER_PRESCALE_1 |
                                          TIMER_SLOWCLK_DIV2 |
                                          (((cp_retrans_time + 14) / 2) - 1));
                    Sys_Timers_Start(SELECT_TIMER1);

                    cp_state = CP_RX_TX_FIRST;
                }
                else
                {

                }

                CP_ReceivePacket();
            }
            else
            {

            }
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void TIMER1_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Second timer interrupt routine for retransmission handling
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER1_IRQHandler(void)
{
    uint8_t i;

    if(cp_master == CP_MASTER_ROLE)
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {
            Sys_Timers_Stop(SELECT_TIMER1);

            if(cp_state == CP_TX_RX_FIRST)
            {
                Sys_Timer_Set_Control(1, TIMER_SHOT_MODE | TIMER_PRESCALE_1 |
                                      TIMER_SLOWCLK_DIV2 |
                                      ((cp_retrans_time / 2 ) - 1));
                Sys_Timers_Start(SELECT_TIMER1);

                cp_env.trx_event(TX_START, &cp_txBuff.length, cp_txBuff.payload);

            }

            for(i = 0; i < cp_txBuff.length; i++)
            {
                RF_REG_WRITE(TXFIFO, cp_txBuff.payload[i]);
            }

            RF_REG_WRITE(FSM_MODE, 0x7);

#if CP_DEBUG
                if(cp_state == CP_TX_RX_FIRST)
                    Sys_GPIO_Set_High(9);
                if(cp_state == CP_TX_SECOND)
                    Sys_GPIO_Set_High(11);
#endif
        }
        else
        {

        }
    }
    else
    {
        if(cp_bidirectional == CP_MONODIRECTION )
        {
            BBIF->SYNC_CFG = (ACTIVE | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
            BBIF->SYNC_CFG = (IDLE   | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);

            cp_state = CP_RX_SECOND;

            CP_ReceivePacket();
        }
        else
        {

        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void RF_SwitchToBLEMode(void)
 * ----------------------------------------------------------------------------
 * Description   : Prepare RF for BLE when the protocol event ends
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
inline void RF_SwitchToBLEMode (void)
{
    RF_REG_WRITE(FSM_MODE, 0x8);
    RF_REG_WRITE(MODE_2, 0x8);
    RF_REG_WRITE(PACKET_HANDLING, 0x0);
    RF_REG_WRITE((CARRIER_RECOVERY_EXTRA+1),0x3f);
    RF_REG_WRITE(IRQ_CONF, 0x0);
}

/* ----------------------------------------------------------------------------
 * Function      : void RF_InitRegistersCustomMode(void)
 * ----------------------------------------------------------------------------
 * Description   : Set RF registers for packet handling mode of RF block
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : All of registers have been set
 * ------------------------------------------------------------------------- */
inline void RF_InitRegistersCustomMode (void)
{
    RF_REG_WRITE(FSM_MODE, 0x8);
    RF_REG_WRITE(MODE_2, 0x0);
    RF_REG_WRITE(TIMEOUT, 0x0);
    RF_REG_WRITE(PACKET_HANDLING, 0xf3);
    RF_REG_WRITE(PACKET_LENGTH, 0xff);
    RF_REG_WRITE(PACKET_LENGTH_OPTS,0x1);
    RF_REG_WRITE(PREAMBLE,cp_preamble);
    RF_REG_WRITE(PREAMBLE_LENGTH,1);
    RF_Reg_WriteBurst(PATTERN, 4 , (uint8_t *)&cp_accessword);
    RF_REG_WRITE(PACKET_EXTRA,0x3);
    RF_REG_WRITE(CODING,0x80);
    RF_Reg_WriteBurst(CRC_POLYNOMIAL,4,(uint8_t *)&cp_crc_poly);
    cp_gtmp = 0x55555555;
    RF_Reg_WriteBurst(CRC_RST, 4,(uint8_t *)&cp_gtmp);
    (void) RF_REG_READ(IRQ_STATUS);
    RF_REG_WRITE(IRQ_CONF, 0x0);
}

/* ----------------------------------------------------------------------------
 * Function      : void RF_Reg_WriteBurst(uint16_t addr, uint8_t size, uint8_t *data)
 * ----------------------------------------------------------------------------
 * Description   : Write a burst of data into RF registers
 * Inputs        : - addr       - Address of the first register
 *                 - size       - The number of registers to be written
 *                 - data       - Pointer to data that should be written
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
inline void RF_Reg_WriteBurst(uint16_t addr, uint8_t size, uint8_t *data)
{
    uint8_t i;

    for(i = 0; i < size; i++)
    {
        RF_REG_WRITE((addr + i), data[i]);
    }
}
