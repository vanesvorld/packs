/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * cp_event.c
 * - Low latency audio streaming protocol event handler
 * ----------------------------------------------------------------------------
 * $Revision: 1.13 $
 * $Date: 2018/05/03 15:17:04 $
 * ------------------------------------------------------------------------- */

#include "cp_pkt.h"

/* ----------------------------------------------------------------------------
 * Function      : uint8_t CP_Configure(struct cp_param_tag param,
 *                                      struct cp_callback callback)
 * ----------------------------------------------------------------------------
 * Description   : Configure protocol environment based on input from application
 * Inputs        : - param          - Application input parameters
 *                 - callback       - Application call back functions
 * Outputs       : return value     - 0 if it configures successfully,
 *                                    error value otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint8_t CP_Configure(struct cp_param_tag *param, struct cp_callback callback)
{
    uint8_t i, pa_pwr_temp;

    cp_bidirectional = param->bidirectional;
    cp_master = param->master;
    cp_interval_time = param->interval_time; //in us
    cp_retrans_time = param->retrans_time;
    cp_audio_rate = param->audio_rate; //in Kbps
    cp_radio_rate = param->radio_rate; //in Kbps
    cp_ifs = param->ifs; //in us
    cp_scan_time = param->scan_time;

    //cp_rendertime = param->render_time;
    cp_preamble = param->preamble;
    cp_accessword = param->accessword;

    cp_pktLostLowThrshld = param->pktLostLowThrshld;
    cp_pktLostHighThrshld = param->pktLostHighThrshld;
    cp_pktLostLowThrshldSlow = param->pktLostLowThrshldSlow;

    cp_hopStatus.numSlowHop = param->numSlowHop;
    cp_hopStatus.numFastHop = param->numFastHop;
    cp_hopStatus.maxClustNum = param->maxClustNum;

     memcpy(cp_hopStatus.slowHopList_first, param->slowHopList_first, 4);
     memcpy(cp_hopStatus.slowHopList_second, param->slowHopList_second, 4);
     memcpy(cp_hopStatus.fastHopList_first, param->fastHopList_first, 40);
     memcpy(cp_hopStatus.fastHopList_second, param->fastHopList_second, 40);

     cp_packet_length = ((cp_audio_rate * cp_interval_time) / 8000);
     cp_crc_poly = 0x8810;

//---------------
     cp_retrans_time = ((((2*10*8 + cp_packet_length*8)*1000)/cp_radio_rate) + 2*cp_ifs + 10);
     cp_scan_time = (cp_hopStatus.numSlowHop * cp_interval_time + 500);
     cp_eventProcessTime = 400;
     param->render_time = cp_retrans_time + 40;
//---------------

     cp_env.trx_event = callback.trx_event;
     cp_env.status_update = callback.status_update;

     if(cp_radio_rate == 1000)
     {
         RF_REG_WRITE(BANK, 0x0);
         pa_pwr_temp = RF_REG19->PA_PWR_BYTE;
         memcpy((uint8_t *)RFREG_BASE,(uint8_t *) &rf_conf_1Mbps[0],0x16);
         memcpy((uint8_t *)(RFREG_BASE+0x17),(uint8_t *) &rf_conf_1Mbps[0x17],0xA8);
         RF_REG19->PA_PWR_BYTE = pa_pwr_temp;
     }
     else if(cp_radio_rate == 2000)
     {
         RF_REG_WRITE(BANK, 0x1);
         pa_pwr_temp = RF_REG19->PA_PWR_BYTE;
         memcpy((uint8_t *)RFREG_BASE,(uint8_t *) &rf_conf_2Mbps[0],0x16);
         memcpy((uint8_t *)(RFREG_BASE+0x17),(uint8_t *) &rf_conf_2Mbps[0x17],0xA8);
         RF_REG19->PA_PWR_BYTE = pa_pwr_temp;
     }
     else if(cp_radio_rate == 500)
     {
         RF_REG_WRITE(BANK, 0x1);
         pa_pwr_temp = RF_REG19->PA_PWR_BYTE;
         memcpy((uint8_t *)RFREG_BASE,(uint8_t *) &rf_conf_500Kbps[0],0x16);
         memcpy((uint8_t *)(RFREG_BASE+0x17),(uint8_t *) &rf_conf_500Kbps[0x17],0xA8);
         RF_REG19->PA_PWR_BYTE = pa_pwr_temp;
     }
     else if(cp_radio_rate == 250)
     {
         RF_REG_WRITE(BANK, 0x1);
         pa_pwr_temp = RF_REG19->PA_PWR_BYTE;
         memcpy((uint8_t *)RFREG_BASE,(uint8_t *) &rf_conf_250Kbps[0],0x16);
         for(i=0;i<100;i++)
             asm volatile ("nop");
         memcpy((uint8_t *)(RFREG_BASE+0x17),(uint8_t *) &rf_conf_250Kbps[0x17],0xA8);
         RF_REG19->PA_PWR_BYTE = pa_pwr_temp;
     }

     if(cp_radio_rate == 1000)
         RF_REG_WRITE(BANK, 0x0);
     else
         RF_REG_WRITE(BANK, 0x1);

     RF_InitRegistersCustomMode();

     cp_event = 0;
     cp_statusChange = 0;
     cp_oldLinkStatus = LINK_DISCONNECTED;
     cp_linkStatus = LINK_DISCONNECTED;

     CustomProtocol_Init();

     return 0;
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t CP_Enable(uint16_t offset)
 * ----------------------------------------------------------------------------
 * Description   : Enable the protocol
 * Inputs        : - offset     - Offset instant in micro second
 * Outputs       : return value - 0 if it enables successfully,
 *                                error value otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint8_t CP_Enable(uint16_t offset)
{
    cp_state = CP_IDLE;

    SYSCTRL_RF_ACCESS_CFG->RF_IRQ_ACCESS_ALIAS = RF_IRQ_ACCESS_ENABLE_BITBAND;

    Sys_Timers_Stop(SELECT_TIMER0);
    Sys_Timers_Stop(SELECT_TIMER1);
    NVIC_ClearPendingIRQ(TIMER0_IRQn);
    NVIC_ClearPendingIRQ(TIMER1_IRQn);


    NVIC_ClearPendingIRQ(RF_TX_IRQn);
    NVIC_EnableIRQ(RF_TX_IRQn);

    NVIC_ClearPendingIRQ(RF_RXSTOP_IRQn);
    NVIC_EnableIRQ(RF_RXSTOP_IRQn);

    NVIC_EnableIRQ(TIMER0_IRQn);

    Sys_Timer_Set_Control(0, TIMER_SHOT_MODE | TIMER_PRESCALE_1 |
                          TIMER_SLOWCLK_DIV2 |
                          ((offset /2 ) - 1));
    Sys_Timers_Start(SELECT_TIMER0);

    return 0;
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t CP_Disable(void)
 * ----------------------------------------------------------------------------
 * Description   : Disable the protocol
 * Inputs        : None
 * Outputs       : return value - 0 if it disables successfully,
 *                                error value otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint8_t CP_Disable(void)
{

    NVIC_DisableIRQ(TIMER0_IRQn);
    NVIC_DisableIRQ(TIMER1_IRQn);
    NVIC_DisableIRQ(RF_TX_IRQn);
    NVIC_DisableIRQ(RF_SYNC_IRQn);
    NVIC_DisableIRQ(RF_RXSTOP_IRQn);

    return 0;
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t CP_EventHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Protocol even handler
 * Inputs        : None
 * Outputs       : return value - 0 if it handles successfully,
 *                                error value otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint8_t CP_EventHandler(void)
{

    if(!cp_event)
    {
        return 0;
    }
    else
    {
        cp_event = 0;

        switch(cp_eventType)
        {
        case CP_PACKET_TRANSMIT:

            CP_TransmitPacket();

            break;
        case CP_PACKET_RECEIVE:

            CP_ReceivePacket();

            break;

        case CP_TX_START:

            cp_env.trx_event(TX_START, &cp_txBuff.length, cp_txBuff.payload);

            break;
        case CP_TX_SUCCESS:

            cp_env.trx_event(TX_SUCCESS, &cp_txBuff.length, cp_txBuff.payload);

            break;
        case CP_TX_RETRANSMIT_DONE:

            cp_env.trx_event(TX_SUCCESS, &cp_txBuff.length, cp_txBuff.payload);

            break;
        case CP_RX_START:

            cp_env.trx_event(RX_START, &cp_rxBuff.length, cp_rxBuff.payload);

            break;
        case CP_RX_SUCCESS:

            for(cp_gi = 0; cp_gi < cp_rxBuff.length; cp_gi++)
            {
                 cp_rxBuff.payload[cp_gi] = RF_REG_READ(RXFIFO);
            }
            cp_env.trx_event(RX_SUCCESS, &cp_rxBuff.length, cp_rxBuff.payload);

            break;
        case CP_RX_RETRANSMIT_RECEIVED:

            for(cp_gi = 0; cp_gi < cp_rxBuff.length; cp_gi++)
            {
                cp_rxBuff.payload[cp_gi] = RF_REG_READ(RXFIFO);
            }
            cp_env.trx_event(RX_RETRANSMIT_RECEIVED, &cp_rxBuff.length, cp_rxBuff.payload);

            break;
        case CP_RX_TIMEOUT:

            cp_env.trx_event(RX_TIMEOUT, &cp_rxBuff.length, cp_rxBuff.payload);

            break;
        default:
            break;
        }
    }

    if(cp_statusChange)
    {
            cp_statusChange = 0;
            cp_env.status_update(cp_linkStatus);
            cp_oldLinkStatus = cp_linkStatus;
    }

    return (cp_eventType);
}
