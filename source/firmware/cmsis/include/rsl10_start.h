/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rsl10_start.h
 * - CMSIS Cortex-M3 compatible header file for application initialization
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2017/10/26 13:26:39 $
 * ------------------------------------------------------------------------- */

#ifndef RSL10_START_H
#define RSL10_START_H

/* Needed includes */
#include <stdint.h>
#include <errno.h>

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
 extern "C" {
#endif

 /* ----------------------------------------------------------------------------
  * Linker provided defines that define the heap
  * ------------------------------------------------------------------------- */
 extern uint8_t __Heap_Begin__;
 extern uint8_t __Heap_Limit__;

 /* ----------------------------------------------------------------------------
  * Linker provided defines for memory initialization
  * ------------------------------------------------------------------------- */
 extern uint32_t __data_init__;
 extern uint32_t __data_start__;
 extern uint32_t __data_end__;

 extern uint32_t __bss_start__;
 extern uint32_t __bss_end__;

 /* ----------------------------------------------------------------------------
  * Linker provided funciton lists for application initialization
  * ------------------------------------------------------------------------- */
 extern void (*__preinit_array_start__[]) (void) __attribute__((weak));
 extern void (*__preinit_array_end__[]) (void) __attribute__((weak));
 extern void (*__init_array_start__[]) (void) __attribute__((weak));
 extern void (*__init_array_end__[]) (void) __attribute__((weak));

 /* ----------------------------------------------------------------------------
  * Function prototypes
  * ------------------------------------------------------------------------- */
extern void _start(void);
extern int8_t * _sbrk(int increment);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /* RSL10_START_H */
