/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * startup.c
 * - Vector table and ISR definitions
 * ----------------------------------------------------------------------------
 * $Revision: 1.3 $
 * $Date: 2017/08/11 10:10:17 $
 * ------------------------------------------------------------------------- */
#include <rsl10.h>

/* A union that describes the entries of the vector table.  The union is needed
   since the first entry is the stack pointer and the remainder are function */
typedef union
{
    void (*pfnHandler)(void);
    unsigned long ulPtr;
}
uVectorEntry;

/* Define stack */
extern unsigned long pulStack[256];

/* Define vector table */
#define NUM_VECTORS (16+42)
extern const uVectorEntry __vector_table[NUM_VECTORS]; 

/* Forward declaration of the functions */
void Reset_Handler(void);
void NmiSR(void);
void FaultISR(void);
void IntDefaultHandler(void);

/* Forward declaration of entry point */
extern void __iar_program_start(void);


/* Reserve space for the system stack. */
unsigned long pulStack[256] @ ".noinit";

/* The vector table */
__root const uVectorEntry __vector_table[NUM_VECTORS] @ ".intvec" =
{
    { .ulPtr = (unsigned long)pulStack + sizeof(pulStack) },
                                            /* -  The initial stack pointer */
    Reset_Handler,                          /* 1  The reset */
    NmiSR,                                  /* 2  The NMI */
    FaultISR,                               /* 3  The hard fault */
    IntDefaultHandler,                      /* 4  The MPU fault */
    IntDefaultHandler,                      /* 5  The bus fault */
    IntDefaultHandler,                      /* 6  The usage fault */
    0x0,                                    /* 7  System reserved */
    0x0,                                    /* 8  System reserved */
    0x0,                                    /* 9  System reserved */
    0x0,                                    /* 10 System reserved */
    IntDefaultHandler,                      /* 11 SVCall */
    IntDefaultHandler,                      /* 12 Debug monitor */
    0x0,                                    /* 13 System reserved */
    IntDefaultHandler,                      /* 14 PendSV */
    IntDefaultHandler,                      /* 15 SYSTICK */
    IntDefaultHandler,                      /* 16 Wake-up interrupt */
    IntDefaultHandler,                      /* 17 RTC alarm interrupt */
    IntDefaultHandler,                      /* 18 RTL clock interrupt */
    IntDefaultHandler,                      /* 19 ADC / Battery monitor interrupt */
    IntDefaultHandler,                      /* 20 Timer0 interrupt */
    IntDefaultHandler,                      /* 21 Timer1 interrupt */
    IntDefaultHandler,                      /* 22 Timer2 interrupt */
    IntDefaultHandler,                      /* 23 Timer3 interrupt */
    IntDefaultHandler,                      /* 24 DMA channel 0 interrupt */
    IntDefaultHandler,                      /* 25 DMA channel 1 interrupt */
    IntDefaultHandler,                      /* 26 DMA channel 2 interrupt */
    IntDefaultHandler,                      /* 27 DMA channel 3 interrupt */
    IntDefaultHandler,                      /* 28 DMA channel 4 interrupt */
    IntDefaultHandler,                      /* 29 DMA channel 5 interrupt */
    IntDefaultHandler,                      /* 30 DMA channel 6 interrupt */
    IntDefaultHandler,                      /* 31 DMA channel 7 interrupt */
    IntDefaultHandler,                      /* 32 DIO0 interrupt */
    IntDefaultHandler,                      /* 33 DIO1 interrupt */
    IntDefaultHandler,                      /* 34 DIO2 interrupt */
    IntDefaultHandler,                      /* 35 DIO3 interrupt */   
    IntDefaultHandler,                      /* 36 Watchdog interrupt */
    IntDefaultHandler,                      /* 37 SPI0 receive interrupt */
    IntDefaultHandler,                      /* 38 SPI0 transmit interrupt */
    IntDefaultHandler,                      /* 39 SPI0 error interrupt */
    IntDefaultHandler,                      /* 40 SPI1 receive interrupt */
    IntDefaultHandler,                      /* 41 SPI1 transmit interrupt */
    IntDefaultHandler,                      /* 42 SPI1 error interrupt */
    IntDefaultHandler,                      /* 43 I2C interrupt */
    IntDefaultHandler,                      /* 44 UART receive interrupt */
    IntDefaultHandler,                      /* 45 UART transmit interrupt */
    IntDefaultHandler,                      /* 46 UART error interrupt */
    IntDefaultHandler,                      /* 47 DMIC and output driver data ready interrupt */
    IntDefaultHandler,                      /* 48 DMIC overrun and output driver underrun detect interrupt */
    IntDefaultHandler,                      /* 49 PCM receive interrupt */
    IntDefaultHandler,                      /* 50 PCM transmit interrupt */
    IntDefaultHandler,                      /* 51 PCM error interrupt */
    IntDefaultHandler,                      /* 52 DSP event interrupt 0 */
    IntDefaultHandler,                      /* 53 DSP event interrupt 1 */
    IntDefaultHandler,                      /* 54 DSP event interrupt 2 */
    IntDefaultHandler,                      /* 55 DSP event interrupt 3 */
    IntDefaultHandler,                      /* 56 DSP event interrupt 4 */
    IntDefaultHandler,                      /* 57 DSP event interrupt 5 */
    IntDefaultHandler,                      /* 58 DSP event interrupt 6 */
    IntDefaultHandler,                      /* 59 DSP event interrupt 7 */
    IntDefaultHandler,                      /* 60 BLE 625us time reference interrupt */
    IntDefaultHandler,                      /* 61 BLE sleep mode interrupt */
    IntDefaultHandler,                      /* 62 BLE received packet interrupt */
    IntDefaultHandler,                      /* 63 BLE event interrupt */
    IntDefaultHandler,                      /* 64 BLE AES ciphering complete interrupt */
    IntDefaultHandler,                      /* 65 BLE baseband error interrupt */
    IntDefaultHandler,                      /* 66 BLE gross timer interrupt */
    IntDefaultHandler,                      /* 67 BLE fine timer interrupt */
    IntDefaultHandler,                      /* 68 BLE SW triggered inerrupt */
    IntDefaultHandler,                      /* 69 Coexistance BLE start/stop RX or TX interrupt */
    IntDefaultHandler,                      /* 70 Coexistance BLE in process interrupt */
    IntDefaultHandler,                      /* 71 BLE transmit interrupt */
    IntDefaultHandler,                      /* 72 BLE receive stop interrupt */
    IntDefaultHandler,                      /* 73 BLE received packet interrupt */
    IntDefaultHandler,                      /* 74 BLE received sync word interrupt */
    IntDefaultHandler,                      /* 75 TX FIFO near underflow detect interrupt */
    IntDefaultHandler,                      /* 76 RX FIFO near overflow detect interrupt */
    IntDefaultHandler,                      /* 77 ASRC error interrupt */
    IntDefaultHandler,                      /* 78 ASRC data input interrupt */
    IntDefaultHandler,                      /* 79 ASRC data output interrupt */
    IntDefaultHandler,                      /* 80 Audio sink clock phase interrupt */
    IntDefaultHandler,                      /* 81 Audio sink clock period interrupt */
    IntDefaultHandler,                      /* 82 Clock detection interrupt */
    IntDefaultHandler,                      /* 83 Flash copy interrupt */
    IntDefaultHandler,                      /* 84 Flash ECC event interrupt */
    IntDefaultHandler,                      /* 85 Memory error event interrupt */
#if RSL10_CID == 101
    IntDefaultHandler,                      /* 86 Audio over BLE channel 0 interrupt */
    IntDefaultHandler,                      /* 87 Audio over BLE channel 1 interrupt */
    IntDefaultHandler                       /* 88 Audio over BLE channel 2 interrupt */
#endif
};

/* Reset ISR - Insert code here that should run before main */
void Reset_Handler(void) @ ".startup"
{
    asm("MOV R0, SP");
    asm("BFC R0, #2, #1");
    asm("MOV SP, R0");
    __iar_program_start();
}

/* NMI Handler */
void NmiSR(void)
{
    while(1)
    {
    }
}

/* Fault Handler */
void FaultISR(void)
{
    while(1)
    {
    }
}

/* Default Handler */
void IntDefaultHandler(void)
{
    while(1)
    {
    }
}

