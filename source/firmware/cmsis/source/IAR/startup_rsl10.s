; -----------------------------------------------------------------------------
; Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a 
; ON Semiconductor), All Rights Reserved
;
; This code is the property of ON Semiconductor and may not be redistributed
; in any form without prior written permission from ON Semiconductor.
; The terms of use and warranty for this code are covered by contractual
; agreements between ON Semiconductor and the licensee.
;
; This is Reusable Code.
;
; -----------------------------------------------------------------------------
; startup_rsl10.s
; - CMSIS Cortex-M3 Core Device start-up file for the ARM Cortex-M3 processor
;   (standard ARM development tools)
; -----------------------------------------------------------------------------
; $Revision: 1.6 $
; $Date: 2019/05/02 19:10:29 $
; -----------------------------------------------------------------------------

                MODULE   ?cstartup

                ;; Forward declaration of sections.
                SECTION  CSTACK:DATA:NOROOT(3)

                SECTION  .intvec:CODE:NOROOT(2)
                EXTERN   __iar_program_start
                EXTERN   SystemInit


                PUBLIC   __vector_table
                PUBLIC   __vector_table_End
                PUBLIC   __vector_table_Size
                PUBLIC   ISR_Vector_Table
                
                
                DATA

__vector_table
        DCD     sfe(CSTACK)
        DCD     Reset_Handler             ; Reset Handler

        DCD     NMI_Handler               ; NMI Handler
        DCD     HardFault_Handler         ; Hard Fault Handler
        DCD     MemManage_Handler         ; MPU Fault Handler
        DCD     BusFault_Handler          ; Bus Fault Handler
        DCD     UsageFault_Handler        ; Usage Fault Handler
        DCD     0                         ; Reserved
        DCD     0                         ; Reserved
        DCD     0                         ; Reserved
        DCD     0                         ; Reserved
        DCD     SVC_Handler               ; SVCall Handler
        DCD     DebugMon_Handler          ; Debug Monitor Handler
        DCD     0                         ; Reserved
        DCD     PendSV_Handler            ; PendSV Handler
        DCD     SysTick_Handler           ; SysTick Handler

         ; External Interrupts
                DCD    WAKEUP_IRQHandler            ; 16 Wakeup Handler */
                DCD    RTC_ALARM_IRQHandler         ; 17 RTC Alarm Handler 
                DCD    RTC_CLOCK_IRQHandler         ; 18 RTC Clock Handler 
                DCD    ADC_BATMON_IRQHandler        ; 19 ADC / Battery Monitor Handler 
                DCD    TIMER0_IRQHandler            ; 20 General-Purpose Timer 0 Handler 
                DCD    TIMER1_IRQHandler            ; 21 General-Purpose Timer 1 Handler 
                DCD    TIMER2_IRQHandler            ; 22 General-Purpose Timer 2 Handler 
                DCD    TIMER3_IRQHandler            ; 23 General-Purpose Timer 3 Handler 
                DCD    DMA0_IRQHandler              ; 24 DMA Channel 0 Handler 
                DCD    DMA1_IRQHandler              ; 25 DMA Channel 1 Handler 
                DCD    DMA2_IRQHandler              ; 26 DMA Channel 2 Handler 
                DCD    DMA3_IRQHandler              ; 27 DMA Channel 3 Handler 
                DCD    DMA4_IRQHandler              ; 28 DMA Channel 4 Handler 
                DCD    DMA5_IRQHandler              ; 29 DMA Channel 5 Handler 
                DCD    DMA6_IRQHandler              ; 30 DMA Channel 6 Handler 
                DCD    DMA7_IRQHandler              ; 31 DMA Channel 7 Handler 
                DCD    DIO0_IRQHandler              ; 32 DIO Interrupt 0 Handler
                DCD    DIO1_IRQHandler              ; 33 DIO Interrupt 1 Handler
                DCD    DIO2_IRQHandler              ; 34 DIO Interrupt 2 Handler
                DCD    DIO3_IRQHandler              ; 35 DIO Interrupt 3 Handler
                DCD    WATCHDOG_IRQHandler          ; 36 Watchdog Timer Handler
                DCD    SPI0_RX_IRQHandler           ; 37 SPI0 Receive Buffer Full Handler 
                DCD    SPI0_TX_IRQHandler           ; 38 SPI0 Transmit Buffer Empty Handler 
                DCD    SPI0_ERROR_IRQHandler        ; 39 SPI0 Error Handler 
                DCD    SPI1_RX_IRQHandler           ; 40 SPI1 Receive Buffer Full Handler 
                DCD    SPI1_TX_IRQHandler           ; 41 SPI1 Transmit Buffer Empty Handler 
                DCD    SPI1_ERROR_IRQHandler        ; 42 SPI1 Error Handler 
                DCD    I2C_IRQHandler               ; 43 I2C Handler
                DCD    UART_RX_IRQHandler           ; 44 UART0 Receive Buffer Full Handler
                DCD    UART_TX_IRQHandler           ; 45 UART0 Transmit Buffer Empty Handler 
                DCD    UART_ERROR_IRQHandler        ; 46 UART0 Error Handler 
                DCD    DMIC_OUT_OD_IN_IRQHandler    ; 47 DMIC and Output Driver Data Ready Handler
                DCD    DMIC_OD_ERROR_IRQHandler     ; 48 DMIC Overrun and Output Driver Underrun Detect Handler
                DCD    PCM_RX_IRQHandler            ; 49 PCM Receive Buffer Full Handler
                DCD    PCM_TX_IRQHandler            ; 50 PCM Transmit Buffer Empty Handler
                DCD    PCM_ERROR_IRQHandler         ; 51 PCM Error Handler
                DCD    DSP0_IRQHandler              ; 52 DSP Event 0 Handler 
                DCD    DSP1_IRQHandler              ; 53 DSP Event 1 Handler 
                DCD    DSP2_IRQHandler              ; 54 DSP Event 2 Handler 
                DCD    DSP3_IRQHandler              ; 55 DSP Event 3 Handler 
                DCD    DSP4_IRQHandler              ; 56 DSP Event 4 Handler 
                DCD    DSP5_IRQHandler              ; 57 DSP Event 5 Handler 
                DCD    DSP6_IRQHandler              ; 58 DSP Event 6 Handler 
                DCD    DSP7_IRQHandler              ; 59 DSP Event 7 Handler 
                DCD    BLE_CSCNT_IRQHandler         ; 60 BLE 625us time reference interrupt 
                DCD    BLE_SLP_IRQHandler           ; 61 BLE sleep mode interrupt 
                DCD    BLE_RX_IRQHandler            ; 62 BLE received packet interrupt 
                DCD    BLE_EVENT_IRQHandler         ; 63 BLE event interrupt 
                DCD    BLE_CRYPT_IRQHandler         ; 64 BLE AES ciphering complete interrupt 
                DCD    BLE_ERROR_IRQHandler         ; 65 BLE baseband error interrupt 
                DCD    BLE_GROSSTGTIM_IRQHandler    ; 66 BLE gross timer interrupt 
                DCD    BLE_FINETGTIM_IRQHandler     ; 67 BLE fine timer interrupt 
                DCD    BLE_SW_IRQHandler            ; 68 BLE SW triggered inerrupt 
                DCD    BLE_COEX_RX_TX_IRQHandler    ; 69 Coexistance BLE start/stop RX or TX interrupt
                DCD    BLE_COEX_IN_PROCESS_IRQHandler ; 70 Coexistance BLE in process interrupt
                DCD    RF_TX_IRQHandler             ; 71 BLE transmit interrupt 
                DCD    RF_RXSTOP_IRQHandler         ; 72 BLE receive stop interrupt 
                DCD    RF_IRQ_RECEIVED_IRQHandler   ; 73 BLE received packet interrupt 
                DCD    RF_SYNC_IRQHandler           ; 74 BLE received sync word interrupt 
                DCD    RF_TXFIFO_IRQHandler         ; 75 TX FIFO near underflow detect interrupt 
                DCD    RF_RXFIFO_IRQHandler         ; 76 RX FIFO near overflow detect interrupt 
                DCD    ASRC_ERROR_IRQHandler        ; 77 ASRC error interrupt 
                DCD    ASRC_IN_IRQHandler           ; 78 ASRC data input interrupt 
                DCD    ASRC_OUT_IRQHandler          ; 79 ASRC data output interrupt 
                DCD    AUDIOSINK_PHASE_IRQHandler   ; 80 Audio sink clock phase interrupt 
                DCD    AUDIOSINK_PERIOD_IRQHandler  ; 81 Audio sink clock period interrupt 
                DCD    CLKDET_IRQHandler            ; 82 Clock detection interrupt
                DCD    FLASH_COPY_IRQHandler        ; 83 Flash copy interrupt 
                DCD    FLASH_ECC_IRQHandler         ; 84 Flash ECC event interrupt 
                DCD    MEMORY_ERROR_IRQHandler      ; 85 Memory error event interrupt
#if RSL10_CID == 101
                DCD    BLE_AUDIO0_IRQHandler        ; 86 Audio over BLE channel 0 interrupt
                DCD    BLE_AUDIO1_IRQHandler        ; 86 Audio over BLE channel 1 interrupt
                DCD    BLE_AUDIO2_IRQHandler        ; 86 Audio over BLE channel 2 interrupt
#endif

__vector_table_End

__vector_table_Size  EQU      __vector_table_End - __vector_table
ISR_Vector_Table       EQU      __vector_table
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Default interrupt handlers.
;;
        THUMB
        PUBWEAK Reset_Handler
        SECTION .text:CODE:NOROOT:REORDER(2)
Reset_Handler

        MOV R0, SP
        BFC R0, #2, #1
        MOV SP, R0
        LDR     R0, =SystemInit
        BLX     R0
        LDR     R0, =__iar_program_start
        BX      R0

        PUBWEAK NMI_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
NMI_Handler
        B NMI_Handler

        PUBWEAK HardFault_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
HardFault_Handler
        B HardFault_Handler

        PUBWEAK MemManage_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
MemManage_Handler
        B MemManage_Handler

        PUBWEAK BusFault_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
BusFault_Handler
        B BusFault_Handler

        PUBWEAK UsageFault_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
UsageFault_Handler
        B UsageFault_Handler

        PUBWEAK SVC_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
SVC_Handler
        B SVC_Handler

        PUBWEAK DebugMon_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
DebugMon_Handler
        B DebugMon_Handler

        PUBWEAK PendSV_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
PendSV_Handler
        B PendSV_Handler

        PUBWEAK SysTick_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
SysTick_Handler
        B SysTick_Handler

        PUBWEAK WAKEUP_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
WAKEUP_IRQHandler  
        B WAKEUP_IRQHandler

        PUBWEAK RTC_ALARM_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
RTC_ALARM_IRQHandler  
        B RTC_ALARM_IRQHandler

        PUBWEAK RTC_CLOCK_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
RTC_CLOCK_IRQHandler  
        B RTC_CLOCK_IRQHandler

        PUBWEAK ADC_BATMON_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)  
ADC_BATMON_IRQHandler  
        B ADC_BATMON_IRQHandler

        PUBWEAK TIMER0_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
TIMER0_IRQHandler  
        B TIMER0_IRQHandler

        PUBWEAK TIMER1_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
TIMER1_IRQHandler  
        B TIMER1_IRQHandler

        PUBWEAK TIMER2_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
TIMER2_IRQHandler  
        B TIMER2_IRQHandler

        PUBWEAK TIMER3_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
TIMER3_IRQHandler  
        B TIMER3_IRQHandler

        PUBWEAK DMA0_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
DMA0_IRQHandler  
        B DMA0_IRQHandler

        PUBWEAK DMA1_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
DMA1_IRQHandler
        B DMA1_IRQHandler

        PUBWEAK DMA2_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DMA2_IRQHandler  
        B DMA2_IRQHandler

        PUBWEAK DMA3_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DMA3_IRQHandler  
        B DMA3_IRQHandler

        PUBWEAK DMA1_Stream1_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DMA1_Stream1_IRQHandler  
        B DMA1_Stream1_IRQHandler

        PUBWEAK DMA4_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DMA4_IRQHandler  
        B DMA4_IRQHandler

        PUBWEAK DMA5_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DMA5_IRQHandler  
        B DMA5_IRQHandler

        PUBWEAK DMA6_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DMA6_IRQHandler  
        B DMA6_IRQHandler

        PUBWEAK DMA7_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DMA7_IRQHandler  
        B DMA7_IRQHandler

        PUBWEAK DIO0_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DIO0_IRQHandler  
        B DIO0_IRQHandler

        PUBWEAK DIO1_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
DIO1_IRQHandler  
        B DIO1_IRQHandler

        PUBWEAK DIO2_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
DIO2_IRQHandler  
        B DIO2_IRQHandler

        PUBWEAK DIO3_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)  
DIO3_IRQHandler  
        B DIO3_IRQHandler

        PUBWEAK WATCHDOG_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)  
WATCHDOG_IRQHandler  
        B WATCHDOG_IRQHandler

        PUBWEAK SPI0_RX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)  
SPI0_RX_IRQHandler  
        B SPI0_RX_IRQHandler

        PUBWEAK SPI0_TX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
SPI0_TX_IRQHandler  
        B SPI0_TX_IRQHandler

        PUBWEAK SPI0_ERROR_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
SPI0_ERROR_IRQHandler  
        B SPI0_ERROR_IRQHandler

        PUBWEAK SPI1_RX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
SPI1_RX_IRQHandler  
        B SPI1_RX_IRQHandler

        PUBWEAK TIM1_TRG_COM_TIM11_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
TIM1_TRG_COM_TIM11_IRQHandler  
        B TIM1_TRG_COM_TIM11_IRQHandler
        
        PUBWEAK SPI1_TX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
SPI1_TX_IRQHandler  
        B SPI1_TX_IRQHandler

        PUBWEAK SPI1_ERROR_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
SPI1_ERROR_IRQHandler  
        B SPI1_ERROR_IRQHandler

        PUBWEAK I2C_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
I2C_IRQHandler  
        B I2C_IRQHandler

        PUBWEAK UART_RX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
UART_RX_IRQHandler  
        B UART_RX_IRQHandler

        PUBWEAK UART_TX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
UART_TX_IRQHandler  
        B UART_TX_IRQHandler

        PUBWEAK UART_ERROR_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
UART_ERROR_IRQHandler  
        B UART_ERROR_IRQHandler

        PUBWEAK DMIC_OUT_OD_IN_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
DMIC_OUT_OD_IN_IRQHandler  
        B DMIC_OUT_OD_IN_IRQHandler

        PUBWEAK DMIC_OD_ERROR_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
DMIC_OD_ERROR_IRQHandler  
        B DMIC_OD_ERROR_IRQHandler

        PUBWEAK PCM_RX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
PCM_RX_IRQHandler  
        B PCM_RX_IRQHandler

        PUBWEAK PCM_TX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
PCM_TX_IRQHandler  
        B PCM_TX_IRQHandler

        PUBWEAK PCM_ERROR_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
PCM_ERROR_IRQHandler  
        B PCM_ERROR_IRQHandler

        PUBWEAK USART2_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
USART2_IRQHandler  
        B USART2_IRQHandler

        PUBWEAK DSP0_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
DSP0_IRQHandler  
        B DSP0_IRQHandler

        PUBWEAK DSP1_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)   
DSP1_IRQHandler  
        B DSP1_IRQHandler

        PUBWEAK DSP2_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)   
DSP2_IRQHandler  
        B DSP2_IRQHandler

        PUBWEAK DSP3_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DSP3_IRQHandler  
        B DSP3_IRQHandler
      
        PUBWEAK DSP4_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DSP4_IRQHandler  
        B DSP4_IRQHandler

        PUBWEAK DSP5_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DSP5_IRQHandler  
        B DSP5_IRQHandler

        PUBWEAK TIM8_TRG_COM_TIM14_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
TIM8_TRG_COM_TIM14_IRQHandler  
        B TIM8_TRG_COM_TIM14_IRQHandler

        PUBWEAK DSP6_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
DSP6_IRQHandler  
        B DSP6_IRQHandler

        PUBWEAK DSP7_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DSP7_IRQHandler  
        B DSP7_IRQHandler

        PUBWEAK BLE_CSCNT_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
BLE_CSCNT_IRQHandler  
        B BLE_CSCNT_IRQHandler

        PUBWEAK BLE_SLP_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
BLE_SLP_IRQHandler  
        B BLE_SLP_IRQHandler

        PUBWEAK BLE_RX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
BLE_RX_IRQHandler  
        B BLE_RX_IRQHandler

        PUBWEAK BLE_EVENT_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
BLE_EVENT_IRQHandler  
        B BLE_EVENT_IRQHandler

        PUBWEAK BLE_CRYPT_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
BLE_CRYPT_IRQHandler  
        B BLE_CRYPT_IRQHandler

        PUBWEAK BLE_ERROR_IRQHandler 
        SECTION .text:CODE:NOROOT:REORDER(1)
BLE_ERROR_IRQHandler  
        B BLE_ERROR_IRQHandler

        PUBWEAK BLE_GROSSTGTIM_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)   
BLE_GROSSTGTIM_IRQHandler  
        B BLE_GROSSTGTIM_IRQHandler

        PUBWEAK BLE_FINETGTIM_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)   
BLE_FINETGTIM_IRQHandler  
        B BLE_FINETGTIM_IRQHandler

        PUBWEAK BLE_SW_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
BLE_SW_IRQHandler  
        B BLE_SW_IRQHandler

        PUBWEAK BLE_COEX_RX_TX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
BLE_COEX_RX_TX_IRQHandler  
        B BLE_COEX_RX_TX_IRQHandler

        PUBWEAK BLE_COEX_IN_PROCESS_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
BLE_COEX_IN_PROCESS_IRQHandler  
        B BLE_COEX_IN_PROCESS_IRQHandler

        PUBWEAK RF_TX_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
RF_TX_IRQHandler  
        B RF_TX_IRQHandler

        PUBWEAK RF_RXSTOP_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
RF_RXSTOP_IRQHandler  
        B RF_RXSTOP_IRQHandler

        PUBWEAK RF_IRQ_RECEIVED_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
RF_IRQ_RECEIVED_IRQHandler  
        B RF_IRQ_RECEIVED_IRQHandler

        PUBWEAK RF_SYNC_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)  
RF_SYNC_IRQHandler  
        B RF_SYNC_IRQHandler

        PUBWEAK RF_TXFIFO_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
RF_TXFIFO_IRQHandler  
        B RF_TXFIFO_IRQHandler

        PUBWEAK RF_RXFIFO_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)  
RF_RXFIFO_IRQHandler  
        B RF_RXFIFO_IRQHandler

        PUBWEAK ASRC_ERROR_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)  
ASRC_ERROR_IRQHandler  
        B ASRC_ERROR_IRQHandler

        PUBWEAK ASRC_IN_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)  
ASRC_IN_IRQHandler  
        B ASRC_IN_IRQHandler

        PUBWEAK OTG_FS_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
OTG_FS_IRQHandler  
        B OTG_FS_IRQHandler

        PUBWEAK ASRC_OUT_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
ASRC_OUT_IRQHandler  
        B ASRC_OUT_IRQHandler

        PUBWEAK DMA2_Stream6_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DMA2_Stream6_IRQHandler  
        B DMA2_Stream6_IRQHandler

        PUBWEAK AUDIOSINK_PHASE_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
AUDIOSINK_PHASE_IRQHandler  
        B AUDIOSINK_PHASE_IRQHandler

        PUBWEAK AUDIOSINK_PERIOD_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
AUDIOSINK_PERIOD_IRQHandler  
        B AUDIOSINK_PERIOD_IRQHandler

        PUBWEAK CLKDET_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
CLKDET_IRQHandler  
        B CLKDET_IRQHandler

        PUBWEAK FLASH_COPY_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1) 
FLASH_COPY_IRQHandler  
        B FLASH_COPY_IRQHandler

        PUBWEAK FLASH_ECC_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
FLASH_ECC_IRQHandler  
        B FLASH_ECC_IRQHandler

        PUBWEAK MEMORY_ERROR_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
MEMORY_ERROR_IRQHandler  
        B MEMORY_ERROR_IRQHandler
        
#if RSL10_CID == 101
        PUBWEAK BLE_AUDIO0_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)    
BLE_AUDIO0_IRQHandler  
        B BLE_AUDIO0_IRQHandler

        PUBWEAK BLE_AUDIO1_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
BLE_AUDIO1_IRQHandler  
        B BLE_AUDIO1_IRQHandler

        PUBWEAK DCMI_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)
DCMI_IRQHandler  
        B DCMI_IRQHandler

        PUBWEAK BLE_AUDIO2_IRQHandler
        SECTION .text:CODE:NOROOT:REORDER(1)  
BLE_AUDIO2_IRQHandler  
        B BLE_AUDIO2_IRQHandler

#endif       
        END