/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * start.c
 * - Cortex-M3 Start Up function
 * ----------------------------------------------------------------------------
 * $Revision: 1.23 $
 * $Date: 2018/10/30 15:00:41 $
 * ------------------------------------------------------------------------- */

#include "rsl10.h"

/* ----------------------------------------------------------------------------
 * Defines and Macros
 * --------------------------------------------------------------------------*/
#define DMA_NUM                         0
#define DMA_CFG                         (DMA_ENABLE                 | \
                                         DMA_ADDR_LIN               | \
                                         DMA_TRANSFER_P_TO_M        | \
                                         DMA_PRIORITY_0             | \
                                         DMA_COMPLETE_INT_ENABLE    | \
                                         DMA_DISABLE_INT_DISABLE    | \
                                         DMA_ERROR_INT_ENABLE       | \
                                         DMA_COUNTER_INT_DISABLE    | \
                                         DMA_START_INT_DISABLE      | \
                                         DMA_LITTLE_ENDIAN          | \
                                         DMA_SRC_WORD_SIZE_32        | \
                                         DMA_DEST_WORD_SIZE_32      | \
                                         DMA_DEST_ADDR_INC          | \
                                         DMA_DEST_ADDR_STEP_SIZE_1  | \
                                         DMA_SRC_PBUS               | \
                                         DMA_SRC_ADDR_STATIC)
#define THREE_BLOCK_APPN(x, y, z)       x##y##z
#define DMA_IRQn(x)                     THREE_BLOCK_APPN(DMA, x, _IRQn)

/* ----------------------------------------------------------------------------
 * Application entry point
 * ------------------------------------------------------------------------- */
extern int main(void);

/* ----------------------------------------------------------------------------
 * Function      : void _start(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the application data and start execution with
 *                 main. Should be called from the reset vector.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : The symbols __data_init__, __data_start__, __data_end__,
 *                 __bss_start__, and __bss_end__ are defined when the
 *                 application is linked.
 * ------------------------------------------------------------------------- */
void __attribute__ ((noreturn)) _start (void)
{
    uint32_t *temp;
    uint32_t *tempInit;
    uint32_t i;
    uint8_t manual_data_copy = 0;

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Check if the destination is from flash or elsewhere */
    if ((uint32_t)&__data_init__ < FLASH_MAIN_TOP)
    {
        /* Enable the flash copier interrupt */
        NVIC_EnableIRQ(FLASH_COPY_IRQn);

        FLASH_COPY_CFG->COPY_DEST_ALIAS = COPY_TO_MEM_BITBAND;
        FLASH_COPY_CFG->COPY_MODE_ALIAS = COPY_TO_32BIT_BITBAND;
        FLASH_COPY_CFG->MODE_ALIAS = COPY_MODE_BITBAND;

        /* Setup the source, destination, and length of the copy */
        FLASH->COPY_SRC_ADDR_PTR = (uint32_t)&__data_init__;
        FLASH->COPY_DST_ADDR_PTR = (uint32_t)&__data_start__;
        FLASH->COPY_WORD_CNT = ((uint32_t)&__data_end__ - (uint32_t)&__data_start__) >> 2;

        /* Start the copy */
        FLASH_COPY_CTRL->START_ALIAS = COPY_START_BITBAND;
        do
        {
            /* Core goes into sleep mode and waits on an interrupt */
            __WFI();
        }
        while (FLASH_COPY_CTRL->BUSY_ALIAS != COPY_IDLE_BITBAND);

        /* Clear pending flash copier interrupt*/
        NVIC_ClearPendingIRQ(FLASH_COPY_IRQn);

        /* Disable flash copier interrupt */
        NVIC_DisableIRQ(FLASH_COPY_IRQn);

        if (FLASH_COPY_CTRL->ERROR_ALIAS == COPY_ERROR_BITBAND)
        {
            manual_data_copy = 1;
        }
    }
    else
    {
        manual_data_copy = 1;
    }

    /* Copy data memory manually */
    if (manual_data_copy == 1)
    {
        /* Initialize the data sections */
        temp = &__data_start__;
        tempInit = &__data_init__;

        while (temp < &__data_end__)
        {
            *temp++ = *tempInit++;
        }
    }

    /* Initialize the bss sections */
    /* Enable DMA interrupt */
    NVIC_EnableIRQ(DMA_IRQn(DMA_NUM));
    Sys_DMA_ClearChannelStatus(DMA_NUM);
    Sys_DMA_ChannelConfig(DMA_NUM,
                          DMA_CFG,
                          ((uint32_t)&__bss_end__ - (uint32_t)&__bss_start__) >> 2,
                          0,
                          (uint32_t)&SYSCTRL->DSS_CMD,
                          (uint32_t)&__bss_start__);

    do
    {
        /* Core goes into sleep mode and waits on an interrupt */
        __WFI();
    }
    while (DMA->STATUS[DMA_NUM] == 0);

    /* Clear pending DMA interrupt */
    NVIC_ClearPendingIRQ(DMA_IRQn(DMA_NUM));

    /* Disable DMA interrupt */
    NVIC_DisableIRQ(DMA_IRQn(DMA_NUM));

    /* Reset manually if the DMA fails */
    if (DMA_STATUS[DMA_NUM].ERROR_INT_STATUS_ALIAS == DMA_ERROR_INT_STATUS_BITBAND)
    {
        /* Initialize the bss sections */
        temp = &__bss_start__;
        while (temp < &__bss_end__)
        {
            *temp++ = 0;
        }
    }

    /* Initialize the heap */
    _sbrk(0);

    /* Execute the pre-initialization code */
    for (i = 0; i < (__preinit_array_end__ - __preinit_array_start__); i++)
    {
        __preinit_array_start__[i] ();
    }

    /* Execute the initialization code */
    for (i = 0; i < (__init_array_end__ - __init_array_start__); i++)
    {
        __init_array_start__[i] ();
    }

    /* Call the main entry point. */
    main();

    /* Wait for the watchdog reset to trigger since main returned
     * and it should not have returned. */
    while (1);
}
