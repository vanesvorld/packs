/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * sbrk.c
 * - Implementation of the sbrk system function
 * ----------------------------------------------------------------------------
 * $Revision: 1.10 $
 * $Date: 2019/01/16 16:05:41 $
 * ------------------------------------------------------------------------- */

#include "rsl10.h"

/* ----------------------------------------------------------------------------
 * Function      : int8_t * _sbrk(int increment)
 * ----------------------------------------------------------------------------
 * Description   : Increment (or decrement) the top of the heap
 * Inputs        : increment    - Increment to be applied to the top of the
 *                                heap
 * Outputs       : return value - The prior value of the heap top (points to
 *                                the base of the newly allocated data if the
 *                                heap was incremented); returns -1 if the
 *                                function was unable to allocate the
 *                                requested memory
 * Assumptions   : The symbols __Heap_Begin__, __Heap_Limit__ are defined
 *                 when the application is linked.
 * ------------------------------------------------------------------------- */
int8_t * _sbrk(int increment)
{
    static uint8_t* heapTop;
    int8_t * result;

    /* Ensure that heapTop has been initialized */
    if (heapTop == 0)
    {
        heapTop = &__Heap_Begin__;
    }

    /* Modify the increment to ensure the memory allocated is word aligned */
    if (increment > 0)
    {
        increment = (increment + 3) & (~0x3);
        if ((heapTop + increment) > &__Heap_Limit__)
        {
            /* Flag an error in allocating memory (no space) and return */
            errno = ENOMEM;
            return (int8_t *) -1;
        }
    }
    else if (increment < 0)
    {
        increment = (increment - 3) & (~0x3);
        if ((heapTop + increment) < &__Heap_Begin__)
        {
            /* Flag an error in allocating memory (deallocating too much
             * space) and return */
            errno = ENOMEM;
            return (int8_t *) -1;
        }
    }

    /* We can allocate the memory, so update the top of the heap and return
     * a pointer to the previous heap top */
    result = (int8_t *) heapTop;
    heapTop = heapTop + increment;
    return result;
}
