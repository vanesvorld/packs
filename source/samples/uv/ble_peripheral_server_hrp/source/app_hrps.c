/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_hrps.c
 * - Bluetooth heart rate profile application code. This file deals with the
 *      heart rate profile code that is dependent of the user application.
 * ------------------------------------------------------------------------- */
#include <app.h>
#include <app_hrps.h>

static struct HRPS_Measure_t APP_HRP_measure;

/** Flag for energy expended field to be notified (every 10s) */
static uint8_t APP_HRPS_send_hrp_ntf_energy_expended;

/* ----------------------------------------------------------------------------
 * Function      : int APP_HRPS_Initialize( void )
 * ----------------------------------------------------------------------------
 * Description   : Initialize the heart rate app environment and message handlers
 * Inputs        : None
 * Outputs       : 0 if the initialization was successful, < 0 otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
/**
 * @brief Initialize the heart rate app environment and message handlers
 *
 * Assumptions : None
 *
 * @return  0 if the initialization was successful, < 0 otherwise
 */
int APP_HRPS_Initialize(void)
{
    memset(&APP_HRP_measure, '\0', sizeof(struct HRPS_Measure_t));

    APP_HRPS_send_hrp_ntf_energy_expended = 0;

    return 0;
}

/* ----------------------------------------------------------------------------
 * Function      : int APP_HRPS_HeartRateMeasurementUpdate(struct hrs_hr_meas *meas)
 * ----------------------------------------------------------------------------
 * Description   : Updates the heart level measurement values
 * Inputs        : None
 * Outputs       : return value - Pointer to the updated HRPS_Measure_t structure
 * Assumptions   : None.
 * ------------------------------------------------------------------------- */
/**
 * @brief Updates the heart level measurement values
 *
 * Assumptions   : None
 *
 * @return 0 if successful, < 0 otherwise
 */
const struct HRPS_Measure_t *APP_HRPS_HeartRateMeasurementUpdate(void)
{
    APP_HRP_measure.skin_contact = 1;

    APP_HRP_measure.measurement.nb_rr_interval = 0;

    /* Create a phony value between 64 and 191 */
    APP_HRP_measure.measurement.heart_rate = (rand() % (191 - 64)) + 64;

    APP_HRP_measure.measurement.flags =  HRS_FLAG_SENSOR_CCT_FET_SUPPORTED |
                   HRS_FLAG_RR_INTERVAL_NOT_PRESENT;

    /* 3.1.1.3 Energy Expended Field
     * The Energy Expended field represents the accumulated energy expended in
     * kilo Joules since the last time it was reset.
     *
     * Since Energy Expended is a UINT16, the highest value that can be
     * represented is 65535 kilo Joules. If the maximum value of 65535 kilo
     * Joules is attained (0xFFFF), the field value should remain at 0xFFFF
     * so that the client can be made aware that a reset of the Energy Expended
     * Field is required. */
    if( APP_HRP_measure.measurement.energy_expended < 0xFFFF )
        /* Energy expended value incremented every second */
        APP_HRP_measure.measurement.energy_expended += 1;

    /* If energy expended is used, it is typically only included in the eart
     * Rate Measurement characteristic once every 10 measurements at a regular
     * interval. */
    /* The above typo is actually in the Heart Rate Service spec :-P */
    APP_HRPS_send_hrp_ntf_energy_expended++;
    /* If timeout expired, send a complete measurement */
    if( APP_HRPS_send_hrp_ntf_energy_expended
            >= APP_HRPS_ENERGY_UPDATE_TIMEOUT_S )
    {
        APP_HRPS_send_hrp_ntf_energy_expended = 0;

        APP_HRP_measure.measurement.flags |= HRS_FLAG_ENERGY_EXPENDED_PRESENT;
    }

    if( APP_HRP_measure.skin_contact != 0 )
    {
        APP_HRP_measure.measurement.flags |= HRS_FLAG_SENSOR_CCT_DETECTED;
    }

    /* If the Heart Rate Measurement Value is less than or equal to 255 bpm
     * a UINT8 format should be used for power savings. */
    APP_HRP_measure.measurement.flags |= (
            APP_HRP_measure.measurement.heart_rate < 256 ?
                    HRS_FLAG_HR_8BITS_VALUE : HRS_FLAG_HR_16BITS_VALUE);

    return &APP_HRP_measure;
}

/* ----------------------------------------------------------------------------
 * Function      : void APP_HRPS_EnergyExpResetInd(void)
 * ----------------------------------------------------------------------------
 * Description   : Callback function to reset the energy expanded value
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void APP_HRPS_EnergyExpResetInd(void)
{
    /* resetting energy expended value to 0. */
    APP_HRP_measure.measurement.energy_expended = 0;
}
