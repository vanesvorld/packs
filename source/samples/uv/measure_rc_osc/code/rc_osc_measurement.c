/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 * ----------------------------------------------------------------------------
 * rc_osc_measurements.c
 * - Support functions to measure the frequency of the 32 kHz RC oscillator
 * ----------------------------------------------------------------------------
 * $Revision: 1.2 $
 * $Date: 2017/12/04 18:35:24 $
 * ------------------------------------------------------------------------- */

#include "app.h"

/* RC oscillator measurement parameters */
rc_osc_measurements_t rc_osc_measurements;

/* ----------------------------------------------------------------------------
 * Function      : void RC_OSC_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize RC oscillator measurement parameters
 *                 and audio sink block/interrupt.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void RC_OSC_Initialize(void)
{
    /* Initialize parameters related to RC oscillator measurements */
    rc_osc_measurements.enabled            = false;
    rc_osc_measurements.SYSCLK_periods     = 0;
    rc_osc_measurements.SYSCLK_periods_sum = 0;
    rc_osc_measurements.sample_cnt         = 0;
    rc_osc_measurements.avg_freq           = 0;

    /* Initialize audio sink block */
    Sys_Audiosink_ResetCounters();
    Sys_Audiosink_InputClock(0, AUDIOSINK_CLK_SRC_STANDBYCLK);
    Sys_Audiosink_Config(AUDIO_SINK_PERIODS_16, 0, 0);

    /* Clear and enable audio sink clock period interrupt */
    NVIC_ClearPendingIRQ(AUDIOSINK_PERIOD_IRQn);
    NVIC_EnableIRQ(AUDIOSINK_PERIOD_IRQn);
}

/* ----------------------------------------------------------------------------
 * Function      : void AUDIOSINK_PERIOD_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Get the number of SYSCLK cycles per audio sink period.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void AUDIOSINK_PERIOD_IRQHandler(void)
{
    /* Record SYSCLK period count value */
    rc_osc_measurements.SYSCLK_periods = Sys_Audiosink_PeriodCounter();

    /* Reset period counter */
    AUDIOSINK->PERIOD_CNT = 0;
}
