/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Main application file
 * ----------------------------------------------------------------------------
 * $Revision: 1.1 $
 * $Date: 2018/11/19 15:09:46 $
 * ------------------------------------------------------------------------- */

#include "app.h"

int main(void)
{
    App_Initialize();

    /* Run the kernel scheduler and refresh the watchdog timer */
    while (1)
    {
        Kernel_Schedule();

        /* Enable RTS for 4 wire UART simulation*/
        #if (CFG_RTS)
        Sys_GPIO_Set_Low(CFG_UART_RTS_DIO);
        Sys_GPIO_Set_High(CFG_UART_RTS_DIO);
        #endif    /* CFG_RTS */

        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* Wait for an event before executing the scheduler again */
        SYS_WAIT_FOR_EVENT;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void TIMER0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Run the Watchdog timer ever 200 ms
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER0_IRQHandler(void)
{
    Sys_Watchdog_Refresh();
}
