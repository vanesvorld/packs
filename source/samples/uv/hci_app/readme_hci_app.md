Host Controller Interface Application
=====================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
The Host Controller Interface (HCI) application initializes the device and 
UART drivers required to perform HCI command transactions to and from RSL10. 
The UART transactions enable the Direct Test Mode (DTM) tests to be conducted.
The application configures voltage regulators, RF power supplies, and UART 
flow control signals. UART support code is used to facilitate communication 
between the UART drivers and the application.

The UART support code interfaces with the UART drivers using the DMA. Calls
to the UART API trigger functions in the code perform the appropriate DMA
operations for read or write calls. UART read operations transfer data from
the DMA controller to the calling function. UART write operations transfer
data from calling function to the DMA controller. The DMA operation status is
returned to the calling function after the transaction completes. The
designated DMA channels for TX and RX are channel 0 and channel 1
respectively.

Configurations
--------------
The RF power supply can be configured for either 6 dBm or 0 dBm mode. In
6 dBm mode, the power supply uses both the VDDPA and VDDRF. In 0 dBm
mode, only VDDRF is used. The voltage regulator can be configured to use 
either low drop output (LDO) or the buck converter. The following preprocessor 
symbols control these configurations:

    CFG_BUCK_CONV         If set, uses the buck converter; otherwise uses LDO
    CFG_6DBM              If set, uses 6 dBm mode; otherwise uses 0 dBm  mode
    
Structure
---------
This sample project is structured as follows:

The source code exists in a `code` folder, all application-related include
header files are in the `include` folder, and the `main()` function `app.c` is 
located in the parent directory.

Code
----
    app_init.c    - All initialization functions are called here, but the 
                    implementation is in the respective C files
    uart.c        - Support code for handling DMA-based UART data transfers

Include
-------
    app.h        - Overall application header file
    uart.h       - Header file for the DMA-based UART data transfer support 
                   code

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development 
Board, with no external connection required. Two devices are required to fully 
test the application.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.

Select the project configuration according to the chip ID version of RSL10. 
The appropriate libraries and include files are loaded according to the build
configuration that you choose. Use **Debug** for CID 101. Ensure that the 
device CID matches the application build.

Verification
------------
To verify that this application is functioning correctly, use two RSL10 
Evaluation and Development boards, or an RSL10 coupled with another 
third-party peripheral application. Set up the devices to take the roles of 
device under test (DUT), and lower tester (LT), as instructed in the dtm 
script `readme` file. Use the upper tester (`dtm_script`) to send data packets
to the DUT. If the packet count received from the LT is consistent with the 
transfer from the UT (upper tester) to the DUT, the application is functioning correctly. The functionality can be tested and verified further by changing 
factors such as baud rate, runtime, payload type, and payload length.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC 
(d/b/a ON Semiconductor).
