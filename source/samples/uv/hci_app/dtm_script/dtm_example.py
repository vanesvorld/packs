# ------------------------------------------------------------------------------
# Copyright (c) 2016 Semiconductor Components Industries, LLC (d/b/a ON
# Semiconductor), All Rights Reserved
# 
# This code is the property of ON Semiconductor and may not be redistributed
# in any form without prior written permission from ON Semiconductor.
# The terms of use and warranty for this code are covered by contractual
# agreements between ON Semiconductor and the licensee.
# ------------------------------------------------------------------------------
#  dtm_example.py
#    - Example code which utilizes the dtm library to run the device test mode 
#      tests for RSL10
# ------------------------------------------------------------------------------
#  $Revision: 1.1 $
#  $Date: 2018/11/19 15:09:46 $
# ------------------------------------------------------------------------------

import dtm_lib as dtm
import serial.tools.list_ports as port_list
import argparse
import sys

# Default globals
DTM_Channel         = 1
LT_Port             = 1
DUT_Port            = 1
Baud_Rate           = 9600
PDU_Length          = 37
PDU_Type            = dtm.Payload_Type.PRBS9
Test_Time_Sec       = 4
PHY                 = dtm.PHY_Type.LE_1M
Modulation          = dtm.Mod_Index.Standard
Logging_Enable      = False


def display_com_ports():
    '''
    Print out the available serial ports of the upper tester
    '''
    print "Available COM ports:"
    ports = list(port_list.comports())
    for p in ports:
        print "%-10s| %s" % (p[0],p[1])

def main():
    """
    The following is an example of using the dtm library provided in dtm_lib to setup
    a testing scenario. This library can be used to created automated tests which 
    sweep across different channels, payload sizes, payload type and duration.
    """
    
    # Parse command line arguments. Set default values if argument not provided.
    parser = argparse.ArgumentParser(
        description='Configure direct test mode (DTM) tests')

    parser.add_argument('--Channel',
        dest='DTM_Channel',
        default=DTM_Channel,
        help='Specify physical channel to test using direct test mode (DTM) [0 to 39]')

    parser.add_argument('--BaudRate',
        dest='Baud_Rate',
        default=Baud_Rate,
        help='Specify UART baud rate')

    parser.add_argument('--LTCOM',
        dest='LT_COM_Port',
        default=LT_Port,
        help='Specify serial port for lower tester')

    parser.add_argument('--DUTCOM',
        dest='DUT_COM_Port',
        default=DUT_Port,
        help='Specify serial port for device under test')

    parser.add_argument('--PDULength',
        dest='Packet_Payload_Length',
        default=PDU_Length,
        help='Specify packet payload length [1 to 255]')

    parser.add_argument('--PDUType',
        dest='Packet_Payload_Type',
        default=PDU_Type,
        help='Specify packet payload type [0 to 7]')

    parser.add_argument('--TestTime',
        dest='Test_Run_Time',
        default=Test_Time_Sec,
        help='Specify test run time')

    parser.add_argument('--PHY',
        dest='PHY_Type',
        default=PHY,
        help='Specify PHY type for enhanced data rate (ENH) [1 or 2]')

    parser.add_argument('--Mod',
        dest='MOD_Index',
        default=Modulation,
        help='Specify modulation index for enhanced data rate (ENH) [0 (Standard) or 1 (Stable)]')

    parser.add_argument('--Logging',
        dest='Logging_Flag',
        default=Logging_Enable,
        help='Specify logging to console visibility [0 or 1]')

    args = parser.parse_args()

    # Check if the user has added in the serial ports as args, if not then
    # show the com ports and let the user select ports for DUT and LT. Check
    # if the input port values are numeric and if not set to default values.
    if args.LT_COM_Port == args.DUT_COM_Port:
        display_com_ports()

        args.LT_COM_Port = raw_input("\nEnter lower tester (LT) COM port: ")
        if not args.LT_COM_Port.isdigit():
            sys.stderr.write("Invalid port value %s for lower tester; "       \
                            % (args.LT_COM_Port))

        args.DUT_COM_Port = raw_input("\nEnter device under test (DUT) COM port: ")
        if not args.DUT_COM_Port.isdigit():
            sys.stderr.write("Invalid port value %s for device under test; "  \
                             % (args.DUT_COM_Port))

    # Initialize setup parameter dict from arguments and create test object.
    DTM_setup = {'LT_COM_Port':'COM' + args.LT_COM_Port,
                 'DUT_COM_Port':'COM' + args.DUT_COM_Port,
                 'DTM_Channel':int(args.DTM_Channel),
                 'Packet_Payload_Length':int(args.Packet_Payload_Length),
                 'Packet_Payload_Type':int(args.Packet_Payload_Type),
                 'Baud_Rate':int(args.Baud_Rate),
                 'Test_Run_Time':int(args.Test_Run_Time),
                 'PHY_Type':int(args.PHY_Type),
                 'MOD_Index':int(args.MOD_Index),
                 'Log':int(args.Logging_Flag)}

    DTM_test = dtm.DTMTest(DTM_setup)

    # Left align each parameter name followed by the parameter value.
    print "\n%-40s: %s" % ("Lower Tester COM Port", DTM_test.LT_COM_Port)
    print "%-40s: %s"   % ("Device Under Test COM Port", DTM_test.DUT_COM_Port)
    print "%-40s: %s"   % ("DTM Channel", DTM_test.DTM_Channel)
    print "%-40s: %s"   % ("Packet Payload Length", DTM_test.Payload_Length)
    print "%-40s: %s"   % ("Baud Rate", DTM_test.Baud_Rate)
    print "%-40s: %s"   % ("Test Run Time", DTM_test.Test_Run_Time)
    print "%-40s: %s"   % ("PHY Type", dtm.PHY_Type.get_name(DTM_test.PHY))
    
    # Use display method to get the enum names to display and left align.
    print "%-40s: %s"   % ("Packet Payload Type",
                        dtm.Payload_Type.get_name(DTM_test.Packet_Payload_Type))

    if args.PHY_Type == dtm.PHY_Type.LE_2M:
        print "%-40s: %s" % ("Modulation Index", dtm.Mod_Index.get_name(DTM_test.Mod))

    try:
        print "===================TRANSMIT TEST=================="
        DTM_test.transmit_test()
        print "\nPacket Error Rate: " + "{:5.3f}%\n".format(DTM_test.TX_PER)

        print "===================RECEIVE TEST==================="
        DTM_test.receive_test()
        print "\nPacket Error Rate: ", "{:5.3f}%\n".format(DTM_test.RX_PER)

    except dtm.DTMError as e:
        print e.error_message()

if __name__ == '__main__':
    main()
