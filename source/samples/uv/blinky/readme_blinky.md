Blinky - Simple GPIO I/O Sample Code
====================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project demonstrates a simple application that:

 1. Uses DIO6 as a toggling output signal. This DIO has been selected so that 
    the LED on the Evaluation and Development Board flashes.

 2. Uses DIO5 as an input signal to disable and re-enable toggling on DIO6.
    This DIO has been selected so the DIO-connected push-button input switch 
    on the Evaluation and Development Board can be used to modify the DIO6 toggling state.

The source code exists in `app.c`, with no defined header files.

Hardware Requirements
---------------------
This application can be executed on an RSL10 Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.

Verification
------------
To verify that this application is functioning correctly, DIO6 continuously
flashes on the Evaluation and Development Board.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which pauses at the
    start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
