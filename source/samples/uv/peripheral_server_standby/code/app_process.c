/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_process.c
 * - Application task handler definition and support processes
 * ----------------------------------------------------------------------------
 * $Revision: 1.53 $
 * $Date: 2019/09/06 19:12:01 $
 * ------------------------------------------------------------------------- */

#include "app.h"

/* Parameters for low power clock period measurements */
#define MAX_BUF_CNT                     5
float measure_buf[MAX_BUF_CNT];
uint8_t buf_cnt=0;
volatile uint32_t loop_cnt = 0;
volatile uint16_t allow_standby = 0;

volatile uint32_t standby_cnt = 0;
volatile bool cs_ntf_send_flag = false;

volatile bool wakeup_from_standby = false;

#if !RC_OSC_UPDATE
volatile int update_disable_flag = 0;
#endif    /* if !RC_OSC_UPDATE */

const struct ke_task_desc TASK_DESC_APP =
{
    NULL,
    &appm_default_handler,
    appm_state,
    APPM_STATE_MAX,
    APP_IDX_MAX
};

/* State and event handler definition */
const struct ke_msg_handler appm_default_state[] =
{
    /* Note: Put the default handler on top as this is used for handling any
     *       messages without a defined handler */
    { KE_MSG_DEFAULT_HANDLER, (ke_msg_func_t)Msg_Handler },
    BLE_MESSAGE_HANDLER_LIST,
    BASS_MESSAGE_HANDLER_LIST,
    CS_MESSAGE_HANDLER_LIST,
    APP_MESSAGE_HANDLER_LIST
};

/* Use the state and event handler definition for all states. */
const struct ke_state_handler appm_default_handler
    = KE_STATE_HANDLER(appm_default_state);

/* Defines a place holder for all task instance's state */
ke_state_t appm_state[APP_IDX_MAX];

volatile uint8_t DIO_wakeup_flag = 0;

/* ----------------------------------------------------------------------------
 * Interrupt handler declaration
 * ------------------------------------------------------------------------- */
extern void DIO0_IRQHandler(void);

/* ----------------------------------------------------------------------------
 * Function      : int APP_Timer(ke_msg_idd_t const msg_id,
 *                                void const *param,
 *                                ke_task_id_t const dest_id,
 *                                ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handle timer event message
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameter (unused)
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : return value - Indicate if the message was consumed;
 *                                compare with KE_MSG_CONSUMED
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int APP_Timer(ke_msg_id_t const msg_id,
              void const *param,
              ke_task_id_t const dest_id,
              ke_task_id_t const src_id)
{

    #ifdef APP_TIMER_ENABLED

    /* Restart timer */
    ke_timer_set(APP_TIMER, TASK_APP, APP_TIMER_INTV_10MS);
    #endif    /* ifdef APP_TIMER_ENABLED */

    return (KE_MSG_CONSUMED);
}

/* ----------------------------------------------------------------------------
 * Function      : int Msg_Handler(ke_msg_id_t const msg_id,
 *                                 void const *param,
 *                                 ke_task_id_t const dest_id,
 *                                 ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handle any message received from kernel that doesn't have
 *                 a dedicated handler
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameter (unused)
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : return value - Indicate if the message was consumed;
 *                                compare with KE_MSG_CONSUMED
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int Msg_Handler(ke_msg_id_t const msg_id, void *param,
                ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    return (KE_MSG_CONSUMED);
}

/* ----------------------------------------------------------------------------
 * Function      : void Standby_Mode_Configure(
 *                       struct standby_mode_env_tag *standby_mode_env)
 * ----------------------------------------------------------------------------
 * Description   : Configure the standby mode
 * Inputs        : Pre-defined parameters and configurations
 *                 for the standby mode
 * Outputs       : standby_mode_env - Parameters and configurations
 *                                    for the standby mode
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Standby_Mode_Configure(struct standby_mode_env_tag *standby_mode_env)
{
    /* If RTC clock source is XTAL 32 kHz oscillator */
    if (RTC_CLK_SRC == RTC_CLK_SRC_XTAL32K)
    {
        /* Enable XTAL32K oscillator amplitude control
         * Set XTAL32K load capacitance to 0x38: 22.4 pF
         * Enable XTAL32K oscillator */

        ACS->XTAL32K_CTRL = (XTAL32K_XIN_CAP_BYPASS_DISABLE                                |
                             XTAL32K_AMPL_CTRL_ENABLE                                      |
                             XTAL32K_NOT_FORCE_READY                                       |
                             (XTAL32K_CLOAD_TRIM_VALUE << ACS_XTAL32K_CTRL_CLOAD_TRIM_Pos) |
                             (XTAL32K_ITRIM_VALUE << ACS_XTAL32K_CTRL_ITRIM_Pos)           |
                             XTAL32K_IBOOST_DISABLE                                        |
                             XTAL32K_ENABLE);

        /* Wait for XTAL32K oscillator to be ready */
        while (ACS_XTAL32K_CTRL->READY_ALIAS != XTAL32K_OK_BITBAND)
        {
        }

        LowPowerClock_Source_Set(0);
    }

    /* Else: if RTC clock source is RC 32 kHz oscillator */
    else if (RTC_CLK_SRC == RTC_CLK_SRC_RC_OSC)
    {
        ACS_VDDC_CTRL->STANDBY_VTRIM_BYTE = ACS_VDDC_CTRL->VTRIM_BYTE;

        /* Start the RC oscillator */
        Sys_Clocks_Osc32kHz(RC_OSC_ENABLE | RC_OSC_NOM);

        /* Read the OSC_32K calibration trim data from NVR4 */
        unsigned int osc_calibration_value = 0;
        Sys_ReadNVR4(MANU_INFO_OSC_32K, 1, (unsigned
                                            int *)&osc_calibration_value);

        /* Use calibrated value for RC clock */
        if (osc_calibration_value != 0xFFFFFFFF)
        {
            ACS_RCOSC_CTRL->FTRIM_32K_BYTE = (uint8_t)(osc_calibration_value);
        }

        /* Delay for 4 ms */
        Sys_Delay_ProgramROM(4 * (SystemCoreClock / 1000));

        LowPowerClock_Source_Set(1);

        /* In us, for typical RCOSC until measurement is obtained. */
        RTCCLK_Period_Value_Set(RCCLK_PERIOD_VALUE);

        /* Set-up the Audiosink block for frequency measurement */
        Sys_Audiosink_ResetCounters();
        Sys_Audiosink_InputClock(0, AUDIOSINK_CLK_SRC_STANDBYCLK);
        Sys_Audiosink_Config(AUDIO_SINK_PERIODS_16, 0, 0);

        /* Enable interrupts */
        NVIC_ClearPendingIRQ(AUDIOSINK_PERIOD_IRQn);
        NVIC_EnableIRQ(AUDIOSINK_PERIOD_IRQn);

        /* Start period counter to start period measurement */
        AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = 1;
    }

    /* else: if RTC clock source is external oscillator */
    else
    {
        DIO->CFG[EXT_LOW_POWER_CLK_GPIO_NUM] = (DIO_2X_DRIVE     |
                                                DIO_LPF_DISABLE  |
                                                DIO_NO_PULL      |
                                                DIO_MODE_INPUT);

        LowPowerClock_Source_Set(1);

        /* Clock period in us for external clock */
        RTCCLK_Period_Value_Set(EXT_LOW_POWER_CLK_PERIOD_VALUE);

        /* Set-up the Audiosink block for frequency measurement */
        Sys_Audiosink_ResetCounters();
        Sys_Audiosink_InputClock(0, AUDIOSINK_CLK_SRC_STANDBYCLK);
        Sys_Audiosink_Config(AUDIO_SINK_PERIODS_16, 0, 0);

        /* Enable interrupts */
        NVIC_ClearPendingIRQ(AUDIOSINK_PERIOD_IRQn);
        NVIC_EnableIRQ(AUDIOSINK_PERIOD_IRQn);

        /* Start period counter to start period measurement */
        AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = 1;
    }

    standby_mode_env->RTC_clk_src = RTC_CLK_SRC;

    /* Update wake-up configuration and control registers */
    standby_mode_env->wakeup_cfg = WAKEUP_DELAY_16          |
    		                       WAKEUP_WAKEUP_PAD_RISING |
                                   WAKEUP_DIO0_RISING       |
                                   WAKEUP_DIO3_DISABLE      |
                                   WAKEUP_DIO2_DISABLE      |
                                   WAKEUP_DIO1_DISABLE      |
                                   WAKEUP_DIO0_ENABLE;

    /* Update wake-up control and clear previous wake-up events */
    standby_mode_env->wakeup_ctrl = PADS_RETENTION_DISABLE       |
                                    WAKEUP_DCDC_OVERLOAD_CLEAR   |
                                    WAKEUP_PAD_EVENT_CLEAR       |
                                    WAKEUP_RTC_ALARM_CLEAR       |
                                    WAKEUP_BB_TIMER_CLEAR        |
                                    WAKEUP_DIO3_EVENT_CLEAR      |
                                    WAKEUP_DIO2_EVENT_CLEAR      |
                                    WAKEUP_DIO1_EVENT_CLEAR      |
                                    WAKEUP_DIO0_EVENT_CLEAR;

    /* Set DMA channel used to save/restore RF registers
     * in each standby/wake-up cycle */
    standby_mode_env->DMA_channel_RF = DMA_CHAN_SLP_WK_RF_REGS_COPY;

    /* Initialize some system blocks for standby mode,
     * save RF registers and memory banks*/
    Sys_PowerModes_Standby_Init(standby_mode_env);

    /* BLE not in standby mode and ready for normal operations */
    BLE_Is_Awake_Flag_Set();

    Sys_DIO_Config(WAKEUP_DIO, DIO_MODE_GPIO_IN_0);

    Sys_DIO_IntConfig(0, (WAKEUP_DIO << DIO_INT_CFG_SRC_Pos) | DIO_EVENT_RISING_EDGE, 0, 0);
    NVIC_ClearPendingIRQ(DIO0_IRQn);

    /* Enable interrupts */
    NVIC_EnableIRQ(DIO0_IRQn);
}

/* ----------------------------------------------------------------------------
 * Function      : void WAKEUP_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Things to do once waking up from standby power mode
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void WAKEUP_IRQHandler(void)
{
    NVIC_ClearPendingIRQ(WAKEUP_IRQn);
    NVIC_DisableIRQ(WAKEUP_IRQn);

    if (VDDPA_enable)
    {
        ACS_VDDPA_CTRL->VDDPA_SW_CTRL_ALIAS = VDDPA_SW_HIZ_BITBAND;
        ACS_VDDPA_CTRL->ENABLE_ALIAS = VDDPA_ENABLE_BITBAND;
    }

    Sys_PowerModes_Standby_Wakeup(&standby_mode_env);

    /* If DIO wake-up event has occurred */
    if ((ACS_WAKEUP_STATE->WAKEUP_SRC_BYTE == WAKEUP_DUE_TO_DIO0_BYTE) ||
        (ACS_WAKEUP_STATE->WAKEUP_SRC_BYTE == WAKEUP_DUE_TO_WAKEUP_PAD_BYTE))
    {
        /* Force to wakeup the BB */
        BBIF->CTRL |= (BB_WAKEUP);
        for (int i = 0; i < 300; i++)
        {
            asm volatile ("nop");
        }
        BBIF->CTRL &= (~BB_WAKEUP);

        DIO_wakeup_flag = 1;
    }

    if (RTC_CLK_SRC == RTC_CLK_SRC_RC_OSC && ble_env.state == APPM_CONNECTED)
    {
        /* The number of times we wait for the device to go through a wake-
         * standby cycle */
        uint16_t num_wakeup;

        /* num_wakeup cycles should be determined by the connection interval
         * of peer device */
        if (ble_env.state == APPM_CONNECTED)
        {
            num_wakeup = ((MEASUREMENT_INTERVAL * SCALE_INTERVAL) /
                          (ble_env.actual_con_interval * (ble_env.actual_con_latency + 1)));
        }
        else
        {
            num_wakeup = ((MEASUREMENT_INTERVAL * 1600) / ADV_INT);
        }

        if (num_wakeup == 0)
        {
            num_wakeup = 1;
        }

        /* Update RCOSC period every num_wakeup cycles of standby */
        loop_cnt++;
        if ((loop_cnt % num_wakeup) == 0)
        {
            /* Set-up the Audiosink block for frequency measurement */
            Sys_Audiosink_ResetCounters();
            Sys_Audiosink_InputClock(0, AUDIOSINK_CLK_SRC_STANDBYCLK);
            Sys_Audiosink_Config(AUDIO_SINK_PERIODS_16, 0, 0);

            /* Enable interrupts */
            NVIC_ClearPendingIRQ(AUDIOSINK_PERIOD_IRQn);
            NVIC_EnableIRQ(AUDIOSINK_PERIOD_IRQn);

            /* Start period counter to start period measurement */
            AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = 1;

            /*Disable standby mode */
#if !RC_OSC_UPDATE
            if (update_disable_flag == 0)
#endif    /* if !RC_OSC_UPDATE */
            {
                allow_standby = 0;
            }
        }
    }

    /* Indicate the wakeup from standby event */
    wakeup_from_standby = true;

    NVIC_ClearPendingIRQ(WAKEUP_IRQn);
    NVIC_EnableIRQ(WAKEUP_IRQn);

    standby_cnt++;
    if ((ble_env.state == APPM_CONNECTED) &&
        (cs_env.tx_cccd_value & ATT_CCC_START_NTF) &&
        (cs_env.sentSuccess == 1) &&
        (standby_cnt % APP_CS_TX_VALUE_NOTF_STBY_CYCLE == 0))
    {
        cs_ntf_send_flag = true;
    }

}

/* ----------------------------------------------------------------------------
 * Function      : void AUDIOSINK_PERIOD_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Calculates the average period for every 16 measurements and
 *                 and then averages 1000 of those measurements before updating
 *                 the RC oscillator.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void AUDIOSINK_PERIOD_IRQHandler(void)
{
    /* Parameters for RC oscillator period measurements */
    static uint32_t num_measurement = INITIAL_MEASUREMENT_CYCLES;
    static uint32_t audio_sink_period_cnt = 0;
    static uint32_t sum_period_cnt = 0;
    static uint32_t sum_period_value = 0;
    float period_avg;

    uint8_t i;

    /* Record period count value and add it to the total sum*/
    audio_sink_period_cnt = Sys_Audiosink_PeriodCounter();
    sum_period_cnt++;
    sum_period_value += audio_sink_period_cnt;

#if !RC_OSC_UPDATE

    /* Allow the RC clock period to be set once */
    if (update_disable_flag == 0)
    {
#endif    /* if !RC_OSC_UPDATE */

    if (sum_period_cnt == num_measurement)
    {
        /* Calculate the average period for the number of audiosink cycles,
         * each taking num_measurement samples */
        period_avg = (sum_period_value /
                      (sum_period_cnt * SCALE_AVERAGE));

        /* Reset our total sum and count */
        sum_period_cnt = 0;
        sum_period_value = 0;

        /* If this is the first iteration:
         * - Make all historical period values the same as the current value
         * - Keep the current value as the overall average value
         */
        if (num_measurement == INITIAL_MEASUREMENT_CYCLES)
        {
            measure_buf[buf_cnt] = period_avg;
            buf_cnt = ((buf_cnt + 1) % MAX_BUF_CNT);
            for(i = 0; i < MAX_BUF_CNT; i++)
            {
                measure_buf[i] = period_avg;
            }
        }

        /* Else (second/third/...  iteration):
         * - Save the new period value into the historical record
         * - Calculate the overall average value (maximum and minimum values
         *   will be filtered out from the calculation)
         */
        else
        {
            measure_buf[buf_cnt] = period_avg;
            buf_cnt = ((buf_cnt + 1) % MAX_BUF_CNT);

            float max = measure_buf[0];
            float min = measure_buf[0];
            for (i = 1; i < MAX_BUF_CNT; i++ )
            {
                if (measure_buf[i] > max)
                {
                    max = measure_buf[i];
                }
                else if (measure_buf[i] < min)
                {
                    min = measure_buf[i];
                }
            }

            period_avg = 0;
            for(i = 0; i < MAX_BUF_CNT; i++ )
            {
                period_avg = (period_avg + measure_buf[i]);
            }

            period_avg = (period_avg - min - max);
            period_avg = (period_avg / (MAX_BUF_CNT - 2));
        }

        NVIC_DisableIRQ(AUDIOSINK_PERIOD_IRQn);

        num_measurement = DYNAMIC_MEAUREMNETS_CYCLES;

        /* Update RCCLK period value */
        RTCCLK_Period_Value_Set(period_avg);

        /* Allow the device to go into standby mode */
        allow_standby = 1;

#if !RC_OSC_UPDATE

        /* Increment the flag so only the first average is used */
        update_disable_flag++;
    }
#endif    /* if !RC_OSC_UPDATE */
    }

    AUDIOSINK->PERIOD_CNT = 0;

    AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = 1;
}

/*
 * Same as Starkey's DIO wakeup handler.
 * Set a flag to inform the main loop an external
 * wakeup request has been set
 */
void DIO0_IRQHandler(void)
{
    /* If BLE is not awake */
    if ((ACS_WAKEUP_STATE->WAKEUP_SRC_BYTE != WAKEUP_DUE_TO_BB_TIMER_BYTE) &&
        (!(BLE_Is_Awake())) &&
        wakeup_from_standby && (DIO_wakeup_flag == 0))
    {
        /* Force to wakeup the BB */
        BBIF->CTRL |= (BB_WAKEUP);
        for (int i = 0; i < 300; i++)
        {
            asm volatile ("nop");
        }
        BBIF->CTRL &= (~BB_WAKEUP);

        DIO_wakeup_flag = 1;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : int GATTC_CmpEvt(ke_msg_id_t const msg_id,
 *                                  struct gattc_cmp_evt
 *                                  const *param,
 *                                  ke_task_id_t const dest_id,
 *                                  ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handle received GATT controller complete event
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameters in format of
 *                                struct gattc_cmp_evt
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : return value - Indicate if the message was consumed;
 *                                compare with KE_MSG_CONSUMED
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int GATTC_CmpEvt(ke_msg_id_t const msg_id,
                 struct gattc_cmp_evt const *param,
                 ke_task_id_t const dest_id,
                 ke_task_id_t const src_id)
{
    if (param->operation == GATTC_NOTIFY)
     {
         if (param->status == GAP_ERR_NO_ERROR)
         {
             /* Set the send success flag */
             cs_env.sentSuccess = 1;
         }

         if (param->status == GAP_ERR_DISCONNECTED)
         {
             cs_env.sentSuccess = 1;
         }
     }

    return (KE_MSG_CONSUMED);
}
