/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * gpio_util.c
 * - GPIO pin utility functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2018/02/27 15:42:17 $
 * ------------------------------------------------------------------------- */

#include <rsl10.h>
#include "gpio_util.h"

/* ----------------------------------------------------------------------------
 * Function      : void Buzz_GPIOs_Init(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure a number of GPIO pins for testing purposes
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : GPIOs 1, 3, 5, and 7 are used for testing purposes
 * ------------------------------------------------------------------------- */
void Buzz_GPIOs_Init(void)
{
    uint32_t i;

    /* Set mode for DIO pins uses for testing purposes */
    Sys_DIO_Config(1, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(3, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(5, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(7, DIO_MODE_GPIO_OUT_0);

    /* Send buzzes to all 4 pins to test them */

    /* Clear DIO pins uses for testing purposes */
    DIO->DATA &= ~((uint32_t)((0x1U << 1) |
                              (0x1U << 3) |
                              (0x1U << 5) |
                              (0x1U << 7)));

    i = 0;
    while (true)
    {
        Sys_Watchdog_Refresh();

        Sys_GPIO_Toggle(1);
        Sys_GPIO_Toggle(3);
        Sys_GPIO_Toggle(5);
        Sys_GPIO_Toggle(7);

        i++;
        if (i == 10)
        {
            break;
        }

        /* ~toggle_duration usec delay required:
         * Assumes system clock of 8 MHz, then the number of cycles is
         * calculated as follows:
         *    toggle_duration [usec] * 8 * 10^6 [cycle/sec]
         *        = toggle_duration * 8 [cycle] */
        Sys_Delay_ProgramROM((uint32_t)(100000 * 8));
    }

    Sys_Watchdog_Refresh();

    /* Clear DIO pins uses for testing purposes */
    DIO->DATA &= ~((uint32_t)((0x1U << 1) |
                              (0x1U << 3) |
                              (0x1U << 5) |
                              (0x1U << 7)));
}

/* ----------------------------------------------------------------------------
 * Function      : void Buzz_GPIO(uint32_t gpio_pin, uint32_t toggle_num,
 *                                uint32_t toggle_duration_us,
 *                                uint32_t sys_clk_MHz)
 * ----------------------------------------------------------------------------
 * Description   : Send a buzz to a specific GPIO pin for testing purposes
 * Inputs        : - gpio_pin           - pin # [1, 2, ...]
 *                 - toggle_num         - # of toggles
 *                 - toggle_duration_us - duration of each toggle [us]
 *                 - sys_clk_MHz        - system clock frequency [MHz]
 * Outputs       : None
 * Assumptions   : System clock of 8 MHz
 * ------------------------------------------------------------------------- */
void Buzz_GPIO(uint32_t gpio_pin, uint32_t toggle_num,
               uint32_t toggle_duration_us, uint32_t sys_clk_MHz)
{
    uint32_t i;

    Sys_GPIO_Set_Low(gpio_pin);

    i = 0;
    while (true)
    {
        Sys_Watchdog_Refresh();

        Sys_GPIO_Toggle(gpio_pin);

        i++;
        if (i == toggle_num)
        {
            break;
        }

        /* ~toggle_duration usec delay required:
         * Assumes system clock of sys_clk_MHz MHz, then the number of
         * cycles is calculated as follows:
         *    toggle_duration [usec] * sys_clk_MHz * 10^6 [cycle/sec]
         *        = toggle_duration * sys_clk_MHz [cycle] */
        Sys_Delay_ProgramROM((uint32_t)(toggle_duration_us * sys_clk_MHz));
    }

    Sys_Watchdog_Refresh();

    Sys_GPIO_Set_Low(gpio_pin);
}
