/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_scan.h
 * - BLE Scanner header
 * ----------------------------------------------------------------------------
 * $Revision: 1.7 $
 * $Date: 2018/09/11 22:39:16 $
 * ------------------------------------------------------------------------- */

#ifndef APP_SCAN_H
#define APP_SCAN_H

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <app.h>

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/
#define UART_BAUDRATE                   115200
#define UART_TX_BUFFER_SIZE             100

/* Keyboard UART CLI input codes */
#define KEY_ARROW_UP                    65
#define KEY_ARROW_DOWN                  66
#define KEY_ENTER                       13

/* Maximum number of devices in scan list */
#define ADV_REPORT_LIST_MAX             30

/* Set scan interval to 62.5ms and scan window to 50% of the interval */
#define SCAN_INTERVAL                   100
#define SCAN_WINDOW                     50

/* Timeout to update the UART CLI */
#define SCAN_UART_CLI_TIMEOUT_MS        300
#define START_CONNECTION_CMD_TIMEOUT_MS 3000

/* ---------------------------------------------------------------------------
* Function prototype definitions
* --------------------------------------------------------------------------*/
void SCAN_APP_Initialize(void);

void SCAN_Start(void);

void SCAN_MsgHandler(ke_msg_id_t const msg_id, void const *param,
                     ke_task_id_t const dest_id, ke_task_id_t const src_id);

void SCAN_AdvReportInd_Handler(const struct gapm_adv_report_ind *p);

void SCAN_AdvReport_ExtractName(const uint8_t* data, uint8_t size, uint8_t *result);

void SCAN_ScreenUpdateTimeout(void);

void SCAN_UART_EventHandler(uint32_t event);

void SCAN_UART_SendString(const char *str);

void SCAN_LED_Timeout_Handler(ke_msg_id_t const msg_id, void const *param,
                              ke_task_id_t const dest_id, ke_task_id_t const src_id);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* APP_SCAN_H */
