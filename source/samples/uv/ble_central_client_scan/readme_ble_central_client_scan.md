Scanning with Central Device Sample Code
========================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project scans for advertisements and prints a list of scanned 
devices over UART. The user can use a UART terminal application (like TeraTerm)
with a baud rate of 115200 to see scanned devices and connect up to four 
peripheral peer devices. The keyboard arrow keys can be used to select a 
device, while the <Enter> key starts a connection. The <c> key is used to clear
the list of scanned devices. This application makes use of the RSL10 UART 
CMSIS-Driver to manage the interface communication.

This sample project is based on the `ble_central_client_bond` application. It is 
equipped with a Bluetooth Low Energy abstraction layer that provides a higher 
level application programming interface (API) to abstract the Bluetooth GAP and
GATT layers. The abstraction layer has been designed to improve flexibility and
simplicity by providing the following features:

    - An event subscription mechanism that allows the application to subscribe 
      and receive callback notifications for any Kernel or Bluetooth event. 
      This improves the encapsulation and integration of different modules, as 
      they can be implemented while isolated in their own files.
    - Generic definition of custom services with callback notification support
    - Security handling and bond list implementation in RSL10 flash
    - Code structure and API naming aligned with RivieraWaves documentation, so 
      you can map the code into the documentation more easily

The sample code has been refactored to keep the generic abstraction layer code 
and application-specific code in separate files. This increases flexibility, 
maintainability and reusability of components between distinct applications.

The device address type and source are set in `app.h`. If `APP_DEVICE_PARAM_SRC`
is set to `FLASH_PROVIDED_or_DFLT` and the address type is set to 
`GAPM_CFG_ADDR_PUBLIC`, the stack loads the public device address stored in NVR3
flash. Otherwise, the address provided in the application is used.

The message subscription mechanism allows the application and services to 
subscribe and receive callback notifications based on the Kernel message ID 
or task ID. This allows each module of the application to be independently 
implemented in its own files. The subscription is performed using the 
`MsgHandler_Add()` function. The application subscribes to receive GAPM and 
GAPC events (see `app.c`). The services subscribe to receive Kernel events in 
their initialization function (see `BASC_Initialize()` in `ble_basc.c`, for an 
example). The application event handlers are implemented in `app_msg_handler.c`
and in `app_scan.c`.

The basic sequence of operations and event messages exchanged between the 
application and the Bluetooth stack is presented below: 

  APP <--> Bluetooth Low Energy Stack
  
  Startup
  
      --->  GAPM_ResetCmd() - app.c
      <---  GAPM_CMP_EVT / GAPM_RESET - app_scan
      --->  GAPM_SetDevConfigCmd() - app_scan.c
      <---  GAPM_CMP_EVT / GAPM_SET_DEV_CONFIG - app_scan
      --->  GAPM_StartScanCmd() - app_scan.c
      <---  GAPM_ADV_REPORT_IND - app_scan.c
            
  Connection request (When <Enter> key is pressed - `app_scan.c`)
  
      --->  GAPM_START_CONNECTION_CMD - app_scan.c
      <---  GAPC_CONNECTION_REQ_IND - app_scan.c
      --->  GAPC_ConnectionCfm() - app_scan.c
      
  Parameters update request
  
      <---  GAPC_PARAM_UPDATE_REQ_IND - app_scan.c
      --->  GAPC_ParamUpdateCfm() - app_scan.c

**This sample project is structured as follows:**

The source code exists in a `source` folder. Application-related header files 
are in the `include` folder. The `main()` function is implemented in the `app.c`
file, which is located in the parent directory.

The Bluetooth Low Energy abstraction layer contains support functions for the 
GAP and GATT layers and has a message handling mechanism that allows the 
application to subscribe to Kernel events. It also contains standard profile-
related files.

Application-specific files
--------------------------
`app.c`                  - Main application file 

source
------
    app_config.c      - Device configuration and definition of application-
                        specific Bluetooth Low Energy messages.
    app_scan.c        - Application-specific message handlers
    app_trace.c       - Debugging (printf) utility functions source (disabled 
                        by default)

include
-------
    app.h             - Main application header file
    app_scan.h        - Application-specific message handlers header
    app_trace.h       - Debugging (printf) utility functions header

    
Bluetooth Low Energy abstraction files (CMSIS-Pack component is generic for any application)
---------------------

    ble_gap.c         - GAP layer support functions and message handlers          
    ble_gatt.c        - GATT layer support functions and message handlers               
    msg_handler.c     - Message handling subscribing mechanism implementation


    ble_gap.h         - GAP layer abstraction header
    ble_gatt.h        - GATT layer abstraction header
    msg_handler.h     - Message handling subscribing mechanism header       


This application can be executed on any Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
Getting Started Guide for your IDE for more information.

Select the project configuration according to the required optimization level. 
Use **Debug** configuration for optimization level **None**, or **Release**
configuration for optimization level **More** or **O2**.

Verification
------------
To verify if this application is functioning correctly, perform the following:

1. Connect the RSL10 Evaluation and Development Board to a computer running 
   windows.
2. Load this application into your RSL10 Evaluation and Development Board.
3. Open a serial terminal application (such as TeraTerm). Connect to the 
   serial port at a baud rate of 115200. To identify the serial port 
   number, check the device manager options and look for 
   **JLINK CDC UART PORT (COMxx)** under the **Ports (COM & LPT)** menu.
4. The serial terminal continuously prints a list of scanned devices.
   Make sure your terminal is configured to perform a line-break when the 
   **LF** (Line Feed) character is received (in TeraTerm, navigate to the menu
   **Setup** > **Terminal** > **New-line** > **Receive** > **LF**).
5. Make sure you have an available device advertising (use another RSL10 
   Evaluation and Development board with the `ble_peripheral_server_bond`
   application).
6. In the UART terminal, use the keyboard up and down arrow keys to 
   select a device to connect and press <Enter> to start a connection .
7. Optionally, press <c> anytime to clear the list of scanned devices.

You can also observe the behavior of the LED on the RSL10 Evaluation 
and Development Board (DIO6). The LED behavior is controlled by the 
`SCAN_LED_Timeout_Handler` function (in `app_scan.c`) and can be one of the 
following:

    - If the device has not started a connection procedure, the LED is off.
    - If the device started a connection procedure but it is not connected to 
      any peer, the LED blinks every 200 ms.
    - If the device is connected to fewer than `APP_NB_PEERS` peers, the LED
      blinks every 2 seconds according to the number of connected peers 
      (i.e., blinks once if one peer is connected, twice if two peers are 
       connected, etc.).
    - If the device is connected to `APP_NB_PEERS` peers, the LED is steady on.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which pauses at the
    start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
