/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Write a default system initialization function to the MANU_INFO_INIT
 *   area of non-volatile record 3 (NVR3).
 * ----------------------------------------------------------------------------
 * $Revision: 1.52 $
 * $Date: 2019/12/04 16:13:08 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>

const uint8_t default_flash_empty[DEFAULT_ERASED_MEM_WORD_LEN * 4] =
    DEFAULT_ERASED_MEM;

uint32_t rand_seed;


/* ----------------------------------------------------------------------------
 * Function      : void TIMER0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : timer0 interrupt configured in free run mode
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER0_IRQHandler(void)
{
    Sys_GPIO_Toggle(LED_DIO);
}

/* ----------------------------------------------------------------------------
 * Function      : void app_display_result(uint32_t status)
 * ----------------------------------------------------------------------------
 * Description   : Configure a Timer to display the result on a LED
 * Inputs        : result - Error code
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void app_display_result(LEDStatus status)
{
    Sys_DIO_Config(LED_DIO, DIO_MODE_GPIO_OUT_0);
    /* Configure timer[0] in free run mode.
     * - Force multi-count to 8 in order to show that field is not active */
    Sys_Timer_Set_Control(0, (TIMER_MULTI_COUNT_8 |
                              TIMER_FREE_RUN      |
                              TIMER_SLOWCLK_DIV2  |
                              TIMER_PRESCALE_32)  | status);

    TIMER_CTRL[0].TIMER_START_ALIAS = TIMER_START_BITBAND;
    NVIC_EnableIRQ(TIMER0_IRQn);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Write NVR3 with the System_Start() function (save and
 *                 restore the device's address and lock information)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    CalSetting_Type cal_values;
    uint32_t cal_result;

    FlashStatus flash_result;
    uint32_t *tempPtr = NULL;
    uint32_t temp    = 0;

    uint16_t initLength_loc;
    uint16_t initVersion_loc;
    uint16_t crcValue_loc;

    unsigned int code_length;
    struct dev_info info = {0};
    PRINTF("APPLICATION STARTED\n");

#ifndef NOT_WAIT
    /* Test DIO5 to pause the program to avoid unwanted re-execution of
     * the application */
    DIO->CFG[BUTTON_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                           DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[BUTTON_DIO] == 1)
    {
        Sys_Watchdog_Refresh();
    }
    PRINTF("BUTTON PRESSED\n");
#endif
    /* Generate a random seed for the random number generator */
    Rand_GenSeed(&rand_seed);

    /* Save device information from NVR3 to be restored later */
    Read_InfoFlash(&info);
    PRINTF("DEVICE INFORMATION READ FROM NVR3\n");

    /* Pre-calculate the CRC needed */
    Sys_CRC_Set_Config(CRC_CCITT | CRC_LITTLE_ENDIAN | CRC_BIT_ORDER_STANDARD |
                       CRC_FINAL_REVERSE_STANDARD | CRC_FINAL_XOR_STANDARD);
    tempPtr = (uint32_t *)&initLength;
    tempPtr++;
    for (CRC->VALUE = CRC_CCITT_INIT_VALUE;
         (uint32_t)tempPtr < (uint32_t)&crcValue;
         CRC->ADD_32 = *(tempPtr++))
    {
    }

    crcValue_loc        = (unsigned short)CRC->FINAL;
    initLength_loc      = (unsigned short)((uint32_t)&crcValue -
                                           (((uint32_t)&System_Start) &
                                            0xFFFFFFFE)) + 2;
    initVersion_loc     = (unsigned short)MANU_INFO_INIT_FW_VER;

    /* Set SLOWCLK prescaler, the FLASH_DELAY_CTRL register, and
     * SystemCoreClock accordingly. */
    CLK_DIV_CFG0->SLOWCLK_PRESCALE_BYTE = 2;
    FLASH->DELAY_CTRL = (FLASH_DELAY_FOR_SYSCLK_3MHZ | DEFAULT_READ_MARGIN);
    SystemCoreClock   = 3000000;

    Sys_ReadNVR4(MANU_INFO_OSC_RC, 1, (unsigned int *)&temp);

    if ((temp != 0xFFFFFFFF) && ((temp >> 16) == 3000))
    {
        /* Store the loaded trim setting and set FTRIM_FLAG in ACS_RCOSC_CTRL
         * byte 1, then explicitly set the system clock source to RCCLK */
        ACS_RCOSC_CTRL->RESERVED0[0]     = (uint8_t)(temp |
                                                     (RC_OSC_CALIBRATED >> 8));
        ACS_RCOSC_CTRL->CLOCK_MULT_ALIAS = RC_START_OSC_3MHZ_BITBAND;
    }
    else
    {
        /* NVR4 does not contain calibration value for 3 MHz, use nominal RC
         * trimming value instead (best guess on non-production part) */
        ACS->RCOSC_CTRL = (RC_OSC_NOM | RC_OSC_RANGE_NOM | RC_START_OSC_NOM |
                           RC_OSC_UNCALIBRATED | RC_OSC_DISABLE |
                           RC_START_OSC_3MHZ);
    }

    /* Switch to 3 MHz RC oscillator */
    CLK->SYS_CFG = (EXTCLK_PRESCALE_1 | JTCK_PRESCALE_1 | SYSCLK_CLKSRC_RCCLK);

    cal_result   = SupplementalCalibrate(&cal_values);
    if (cal_result != ERRNO_NO_ERROR)
    {
        Test_Error(cal_result);
    }

    /* Load default address if cleared (all bits set to 0) or empty (all bits
     * set to 1) */
    if (*(int64_t *)info.deviceAddr == 0x0 ||
        *(int64_t *)info.deviceAddr == -1)
    {
        /* Address was cleared or erased. Read device address from NVR4 flash */
        Sys_ReadNVR4(MANU_INFO_BLUETOOTH_ADDR, INFO_ADDR_WORD_LEN,
                     info.deviceAddr);
    }

    /* Load random IRK/CSRK if empty */
    if (memcmp((void *)info.irk, (void *)default_flash_empty,
               INFO_IRK_WORD_LEN * 4) == 0)
    {
        Rand_FillBuffer(info.irk, INFO_IRK_WORD_LEN);
    }
    if (memcmp((void *)info.csrk, (void *)default_flash_empty,
               INFO_CSRK_WORD_LEN * 4) == 0)
    {
        Rand_FillBuffer(info.csrk, INFO_CSRK_WORD_LEN);
    }

    if (memcmp((void *)info.priv_key, (void *)default_flash_empty,
               INFO_PRIVATE_KEY_WORD_LEN) == 0 ||
        memcmp((void *)info.pub_key_x, (void *)default_flash_empty,
               INFO_PUBLIC_X_WORD_LEN) == 0 ||
        memcmp((void *)info.pub_key_y, (void *)default_flash_empty,
               INFO_PUBLIC_Y_WORD_LEN) == 0)
    {
        BLE_KeyRead((uint8_t *)info.priv_key, (uint8_t *)info.pub_key_x,
                    (uint8_t *)info.pub_key_y, rand_seed);
    }

    /* Write enable NVR3 */
    FLASH->NVR_CTRL = (NVR1_WRITE_DISABLE | NVR2_WRITE_DISABLE |
                       NVR3_WRITE_ENABLE);
    FLASH->NVR_WRITE_UNLOCK = FLASH_NVR_KEY;

    /* Erase NVR3 */
    PRINTF("ERASING NVR3\n");
    flash_result = (FlashStatus) Flash_EraseSector(DEVICE_INFO_BASE);
    if (flash_result != FLASH_ERR_NONE)
    {
        Test_Error(flash_result);
    }

    /* Restore saved NVR3 information*/
    flash_result = Restore_InfoFlash(&info);
    if (flash_result != FLASH_ERR_NONE)
    {
        Test_Error(flash_result);
    }

    code_length = ((unsigned int)&crcValue -
                   (unsigned int)&initLength + 4) >> 2;

    /* Copy the main flash content into PRAM */
    tempPtr = (uint32_t *)PRAM0_BASE;
#if defined ( __ICCARM__ )
    memcpy(tempPtr, (void const*)&initLength, code_length * sizeof(uint32_t));
#else
    memcpy(tempPtr, &initLength, code_length * sizeof(uint32_t));
#endif

    *tempPtr = initLength_loc | (initVersion_loc << 16);
    *(tempPtr + (code_length - 1)) = crcValue_loc;

    if (code_length % 2 != 0)
    {
        code_length++;
    }

    /* Write the initialization function to MANU_INFO_INIT */
    PRINTF("WRITING MANU_INFO_INIT FUNCTION TO NVR3\n");
    flash_result = (FlashStatus) Flash_WriteBuffer(MANU_INFO_INIT, code_length,
                                     (unsigned int *)tempPtr);
    PRINTF("MANU_INFO_INIT WRITTEN SUCCESSFULLY\n");
    if (flash_result != FLASH_ERR_NONE)
    {
        Test_Error(flash_result);
    }

    /* Write structure to flash memory */
    flash_result = (FlashStatus) Flash_WriteBuffer(CALSETTING_BASE,
                                     sizeof(CalSetting_Type) / 4,
                                     ((unsigned int *)&cal_values));

    /* Make sure that no error occurred in writing to flash */
    if (flash_result != FLASH_ERR_NONE)
    {
        Test_Error(flash_result);
    }

    /* If we reached here, then there were no flash failures
     * Display the output result on a LED, requires LED_STATUS_ENABLED to be set to 1 */
    app_display_result(LED_SUCCESS);

    /* No errors encountered. Enter a spin loop refreshing the watchdog timer,
     * waiting for a user to stop the application.
     */
    while (1)
    {
        Sys_Watchdog_Refresh();
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Test_Error(uint32_t result)
 * ----------------------------------------------------------------------------
 * Description   : Spin loop function that gets called if calibration function
 *                 or flash write or erase functions fail.
 * Inputs        : result - Error code indicating what triggered the code to
 *                          divert to this function.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Test_Error(uint32_t result)
{
    app_display_result(LED_FAIL);
    PRINTF("TEST_ERROR\n");

    /* An error has occured. Enter a spin loop, refreshing the watchdog timer
     * and holding it here to allow a user to identify the cause of the error.
     */
    while (1)
    {
        Sys_Watchdog_Refresh();
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Read_InfoFlash
 *                             (struct dev_info *info )
 * ----------------------------------------------------------------------------
 * Description   : Read device information from flash to be restored
 * Inputs        : info - Device information object where saved parameters are
 *                 to be stored.
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Read_InfoFlash(struct dev_info *info)
{
    /* Read bluetooth address from NVR3 flash */
    memcpy((void *)info->deviceAddr, (void *)DEVICE_INFO_BLUETOOTH_ADDR,
           INFO_LOCK_WORD_LEN * 4);

    /* Read lock information from NVR3 flash */
    memcpy((void *)info->deviceLockInfo, (void *)LOCK_INFO_BASE,
           INFO_LOCK_WORD_LEN * 4);

    /* Read IRK from NVR3 flash */
    memcpy((void *)info->irk, (void *)DEVICE_INFO_BLUETOOTH_IRK,
           INFO_IRK_WORD_LEN * 4);

    /* Read CSRK from NVR3 flash */
    memcpy((void *)info->csrk, (void *)DEVICE_INFO_BLUETOOTH_CSRK,
           INFO_CSRK_WORD_LEN * 4);

    /* Read private key from NVR3 flash */
    memcpy((void *)info->priv_key, (void *)DEVICE_INFO_ECDH_PRIVATE,
           INFO_PRIVATE_KEY_WORD_LEN * 4);

    /* Read private key from NVR3 flash */
    memcpy((void *)info->pub_key_x, (void *)DEVICE_INFO_ECDH_PUBLIC_X,
           INFO_PUBLIC_X_WORD_LEN * 4);

    /* Read private key from NVR3 flash */
    memcpy((void *)info->pub_key_y, (void *)DEVICE_INFO_ECDH_PUBLIC_Y,
           INFO_PUBLIC_Y_WORD_LEN * 4);
}

/* ----------------------------------------------------------------------------
 * Function      : Restore_InfoFlash restore_info_flash
 *                             (struct dev_info_restore *info )
 * ----------------------------------------------------------------------------
 * Description   : Restore saved device information to flash
 * Inputs        : info - Device information object where saved parameters are
 *                 stored.
 * Outputs       : status Flash status error code
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
FlashStatus Restore_InfoFlash(struct dev_info *info)
{
    FlashStatus status;

    /* Restore address */
    status = (FlashStatus) Flash_WriteBuffer(DEVICE_INFO_BLUETOOTH_ADDR,
                               INFO_ADDR_WORD_LEN, info->deviceAddr);
    if (status != FLASH_ERR_NONE)
    {
        return (status);
    }

    /* Restore device lock information */
    status = (FlashStatus) Flash_WriteBuffer(LOCK_INFO_BASE,
                               INFO_LOCK_WORD_LEN, info->deviceLockInfo);
    if (status != FLASH_ERR_NONE)
    {
        return (status);
    }

    /* Restore IRK */
    status = (FlashStatus) Flash_WriteBuffer(DEVICE_INFO_BLUETOOTH_IRK,
                               INFO_IRK_WORD_LEN, info->irk);
    if (status != FLASH_ERR_NONE)
    {
        return (status);
    }

    /* Restore CSRK */
    status = (FlashStatus) Flash_WriteBuffer(DEVICE_INFO_BLUETOOTH_CSRK,
                               INFO_CSRK_WORD_LEN, info->csrk);
    if (status != FLASH_ERR_NONE)
    {
        return (status);
    }

    /* Restore Private Key */
    status = (FlashStatus) Flash_WriteBuffer(DEVICE_INFO_ECDH_PRIVATE,
                               INFO_PRIVATE_KEY_WORD_LEN, info->priv_key);
    if (status != FLASH_ERR_NONE)
    {
        return (status);
    }

    /* Restore Public Key X */
    status = (FlashStatus) Flash_WriteBuffer(DEVICE_INFO_ECDH_PUBLIC_X,
                               INFO_PUBLIC_X_WORD_LEN, info->pub_key_x);
    if (status != FLASH_ERR_NONE)
    {
        return (status);
    }

    /* Restore Public Key Y */
    status = (FlashStatus) Flash_WriteBuffer(DEVICE_INFO_ECDH_PUBLIC_Y,
                               INFO_PUBLIC_Y_WORD_LEN, info->pub_key_y);
    if (status != FLASH_ERR_NONE)
    {
        return (status);
    }

    return (FLASH_ERR_NONE);
}

/* ----------------------------------------------------------------------------
 * Function      : void Rand_GenSeed(void)
 * ----------------------------------------------------------------------------
 * Description   : Generate a random seed. VBAT is connected to ADC input
 *                 channel to generate random data.
 * Inputs        : Seed - Random seed to be generated
 * Outputs       : None
 * Assumptions   : None
 * -------------------------------------------------------------------------*/
void Rand_GenSeed(uint32_t *seed)
{
    /* Set the ADC configuration */
    Sys_ADC_Set_Config(ADC_VBAT_DIV2_NORMAL | ADC_NORMAL | ADC_PRESCALE_200);

    /* Configure ADC_CHANNEL input selection to VBAT/2 */
    Sys_ADC_InputSelectConfig(ADC_RAND_CHANNEL, ADC_POS_INPUT_VBAT_DIV2 |
                              ADC_NEG_INPUT_GND);

    /* Set the battery monitor interrupt configuration */
    Sys_ADC_Set_BATMONIntConfig(INT_EBL_ADC |
                                ADC_RAND_CHANNEL <<
                                ADC_BATMON_INT_ENABLE_ADC_INT_CH_NUM_Pos |
                                INT_DIS_BATMON_ALARM);

    /* Clear ADC Ready Status Bit */
    ADC_BATMON_STATUS->ADC_READY_CLEAR_ALIAS = ADC_READY_CLEAR_BITBAND;

    /* Read 32 times LSB bit of ADC sample to generate a random word */
    for (uint8_t i = 0; i < 32; i++)
    {
        /* Wait for ADC to complete sampling of ADC channels */
        while (ADC_BATMON_STATUS->ADC_READY_STAT_ALIAS == ADC_READY_FALSE_BITBAND)
        {
            Sys_Watchdog_Refresh();
        }

        /* Clear ADC Ready Status Bit */
        ADC_BATMON_STATUS->ADC_READY_CLEAR_ALIAS = ADC_READY_CLEAR_BITBAND;
        uint32_t adc_sample = ADC->DATA_TRIM_CH[ADC_RAND_CHANNEL];
        *seed = *seed | (adc_sample & 0x01);
        *seed <<= 1;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Rand_FillBuffer(unsigned int* mem,
 *                                      unsigned int word_len)
 * ----------------------------------------------------------------------------
 * Description   : Fill memory with random generated bytes
 * Inputs        : mem - Address of memory location.
 *               : word_len - Length of memory in words.
 * Outputs       : None
 * Assumptions   : RAND_MAX defined as 0x7fffffff .
 * -------------------------------------------------------------------------*/
void Rand_FillBuffer(unsigned int *mem, unsigned int word_len)
{
    unsigned int i;
    static uint8_t random_gen_is_initialized = 0;

    /* Initialize random number generator once if uninitialized */
    if (random_gen_is_initialized == 0)
    {
        srand(rand_seed);
        random_gen_is_initialized = 1;
    }

    /* Fill with random data words */
    for (i = 0; i < word_len; i++)
    {
        mem[i] = rand();
    }
}
