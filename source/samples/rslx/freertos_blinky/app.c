/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - RTOS Blinky demo for RSL10.
 *      Uses DIO6 as output to control LED on RSL10 EVB.
 *      Uses DIO5 as input to control Button on RSL10 EVB.
 *      Button press enables and disables LED Blinking.
 * ------------------------------------------------------------------------- */

#include <app.h>
#include <printf.h>

/* variable enabling / disabling led blinking */
volatile uint8_t led_toggle_status;

/* attribute structure for Binky thread */
const osThreadAttr_t thread_bli_attr =
{
    .name = "thBli",
    .priority = osPriorityLow,
    .stack_size = 256
};

/* Function prototype definitions */
static void Initialize(void);

/* ----------------------------------------------------------------------------
 * Function      : void DIO0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Toggle the toggle status global flag.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO0_IRQHandler(void)
{
    static uint8_t ignore_next_dio_int = 0;
    if (ignore_next_dio_int == 1)
    {
        ignore_next_dio_int = 0;
    }
    else if (DIO_DATA->ALIAS[BUTTON_DIO] == 0)
    {
        /* Button is pressed: Ignore next interrupt.
         * This is required to deal with the debounce circuit limitations. */
        ignore_next_dio_int = 1;

        /* Invert toggle status */
        if (led_toggle_status == 1)
        {
            led_toggle_status = 0;
            PRINTF("LED BLINK DISABLED\n");
        }
        else
        {
            led_toggle_status = 1;
            PRINTF("LED BLINK ENABLED\n");
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, configuring
 *                 the required DIOs and DIO interrupt, and enabling interrupts.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Ensure all priority bits are assigned as preemption priority bits.
     * Should not be changed! */
    NVIC_SetPriorityGrouping(0);
    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Setup DIO5 as a GPIO input with interrupts on transitions, DIO6 as a
     * GPIO output. Use the integrated debounce circuit to ensure that only a
     * single interrupt event occurs for each push of the pushbutton.
     * The debounce circuit always has to be used in combination with the
     * transition mode to deal with the debounce circuit limitations.
     * A debounce filter time of 50 ms is used. */
    Sys_DIO_Config(LED_DIO, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(BUTTON_DIO, DIO_MODE_GPIO_IN_0 | DIO_WEAK_PULL_UP |
                   DIO_LPF_DISABLE);
    Sys_DIO_IntConfig(0,
                      DIO_EVENT_TRANSITION | DIO_SRC(BUTTON_DIO) |
                      DIO_DEBOUNCE_ENABLE,
                      DIO_DEBOUNCE_SLOWCLK_DIV1024, 49);
    NVIC_EnableIRQ(DIO0_IRQn);

    /* Unmask all interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system, then toggle DIO6 as controlled by
 *                 DIO5 (press to toggle input/output).
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /*Initialize global variables */
    led_toggle_status = true;

    /* Initialize the system */
    Initialize();
    PRINTF("DEVICE INITIALIZED\n");
	
    SystemCoreClockUpdate();

    /* Initialize RTOS */
    osKernelInitialize();

    /* Create main thread */
    osThreadNew(vThread_Blinky, NULL, &thread_bli_attr);

    /* Start RTOS scheduler */
    if (osKernelGetState() == osKernelReady)
    {
        osKernelStart();
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void vThread_Blinky(void *argument)
 * ----------------------------------------------------------------------------
 * Description   : Function for thread blinking.
 * Inputs        : argument - pointer to thread arguments
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__NO_RETURN void vThread_Blinky (void *argument)
{
    while (true)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* Toggle GPIO 6 (if toggling is enabled) then wait 0.5 seconds */
        if (led_toggle_status == 1)
        {
            Sys_GPIO_Toggle(LED_DIO);
            PRINTF("LED %s\n", (DIO->CFG[LED_DIO] & 0x1 ? "ON" : "OFF"));
        }
        else
        {
            Sys_GPIO_Set_Low(LED_DIO);
        }
        /* Wait for defined time */
        osDelay(BLINKING_PERIOD_MS);
    }
}


