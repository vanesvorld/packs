FreeRTOS Blinky - Simple GPIO I/O Sample Code
=============================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This description is for FreeRTOS. If you want to know about the Blinky � Simple 
GPIO I/O sample code, refer to the `readme_blinky.txt` in the `blinky` 
sample application.

This sample project demonstrates a simple application using CMSIS-FreeRTOS 
provided by Arm. CMSIS-FreeRTOS is a common API for real-time operating 
systems (RTOS2). To use this sample application, `ARM.CMSIS` and 
`ARM.CMSIS-FreeRTOS` packs must be installed in your IDE. In CMSIS Pack 
Manager, on the right panel you can see the Packs and Examples view. In the 
Packs view, you can see CMSIS packs. Find `ARM.CMSIS` and 
`ARM.CMSIS-FreeRTOS` and click on the Install button. When you copy 
the `freertos_blinky` on the Examples view, the C/C++ perspective opens 
and displays your newly copied project. On the right side, the 
`freertos_blinky.rteconfig` file displays software components. You need 
to verify that FreeRTOS in CMSIS/RTOS2 (API) and the components of RTOS2 are 
selected.

This sample application calls `osKernelInitialize()` to initialize RTOS2 
and creates the main `vThread_Blinky` thread to run FreeRTOS.
The operation of the `LED blinky` runs in the main `vThread_Blinky`.

Hardware Requirements
---------------------
This application can be executed on an RSL10 Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
Getting Started Guide for your IDE for more information.

Verification
------------
To verify if this application is functioning correctly, look for DIO6 
continuously flashing on the Evaluation and Development Board.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***********************************************************
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
