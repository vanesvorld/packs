/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 *  - Simple example on how to use RSL10 dma driver
 * ------------------------------------------------------------------------- */
#include <app.h>
#include <printf.h>

/* Global variables */
DRIVER_DMA_t *dma;
DRIVER_GPIO_t *gpio;
char src_buffer[] = "RSL10 DMA TEST!";
char dst_buffer[sizeof(src_buffer)];

volatile uint32_t transferCompletedFlag = 0;

/* ----------------------------------------------------------------------------
 * Function      : void DMA_EventCallback(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a callback registered by the function
 *                 Initialize. Based on event argument different actions are
 *                 executed.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA_EventCallback(uint32_t event)
{
    /* Check if dma channel0 event has been triggered */
    if (event & DMA_DMA4_EVENT)
    {
        /* Check if transfer has been completed */
        if (dma->GetStatus(DMA_CH_4).completed == 1)
        {
            /* Indicate transfer completed */
            transferCompletedFlag = 1;
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Prepare the 48 MHz crystal
     * Start and configure VDDRF */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_1_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to 48 MHz oscillator clock */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Initialize gpio structure */
    gpio = &Driver_GPIO;

    /* Initialize gpio driver */
    gpio->Initialize(NULL);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    /* Initialize dma structure */
    dma = &Driver_DMA;

    /* Configure dma callback function */
    dma->Initialize(DMA_EventCallback);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system and the dma driver. Configures
 *                 dma driver and enables one dma channel.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* Initialize the system */
    Initialize();
    PRINTF("DEVICE INITIALIZED\n");

    /* Prepare src / dst configuration */
    DMA_ADDR_CFG_t addrCfg = {
        .src_addr     = src_buffer,
        .dst_addr     = dst_buffer,
        .counter_len  = sizeof(src_buffer) / 4,
        .transfer_len = sizeof(src_buffer) / 4
    };

    /* Set the src / dst configuration */
    dma->ConfigureAddr(DMA_CH_4, &addrCfg);

    /* Start the data transfer */
    dma->Start(DMA_CH_4);

    /* Wait for dma transfer to be completed */
    while (!transferCompletedFlag)
    {
        /* Refresh Watchdog */
        Sys_Watchdog_Refresh();
    }

    /* Check if the data was transferred correctly */
    uint32_t dataCorrect = 1;
    for (int i = 0; i < sizeof(src_buffer); i++)
    {
        /* Check if each byte is correct */
        if (dst_buffer[i] != src_buffer[i])
        {
            /* Indicate error */
            dataCorrect = 0;
            PRINTF("ERROR : NOT CORRECT DATA\n");
            break;
        }
    }

    /* If data was sent correctly, light the LED */
    if (dataCorrect)
    {
        gpio->SetHigh(LED_DIO);
        PRINTF("LED ON : CORRECT DATA\n");
    }

    /* Spin loop */
    while (true)
    {
        /* Refresh Watchdog */
        Sys_Watchdog_Refresh();
    }
}
