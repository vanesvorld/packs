/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 *  - Simple example on how to use RSL10 pwm driver
 * ------------------------------------------------------------------------- */
#include <app.h>
#include <RTE_Device.h>
#include <PWM_RSLxx.h>
#include <GPIO_RSLxx.h>
#include <printf.h>

#if !RTE_PWM
    #error "Please configure PWM in RTE_Device.h"
#endif    /* if !RTE_PWM */

/* Global variables and types*/
DRIVER_PWM_t *pwm;
DRIVER_GPIO_t *gpio;
DRIVER_PWM_t Driver_PWM;
DRIVER_GPIO_t Driver_GPIO;

/* ----------------------------------------------------------------------------
 * Function      : void Button_EventCallback(void)
 * ----------------------------------------------------------------------------
 * Description   : This function is a callback registered by the function
 *                 Initialize. Based on event argument different actions are
 *                 executed.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Button_EventCallback(uint32_t event)
{
    /* PWM duty cycle */
    static uint8_t pwmDutyCycle = PWM_DUTYCYCLE_HIGH;

    /* Check if expected interrupt occured */
    if (event == GPIO_EVENT_0_IRQ)
    {
        PRINTF("BUTTON PRESSED: CHANGE THE DUTY CYCLE VALUE\n");
        /* Change the duty cycle value */
        if (pwmDutyCycle == PWM_DUTYCYCLE_HIGH)
        {
            /* Update the duty cycle value to low */
            pwmDutyCycle = PWM_DUTYCYCLE_LOW;
        }
        else
        {
            /* Update the duty cycle value to high */
            pwmDutyCycle = PWM_DUTYCYCLE_HIGH;
        }

        /* Set new duty cycle value */
        pwm->SetDutyCycle(PWM_0, pwmDutyCycle);
        PRINTF("LED BRIGHTNESS CHANGED: PWM_DUTYCYCLE VALUE IS %s\n", (pwmDutyCycle==PWM_DUTYCYCLE_HIGH) ? "HIGH" : "LOW");
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the drivers.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    gpio = &Driver_GPIO;
    gpio->Initialize(Button_EventCallback);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);


    pwm = &Driver_PWM;
    pwm->Initialize();
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the drivers. Start the PWM0.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* Initialize the system */
    Initialize();
    PRINTF("DEVICE INITIALIZED\n");
    /* Power up pwm driver */
    pwm->Start(PWM_0);

    /* Spin loop */
    while (true)
    {
        /* Refresh Watchdog */
        Sys_Watchdog_Refresh();
    }
}
