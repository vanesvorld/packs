/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * gpio_util.h
 * - GPIO pin utility functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2018/02/27 15:46:06 $
 * ------------------------------------------------------------------------- */

#ifndef GPIO_UTIL_H
#define GPIO_UTIL_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Defines
 * ------------------------------------------------------------------------- */

#define APP_INIT_PIN                    1
#define BUZZ_APP_INIT_TOGGLE_NUM        10
#define BUZZ_APP_INIT_TOGGLE_DURATION   200000

/* --------------------------------------------------------------------------*/
#define SLEEP_WK_PIN                    1
#define BUZZ_SLEEP_WK_TOGGLE_NUM        100
#define BUZZ_SLEEP_WK_TOGGLE_DURATION   50

/* --------------------------------------------------------------------------*/
#define SVC_NOTIF_PIN                   3
#define BUZZ_SVC_NOTIF_TOGGLE_NUM       100
#define BUZZ_SVC_NOTIF_TOGGLE_DURATION  500

/* --------------------------------------------------------------------------*/
#define TIMER0_ISR_PIN                  5

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * ------------------------------------------------------------------------- */

void Buzz_GPIOs_Init(void);

void Buzz_GPIO(uint32_t gpio_pin, uint32_t toggle_num,
               uint32_t toggle_duration_us, uint32_t sys_clk_MHz);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* GPIO_UTIL_H */
