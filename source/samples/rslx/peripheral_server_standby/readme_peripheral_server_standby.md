Peripheral Device with Standby Mode Sample Code
===============================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project generates a battery service and a custom service. It
then starts an undirected connectable advertising with the device's public
address if an address is available at `DEVICE_INFO_BLUETOOTH_ADDR` in
non-volatile memory three (`NVR3`). If this address is not defined
(all 1s or 0s), use a pre-defined, private Bluetooth(R) address 
(`PRIVATE_BDADDR`) located in `ble_std.h`.

When interacting with a device implementing this sample project, any central
device can scan, connect, and perform service discovery.

The Standby Mode of the device is supported by the Bluetooth Low Energy
library and the system library. In each loop of the main
application, after routine operations are performed, if the system can switch
to Standby Mode, Bluetooth Low Energy configurations and states are
saved and the system is put in Standby Mode. The system is then woken up by 
the Bluetooth Low Energy baseband timer. On waking up, 
configurations and states are restored; therefore, the Bluetooth Low Energy 
connection with the central device (established before going to 
standby) and normal operations of the application are resumed.

This sample project passes through several states before all services are 
enabled:

1. `APPM_INIT` (initialization)
   Application initializing and is configured into an idle state. 
2. `APPM_CREATE_DB` (create database)
   Application has configured the Bluetooth stack, including GAP, according to
   the required role and features. It is now adding services, handlers, and 
   characteristics for all services that it will provide.
3. `APPM_READY` (ready)
   Application has added the desired standard and custom services or profiles 
   into the Bluetooth GATT database and handlers.
4. `APPM_ADVERTISING` (advertising)
   The device starts advertising based on the sample project.
5. `APPM_CONNECTED` (connected)
   Connection is now established with another compatible device.

**This sample project is structured as follows:**

The source code exists in a `code` folder, and application-related include
header files are in the `include` folder.

Code
----
    app.c         - main()
    app_init.c    - All initialization functions called here but the
                    implementation is in the respective c files
    app_process.c - Message handlers for application
    ble_bass.c    - Support functions and message handlers pertaining to 
                    Battery Service Server
    app_custom.c  - Support functions and message handlers pertaining to 
                    Custom Service Server
    ble_std.c     - Support functions and message handlers pertaining to
                    Bluetooth low energy technology
    calibration.c - Fetches or calculates trim values and loads them into the
                    appropriate trim registers
    gpio_util.c   - Utility functions that use GPIOs for testing purposes   

Headers
-------
    app.h         - Overall application header file
    ble_bass.h    - Header file for Battery Service Server
    ble_custom.h  - Header file for Custom Service Server
    ble_std.h     - Header file for Bluetooth Low Energy standard
    calibration.h - Header file for Calibration file
    gpio_util.h   - Header for GPIO utility function

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board
with no external connections required.

Importing Project
-----------------
To import the sample code into your IDE workspace, refer to the
*Getting Started Guide* for your IDE for more information.

Verification
------------
To verify that this application is functioning correctly, use RSL10 or another
third-party central device application to establish a connection. In addition
to establishing a connection, this application can be used to read/write
characteristics and receive notifications. Note that, for power saving 
purpose, the default name of the peripheral device is set to a zero-length 
string.

The LED connected to DIO6 on the Evaluation and Development Board will be on
during the active mode and off during the standby mode. Therefore, you
will see this LED to flash at a slower or faster rate when the advertising
interval (or connection interval) is decreased or increased, respectively. 

To show how an application can send notifications, the application 
periodically sends a custom service notification.

To show that the RF transmission power is set to the desired level (which 
could be 0, 3, or 6 dBm), use a spectrum analyzer to probe the antenna with an
SMA connector. For instance, for a 0 dBm configuration, the spectrum analyzer 
should yeild a maximum RF power of around 0 dBm.

To show the current consumption of the application, configure the board for an
unregulated external supply as described in the *RSL10 Evaluation and 
Development Board Manual*. Connect the board to a DC Power Analyzer and acquire 
the measurement parameters listed below:

Is - Average standby current
Ts - Standby time duration
Ia - Average active current
Ta - Active time duration
I  - Overall average current

The overall average current is calculated as follows:

`I = ( (Ts * Is) + (Ia * Ta) ) / (Ts + Ta)`

Notes
-----
This sample application considers three possible cases regarding the 
calibration of supply voltages:

  - **Case 1:** VDDC, VDDM and VCC trim values are read from NVR4 and loaded into
    corresponding trim registers to calibrate the board. `Sys_RFFE_SetTXPower()`
    sets VDDRF, VDDPA, `PA_PWR` (`RF_REG19`) when applicable to have the desirable 
    radio transmission power. Select this case by defining `CALIB_RECORD`
    as `MANU_CALIB`.

  - **Case 2:** VDDC, VDDM and VCC trim values were calculated and stored in the
    NVR3 by the supplemental calibrate sample application. During the system
    boot process, the user-defined initialization function reads and loads
    those supplemental trim values from NVR3 into corresponding trim registers
    to calibrate the board. `Sys_RFFE_SetTXPower` sets VDDRF, VDDPA, `PA_PWR`
    (`RF_REG19`) when applicable to have the desirable radio transmission power.
    Select this case by defining `CALIB_RECORD` as `SUPPLEMENTAL_CALIB`.

  - **Case 3:** this sample application needs to calculate trim values of VDDC,
    VDDM, VCC, VDDRF and VDDPA for desired voltages and load them into
    corresponding trim registers to calibrate the board. In this case, define
    `CALIB_RECORD` as `USER_CALIB`.

To use DIO's 0, 1, 2, and 3 as wake-up sources, in the `Standby_Mode_Configure()`
function, set `standby_mode_env->wakeup_cfg` with `WAKEUP_DIO*_ENABLE` and
`WAKEUP_DIO*_[RISING | FALLING]`. Besides, it is required to configure
corresponding DIO pins as inputs.

Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
