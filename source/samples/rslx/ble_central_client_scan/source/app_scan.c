/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2018
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_scan.c
 * - SCAN application message handlers
 * ----------------------------------------------------------------------------
 * $Revision: 1.7 $
 * $Date: 2018/09/11 22:39:16 $
 * ------------------------------------------------------------------------- */

#include <app_scan.h>
#include <stdio.h>

/* UART driver and buffers */
extern ARM_DRIVER_USART Driver_USART0;
ARM_DRIVER_USART *uart;
static uint8_t userInput[1];
static char uart_tx_buffer[UART_TX_BUFFER_SIZE];

/* Configuration pre-set in app_config.c */
extern struct gapm_set_dev_config_cmd devConfigCmd;
extern struct gapc_connection_cfm connectionCfm;
extern struct gapm_start_scan_cmd startScanCmd;
extern struct gapm_start_connection_cmd startConnectionCmd;

/* Bluetooth advertisement report list */
static adv_report_t adv_report_list[ADV_REPORT_LIST_MAX];
static uint8_t adv_report_list_size;
static uint8_t device_selection;
static bool cliTimerStarted;


enum app_state_t
{
    APP_STATE_SCANNING,
    APP_STATE_CONNECTING
};

/* Application state CONNECTING or SCANNING */
enum app_state_t app_state;

/* ----------------------------------------------------------------------------
 * Function      : void SCAN_MsgHandler(ke_msg_id_t const msg_id,
 *                                      void const *param,
 *                                      ke_task_id_t const dest_id,
 *                                      ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Scanner kernel message handler. Handles UART terminal screen
 *                 updates and the start/stop of scanning or connection.
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameter
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SCAN_MsgHandler(ke_msg_id_t const msg_id, void const *param,
                     ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    switch(msg_id)
    {
        case GAPM_CMP_EVT:
        {
            const struct gapm_cmp_evt* p = param;
            /* Reset completed. Apply device configuration. */
            if(p->operation == GAPM_RESET && p->status == GAP_ERR_NO_ERROR)
            {
                GAPM_SetDevConfigCmd(&devConfigCmd);
                /* Trigger a GAPM_CMP_EVT / GAPM_SET_DEV_CONFIG when finished */
            }
            /* Set device configuration finished */
            else if(p->operation == GAPM_SET_DEV_CONFIG && p->status == GAP_ERR_NO_ERROR)
            {
                /* Start scanning */
                app_state = APP_STATE_SCANNING;
                SCAN_Start();

                /* Start the LED periodic timer. LED blinks according to the
                 * number of connected peers. See SCAN_LED_Timeout_Handler. */
                ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_MS(200));

                /* Read keyboard strokes from UART (1 byte). See SCAN_UART_EventHandler for details. */
                uart->Receive(userInput, 1);
            }
            /* If in APP_STATE_CONNECTING, keep trying to connect until connection is completed
             * or timed out (i.e. START_CONNECTION_CMD_TIMEOUT expired) */
            else if((app_state == APP_STATE_CONNECTING) &&
               (p->operation == GAPM_CANCEL ||
                p->operation == GAPM_CONNECTION_AUTO ||
                p->operation == GAPM_SCAN_ACTIVE))
            {
                /* Start connection procedure */
                GAPM_StartConnectionCmd(&startConnectionCmd);
            }
            else if((app_state == APP_STATE_SCANNING) &&
                    (p->operation == GAPM_SCAN_ACTIVE ||
                     p->operation == GAPM_CONNECTION_AUTO ||
                     p->operation == GAPM_CANCEL))
            {
                /* Re-start scanning once scan timed out or connection is established */
                SCAN_Start();
            }
        }
        break;

        case GAPM_ADV_REPORT_IND:
        {
            SCAN_AdvReportInd_Handler(param);
        }
        break;

        case GAPC_CONNECTION_REQ_IND:
        {
            uint8_t conidx = KE_IDX_GET(src_id);
            GAPC_ConnectionCfm(conidx, &connectionCfm);
            PRINTF("\n\rGAPC_CONNECTION_REQ_IND\n\r");

            /* Back to scanning mode */
            app_state = APP_STATE_SCANNING;
        }
        break;

        case GAPC_DISCONNECT_IND:
        {
            PRINTF("\n\rGAPC_DISCONNECT_IND: reason = %d", ((struct gapc_disconnect_ind*)param)->reason);
        }
        break;

        case GAPC_PARAM_UPDATE_REQ_IND:
        {
            uint8_t conidx = KE_IDX_GET(src_id);
            struct gapc_param_update_req_ind const *p = param;
            GAPC_ParamUpdateCfm(conidx, true, p->intv_min * 2, p->intv_max * 2);
            PRINTF("\n\rGAPC_PARAM_UPDATE_REQ_IND");
        }
        break;

        case START_CONNECTION_CMD_TIMEOUT:
        {
            /* Cancel any previous air operation */
            GAPM_CancelCmd();

            /* Re-start scanning once connection timed out */
            app_state = APP_STATE_SCANNING;
        }
        break;

        case SCAN_UART_SCREEN_UPDATE_TIMEOUT:
        {
            ke_timer_set(SCAN_UART_SCREEN_UPDATE_TIMEOUT, TASK_APP, SCAN_UART_CLI_TIMEOUT_MS/10);
            SCAN_ScreenUpdateTimeout();
        }
        break;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SCAN_APP_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize UART CMSIS-Driver, application message handlers
 *                 and state variables.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SCAN_APP_Initialize(void)
{
    /* Initialize UART, register callback function and set mode/baud rate */
    uart = &Driver_USART0;
    uart->Initialize(SCAN_UART_EventHandler);
    uart->PowerControl(ARM_POWER_FULL);
    uart->Control(ARM_USART_MODE_ASYNCHRONOUS, UART_BAUDRATE);

    /* Configure application message handlers */
    MsgHandler_Add(TASK_ID_GAPM, SCAN_MsgHandler);
    MsgHandler_Add(TASK_ID_GAPC, SCAN_MsgHandler);
    MsgHandler_Add(SCAN_UART_SCREEN_UPDATE_TIMEOUT, SCAN_MsgHandler);
    MsgHandler_Add(START_CONNECTION_CMD_TIMEOUT, SCAN_MsgHandler);
    MsgHandler_Add(APP_LED_TIMEOUT, SCAN_LED_Timeout_Handler);

    /* Initialize global variables */
    adv_report_list_size = 0;
    device_selection = 0;
    cliTimerStarted = false;
}

/* ----------------------------------------------------------------------------
 * Function      : void SCAN_Start(void)
 * ----------------------------------------------------------------------------
 * Description   : Start scanning for advertisements (send a
 *                 GAPM_START_SCAN_CMD to the stack)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SCAN_Start(void)
{
    if(!cliTimerStarted)
    {
        /* Start the periodic UART report timer. This will print the list of nearby
         * devices currently advertising */
        ke_timer_set(SCAN_UART_SCREEN_UPDATE_TIMEOUT, TASK_APP, SCAN_UART_CLI_TIMEOUT_MS);
        cliTimerStarted = true;
    }

    ke_timer_clear(START_CONNECTION_CMD_TIMEOUT, TASK_APP);
    GAPM_StartScanCmd(&startScanCmd);
}

/* ----------------------------------------------------------------------------
 * Function      : void SCAN_SortAdvReportList(void)
 * ----------------------------------------------------------------------------
 * Description   : Handler advertising reports from the stack and add them to
 *                 the local list of advertisement reports. This function also
 *                 uses SCAN_AdvReport_ExtractName to search for the device
 *                 name in the advertisement data.
 *                 name.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SCAN_AdvReportInd_Handler(const struct gapm_adv_report_ind *p)
{
    for (uint8_t i = 0; i < adv_report_list_size + 1; i++)
    {
        /* Check if the address is already in the list. If so update RSSI */
        if(!memcmp(p->report.adv_addr.addr, adv_report_list[i].adv_addr.addr, BD_ADDR_LEN))
        {
            adv_report_list[i].rssi = p->report.rssi;
            return;
        }
    }

    if(adv_report_list_size < ADV_REPORT_LIST_MAX)
    {
        /* Add adv report to the list, sorted by RSSI */
        memcpy(&adv_report_list[adv_report_list_size], &(p->report), sizeof(adv_report_t));

        /* Search payload for short name or name and update name field */
        SCAN_AdvReport_ExtractName(p->report.data, p->report.data_len, adv_report_list[adv_report_list_size].data);

        adv_report_list_size++;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SCAN_AdvReport_ExtractName(const uint8_t* data,
 *                                                 uint8_t size,
 *                                                 uint8_t *result)
 * ----------------------------------------------------------------------------
 * Description   : Search for the device name based on flags in the ADV data.
 *                 name.
 * Inputs        : - data      -
 *                 - size      - Number of bytes in data
 * Outputs       : result      - Returns the device name in the "result"
 *                               variable. If no name is found, return "-".
 * Assumptions   : result has enough space allocated for the device name.
 * ------------------------------------------------------------------------- */
void SCAN_AdvReport_ExtractName(const uint8_t* data, uint8_t size,
                                uint8_t *result)
{
    uint8_t len = 0;
    uint8_t type;

    for (uint8_t i = 0; i < size;  i += len+1)
    {
        len = data[i];
        type = data[i+1];
        if (type == 0x8 || type == 0x9)
        {
            memcpy(&result[0], (uint8_t*)(&data[i+2]), len - 1);
            result[len - 1] = 0;
            return;
        }
    }

    /* Didn't find a name */
    sprintf((char*)result, "-");
}

/* ----------------------------------------------------------------------------
 * Function      : void SCAN_ScreenUpdateTimeout(void)
 * ----------------------------------------------------------------------------
 * Description   : Periodically updates the information printed at the UART.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SCAN_ScreenUpdateTimeout(void)
{
    /* VT100 codes for clearing the screen. Works on VT100 terminals only */
    /* Clear ("\033[2J") and move cursor ("\033[0;0H") */
    SCAN_UART_SendString("\033[2J\033[0;0H");
    SCAN_UART_SendString("Scanned devices:\n");

    for (int i = 0; i < adv_report_list_size; i++)
    {
        const uint8_t* addr = adv_report_list[i].adv_addr.addr;
        /* Create a UART string entry with device index, rssi, address, device name */
        /* Indicate which device was selected for a connection */
        snprintf(uart_tx_buffer, UART_TX_BUFFER_SIZE,
                 "[ ] %02d | %02d dBm | 0x%02x%02x%02x%02x%02x%02x | %s\n", i+1, adv_report_list[i].rssi,
                 addr[5], addr[4], addr[3], addr[2], addr[1], addr[0], adv_report_list[i].data);

        if(i == device_selection)
        {
            uart_tx_buffer[1] = 'X';
        }

        SCAN_UART_SendString(uart_tx_buffer);
    }

    SCAN_UART_SendString("\nConnected devices:\n");

    for(uint8_t i = 0, j = 1; i < BLE_CONNECTION_MAX; i++)
    {
        if(GAPC_IsConnectionActive(i))
        {
            const uint8_t* addr = GAPC_GetConnectionInfo(i)->peer_addr.addr;
            /* Create a UART string entry with device index, rssi, address, device name */
            /* Indicate which device was selected for a connection */
            snprintf(uart_tx_buffer, UART_TX_BUFFER_SIZE,
                     "%02d | 0x%02x%02x%02x%02x%02x%02x \n", j++,
                     addr[5], addr[4], addr[3], addr[2], addr[1], addr[0]);

            SCAN_UART_SendString(uart_tx_buffer);
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SCAN_UART_EventHandler(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : Handle UART CMSIS-Driver events. Mostly data reception.
 * Inputs        : event      - Event type. The function only handles
 *                              ARM_USART_EVENT_RECEIVE_COMPLETE
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SCAN_UART_EventHandler(uint32_t event)
{
    if(event == ARM_USART_EVENT_RECEIVE_COMPLETE)
    {
        switch(userInput[0])
        {
            case KEY_ARROW_UP:
            {
                /* Move cursor up */
                if(device_selection > 0)
                {
                    device_selection--;
                }
            }
            break;

            case KEY_ARROW_DOWN:
            {
                /* Move cursor down */
                if(device_selection < (adv_report_list_size - 1))
                {
                    device_selection++;
                }
            }
            break;

            case 'c':
            case 'C':
            {
                /* Clear list of scanned devices */
                adv_report_list_size = 0;
                device_selection = 0;
            }
            break;

            case KEY_ENTER:
            {
                /* Copy peer information into connection request */
                memcpy(startConnectionCmd.peers[0].addr.addr,
                       adv_report_list[device_selection].adv_addr.addr, GAP_BD_ADDR_LEN);
                startConnectionCmd.peers[0].addr_type = adv_report_list[device_selection].adv_addr_type;
                startConnectionCmd.nb_peers = 1;

                app_state = APP_STATE_CONNECTING;
                ke_timer_set(START_CONNECTION_CMD_TIMEOUT, TASK_APP, START_CONNECTION_CMD_TIMEOUT_MS/10);

                /* Cancel any previous air operation */
                GAPM_CancelCmd();
            }
            break;

            default:
            {
                /* no action for other  */
            }
        }
        uart->Receive(userInput, 1);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SCAN_UART_SendString(const uint8_t *str)
 * ----------------------------------------------------------------------------
 * Description   : Wrapper to send a string over UART using the CMSIS-Driver.
 *                 Wait transfer to finish before returning.
 * Inputs        : str      - String data
 * Outputs       : None
 * Assumptions   : The UART CMISIS-driver has been initialized; 'str' has an
 *                 end of string byte at the end.
 * ------------------------------------------------------------------------- */
void SCAN_UART_SendString(const char *str)
{
    uart->Send(str, strlen(str));

    /* Wait for transfer to finish */
    while(uart->GetStatus().tx_busy)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void APP_LED_Timeout_Handler(ke_msg_idd_t const msg_id,
 *                                              void const *param,
 *                                              ke_task_id_t const dest_id,
 *                                              ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Control GPIO "LED_DIO_NUM" behavior using a timer.
 *                 Possible LED behaviors:
 *                     - If the device is advertising but it has not connected
 *                       to any peer: the LED blinks every 200 ms.
 *                     - If the device is advertising and it is connecting to
 *                       fewer than BLE_CONNECTION_MAX peers: the LED blinks
 *                       every 2 seconds according to the number of connected
 *                       peers (i.e., blinks once if one peer is connected,
 *                       twice if two peers are connected, etc.).
 *                     - If the device is connected to BLE_CONNECTION_MAX peers
 *                       the LED is steady on.
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameter (unused)
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SCAN_LED_Timeout_Handler(ke_msg_id_t const msg_id, void const *param,
                             ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    static uint8_t toggle_cnt = 0;
    uint8_t connectionCount = GAPC_GetConnectionCount();

    /* Blink LED according to the number of connections */
    switch (connectionCount)
    {
        case 0:
        {
            ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_MS(200));
            Sys_GPIO_Toggle(LED_DIO_NUM); /* Toggle LED_DIO_NUM every 200ms */
            toggle_cnt = 0;
        }
        break;

        case APP_NB_PEERS:
        {
            ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_MS(200));
            Sys_GPIO_Set_High(LED_DIO_NUM); /* LED_DIO_NUM steady high */
            toggle_cnt = 0;
        }
        break;

        default: /* connectionCount is between 1 and APP_NB_PEERS (exclusive) */
        {
            if (toggle_cnt >= connectionCount * 2)
            {
                toggle_cnt = 0;
                ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_S(2)); /* Schedule timer for a long 2s break */
                Sys_GPIO_Set_High(LED_DIO_NUM); /* LED_DIO_NUM steady high until next 2s blinking period */
            }
            else
            {
                toggle_cnt++;
                Sys_GPIO_Toggle(LED_DIO_NUM);
                ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_MS(200));
            }
        }
    }
}
