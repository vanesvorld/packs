/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2018
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Main application file
 * ----------------------------------------------------------------------------
 * $Revision: 1.4 $
 * $Date: 2018/09/11 22:39:16 $
 * ------------------------------------------------------------------------- */

#include <app.h>

int main(void)
{
    /* Configure hardware and initialize BLE stack */
    Device_Initialize();

    /* Initialize application (UART driver and message handlers) */
    SCAN_APP_Initialize();
    SCAN_UART_SendString("Starting ble_central_client_scan application...\n\r");

    /* Reset the GAP manager. Trigger GAPM_CMP_EVT / GAPM_RESET when finished.
     * See SCAN_MsgHandler */
    GAPM_ResetCmd();

    while (1)
    {
        Kernel_Schedule();

        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* Wait for an event before executing the scheduler again */
        SYS_WAIT_FOR_EVENT;
    }
}
