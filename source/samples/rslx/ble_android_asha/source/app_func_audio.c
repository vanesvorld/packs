/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_func.c
 * - Audio application functions
 * ------------------------------------------------------------------------- */

#include "app.h"
#include "app_audio.h"
#include "queue.h"

#include <ble_asha.h>

/* Global variables */
uint32_t primEvtCnt = 0, secEvtCnt = 0, invalidRxCnt = 0;
uint8_t sentValue = 0;
uint32_t cntr = 0;
uint32_t cntr_coded = 0;
int16_t temp_sin_signal[FRAME_LENGTH];
uint8_t volumeShift = 0;
uint8_t seqNum_prev;

sync_param_t env_sync;
audio_frame_param_t env_audio;
struct queue_t audio_queue;

/* Enable / disable PLC feature */
bool plc_enable = true;

/* ----------------------------------------------------------------------------
 * Function      : void DIO0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Toggle selection to enable/ disable PLC feature
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#if 0
void DIO0_IRQHandler(void)
{
    static uint8_t ignore_next_dio_int = 0;
    if (ignore_next_dio_int)
    {
        ignore_next_dio_int = 0;
    }
    else if (DIO_DATA->ALIAS[BUTTON_DIO] == 0)
    {
        NVIC_DisableIRQ(DIO0_IRQn);

        /* Button is pressed: Ignore next interrupt.
         * This is required to deal with the debounce circuit limitations. */
        ignore_next_dio_int = 1;

        //plc_enable = !(plc_enable);

        PRINTF("%s\r\n", (plc_enable) ? "PLC Enabled" : "PLC Disabled");
        NVIC_EnableIRQ(DIO0_IRQn);
    }
}
#endif

/* ----------------------------------------------------------------------------
 * Function      : void APP_Audio_Transfer(const uint8_t *audio_buff,
 *                                    uint8_t audio_length, uint8_t seqNum)
 * ----------------------------------------------------------------------------
 * Description   : Handles a received Android Audio frame
 * Inputs        : audio_buff    - pointer of the received frame buffer
 *                 audio_length  -
 *                 seqNum        -
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */

void APP_Audio_Transfer(uint8_t *audio_buff, uint16_t audio_length, uint8_t seqNum)
{
    uint8_t i;

    Sys_GPIO_Toggle(ASHA_EVENT_DIO);

    if( ((uint8_t) (seqNum - seqNum_prev - 1)) != 0 ) {
        PRINTF("\r\n APP_Audio_Transfer: seqNum_prev = %d, seqNum = %d\r\n", seqNum_prev, seqNum);
    }

    seqNum_prev = seqNum;

    /* Transient if this is the first packet received after a start */
    if ( env_audio.state == LINK_TRANSIENT )
    {
        /* Add credits to avoid starvation */
        ASHA_AddCredits(ASHA_L2CC_INITIAL_CREDITS);
        APP_Audio_FlushQueue();
    }

    for (i = 0; i < audio_length; i+= ENCODED_FRAME_LENGTH)
    {
        QueueInsert(&audio_queue, &audio_buff[i], GOOD_PACKET);
    }

    env_sync.cntr_connection = 0;

    if ( env_audio.state == LINK_TRANSIENT )
        APP_Audio_Start();
}

void APP_Audio_Start(void)
{
    PRINTF("Established\r\n");

    env_sync.cntr_transient = 0;
    env_sync.audio_sink_cnt = 0;
    env_sync.flag_ascc_phase = false;

    ASRC_CTRL->ASRC_ENABLE_ALIAS = ASRC_ENABLED_BITBAND;
    Sys_ASRC_Reset();

    /* ASCC interrupts */
    NVIC_EnableIRQ(AUDIOSINK_PHASE_IRQn);
    NVIC_EnableIRQ(AUDIOSINK_PERIOD_IRQn);

    /* LPDSP32 interrupt */
    NVIC_EnableIRQ(DSP0_IRQn);

    /* Timer interrupts */
    NVIC_EnableIRQ(TIMER_IRQn(TIMER_RENDER));
    NVIC_EnableIRQ(TIMER_IRQn(TIMER_REGUL));

    env_audio.state = LINK_ESTABLISHED;
}

void APP_Audio_Disconnect(void)
{
    APP_ResetPrevSeqNumber();

    PRINTF("\r\nDisconnect\r\n");

    env_sync.cntr_connection = 0;

    ASRC_CTRL->ASRC_DISABLE_ALIAS = ASRC_DISABLED_BITBAND;

    /* ASCC interrupt */
    NVIC_DisableIRQ(AUDIOSINK_PHASE_IRQn);
    NVIC_DisableIRQ(AUDIOSINK_PERIOD_IRQn);

    /* LPDSP32 interrupt */
    NVIC_DisableIRQ(DSP0_IRQn);

    /* Timer interrupts */
    NVIC_DisableIRQ(TIMER_IRQn(TIMER_RENDER));
    NVIC_DisableIRQ(TIMER_IRQn(TIMER_REGUL));
    Sys_Timers_Stop(1U << TIMER_RENDER);
    Sys_Timers_Stop(1U << TIMER_REGUL);

    APP_Audio_FlushQueue();

    env_audio.state = LINK_DISCONNECTED;
}

void APP_Audio_FlushQueue(void)
{
    uint16_t queue_len;
    uint16_t i;

    queue_len = QueueCount(&audio_queue);
    for(i = 0; i < queue_len; ++i)
        QueueFree(&audio_queue);
}


/* ----------------------------------------------------------------------------
 * Function      : void Volume_Set(int8_t volume)
 * ----------------------------------------------------------------------------
 * Description   : Normalizes the volume range from [-128, 0] to [16, 0]. The
 * 				   new scale is used in Volume_Set() to adjust by shifting the
 * 				   samples to the right
 * Inputs        : - volume	- Volume [-128 to 0 range]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Volume_Set(int8_t volume)
{
    volumeShift = 16 - ((volume + 128) / 8);
}

/* ----------------------------------------------------------------------------
 * Function      : void Volume_Shift_Subframe (int16_t *src)
 * ----------------------------------------------------------------------------
 * Description   : Adjust the audio level by a simple shift.
 * Inputs        : - src	- pointer to the audio frame
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Volume_Shift_Subframe(int16_t *src)
{
    /* Shift subframe samples to change the volume */
    for (uint16_t i = 0; i < SUBFRAME_LENGTH; i++)
    {
        src[i] = src[i] >> volumeShift;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void APP_ResetPrevSeqNumber(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize ASHA sequence number control after a disconnect.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void APP_ResetPrevSeqNumber(void)
{
    seqNum_prev = 0xFF;
}

/* ----------------------------------------------------------------------------
 * Function      : void TIMER_IRQ_FUNC(TIMER_REGUL)(void)
 * ----------------------------------------------------------------------------
 * Description   : regulator timer interrupt handler
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER_IRQ_FUNC(TIMER_REGUL)(void)
{
    env_audio.frame_idx += SUBFRAME_LENGTH;
    if (env_audio.frame_idx <= FRAME_LENGTH)
    {
        Sys_Timers_Start(1U << TIMER_REGUL);

        if (env_audio.frame_idx < FRAME_LENGTH)
        {
        	Volume_Shift_Subframe(&env_audio.frame_dec[env_audio.frame_idx]);
            Sys_DMA_Set_ChannelSourceAddress(ASRC_IN_IDX, (uint32_t)&env_audio.frame_dec[env_audio.frame_idx]);

            /* Re-enable DMA for ASRC input */
            Sys_DMA_ChannelEnable(ASRC_IN_IDX);

            /* Enable ASRC block */
            Sys_ASRC_StatusConfig(ASRC_ENABLE);
        }
    }
    else
    {
        /* De-assert SPI_CS */
        SPI0_CTRL1->SPI0_CS_ALIAS = SPI0_CS_1_BITBAND;

        /* Check if the ASRC has finish on the frame */
        ASSERT(ASRC_CTRL->ASRC_PROC_STATUS_ALIAS == ASRC_IDLE_BITBAND);
        Sys_ASRC_StatusConfig(ASRC_DISABLE);

        env_audio.proc = false;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void rendering_timer_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : rendering timer interrupt handler
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER_IRQ_FUNC(TIMER_RENDER) (void)
{
    /* for accuracy start the rendering timer in free-run mode once */
    if (!env_sync.timer_free_run)
    {
        Sys_Timers_Stop(1U << TIMER_RENDER);
        Sys_Timer_Set_Control(TIMER_RENDER, TIMER_FREE_RUN  | \
                              (AUDIO_INTV_PERIOD - 1)    | \
                              TIMER_SLOWCLK_DIV2);
        Sys_Timers_Start(1U << TIMER_RENDER);
        env_sync.timer_free_run = true;
    }

    env_audio.frame_in = QueueFront(&audio_queue, &env_audio.packet_state);

    if (env_audio.frame_in!=NULL)
    {
        memcpy(lpdsp32.incoming, env_audio.frame_in, ENCODED_FRAME_LENGTH * sizeof(uint8_t));
        QueueFree(&audio_queue);
        ASHA_AddCredits(1);
    }
    else
    {
        env_audio.packet_state = BAD_PACKET;
    }

    /* Check if the decoder has finished on the frame */
    ASSERT(lpdsp32.state == DSP_IDLE);
    lpdsp32.state = DSP_BUSY;

    if (plc_enable)
    {
        if (env_audio.packet_state == GOOD_PACKET)
        {
            lpdsp32.channels.action &= ~DECODE_PLC;
        }
        else if (env_audio.packet_state == BAD_PACKET)
        {
            lpdsp32.channels.action |= DECODE_PLC;
        }
        else
        {
            lpdsp32.channels.action |= DECODE_PLC;
        }
    }
    else
    {
        /* No PLC performed */
        lpdsp32.channels.action &= ~DECODE_PLC;
    }

    codecSetParameters(lpdsp32.codec, &lpdsp32.channels);
    codecDecode(lpdsp32.codec);

    lpdsp32.channels.action &= ~DECODE_RESET;

    env_sync.cntr_connection++;

    /* disable the interrupts if there is no audio packet */

//    if (env_sync.cntr_connection > DISCONN_TIMES)
//        APP_Audio_Disconnect();
}

/* ----------------------------------------------------------------------------
 * Function      : void DSP0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : LPDSP32 decoder interrupt handler (RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DSP0_IRQHandler(void)
{
    /* Check if there is any sub-frame from prev frame waiting to be processed */
    ASSERT(!env_audio.proc); //TODO: investigate why this is being triggered.
    env_audio.proc = true;

    env_audio.frame_dec = (int16_t *)lpdsp32.outgoing;
    if (env_sync.flag_ascc_phase)
    {
        asrc_reconfig(&env_sync);
        env_sync.flag_ascc_phase = false;
    }

#if SIMUL
    memcpy(env_audio.frame_dec, sin_signal, sizeof(uint16_t) * FRAME_LENGTH);
#else    /* if SIMUL */

    /* Uncomment these two lines if you need a predefined signal to generate
     * for debug */

    //memcpy(temp_sin_signal, sin_signal, FRAME_LENGTH* sizeof(int16_t));
    //env_audio.frame_dec = temp_sin_signal;
#endif    /* if SIMUL */

    Volume_Shift_Subframe(env_audio.frame_dec);

    /* Assert SPI_CS */
    SPI0_CTRL1->SPI0_CS_ALIAS = SPI0_CS_0_BITBAND;
    Sys_DMA_ClearChannelStatus(ASRC_IN_IDX);
    Sys_DMA_Set_ChannelSourceAddress(ASRC_IN_IDX, (uint32_t)env_audio.frame_dec);

    /* Re-enable DMA for ASRC input */
    Sys_DMA_ChannelEnable(ASRC_IN_IDX);

    /* Enable ASRC block */
    Sys_ASRC_StatusConfig(ASRC_ENABLE);

    env_audio.frame_idx = 0;
    Sys_Timers_Start(1U << TIMER_REGUL);
    lpdsp32.state = DSP_IDLE;
}

/* ----------------------------------------------------------------------------
 * Function      : void TIMER_IRQ_FUNC(TIMER_SIMUL)(void)
 * ----------------------------------------------------------------------------
 * Description   : Simulation timer interrupt routine. It's for debugging the
 *                 audio sync mechanism
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER_IRQ_FUNC(TIMER_SIMUL)(void)
{
#if SIMUL
    uint8_t frame_static[ENCODED_FRAME_LENGTH];
    static uint16_t crpt_cntr = 0;

    //Sys_GPIO_Toggle(ASCC_PHASE_ISR_DIO);


    crpt_cntr = (crpt_cntr > CONN_TIMES) ? 0 : (crpt_cntr + 1);

    /* Insert consecutive bad packets */
    if (crpt_cntr == 0)
    {
        env_audio.packet_state = BAD_PACKET;
    }
    else if (crpt_cntr <= 1)
    {
        env_audio.packet_state = BAD_CONSECUTIVE_PACKET;
    }
    else
    {
        memcpy(frame_static, &coded_sin[cntr_coded], sizeof(uint8_t)* ENCODED_FRAME_LENGTH) ;
        env_audio.packet_state = GOOD_PACKET;
    }

    cntr_coded = (cntr_coded + ENCODED_FRAME_LENGTH) % (3*ENCODED_FRAME_LENGTH);

    BBIF->SYNC_CFG = (ACTIVE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    BBIF->SYNC_CFG = (ACTIVE | RX_ACTIVE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);

    DIO->BB_RX_SRC = (BB_RF_SYNC_P_SRC_CONST_HIGH | (DIO->BB_RX_SRC &
                                                     ~DIO_BB_RX_SRC_RF_SYNC_P_Mask));
    DIO->BB_RX_SRC = (BB_RF_SYNC_P_SRC_CONST_LOW | (DIO->BB_RX_SRC &
                                                    ~DIO_BB_RX_SRC_RF_SYNC_P_Mask));

    BBIF->SYNC_CFG = (ACTIVE | RX_IDLE | SYNC_ENABLE | SYNC_SOURCE_RF_RX);
    BBIF->SYNC_CFG = (IDLE | RX_IDLE | SYNC_DISABLE | SYNC_SOURCE_RF_RX);

    Sys_GPIO_Toggle(ASCC_PHASE_ISR_DIO);

    QueueInsert(&audio_queue, frame_static, env_audio.packet_state);

    env_sync.cntr_connection = 0;
    if (env_audio.state == LINK_DISCONNECTED)
    {
        if (QueueCount(&audio_queue) >= PACK_NUM_QUEUE)
        {
            PRINTF("Transient\r\n");
            env_sync.cntr_transient = 0;
            env_sync.audio_sink_cnt = 0;
            env_sync.flag_ascc_phase = false;

            ASRC_CTRL->ASRC_ENABLE_ALIAS = ASRC_ENABLED_BITBAND;
            Sys_ASRC_Reset();

            /* ASCC interrupts */
            NVIC_EnableIRQ(AUDIOSINK_PHASE_IRQn);
            NVIC_EnableIRQ(AUDIOSINK_PERIOD_IRQn);

            /* LPDSP32 interrupt */
            NVIC_EnableIRQ(DSP0_IRQn);

            /* Timer interrupts */
            NVIC_EnableIRQ(TIMER_IRQn(TIMER_RENDER));
            NVIC_EnableIRQ(TIMER_IRQn(TIMER_REGUL));

            env_audio.state = LINK_TRANSIENT;
        }
    }
#endif    /* if SIMUL */
}

/* ----------------------------------------------------------------------------
 * Function      : void AUDIOSINK_PHASE_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : ASCC phase interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void AUDIOSINK_PHASE_IRQHandler(void)
{
    Sys_GPIO_Toggle(ASCC_PHASE_ISR_DIO);
#if SIMUL
    static uint32_t sim_missed = 0;
    sim_missed = (sim_missed + 1) % 100;

    if (sim_missed < 1)
#else    /* if SIMUL */
    if (AUDIOSINK_CTRL->PHASE_CNT_MISSED_STATUS_ALIAS)
#endif    /* if SIMUL */
    {
        env_sync.phase_cnt_missed = true;
    }
    else
    {
        /* Start the rendered timer */
        Sys_Timers_Stop(1U << TIMER_RENDER);

        /* 4.5 ms (connection interval is 10 ms) + channel delay */
        Sys_Timer_Set_Control(TIMER_RENDER, TIMER_SHOT_MODE             | \
                              (RENDER_TIME_US + /*TODO: android_support_env.channel_delay*/ - 1)  | \
                              TIMER_SLOWCLK_DIV2);
        Sys_Timers_Start(1U << TIMER_RENDER);
        env_sync.timer_free_run = false;

        /* Get audio sink phase count */
        env_sync.audio_sink_phase_cnt = Sys_Audiosink_PhaseCounter();
        if (!env_sync.phase_cnt_missed)
        {
            /* Get audio sink count */
            env_sync.audio_sink_cnt = Sys_Audiosink_Counter() << SHIFT_BIT;
            env_sync.audio_sink_cnt +=
                ((((env_sync.audio_sink_phase_cnt_prev - env_sync.audio_sink_phase_cnt))
                  << SHIFT_BIT) / env_sync.audio_sink_period_cnt);
        }

        /* store audio sink count phase for the next time */
        env_sync.audio_sink_phase_cnt_prev = env_sync.audio_sink_phase_cnt;
        env_sync.phase_cnt_missed = false;
    }
    env_sync.flag_ascc_phase = true;

    AUDIOSINK_CTRL->CNT_RESET_ALIAS = CNT_RESET_BITBAND;
    AUDIOSINK->PHASE_CNT = 0;
    AUDIOSINK_CTRL->PHASE_CNT_START_ALIAS = PHASE_CNT_START_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void AUDIOSINK_PERIOD_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : ASCC period interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void AUDIOSINK_PERIOD_IRQHandler(void)
{
    env_sync.audio_sink_period_cnt = Sys_Audiosink_PeriodCounter() /
                                     (AUDIOSINK->CFG + 1);
    AUDIOSINK->PERIOD_CNT = 0;
    AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = PERIOD_CNT_START_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void asrc_reconfig(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure ASRC (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void asrc_reconfig(sync_param_t *sync_param)
{
    int64_t asrc_inc_carrier;

    Sys_ASRC_ResetOutputCount();

    sync_param->Cr = FRAME_LENGTH << SHIFT_BIT;
    sync_param->Ck = sync_param->audio_sink_cnt;

    if ((sync_param->Ck <= (FRAME_LENGTH - ASRC_CFG_THR) << SHIFT_BIT) ||
        (sync_param->Ck >= (FRAME_LENGTH + ASRC_CFG_THR) << SHIFT_BIT))
    {
        sync_param->Ck = sync_param->Ck_prev;
        sync_param->avg_ck_outputcnt = 0;
    }

    /* Store Ck to apply on the next packet if the audio sink value is out of
     * range
     */
    sync_param->Ck_prev = sync_param->Ck;

    /* Configure ASRC base on new Ck */
    asrc_inc_carrier = (((sync_param->Cr - sync_param->Ck) << 29) / sync_param->Ck);
    asrc_inc_carrier &= 0xFFFFFFFF;
    Sys_ASRC_Config(asrc_inc_carrier, LOW_DELAY | ASRC_DEC_MODE1);
}

/* ----------------------------------------------------------------------------
 * Function      : void ASRC_ERROR_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : ASRC error interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void ASRC_ERROR_IRQHandler(void)
{
    ASSERT(ASRC_CTRL->ASRC_IN_ERR_ALIAS == 0);
    ASSERT(ASRC_CTRL->ASRC_UPDATE_ERR_ALIAS == 0);

    ASRC_CTRL->ASRC_UPDATE_ERR_CLR_ALIAS = CLR_ASRC_UPDATE_ERR_BITBAND;
    ASRC_CTRL->ASRC_IN_ERR_CLR_ALIAS = CLR_ASRC_IN_ERR_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void Reset_Audio_Sync_Param(sync_param_t *sync_param,
 *                                          audio_frame_param_t *audio_param)
 * ----------------------------------------------------------------------------
 * Description   : Reset the audio and sync instances
 * Inputs        : - sync_param  - pointer to the sync structure
 *                 - audio_param - pointer to the audio structure
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Reset_Audio_Sync_Param(sync_param_t *sync_param, audio_frame_param_t *audio_param)
{
    sync_param->Ck_prev = FRAME_LENGTH << SHIFT_BIT;
    sync_param->Cr = 0;
    sync_param->Ck = 0;
    sync_param->audio_sink_cnt = 0;
    sync_param->avg_ck_outputcnt = 0;
    sync_param->audio_sink_period_cnt = 0;
    sync_param->audio_sink_phase_cnt = 0;
    sync_param->audio_sink_phase_cnt_prev = 0;
    sync_param->asrc_cnt_cnst = 0;
    sync_param->phase_cnt_missed = false;
    sync_param->flag_ascc_phase = false;
    sync_param->cntr_connection = 0;
    sync_param->cntr_transient = 0;
    sync_param->timer_free_run = false;

    audio_param->proc = false;
    audio_param->frame_idx = 0;
    audio_param->state = LINK_DISCONNECTED;
    audio_param->packet_state = BAD_PACKET;

    Sys_Timers_Stop(1U << TIMER_REGUL);
    Sys_Timer_Set_Control(TIMER_REGUL, TIMER_SHOT_MODE  | \
                          (REGUL_TIME_US - 1)        | \
                          TIMER_SLOWCLK_DIV2);
}
