/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_func.c
 * - Application functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.7 $
 * $Date: 2018/02/27 15:46:06 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <stdio.h>

uint8_t ear_side = APP_RM_AUDIO_CHANNEL;
uint8_t outTempBuff[100];
#if (INPUT_INTRF == SPI_RX_CODED_INPUT)
int8_t spi_buf[AUDIO_FRAME_SIZE * 2];
#endif    /* if (INPUT_INTRF == SPI_RX_CODED_INPUT) */

/* ----------------------------------------------------------------------------
 * Function      : void  DMA_IRQ_FUNC(RX_DMA_NUM)(void)
 * ----------------------------------------------------------------------------
 * Description   : DMA Channel assigned to SPI and PCM receive interrupt handler
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA_IRQ_FUNC(RX_DMA_NUM)(void)
{
    /* Enabled if input to the transmitter is coded format. From SPI interface. */
#if (INPUT_INTRF == SPI_RX_CODED_INPUT)
    static uint8_t length = AUDIO_FRAME_SIZE;
    RM_EventHandler(RM_TX_PAYLOAD_READY_LEFT, &length, (uint8_t *)&spi_buf[0]);
    RM_EventHandler(RM_TX_PAYLOAD_READY_RIGHT, &length,
                    (uint8_t *)&spi_buf[length]);
#endif    /* if (INPUT_INTRF == SPI_RX_CODED_INPUT) */
    Sys_DMA_ClearChannelStatus(RX_DMA_NUM);
}

/* ----------------------------------------------------------------------------
 * Function      : void app_process_incomming_data(uint8_t *data, uint8_t length)
 * ----------------------------------------------------------------------------
 * Description   : Process incoming data from the radio application layer
 * Inputs        : data - pointer to the input buffer
 *               : length - length of data in the buffer
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void App_Process_Incoming_Data(uint8_t *data, uint8_t length)
{
#if (OUTPUT_INTRF == SPI_TX_CODED_OUTPUT)
    if (length == 0)
    {
        /* Packet loss concealment for G722 should be applied as TX hasn't
         * sent data */
        memset(outTempBuff, 0xaa,
               ((app_env.rm_param.audio_rate * app_env.rm_param.interval_time) /
                8000));
    }
    else
    {
        SPI0_CTRL1->SPI0_CS_ALIAS = SPI0_CS_1_BITBAND;
        memcpy(outTempBuff, data, length);
        SPI0_CTRL1->SPI0_CS_ALIAS = SPI0_CS_0_BITBAND;
        Sys_DMA_ChannelConfig(
            TX_DMA_NUM,
            TX_DMA_SPI,
            AUDIO_FRAME_SIZE,
            0,
            (uint32_t)outTempBuff,
            (uint32_t)&SPI0->TX_DATA
            );
        Sys_DMA_ClearChannelStatus(TX_DMA_NUM);
        Sys_DMA_ChannelEnable(TX_DMA_NUM);
    }
#endif    /* if (OUTPUT_INTRF == SPI_TX_CODED_OUTPUT) */
}
