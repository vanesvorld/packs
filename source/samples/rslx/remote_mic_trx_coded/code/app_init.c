/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_init.c
 * - Contains application initialization code for hardware
 * ----------------------------------------------------------------------------
 * $Revision: 1.9 $
 * $Date: 2019/08/29 15:34:25 $
 * ------------------------------------------------------------------------- */

#include "app.h"

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_Coded_SPI_Input_Type(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize SPI peripheral and DMA for data transfer
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#if (INPUT_INTRF == SPI_RX_CODED_INPUT)
void Initialize_Coded_SPI_Input_Type(void)
{
    /* Initialize SPI0 slave interface  interface. */
    Sys_SPI_DIOConfig(0, SPI0_SELECT_SLAVE,
                      DIO_LPF_DISABLE | DIO_WEAK_PULL_UP, SPI_CLK_DO, SPI_CS_DO,
                      SPI_SER_DI,
                      SPI_SER_DO);

    /* Configure the SPI0 slave interface. */
    Sys_SPI_Config(0, SPI0_SELECT_SLAVE | SPI0_ENABLE |
                   SPI0_CLK_POLARITY_NORMAL | SPI0_CONTROLLER_DMA |
                   SPI0_MODE_SELECT_AUTO | SPI0_PRESCALE_16);

    /* Configure the slave interface for 8-bit transfer. */
    Sys_SPI_TransferConfig(0, SPI0_START | SPI0_RW_DATA | SPI0_CS_1 |
                           SPI0_WORD_SIZE_8);

    /* Setup DMA channel for transferring data from SPI Slave into memory. */
    Sys_DMA_ChannelConfig(RX_DMA_NUM, DMA_RX_CONFIG, AUDIO_FRAME_SIZE * 2, 0,
                          (uint32_t)&(SPI0->RX_DATA),
                          (uint32_t)&spi_buf[0]);

    /* Clear the current status for the DMA receive channel for SPI */
    Sys_DMA_ClearChannelStatus(RX_DMA_NUM);

    /* Enable DMA receive channel. */
    Sys_DMA_ChannelEnable(RX_DMA_NUM);

    /* Clear pending DMA receive channel request and configure interrupt. */
    NVIC_ClearPendingIRQ(DMA_IRQn(RX_DMA_NUM));
    NVIC_SetPriority(DMA_IRQn(RX_DMA_NUM), 2);

    /* Enable DMA receive channel interrupt. */
    NVIC_EnableIRQ(DMA_IRQn(RX_DMA_NUM));
}

#endif    /* if (INPUT_INTRF == SPI_RX_CODED_INPUT) */

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_Coded_SPI_Input_Type(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize SPI peripheral and DMA for data transfer
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
#if (OUTPUT_INTRF == SPI_TX_CODED_OUTPUT)
void Initialize_Coded_SPI_Output_Type(void)
{
    /* Initialize SPI interface */
    Sys_SPI_DIOConfig(0, SPI0_SELECT_MASTER,
                      DIO_LPF_DISABLE | DIO_WEAK_PULL_UP, SPI_CLK_DO, SPI_CS_DO,
                      SPI_SER_DI, SPI_SER_DO);

    /* Configure the SPI0 interface */
    Sys_SPI_Config(0, SPI0_SELECT_MASTER    | SPI0_ENABLE         |
                   SPI0_CLK_POLARITY_NORMAL | SPI0_CONTROLLER_DMA |
                   SPI0_MODE_SELECT_AUTO    | SPI0_PRESCALE_32);

    Sys_SPI_TransferConfig(0, SPI0_START | SPI0_WRITE_DATA | SPI0_CS_1 |
                           SPI0_WORD_SIZE_8);

    NVIC_ClearPendingIRQ(DMA_IRQn(TX_DMA_NUM));
    NVIC_SetPriority(DMA_IRQn(TX_DMA_NUM), 2);
    NVIC_EnableIRQ(DMA_IRQn(TX_DMA_NUM));
}

#endif    /* if (OUTPUT_INTRF == SPI_TX_CODED_OUTPUT) */
/* ----------------------------------------------------------------------------
 * Function      : void App_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system for proper application execution.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void App_Initialize(void)
{
    /* Initialize the ISR Vector Table address in VTOR. */
    SCB->VTOR = (unsigned int)(&ISR_Vector_Table);

    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all interrupts and clear any pending interrupts. */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO13 to pause the program to make it easy to re-flash. */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Configure the current trim settings for VCC, VDDA. */
    ACS_VCC_CTRL->ICH_TRIM_BYTE  = VCC_ICHTRIM_16MA_BYTE;
    ACS_VDDA_CP_CTRL->PTRIM_BYTE = VDDA_PTRIM_16MA_BYTE;

    /* Start and configure VDDRF. */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up. */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Disable RF power amplifier. */
    ACS_VDDPA_CTRL->ENABLE_ALIAS = VDDPA_DISABLE_BITBAND;
    ACS_VDDPA_CTRL->VDDPA_SW_CTRL_ALIAS    = VDDPA_SW_VDDRF_BITBAND;

    /* Enable RF power switches. */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation. */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits. */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable the 48 MHz oscillator divider using the desired
     * pre-scale value. */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_3_BYTE;

    /* Wait until 48 MHz oscillator is started. */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to (divided 48 MHz) oscillator clock. */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Configure clock dividers. */
    CLK->DIV_CFG0 = (SLOWCLK_PRESCALE_8 |
                     BBCLK_PRESCALE_2   |
                     USRCLK_PRESCALE_1);

    CLK->DIV_CFG2 = (CPCLK_PRESCALE_8 | DCCLK_PRESCALE_4);

    /* Wake-up and apply clock to the BLE base-band interface. */
    BBIF->CTRL    = (BB_CLK_ENABLE | BBCLK_DIVIDER_8 | BB_WAKEUP);

    /* Set radio output power of RF */
    Sys_RFFE_SetTXPower(OUTPUT_POWER_DBM);

    /* Enable Flash overlay. */
    memcpy((uint8_t *)PRAM0_BASE, (uint8_t *)FLASH_MAIN_BASE, PRAM0_SIZE);
    memcpy((uint8_t *)PRAM1_BASE, (uint8_t *)(FLASH_MAIN_BASE + PRAM0_SIZE),
           PRAM1_SIZE);
    memcpy((uint8_t *)PRAM2_BASE,
           (uint8_t *)(FLASH_MAIN_BASE + PRAM0_SIZE + PRAM1_SIZE),
           PRAM2_SIZE);
    memcpy((uint8_t *)PRAM3_BASE,
           (uint8_t *)(FLASH_MAIN_BASE + PRAM0_SIZE + PRAM1_SIZE + PRAM2_SIZE),
           PRAM3_SIZE);
    SYSCTRL->FLASH_OVERLAY_CFG  = 0xf;

    /* Enable CM3 loop cache. */
    SYSCTRL->CSS_LOOP_CACHE_CFG = CSS_LOOP_CACHE_ENABLE;

#if (INPUT_INTRF == SPI_RX_CODED_INPUT)
    Initialize_Coded_SPI_Input_Type();
#endif    /* if (INPUT_INTRF == SPI_RX_CODED_INPUT) */
#if (OUTPUT_INTRF == SPI_TX_CODED_OUTPUT)
    Initialize_Coded_SPI_Output_Type();
#endif    /* if (OUTPUT_INTRF == SPI_TX_CODED_OUTPUT) */

    Sys_DIO_Config(DIO_SYNC_PULSE, DIO_MODE_GPIO_OUT_0);

    /* Unmask interrupts. */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}
