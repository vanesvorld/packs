/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.h
 * - Application header file
 * ----------------------------------------------------------------------------
 * $Revision: 1.12 $
 * $Date: 2019/12/27 20:29:57 $
 * ------------------------------------------------------------------------- */

#ifndef APP_H_
#define APP_H_

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>
#include <stdlib.h>
#include <rm_pkt.h>

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/

/*  (Nordic channel/2 - 1)
 *  {1, 7, 13, 19, 25, 31, 37}
 *  {2, 8, 14, 20, 26, 35, 38}
 *  {3, 9, 15, 21, 24, 33, 36}
 */
#define RM_HOPLIST                      { 3, 9, 15, 21, 24, 33, 36 }

#ifndef APP_RM_ROLE
#define APP_RM_ROLE                     RM_MASTER_ROLE
#endif

#define RM_LEFT                         0
#define RM_RIGHT                        1

#define APP_RM_AUDIO_CHANNEL            RM_RIGHT

/* Output power */
#define OUTPUT_POWER_DBM                0

#define NO_RX_INPUT                     0
#define SPI_RX_CODED_INPUT              1

#define NO_TX_OUTPUT                    2
#define SPI_TX_CODED_OUTPUT             3

#if (APP_RM_ROLE == RM_SLAVE_ROLE)
#define INPUT_INTRF                     NO_RX_INPUT
#define OUTPUT_INTRF                    SPI_TX_CODED_OUTPUT
#else    /* if (APP_RM_ROLE == RM_SLAVE_ROLE) */
#define INPUT_INTRF                     SPI_RX_CODED_INPUT
#define OUTPUT_INTRF                    NO_TX_OUTPUT
#endif    /* if (APP_RM_ROLE == RM_SLAVE_ROLE) */

#define APP_RM_DATA_REQUEST_TYPE        RM_APP_REQUEST

#define THREE_BLOCK_APPN(x, y, z)       x##y##z
#define DMA_IRQn(x)                     THREE_BLOCK_APPN(DMA, x, _IRQn)
#define TIMER_IRQn(x)                   THREE_BLOCK_APPN(TIMER, x, _IRQn)
#define DMA_IRQ_FUNC(x)                 THREE_BLOCK_APPN(DMA, x, _IRQHandler)
#define TIMER_IRQ_FUNC(x)               THREE_BLOCK_APPN(TIMER, x, _IRQHandler)

#define MEMCPY_DMA_NUM                  0
#define RX_DMA_NUM                      5
#define TX_DMA_NUM                      6

#define TX_DMA_SPI                      (DMA_DEST_SPI0 |            \
                                         DMA_TRANSFER_M_TO_P |      \
                                         DMA_LITTLE_ENDIAN |        \
                                         DMA_COMPLETE_INT_DISABLE | \
                                         DMA_COUNTER_INT_DISABLE |  \
                                         DMA_DEST_WORD_SIZE_8 |     \
                                         DMA_SRC_WORD_SIZE_32 |     \
                                         DMA_SRC_ADDR_INC |         \
                                         DMA_DEST_ADDR_STATIC |     \
                                         DMA_ADDR_LIN |             \
                                         DMA_DISABLE)

#if (INPUT_INTRF == SPI_RX_CODED_INPUT)
#define DMA_RX_CONFIG                   (DMA_LITTLE_ENDIAN |       \
                                         DMA_DISABLE |             \
                                         DMA_DISABLE_INT_DISABLE | \
                                         DMA_ERROR_INT_DISABLE |   \
                                         DMA_COMPLETE_INT_ENABLE | \
                                         DMA_COUNTER_INT_DISABLE | \
                                         DMA_START_INT_DISABLE |   \
                                         DMA_DEST_WORD_SIZE_32 |   \
                                         DMA_SRC_WORD_SIZE_8 |     \
                                         DMA_SRC_SPI0 |            \
                                         DMA_PRIORITY_0 |          \
                                         DMA_TRANSFER_P_TO_M |     \
                                         DMA_DEST_ADDR_INC |       \
                                         DMA_SRC_ADDR_STATIC |     \
                                         DMA_ADDR_CIRC)
#endif    /* if (INPUT_INTRF == SPI_RX_CODED_INPUT) */

#define AUDIO_FRAME_SIZE                60
#define CONCAT(x, y)                    x##y
#define DIO_SRC(x)                      CONCAT(DIO_SRC_DIO_, x)

/* DIO pin configuration for SPI */
#define SPI_SER_DI                      2
#define SPI_SER_DO                      1
#define SPI_CLK_DO                      3
#define SPI_CS_DO                       0

#define DIO_SYNC_PULSE                  8

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    13

#define DEBUG_DIO_FIRST                 15
#define DEBUG_DIO_SECOND                11
#define DEBUG_DIO_THIRD                 10

/* ----------------------------------------------------------------------------
 * Data types
 * --------------------------------------------------------------------------*/
struct app_env_tag
{
    struct rm_param_tag rm_param;
    uint8_t rm_link_status;
    uint16_t rm_lostLink_counter;
    uint16_t rm_unsuccessLink_counter;
    uint8_t audio_streaming;
};

typedef enum
{
    PKT_LEFT = 0,
    PKT_RIGHT = 1
} PacketSide;

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/
extern void *ISR_Vector_Table;
void App_Initialize(void);

void APP_RM_Init(uint8_t side);

void App_Process_Incoming_Data(uint8_t *data, uint8_t length);

/* ----------------------------------------------------------------------------
 * Global shared variables
 * --------------------------------------------------------------------------*/
extern uint8_t ear_side;
extern struct app_env_tag app_env;
#if (INPUT_INTRF == SPI_RX_CODED_INPUT)
int8_t spi_buf[AUDIO_FRAME_SIZE * 2];
#endif    /* if (INPUT_INTRF == SPI_RX_CODED_INPUT) */

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* APP_H */
