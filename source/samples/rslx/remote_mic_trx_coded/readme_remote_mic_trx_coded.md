Coded Audio Stream Broadcast Transmitter/ Receiver Custom Protocol Sample Application
================


NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample code demonstrates functionality of a transmitter/receiver that
transmits/receives audio data wirelessly using the Audio Stream Broadcast
Custom Protocol (remote microphone custom protocol). It showcases the RSL10
firmware implementation required for remote microphone or wireless auxiliary
audio input use cases. The transmitter section of this sample code illustrates
how the transmitter relays the encoded (coded) data it receives from
the Ezairo 7100 Evaluation and Development Board. The receiver section of this
sample code illustrates how the receiver sends the encoded data it receives
over the air to the Ezairo 7100 Evaluation and Development Board. It also
illustrates how to configure the relevant hardware peripheral blocks required
for audio input/output. More specifically, for the transmitter, the encoded
stereo audio input stream is received through the SPI bus from the Ezairo 7100
Evaluation and Development Board, and thereafter, transmitted wirelessly via
the remote microphone custom protocol. For the receiver, the single (left or
right) channel audio stream is received wirelessly via the remote microphone
custom protocol, and then directly sent through the SPI bus to the Ezairo 7100
Evaluation and Development Board, where the mono audio stream is decoded and
sent to the output driver.

**This sample project is structured as follows:**

The source code exists in a `code` folder, all application-related include
header files are in the `include` folder, and the `main()` function `app.c` is
located in the parent directory.

Code
----
    app_func.c      - Support functions and message handlers pertaining to
                      peripheral and audio processing hardware blocks
    app_init.c      - All initialization functions are called here, but the
                      implementations are in their respective C files
    rm_app.c        - Support functions and message handlers pertaining to the
                      remote microphone custom protocol

Include
-------
    app.h           - Overall application header file

The appropriate libraries and `include` files are loaded according to the 
build configuration selected.
    
Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development 
Board. 

For the transmitter, the encoded input audio stream sent from the Ezairo 7100
is sent to the RSL10 over the SPI bus. Ensure that the following pins are
connected between the two devices:

    ----------------------------------------------
    Pin Signal               | RSL10 | Ezairo 7100
    ----------------------------------------------
    SPI Chip Select          | DIO0  | DIO25     
    SPI MISO                 | DIO1  | DIO26
    SPI MOSI                 | DIO2  | DIO23
    SPI Clock                | DIO3  | DIO24
    Sampling Frequency Clock | DIO7  | DIO21
    PLL Sync                 | DIO8  | DIO19
    VDDO                     | VDDO  | VDDO3
    Ground                   | GND   | GND
    ----------------------------------------------
In this configuration, the Ezairo 7100 is the SPI Master and the RSL10 is the
SPI Slave.  Also, ensure that both VDDO2-SEL and VDDO3-SEL have jumpers on 
pins 1-2 on the Ezairo 7100 EDK.

For the receiver, the encoded audio stream received is sent to the Ezairo 7100
over the SPI bus. Ensure that the following pins are connected between the two
devices:

    ----------------------------------------------
    Pin Signal               | RSL10 | Ezairo 7100
    ----------------------------------------------
    SPI Chip Select          | DIO0  | DIO25
    SPI MOSI                 | DIO1  | DIO26
    SPI MISO                 | DIO2  | DIO15
    SPI Clock                | DIO3  | DIO24
    Sampling Frequency Clock | DIO7  | DIO21
    PLL Sync                 | DIO8  | DIO19
    VDDO                     | VDDO  | VDDO3
    Ground                   | GND   | GND
    ----------------------------------------------
In this configuration, the RSL10 is the SPI Master and the Ezairo 7100 is the
SPI Slave. As well, ensure that both VDDO2-SEL and VDDO3-SEL have jumpers on 
pins 1-2 on the Ezairo 7100.

In addition, note the following when setting up the various hardware 
configurations: 

- Ensure that no jumpers are placed on P9 on the RSL10 QFN Evaluation and
  Development Board for the configurations where VDDO is being supplied
  externally.
- On the Ezairo 7100 EDK, ensure that jumpers are placed on VBATOD-EN and on 
  pins   1-2 on the `VDDO*_SEL` of the `VDDO*` being provided to the RSL10 in 
  all configurations.
- DIO7 and DIO8 are mapped to the SCL and SDA pins respectively on the RSL10
  QFN Evaluation and Development Board.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.

Select the project configuration according to the required optimization level. 
Use **Debug** configuration for optimization level **None**, or **Release**
configuration for optimization level **More** or **O2**.

Verification
============
To verify that this application is functioning correctly, the transmitter�s 
input audio stream can be monitored on the receiver. In addition to ensuring 
that the hardware setup is as outlined in the Hardware Requirements section 
for the transmitter/receiver configurations, this sample code needs to be 
compiled and flashed onto the RSL10 Evaluation and Development Board with the
appropriate pre-processor definition settings, to ensure that the 
transmitter/receiver can function as expected. The corresponding Ezairo 7100 
firmware also needs to be flashed onto the Ezairo 7100 Evaluation and 
Development Boards. The Ezairo 7100 firmware required for the transmitter and
receiver configurations are provided in the `RSL10_Utility_Apps.zip` file.

For the transmitter, where the encoded input audio stream sent from the 
Ezairo 7100 is sent to the RSL10 over the SPI bus, compile and flash this
sample application with the `APP_RM_ROLE` and `OUTPUT_INTRF` defines as
follows:

    #define APP_RM_ROLE                     RM_MASTER_ROLE
    #define INPUT_INTRF                     SPI_RX_CODED_INPUT

In addition, program the Ezairo 7100 with the `RSL10_RemoteDongle` application
provided in the utility applications folder. AI1 and AI3 capture the left and
right audio channel data respectively on the Ezairo 7100 Evaluation and 
Development Board.

For the receiver, where the encoded audio stream received is sent to the 
Ezairo 7100 over the SPI bus, compile and flash this sample application with
the `APP_RM_ROLE` and `OUTPUT_INTRF` define as follows:

    #define APP_RM_ROLE                     RM_SLAVE_ROLE
    #define OUTPUT_INTRF                    SPI_TX_CODED_OUTPUT

In addition, program the Ezairo 7100 with the `RSL10_RM_HearingAid` 
application provided in the utility applications folder. The audio output can 
be monitored through the first RCA output channel (RCVR0 - with FILTEN0 pins 
1-2 and 3-4 shorted) on the Ezairo 7100 Evaluation and Development Board.

Note that the frequency channel hopping list (`RM_HOPLIST`), modulation index
(`app_env.rm_param.mod_idx`), and remote microphone custom protocol streaming
address (`app_env.rm_param.accessword`) in the receiver and transmitter RSL10
programs need to be matched in order to successfully transmit/receive audio 
via the remote microphone custom protocol.

Notes
=====
The RSL10 and/or Ezairo 7100 may be damaged if the corresponding
transmitter/receiver side programs do not match because of potential pin
direction conflicts.

Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO13 can be implemented with the following steps:

1.  Connect DIO13 to ground.
2.  Press the RESET button (this restarts the application, which 
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO13 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
