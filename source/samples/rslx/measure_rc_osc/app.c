/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - A simple application that measures and reports the RC oscillator frequency
 * ----------------------------------------------------------------------------
 * $Revision: 1.17 $
 * $Date: 2019/11/29 18:08:03 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>

/* ----------------------------------------------------------------------------
 * Function      : void Clock_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system to support the clock calibration,
 *                 consisting of the 48 MHz XTAL oscillator and RC oscillator.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Clock_Initialize(void)
{
    /* Prepare the 48 MHz crystal
     * Start and configure VDDRF */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_1_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to 48 MHz oscillator clock */
    Sys_Clocks_SystemClkConfig(SYSCLK_CLKSRC_RFCLK |
                               EXTCLK_PRESCALE_1   |
                               JTCK_PRESCALE_1);

    /* Start the RC oscillator */
    Sys_Clocks_Osc32kHz(RC_OSC_ENABLE | RC_OSC_NOM);

    /* Read the OSC_32K calibration trim data from NVR4 */
    unsigned int osc_calibration_value = 0;
    Sys_ReadNVR4(MANU_INFO_OSC_32K, 1, (unsigned int *)&osc_calibration_value);

    /* Use calibrated value for RC clock */
    if (osc_calibration_value != 0xFFFFFFFF)
    {
        ACS_RCOSC_CTRL->FTRIM_32K_BYTE = (uint8_t)(osc_calibration_value);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, configuring
 *                 the required DIOs and DIO interrupt,
 *                 updating SystemCoreClockUpdate and enabling interrupts.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Configure DIOs */
    Sys_DIO_Config(LED_DIO_NUM, DIO_MODE_GPIO_OUT_0);

    /* Initialize the clocking */
    Clock_Initialize();

    /* Initialize audio sink block */
    RC_OSC_Initialize();

    /* Initialize the UART and associated DMA */
    printf_init();

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}

/* ----------------------------------------------------------------------------
 * Function      : void main(void)
 * ----------------------------------------------------------------------------
 * Description   : Main application
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    unsigned int loop_cnt = 0;

    /* Initialize the system */
    Initialize();
    PRINTF("DEVICE INITIALIZED\n");

    while (true)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* If RC oscillator measurement is disabled */
        if (rc_osc_measurements.enabled == false)
        {
            /* If it is time to start a new RC oscillator frequency measurement
             * batch */
            if (loop_cnt == RC_OSC_MEASUREMENTS_LOOPS)
            {
                /* Enable RC oscillator measurement */
                rc_osc_measurements.enabled = true;

                /* Initialize parameters used for a new RC oscillator
                 * measurements */
                rc_osc_measurements.SYSCLK_periods     = 0;
                rc_osc_measurements.SYSCLK_periods_sum = 0;
                rc_osc_measurements.sample_cnt = 0;
                rc_osc_measurements.avg_freq   = 0;

                /* Start period counter to start the first measurement */
                AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = 1;

                /* Reset the loop counter */
                loop_cnt = 0;
            }
            else
            {
                /* Delay for 5ms */
                Sys_Delay_ProgramROM(5 * (SystemCoreClock / 1000));

                /* Increase the loop counter */
                loop_cnt++;
            }
        }
        /* Else (RC oscillator measurement is on going) and
         * if a measurement has just been completed */
        else if (AUDIOSINK_CTRL->PERIOD_CNT_STATUS_ALIAS == 0)
        {
            /* Accumulate SYSCLK period count value */
            rc_osc_measurements.SYSCLK_periods_sum +=
                rc_osc_measurements.SYSCLK_periods;

            /* Increase the measurement counter */
            rc_osc_measurements.sample_cnt++;

            /* If the RC oscillator measurement batch is completed */
            if (rc_osc_measurements.sample_cnt == RC_OSC_MEASUREMENTS_SAMPLES)
            {
                /* Calculate the average RC oscillator frequency value */
                rc_osc_measurements.avg_freq =
                    ((uint64_t)(RC_OSC_MEASUREMENTS_SAMPLES)*SystemCoreClock *
                     (AUDIOSINK->CFG + 1) +
                     (rc_osc_measurements.SYSCLK_periods_sum >> 1)) /
                    (rc_osc_measurements.SYSCLK_periods_sum);

                /* Disable RC oscillator measurement */
                rc_osc_measurements.enabled = false;

                /* Notify the results */
                Sys_GPIO_Toggle(LED_DIO_NUM);
                PRINTF("RC oscillator frequency (Hz):\t%d\r\n",
                        rc_osc_measurements.avg_freq);
            }

            /* Else: the RC oscillator measurement batch is not completed yet */
            else
            {
                /* Start period counter to start the next measurement */
                AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = 1;
            }
        }
    }
}
