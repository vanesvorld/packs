/* ----------------------------------------------------------------------------
* Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a ON
* Semiconductor). All Rights Reserved.
*
* This code is the property of ON Semiconductor and may not be redistributed
* in any form without prior written permission from ON Semiconductor.
* The terms of use and warranty for this code are covered by contractual
* agreements between ON Semiconductor and the licensee.
*
* This is Reusable Code.
*
* ----------------------------------------------------------------------------
* main.h
* - Header file for sample code
* ----------------------------------------------------------------------------
* $Revision: 1.24 $
* $Date: 2019/02/05 19:12:31 $
* -------------------------------------------------------------------------- */

#ifndef MAIN_H_
#define MAIN_H_

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/

#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/

/* Clear all reset flags in ACS_RESET_STATUS */
#define ACS_RESET_STATUS_FLAG_CLEAR     (uint32_t)(0x7F)

/* Clear all reset flags in DIG_RESET_STATUS */
#define DIG_RESET_STATUS_FLAG_CLEAR     (uint32_t)(0xF0)

/* Board calibration: voltage targets [x10mV] */
#define BG_TARGET                       (uint8_t)(75)
#define VDDRF_TARGET                    (uint8_t)(110)
#define VDDPA_TARGET                    (uint8_t)(130)
#define VDDC_TARGET                     (uint8_t)(92)
#define VDDM_TARGET                     (uint8_t)(100)
#define DCDC_TARGET                     (uint8_t)(120)

/* Board calibration: clock targets [Hz] */
#define RCOSC32K_TARGET                 (uint16_t)(32768)

/* Enable/disable buck converter
 * Options: VCC_BUCK_BITBAND or VCC_LDO_BITBAND */
#define VCC_BUCK_LDO_CTRL               VCC_BUCK_BITBAND

/* Define DCCLK prescaler configuration depending on VCC configuration */
#if (VCC_BUCK_LDO_CTRL == VCC_BUCK_BITBAND)
#define DCCLK_BYTE_VALUE DCCLK_PRESCALE_2_BYTE
#else    /* if (VCC_BUCK_LDO_CTRL == VCC_BUCK_BITBAND) */
#define DCCLK_BYTE_VALUE DCCLK_DISABLE_BYTE
#endif    /* if (VCC_BUCK_LDO_CTRL == VCC_BUCK_BITBAND) */

/* DIO number that is connected to LED of EVB */
#define LED_DIO                         6

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    12

/* The duration LED is on [s] */
#define LED_ON_DURATION                 0.1

/* Enable/disable wake-up by RTC
 * Options are:
 *     - 0: enabled
 *     - 1: disabled */
#define WAKEUP_RTC_ENABLE               1

#if WAKEUP_RTC_ENABLE == 1

/* Select the low power clock source for RTC
 * Options are:
 *     - RTC_CLK_SRC_XTAL32K: 32kHz crystal oscillator
 *     - RTC_CLK_SRC_RC_OSC:  32kHz RC oscillator */
#define RTC_CLK_SRC                     RTC_CLK_SRC_XTAL32K

/* Select RTC alarm time interval (in second)*/
#define RTC_ALARM_INTV_S                1.0

/* RTC counter start value
 * Notes: this is calculated respectively to the selected alarm interval */
#define RTC_CNT_START_VALUE             \
    ((uint32_t)(32768.0 * (RTC_ALARM_INTV_S)) << ACS_RTC_CFG_START_VALUE_Pos);

/* XTAL32K ITRIM value set point */
#define XTAL32K_ITRIM_VALUE             0x0F

/* XTAL32K ITRIM value set point */
#define XTAL32K_CLOAD_TRIM_VALUE        0x38

#endif    /* WAKEUP_RTC_ENABLE */

/* RAM instance(s) to be retained in sleep mode
 * 2 cases:
 *   - CASE 1: without memory retention (RAM_RETENTION_NUM = 0)
 *   - CASE 2: with memory retention    (RAM_RETENTION_NUM = 1, 2, ...)
 */
#define RAM_RETENTION_NUM               0

/* RAM retention:  1 x 8kB */
#if (RAM_RETENTION_NUM == 1)
#define MEM_CONFIGURATIONS              (PRAM0_POWER_ENABLE)

/* RAM retention:  2 x 8kB */
#elif (RAM_RETENTION_NUM == 2)
#define MEM_CONFIGURATIONS              (PRAM0_POWER_ENABLE | \
                                         PRAM1_POWER_ENABLE)

/* RAM retention:  3 x 8kB */
#elif (RAM_RETENTION_NUM == 3)
#define MEM_CONFIGURATIONS              (PRAM0_POWER_ENABLE | \
                                         PRAM1_POWER_ENABLE | \
                                         PRAM2_POWER_ENABLE)

/* RAM retention:  4 x 8kB */
#elif (RAM_RETENTION_NUM == 4)
#define MEM_CONFIGURATIONS              (PRAM0_POWER_ENABLE | \
                                         PRAM1_POWER_ENABLE | \
                                         PRAM2_POWER_ENABLE | \
                                         PRAM3_POWER_ENABLE)

/* RAM retention:  5 x 8kB */
#elif (RAM_RETENTION_NUM == 5)
#define MEM_CONFIGURATIONS              (PRAM0_POWER_ENABLE | \
                                         PRAM1_POWER_ENABLE | \
                                         PRAM2_POWER_ENABLE | \
                                         PRAM3_POWER_ENABLE | \
                                         DRAM0_POWER_ENABLE)

/* RAM retention:  6 x 8kB */
#elif (RAM_RETENTION_NUM == 6)
#define MEM_CONFIGURATIONS              (PRAM0_POWER_ENABLE | \
                                         PRAM1_POWER_ENABLE | \
                                         PRAM2_POWER_ENABLE | \
                                         PRAM3_POWER_ENABLE | \
                                         DRAM0_POWER_ENABLE | \
                                         DRAM1_POWER_ENABLE)

/* RAM retention:  7 x 8kB */
#elif (RAM_RETENTION_NUM == 7)
#define MEM_CONFIGURATIONS              (PRAM0_POWER_ENABLE | \
                                         PRAM1_POWER_ENABLE | \
                                         PRAM2_POWER_ENABLE | \
                                         PRAM3_POWER_ENABLE | \
                                         DRAM0_POWER_ENABLE | \
                                         DRAM1_POWER_ENABLE | \
                                         DRAM2_POWER_ENABLE)
#endif    /* if (RAM_RETENTION_NUM == 1) */

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/

void Main_Application(void);

void Sleep_Mode_Configure(struct sleep_mode_flash_env_tag *sleep_mode_env);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* MAIN_H_ */
