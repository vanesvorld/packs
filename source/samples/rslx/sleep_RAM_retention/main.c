/* ----------------------------------------------------------------------------
* Copyright (c) 2016-17 Semiconductor Components Industries, LLC (d/b/a ON
* Semiconductor). All Rights Reserved.
*
* This code is the property of ON Semiconductor and may not be redistributed
* in any form without prior written permission from ON Semiconductor.
* The terms of use and warranty for this code are covered by contractual
* agreements between ON Semiconductor and the licensee.
*
* This is Reusable Code.
*
* ----------------------------------------------------------------------------
* main.c
* - Sample code for sleep mode
* ----------------------------------------------------------------------------
* $Revision: 1.45 $
* $Date: 2019/06/17 12:48:41 $
* -------------------------------------------------------------------------- */

#include "main.h"

/* Parameters and configurations for the sleep mode */
struct sleep_mode_flash_env_tag sleep_mode_flash_env;

/* ----------------------------------------------------------------------------
 * Function      : void main(void)
 * ----------------------------------------------------------------------------
 * Description   : The main function
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all interrupts and clear any pending interrupts */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Configure and turn LED on */
    Sys_DIO_Config(LED_DIO, DIO_MODE_GPIO_OUT_1);

    /* Disable DIO4 and DIO5 to avoid current consumption on VDDO */
    Sys_DIO_Config(4, DIO_MODE_DISABLE | DIO_NO_PULL);
    Sys_DIO_Config(5, DIO_MODE_DISABLE | DIO_NO_PULL);

    /* Check reset source to see if wake-up from sleep */
    if ((DIG->RESET_STATUS == 0x1) && (ACS->RESET_STATUS == 0x0))
    {
        /* Wake-up from sleep
         * The XTAL has already been started by the ROM at this point */

        /* Lower drive strength (required when VDDO > 2.7)*/
        DIO->PAD_CFG = PAD_LOW_DRIVE;

        /* Turn off pad retention */
        ACS_WAKEUP_CTRL->PADS_RETENTION_EN_BYTE = PADS_RETENTION_DISABLE_BYTE;

        /* Disable RTC */
        ACS_RTC_CTRL->ENABLE_ALIAS = RTC_DISABLE_BITBAND;

        /* Enable 48 MHz oscillator divider at desired prescale value */
        RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_6_BYTE;

        /* Wait until 48 MHz oscillator is started */
        while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
               ANALOG_INFO_CLK_DIG_READY_BITBAND);

        /* Switch to (divided 48 MHz) oscillator clock */
        Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                                   EXTCLK_PRESCALE_1 |
                                   SYSCLK_CLKSRC_RFCLK);

        /* Configure clock dividers */
        CLK_DIV_CFG2->DCCLK_BYTE = DCCLK_BYTE_VALUE;
    }
    else
    {
        /* Power-on reset */

        /* Wait for 3 seconds to allow re-flashing after reset */
        Sys_Delay_ProgramROM(3 * SystemCoreClock);

        /* Calibrate voltages */
        Sys_Power_BandGapCalibratedConfig(BG_TARGET);
        Sys_Power_VDDRFCalibratedConfig(VDDRF_TARGET);
        Sys_Power_VDDPACalibratedConfig(VDDPA_TARGET);
        Sys_Power_VDDCCalibratedConfig(VDDC_TARGET);
        Sys_Power_VDDMCalibratedConfig(VDDM_TARGET);
        Sys_Power_DCDCCalibratedConfig(DCDC_TARGET);

        /* Enable/disable buck converter */
        ACS_VCC_CTRL->BUCK_ENABLE_ALIAS = VCC_BUCK_LDO_CTRL;

        /* Start 48 MHz XTAL oscillator */
        /* Enable VDDRF supply without changing trimming settings */
        ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
        ACS_VDDRF_CTRL->CLAMP_ALIAS = VDDRF_DISABLE_HIZ_BITBAND;

        /* Wait until VDDRF supply has powered up */
        while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

        /* Enable RF power switches */
        SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS = RF_POWER_ENABLE_BITBAND;

        /* Remove RF isolation */
        SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

        /* Start the 48 MHz oscillator without changing the other register
         * bits */
        RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                         XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

        /* Enable 48 MHz oscillator divider at desired prescale value */
        RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_6_BYTE;

        /* Wait until 48 MHz oscillator is started */
        while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
               ANALOG_INFO_CLK_DIG_READY_BITBAND);

        /* Switch to (divided 48 MHz) oscillator clock */
        Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                                   EXTCLK_PRESCALE_1 |
                                   SYSCLK_CLKSRC_RFCLK);

        /* Configure clock dividers */
        CLK_DIV_CFG2->DCCLK_BYTE = DCCLK_BYTE_VALUE;
    }

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    /* Disable and ignore clock detector,
     * set trim flag to avoid ROM reinitializing all registers */
    ACS_CLK_DET_CTRL->RESET_IGNORE_ALIAS = ACS_CLK_DET_RESET_DISABLE_BITBAND;
    ACS_CLK_DET_CTRL->ENABLE_ALIAS = ACS_CLK_DET_DISABLE_BITBAND;
    ACS_RCOSC_CTRL->FTRIM_FLAG_ALIAS = RC_OSC_CALIBRATED_BITBAND;

    /* Clear all reset flags */
    ACS->RESET_STATUS = ACS_RESET_STATUS_FLAG_CLEAR;
    DIG->RESET_STATUS = DIG_RESET_STATUS_FLAG_CLEAR;

    /* ------------------------------------------------------------------------
     * Application during awake period
     * ----------------------------------------------------------------------*/

    /* Perform required operations during awake period */
    Main_Application();

    /* --------------------------------------------------------------------- */

    /* Configure the sleep mode parameters */
    Sleep_Mode_Configure(&sleep_mode_flash_env);

    /* Go to sleep mode */
    Sys_PowerModes_Sleep_WakeupFromFlash(&sleep_mode_flash_env);
}

/* ----------------------------------------------------------------------------
 * Function      : void main_application(void)
 * ----------------------------------------------------------------------------
 * Description   : The main application, simply turns on the LED for a while
 *                 to indicate awake periods
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Main_Application(void)
{
    Sys_GPIO_Set_High(LED_DIO);

    /* Delay for LED_ON_DURATION seconds
     * The number of cycles is calculated as follows:
     * LED_ON_DURATION [s] * SystemCoreClock [cycle/s] */
    Sys_Delay_ProgramROM(LED_ON_DURATION * SystemCoreClock);
    Sys_Watchdog_Refresh();

    Sys_GPIO_Set_Low(LED_DIO);
}

/* ----------------------------------------------------------------------------
 * Function      : void Sleep_Mode_Configure(
 *                         struct sleep_mode_flash_env_tag *sleep_mode_env)
 * ----------------------------------------------------------------------------
 * Description   : Configure the sleep mode
 * Inputs        : None
 * Outputs       : sleep_mode_env   - Parameters and configurations
 *                                    for the sleep mode
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Sleep_Mode_Configure(
    struct sleep_mode_flash_env_tag *sleep_mode_flash_env)
{

/* If RTC is selected as a wake-up source */
#if WAKEUP_RTC_ENABLE == 1

    /* Configure RTC */
    ACS->RTC_CFG = RTC_CNT_START_VALUE;
    *((volatile uint8_t *) &ACS->RTC_CTRL) = (uint8_t)(RTC_CLK_SRC |
                                                       RTC_ALARM_ZERO);

    /* Reset and enable RTC */
    ACS_RTC_CTRL->RESET_ALIAS = RTC_RESET_BITBAND;
    ACS_RTC_CTRL->ENABLE_ALIAS = RTC_ENABLE_BITBAND;

    /* If RTC clock source is XTAL32K oscillator */
    if (RTC_CLK_SRC == RTC_CLK_SRC_XTAL32K)
    {
        /* - Enable XTAL32K oscillator amplitude control
         * - Set XTAL32K load capacitance
         * - Enable XTAL32K oscillator */
        ACS->XTAL32K_CTRL = \
            (XTAL32K_XIN_CAP_BYPASS_DISABLE                                |
             XTAL32K_AMPL_CTRL_ENABLE                                      |
             XTAL32K_NOT_FORCE_READY                                       |
             (XTAL32K_CLOAD_TRIM_VALUE << ACS_XTAL32K_CTRL_CLOAD_TRIM_Pos) |
             (XTAL32K_ITRIM_VALUE << ACS_XTAL32K_CTRL_ITRIM_Pos)           |
             XTAL32K_IBOOST_DISABLE                                        |
             XTAL32K_ENABLE);

       /* Wait for XTAL32K oscillator to be ready */
       while (ACS_XTAL32K_CTRL->READY_ALIAS != XTAL32K_OK_BITBAND);
    }

    /* Else if RTC clock source is RC32K oscillator */
    else if (RTC_CLK_SRC == RTC_CLK_SRC_RC_OSC)
    {
        /* Start RC32K oscillator */
        Sys_Clocks_Osc32kHz(RC_OSC_ENABLE | RC_OSC_NOM);

        /* Read the OSC_32K calibration trim data from NVR4 */
        unsigned int osc_calibration_value = 0;
        Sys_ReadNVR4(MANU_INFO_OSC_32K, 1, (unsigned
                                            int *)&osc_calibration_value);

        /* Use calibrated value for RC32K oscillator */
        if (osc_calibration_value != 0xFFFFFFFF)
        {
            ACS_RCOSC_CTRL->FTRIM_32K_BYTE = (uint8_t)(osc_calibration_value);
        }
    }

/* Else: RTC is not selected as a wake-up source */
#else

    /* Disable RTC */
    ACS_RTC_CTRL->ENABLE_ALIAS = RTC_DISABLE_BITBAND;

#endif /* WAKEUP_RTC_ENABLE */

    /* Disable VDDC retention regulator */
    sleep_mode_flash_env->VDDCRET_enable = VDDCRET_DISABLE_BITBAND;

    /* Disable VDDT retention regulator */
    sleep_mode_flash_env->VDDTRET_enable = VDDTRET_DISABLE_BITBAND;

    /* ------------------------------------------------------------------------
     * Configure memory retention
     * Either CASE 1 or CASE 2 is selected */

    /* CASE 1: without RAM retention */
#if (RAM_RETENTION_NUM == 0)

    /* Disable VDDM retention regulator */
    sleep_mode_flash_env->VDDMRET_enable = VDDMRET_DISABLE_BITBAND;

    /* CASE 2: with RAM retention */
#else    /* if (RAM_RETENTION_NUM == 0) */

    /* Enable VDDM retention regulator */
    sleep_mode_flash_env->VDDMRET_enable = VDDMRET_ENABLE_BITBAND;

    /* Select memory instance(s) to be retained */
    sleep_mode_flash_env->mem_power_cfg  = MEM_CONFIGURATIONS;
#endif    /* if (RAM_RETENTION_NUM == 0) */

    /* --------------------------------------------------------------------- */

    /* Set delay and wake-up sources, use
     *    WAKEUP_DELAY_[ 1 | 2 | 4 | ... | 128],
     *    WAKEUP_DCDC_OVERLOAD_[ENABLE | DISABLE],
     *    WAKEUP_WAKEUP_PAD_[RISING | FALLING],
     *    WAKEUP_DIO*_[RISING | FALLING],
     *    WAKEUP_DIO*_[ENABLE | DISABLE] */
    sleep_mode_flash_env->wakeup_cfg = WAKEUP_DELAY_32          |
                                       WAKEUP_WAKEUP_PAD_RISING |
                                       WAKEUP_DIO3_DISABLE      |
                                       WAKEUP_DIO2_DISABLE      |
                                       WAKEUP_DIO1_DISABLE      |
                                       WAKEUP_DIO0_DISABLE;

    /* Set wake-up control/status registers, use
     *    PADS_RETENTION_[ENABLE | DISABLE],
     *    BOOT_FLASH_APP_REBOOT_[ENABLE | DISABLE],
     *    BOOT_[CUSTOM | FLASH_XTAL_*],
     *    WAKEUP_DCDC_OVERLOAD_[SET | CLEAR],
     *    WAKEUP_PAD_EVENT_[SET | CLEAR],
     *    WAKEUP_RTC_ALARM_[SET | CLEAR],
     *    WAKEUP_BB_TIMER_[SET | CLEAR],
     *    WAKEUP_DIO3_EVENT_[SET | CLEAR],
     *    WAKEUP_DIO2_EVENT_[SET | CLEAR],
     *    WAKEUP_DIO1_EVENT_[SET | CLEAR],
     *    WAKEUP_DIO0_EVENT_[SET | CLEAR] */
    sleep_mode_flash_env->wakeup_ctrl = PADS_RETENTION_ENABLE         |
                                        BOOT_FLASH_APP_REBOOT_DISABLE |
                                        BOOT_FLASH_XTAL_DEFAULT_TRIM  |
                                        WAKEUP_DCDC_OVERLOAD_CLEAR    |
                                        WAKEUP_PAD_EVENT_CLEAR        |
                                        WAKEUP_RTC_ALARM_CLEAR        |
                                        WAKEUP_BB_TIMER_CLEAR         |
                                        WAKEUP_DIO3_EVENT_CLEAR       |
                                        WAKEUP_DIO2_EVENT_CLEAR       |
                                        WAKEUP_DIO1_EVENT_CLEAR       |
                                        WAKEUP_DIO0_EVENT_CLEAR;
}
