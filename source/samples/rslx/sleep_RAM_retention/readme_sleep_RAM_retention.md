Sleep and Wakeup Sample Code
============================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project demostrates a simple application that:

 1. Sets DIO6 high then low. On the RSL10 Evaluation and Development Board,
    this toggle briefly flashes LED1.

 2. Switches to Sleep Mode (the number of memory instances to be 
    retained during sleep mode can be configured).

 3. Wakes up and restarts execution from step 1 if one of the following 
    occurs:

       - RTC alarm
       - WAKEUP pad rising edge

Using the RTC alarm as a wake-up source can be enabled or disabled by defining
`WAKEUP_RTC_ENABLE` in the main header file. By default, `WAKEUP_RTC_ENABLE`
is set to one (i.e., wake-up by RTC alarm is enabled).
   
If wake-up by RTC alarm is enabled, the clock source for RTC can be selected 
by defining `RTC_CLK_SRC` in the main header file. By default, `RTC_CLK_SRC` 
is set to `RTC_CLK_SRC_XTAL32K` (i.e., 32kHz crystal oscillator). The RTC 
alarm time interval can be selected by defining `RTC_ALARM_INTV_S` 
(by default, it is set to one second).

The number of memory instances to be retained in Sleep Mode can be selected by
defining `RAM_RETENTION_NUM` in the main header file. By default,
`RAM_RETENTION_NUM` is set to zero (i.e., no memory instances are retained in
Sleep Mode).

**This sample project is structured as follows:**

The source code exists in a `code` folder, and application-related include
header files are in the `include` folder. 

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the
*Getting Started Guide* for your IDE for more information.

Verification
------------
To verify that this application is functioning correctly, the LED connected to
DIO6 on the RSL10 Evaluation and Development Board flashes every four seconds.
The LED also flashes whenever there is a rising edge on the WAKEUP pad or 
DIO0. 

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which 
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).