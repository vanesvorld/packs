/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_init.c
 * - Application initialization function
 * ----------------------------------------------------------------------------
 * $Revision: 1.31 $
 * $Date: 2019/08/29 15:34:25 $
 * ------------------------------------------------------------------------- */

#include <rsl10.h>
#include <app.h>
#include <dsp_pm_dm.h>

extern uint8_t *Dsp2CmBuff0dec;
/* ----------------------------------------------------------------------------
 * Function      : void Initialize_system(void)
 * ----------------------------------------------------------------------------
 * Description   : System initialization function (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize_system(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all interrupts and clear any pending interrupts */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Configure the current trim settings for VCC, VDDA */
    ACS_VCC_CTRL->ICH_TRIM_BYTE  = VCC_ICHTRIM_16MA;
    ACS_VDDA_CP_CTRL->PTRIM_BYTE = VDDA_PTRIM_16MA;

    /* Start 48 MHz XTAL oscillator */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    ACS_VDDPA_CTRL->ENABLE_ALIAS = VDDPA_DISABLE_BITBAND;
    ACS_VDDPA_CTRL->VDDPA_SW_CTRL_ALIAS    = VDDPA_SW_VDDRF_BITBAND;

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_3_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to (divided 48 MHz) oscillator clock */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Configure clock dividers */
    CLK->DIV_CFG0 = (SLOWCLK_PRESCALE_16 | BBCLK_PRESCALE_1 |
                     USRCLK_PRESCALE_1);
    CLK->DIV_CFG2 = (CPCLK_PRESCALE_8 | DCCLK_PRESCALE_8);

    BBIF->CTRL    = (BB_CLK_ENABLE | BBCLK_DIVIDER_16 | BB_WAKEUP);

#if (INPUT_INTRF != DMIC_RX_INPUT)

    /* LPDSP32 DIO configuration */
    Sys_LPDSP32_DIOJTAG(DIO_WEAK_PULL_DOWN, TDI_DIO, TMS_DIO, TCK_DIO, TDO_DIO);
#endif    /* if (INPUT_INTRF != DMIC_RX_INPUT) */

    SYSCTRL->DSS_CTRL = DSS_LPDSP32_PAUSE;

    Sys_Flash_Copy((uint32_t)&LPDSP32_Prog_40bit_PM[0], DSP_PRAM0_BASE,
                   MEM_PM_SIZE, COPY_TO_MEM_BITBAND);
    Sys_Flash_Copy((uint32_t)&LPDSP32_Data_low_DM[0], DSP_DRAM01_BASE,
                   MEM_DMA_SIZE, COPY_TO_MEM_BITBAND);
    Sys_Flash_Copy((uint32_t)&LPDSP32_Data_low_DM[MEM_DMA_SIZE + 3],
                   DSP_DRAM4_BASE, 8000, COPY_TO_MEM_BITBAND);
    Sys_Flash_Copy((uint32_t)&LPDSP32_Data_low_DM[((MEM_DMA_SIZE + 3) + 8001)],
                   (DSP_DRAM4_BASE + 0x1F41), (MEM_DMB_SIZE - 8000),
                   COPY_TO_MEM_BITBAND);

    while (FLASH_COPY_CTRL->BUSY_ALIAS != COPY_IDLE_BITBAND)
    {
        Sys_Watchdog_Refresh();
    }

    SYSCTRL->DSS_CTRL = DSS_LPDSP32_PAUSE;
    SYSCTRL->DSS_CTRL = DSS_RESET;
    int i = 0;
    while (i < 7000)
    {
        i++;
        Sys_Watchdog_Refresh();
    }
    SYSCTRL->DSS_CTRL = DSS_LPDSP32_RESUME;

    /* ------------------------- */
#if (INPUT_INTRF == SPI_RX_INPUT || OUTPUT_INTRF == SPI_TX_OUTPUT)

    /* Initialize SPI interface */
    Sys_SPI_DIOConfig(0, SPI0_SELECT_SLAVE,
                      DIO_LPF_DISABLE | DIO_WEAK_PULL_UP, SPI_CLK_DO, SPI_CS_DO,
                      SPI_SER_DI,
                      SPI_SER_DO);

    /* Configure the SPI0 interface */
    Sys_SPI_Config(0, SPI0_SELECT_SLAVE | SPI0_ENABLE |
                   SPI0_CLK_POLARITY_NORMAL | SPI0_CONTROLLER_DMA |
                   SPI0_MODE_SELECT_AUTO | SPI0_PRESCALE_16);
    Sys_SPI_TransferConfig(0, SPI0_START | SPI0_RW_DATA | SPI0_CS_1 |
                           SPI0_WORD_SIZE_16);
#endif    /* if (INPUT_INTRF == SPI_RX_INPUT || OUTPUT_INTRF == SPI_TX_OUTPUT) */

#if (INPUT_INTRF == PCM_RX_INPUT || OUTPUT_INTRF == PCM_TX_OUTPUT)
    Sys_PCM_ConfigClk(PCM_SELECT_SLAVE, DIO_WEAK_PULL_UP, PCM_CLK_DO,
                      PCM_FRAME_SYNC, PCM_SER_DI, PCM_SER_DO, DIO_MODE_INPUT);
    Sys_PCM_Config(PCM_CFG_RX);
    Sys_PCM_Enable();
#endif    /* if (INPUT_INTRF == PCM_RX_INPUT || OUTPUT_INTRF == PCM_TX_OUTPUT) */
    APP_CPInit();

    /* Set radio output power of RF */
    Sys_RFFE_SetTXPower(OUTPUT_POWER_DBM);
	
    /* Start period count */
    AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = 1;

    /* Enable ASRC error IRQ */
    Sys_ASRC_IntEnableConfig(INT_EBL_ASRC_IN_ERR | INT_EBL_ASRC_UPDATE_ERR);

    if (app_env.cp_param.master == CP_MASTER_ROLE)    /* TX */
    {
#if (INPUT_INTRF == SPI_RX_INPUT || INPUT_INTRF == PCM_RX_INPUT)

        /* Enable DMA for ASRC input */
        Sys_DMA_ChannelConfig(
            ASRC_IN_IDX,
            TX_DMA_ASRC_IN,
            SUBFRAME_LENGTH,
            0,
            (uint32_t)&asrc_in_buf[0],
            (uint32_t)&ASRC->IN
            );
        Sys_DMA_ClearChannelStatus(ASRC_IN_IDX);

        /* Enable DMA for ASRC output */
        Sys_DMA_ChannelConfig(
            ASRC_OUT_IDX,
            TX_DMA_ASRC_OUT,
            SUBFRAME_LENGTH,
            0,
            (uint32_t)&ASRC->OUT,
            (uint32_t)&asrc_out_buf[0]
            );
        Sys_DMA_ClearChannelStatus(ASRC_OUT_IDX);
        Sys_DMA_ChannelEnable(ASRC_OUT_IDX);

#if (INPUT_INTRF == PCM_RX_INPUT)
        Sys_DMA_ChannelConfig(
            RX_DMA_NUM,
            DMA_RX_CONFIG,
            4 * SUBFRAME_LENGTH,    /*because of left/ right channel*/
            2 * SUBFRAME_LENGTH,
            (uint32_t)&(PCM->RX_DATA),
            (uint32_t)&pcm_buf[0]);
        Sys_DMA_ClearChannelStatus(RX_DMA_NUM);
        Sys_DMA_ChannelEnable(RX_DMA_NUM);
#elif (INPUT_INTRF == SPI_RX_INPUT)
        while (SPI0_CTRL1->SPI0_CS_ALIAS == SPI0_CS_0_BITBAND)
        {
        }
        while (SPI0_CTRL1->SPI0_CS_ALIAS == SPI0_CS_1_BITBAND)
        {
        }
        Sys_DMA_ChannelConfig(
            RX_DMA_NUM,
            DMA_RX_CONFIG,
            SUBFRAME_LENGTH,
            0,
            (uint32_t)&(SPI0->RX_DATA),
            (uint32_t)&spi_buf[0]);
        Sys_DMA_ClearChannelStatus(RX_DMA_NUM);
        Sys_DMA_ChannelEnable(RX_DMA_NUM);
#endif    /* if (INPUT_INTRF == PCM_RX_INPUT) */
#endif    /* if (INPUT_INTRF == SPI_RX_INPUT || INPUT_INTRF == PCM_RX_INPUT) */

        /* Setup LPDSP */
        uint8_t *message = MEM_MESSAGE;
        *message = SUBFRAME_LENGTH;       /* Encoder Frame size */
        *(message + 1) = SUBFRAME_LENGTH;    /* Encoder block size */
        *(message + 4) = CODEC_MODE;    /* Codec Mode */
    }
    else    /* RX */
    {
        /* Configure DMA for ASRC input */
        Sys_DMA_ChannelConfig(
            ASRC_IN_IDX,
            RX_DMA_ASRC_IN,
            SUBFRAME_LENGTH,
            0,
            (uint32_t)&lpdsp_buf[0],
            (uint32_t)&ASRC->IN
            );
#if (OUTPUT_INTRF == SPI_TX_OUTPUT)

        /* Configure DMA for ASRC output */
        /* Need to sync with SPI transactions, look for positive edge of CS */
        while (SPI0_CTRL1->SPI0_CS_ALIAS == SPI0_CS_0_BITBAND)
        {
        }
        while (SPI0_CTRL1->SPI0_CS_ALIAS == SPI0_CS_1_BITBAND)
        {
        }
#endif    /* if (OUTPUT_INTRF == SPI_TX_OUTPUT) */
        Sys_DMA_ChannelConfig(
            ASRC_OUT_IDX,
            RX_DMA_ASRC_OUT,
            SUBFRAME_LENGTH,
            0,
            (uint32_t)&ASRC->OUT,
            (uint32_t)&rx_data_fifo[0]
            );
        Sys_DMA_ChannelEnable(ASRC_OUT_IDX);
#if (OUTPUT_INTRF == SPI_TX_OUTPUT)

        /* Configure DMA for SPI slave */
        Sys_DMA_ChannelConfig(
            TX_DMA_NUM,
            DMA_TX_CONFIG,
            SUBFRAME_LENGTH,
            0,
            (uint32_t)&spi_buf[0],
            (uint32_t)&(SPI0->TX_DATA)
            );
#elif (OUTPUT_INTRF == PCM_TX_OUTPUT)

        /* Configure DMA for PCM slave */
        Sys_DMA_ChannelConfig(
            TX_DMA_NUM,
            DMA_TX_CONFIG,
            2 * SUBFRAME_LENGTH,
            SUBFRAME_LENGTH,
            (uint32_t)&pcm_buf[0],
            (uint32_t)&(PCM->TX_DATA)
            );
#endif    /* if (OUTPUT_INTRF == SPI_TX_OUTPUT) */
        Sys_DMA_ChannelEnable(TX_DMA_NUM);

        /* Setup LPDSP */
        uint8_t *message = MEM_MESSAGE;
        *(message + 2) = SUBFRAME_LENGTH;    /* Decoder Frame size */
        *(message + 3) = SUBFRAME_LENGTH;    /* Decoder block size */
        *(message + 4) = CODEC_MODE;    /* Codec Mode */

        /*
         * Initialization for the DMA as a replacement of memcpy:
         * If you intend to use for short packet transfers
         * (less than 8 samples), it is recommended not changing source and
         * destination addresses.
         * Replacing DMA with memcpy should be studied case by case.
         * Even checking the DMA transfer complete for short packets will make
         * the replacement useless.
         */
        Sys_DMA_ChannelConfig(
            MEMCPY_DMA_NUM,
            DMA_MEMCPY_CONFIG,
            SUBFRAME_LENGTH,
            0,
            (uint32_t)Dsp2CmBuff0dec,
            (uint32_t)&lpdsp_buf[0]
            );
    }

    Sys_Audiosink_ResetCounters();
    Sys_Audiosink_InputClock(0, ((uint32_t)(SAMPL_CLK <<
                                            DIO_AUDIOSINK_SRC_CLK_Pos)));
    Sys_Audiosink_Config(AUDIO_SINK_PERIODS_1, 0, 0);

    NVIC_EnableIRQ(RF_TX_IRQn);
    NVIC_EnableIRQ(RF_RXSTOP_IRQn);
#if (INPUT_INTRF != DMIC_RX_INPUT)
    NVIC_EnableIRQ(AUDIOSINK_PHASE_IRQn);
    NVIC_EnableIRQ(AUDIOSINK_PERIOD_IRQn);
    NVIC_EnableIRQ(ASRC_ERROR_IRQn);

    /* DMA interrupts */
    NVIC_SetPriority(DMA_IRQn(ASRC_IN_IDX), 1);
    NVIC_EnableIRQ(DMA_IRQn(ASRC_IN_IDX));
    NVIC_SetPriority(DMA_IRQn(ASRC_OUT_IDX), 1);
    NVIC_EnableIRQ(DMA_IRQn(ASRC_OUT_IDX));
#endif    /* if (INPUT_INTRF != DMIC_RX_INPUT) */
    if (app_env.cp_param.master == CP_MASTER_ROLE)    /* TX */
    {
        NVIC_SetPriority(DSP0_IRQn, 1);
        NVIC_EnableIRQ(DSP0_IRQn);
#if (INPUT_INTRF == SPI_RX_INPUT || INPUT_INTRF == PCM_RX_INPUT)
        NVIC_SetPriority(DMA_IRQn(RX_DMA_NUM), 1);
        NVIC_EnableIRQ(DMA_IRQn(RX_DMA_NUM));
#elif (INPUT_INTRF == DMIC_RX_INPUT)
        Sys_Clocks_SystemClkPrescale1(AUDIOCLK_PRESCALE_5);

        Sys_Audio_DMICDIOConfig(
            DIO_WEAK_PULL_UP,
            DMIC_CLK_PIN,
            DMIC_DATA_PIN,
            DIO_MODE_AUDIOCLK
            );
        Sys_Audio_Set_Config(DMIC_AUDIO_CFG);
        Sys_Audio_Set_DMICConfig(DMIC_CFG, 0);

        AUDIO_CFG->DMIC0_DMA_REQ_EN_ALIAS = DMIC0_DMA_REQ_ENABLE_BITBAND;

        AUDIO->DMIC0_GAIN = 0xFFF;

        Sys_DMA_ChannelConfig(DMIC_DMA_NUM,
                              DMA_DMIC_CONFIG,
                              DIG_MIC_RATE * SUBFRAME_LENGTH,
                              0,
                              (uint32_t)&(AUDIO->DMIC_DATA),
                              (uint32_t)dmic_buffer_in);

        Sys_DMA_ClearChannelStatus(DMIC_DMA_NUM);
        Sys_DMA_ChannelEnable(DMIC_DMA_NUM);

        NVIC_ClearPendingIRQ(DMA_IRQn(DMIC_DMA_NUM));
        NVIC_EnableIRQ(DMA_IRQn(DMIC_DMA_NUM));
#endif    /* if (INPUT_INTRF == SPI_RX_INPUT || INPUT_INTRF == PCM_RX_INPUT) */
    }
    else    /* RX */
    {
        NVIC_SetPriority(DMA_IRQn(TX_DMA_NUM), 1);
        NVIC_EnableIRQ(DMA_IRQn(TX_DMA_NUM));
        NVIC_SetPriority(DSP1_IRQn, 1);
        NVIC_EnableIRQ(DSP1_IRQn);
    }

    /* Enable Flash overlay */
    memcpy((uint8_t *)PRAM0_BASE, (uint8_t *)FLASH_MAIN_BASE, PRAM0_SIZE);
    memcpy((uint8_t *)PRAM1_BASE, (uint8_t *)(FLASH_MAIN_BASE + PRAM0_SIZE),
           PRAM1_SIZE);
    memcpy((uint8_t *)PRAM2_BASE, (uint8_t *)(FLASH_MAIN_BASE + PRAM0_SIZE +
                                              PRAM1_SIZE), PRAM2_SIZE);
    memcpy((uint8_t *)PRAM3_BASE, (uint8_t *)(FLASH_MAIN_BASE + PRAM0_SIZE +
                                              PRAM1_SIZE + PRAM2_SIZE),
           PRAM3_SIZE);
    SYSCTRL->FLASH_OVERLAY_CFG  = 0xf;

    /* Enable CM3 loop cache */
    SYSCTRL->CSS_LOOP_CACHE_CFG = CSS_LOOP_CACHE_ENABLE;

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    Sys_DIO_Config(9, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(10, DIO_MODE_GPIO_OUT_0);

#if TEST
    Sys_DIO_Config(9, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(10, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(11, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(12, DIO_MODE_GPIO_OUT_0);
#endif    /* if TEST */
}
