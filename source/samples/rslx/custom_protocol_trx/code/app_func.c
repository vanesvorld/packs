/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_func.c
 * - Application functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.31 $
 * $Date: 2018/03/02 19:16:32 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include "queue.h"

void __attribute__ ((alias("port_rx_dma_isr"))) DMA_IRQ_FUNC(RX_DMA_NUM)(void);

void __attribute__ ((alias("asrc_in_dma_isr"))) DMA_IRQ_FUNC(ASRC_IN_IDX)(void);

void __attribute__ ((alias("asrc_out_dma_isr"))) DMA_IRQ_FUNC(ASRC_OUT_IDX)(void);

void __attribute__ ((alias("dspEnc_isr"))) DSP0_IRQHandler(void);

void __attribute__ ((alias("ascc_phase_isr"))) AUDIOSINK_PHASE_IRQHandler(void);

void __attribute__ ((alias("ascc_period_isr"))) AUDIOSINK_PERIOD_IRQHandler(void);

void __attribute__ ((alias("dspDec_isr"))) DSP1_IRQHandler(void);

void __attribute__ ((alias("port_tx_dma_isr"))) DMA_IRQ_FUNC(TX_DMA_NUM)(void);

#if (INPUT_INTRF == DMIC_RX_INPUT)
void __attribute__ ((alias("dmic_dma_isr"))) DMA_IRQ_FUNC(DMIC_DMA_NUM)(void);

#endif    /* if (INPUT_INTRF == DMIC_RX_INPUT) */

/* Global variables */
int16_t spi_buf[SUBFRAME_LENGTH];
uint32_t pcm_buf[2 * SUBFRAME_LENGTH_LEFT_AND_RIGHT];
#if (INPUT_INTRF == SPI_RX_INPUT || INPUT_INTRF == PCM_RX_INPUT)
int16_t asrc_in_buf[SUBFRAME_LENGTH];
#else    /* if (INPUT_INTRF == SPI_RX_INPUT || INPUT_INTRF == PCM_RX_INPUT) */
int16_t dmic_buffer_in[DIG_MIC_RATE * SUBFRAME_LENGTH];
#endif    /* if (INPUT_INTRF == SPI_RX_INPUT || INPUT_INTRF == PCM_RX_INPUT) */
int16_t asrc_out_buf[SUBFRAME_LENGTH];
uint8_t tx_data_fifo[TX_DATA_FIFO_LENGTH];
int16_t rx_data_fifo[RX_DATA_FIFO_LENGTH];
int16_t data_wr_idx = 0;
int16_t data_rd_idx = 0;
int16_t w2r   = 0;
int16_t r2w   = 0;
uint8_t error = 0;
uint8_t ptr_rst_cnt     = 0;
bool frame_decoded   = true;
bool flag_ascc_phase = false;
uint8_t frame_in[ENCODED_FRAME_LENGTH];
uint8_t frame_idx   = 0;
int16_t lpdsp_buf[SUBFRAME_LENGTH];
uint8_t prev_status = LINK_DISCONNECTED;
#if TEST
uint8_t tmp0[TMP_LEN];
uint8_t tmp1[TMP_LEN];
int tmp_idx = 0;
#endif    /* if TEST */

/* Local variables */
uint32_t cntr = 0;
int64_t Cr   = 0;
int64_t Ck   = 0;
int64_t asrc_out_cnt              = 0;
int64_t audio_sink_cnt            = 0;
int64_t avg_ck_outputcnt          = 0;
int64_t diff_ck_outputcnt         = 0;
int64_t audio_sink_period_cnt     = 0;
int64_t audio_sink_phase_cnt      = 0;
int64_t audio_sink_phase_cnt_prev = 0;
int64_t asrc_inc_carrier;
uint32_t asrc_cnt_prev;
uint32_t asrc_cnt_cnst             = 0;
bool lpdsp_rdy = true;
bool phase_cnt_missed          = false;
uint16_t sim_missed = 0;
uint8_t *Cm2DspBuff0enc            = MEM_CM2DSP_ADDR0_ENC;
uint8_t *Cm2DspBuff1enc            = MEM_CM2DSP_ADDR1_ENC;
uint8_t *Dsp2CmBuff0enc            = MEM_DSP2CM_ADDR0_ENC;
uint8_t *Dsp2CmBuff1enc            = MEM_DSP2CM_ADDR1_ENC;
uint8_t *Cm2DspBuff0dec            = MEM_CM2DSP_ADDR0_DEC;
uint8_t *Cm2DspBuff1dec            = MEM_CM2DSP_ADDR1_DEC;
uint8_t *Dsp2CmBuff0dec            = MEM_DSP2CM_ADDR0_DEC;
uint8_t *Dsp2CmBuff1dec            = MEM_DSP2CM_ADDR1_DEC;
int32_t phase_inc[100];
int32_t phase_cnt[100];
#if TEST
int32_t tmp2[100];
int32_t tmp3[100];
int j = 0;
#endif    /* if TEST */
struct queue_t queue_tx = {
    NULL, NULL
};

/* the number of shifts of ASRC registers for using fix point variables */
#define SHIFT_BIT           24
int16_t sin_signal[] = {
    0, 788, 1455, 1898, 2048, 1882, 1427, 751, -40,
    -826, -1483, -1913, -2047, -1866, -1397, -713, 81, 862, 1511, 1927,
    2046, 1849, 1367, 675, -121, -899, -1538, -1940, -2043, -1831,
    -1337, -637, 162, 935, 1565, 1953, 2040, 1813, 1306, 598, -202,
    -971, -1590, -1965, -2036, -1794, -1275, -559, 242, 1006, 1616,
    1976, 2031, 1774, 1243, 520, -282, -1041, -1640, -1986, -2026,
    -1753, -1210, -481, 322, 1076, 1664, 1995, 2019, 1732, 1177, 442,
    -362, -1110, -1687, -2004, -2012, -1710, -1144, -402, 402, 1144,
    1710, 2012, 2004, 1687, 1110, 362, -442, -1177, -1732, -2019, -1995,
    -1664, -1076, -322, 481, 1210, 1753, 2026, 1986, 1640, 1041, 282,
    -520, -1243, -1774, -2031, -1976, -1616, -1006, -242, 559, 1275,
    1794, 2036, 1965, 1590, 971, 202, -598, -1306, -1813, -2040, -1953,
    -1565, -935, -162, 637, 1337, 1831, 2043, 1940, 1538, 899, 121,
    -675, -1367, -1849, -2046, -1927, -1511, -862, -81, 713, 1397, 1866,
    2047, 1913, 1483, 826, 40, -751, -1427, -1882, -2048, -1898, -1455,
    -788, 0
};

#if DELAY_MEAS
int16_t sample_in[160] = {
    32760, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
};
int sample_in_idx = 0;
int16_t prev_buf;
#endif    /* if DELAY_MEAS */

#if (INPUT_INTRF == DMIC_RX_INPUT)
/* ----------------------------------------------------------------------------
 * Function      : void dmic_dma_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : DMIC interrupt handler (TX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void dmic_dma_isr(void)
{
    if (AUDIO_STATUS->DMIC0_OVERRUN_FLAG_ALIAS ==
        DMIC0_OVERRUN_DETECTED_BITBAND)
    {
        AUDIO_STATUS->DMIC0_OVERRUN_FLAG_CLEAR_ALIAS =
            DMIC0_OVERRUN_FLAG_CLEAR_BITBAND;
    }

#if TEST
    Sys_GPIO_Set_High(10);
    Sys_GPIO_Set_Low(10);
#endif    /* if TEST */

    if (Sys_DMA_Get_ChannelStatus(DMIC_DMA_NUM) & DMA_COMPLETE_INT_STATUS)
    {
        QueueInsert(&queue_tx,
                    (uint16_t *)&dmic_buffer_in[0]);
        Start_Enc_Lpdsp32_Channel();
    }
    else
    {
        error++;
    }

    /* Clear the DMIC DMA interrupt */
    Sys_DMA_ClearChannelStatus(DMIC_DMA_NUM);
}

#endif    /* if (INPUT_INTRF == DMIC_RX_INPUT) */
/* ----------------------------------------------------------------------------
 * Function      : void port_rx_dma_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : PCM/ SPI receive interrupt handler (TX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void port_rx_dma_isr(void)
{
    Sys_GPIO_Toggle(10);

    /* Send SPI data through ASRC */
#if DELAY_MEAS
    memcpy(&asrc_in_buf[0], &sample_in[sample_in_idx], SUBFRAME_LENGTH *
           sizeof(int16_t));
    if (sample_in_idx == 0)
    {
        Sys_GPIO_Set_High(10);
        Sys_GPIO_Set_Low(10);
    }
    sample_in_idx = (sample_in_idx + SUBFRAME_LENGTH) % 160;
#else    /* if DELAY_MEAS */
#if (INPUT_INTRF == SPI_RX_INPUT)
    memcpy(&asrc_in_buf[0], &spi_buf[0], SUBFRAME_LENGTH * sizeof(int16_t));
#elif (INPUT_INTRF == PCM_RX_INPUT)
    int i;

    /* Effectively using two PCM data buffers to prevent read/write conflicts
     * Packing ASRC buffer with data received from first part of the PCM RX
     * buffer */
    if ((Sys_DMA_Get_ChannelStatus(RX_DMA_NUM) & DMA_COUNTER_INT_STATUS) != 0)
    {
        if (pcm_buf[0] == 0 && pcm_buf[2] == 0 && pcm_buf[4] == 0)
        {
            for (i = 0; i < SUBFRAME_LENGTH; i++)
            {
                asrc_in_buf[i] = pcm_buf[(i << 1) + 1] >> 8;
            }
        }
        else
        {
            for (i = 0; i < SUBFRAME_LENGTH; i++)
            {
                asrc_in_buf[i] = pcm_buf[(i << 1)] >> 8;
            }
        }
    }
    /* Packing ASRC buffer with data received from second part of the PCM RX
     * buffer */
    else
    {
        if (pcm_buf[0] == 0 && pcm_buf[2] == 0 && pcm_buf[4] == 0)
        {
            for (i = 0; i < SUBFRAME_LENGTH; i++)
            {
                asrc_in_buf[i] = \
                    pcm_buf[(i << 1) + 1 + SUBFRAME_LENGTH_LEFT_AND_RIGHT] >> 8;
            }
        }
        else
        {
            for (i = 0; i < SUBFRAME_LENGTH; i++)
            {
                asrc_in_buf[i] = \
                    pcm_buf[(i << 1) + SUBFRAME_LENGTH_LEFT_AND_RIGHT] >> 8;
            }
        }
    }
#endif    /* if (INPUT_INTRF == SPI_RX_INPUT) */
#endif    /* if DELAY_MEAS */

    /* Re-enable ASRC input DMA and start ASRC */
    Sys_DMA_ChannelEnable(ASRC_IN_IDX);
    Sys_ASRC_StatusConfig(ASRC_ENABLE);
    Sys_DMA_ClearChannelStatus(RX_DMA_NUM);
}

/* ----------------------------------------------------------------------------
 * Function      : void asrc_in_dma_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : ASRC input DMA interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void asrc_in_dma_isr(void)
{
    if (app_env.cp_param.master == CP_MASTER_ROLE)    /* TX */
    {
        /* Stop ASRC */
        Sys_ASRC_StatusConfig(ASRC_DISABLE);

        /* Configure ASRC if ASCC phase interrupt has been handled */
        if (flag_ascc_phase)
        {
            AsrcFunc();
            flag_ascc_phase = false;
        }
    }
    else    /* RX */
    {
        /* Stop ASRC if complete frame has been handled */
        Sys_ASRC_StatusConfig(ASRC_DISABLE);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void asrc_out_dma_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : ASRC output DMA interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void asrc_out_dma_isr(void)
{
    if (app_env.cp_param.master == CP_MASTER_ROLE)    /* TX */
    {
        if (Sys_DMA_Get_ChannelStatus(ASRC_OUT_IDX) & DMA_COMPLETE_INT_STATUS)
        {
            /* Send ASRC output to LDPSP for encoding */
            QueueInsert(&queue_tx,
                        (uint16_t *)&asrc_out_buf[0]);
            Start_Enc_Lpdsp32_Channel();
        }
        else
        {
            error++;
        }
    }
    else    /* RX */
    {
        /* Update write pointer and DMA destination */
        data_wr_idx = (data_wr_idx + SUBFRAME_LENGTH) % RX_DATA_FIFO_LENGTH;
        Sys_DMA_Set_ChannelDestAddress(ASRC_OUT_IDX,
                                       (uint32_t)&rx_data_fifo[data_wr_idx]);
    }

    /* Re-enable DMA for ASRC output */
    Sys_DMA_ClearChannelStatus(ASRC_OUT_IDX);
    Sys_DMA_ChannelEnable(ASRC_OUT_IDX);
}

/* ----------------------------------------------------------------------------
 * Function      : void dspEnc_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : LPDSP encoder interrupt handler (TX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void dspEnc_isr(void)
{
    /* Store LPDSP output */
    StoreDspEncData(Dsp2CmBuff0enc);
}

/* ----------------------------------------------------------------------------
 * Function      : void StoreDspEncData(void)
 * ----------------------------------------------------------------------------
 * Description   : Store encoded data in memory
 * Inputs        : src_addr: Address of encoded data source
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void StoreDspEncData(uint8_t *src_addr)
{
    lpdsp_rdy = true;

    Start_Enc_Lpdsp32_Channel();

    data_wr_idx = (data_wr_idx +
                   ENCODED_SUBFRAME_LENGTH) % TX_DATA_FIFO_LENGTH;

    memcpy(&tx_data_fifo[data_wr_idx],
           src_addr,
           ENCODED_SUBFRAME_LENGTH * sizeof(uint8_t));
}

/* ----------------------------------------------------------------------------
 * Function      : void Start_Enc_Lpdsp32_Channel (void)
 * ----------------------------------------------------------------------------
 * Description   : If the LPDSP32 is not busy look at the queues and send it to
 *                 to LPDSP32 to encode
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Start_Enc_Lpdsp32_Channel(void)
{
    if (lpdsp_rdy)
    {
        uint16_t *temp = QueueFront(&queue_tx);

        /* Check if there is a packet in the queue send it to the encoder */
        if (temp != NULL)
        {
            start_lpdsp((uint32_t)temp);
            QueueFree(&queue_tx);
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void start_lpdsp(uint32_t src_addr)
 * ----------------------------------------------------------------------------
 * Description   : Issue LPDSP start command (TX/RX)
 * Inputs        : src_addr - source address of LPDSP input
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void start_lpdsp(uint32_t src_addr)
{
    if (app_env.cp_param.master == CP_MASTER_ROLE)    /* TX */
    {
        lpdsp_rdy = false;
        memcpy(Cm2DspBuff0enc, (uint8_t *)src_addr,
               SUBFRAME_LENGTH * sizeof(uint16_t));

        /* memcpy(Cm2DspBuff0enc, (uint8_t *) &sin_signal[cntr],
         *    SUBFRAME_LENGTH * sizeof(uint16_t));
         * cntr = (cntr + SUBFRAME_LENGTH) % 160; */
        SYSCTRL_DSS_CMD->DSS_CMD_0_ALIAS = DSS_CMD_0_BITBAND;
    }
    else    /* RX */
    {
        memcpy(Cm2DspBuff0dec, (uint8_t *)src_addr,
               ENCODED_SUBFRAME_LENGTH * sizeof(uint8_t));
        SYSCTRL_DSS_CMD->DSS_CMD_1_ALIAS = DSS_CMD_1_BITBAND;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void ascc_phase_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : ASCC phase interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void ascc_phase_isr(void)
{
    if (app_env.cp_param.master == CP_MASTER_ROLE)    /* TX */
    {
        /* Get ASRC output count */
        asrc_out_cnt = Sys_ASRC_OutputCount();
        Sys_ASRC_ResetOutputCount();

        /* Get audio sink phase count */
        audio_sink_phase_cnt = Sys_Audiosink_PhaseCounter();

        /* Get audio sink count */
        audio_sink_cnt  = Sys_Audiosink_Counter() << SHIFT_BIT;
        AUDIOSINK_CTRL->CNT_RESET_ALIAS = CNT_RESET_BITBAND;
        audio_sink_cnt +=
            ((((audio_sink_phase_cnt_prev - audio_sink_phase_cnt))
              << SHIFT_BIT) / audio_sink_period_cnt);

        /* store audio sink count phase for the next time */
        audio_sink_phase_cnt_prev = audio_sink_phase_cnt;

        AUDIOSINK->PHASE_CNT = 0;
        AUDIOSINK_CTRL->PHASE_CNT_START_ALIAS  = PHASE_CNT_START_BITBAND;
        AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = PERIOD_CNT_START_BITBAND;

        /* Only configure ASRC when CP link is well established */
        if ((app_env.cp_link_status == LINK_ESTABLISHED) &&
            (prev_status == LINK_ESTABLISHED))
        {
            flag_ascc_phase = true;
        }
    }
    else    /* RX */
    {
        sim_missed = (sim_missed + 1) % 30;
        Sys_GPIO_Toggle(0);
        if (AUDIOSINK_CTRL->PHASE_CNT_MISSED_STATUS_ALIAS    /*sim_missed == 0*/)
        {
            Sys_GPIO_Toggle(0);
            phase_cnt_missed = true;
        }
        else
        {
            Sys_Timers_Stop(SELECT_TIMER2);
            Sys_Timer_Set_Control(2, TIMER_SHOT_MODE | TIMER_PRESCALE_1 |
                                  TIMER_SLOWCLK_DIV2 |
                                  ((app_env.cp_param.render_time / 2) - 1));
            Sys_Timers_Start(SELECT_TIMER2);

            /* Get audio sink phase count */
            audio_sink_phase_cnt = Sys_Audiosink_PhaseCounter();
            if (!phase_cnt_missed)
            {
                /* Get audio sink count */
                audio_sink_cnt  = Sys_Audiosink_Counter() << SHIFT_BIT;
                audio_sink_cnt +=
                    ((((audio_sink_phase_cnt_prev - audio_sink_phase_cnt))
                      << SHIFT_BIT) / audio_sink_period_cnt);
            }

            /* store audio sink count phase for the next time */
            audio_sink_phase_cnt_prev = audio_sink_phase_cnt;
            phase_cnt_missed = false;
        }
        flag_ascc_phase = true;
        AUDIOSINK_CTRL->CNT_RESET_ALIAS = CNT_RESET_BITBAND;
        AUDIOSINK->PHASE_CNT = 0;
        AUDIOSINK_CTRL->PHASE_CNT_START_ALIAS = PHASE_CNT_START_BITBAND;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void ascc_period_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : ASCC period interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void ascc_period_isr(void)
{
    audio_sink_period_cnt = Sys_Audiosink_PeriodCounter() / (AUDIOSINK->CFG +
                                                             1);
    AUDIOSINK->PERIOD_CNT = 0;
    AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = PERIOD_CNT_START_BITBAND;
}

/* ----------------------------------------------------------------------------
 * Function      : void AsrcFunc(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure ASRC (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void AsrcFunc(void)
{
    if (app_env.cp_param.master == CP_MASTER_ROLE)    /* TX */
    {
        diff_ck_outputcnt = Cr - (asrc_out_cnt << SHIFT_BIT);
        avg_ck_outputcnt  = ((diff_ck_outputcnt - avg_ck_outputcnt) >> 10) +
                            avg_ck_outputcnt;

        /* Average over Cr */
        Cr = ((audio_sink_cnt - Cr) >> 5) + Cr;
        Ck = FRAME_LENGTH << SHIFT_BIT;

        /* Update ASRC */
        if ((Cr <= (FRAME_LENGTH - ASRC_CFG_THR) << SHIFT_BIT) ||
            (Cr >= (FRAME_LENGTH + ASRC_CFG_THR) << SHIFT_BIT) ||
            (ptr_rst_cnt > PTR_RST_THR))
        {
            Cr = FRAME_LENGTH << SHIFT_BIT;
            avg_ck_outputcnt = 0;
            ptr_rst_cnt = 0;
        }
        else if (ptr_rst_cnt > 0)
        {
            ptr_rst_cnt--;
        }

        /* Configure ASRC base on new Cr Ck */
        asrc_inc_carrier  = ((((Cr - Ck) << 29) / Ck) << 0) -
                            (avg_ck_outputcnt >> SHIFT_BIT);
        asrc_inc_carrier &= 0xFFFFFFFF;
        Sys_ASRC_Config(asrc_inc_carrier, LOW_DELAY | ASRC_DEC_MODE1);
#if TEST
        tmp2[j] = Cr;
        tmp3[j] = avg_ck_outputcnt;
        j = (j + 1) % 100;
#endif    /* if TEST */
    }
    else    /* RX */
    {
        Sys_GPIO_Set_High(1);

        /* Get ASRC output count */
        asrc_out_cnt = Sys_ASRC_OutputCount();
        Sys_ASRC_ResetOutputCount();

        diff_ck_outputcnt = Ck - (asrc_out_cnt << SHIFT_BIT);
        avg_ck_outputcnt  = ((diff_ck_outputcnt - avg_ck_outputcnt) >> 10) +
                            avg_ck_outputcnt;

        Cr = FRAME_LENGTH << SHIFT_BIT;

        /* Average over Ck */
        Ck = ((audio_sink_cnt - Ck) >> 5) + Ck;

        if ((Ck <= (FRAME_LENGTH - ASRC_CFG_THR) << SHIFT_BIT) ||
            (Ck >= (FRAME_LENGTH + ASRC_CFG_THR) << SHIFT_BIT) ||
            (ptr_rst_cnt > PTR_RST_THR))
        {
            Ck = FRAME_LENGTH << SHIFT_BIT;
            avg_ck_outputcnt = 0;
            ptr_rst_cnt = 0;
        }
        else if (ptr_rst_cnt > 0)
        {
            ptr_rst_cnt--;
        }

        if (asrc_cnt_prev == ASRC->PHASE_CNT)
        {
            asrc_cnt_cnst++;
            if (asrc_cnt_cnst >= 20)
            {
                Sys_ASRC_Reset();
                asrc_cnt_cnst = 0;
            }
        }
        else
        {
            asrc_cnt_cnst = 0;
        }

        /* Configure ASRC base on new Cr Ck */
        asrc_inc_carrier  = ((((Cr - Ck) << 29) / Ck) << 0) -
                            (avg_ck_outputcnt >> SHIFT_BIT);
        asrc_inc_carrier &= 0xFFFFFFFF;
        Sys_ASRC_Config(asrc_inc_carrier, LOW_DELAY | ASRC_DEC_MODE1);
        asrc_cnt_prev     = ASRC->PHASE_CNT;
        Sys_GPIO_Set_Low(1);
#if TEST
        tmp2[j] = Ck;
        tmp3[j] = audio_sink_cnt;
        phase_inc[j] = ASRC->PHASE_INC;
        phase_cnt[j] = ASRC->PHASE_CNT;
        j = (j + 1) % 100;
#endif    /* if TEST */
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void dspDec_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : LPDSP decoder interrupt handler (RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void dspDec_isr(void)
{
    while (ASRC_CTRL->ASRC_PROC_STATUS_ALIAS == ASRC_BUSY_BITBAND);

    if (flag_ascc_phase)
    {
        AsrcFunc();
        flag_ascc_phase = false;
    }

    /* Store LPDSP output */
    /* Uncomment these lines if you want to use memcpy instead of DMA */

    /*memcpy(&lpdsp_buf[0], Dsp2CmBuff0dec, SUBFRAME_LENGTH*sizeof(int16_t)); */

    /*
     * Enabling the DMA
     * assumption: next time here we are sure that the previous block is
     * copied. we do not check anything in ISR as it takes more cycle than
     * memcpy
     */
    Sys_DMA_ChannelEnable(MEMCPY_DMA_NUM);

    /* Uncomment these lines if you want to load a sin wave on the RX side */

    /* memcpy(&lpdsp_buf[0], &sin_signal[cntr], SUBFRAME_LENGTH*sizeof(int16_t));
     * cntr = (cntr + SUBFRAME_LENGTH) % 160; */

    /* Continue transfer to LPDSP if not complete otherwise set flag */
    if (frame_idx < ENCODED_FRAME_LENGTH)
    {
        start_lpdsp((uint32_t)&frame_in[frame_idx]);
        frame_idx += ENCODED_SUBFRAME_LENGTH;
    }
    else
    {
        frame_decoded = true;
    }
    Sys_DMA_ClearChannelStatus(ASRC_IN_IDX);

    /* Re-enable DMA for ASRC input */
    Sys_DMA_ChannelEnable(ASRC_IN_IDX);
    Sys_ASRC_StatusConfig(ASRC_ENABLE);
}

/* ----------------------------------------------------------------------------
 * Function      : void port_tx_dma_isr(void)
 * ----------------------------------------------------------------------------
 * Description   : PCM/ SPI transmit interrupt handler (RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void port_tx_dma_isr(void)
{
#if (OUTPUT_INTRF == SPI_TX_OUTPUT)
    Sys_DMA_ClearChannelStatus(TX_DMA_NUM);
#endif    /* if (OUTPUT_INTRF == SPI_TX_OUTPUT) */

#if TEST
    tmp0[tmp_idx] = data_wr_idx;
    tmp1[tmp_idx] = data_rd_idx;
    tmp_idx = (tmp_idx + 1) % TMP_LEN;
#endif    /* if TEST */

    /* Copy data to SPI buffer and set read pointers based on link status */
    if (app_env.cp_link_status == LINK_ESTABLISHED)
    {
        w2r = data_wr_idx - data_rd_idx;
        if (w2r < 0)
        {
            w2r += RX_DATA_FIFO_LENGTH;
        }
        r2w = RX_DATA_FIFO_LENGTH - w2r;
        if ((w2r < RX_FIFO_W2R_THR) || (r2w < RX_FIFO_R2W_THR) ||
            (prev_status != LINK_ESTABLISHED))
        {
            /* (Re)initialize read pointer when link newly established */
            data_rd_idx = data_wr_idx - RX_FIFO_RWPTR_INIT;
            if (data_rd_idx < 0)
            {
                data_rd_idx += RX_DATA_FIFO_LENGTH;
            }
            data_rd_idx -= (data_rd_idx % SUBFRAME_LENGTH);
            ptr_rst_cnt += ((w2r < RX_FIFO_W2R_THR) || (r2w < RX_FIFO_R2W_THR));

            /* if ((w2r < RX_FIFO_W2R_THR) || (r2w < RX_FIFO_R2W_THR))
             * Sys_GPIO_Toggle(1); */
#if TEST && !DELAY_MEAS

            /*Sys_GPIO_Toggle(10); */
#endif    /* if TEST && !DELAY_MEAS */
        }

        /* Send ASRC output via port */
#if (OUTPUT_INTRF == SPI_TX_OUTPUT)
        Sys_DMA_Set_ChannelSourceAddress(TX_DMA_NUM,
                                         (uint32_t)&rx_data_fifo[data_rd_idx]);
        data_rd_idx = (data_rd_idx + SUBFRAME_LENGTH) % RX_DATA_FIFO_LENGTH;
#elif (OUTPUT_INTRF == PCM_TX_OUTPUT)
        int i;
        if (0x1 & (Sys_DMA_Get_ChannelStatus(TX_DMA_NUM) >>
                   DMA_STATUS_COUNTER_INT_STATUS_Pos))
        {
            Sys_DMA_ClearChannelStatus(TX_DMA_NUM);
            for (i = 0; i < SUBFRAME_LENGTH / 2; i++)
            {
                pcm_buf[2 * i] =
                    rx_data_fifo[(data_rd_idx + i + SUBFRAME_LENGTH) %
                                 RX_DATA_FIFO_LENGTH] << 8;
            }
            data_rd_idx = (data_rd_idx + SUBFRAME_LENGTH) % RX_DATA_FIFO_LENGTH;
        }
        else
        {
            Sys_DMA_ClearChannelStatus(TX_DMA_NUM);
            for (i = SUBFRAME_LENGTH / 2; i < SUBFRAME_LENGTH; i++)
            {
                pcm_buf[2 * i] =
                    rx_data_fifo[(data_rd_idx + i) % RX_DATA_FIFO_LENGTH] << 8;
            }
        }
#endif    /* if (OUTPUT_INTRF == SPI_TX_OUTPUT) */
    }
    else
    {
        /* Link not connected, send 0's */
#if (OUTPUT_INTRF == SPI_TX_OUTPUT)
        memset(&spi_buf[0], 0, SUBFRAME_LENGTH * sizeof(int16_t));
        Sys_DMA_Set_ChannelSourceAddress(TX_DMA_NUM, (uint32_t)&spi_buf[0]);
#endif    /* if (OUTPUT_INTRF == SPI_TX_OUTPUT) */
    }
#if DELAY_MEAS
    if (spi_buf[0] > DELAY_MEAS_THR || spi_buf[1] > DELAY_MEAS_THR ||
        spi_buf[2] > DELAY_MEAS_THR || spi_buf[3] > DELAY_MEAS_THR)
    {
        Sys_GPIO_Set_High(10);
        Sys_GPIO_Set_Low(10);
    }
#endif    /* if DELAY_MEAS */

    /* Store link status and re-enable DMA for SPI transmit */
    prev_status = app_env.cp_link_status;

#if (OUTPUT_INTRF == SPI_TX_OUTPUT)
    Sys_DMA_ChannelEnable(TX_DMA_NUM);
#endif    /* if (OUTPUT_INTRF == SPI_TX_OUTPUT) */
}

/* ----------------------------------------------------------------------------
 * Function      : void ASRC_ERROR_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : ASRC error interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void ASRC_ERROR_IRQHandler(void)
{
    error++;
    ASRC_CTRL->ASRC_UPDATE_ERR_CLR_ALIAS = CLR_ASRC_UPDATE_ERR_BITBAND;
    ASRC_CTRL->ASRC_IN_ERR_CLR_ALIAS     = CLR_ASRC_IN_ERR_BITBAND;
}
