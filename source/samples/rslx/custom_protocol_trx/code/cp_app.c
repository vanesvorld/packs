/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * cp_app.c
 * - Custom protocol application functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.20 $
 * $Date: 2019/12/27 21:07:24 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include "rsl10.h"
#include <printf.h>

uint8_t tmp;
struct app_env_tag app_env;
uint8_t frame_in_buf[ENCODED_FRAME_LENGTH];

/* ----------------------------------------------------------------------------
 * Function      : void APP_CPInit(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize custom protocol application
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void APP_CPInit(void)
{
    struct cp_callback callback;

    uint8_t temp1[4]  = CP_SLOWLIST_FIRST;
    uint8_t temp2[4]  = CP_SLOWLIST_SECOND;
    uint8_t temp3[40] = CP_FASTLIST_FIRST;
    uint8_t temp4[40] = CP_FASTLIST_SECOND;

    app_env.cp_link_status                 = LINK_DISCONNECTED;
    app_env.cp_lostLink_counter            = 0;
    app_env.cp_unsuccessLink_cunter        = 0;
    app_env.audio_streaming                = 0;

    app_env.cp_param.bidirectional         = CP_MONODIRECTION;
    app_env.cp_param.master                = CP_ROLE;
    app_env.cp_param.interval_time         = 2000;    /* in us */
    app_env.cp_param.audio_rate            = 64;    /* in Kbps */
    app_env.cp_param.radio_rate            = 2000;    /* in Kbps */
    app_env.cp_param.ifs = 100;    /* in us */
    app_env.cp_param.scan_time             = 6500;
    app_env.cp_param.preamble              = 0x55;
    app_env.cp_param.accessword            = ~(0x8E89BED6);
    app_env.cp_param.render_time           = 650;

    app_env.cp_param.pktLostLowThrshld     = 10;
    app_env.cp_param.pktLostHighThrshld    = 400;
    app_env.cp_param.pktLostLowThrshldSlow = 1;

    app_env.cp_param.numSlowHop            = 3;
    app_env.cp_param.numFastHop            = 30;
    app_env.cp_param.maxClustNum           = 3;

    memcpy(app_env.cp_param.slowHopList_first, temp1, 4);
    memcpy(app_env.cp_param.slowHopList_second, temp2, 4);
    memcpy(app_env.cp_param.fastHopList_first, temp3, 40);
    memcpy(app_env.cp_param.fastHopList_second, temp4, 40);

    callback.trx_event     = CP_Callback_TRX;
    callback.status_update = CP_Callback_StatusUpdate;

    CP_Configure(&app_env.cp_param, callback);

    cp_env.status_update(LINK_DISCONNECTED);

    CP_Enable(500);
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t CP_Callback_TRX(uint8_t type, uint8_t *length,
 *                 uint8_t *ptr)
 * ----------------------------------------------------------------------------
 * Description   : Custom protocol transmit/receive call back function
 * Inputs        : - type   - Event type
 *                 - length - Pointer to data length
 *                 - ptr    - Pointer to data
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint8_t CP_Callback_TRX(uint8_t type, uint8_t *length, uint8_t *ptr)
{
    switch (type)
    {
        case TX_START:
        {
            /* set ptr to the app buffer to be sent (don't memcpy), set length
             * */
            *length = ((app_env.cp_param.audio_rate *
                        app_env.cp_param.interval_time) / 8000);

            if (app_env.cp_link_status == LINK_ESTABLISHED)
            {
                w2r = data_wr_idx - data_rd_idx;
                if (w2r < 0)
                {
                    w2r += TX_DATA_FIFO_LENGTH;
                }
                r2w = TX_DATA_FIFO_LENGTH - w2r;
                if ((w2r < TX_FIFO_W2R_THR) || (r2w < TX_FIFO_R2W_THR) ||
                    (prev_status != LINK_ESTABLISHED))
                {
                    /* Reinitialize read pointer based on write pointer */
                    data_rd_idx = data_wr_idx - TX_FIFO_RWPTR_INIT;
                    if (data_rd_idx < 0)
                    {
                        data_rd_idx += TX_DATA_FIFO_LENGTH;
                    }
                    ptr_rst_cnt += ((w2r < TX_FIFO_W2R_THR) || (r2w <
                                                                TX_FIFO_R2W_THR));
#if TEST && !DELAY_MEAS

                    /*Sys_GPIO_Toggle(10); */
#endif    /* if TEST && !DELAY_MEAS */
                }
#if TEST
                tmp0[tmp_idx] = data_wr_idx;
                tmp1[tmp_idx] = data_rd_idx;
                tmp_idx++;
                if (tmp_idx == TMP_LEN)
                {
                    tmp_idx = 0;
                }
#endif    /* if TEST */
                if ((data_rd_idx + (*length)) > TX_DATA_FIFO_LENGTH)
                {
                    memcpy(ptr, &tx_data_fifo[data_rd_idx],
                           TX_DATA_FIFO_LENGTH - data_rd_idx);
                    memcpy(&ptr[TX_DATA_FIFO_LENGTH - data_rd_idx],
                           &tx_data_fifo[0],
                           (*length) - TX_DATA_FIFO_LENGTH + data_rd_idx);
                }
                else
                {
                    memcpy(ptr, &tx_data_fifo[data_rd_idx], *length);
                }
                data_rd_idx = (data_rd_idx + (*length)) % TX_DATA_FIFO_LENGTH;
            }
            else
            {
                memset(ptr, 0, *length);
            }
            prev_status = app_env.cp_link_status;
        }
        break;

        case TX_SUCCESS:
        {
            /* Free the allocated buffer in app */
        }
        break;

        case TX_RETRANSMIT_DONE:
        {
            /* Free the allocated buffer in app */
        }
        break;

        case RX_START:
        {
            /* For notification to app */
        }
        break;

        case RX_SUCCESS:
        {
            /* Read pointer ptr pointing to received data with the passed
             * length, trigger rendering for the first time */
            if ((*length) == 0)
            {
                /* PLC should be applied as tx hasn't sent data */
                /* For example: repeat previous packet, */
            }
            else
            {
                memcpy(frame_in_buf, ptr, *length);
            }
        }
        break;

        case RX_RETRANSMIT_RECEIVED:
        {
            /* Read pointer ptr pointing to received data with the passed
             * length */
            if ((*length) == 0)
            {
                /* PLC should be applied as tx hasn't sent data */
                /* For example: repeat previous packet, */
            }
            else
            {
                memcpy(frame_in_buf, ptr, *length);
            }
        }
        break;

        case RX_TIMEOUT:
        {
            /* For notification to app to do PLC */
            /* For example: repeat previous packet, */
            memcpy(frame_in_buf, ptr, *length);
        }
        break;
    }

    return (0);
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t CP_Callback_StatusUpdate(uint8_t status)
 * ----------------------------------------------------------------------------
 * Description   : Custom protocol status call back function
 * Inputs        : - status   - Connection status
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint8_t CP_Callback_StatusUpdate(uint8_t status)
{
    app_env.cp_link_status = status;

    switch (status)
    {
        case LINK_DISCONNECTED:
        {
            PRINTF("__CP_LINK_DISCONNECTED\n");			
            /* Stop audio transmission to avoid having annoying noise */
            /* Decide if the number of lost links is large, do an action */

            app_env.cp_lostLink_counter++;
        }
        break;

        case LINK_ESTABLISHMENT_UNSUCCESS:
        {
            app_env.cp_unsuccessLink_cunter++;
        }
        break;

        case LINK_ESTABLISHED:
        {
            PRINTF("__CP_LINK_ESTABLISHED\n");		
            /* Start audio transmission */

            NVIC_ClearPendingIRQ(TIMER2_IRQn);
            NVIC_EnableIRQ(TIMER2_IRQn);
        }
        break;

        default:
        {
        }
        break;
    }

    return (0);
}

/* ----------------------------------------------------------------------------
 * Function      : void TIMER2_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Timer 2 interrupt handler
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER2_IRQHandler(void)
{
    if (app_env.cp_param.master == CP_SLAVE_ROLE)    /* RX */
    {
        if (app_env.cp_param.bidirectional == CP_MONODIRECTION)
        {
            /* Start audio streaming */
            app_env.audio_streaming = 1;

            Sys_Timers_Stop(SELECT_TIMER2);
            Sys_Timer_Set_Control(2, TIMER_FREE_RUN | TIMER_PRESCALE_1 |
                                  TIMER_SLOWCLK_DIV2 |
                                  ((app_env.cp_param.interval_time / 2) - 1));
            Sys_Timers_Start(SELECT_TIMER2);

            /* Start transfer to LPDSP */
            if (!frame_decoded)
            {
                error++;
            }
            frame_decoded = false;
            memcpy(frame_in, frame_in_buf, ENCODED_FRAME_LENGTH *
                   sizeof(uint8_t));
            start_lpdsp((uint32_t)&frame_in[0]);
            frame_idx = ENCODED_SUBFRAME_LENGTH;
        }
        else
        {
        }
    }
}
