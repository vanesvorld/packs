/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Main program
 * ----------------------------------------------------------------------------
 * $Revision: 1.9 $
 * $Date: 2019/12/27 21:07:23 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>

/* ----------------------------------------------------------------------------
 * Function      : int main()
 * ----------------------------------------------------------------------------
 * Description   : Main program
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    SCB->VTOR = (unsigned int)(&ISR_Vector_Table);

    Initialize_system();
    /* Debug/trace initialization. In order to enable UART or RTT trace,
     * configure the 'OUTPUT_INTERFACE' macro in printf.h */
    printf_init();	
    PRINTF("__custom_protocol_trx has started as %s\n", (CP_ROLE & 0x1 ? "MASTER" : "SLAVE"));

    while (1)
    {
        Sys_Watchdog_Refresh();

        CP_EventHandler();

        SYS_WAIT_FOR_EVENT;
    }
}
