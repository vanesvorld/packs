Low Latency Audio Sample Application with Custom Protocol
=========================================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

This sample code demonstrates a complete audio path with the custom protocol.

On the transmitter side, data is received through the SPI interface. It is
processed with an asynchronous sample rate converter (ASRC) to synchronize
the audio sample rate between the Ezairo 8300 DSP and RSL10. Data is then 
encoded with LPDSP32, and sent over to the receiver via the custom protocol.

On the receiver side, when data is successfully received from the custom
protocol, the LPDSP32 first decodes it, and then ASRC processes it. Finally
the data is sent via SPI to the Ezairo 8300.

This sample application can be compiled and loaded onto the RSL10 transmitter
and receiver. In the `app.h` file, set the master role or slave role
accordingly.

To compile the sample for a transmitter, define: 

`#define CP_ROLE                 MASTER`

To compile the sample for a receiver, define: 

`#define CP_ROLE                 SLAVE`

To use this application, load the `bi_directional_master` application to the
Ezairo 7100 devices used on both the transmitter and receiver sides of the
link. Ezairo 7100 components of this sample application set are provided in
the `RSL10_Utility_Apps.zip` file; you need the Ezairo 7100 EDK to work with
these components.

The source code exists in a `code` folder, and application-related include
header files are in the `include` folder. The main program is located in
`app.c` in the home directory.

Select the project configuration according to the chip ID version of
RSL10. The appropriate libraries and include files are loaded according to
the build configuration that you choose. Use **Debug** for CID 101.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which 
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).

