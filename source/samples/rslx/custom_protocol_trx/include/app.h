/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.h
 * - Application header file
 * ----------------------------------------------------------------------------
 * $Revision: 1.32 $
 * $Date: 2019/12/27 21:07:24 $
 * ------------------------------------------------------------------------- */

#ifndef APP_H_
#define APP_H_

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>
#include <stdlib.h>
#include <stdbool.h>
#include "cp_pkt.h"

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/
#define CP_SLOWLIST_FIRST       { 0, 12, 39 }
#define CP_SLOWLIST_SECOND      { 11, 24, 28 }
#define CP_FASTLIST_FIRST       { 1, 15, 31, 22, 5, 29, 21, 8, 19, 10, 28, 13, \
                                  25, 2, 16, 27, 18, 9, 20, 32, 4, 30, 24, 14, \
                                  26, 17, 11, 6, 36, 3 }
#define CP_FASTLIST_SECOND      { 13, 25, 21, 11, 15, 31, 18, 29, 20, 38, 36, \
                                  15, 14, 26, 17, 28, 30, 32, 16, 20, 34, 24, \
                                  10, 27, 22, 37, 33, 20, 19,                 \
                                  18 }

/* #define CP_FASTLIST_FIRST       {1,2,3,4,5,6,7,8,9,10,11,13,14,15,16,17,18,19,20,21,22,24,25,26,27,28,29,30,31,32}
 * #define CP_FASTLIST_SECOND
 *      {11,13,14,15,16,17,18,19,20,21,22,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,20,19,18,17} */

/* Output power */
#define OUTPUT_POWER_DBM                0

#define NO_RX_INPUT             0
#define SPI_RX_INPUT            1
#define PCM_RX_INPUT            2
#define DMIC_RX_INPUT           3

#define NO_TX_OUTPUT            0
#define SPI_TX_OUTPUT           1
#define PCM_TX_OUTPUT           2

#define SLAVE                   0
#define MASTER                  1

#ifndef CP_ROLE
#define CP_ROLE                 MASTER
#endif

#if (CP_ROLE == MASTER)
#define INPUT_INTRF             SPI_RX_INPUT
#define OUTPUT_INTRF            NO_TX_OUTPUT
#else    /* if (CP_ROLE == MASTER) */
#define INPUT_INTRF             NO_RX_INPUT
#define OUTPUT_INTRF            SPI_TX_OUTPUT
#endif    /* if (CP_ROLE == MASTER) */

/* LPDSP32 JTAG DIO's */
#define TCK_DIO                 0
#define TMS_DIO                 1
#define TDI_DIO                 2
#define TDO_DIO                 3

/* SPI Port DIO's */
#define SPI_SER_DI              2
#define SPI_SER_DO              1
#define SPI_CLK_DO              3
#define SPI_CS_DO               0

/* PCM Port DIO's */
#define PCM_SER_DI              2
#define PCM_SER_DO              1
#define PCM_CLK_DO              3
#define PCM_FRAME_SYNC          0

#define SAMPL_CLK               7

#define THREE_BLOCK_APPN(x, y, z)       x##y##z

/* Generate DMAx_IRQn */
#define DMA_IRQn(x)                     THREE_BLOCK_APPN(DMA, x, _IRQn)

/* Generate DMAx_IRQHandler */
#define DMA_IRQ_FUNC(x)                 THREE_BLOCK_APPN(DMA, x, _IRQHandler)

/* DMA channels */
#define DMIC_DMA_NUM            0
#define ASRC_IN_IDX             3
#define ASRC_OUT_IDX            4
#define RX_DMA_NUM              5
#define TX_DMA_NUM              2
#define MEMCPY_DMA_NUM          7

/* Store read/write pointer for testing purposes */
#define TEST                    0

#define DIG_MIC_RATE            1
#define DMIC_CLK_PIN            0
#define DMIC_DATA_PIN           1

/* For delay measurement */
#define DELAY_MEAS              0

/* Lower threshold of test signal for delay measurement */
#define DELAY_MEAS_THR          (5000)

/* LPDSP32 CODEC related defines */
#define MEM_CM2DSP_ADDR0_ENC    (uint8_t *)(DSP_DRAM5_BASE)
#define MEM_CM2DSP_ADDR1_ENC    (uint8_t *)(DSP_DRAM5_BASE + 160 * 2)
#define MEM_DSP2CM_ADDR0_ENC    (uint8_t *)(DSP_DRAM4_BASE)
#define MEM_DSP2CM_ADDR1_ENC    (uint8_t *)(DSP_DRAM4_BASE + 80)
#define MEM_CM2DSP_ADDR0_DEC    (uint8_t *)(DSP_DRAM5_BASE + 160 * 4)
#define MEM_CM2DSP_ADDR1_DEC    (uint8_t *)(DSP_DRAM5_BASE + 160 * 4 + 80)
#define MEM_DSP2CM_ADDR0_DEC    (uint8_t *)(DSP_DRAM4_BASE + 160 * 4)
#define MEM_DSP2CM_ADDR1_DEC    (uint8_t *)(DSP_DRAM4_BASE + 160 * 6)
#define MEM_MESSAGE             (uint8_t *)(DSP_DRAM4_BASE + 12 * 160)
#define CODEC_MODE              1

/* Subframe length in uint16_t */
#define SUBFRAME_LENGTH         4

/* Frmae length in uint16_t */
#define FRAME_LENGTH            32

/* Encoeded frame length in uint8_t */
#define ENCODED_FRAME_LENGTH    (FRAME_LENGTH / 2)

/* Encoded subframe length in uint8_t */
#define ENCODED_SUBFRAME_LENGTH (SUBFRAME_LENGTH / 2)

/* Threshold for valid Cr/Ck distance */
#define ASRC_CFG_THR            4

/* Threshold for allowed read/write pointer crossing */
#define PTR_RST_THR             200

/* Total subframe length accounting for left and right channel samples; for
 * PCM data transfer */
#define SUBFRAME_LENGTH_LEFT_AND_RIGHT  (SUBFRAME_LENGTH * 2)

/* TX data fifo related defines */
#define TX_DATA_FIFO_LENGTH     (ENCODED_FRAME_LENGTH + 6 * ENCODED_SUBFRAME_LENGTH)
#define TX_FIFO_RWPTR_INIT      (ENCODED_FRAME_LENGTH + 0 * ENCODED_SUBFRAME_LENGTH)
#define TX_FIFO_W2R_THR         (ENCODED_FRAME_LENGTH)
#define TX_FIFO_R2W_THR         (ENCODED_SUBFRAME_LENGTH)

/* RX data fifo related defines */
#if DELAY_MEAS
#define RX_DATA_FIFO_LENGTH     160
#else    /* if DELAY_MEAS */
#define RX_DATA_FIFO_LENGTH     (8 * SUBFRAME_LENGTH)
#endif    /* if DELAY_MEAS */
#define RX_FIFO_RWPTR_INIT      ((RX_DATA_FIFO_LENGTH / 2) - SUBFRAME_LENGTH)
#define RX_FIFO_W2R_THR         (SUBFRAME_LENGTH)
#define RX_FIFO_R2W_THR         (SUBFRAME_LENGTH)

#define DMIC_CFG                (DMIC0_DCRM_CUTOFF_320HZ | \
                                 DMIC1_DCRM_CUTOFF_320HZ | \
                                 DMIC1_FALLING_EDGE |      \
                                 DMIC0_RISING_EDGE |       \
                                 DMIC1_DELAY_0P25)

#define DMIC_DECIMATION_RATE    ((uint32_t)(0x11U << AUDIO_CFG_DEC_RATE_Pos))

#define DMIC_AUDIO_CFG          (DMIC0_ENABLE |         \
                                 DMIC_DECIMATION_RATE | \
                                 DMIC_AUDIOCLK)

#define DMA_DMIC_CONFIG         (DMA_LITTLE_ENDIAN |       \
                                 DMA_ENABLE |              \
                                 DMA_DISABLE_INT_DISABLE | \
                                 DMA_ERROR_INT_ENABLE |    \
                                 DMA_COMPLETE_INT_ENABLE | \
                                 DMA_COUNTER_INT_DISABLE | \
                                 DMA_START_INT_DISABLE |   \
                                 DMA_DEST_WORD_SIZE_32 |   \
                                 DMA_SRC_WORD_SIZE_16 |    \
                                 DMA_DEST_ADDR_INC |       \
                                 DMA_PRIORITY_0 |          \
                                 DMA_TRANSFER_P_TO_M |     \
                                 DMA_SRC_ADDR_STATIC |     \
                                 DMA_SRC_DMIC |            \
                                 DMA_ADDR_CIRC)

/* DMA for ASRC input on TX side */
#define TX_DMA_ASRC_IN          (DMA_DEST_ASRC |           \
                                 DMA_TRANSFER_M_TO_P |     \
                                 DMA_LITTLE_ENDIAN |       \
                                 DMA_COMPLETE_INT_ENABLE | \
                                 DMA_COUNTER_INT_DISABLE | \
                                 DMA_DEST_WORD_SIZE_16 |   \
                                 DMA_SRC_WORD_SIZE_32 |    \
                                 DMA_SRC_ADDR_INC |        \
                                 DMA_DEST_ADDR_STATIC |    \
                                 DMA_ADDR_LIN |            \
                                 DMA_DISABLE)

/* DMA for ASRC output on TX side */
#define TX_DMA_ASRC_OUT         (DMA_SRC_ASRC |            \
                                 DMA_TRANSFER_P_TO_M |     \
                                 DMA_LITTLE_ENDIAN |       \
                                 DMA_COMPLETE_INT_ENABLE | \
                                 DMA_COUNTER_INT_DISABLE | \
                                 DMA_DEST_WORD_SIZE_32 |   \
                                 DMA_SRC_WORD_SIZE_16 |    \
                                 DMA_SRC_ADDR_STATIC |     \
                                 DMA_DEST_ADDR_INC |       \
                                 DMA_ADDR_LIN |            \
                                 DMA_DISABLE)

/* DMA for RX data port on TX side */
#if (INPUT_INTRF == SPI_RX_INPUT)
#define DMA_RX_CONFIG           (DMA_LITTLE_ENDIAN |       \
                                 DMA_DISABLE |             \
                                 DMA_DISABLE_INT_DISABLE | \
                                 DMA_ERROR_INT_DISABLE |   \
                                 DMA_COMPLETE_INT_ENABLE | \
                                 DMA_COUNTER_INT_DISABLE | \
                                 DMA_START_INT_DISABLE |   \
                                 DMA_DEST_WORD_SIZE_32 |   \
                                 DMA_SRC_WORD_SIZE_16 |    \
                                 DMA_SRC_SPI0 |            \
                                 DMA_PRIORITY_0 |          \
                                 DMA_TRANSFER_P_TO_M |     \
                                 DMA_DEST_ADDR_INC |       \
                                 DMA_SRC_ADDR_STATIC |     \
                                 DMA_ADDR_CIRC)
#elif (INPUT_INTRF == PCM_RX_INPUT)
#define DMA_RX_CONFIG      (DMA_LITTLE_ENDIAN |       \
                            DMA_DISABLE |             \
                            DMA_DISABLE_INT_DISABLE | \
                            DMA_COMPLETE_INT_ENABLE | \
                            DMA_COUNTER_INT_ENABLE |  \
                            DMA_START_INT_DISABLE |   \
                            DMA_DEST_WORD_SIZE_32 |   \
                            DMA_SRC_WORD_SIZE_32 |    \
                            DMA_SRC_PCM |             \
                            DMA_PRIORITY_0 |          \
                            DMA_TRANSFER_P_TO_M |     \
                            DMA_DEST_ADDR_INC |       \
                            DMA_SRC_ADDR_STATIC |     \
                            DMA_ADDR_CIRC)
#endif    /* if (INPUT_INTRF == SPI_RX_INPUT) */
#define PCM_CFG_RX          (PCM_BIT_ORDER_MSB_FIRST | \
                             PCM_TX_ALIGN_LSB |        \
                             PCM_WORD_SIZE_24 |        \
                             PCM_FRAME_ALIGN_LAST |    \
                             PCM_FRAME_WIDTH_LONG |    \
                             PCM_MULTIWORD_2 |         \
                             PCM_SUBFRAME_DISABLE |    \
                             PCM_CONTROLLER_DMA |      \
                             PCM_DISABLE |             \
                             PCM_SELECT_SLAVE)

/* DMA for ASRC input on RX side */
#define RX_DMA_ASRC_IN          (DMA_DEST_ASRC |           \
                                 DMA_TRANSFER_M_TO_P |     \
                                 DMA_LITTLE_ENDIAN |       \
                                 DMA_COMPLETE_INT_ENABLE | \
                                 DMA_COUNTER_INT_DISABLE | \
                                 DMA_DEST_WORD_SIZE_16 |   \
                                 DMA_SRC_WORD_SIZE_32 |    \
                                 DMA_SRC_ADDR_INC |        \
                                 DMA_DEST_ADDR_STATIC |    \
                                 DMA_ADDR_LIN |            \
                                 DMA_DISABLE)

/* DMA for ASRC output on RX side */
#define RX_DMA_ASRC_OUT         (DMA_SRC_ASRC |            \
                                 DMA_TRANSFER_P_TO_M |     \
                                 DMA_LITTLE_ENDIAN |       \
                                 DMA_COMPLETE_INT_ENABLE | \
                                 DMA_COUNTER_INT_DISABLE | \
                                 DMA_DEST_WORD_SIZE_32 |   \
                                 DMA_SRC_WORD_SIZE_16 |    \
                                 DMA_SRC_ADDR_STATIC |     \
                                 DMA_DEST_ADDR_INC |       \
                                 DMA_ADDR_LIN |            \
                                 DMA_DISABLE)

/* DMA for TX data port on RX side */
#if (OUTPUT_INTRF == SPI_TX_OUTPUT)
#define DMA_TX_CONFIG            (DMA_LITTLE_ENDIAN |       \
                                  DMA_DISABLE |             \
                                  DMA_DISABLE_INT_DISABLE | \
                                  DMA_ERROR_INT_DISABLE |   \
                                  DMA_COMPLETE_INT_ENABLE | \
                                  DMA_COUNTER_INT_DISABLE | \
                                  DMA_START_INT_DISABLE |   \
                                  DMA_DEST_WORD_SIZE_16 |   \
                                  DMA_SRC_WORD_SIZE_32 |    \
                                  DMA_DEST_SPI0 |           \
                                  DMA_PRIORITY_0 |          \
                                  DMA_TRANSFER_M_TO_P |     \
                                  DMA_DEST_ADDR_STATIC |    \
                                  DMA_SRC_ADDR_INC |        \
                                  DMA_ADDR_LIN)
#elif (OUTPUT_INTRF == PCM_TX_OUTPUT)
#define DMA_TX_CONFIG            (DMA_LITTLE_ENDIAN |       \
                                  DMA_DISABLE |             \
                                  DMA_DISABLE_INT_DISABLE | \
                                  DMA_ERROR_INT_DISABLE |   \
                                  DMA_COMPLETE_INT_ENABLE | \
                                  DMA_COUNTER_INT_ENABLE |  \
                                  DMA_START_INT_DISABLE |   \
                                  DMA_DEST_WORD_SIZE_32 |   \
                                  DMA_SRC_WORD_SIZE_32 |    \
                                  DMA_DEST_PCM |            \
                                  DMA_PRIORITY_0 |          \
                                  DMA_TRANSFER_M_TO_P |     \
                                  DMA_DEST_ADDR_STATIC |    \
                                  DMA_SRC_ADDR_INC |        \
                                  DMA_ADDR_CIRC)
#endif    /* if (OUTPUT_INTRF == SPI_TX_OUTPUT) */

#define DMA_MEMCPY_CONFIG       (DMA_LITTLE_ENDIAN |        \
                                 DMA_DISABLE |              \
                                 DMA_DISABLE_INT_DISABLE |  \
                                 DMA_ERROR_INT_DISABLE |    \
                                 DMA_COMPLETE_INT_DISABLE | \
                                 DMA_COUNTER_INT_DISABLE |  \
                                 DMA_START_INT_DISABLE |    \
                                 DMA_DEST_WORD_SIZE_32 |    \
                                 DMA_SRC_WORD_SIZE_32 |     \
                                 DMA_PRIORITY_0 |           \
                                 DMA_TRANSFER_M_TO_M |      \
                                 DMA_DEST_ADDR_INC |        \
                                 DMA_SRC_ADDR_INC |         \
                                 DMA_ADDR_LIN)

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    12

/* ----------------------------------------------------------------------------
 * Data types
 * --------------------------------------------------------------------------*/
struct app_env_tag
{
    struct cp_param_tag cp_param;
    uint8_t cp_link_status;
    uint16_t cp_lostLink_counter;
    uint16_t cp_unsuccessLink_cunter;
    uint8_t audio_streaming;
};
/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/
extern void *ISR_Vector_Table;

extern int16_t spi_buf[];
extern uint32_t pcm_buf[];
extern int16_t asrc_in_buf[];
extern int16_t asrc_out_buf[];
extern uint8_t tx_data_fifo[];
extern int16_t rx_data_fifo[];
#if (INPUT_INTRF == DMIC_RX_INPUT)
extern int16_t dmic_buffer_in[];
#endif    /* if (INPUT_INTRF == DMIC_RX_INPUT) */
extern int16_t data_wr_idx;
extern int16_t data_rd_idx;
extern int16_t w2r;
extern int16_t r2w;
extern uint8_t error;
extern uint8_t ptr_rst_cnt;
extern uint8_t prev_status;
extern bool frame_decoded;
extern bool flag_ascc_phase;
extern uint8_t frame_in[];
extern uint8_t frame_idx;
extern int16_t lpdsp_buf[];
extern struct app_env_tag app_env;
#if TEST
#define TMP_LEN 5000
extern uint8_t tmp0[];
extern uint8_t tmp1[];
extern int tmp_idx;
#endif    /* if TEST */
/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/
void Initialize_system(void);

void APP_CPInit(void);

uint8_t CP_Callback_TRX(uint8_t type, uint8_t *length, uint8_t *ptr);

uint8_t CP_Callback_StatusUpdate(uint8_t status);

void port_rx_dma_isr(void);

void asrc_in_dma_isr(void);

void asrc_out_dma_isr(void);

void dspEnc_isr(void);

void StoreDspEncData(uint8_t *src_addr);

void Start_Enc_Lpdsp32_Channel(void);

void ascc_phase_isr(void);

void ascc_period_isr(void);

void start_lpdsp(uint32_t src_addr);

void AsrcFunc(void);

void dspDec_isr(void);

void port_tx_dma_isr(void);

void ASRC_ERROR_IRQHandler(void);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* APP_H */
