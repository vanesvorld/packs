/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * dsp_pm_dm.h
 * - LPDSP constant header file
 * ----------------------------------------------------------------------------
 * $Revision: 1.6 $
 * $Date: 2018/02/15 16:00:17 $
 * ------------------------------------------------------------------------- */

#define MEM_PM_SIZE  5245
#define MEM_PM_EXTRA  0
#define MEM_PM_LINES  1049

#define MEM_DMA_SIZE  1732
#define MEM_DMA_LINES  433.0
#define MEM_DMB_SIZE  8992
#define MEM_DMB_LINES  2248.0

extern uint8_t LPDSP32_Data_low_DM[];
extern uint8_t LPDSP32_Prog_40bit_PM[];
