Flash Copier and CRC Sample Code
================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project demonstrates an application that:

1.  Uses DIO6 as a toggling output signal. This DIO was selected so that the
    LED on the Evaluation and Development Board flashes.
 
2.  Demonstrates the usage of the flash copier block by copying 4 KB into PRAM
    and calculating the CRC of the same data. This is done using the following
    steps:

     - Compute the CRC of the first 4 KB of flash memory (using 
       Sys_Flash_Copy).
     - Copy the first 4 KB of the flash into PRAM (using Sys_Flash_Copy).
     - Compute the CRC of the first 4 KB of PRAM.
     - Compute the CRC of the first 4 KB of PRAM minus 1 word, to check the 
       CRC engine.
     - Check the results and toggle DIO6 accordingly:
         - Toggle slowly (2.0 s) in case of an error.
         - Toggle rapidly (0.5 s) if all tests passed.

The source code exists in `app.c`, with no defined header files.

Hardware Requirements
---------------------
This application can be executed on any Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the
*Getting Started Guide* for your IDE for more information.

Verification
------------
To verify that this application is functioning correctly, DIO6 flashes 
continuously and rapidly on the Evaluation and Development Board.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which 
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
