/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * This sample code is to demonstrate the usage of Flash copier block
 * in copying into PRAM memory and CRC block.
 * 1 - Compute the CRC of the first 4 KB of flash memory (using Sys_Flash_Copy)
 * 2 - Copy the first 4 KB of the flash into PRAM (using Sys_Flash_Copy)
 * 3 - Compute the CRC of the first 4 KB of PRAM
 * 4 - Compute the CRC of the first 4 KB of PRAM minus 1 word, to check the CRC
 *     engine
 * 5 - Check the results and toggle DIO6 accordingly:
 *       - Toggle slowly (2.0 s) in case of an error
 *       - Toggle rapidly (0.5 s) in case all tests passed
 * ----------------------------------------------------------------------------
 * $Revision: 1.9 $
 * $Date: 2019/12/18 15:19:31 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>

/* ----------------------------------------------------------------------------
 * Function      : void FLASH_COPY_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Set a status flag to indicate the flash copier
 *                 operation is complete.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void FLASH_COPY_IRQHandler(void)
{
    flash_copier_complete = 1;
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Perform the following steps:
 *                 - Initialize the system
 *                 - Compute the CRC of the first 4 KB of flash memory (using
 *                   Sys_Flash_Copy)
 *                 - Copy the first 4 KB of the flash into PRAM (using
 *                   Sys_Flash_Copy)
 *                 - Compute the CRC of the first 4 KB of PRAM
 *                 - Compute the CRC of the first 4 KB of PRAM minus 1 word, to
 *                   check the CRC engine
 *                 - Check the results and toggle DIO6 accordingly:
 *                     - Toggle slowly (2.0 s) in case of an error
 *                     - Toggle rapidly (0.5 s) in case all tests passed
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* local variables */
    uint32_t crc_flash_result = 0;
    uint32_t crc_pram2_result = 0;
    uint32_t crc_pram2_1023_result = 0;
    uint32_t i;
    uint8_t crc_passed = 0;
    uint32_t *p_pram2    = (uint32_t *)(PRAM2_BASE);

    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Setup DIO6 as a GPIO output. */
    Sys_DIO_Config(LED_DIO, DIO_MODE_GPIO_OUT_0);

    /* Enable the flash copier interrupt */
    NVIC_EnableIRQ(FLASH_COPY_IRQn);

    /* Clear the program flag */
    flash_copier_complete = 0;

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    PRINTF("DEVICE INITIALIZED\n");
    /* 1. The first 4 KB of the flash is copied into the CRC block and
     *    the value is stored in crc_flash_result */

    /* Configure the CRC engine */
    Sys_CRC_Set_Config(CRC_32 | CRC_LITTLE_ENDIAN);

    /* Reset the CRC engine */
    CRC->VALUE = CRC_32_INIT_VALUE;

    /* Set the flash copier to copy to the CRC block;
     * wait for the copy to complete before continuing */
    Sys_Flash_Copy(FLASH_MAIN_BASE, 0, 1024, COPY_TO_CRC_BITBAND);
    while (flash_copier_complete == 0);

    /* Save the result */
    crc_flash_result = (CRC->FINAL);

    /* 2. The first 4 KB of the flash is copied into the PRAM memory */

    /* Clear the program flag */
    flash_copier_complete = 0;

    /* Set the flash copier to copy to PRAM;
     * wait for the copy to complete before continuing */
    Sys_Flash_Copy(FLASH_MAIN_BASE, PRAM2_BASE, 1024, COPY_TO_MEM_BITBAND);
    while (flash_copier_complete == 0);

    /* 3. The CRC value of the portion in the PRAM is computed
     * using the CRC block and the value is store in crc_pram_result */

    /* Configure the CRC engine */
    Sys_CRC_Set_Config(CRC_32 | CRC_LITTLE_ENDIAN);

    /* Reset the CRC engine */
    CRC->VALUE = CRC_32_INIT_VALUE;

    /* Accumulate PRAM2 into CRC block */
    for (i = 0; i < 1024; i++)
    {
        CRC->ADD_32 = p_pram2[i];
    }

    /* Save the result */
    crc_pram2_result = (CRC->FINAL);

    /* 4. Compute the CRC of the first 4 KB of PRAM minus 1 word, to check the
     * CRC engine */

    /* Configure the CRC engine */
    Sys_CRC_Set_Config(CRC_32 | CRC_LITTLE_ENDIAN);

    /* Reset the CRC engine */
    CRC->VALUE = CRC_32_INIT_VALUE;

    /* Accumulate PRAM2 into CRC block minus 1 word */
    for (i = 0; i < 1023; i++)
    {
        CRC->ADD_32 = p_pram2[i];
    }

    /* Save the result */
    crc_pram2_1023_result = (CRC->FINAL);

    /* 5. Compare the result and define crc_passed variable */
    if (crc_flash_result == crc_pram2_result &&
        crc_pram2_result != crc_pram2_1023_result)
    {
        crc_passed = 1;
    }

    /* Spin loop */
    while (1)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* Toggle DIO6 rapidly if the CRCs matched;
         * otherwise toggle DIO6 slowly */
        Sys_GPIO_Toggle(LED_DIO);

        /* The wait time is dependent on the result */
        if (crc_passed == 1)
        {
            Sys_Delay_ProgramROM(0.5 * SystemCoreClock);
            PRINTF("CRC PASSED\n");
        }
        else
        {
            Sys_Delay_ProgramROM(2.0 * SystemCoreClock);
            PRINTF("CRC FAILED\n");
        }
    }
}
