/* ----------------------------------------------------------------------------
 * Copyright (c) 2020 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2020
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Main application file
 * ----------------------------------------------------------------------------
 * $Revision: 1.4 $
 * $Date: 2020/05/22 16:13:54 $
 * ------------------------------------------------------------------------- */

#include <app.h>

extern const struct DISS_DeviceInfo_t deviceInfo;

/* ----------------------------------------------------------------------------
 * Function      : void BASS_Setup(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure the Battery Service Server
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void BASS_Setup(void)
{
    BASS_Initialize(APP_BAS_NB, APP_BASS_ReadBatteryLevel);
    BASS_NotifyOnBattLevelChange(TIMER_SETTING_S(1));     /* Periodically monitor the battery level. Only notify changes */
    BASS_NotifyOnTimeout(TIMER_SETTING_S(6));             /* Periodically notify the battery level to connected peers */
    APP_BASS_SetBatMonAlarm(BATMON_SUPPLY_THRESHOLD_CFG); /* BATMON alarm configuration */
}

/* ----------------------------------------------------------------------------
 * Function      : void DISS_Setup(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure the Device Information Service Server
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
/**
 * @brief Configure the Device Information Service Server
 */
static void DISS_Setup(void)
{
    DISS_Initialize(APP_DIS_FEATURES, (const struct DISS_DeviceInfo_t*) &deviceInfo);
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_Setup(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure the Continuous Glucose Monitoring Server
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void CGMS_Setup(void)
{
    CGMS_Initialize(TIMER_SETTING_S(APP_CGMS_UPDATE_NTF_TIMER_S),
    		CGM_FEAT_LOW_BATT_DETECT_SUP_BIT, /* See (cgm_feat_bf) */
			CGM_SMP_LOC_FINGER, /* See (cgmp_sample_location_id) */
			CGM_TYPE_SMP_CAPILLARY_WHOLE_BLOOD, /* See (cgmp_type_sample_id) */
			APP_CGMS_GlucoseMeasurementUpdate,
			APP_CGMS_ResetDeviceSpecificAlert,
			APP_CGMS_StatusUpdate);
    APP_CGMS_Initialize();
}

int main(void)
{
    /* Configure hardware and initialize BLE stack */
    Device_Initialize();

    /* Debug/trace initialization. In order to enable UART or RTT trace,
     * configure the 'RSL10_DEBUG' macro in printf.h */
    printf_init();
    PRINTF("__ble_peripheral_server_cgm has started!\n\r");

    /* Configure application-specific advertising data and scan response data.
     * The advertisement period will change after 30 s as per 5.1.1 */
    APP_SetAdvScanData();

    /* Configure Battery Service Server */
    BASS_Setup();

    /* Configure Device Information Service Server */
    DISS_Setup();

    /* Configure Heart Rate Service Server */
    CGMS_Setup();

    /* Add application message handlers */
    MsgHandler_Add(TASK_ID_GAPM, APP_GAPM_GATTM_Handler);
    MsgHandler_Add(GATTM_ADD_SVC_RSP, APP_GAPM_GATTM_Handler);
    MsgHandler_Add(TASK_ID_GAPC, APP_GAPC_Handler);
    MsgHandler_Add(APP_LED_TIMEOUT, APP_LED_Timeout_Handler);
    MsgHandler_Add(APP_BATT_LEVEL_LOW, APP_BASS_BattLevelLow_Handler);


    /* Reset the GAP manager. Trigger GAPM_CMP_EVT / GAPM_RESET when finished. See APP_GAPM_GATTM_Handler */
    GAPM_ResetCmd();

    while (1)
    {
        Kernel_Schedule();    /* Dispatch all events in Kernel queue */
        Sys_Watchdog_Refresh();
        SYS_WAIT_FOR_EVENT;    /* Wait for an event before re-executing the scheduler */
    }
}
