Continuous Glucose Monitoring Peripheral Device with Server Sample Code
=======================================================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in Software_Use_Agreement.rtf
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------

This sample project is equipped with a Bluetooth(R) Low Energy abstraction
layer that provides a higher level application programming interface (API)
to abstract the Bluetooth GAP and GATT layers. The abstraction layer has been
designed to improve flexibility and simplicity by providing the following
features:

    - An event subscription mechanism that allows the application to subscribe 
      and receive callback notifications for any Kernel or Bluetooth event. 
      This improves encapsulation/integration of different modules, as they 
      can be implemented while isolated in their own files.
    - Generic definition of custom services with callback notification support
    - Security handling and bond list implementation in RSL10 flash
    - Code structure and API naming aligned with RivieraWaves documentation, 
      so you can map the code into the documentation more easily

The sample code has been refactored to keep the generic abstraction layer code 
and application-specific code in separate files. This increases flexibility, 
maintainability and reusability of components between distinct applications.

After the connection is established, for any profiles/services that need to
be enabled, an enable request message is sent to the corresponding profile of
the Bluetooth stack. Once a response is received for each of those profiles,
the application starts advertising.

**Battery Service:** this service database is configured for a single battery 
                 instance. A second battery can be added by modifying the 
                 `APP_BAS_NB` definition in `app_bass.h`. The application provides
                 a callback function to read the battery level using the 
                 average of 16 reads of RSL10 ADC. 


The application notifies clients about the battery level on two occasions:

**Periodically:** the notification timeout is configured using the 
              `BASS_NotifyOnTimeout()` function.

**On battery level change:** It monitors the battery level periodically and sends
                         a notification when a change is detected. The 
                         monitoring timeout is configured using the 
                         `BASS_NotifyOnBattLevelChange()` function.

Device Information Service: this service database contains only one instance 
of the service. The user application code in the function 
`APP_DISS_DeviceInfoValueReqInd` handles the different characteristics requests. 

**Continuous Glucose Service:** this service database contains only one instance 
of the service. The abstraction layer code deals with the Time Offset and storing 
many of the Control Point values. It uses a callback supplied by the application 
to read the glucose measurement values, read the sensor status fields, as well as 
reset the Device Specifc Alarm. This measurement callback is called every time the 
continuous glucose profile application code needs to send a notification.
�Note: This is a demonstration implementation, intended to show basic message 
functionality. Record Access Control Points (RACP) are not supported by 
default, and the "Session" functionality of the "Operating Control Points"
is in Beta and may not perform as expected. OpCtrlPt get/set functions are 
implemented but require application level functional definitions (with the 
exception of the Communication Interval which will set the Notification 
Timeout for the Glucose Measurement within the Abstraction).�

The message subscription mechanism allows the application and services to 
subscribe and receive callback notifications based on the Kernel message ID 
or task ID. This allows each module of the application to be independently 
implemented in its own files. The subscription is performed using the 
`MsgHandler_Add()` function. The application subscribes to receive GAPM and 
GAPC events (see `app.c`). The services subscribe to receive Kernel events in 
their initialization function (see `BASS_Initialize()` in `ble_bass.c` for an 
example). The application event handlers are implemented in `app_msg_handler.c`.

The basic sequence of operations and event messages exchanged between the 
application and the Bluetooth stack is presented below: 

  APP <--> Bluetooth Low Energy Stack
  
  Startup
  
      --->  GAPM_ResetCmd() - app.c
      <---  GAPM_CMP_EVT / GAPM_RESET
      --->  GAPM_SetDevConfigCmd() - app_msg_handler.c
      <---  GAPM_CMP_EVT / GAPM_SET_DEV_CONFIG
      --->  GAPM_ProfileTaskAddCmd() - ble_bass.c
	  --->  GAPM_ProfileTaskAddCmd() - ble_diss.c
	  --->  GAPM_ProfileTaskAddCmd() - ble_cgms.c
      <---  GAPM_PROFILE_ADDED_IND
      --->  GAPM_StartAdvertiseCmd() - app_msg_handler.c
            
  Connection request / parameters update request / device info request
  
      <--- GAPC_CONNECTION_REQ_IND
      ---> GAPM_ResolvAddrCmd() - app_msg_handler.c
      ---> GAPC_ConnectionCfm() - app_msg_handler.c
      ---> GAPM_StartAdvertiseCmd() - app_msg_handler.c
      <--- GAPC_PARAM_UPDATE_REQ_IND
      ---> GAPC_ParamUpdateCfm() - app_msg_handler.c
      <--- GAPC_GET_DEV_INFO_REQ_IND
      ---> GAPC_GetDevInfoCfm() - app_msg_handler.c

  Pairing / Bonding request
  
      <--- GAPC_BOND_REQ_IND / GAPC_PAIRING_REQ
      GAPC_BondCfm() - app_msg_handler.c
      <--- GAPC_BOND_REQ_IND / GAPC_LTK_EXCH
      GAPC_BondCfm() - app_msg_handler.c
      <--- GAPC_BOND_REQ_IND / GAPC_IRK_EXCH
      GAPC_BondCfm() - app_msg_handler.c
      <--- GAPC_BOND_REQ_IND / GAPC_CSRK_EXCH
      GAPC_BondCfm() - app_msg_handler.c

  Encrypt request
  
      <--- GAPC_ENCRYPT_REQ_IND
      ---> GAPC_EncryptCfm() - app_msg_handler.c

  Disconnection
  
      <--- GAPC_DISCONNECT_IND
      ---> GAPM_StartAdvertiseCmd() - app_msg_handler.c


Continuous Glucose Service
==========================

The following are overviews of the Continuous Glucose Monitoring Profile 
and its implementation in this sample project. 

The Continuous Glucose Monitoring Profile is used to enable a data 
collection device to obtain data from a CGM Sensor that exposes the 
Continuous Glucose Monitoring Service (intended for health applications).

The GATT profile defines two roles: Continuous Glucose Sensor (GATT Server) 
and Collector (GATT Client). The Continuous Glucose Sensor is the device 
that measures Glucose levels and other information while the Collector is 
the device that receives the measurement and other data from a Continuous 
Glucose Sensor. In this sample project, only the Glucose Measurement portion 
of the GATT profile is implemented.

The Continuous Glucosee Sensor accepts any connection request from any 
Continuous Glucose Collector. During the connection procedure, if the 
Continuous Glucose Sensor creates a bond with a Collector, it writes the 
Bluetooth device information of the bonded Collector in its controller's 
whitelist.

For demonstration purpose, the Continuous Glucose Sensor implemented in 
this project periodically sends randomly-generated glucose level data to 
the Collector (this project, however, can be customized to read and send 
actual data provided by a real-life glucose sensor attached to a human 
body, as well as keep records or previous measurements and trigger alarms
when certain changing parameters are observed). 

�Note: This is a demonstration implementation, intended to show basic message 
functionality. Record Access Control Points (RACP) are not supported by 
default, and the "Session" functionality of the "Operating Control Points"
is in Beta and may not perform as expected. OpCtrlPt get/set functions are 
implemented but require application level functional definitions (with the 
exception of the Communication Interval which will set the Notification 
Timeout for the Glucose Measurement within the Abstraction).�

Source Code Organization
==========================

This sample project is structured as follows:

The source code exists in a `source` folder. Application-related header files 
are in the `include` folder. The `main()` function is implemented in the `app.c`
file, which is located in the parent directory.

The Bluetooth Low Energy abstraction layer contains support functions for the 
GAP and GATT layers and has a message handling mechanism that allows the 
application to subscribe to Kernel events. It also contains standard profile-
related files.

The device address type and source are set in `app.h`. If `APP_DEVICE_PARAM_SRC`
is set to `FLASH_PROVIDED_or_DFLT` and the address type is set to 
`GAPM_CFG_ADDR_PUBLIC`, the stack loads the public device address stored in NVR3
flash. Otherwise, the address provided in the application is used.

Erasing NVR2 leads to an empty bondlist. If the desired behavior of this
application or test configuration is to start from a new bondlist at startup, 
the following steps can be undertaken:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which pauses at the
    start of its initialization routine).

Note that this erases the flash, but only if there is anything in the NVR2 drive.


Application-specific files
--------------------------
`app.c`                  - Main application file 

source
------
    app_config.c      - Device configuration and definition of application-
                        specific Bluetooth Low Energy messages.
    app_msg_handler.c - Application-specific message handlers
    app_bass.c        - Application-specific Battery Service Server functions 
                        and message handlers (ADC read, BATMON alarm, etc.)
    app_diss.c        - Application-specific Device Information Service Server 
                        functions and message handlers.
    app_cgms.c        - Application-specific Continuous Glucose Service Server 
                        functions and message handlers.
    app_trace.c       - Debugging (printf) utility functions

include
-------
    app.h             - Main application header file
    app_bass.h        - Application-specific Battery Service Server header
    app_diss.h        - Application-specific Device Information Service Server 
                        header
    app_cgms.h        - Application-specific Continuous Glucose Service Server header
    
Bluetooth Low Energy abstraction files (CMSIS-Pack component is generic for any application)
---------------------

    ble_gap.c         - GAP layer support functions and message handlers        
    ble_gatt.c        - GATT layer support functions and message handlers     
    msg_handler.c     - Message handling subscribing mechanism implementation


    ble_gap.h         - GAP layer abstraction header
    ble_gatt.h        - GATT layer abstraction header
    msg_handler.h     - Message handling subscribing mechanism header       
    

    ble_bass.c        - Battery Service Server support functions and handlers
    ble_diss.c        - Device Information Service Server support functions and 
                        handlers
    ble_cgms.c        - Continuous Glucose Service Server support functions and 
                        handlers


    ble_bass.h        - Battery Service Server header
    ble_diss.h        - Device Information Service Server header
    ble_cgms.h        - Continuous Glucose Service Server header

       
Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board 
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.

Select the project configuration according to the required optimization level. 
Use the **Debug** configuration for optimization level **None**, or **Release**
configuration for optimization level **More** or **O2**.

Verification
------------
To verify if this application is functioning correctly, use RSL10 or another 
third-party central device application to establish a connection and execute 
pairing and bonding functionality. In addition to establishing a connection, 
this application can be used to read/write characteristics and receive 
notifications.

To show how an application can send notifications, the battery service is 
configured to send the battery level every 6 seconds or less (if a battery level 
change is detected in VBAT). Also, he custom service sends an incrementing 
value of the first attribute every 6 seconds and the CGMS Glucose Mesaurement
Command is used to send an updated Value Notifications every 10 seconds. 

Alternatively, you can observe the behavior of the LED on the RSL10 Evaluation 
and Development Board (DIO6). The LED behavior is controlled by the 
`APP_LED_Timeout_Handler` function (in `app_msg_handler.c`) and can be one of the 
following:

    - If the device has not started advertising, the LED is off.
    - If the device is advertising but it has not connected to any peer, the 
      LED blinks every 200 ms.
    - If the device is connected to fewer than APP_NB_PEERS peers, the LED 
      blinks every 2 seconds according to the number of connected peers (i.e., 
      once if one peer is connected, twice if two peers are connected, etc.).
    - If the device is connected to APP_NB_PEERS peers, the LED is steady on 
      and the application is no longer advertising.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which pauses at the
    start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

Noted that the use of this method also resets the NVR2 memory.

***
Copyright (c) 2020 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
