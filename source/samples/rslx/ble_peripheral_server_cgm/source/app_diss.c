/*
 * app_diss.c
 *
 *  Created on: Sep 17, 2018
 *      Author: zbhhrc
 */

#include <app.h>
#include <diss.h>

/* ----------------------------------------------------------------------------
 * Function      : int APP_DISS_Initialize( void )
 * ----------------------------------------------------------------------------
 * Description   : Initialize the device information app environment and message handlers
 * Inputs        : None
 * Outputs       : 0 if the initialization was successful, < 0 otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
/**
 * @brief Initialize the heart rate app environment and message handlers
 *
 * Assumptions : None
 *
 * @return  0 if the initialization was successful, < 0 otherwise
 */
int APP_DISS_Initialize( void )
{
    MsgHandler_Add(DISS_VALUE_REQ_IND, APP_DISS_DeviceInfoValueReqInd);

    return 0;
}

/* ----------------------------------------------------------------------------
 * Function      : void DevinceInfo_ValueReqInd(ke_msg_id_t const msg_id,
 *                                     struct diss_value_req_ind
 *                                     const *param,
 *                                     ke_task_id_t const dest_id,
 *                                     ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handle the indication when peer device requests a value
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameters in format of
 *                                struct diss_value_req_ind
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
/**
 * @brief Handle the indication when peer device requests a value
 *
 * Assumptions   : None
 *
 * @param[in] msgid Kernel message ID number
 * @param[in] param Message parameters in format of struct diss_value_req_ind
 * @param[in] dest_id Destination task ID number
 * @param[in] src_id Source task ID number
 */
void APP_DISS_DeviceInfoValueReqInd( ke_msg_id_t const msgid, void const * param,
        ke_task_id_t const dest_id, ke_task_id_t const src_id )
{
    struct diss_value_req_ind const *p = param;
    /* Initialize length */
    uint8_t len = 0;

    /* Pointer to the data */
    uint8_t *data;

    /* Check requested value */
    switch( p->value )
    {
        case DIS_MANUFACTURER_NAME_CHAR:
        {
            /* Set information */
            len = APP_DIS_MANUFACTURER_NAME_LEN;
            data = (uint8_t *) APP_DIS_MANUFACTURER_NAME;
        }
        break;

        case DIS_MODEL_NB_STR_CHAR:
        {
            /* Set information */
            len = APP_DIS_MODEL_NB_STR_LEN;
            data = (uint8_t *) APP_DIS_MODEL_NB_STR;
        }
        break;

        case DIS_SYSTEM_ID_CHAR:
        {
            /* Set information */
            len = APP_DIS_SYSTEM_ID_LEN;
            data = (uint8_t *) APP_DIS_SYSTEM_ID;
        }
        break;

        case DIS_PNP_ID_CHAR:
        {
            /* Set information */
            len = APP_DIS_PNP_ID_LEN;
            data = (uint8_t *) APP_DIS_PNP_ID;
        }
        break;

        case DIS_SERIAL_NB_STR_CHAR:
        {
            /* Set information */
            len = APP_DIS_SERIAL_NB_STR_LEN;
            data = (uint8_t *) APP_DIS_SERIAL_NB_STR;
        }
        break;

        case DIS_HARD_REV_STR_CHAR:
        {
            /* Set information */
            len = APP_DIS_HARD_REV_STR_LEN;
            data = (uint8_t *) APP_DIS_HARD_REV_STR;
        }
        break;

        case DIS_FIRM_REV_STR_CHAR:
        {
            /* Set information */
            len = APP_DIS_FIRM_REV_STR_LEN;
            data = (uint8_t *) APP_DIS_FIRM_REV_STR;
        }
        break;

        case DIS_SW_REV_STR_CHAR:
        {
            /* Set information */
            len = APP_DIS_SW_REV_STR_LEN;
            data = (uint8_t *) APP_DIS_SW_REV_STR;
        }
        break;

        case DIS_IEEE_CHAR:
        {
            /* Set information */
            len = APP_DIS_IEEE_LEN;
            data = (uint8_t *) APP_DIS_IEEE;
        }
        break;

        default:
        break;
    }

    /* Allocate confirmation to send the value */
    struct diss_value_cfm *cfm_value = KE_MSG_ALLOC_DYN(DISS_VALUE_CFM, src_id,
            dest_id, diss_value_cfm, len);

    /* Set parameters */
    cfm_value->value = p->value;
    cfm_value->length = len;
    if (len)
    {
        /* Copy data */
        memcpy(&cfm_value->data[0], data, len);
    }

    /* Send message */
    ke_msg_send(cfm_value);
}
