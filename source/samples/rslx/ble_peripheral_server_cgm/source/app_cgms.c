/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_cgms.c
 * - Bluetooth continuous glucose monitoring profile application code. This file
 *   deals with the continuous glucose monitoring profile code that is dependent
 *   of the user application.
 * ------------------------------------------------------------------------- */
#include <app.h>
#include <app_cgms.h>

static struct cgms_meas_value_cmd APP_CGM_measure;
static struct cgms_rd_status APP_CGM_status;

bool APP_CGM_BattLevelLow;

/* ----------------------------------------------------------------------------
 * Function      : int APP_CGMS_Initialize( void )
 * ----------------------------------------------------------------------------
 * Description   : Initialize the cgm app environment and message handlers
 * Inputs        : None
 * Outputs       : 0 if the initialization was successful, < 0 otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int APP_CGMS_Initialize(void)
{
    memset(&APP_CGM_measure, '\0', sizeof(struct cgms_meas_value_cmd));

    memset(&APP_CGM_status, '\0', sizeof(struct cgms_rd_status));

    APP_CGM_BattLevelLow = false;

    return 0;
}

/* ----------------------------------------------------------------------------
 * Function      : struct CGMS_Measure_t *APP_CGMS_GlucoseMeasurementUpdate(void)
 * ----------------------------------------------------------------------------
 * Description   : Updates the heart level measurement values
 * Inputs        : None
 * Outputs       : return struct - Updated CGMS_Measure_t structure
 * Assumptions   : None.
 * ------------------------------------------------------------------------- */
struct cgms_meas_value_cmd *APP_CGMS_GlucoseMeasurementUpdate(void)
{
	/* Configure Measurement Notification Flags to define which data is available */
    APP_CGM_measure.flags = CGM_MEAS_FLAGS_SENSOR_STATUS_ANNUNC_STATUS_BIT |
    									CGM_MEAS_FLAGS_SENSOR_STATUS_ANNUNC_CAL_TEMP_BIT |
										CGM_MEAS_FLAGS_SENSOR_STATUS_ANNUNC_WARN_BIT;

    if(APP_CGM_BattLevelLow)
    {
    	APP_CGM_status.annunc_status = APP_CGM_status.annunc_status |
    										  CGM_MEAS_ANNUNC_STATUS_DEV_BATT_LOW_BIT;
    }
    else
    {
    	APP_CGM_status.annunc_status = APP_CGMS_RESET_DEV_BATT_LOW_BIT(APP_CGM_status.annunc_status);
    }

    /* Create a phony value between 100 and 150 */
    APP_CGM_measure.gluc_concent = (rand() % (150 - 100)) + 100;

    /* Measurement warnings being triggered if any (See cgm_meas_sensor_status_annuc_warn_bf) */
    APP_CGM_measure.warn = APP_CGM_status.warning;

    /* Calibration and Temperature related updates (See cgm_meas_sensor_status_annuc_cal_temp_bf) */
    APP_CGM_measure.cal_temp = APP_CGM_status.cal_temp;

    /* Sensor status updates (See cgm_meas_sensor_status_annuc_status_bf) */
    APP_CGM_measure.status = APP_CGM_status.annunc_status;

    return &APP_CGM_measure;
}


/* ----------------------------------------------------------------------------
 * Function      : void APP_CGMS_ResetDeviceSpecificAlert(void)
 * ----------------------------------------------------------------------------
 * Description   : Reset Device Specific Alarm received, perform actions
 * Inputs        : None
 * Outputs       : return struct - Updated CGMS_Measure_t structure
 * Assumptions   : None.
 * ------------------------------------------------------------------------- */
void APP_CGMS_ResetDeviceSpecificAlert(void)
{
	/* Application level message for the Reset Device Specific Alert Indication.
	 * Reset used in the implementation a custom application alert */
}

/* ----------------------------------------------------------------------------
 * Function      : struct CGMS_Status_t *APP_CGMS_StatusUpdate(void)
 * ----------------------------------------------------------------------------
 * Description   : Application function used to access Sensor Status'
 * Inputs        : None
 * Outputs       : return struct - Current CGMS_Status_t structure
 * Assumptions   : None.
 * ------------------------------------------------------------------------- */
struct cgms_rd_status *APP_CGMS_StatusUpdate(void)
{
	return &APP_CGM_status;
}
