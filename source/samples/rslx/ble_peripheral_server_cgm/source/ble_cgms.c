/* ----------------------------------------------------------------------------
 * Copyright (c) 2020 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 *  This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * ble_cgms.c
 * - Bluetooth continuous glucose monitoring service functions. This file deals
 * 		with the glucose monitoring profile communication behavior as defined on
 * 		the CONTINUOUS GLUCOSE MONITORING PROFILE.
 * ------------------------------------------------------------------------- */
#include <rsl10.h>
#include <ble_cgms.h>
#include <ble_gap.h>
#include <msg_handler.h>
#include <prf_utils.h>
#include <cgms.h>

/* Global variable definition */
static CGMS_Env_t cgms_env;

/* ----------------------------------------------------------------------------
 * Function      : int CGMS_Initialize(uint16_t notification_timeout,
				  	  	  	uint32_t features,
				  	  	  	uint8_t body_sample_loc,
				  	  	  	uint8_t type_sample,
				  	  	  	union gapm_adv_info *info,
				  	  	  	struct cgms_meas_value_cmd*(*readGlucoseCallback)(void),
				   	   	   	void (*resetSpecificDeviceAlterCallback)(void),
				   	   	   	struct CGMS_Status_t*(*readStatusCallback)(void))
 * ----------------------------------------------------------------------------
 * Description   : Initialize device information server service environment and
 *                 configure message handlers
 * Inputs        : notification_timeout - time between notifications.
 *                 features 			- Database features: See (cgm_feat_bf).
 *                 body_sample_loc 		- Location of sensor: See
 *                 						  (cgmp_sample_location_id).
 *                 type_sample 			- Type of sample: See
 *                 						  (cgmp_type_sample_id).
 *                 info					- Advertising information
 *                 readGlucoseCallback	- App level function to read Gluc Meas.
 *                 resetAlertCallback	- App level function to reset device
 *                 						  specific alarm.
 *                 readStatusCallback	- App level function to read Status.
 * Outputs       : 0 if the initialization was successful, < 0 otherwise
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int CGMS_Initialize(uint16_t notification_timeout,
        uint32_t features,
        uint8_t body_sample_loc,
		uint8_t type_sample,
        struct cgms_meas_value_cmd*(*readGlucoseCallback)(void),
		void (*resetAlertCallback)(void),
		struct cgms_rd_status*(*readStatusCallback)(void))
{
    /* Reset the application manager environment */
    memset(&cgms_env, 0, sizeof(CGMS_Env_t));

    memset(&cgms_env.sess_start_time, 0, sizeof(struct prf_date_time));

    cgms_env.readGlucoseCallback = readGlucoseCallback;

    cgms_env.resetAlertCallback = resetAlertCallback;

    cgms_env.readStatusCallback = readStatusCallback;

    cgms_env.type_sample = type_sample;

    cgms_env.sample_location = body_sample_loc;

    cgms_env.cgm_feature = features;

    cgms_env.dst_offset = CGM_DST_OFFSET_UNKNOWN_DAYLIGHT_TIME;

    cgms_env.notification_timeout = notification_timeout;

    /* General handler */
    MsgHandler_Add(TASK_ID_CGMS, CGMS_MsgHandler);
    /* GAP event (operation == GAPM_SET_DEV_CONFIG) needed to add the service */
    MsgHandler_Add(GAPM_CMP_EVT, CGMS_MsgHandler);
    /* Confirmation that the service has been added */
    MsgHandler_Add(GAPM_PROFILE_ADDED_IND, CGMS_MsgHandler);
    /* Starts service after this */
    MsgHandler_Add(GAPC_CONNECTION_REQ_IND, CGMS_MsgHandler);
    /* Manages notification config */
    MsgHandler_Add(GAPC_DISCONNECT_IND, CGMS_MsgHandler);

    return 0;
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_ProfileTaskAddCmd(void)
 * ----------------------------------------------------------------------------
 * Description   : Send request to add the device information service in kernel
 * 				   and DB
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */

void CGMS_ProfileTaskAddCmd(void)
{
    struct cgms_db_cfg db_cfg;

    /* Configure CGMS database parameters  */
    db_cfg.cgm_feature = cgms_env.cgm_feature;
    db_cfg.type_sample = cgms_env.type_sample;
    db_cfg.sample_location = cgms_env.sample_location;

    /* Add CGMS profile task to the stack. A GAPM_PROFILE_ADDED_IND event
     * will be generated when finished. */
    GAPM_ProfileTaskAddCmd(PERM(SVC_AUTH, DISABLE), TASK_ID_CGMS, TASK_APP, 0,
            (uint8_t*) &db_cfg, sizeof(struct cgms_db_cfg));
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_EnableReq(uint8_t conidx)
 * ----------------------------------------------------------------------------
 * Description   : Send request for enabling  continuos glucose monitoring
 *				   profile to cgm server
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CGMS_EnableReq(void)
{
    struct cgms_enable_req *req;
    struct cgms_env_tag *env;
    env = PRF_ENV_GET(CGMS, cgms);

    /* Enable the default notification state */
    cgms_env.cgm_meas_ntf_cfg = CGMS_DEFAULT_NOTIFICATION;
    cgms_env.cgm_ops_ctrl_pt_ntf_cfg = CGMS_DEFAULT_NOTIFICATION;
    cgms_env.cgm_racp_ntf_cfg = CGMS_DEFAULT_NOTIFICATION;

    req = KE_MSG_ALLOC(CGMS_ENABLE_REQ,
            prf_src_task_get(&(env->prf_env), 0),
            TASK_APP, cgms_enable_req);

    /* Configure Enable Request to inform of all default Enable Notifications */
    req->prfl_ntf_ind_cfg = CGMS_CCC_MEASURE_NTF ||
    						CGMS_CCC_RCAP_NTF ||
							CGMS_CCC_CTRL_PT_NTF;

    ke_msg_send(req);
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_MeasureValueCmd(struct cgms_meas_value_cmd *param)
 * ----------------------------------------------------------------------------
 * Description   : Send an cgms_meas_value_cmd to a connected Client that is
 * 				   filled with the latest Sensor info from the measure Callback
 * Inputs        :  - param      - Message parameters in format of
 *                                struct cgms_meas_value_cmd
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CGMS_MeasureValueCmd(struct cgms_meas_value_cmd *param)
{
	struct cgms_meas_value_cmd *cmd;
	struct cgms_env_tag *env;
	env = PRF_ENV_GET(CGMS, cgms);

	cmd = KE_MSG_ALLOC(CGMS_MEAS_VALUE_CMD,
			prf_src_task_get(&(env->prf_env), 0),
			TASK_APP, cgms_meas_value_cmd);

	/* Load Cmd with Glucose Measurement information from the Application
	 * and Time Offset from the Abstraction */
	cmd->operation = CGMS_MEAS_CMD_OP_CODE;
	cmd->gluc_concent = param->gluc_concent;
	cmd->flags = param->flags;
	cmd->quality = param->quality;
	cmd->cal_temp = param->cal_temp;
	cmd->warn = param->warn;
	cmd->status = param->status;
	cmd->trend_info = param->trend_info;
	cmd->time_offset = cgms_env.time_offset;

	ke_msg_send(cmd);
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_ReadCharReqInd(struct cgms_rd_char_req_ind *ind)
 * ----------------------------------------------------------------------------
 * Description   : Handles a CGMS_RD_CHAR_REQ_IND by allocating a cfm message
 * 				   and adding any necessary requested data points
 * Inputs        : - ind       - Indication parameters in format of
 *                                struct cgms_rd_char_req_ind
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CGMS_ReadCharReqInd(struct cgms_rd_char_req_ind *ind)
{
	struct cgms_rd_char_cfm * cfm;
	struct cgms_env_tag *env;
	env = PRF_ENV_GET(CGMS, cgms);

	cfm = KE_MSG_ALLOC(CGMS_RD_CHAR_CFM,
			prf_src_task_get(&(env->prf_env), 0),
			TASK_APP, cgms_rd_char_cfm);

	cfm->char_type = ind->char_type;
	cfm->status = GAP_ERR_NO_ERROR;

	switch(ind->char_type)
	{
		/* Returns current MEAS CCC */
		case CGMS_CHAR_ID_MEAS_CCC:
		break;

		/* Returns current OPS CTRL PT CCC */
		case CGMS_CHAR_ID_OPS_CTRL_PT_CCC:
		break;

		/* Returns current RACP CCC */
		case CGMS_CHAR_ID_RACP_CCC:
		break;

		/* Returns Status from the Application and Session Runtime from Abstraction*/
		case CGMS_CHAR_ID_STATUS_VAL:
		{
			struct cgms_rd_status *param = cgms_env.readStatusCallback();
			cfm->value.status.annunc_status = param->annunc_status;
			cfm->value.status.cal_temp = param->cal_temp;
			cfm->value.status.time_offset = cgms_env.time_offset;
			cfm->value.status.warning = param->warning;
		}
		break;

		/* Returns Session Start Time information */
		case CGMS_CHAR_ID_SESSION_START_TIME_VAL:
		{
			cfm->value.sess_start_time.sess_start_time = cgms_env.sess_start_time;
			cfm->value.sess_start_time.dst_offset = cgms_env.dst_offset;
			cfm->value.sess_start_time.time_zone = cgms_env.time_zone;
		}
		break;

		/* Returns the Runtime of the Current Session */
		case CGMS_CHAR_ID_SESSION_RUN_TIME_VAL:
		{
			cfm->value.sess_run_time.run_time = cgms_env.time_offset;
		}
		break;
	}

	ke_msg_send(cfm);
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_WriteCharCCCInd(struct cgms_wr_char_ccc_ind *ind)
 * ----------------------------------------------------------------------------
 * Description   : Handles a CGMS_WR_CHAR_CCC_IND by saving the CCC value for
 * 				   future notify/indicate reference
 * Inputs        : - ind       - Indication parameters in format of
 *                                struct cgms_wr_char_ccc_ind
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CGMS_WriteCharCCCInd(struct cgms_wr_char_ccc_ind *ind)
{
	switch(ind->char_type)
	{
		/* Update the Glucose Measurement Notification CCC */
		case CGMS_CHAR_ID_MEAS_CCC:

			cgms_env.cgm_meas_ntf_cfg = ind->ind_cfg;
		break;

		/* Update the Operation Control Point Response CCC */
		case CGMS_CHAR_ID_OPS_CTRL_PT_CCC:

			cgms_env.cgm_ops_ctrl_pt_ntf_cfg = ind->ind_cfg;
		break;

		/* Update the Record Access Control Point Response CCC */
		case CGMS_CHAR_ID_RACP_CCC:

			cgms_env.cgm_racp_ntf_cfg = ind->ind_cfg;
		break;
	}
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_WriteSessStartTimeInd(struct cgms_wr_sess_start_time_ind *ind)
 * ----------------------------------------------------------------------------
 * Description   : Handles a CGMS_WR_SESS_START_TIME_IND by saving the new
 * 				   Session Start Time
 * Inputs        : - ind       - Indication parameters in format of
 *                                struct cgms_wr_sess_start_time_ind
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CGMS_WriteSessStartTimeInd(struct cgms_wr_sess_start_time_ind *ind)
{
	/* New Session Start Time is written to the cgms_env */
	cgms_env.sess_start_time = ind->sess_start_time;
	cgms_env.dst_offset = ind->dst_offset;
	cgms_env.time_zone = ind->time_zone;
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_WriteRACPInd(struct cgms_wr_racp_ind *ind)
 * ----------------------------------------------------------------------------
 * Description   : Handles a CGMS_WR_RACP_IND by allocating a Response Command
 * 				   to let the Client know of the lack of Record support
 * Inputs        : - ind       - Indication parameters in format of
 *                                struct cgms_wr_racp_ind
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : Records are not currently implemented
 * ------------------------------------------------------------------------- */
void CGMS_WriteRACPInd(struct cgms_wr_racp_ind *ind)
{
	struct cgms_racp_resp_cmd * cmd;
	struct cgms_env_tag *env;
	env = PRF_ENV_GET(CGMS, cgms);

	cmd = KE_MSG_ALLOC(CGMS_RACP_RESP_CMD,
			prf_src_task_get(&(env->prf_env), 0),
			TASK_APP, cgms_racp_resp_cmd);

	/* Load Response with the Op Code of the Request */
	cmd->req_cp_opcode = ind->cp_opcode;

	/* No Current Record Implementation, Return NOT SUPPORTED */
	cmd->operation = CGMS_RACP_CMD_OP_CODE;
	cmd->rsp_code = CGMP_RSP_OP_CODE_NOT_SUP;

	switch(ind->cp_opcode)
	{
		case CGMP_OPCODE_REP_STRD_RECS:
		{
		}
		break;

		case CGMP_OPCODE_DEL_STRD_RECS:
		{
		}
		break;

		case CGMP_OPCODE_REP_NUM_OF_STRD_RECS:
		{
		}
		break;

		case CGMP_OPCODE_ABORT_OP:
		{
		}
		break;

		case CGMP_OPCODE_NUM_OF_STRD_RECS_RSP:
		{
		}
		break;

		case CGMP_OPCODE_RSP_CODE:
		{
		}
		break;

		default:
		break;
	}

	if(CGMS_CheckNtfEnbl(cgms_env.cgm_racp_ntf_cfg))
	{
		ke_msg_send(cmd);
	}
	else
	{
		KE_MSG_FREE(cmd);
	}
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_WriteOpsCtrlPtInd(const struct cgms_wr_ops_ctrl_pt_req_ind *ind)
 * ----------------------------------------------------------------------------
 * Description   : Handles a CGMS_WR_OPS_CTRL_PT_REQ_IND by allocating a
 * 				   Confirmation, Filling the Structure, and acting on the Ctrl
 * Inputs        : - ind       - Indication parameters in format of
 *                                struct cgms_wr_ops_ctrl_pt_req_ind
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CGMS_WriteOpsCtrlPtInd(struct cgms_wr_ops_ctrl_pt_req_ind *ind)
{
	struct cgms_wr_ops_ctrl_pt_cfm * cfm;
	struct cgms_env_tag *env;
	env = PRF_ENV_GET(CGMS, cgms);

	cfm = KE_MSG_ALLOC(CGMS_WR_OPS_CTRL_PT_CFM,
			prf_src_task_get(&(env->prf_env), 0),
			TASK_APP, cgms_wr_ops_ctrl_pt_cfm);

	/* Prepare a Cfm message to Respond to the Wr Ops Ctrl Pt Req */
	cfm->opcode = CGMP_OPS_CODE_RSP_CODE;
	cfm->rsp = CGMP_OPS_RSP_SUCCESS;
	cfm->operand.req_rsp_value.req_op_code = ind->opcode;
	cfm->operand.req_rsp_value.rsp_code = CGMP_OPS_CODE_RSP_CODE;

	switch(ind->opcode)
	{
		/* Glucose Measurement Ntf Interval Set */
		case CGMP_OPS_CODE_SET_CGM_COM_INTVL:
		{
			cgms_env.notification_timeout = ind->operand.intvl;

			/* Disable current running Ntf timer and set using new interval */
			ke_timer_clear(CGMS_TIMEOUT_GLUCOSE_NTF, TASK_APP);
			if(cgms_env.notification_timeout > 0)
			{
				ke_timer_set(CGMS_TIMEOUT_GLUCOSE_NTF, TASK_APP,
						cgms_env.notification_timeout);
			}
		}
		break;

		/* Glucose Measurement Ntf Interval Get */
		case CGMP_OPS_CODE_GET_CGM_COM_INTVL:
		{
			cfm->opcode = CGMP_OPS_CODE_CGM_COM_INTVL_RSP;
			cfm->operand.intvl = cgms_env.notification_timeout;
		}
		break;

		/* Glucose Calibration Value Set */
		case CGMP_OPS_CODE_SET_GLUC_CALIB_VALUE:
		{
			cgms_env.calib = ind->operand.calib;
			cgms_env.cal_data_record_num = ind->operand.cal_data_record_num;
		}
		break;

		/* Glucose Calibration Value Set */
		case CGMP_OPS_CODE_GET_GLUC_CALIB_VALUE:
		{
			cfm->opcode = CGMP_OPS_CODE_GLUC_CALIB_VALUE_RSP;
			cfm->operand.calib = cgms_env.calib;
			cfm->operand.cal_data_record_num = cgms_env.cal_data_record_num;
		}
		break;

		/* Patient High Level Alert Set */
		case CGMP_OPS_CODE_SET_PAT_HIGH_ALERT_LEVEL:
		{
			cgms_env.pat_high_bg = ind->operand.pat_high_bg;
		}
		break;

		/* Patient High Level Alert Get */
		case CGMP_OPS_CODE_GET_PAT_HIGH_ALERT_LEVEL:
		{
			cfm->opcode = CGMP_OPS_CODE_PAT_HIGH_ALERT_LEVEL_RSP;
			cfm->operand.pat_high_bg = cgms_env.pat_high_bg;
		}
		break;

		/* Patient Low Level Alert Set */
		case CGMP_OPS_CODE_SET_PAT_LOW_ALERT_LEVEL:
		{
			cgms_env.pat_low_bg = ind->operand.pat_low_bg;
		}
		break;

		/* Patient Low Level Alert Get */
		case CGMP_OPS_CODE_GET_PAT_LOW_ALERT_LEVEL:
		{
			cfm->opcode = CGMP_OPS_CODE_PAT_LOW_ALERT_LEVEL_RSP;
			cfm->operand.pat_low_bg = cgms_env.pat_low_bg;
		}
		break;

		/* Hypo Level Alert Set */
		case CGMP_OPS_CODE_SET_HYPO_ALERT_LEVEL:
		{
			cgms_env.hypo_alert_level = ind->operand.hypo_alert_level;
		}
		break;

		/* Hypo Level Alert Get */
		case CGMP_OPS_CODE_GET_HYPO_ALERT_LEVEL:
		{
			cfm->opcode = CGMP_OPS_CODE_HYPO_ALERT_LEVEL_RSP;
			cfm->operand.hypo_alert_level = cgms_env.hypo_alert_level;
		}
		break;

		/* Hyper Level Alert Set */
		case CGMP_OPS_CODE_SET_HYPER_ALERT_LEVEL:
		{
			cgms_env.hyper_alert_level = ind->operand.hyper_alert_level;
		}
		break;

		/* Hyper Level Alert Get */
		case CGMP_OPS_CODE_GET_HYPER_ALERT_LEVEL:
		{
			cfm->opcode = CGMP_OPS_CODE_HYPER_ALERT_LEVEL_RSP;
			cfm->operand.hyper_alert_level = cgms_env.hyper_alert_level;
		}
		break;

		/* Rate Of Decrement Level Alert Set*/
		case CGMP_OPS_CODE_SET_RATE_OF_DECR_ALERT_LEVEL:
		{
			cgms_env.rate_decr_alert_level = ind->operand.rate_decr_alert_level;
		}
		break;

		/* Rate Of Decrement Level Alert Get*/
		case CGMP_OPS_CODE_GET_RATE_OF_DECR_ALERT_LEVEL:
		{
			cfm->opcode = CGMP_OPS_CODE_RATE_OF_DECR_ALERT_LEVEL_RSP;
			cfm->operand.rate_decr_alert_level = cgms_env.rate_decr_alert_level;
		}
		break;

		/* Rate Of Increment Level Alert Set*/
		case CGMP_OPS_CODE_SET_RATE_OF_INCR_ALERT_LEVEL:
		{
			cgms_env.rate_incr_alert_level = ind->operand.rate_incr_alert_level;
		}
		break;

		/* Rate Of Increment Level Alert Get*/
		case CGMP_OPS_CODE_GET_RATE_OF_INCR_ALERT_LEVEL:
		{
			cfm->opcode = CGMP_OPS_CODE_RATE_OF_INCR_ALERT_LEVEL_RSP;
			cfm->operand.rate_incr_alert_level = cgms_env.rate_incr_alert_level;
		}
		break;

		/* Call the Application Level Device Specific Alert Reset */
		case CGMP_OPS_CODE_RESET_DEV_SPEC_ALERT:
		{
			cgms_env.resetAlertCallback();
		}
		break;

		/* Session Start Operation Control Point Written */
		case CGMP_OPS_CODE_START_SESS:
		{
			cgms_env.session_started = true;
			cgms_env.time_offset = 0;
			/* Start the Minute Offset Timer (60 seconds) */
			ke_timer_set(CGMS_TIMEOUT_TME_OFFSET, TASK_APP,
					100*60);
		}
		break;

		/* Session Stop Operation Control Point Written */
		case CGMP_OPS_CODE_STOP_SESS:
		{
			cgms_env.session_started = false;
			cgms_env.time_offset = 0;
			ke_timer_clear(CGMS_TIMEOUT_TME_OFFSET, TASK_APP);
		}
		break;

		/* Respond with Message if Unknown/Unsupported OpCtrlPt */
		default :
		{
			cfm->rsp = CGMP_OPS_RSP_OP_CODE_NOT_SUP;
		}
		break;
	}

	if(CGMS_CheckNtfEnbl(cgms_env.cgm_ops_ctrl_pt_ntf_cfg))
	{
		ke_msg_send(cfm);
	}
	else
	{
		KE_MSG_FREE(cfm);
	}
}

/* ----------------------------------------------------------------------------
 * Function      : bool CGMS_CheckNtfEnbl()
 * ----------------------------------------------------------------------------
 * Description   : Check if CGMS notifications are enabled for central device.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
bool CGMS_CheckNtfEnbl(uint16_t cgm_ntf_cfg)
{
    if(GAPC_GetConnectionCount() <= 0)
    {
    	return false;
    }

    if(cgm_ntf_cfg)
	{
		return true;
	}

    return false;
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_DisconnectReq( void )
 * ----------------------------------------------------------------------------
 * Description   : Handles the GAPC_DISCONNECT_IND event.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CGMS_DisconnectInd(void)
{
    /* Clears notification on disconnect */
    cgms_env.cgm_meas_ntf_cfg = 0;
    cgms_env.cgm_ops_ctrl_pt_ntf_cfg = 0;
    cgms_env.cgm_racp_ntf_cfg = 0;

    cgms_env.racp_op_aborted = false;
    cgms_env.session_started = false;

    memset(&cgms_env.sess_start_time, 0, sizeof(struct prf_date_time));
	cgms_env.dst_offset =0xFF;
	cgms_env.time_zone = 0xFF;

	/* Stop the Timers */
    ke_timer_clear(CGMS_TIMEOUT_GLUCOSE_NTF, TASK_APP);
    ke_timer_clear(CGMS_TIMEOUT_TME_OFFSET, TASK_APP);
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_GetEnv(void)
 * ----------------------------------------------------------------------------
 * Description   : Return a reference to the internal environment structure.
 * Inputs        : None
 * Outputs       : Pointer to CGMS_Env_t
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
CGMS_Env_t* CGMS_GetEnv(void)
{
    return &cgms_env;
}

/* ----------------------------------------------------------------------------
 * Function      : bool CGMS_IsAdded(void)
 * ----------------------------------------------------------------------------
 * Description   : Return a boolean indication is the CGMS server has been
 *                  successfully added to the services database.
 * Inputs        : None
 * Outputs       : Return value: true if the service is added, false if not
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
bool CGMS_IsAdded(void)
{
    return cgms_env.serviceAdded;
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_SendGlucoseUpdateNtf(void)
 * ----------------------------------------------------------------------------
 * Description   : Call the Application Glucose Measurement function and send
 * 				   a Notification if the configuration allows.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CGMS_SendGlucoseUpdateNtf(void)
{
    struct cgms_meas_value_cmd *param = cgms_env.readGlucoseCallback();
    if(cgms_env.session_started && CGMS_CheckNtfEnbl(cgms_env.cgm_meas_ntf_cfg))
    {
        CGMS_MeasureValueCmd(param);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void CGMS_MsgHandler(ke_msg_id_t const msg_id,
 *                                      void const *param,
 *                                      ke_task_id_t const dest_id,
 *                                      ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : 	Handle all events related to the heart rate service server
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameter
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void CGMS_MsgHandler(ke_msg_id_t const msg_id, void const *param,
        ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    switch (msg_id)
    {
        case GAPM_CMP_EVT:  // GAP initialized, add the Device Information
        {			        // Service will receive a GAPM_PROFILE_ADDED_IND
                            // event as response
            struct gapm_cmp_evt *gap_event = (struct gapm_cmp_evt*) param;
            switch (gap_event->operation)
            {
                case GAPM_SET_DEV_CONFIG:
                {
                    CGMS_ProfileTaskAddCmd();
                }
                break;
            }
        }
        break;

        case GAPM_PROFILE_ADDED_IND:
        {
            struct gapm_profile_added_ind *ind = (struct gapm_profile_added_ind*) param;
            if( ind->prf_task_id == TASK_ID_CGMS )
            {
                cgms_env.serviceAdded = true;
            }
        }
        break;

        case GAPC_CONNECTION_REQ_IND:
        {
            CGMS_EnableReq();
        }
        break;

        case GAPC_DISCONNECT_IND:
        {
            CGMS_DisconnectInd();
        }
        break;

        case CGMS_ENABLE_RSP:
        {
        	struct cgms_enable_rsp* rsp = (struct cgms_enable_rsp*) param;
            if(rsp->status == GAP_ERR_NO_ERROR) /* Already enabled */
            {
                cgms_env.enable = true;
            }
            /* On Enable, Set Session to Started */
            cgms_env.session_started = true;

            /* Enable the Minute Offset counter for the session */
			ke_timer_set(CGMS_TIMEOUT_TME_OFFSET, TASK_APP,
					100*60);

			if(cgms_env.notification_timeout > 0)
			{
				ke_timer_set(CGMS_TIMEOUT_GLUCOSE_NTF, TASK_APP,
						cgms_env.notification_timeout);
			}
        }
        break;

            /* Notification indication changed by client */
        case CGMS_RD_CHAR_REQ_IND:
        {
        	CGMS_ReadCharReqInd((struct cgms_rd_char_req_ind*) param);
        }
        break;

        case CGMS_WR_CHAR_CCC_IND:
        {
        	CGMS_WriteCharCCCInd((struct cgms_wr_char_ccc_ind*) param);
        }
        break;

        case CGMS_WR_SESS_START_TIME_IND:
        {
        	CGMS_WriteSessStartTimeInd((struct cgms_wr_sess_start_time_ind*) param);
        }
        break;

        case CGMS_WR_RACP_IND:
        {
        	CGMS_WriteRACPInd((struct cgms_wr_racp_ind *) param);
        }
        break;

        case CGMS_WR_OPS_CTRL_PT_REQ_IND:
        {
        	CGMS_WriteOpsCtrlPtInd((struct cgms_wr_ops_ctrl_pt_req_ind*) param);
        }
        break;

        case CGMS_TIMEOUT_GLUCOSE_NTF:
        {
            CGMS_SendGlucoseUpdateNtf();

            /* Restart timer */
            ke_timer_set(CGMS_TIMEOUT_GLUCOSE_NTF, TASK_APP,
                    cgms_env.notification_timeout);
        }
        break;

        case CGMS_TIMEOUT_TME_OFFSET:
        {
        	cgms_env.time_offset++;

			if(cgms_env.notification_timeout > 0 && cgms_env.session_started)
			{
				/* Re-Start the Minute Offset Timer (60 seconds) */
				ke_timer_set(CGMS_TIMEOUT_TME_OFFSET, TASK_APP,
						100*60);
			}
        }
        break;

        case CGMS_CMP_EVT:
        {
        }
        break;

        default:
        break;
    }
}
