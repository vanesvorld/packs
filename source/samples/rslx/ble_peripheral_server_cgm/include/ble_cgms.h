/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2020 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2020
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * ble_cgms.h
 * - Bluetooth continuos glucose monitor server service header
 * ------------------------------------------------------------------------- */

#ifndef BLE_CGMS_H
#define BLE_CGMS_H


/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <cgms.h>
#include <ke_timer.h>
/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/

/** If the server should send notifications after a client connected */
#define CGMS_DEFAULT_NOTIFICATION       0x01

/** Continuous Glucose Monitoring UUID */
#define CGMS_SERVICE_UUID               {0x1F, 0x18}

/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/

typedef struct
{
    /********** Glucose Level **********/

    /** Glucose Level notification configuration */
    uint16_t cgm_meas_ntf_cfg;
    uint16_t cgm_ops_ctrl_pt_ntf_cfg;
    uint16_t cgm_racp_ntf_cfg;

    /** The flag that indicates that service has been added */
    bool serviceAdded;

    /** The flag that indicates that service has been enabled */
    bool enable;

    /** Database configuration */
    uint32_t cgm_feature;
    uint8_t  type_sample;
    uint8_t  sample_location;

    /** Runtime since session start */
    bool          session_started;
    bool          racp_op_aborted;
    uint8_t       time_zone;
    uint8_t       dst_offset;
    uint16_t 	  time_offset;
    struct prf_date_time sess_start_time;

    /** Control Point Variables **/
    struct cgm_calib_operand calib;
    uint16_t cal_data_record_num;
    prf_sfloat pat_high_bg;
    prf_sfloat pat_low_bg;
    prf_sfloat hypo_alert_level;
    prf_sfloat hyper_alert_level;
    prf_sfloat rate_decr_alert_level;
    prf_sfloat rate_incr_alert_level;

    /** Time between notifications */
    uint16_t notification_timeout;

    /** Pointer to an application function that returns the glucose measurement */
    struct cgms_meas_value_cmd*(*readGlucoseCallback)(void);

    /** Pointer to an application function that resets the Device Specific Alert */
    void (*resetAlertCallback)(void);

    /** Pointer to an application function that returns the sensor status */
    struct cgms_rd_status*(*readStatusCallback)(void);

} CGMS_Env_t;

/// Specific Ops Control Point Response Codes
enum cgms_ntf_cfg_values
{
    /// Success - Normal response for successful operation
    CGMS_CCC_MEASURE_DISABLED                       = 0,
    /// Success - Normal response for successful operation
    CGMS_CCC_MEASURE_NTF                            = 1,
    /// Success - Normal response for successful operation
    CGMS_CCC_RCAP_DISABLED                          = (0 << 1),
    /// Success - Normal response for successful operation
    CGMS_CCC_RCAP_NTF                               = (1 << 1),
    /// Success - Normal response for successful operation
    CGMS_CCC_CTRL_PT_DISABLED                       = (0 << 2),
    /// Success - Normal response for successful operation
    CGMS_CCC_CTRL_PT_NTF                            = (1 << 2),
    /// Parameters out of range - Normal response if Operand received does not meet the range requirements
    CGMS_OPS_RSP_PARAM_OUT_OF_RANGE                 = 8,

};

enum cgms_app_msg_id
{
    /** Notification timer */
    CGMS_TIMEOUT_GLUCOSE_NTF = TASK_FIRST_MSG(TASK_ID_CGMS) + 50,
    /** Connection mode timeout */
    CGMS_TIMEOUT_ADVERTISE_MODE,
	/** Minute offset timeout */
	CGMS_TIMEOUT_TME_OFFSET,
};

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/
int CGMS_Initialize(uint16_t notification_timeout,
        uint32_t features,
        uint8_t body_sample_loc,
		uint8_t type_sample,
        struct cgms_meas_value_cmd*(*readGlucoseCallback)(void),
		void (*resetAlertCallback)(void),
		struct cgms_rd_status*(*readStatusCallback)(void));

void CGMS_ProfileTaskAddCmd(void);

void CGMS_EnableReq(void);

void CGMS_MeasureValueCmd(struct cgms_meas_value_cmd *cmd);

void CGMS_ReadCharReqInd(struct cgms_rd_char_req_ind *ind);

void CGMS_WriteCharCCCInd(struct cgms_wr_char_ccc_ind *ind);

void CGMS_WriteSessStartTimeInd(struct cgms_wr_sess_start_time_ind *ind);

void CGMS_WriteRACPInd(struct cgms_wr_racp_ind *ind);

void CGMS_WriteOpsCtrlPtInd(struct cgms_wr_ops_ctrl_pt_req_ind *ind);

bool CGMS_CheckNtfEnbl(uint16_t cgm_ntf_cfg);

void CGMS_DisconnectInd(void);

CGMS_Env_t* CGMS_GetEnv(void);

bool CGMS_IsAdded(void);

void CGMS_SendGlucoseUpdateNtf(void);

void CGMS_MsgHandler(ke_msg_id_t const msg_id, void const *param,
                     ke_task_id_t const dest_id, ke_task_id_t const src_id);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif /* BLE_PROFILES_INCLUDE_BLE_HRPS_H_ */
