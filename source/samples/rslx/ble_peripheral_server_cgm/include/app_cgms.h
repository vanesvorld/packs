/* ----------------------------------------------------------------------------
 * Copyright (c) 2020 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_cgms.h
 * - CGMS Application-specific header file
 * ------------------------------------------------------------------------- */

#ifndef APP_CGMS_H
#define APP_CGMS_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/

#define APP_CGMS_RESET_DEV_BATT_LOW_BIT(status)			(status & 0xFD)

/* ----------------------------------------------------------------------------
 * Function prototypes
 * --------------------------------------------------------------------------*/

int APP_CGMS_Initialize(void);

struct cgms_meas_value_cmd *APP_CGMS_GlucoseMeasurementUpdate(void);

void APP_CGMS_ResetDeviceSpecificAlert(void);

struct cgms_rd_status *APP_CGMS_StatusUpdate(void);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif /*APP_CGMS_H */
