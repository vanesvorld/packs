Peripheral Device with Server Sample Code
=========================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project generates a battery service and a custom service. It then
starts undirected connectable advertising.

Depending on the device address type selected by the application (in `ble_std.h`),
either a public or private address is used for the device. When the public
address type is selected, one of the following addresses is used: 

1.  A public address provided by the application. 
2.  If a public address is not provided by the application, the device's
    public address that is available in `DEVICE_INFO_BLUETOOTH_ADDR` (located in
    `NVR3`) is used. 
3.  If `NVR3` does not have a public address, a default public address that is
    predefined in the stack is used. 

When the private address type is selected, the private address provided by the
application is used.

For this sample project, any central device can scan, connect, and perform
service discovery, receive battery value notifications or read the battery
value. The central device has the ability to read and write custom attributes.
The custom service on the peripheral includes two long characteristic attributes. 
The value written on the attribute with `RX_LONG_VALUE` characteristic name is 
inverted and written back to `TX_LONG_VALUE` by the custom service. The central 
device periodically performs long reads and writes on these characteristics.
The RSL10 ADC is used to read the battery level value every 200 ms when there
is a kernel timer event. The average for 16 reads is calculated and if this
average value changes, a flag is set to send the battery level notification. 
In addition, if the custom service notification is enabled, it sends a 
notification with an incremental value every six seconds.

This application allows the peripheral device to connect to up to four
different central devices.

This sample project passes through several states before all services are
enabled:

1.  `APPM_INIT` (initialization)
    Application initializing and is configured into an idle state. 
2.  `APPM_CREATE_DB` (create database)
    Application has configured the Bluetooth stack, including GAP, according to
    the required role and features. It is now adding services, handlers, and 
    characteristics for all services that it will provide.
3.  `APPM_READY` (ready)
    Generated for each peripheral instance. Application has added the desired
    standard and custom services or profiles into the Bluetooth GATT database
    and handlers.
4.  `APPM_ADVERTISING` (advertising)
    Generated for each peripheral instance. The device starts advertising based
    on the sample project.
5.  `APPM_CONNECTED` (connected)
    Generated for each peripheral instance. Connection is now established with
    another compatible device.

After the connection is established, the `BLE_SetServiceState` function is called
in which, for any profiles/services that need to be enabled, an enable request 
message is sent to the corresponding profile of the Bluetooth stack. Once
a response is received each of those profiles, a flag is set to indicate to the
application that the corresponding profile is enabled and ready to use.  

This sample project is structured as follows:

The source code exists in a `code` folder, all application-related include
header files are in the `include` folder and the `main()` function `app.c` is 
located in the parent directory.

Code
----
    app_init.c    - All initialization functions are called here, but the 
                    implementation is in the respective C files
    app_process.c - Message handlers for application
    ble_bass.c    - Support functions and message handlers pertaining to Battery
                    Service Server
    ble_custom.c  - Support functions and message handlers pertaining to Custom
                    Service Server
    ble_std.c     - Support functions and message handlers pertaining to
                    Bluetooth low energy technology

Include
-------
    app.h        - Overall application header file
    ble_bass.h   - Header file for Battery Service Server
    ble_custom.h - Header file for Custom Service Server
    ble_std.h    - Header file for Bluetooth low energy standard
    
Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board 
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.

Select the project configuration according to the required optimization level. 
Use **Debug** configuration for optimization level **None** or **Release**
configuration for optimization level **More** or **O2**.

Verification
------------
To verify that this application is functioning correctly, use RSL10 or another 
third-party central device application to establish a connection. 
In addition to establishing a connection, this application can be used to 
read/write characteristics and receive notifications.

To show how an application can send notifications, for every 30 timer 
expirations (6 seconds) a notification request flag is set and the application
sends an incremental value of the first attribute to a peer device.

Alternatively, you can observe the behavior of the LED on the RSL10 Evaluation 
and Development Board (DIO6). The LED behavior is controlled by the `LED_Timer` 
function (`app_process.c`) and can be one of the following:

- If the device has not started advertising: the LED is off.
- If the device is advertising but it has not connected to any peer: the LED
      blinks every 200 ms.
- If the device is advertising and it is connecting to fewer than
      `NUM_MASTERS` peers: the LED blinks every 2 seconds according to the
      number of connected peers (i.e., blinks once if one peer is connected,
      twice if two peers are connected, etc.).
- If the device is connecting to `NUM_MASTERS` peers (it has stopped advertising):
      the LED is steady on.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
