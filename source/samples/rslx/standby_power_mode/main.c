/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * main.c
 * - Main application
 * ----------------------------------------------------------------------------
 * $Revision: 1.25 $
 * $Date: 2018/03/21 18:10:41 $
 * ------------------------------------------------------------------------- */

#include "main.h"

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : The main function
 * Inputs        : None
 * Outputs       : return value - Undefined as main() should not return
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main()
{
    /* Test DIO12 to pause the program to make it easy to reprogram to flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    Sys_DIO_Config(APP_START_PIN, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(WAKEUP_IRQ_PIN, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(AFTER_WK_IRQ_PIN, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(LED_PIN, DIO_MODE_GPIO_OUT_0);

    /* Configure RTC as wake-up source using XTAL32kHz
     * to alarm & wake-up after a pre-defined duration */
    ACS->RTC_CFG  = RTC_CNT_START_4294967295;
    ACS->RTC_CTRL = RTC_ALARM_DURATION | RTC_CLK_SRC_XTAL32K;

    /* Reset and enable RTC */
    ACS_RTC_CTRL->RESET_ALIAS  = RTC_RESET_BITBAND;
    ACS_RTC_CTRL->ENABLE_ALIAS = RTC_ENABLE;

    /* Enable XTAL32K oscillator amplitude control
     * Set XTAL32K load capacitance to 0x38: 22.4 pF
     * Enable XTAL32K oscillator */

    ACS->XTAL32K_CTRL = (XTAL32K_XIN_CAP_BYPASS_DISABLE                                |
                         XTAL32K_AMPL_CTRL_ENABLE                                      |
                         XTAL32K_NOT_FORCE_READY                                       |
                         (XTAL32K_CLOAD_TRIM_VALUE << ACS_XTAL32K_CTRL_CLOAD_TRIM_Pos) |
                         (XTAL32K_ITRIM_VALUE << ACS_XTAL32K_CTRL_ITRIM_Pos)           |
                         XTAL32K_IBOOST_DISABLE                                        |
                         XTAL32K_ENABLE);

    /* Wait for XTAL32kHz to be ready */
    while (ACS_XTAL32K_CTRL->READY_ALIAS != XTAL32K_OK_BITBAND);

    /* ------------------------------------------------------------------------
    * - Toggle a specific pin to notify that the application gets started
    * - Notes: this also gives a time window to re-flash the board, therefore,
    *   it should be used when this sample code is tested
    * ---------------------------------------------------------------------- */
    Toggle_APP_START_PIN();

    /* --------------------------------------------------------------------- */

    /* Configure the current trim settings for VCC */
    ACS_VCC_CTRL->ICH_TRIM_BYTE     = VCC_ICHTRIM_80MA_BYTE;

    /* Enable/disable buck converter */
    ACS_VCC_CTRL->BUCK_ENABLE_ALIAS = VCC_BUCK_LDO_CTRL;

    /* Configure memory power */
    SYSCTRL->MEM_POWER_CFG = MEM_CONFIGURATIONS;

    while (1)
    {
        Sys_Watchdog_Refresh();

        /* Clear pad retention */
        ACS_WAKEUP_CTRL->PADS_RETENTION_EN_ALIAS =
            PADS_RETENTION_DISABLE_BITBAND;

        /* Mask all interrupts */
        __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

        /* Disable all interrupts and clear any pending interrupts */
        Sys_NVIC_DisableAllInt();
        Sys_NVIC_ClearAllPendingInt();

        /* Enable VDDRF supply without changing trimming settings */
        ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
        ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

        /* Wait until VDDRF supply has powered up */
        while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

        /* Enable RF power switches */
        SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

        /* Remove RF isolation */
        SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

        /* Start the 48 MHz oscillator without changing the other register bits
         * */
        RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                         XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

        /* Enable 48 MHz oscillator divider at desired prescale value */
        RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_6_BYTE;

        /* Wait until 48 MHz oscillator is started */
        while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
               ANALOG_INFO_CLK_DIG_READY_BITBAND);

        /* Switch to (divided 48 MHz) oscillator clock */
        CLK_SYS_CFG->SYSCLK_SRC_SEL_BYTE = SYSCLK_CLKSRC_RFCLK_BYTE;

        /* Configure clock dividers */
        CLK->DIV_CFG0 = (SLOWCLK_PRESCALE_1 | BBCLK_PRESCALE_2 |
                         USRCLK_PRESCALE_1);
        CLK->DIV_CFG2 = (CPCLK_PRESCALE_8 | DCCLK_PRESCALE_2);

        /* Disable and ignore clock detector,
         * set trim flag to avoid ROM reinitializing all registers */
        ACS_CLK_DET_CTRL->RESET_IGNORE_ALIAS =
            ACS_CLK_DET_RESET_DISABLE_BITBAND;
        ACS_CLK_DET_CTRL->ENABLE_ALIAS = ACS_CLK_DET_DISABLE_BITBAND;
        ACS_RCOSC_CTRL->FTRIM_FLAG_ALIAS     = RC_OSC_CALIBRATED_BITBAND;

        /* Clear reset flags */
        ACS->RESET_STATUS = ACS_RESET_STATUS_FLAG_CLEAR;
        DIG->RESET_STATUS = DIG_RESET_STATUS_FLAG_CLEAR;

        /* --------------------------------------------------------------------
        * The main application
        * ------------------------------------------------------------------ */
        Main_Application();

        /* ----------------------------------------------------------------- */

        /* Configure RTC as wake-up source using XTAL32kHz
         * to alarm & wake-up after a pre-defined duration */
        ACS->RTC_CFG  = RTC_CNT_START_4294967295;
        ACS->RTC_CTRL = RTC_ALARM_DURATION | RTC_CLK_SRC_XTAL32K;

        /* Reset and enable RTC */
        ACS_RTC_CTRL->RESET_ALIAS = RTC_RESET_BITBAND;
        ACS_RTC_CTRL->ENABLE_ALIAS = RTC_ENABLE_BITBAND;

        /* Enable XTAL32kHz */
        ACS_XTAL32K_CTRL->ENABLE_ALIAS = XTAL32K_ENABLE_BITBAND;

        /* Wait for XTAL32kHz to be ready */
        while (ACS_XTAL32K_CTRL->READY_ALIAS != XTAL32K_OK_BITBAND);

        Sys_Watchdog_Refresh();

        /* Clear pending wake-up interrupt */
        NVIC_ClearPendingIRQ(WAKEUP_IRQn);

        /* Stop masking interrupts */
        __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
        __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);

        /* Put the system into standby mode*/

        /* Update wake-up configuration and control registers */
        ACS->WAKEUP_CFG = WAKEUP_DELAY_32     |
                          WAKEUP_DIO3_DISABLE |
                          WAKEUP_DIO2_DISABLE |
                          WAKEUP_DIO1_DISABLE |
                          WAKEUP_DIO0_DISABLE;

        /* Update wake-up control and clear previous wake-up events */
        ACS->WAKEUP_CTRL = PADS_RETENTION_ENABLE      |
                           WAKEUP_DCDC_OVERLOAD_CLEAR |
                           WAKEUP_PAD_EVENT_CLEAR     |
                           WAKEUP_RTC_ALARM_CLEAR     |
                           WAKEUP_BB_TIMER_CLEAR      |
                           WAKEUP_DIO3_EVENT_CLEAR    |
                           WAKEUP_DIO2_EVENT_CLEAR    |
                           WAKEUP_DIO1_EVENT_CLEAR    |
                           WAKEUP_DIO0_EVENT_CLEAR;

        /* Switch sysclk to RC startup oscillator */
        CLK_SYS_CFG->SYSCLK_SRC_SEL_BYTE = SYSCLK_CLKSRC_RCCLK_BYTE;

        /* -- XTAL48MHz -- */
        /* Stop the 48 MHz oscillator without changing the other register bits
         * */
        RF->XTAL_CTRL = (RF->XTAL_CTRL | XTAL_CTRL_DISABLE_OSCILLATOR);

        /*--------------- */

        /* -- RF power -- */
        /* Put RF isolation */
        SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_DISABLE_BITBAND;

        /* Disable RF power switches */
        SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_DISABLE_BITBAND;

        /* Disable VDDRF power supply */
        ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_DISABLE_BITBAND;

        /*--------------- */

        /* Set VDDC standby voltage trimming: 0.8V */
        ACS_VDDC_CTRL->STANDBY_VTRIM_BYTE =
            ((uint8_t)(0x5U << ACS_VDDC_CTRL_STANDBY_VTRIM_Pos));

        /* Enable wake-up interrupt */
        NVIC_EnableIRQ(WAKEUP_IRQn);

        /* Enter standby mode */
        ACS->PWR_MODES_CTRL = PWR_STANDBY_MODE;

        /* DO NOT change SYS_WAIT_FOR_INTERRUPT to SYS_WAIT_FOR_EVENT */
        /* Wait for interrupt */
        SYS_WAIT_FOR_INTERRUPT;

        /* After waking up from standby mode */

        Buzz_DIO(AFTER_WK_IRQ_PIN, AFTER_WK_IRQ_TOGGLE_NUM,
                 AFTER_WK_IRQ_TOGGLE_DURATION_US, 3);
    }

    return (0);
}

/* ----------------------------------------------------------------------------
 * Function      : void Buzz_DIO(uint32_t dio_pad, uint32_t toggle_num,
 *                               uint32_t toggle_duration_us,
 *                               uint32_t sys_clk_MHz)
 * ----------------------------------------------------------------------------
 * Description   : Toggles a specific pin to debug
 * Inputs        : - dio_pin: pin # [1, 2, ...]
 *                 - toggle_num: # of toggles
 *                 - toggle_duration_us: duration of each toggle [us]
 *                 - sys_clk_MHz: system clock frequency [MHz]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Buzz_DIO(uint32_t dio_pin, uint32_t toggle_num,
              uint32_t toggle_duration_us, uint32_t sys_clk_MHz)
{
    uint32_t i;

    /* Clear pad retention */
    ACS_WAKEUP_CTRL->PADS_RETENTION_EN_ALIAS = PADS_RETENTION_DISABLE_BITBAND;

    Sys_DIO_Config(dio_pin, DIO_MODE_GPIO_OUT_0);

    /* Toggle the pin */
    i = 0;
    while (true)
    {
        Sys_Watchdog_Refresh();

        Sys_GPIO_Toggle(dio_pin);

        i++;

        if (i == toggle_num)
        {
            break;
        }

        /* Delay for a while, assume clock is sys_clk_MHz [MHz] */
        Sys_Delay_ProgramROM((uint32_t)(toggle_duration_us * sys_clk_MHz));
    }

    Sys_Watchdog_Refresh();
}

/* ----------------------------------------------------------------------------
 * Function      : void Toggle_APP_START_PIN(void)
 * ----------------------------------------------------------------------------
 * Description   : Toggles a specific pin to notify that the application
 *                 gets started
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Toggle_APP_START_PIN(void)
{
    Buzz_DIO(APP_START_PIN, APP_START_TOGGLE_NUM,
             APP_START_TOGGLE_DURATION_US, 3);
}

/* ----------------------------------------------------------------------------
 * Function      : void Main_Application(void)
 * ----------------------------------------------------------------------------
 * Description   : - The main application
 *                 - As an illustration, simply turns on the LED 1 on the
 *                   evaluation and development board for a while
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Main_Application(void)
{
    /* Disable DIO4 and DIO5 to avoid current consumption on VDDO */
    Sys_DIO_Config(4, DIO_MODE_DISABLE | DIO_NO_PULL);
    Sys_DIO_Config(5, DIO_MODE_DISABLE | DIO_NO_PULL);

    Sys_DIO_Config(LED_PIN, DIO_MODE_GPIO_OUT_0);

    Sys_GPIO_Set_High(LED_PIN);

    Sys_Watchdog_Refresh();

    /* Delay for a while, assume clock is 8 MHz */
    Sys_Delay_ProgramROM((uint32_t)(LED_ON_DURATION_US * 8));

    Sys_Watchdog_Refresh();

    Sys_GPIO_Set_Low(LED_PIN);
}

/* ----------------------------------------------------------------------------
 * Function      : void WAKEUP_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Things to do once waking up from standby power mode
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void WAKEUP_IRQHandler(void)
{
    /* Things to do once waking up from standby power mode */

    Buzz_DIO(WAKEUP_IRQ_PIN, WAKEUP_IRQ_TOGGLE_NUM,
             WAKEUP_IRQ_TOGGLE_DURATION_US, 3);
}
