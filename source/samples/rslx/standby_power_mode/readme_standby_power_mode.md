Standby Power Mode Sample Code
==============================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

This sample project demonstrates a simple application that:

 1. Sets DIO 6 high then low. On the RSL10 Evaluation and Development Board,
    this toggle will briefly flash LED 1
    
 2. Switches to standby power mode to save power
 
 3. Wakes up on RTC alarm and then restarts the executions from step 1
  
The source code exists in a `code` folder, and application-related include
header files are in the `include` folder.

Select the project configuration according to the chip ID version of 
RSL10. The appropriate libraries and include files are loaded according to 
the build configuration that you choose. Use **Debug** for CID 101.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).