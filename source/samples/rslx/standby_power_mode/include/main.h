/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * main.h
 * - Main application header
 * ----------------------------------------------------------------------------
 * $Revision: 1.11 $
 * $Date: 2018/03/15 15:42:18 $
 * ------------------------------------------------------------------------- */

#ifndef MAIN_H
#define MAIN_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/

/* Clear all reset flags in ACS_RESET_STATUS */
#define ACS_RESET_STATUS_FLAG_CLEAR     (uint32_t)(0x7F)

/* Clear all reset flags in DIG_RESET_STATUS */
#define DIG_RESET_STATUS_FLAG_CLEAR     (uint32_t)(0xF0)

#define APP_START_PIN                   1
#define APP_START_TOGGLE_NUM            10
#define APP_START_TOGGLE_DURATION_US    200000

#define WAKEUP_IRQ_PIN                  1
#define WAKEUP_IRQ_TOGGLE_NUM           10
#define WAKEUP_IRQ_TOGGLE_DURATION_US   10000

#define AFTER_WK_IRQ_PIN                3
#define AFTER_WK_IRQ_TOGGLE_NUM         10
#define AFTER_WK_IRQ_TOGGLE_DURATION_US 10000

/* Pin connected to LED on the evaluation board */
#define LED_PIN                         6

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    12

#define LED_ON_DURATION_US              1000000

/* Enable/disable buck converter
 * Options: VCC_BUCK_BITBAND or VCC_LDO_BITBAND */
#define VCC_BUCK_LDO_CTRL               VCC_BUCK_BITBAND

/* Sleep duration
 * Options:  RTC_ALARM_[ 1 | 2 | 4 | ... | 64 ]S, RTC_ALARM_ZERO */
#define RTC_ALARM_DURATION              RTC_ALARM_16S

#define MEM_CONFIGURATIONS              (PROM_POWER_ENABLE  | \
                                         FLASH_POWER_ENABLE | \
                                         DRAM2_POWER_ENABLE)

/* XTAL32K ITRIM value set point */
#define XTAL32K_ITRIM_VALUE             0xF

/* XTAL32K ITRIM value set point */
#define XTAL32K_CLOAD_TRIM_VALUE        0x09

/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/

void Buzz_DIO(uint32_t dio_pin, uint32_t toggle_num,
              uint32_t toggle_duration_us, uint32_t sys_clk_MHz);

void Toggle_APP_START_PIN(void);

void Main_Application(void);

void WAKEUP_IRQHandler(void);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* MAIN_H */
