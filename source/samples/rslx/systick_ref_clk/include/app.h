/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 * ----------------------------------------------------------------------------
 * app.h
 * - Overall application header file for the systick_ref_clk sample
 *   application
 * ------------------------------------------------------------------------- */

#ifndef APP_H
#define APP_H

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>

/* Defines */
#define CONCAT(x, y)                    x##y
#define DIO_SRC(x)                      CONCAT(DIO_SRC_DIO_, x)

/* DIO number that is connected to push button of EVB */
#define BUTTON_DIO                      5

/* DIO number that is connected to LED of EVB */
#define LED_DIO                         6

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    12

/* ----------------------------------------------------------------------------
 * Forward declaration
 * ------------------------------------------------------------------------- */

/* Interrupt handler function(s) */
extern void SysTick_Handler(void);

extern void DIO0_IRQHandler(void);

/* local function(s) */
void Initialize(void);

void SysTickConfigLaunch(void);

#endif    /* APP_H_ */
