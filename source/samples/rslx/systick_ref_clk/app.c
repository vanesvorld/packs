/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Simple application using Cortex-M3 timer or timer peripheral
 * ----------------------------------------------------------------------------
 * $Revision: 1.11 $
 * $Date: 2019/12/19 17:08:47 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>

/* ----------------------------------------------------------------------------
 * Function      : void SysTick_Handler(void)
 * ----------------------------------------------------------------------------
 * Description   : SysTick timer handler
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SysTick_Handler(void)
{
    Sys_GPIO_Toggle(LED_DIO);
    PRINTF("LED_%s\n", (DIO->CFG[LED_DIO] & 0x1 ? "ON" : "OFF"));
}

/* ----------------------------------------------------------------------------
 * Function      : void DIO0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : define how much Ticks needs to be applied.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO0_IRQHandler(void)
{
    static uint8_t ignore_next_dio_int = 0;
    if (ignore_next_dio_int)
    {
        ignore_next_dio_int = 0;
    }
    else if (DIO_DATA->ALIAS[BUTTON_DIO] == 0)
    {
        /* Button is pressed: Ignore next interrupt.
         * This is required to deal with the debounce circuit limitations. */
        ignore_next_dio_int = 1;
        PRINTF("BUTTON_PRESSED\n");

        /* Configure SysTick timer and launch it */
        SysTickConfigLaunch();
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SysTickConfigLaunch(void)
 * ----------------------------------------------------------------------------
 * Description   : Init. systick timer and launch it.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SysTickConfigLaunch(void)
{
    static uint32_t sys_tick_div = 0;

    /* select divider to apply */
    if (sys_tick_div == 100)
    {
        sys_tick_div = 1000;
    }
    else
    {
        sys_tick_div = 100;
    }

    /* 1. Disable timer.
     * This step is optional. It's recommended for reusable code because
     * the timer could have been enabled previously.
     */
    SysTick->CTRL = 0;

    /* 2. Write new reload value.
     * The reload value should be the interval value -1.
     * 1M is given by the slowclk prescale value which is around 1 MHz
     */
    SysTick->LOAD = 1000000 / sys_tick_div;
    PRINTF("SYSTICK_LOAD_VALUE=%d\n", SysTick->LOAD);

    /* 3. Clear counter value. Write any value to clear counter. */
    SysTick->VAL  = 0;

    /* 4.  Start counter */
    SysTick->CTRL = ((0 << SysTick_CTRL_CLKSOURCE_Pos) |
                     (1 << SysTick_CTRL_TICKINT_Pos)   |
                     (1 << SysTick_CTRL_ENABLE_Pos));
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts and
 *                 configuring the required DIOs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Setup DIO5 as a GPIO input with interrupts on falling edges, DIO6 as a
     * GPIO output. Use the integrated debounce circuit to ensure that only a
     * single interrupt event occurs for each push of the pushbutton.
     * The debounce circuit always has to be used in combination with the
     * transition mode to deal with the debounce circuit limitations.
     * A debounce filter time of 50 ms is used. */
    Sys_DIO_Config(LED_DIO, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(BUTTON_DIO, DIO_MODE_GPIO_IN_0 | DIO_WEAK_PULL_UP |
                   DIO_LPF_DISABLE);
    Sys_DIO_IntConfig(0,
                      DIO_EVENT_TRANSITION | DIO_SRC(BUTTON_DIO) |
                      DIO_DEBOUNCE_ENABLE,
                      DIO_DEBOUNCE_SLOWCLK_DIV1024, 49);

    /* set Priority for DIO0 Interrupt
     * Service DIO0 irq will be made only when others finished (side effects on
     * the toggling mechanism) */
    NVIC_SetPriority(DIO0_IRQn, (1 << __NVIC_PRIO_BITS) - 1);

    /* Enable interrupts at peripheral level */
    NVIC_EnableIRQ(DIO0_IRQn);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system, then toggle DIO6 as controlled by
 *                 DIO5 (press to modify the blink frequency).
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* Initialize the system */
    Initialize();
    PRINTF("DEVICE_INITIALIZED\n");
    SysTickConfigLaunch();

    /* Spin loop */
    while (1)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* Core goes into sleep mode and waits on an interrupt */
        __WFI();
    }
}
