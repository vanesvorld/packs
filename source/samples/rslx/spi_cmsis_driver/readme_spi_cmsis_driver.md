SPI CMSIS-Driver Sample Code
============================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software Development Kit
      (SDK).

Overview
--------

This sample project demonstrates how to simultaneously operate two SPI
instances (SPI0 and SPI1) using the SPI CMSIS-Driver. 

To use this sample application, the `ARM.CMSIS` pack must be installed in your 
IDE. In **CMSIS Pack Manager**, on the right panel, you can see the **Packs 
and Examples** view. In the **Packs** view, you will see **CMSIS packs**. Find 
`ARM.CMSIS` and click on the **Install** button.

Additional information about the application:

1) SPI0 is configured as master and SPI1 as slave.

2) Both SPI instances transmit/receive an SPI frame containing the string 
   message "RSL10 SPI TEST".

3) The transfer is started by SPI0 (master) when the DIO5 button on the board
   is pressed.

4) The application blinks the LED (DIO6) for 0.5 seconds, to indicate that the 
   received frame matches the expected string: "RSL10 SPI TEST". The 
   LED blinks for each SPI instance. Therefore, if SPI0 and SPI1 are connected
   and the data has been transferred correctly, the LED blinks twice.

5) If an overrun or underrun error is detected, the application blinks the LED
   5 times very fast (for 0.05 seconds) for each error occurrence.

6) RSL10 is configured to execute at 48 MHz, with an SPI BUS speed of 
   750000 bps and a word size of 8 bits. Bus speed and word size can be 
   configured on `app.h`.
   
7) The application can be tested with two RSL10 Evaluation and Development 
   Boards linked through an SPI connection, or using a single board connecting
   SPI0 <-> SPI1. Check the *Hardware Requirements* section for more details.
    
8) In order to demonstrate how to use the `ABORT_TRANSFER` feature, the 
   application constantly monitors (polls) the content being received on SPI1
   (slave). If the data received so far does not match the expected data, the 
   application aborts the ongoing transfer. This is also useful for avoiding 
   the accumulation of errors between transfers.

Hardware Requirements
---------------------
At least one RSL10 Evaluation and Development Board is required.
If only one board is available, connect SPI0 to SPI1 as follows:

        Master                     Slave
    SPI0_SCLK(SPI_SC)   ->    SPI1_SCLK(DIO13)
    SPI0_SSEL(DIO10)    ->    SPI1_SSEL(DIO14)
    SPI0_MISO(DIO0)     <-    SPI1_MISO(DIO1)
    SPI0_MOSI(DIO12)    ->    SPI1_MOSI(DIO15)
                            
If two boards are available, SPI0 can be connected to SPI1 as follows:

     RSL10 EVB #1               RSL10 EVB #2
        Master                     Slave
    SPI0_SCLK(SPI_SC)   ->    SPI1_SCLK(DIO13)
    SPI0_SSEL(DIO10)    ->    SPI1_SSEL(DIO14)
    SPI0_MISO(DIO0)     <-    SPI1_MISO(DIO1)
    SPI0_MOSI(DIO12)    ->    SPI1_MOSI(DIO15)

        Slave                     Master
    SPI1_SCLK(DIO13)    <-    SPI0_SCLK(SPI_SC)
    SPI1_SSEL(DIO14)    <-    SPI0_SSEL(DIO10)
    SPI1_MISO(DIO1)     ->    SPI0_MISO(DIO0)
    SPI1_MOSI(DIO15)    <-    SPI0_MOSI(DIO12)

        Slave                     Master
         GND            <->        GND
                      
EVB #1 Slave GND <-> GND EVB #2

Using two boards, you can opt to connect only a single Master from one board
to the Slave of another board.
  
  
Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.
  
Verification
------------
To verify that the application is working correctly, connect SPI0 to SPI1,
with either one or multiple boards loaded with the SPI program. Press the push
button on the board (DIO5) and observe whether the LED (DIO6) blinks according
to your connection configuration:

1) If you are using a single board, the LED blinks twice, to indicate that 
   both SPI0 and SPI1 have received the message from each other correctly.
   
2) If you are using two boards and have connected all the wires, the LED 
   blinks twice on each board, indicating that SPI0 and SPI1 have each 
   received the message correctly from the other board.
   
3) If you are using two boards but have opted to connect only the Master of 
   one board to the Slave of the other board, the LED blinks only once on
   each board, to indicate that only a single SPI instance has received the 
   message.

 
The source code exists in `app.c` and `app.h`. Driver configuration is 
specified in `RTE_Device.h`.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1)  Connect DIO12 to ground.

2)  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
	
3)  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can now work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
