                           DMIC and OD Sample Code
                           =======================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in Software_Use_Agreement.rtf
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project demonstrates an application that:
 
1.  Receives audio from DMIC0 and DMIC1 in 18-bit mode using ARM Cortex-M3
    processor.

2.  Allows user to switch among DMIC0, DMIC1, DMIC0+1 (mixed of DMIC 
    channels) and DMIC0-1 (with a delay applied to DMIC1 to do 
    beamforming) streams on OD, by pressing on the push button connected 
    to DIO5.
    
3.  Uses the DMIC_OD interrupt to process the DMICs samples and write them
    into the OD_DATA register.
    
4.  Uses the following settings for the blocks:
        SYSCLK at 8 MHz
        AUDIOCLK at 2 MHz
        AUDIOSLOWCLK at 1 MHz
        Decimation Rate 64 (sampling frequency = 31250 Hz)
        DMIC at AUDIOCLK (2 MHz)
        OD at AUDIOSLOWCLK (1 MHz)
        Use MSB mode on both DMIC and OD
        Use DC removal filters on both DMIC and OD
    
This sample project is structured as follows:

Application-related include header files are in the "include" folder and the 
main() function "app.c" is located in the parent directory.

Include
-------
    app.h  - Overall application header file
    
Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development 
Board. The application needs to be connected to two PDM microphones and to a
headphone. For RSL10 Evaluation and Development Board, connect:

    - DMIC_CLK  to DIO15
    - DMIC_DATA to DIO14
 
    - Headphone+ to DIO9 through an inductor (e.g. 680 uH)
    - Headphone- to DIO8 through an inductor (e.g. 680 uH)
 
 The inductors are required to filter high frequency currents, unless the
 headphone has sufficient self-inductance.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
Getting Started Guide for your IDE for more information.

Verification
------------
The DMIC OD application can be verified by connecting two DMICs and a 
headphone to RSL10. Pressing the button will change the mode. The LEFT_CHANNEL
and RIGHT_CHANNEL can be verified putting sound only in one of the DMIC 
channels. The Mixed Mode will use both DMIC channels. The Delay Mode will have
a gain directional (The low frequencies and the sound from behind DMIC1 will
be attenuated).

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which will
    pause at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application will work
    properly.

==============================================================================
Copyright (c) 2017 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
