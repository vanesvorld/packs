/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 * ----------------------------------------------------------------------------
 * app.h
 * - Overall application header file for the DMIC OD sample application
 * ------------------------------------------------------------------------- */

#ifndef APP_H_
#define APP_H_

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/
#define CONCAT(x, y)                    x##y
#define DIO_SRC(x)                      CONCAT(DIO_SRC_DIO_, x)
#define BUTTON_DIO                      5
#define LED_DIO                         6
#define DMIC_CLK_DIO                    15
#define DMIC_DATA_DIO                   14
#define OD_P_DIO                        8
#define OD_N_DIO                        9

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    12

#define AUDIO_DMIC0_GAIN                0x800
#define AUDIO_DMIC1_GAIN                0x800
#define AUDIO_OD_GAIN                   0x800

#define AUDIO_CONFIG                    (OD_AUDIOSLOWCLK            | \
                                         DMIC_AUDIOCLK              | \
                                         DECIMATE_BY_64             | \
                                         OD_UNDERRUN_PROTECT_ENABLE | \
                                         OD_DATA_MSB_ALIGNED        | \
                                         DMIC0_DATA_MSB_ALIGNED     | \
                                         DMIC1_DATA_MSB_ALIGNED     | \
                                         OD_DMA_REQ_DISABLE         | \
                                         DMIC0_DMA_REQ_DISABLE      | \
                                         DMIC1_DMA_REQ_DISABLE      | \
                                         OD_INT_GEN_DISABLE         | \
                                         DMIC0_INT_GEN_ENABLE       | \
                                         DMIC1_INT_GEN_DISABLE      | \
                                         OD_ENABLE                  | \
                                         DMIC0_ENABLE               | \
                                         DMIC1_ENABLE)

#define DMIC_CFG_NO_DELAY               (DMIC0_DCRM_CUTOFF_20HZ | \
                                         DMIC1_DCRM_CUTOFF_20HZ | \
                                         DMIC1_DELAY_DISABLE    | \
                                         DMIC0_FALLING_EDGE     | \
                                         DMIC1_RISING_EDGE)

#define DMIC_CFG_DELAY                  (DMIC0_DCRM_CUTOFF_20HZ | \
                                         DMIC1_DCRM_CUTOFF_20HZ | \
                                         DMIC0_FALLING_EDGE     | \
                                         DMIC1_RISING_EDGE)

#define DISTANCE                        0.01
#define DELAY_NS                        (1000000000 * DISTANCE / 343)
#define FREQ_MHZ                        2
#define US_TO_NS                        1000

#define DELAY_CFG                      ((int32_t)(DELAY_NS * FREQ_MHZ * 8) / \
                                        (64 * US_TO_NS))
#define FRAC_DELAY_CFG                 ((int32_t)((DELAY_NS - \
                                                   (DELAY_CFG * 64 * US_TO_NS / \
                                                    (FREQ_MHZ * 8))) * FREQ_MHZ / US_TO_NS))

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/

extern void DIO0_IRQHandler(void);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* APP_H_ */
