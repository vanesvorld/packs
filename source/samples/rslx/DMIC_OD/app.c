/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Receive audio from DMIC0 and DMIC1 in 18-bit mode using ARM Cortex-M3
 *   processor.
 * - User can switch among DMIC0, DMIC1, DMIC0+1 (mixed of DMIC channels) and
 *   DMIC0-1 (with a delay applied to DMIC1 to do beam forming) streams on OD
 *   by pressing on the push button.
 * - User can change the gain of DMIC channels and OD in the code.
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2018/03/02 17:05:01 $
 * ------------------------------------------------------------------------- */

#include <app.h>

/* ----------------------------------------------------------------------------
 * Global variables declaration
 * --------------------------------------------------------------------------*/
enum mode
{
    LEFT_CHANNEL, RIGHT_CHANNEL, MIXED, DELAY
};
volatile enum mode od_mode;

/* ----------------------------------------------------------------------------
 * Function      : void DMIC_OUT_OD_IN_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Read the data from the DMIC0 and DMIC1 and write the new
 *                 value for the OD accordingly with the od_mode.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMIC_OUT_OD_IN_IRQHandler(void)
{
    int32_t od_value;
    int32_t dmic_value[2];
    dmic_value[0] = (int32_t)AUDIO->DMIC0_DATA;
    dmic_value[1] = (int32_t)AUDIO->DMIC1_DATA;

    /* Generate data for OD value based on mode selected */
    switch (od_mode)
    {
        case LEFT_CHANNEL:
        {
            od_value = dmic_value[0];
        }
        break;

        case RIGHT_CHANNEL:
        {
            od_value = dmic_value[1];
        }
        break;

        case MIXED:
        {
            od_value = (dmic_value[0] / 2) + (dmic_value[1] / 2);
        }
        break;

        default:    /* DELAY */
        {
            od_value = (dmic_value[0] / 2) - (dmic_value[1] / 2);
        }
        break;
    }

    /* Write data to OD value */
    AUDIO->OD_DATA = od_value;
}

/* ----------------------------------------------------------------------------
 * Function      : void DIO0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Modify the od_mode
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO0_IRQHandler(void)
{
    static uint8_t ignore_next_dio_int = 1;
    if (ignore_next_dio_int == 1)
    {
        ignore_next_dio_int = 0;
    }
    else if (DIO_DATA->ALIAS[BUTTON_DIO] == 0)
    {
        /* Button is pressed: Ignore next interrupt.
         * This is required to deal with the debounce circuit limitations. */
        ignore_next_dio_int = 1;

        /* Change the od_mode */
        switch (od_mode)
        {
            case LEFT_CHANNEL:
            {
                od_mode = RIGHT_CHANNEL;
                Sys_Audio_Set_DMICConfig(DMIC_CFG_NO_DELAY, 0);
            }
            break;

            case RIGHT_CHANNEL:
            {
                od_mode = MIXED;
                Sys_Audio_Set_DMICConfig(DMIC_CFG_NO_DELAY, 0);
            }
            break;

            case MIXED:
            {
                od_mode = DELAY;
                Sys_Audio_Set_DMICConfig(DMIC_CFG_DELAY |
                                         (DELAY_CFG <<
                                          AUDIO_DMIC_CFG_DMIC1_DELAY_Pos),
                                         FRAC_DELAY_CFG);
            }
            break;

            default:    /* DELAY */
            {
                od_mode = LEFT_CHANNEL;
                Sys_Audio_Set_DMICConfig(DMIC_CFG_NO_DELAY, 0);
            }
            break;
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, switching to
 *                 the 8 MHz clock (divided from the 48 MHz crystal) and
 *                 configuring the DIOs required for the DMIC and OD.
 *                 Enabling interrupts
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Prepare the 48 MHz crystal
     * Start and configure VDDRF */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider to generate an 8 MHz clock. */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_6_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to (divided 48 MHz) oscillator clock */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Configure AUDIOCLK to 2 MHz and AUDIOSLOWCLK to 1 MHz. The others
     * prescale values are the default value */
    Sys_Clocks_SystemClkPrescale1(PWM0CLK_PRESCALE_1 | PWM1CLK_PRESCALE_1 |
                                  UARTCLK_PRESCALE_1 | AUDIOCLK_PRESCALE_4 |
                                  AUDIOSLOWCLK_PRESCALE_2);

    /* Configure OD, DMIC0 and DMIC1 */
    Sys_Audio_Set_Config(AUDIO_CONFIG);

    /* Configure DMIC parameters */
    Sys_Audio_Set_DMICConfig(DMIC0_DCRM_CUTOFF_20HZ | DMIC1_DCRM_CUTOFF_20HZ |
                             DMIC1_DELAY_DISABLE | DMIC0_FALLING_EDGE |
                             DMIC1_RISING_EDGE, 0);

    /* Configure OD parameters */
    Sys_Audio_Set_ODConfig(DCRM_CUTOFF_20HZ | DITHER_ENABLE | OD_RISING_EDGE);

    /* Configure Gains for DMIC0, DMIC1 and OD */
    AUDIO->DMIC0_GAIN = AUDIO_DMIC0_GAIN;
    AUDIO->DMIC1_GAIN = AUDIO_DMIC1_GAIN;
    AUDIO->OD_GAIN    = AUDIO_OD_GAIN;

    /* Configure DIO used for DMIC
     * Disable JTAG data on DIO14 and DIO15 As they are used defined as
     * DMIC_DATA_DIO and DMIC_CLK_DIO */
    DIO_JTAG_SW_PAD_CFG->CM3_JTAG_DATA_EN_ALIAS = 0;
    Sys_Audio_DMICDIOConfig(DIO_6X_DRIVE | DIO_LPF_DISABLE | DIO_NO_PULL,
                            DMIC_CLK_DIO, DMIC_DATA_DIO, DIO_MODE_AUDIOCLK);

    /* Configure DIO used for OD */
    Sys_Audio_ODDIOConfig(DIO_6X_DRIVE | DIO_LPF_DISABLE | DIO_NO_PULL,
                          OD_P_DIO, OD_N_DIO);

    /* Setup DIO5 as a GPIO input with interrupts on transitions, DIO6 as a
     * GPIO output. Use the integrated debounce circuit to ensure that only a
     * single interrupt event occurs for each push of the pushbutton.
     * The debounce circuit always has to be used in combination with the
     * transition mode to deal with the debounce circuit limitations.
     * A debounce filter time of 50 ms is used. */
    Sys_DIO_Config(BUTTON_DIO, DIO_MODE_GPIO_IN_0 | DIO_WEAK_PULL_UP |
                   DIO_LPF_DISABLE);
    Sys_DIO_Config(LED_DIO, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_IntConfig(0, DIO_EVENT_TRANSITION | DIO_SRC(BUTTON_DIO) |
                      DIO_DEBOUNCE_ENABLE,
                      DIO_DEBOUNCE_SLOWCLK_DIV1024, 49);

    /* Enable interrupts */
    NVIC_EnableIRQ(DIO0_IRQn);
    NVIC_EnableIRQ(DMIC_OUT_OD_IN_IRQn);

    /* Unmask all interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system, the DMIC and the OD. Blinks the LED
 *                 depending on the current mode.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    uint8_t nbr_blinks;

    /* Initialize application global variable */
    od_mode = LEFT_CHANNEL;

    /* Initialize the system */
    Initialize();

    /* Spin loop */
    while (1)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* LEFT_CHANNEL mode: Blinks 1x
         * RIGHT_CHANNEL mode: Blinks 2x
         * MIXED mode: Blinks 3x
         * DELAY mode: Blinks 4x */
        switch (od_mode)
        {
            case LEFT_CHANNEL:
            {
                nbr_blinks = 1;
            }
            break;

            case RIGHT_CHANNEL:
            {
                nbr_blinks = 2;
            }
            break;

            case MIXED:
            {
                nbr_blinks = 3;
            }
            break;

            default:    /* DELAY */
            {
                nbr_blinks = 4;
            }
            break;
        }

        /* Flash user LED based on the OD mode */
        uint8_t i;
        for (i = 0; i < nbr_blinks; i++)
        {
            Sys_Delay_ProgramROM(0.1 * SystemCoreClock);
            Sys_GPIO_Set_High(LED_DIO);
            Sys_Delay_ProgramROM(0.1 * SystemCoreClock);
            Sys_GPIO_Set_Low(LED_DIO);
        }
        Sys_Delay_ProgramROM(0.5 * SystemCoreClock);
    }
}
