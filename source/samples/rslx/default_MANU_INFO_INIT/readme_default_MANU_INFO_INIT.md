Default System Initialization Function
======================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project:

- Erases the non-volatile record three (`NVR3`) area of flash
- Writes the `MANU_INFO_INIT` area of `NVR3` with:

     1.  A variable indicating the length of the function defined in (2)
     2.  A default system initialization function
     3.  A CRC-CCITT value calculated over the length variable and the
         initialization function's code.

If no errors are encountered, the LED blinks at a slower rate. If an error
occurs, the code exits to a spin loop in `testError()` and the LED blinks
at a faster rate.

The function written to the `MANU_INFO_INIT` area is used by the
`Sys_Initialize()` function of the Program ROM to load calibration settings
for the device. The provided default initialization function writes the
first calibration record for each of the following blocks to the appropriate
trim register:

- Supply Voltages:
    - Band gap
    - DC-DC
    - VDDC and VDDC standby
    - VDDM and VDDM standby
    - VDDRF
    - VDDPA

- Clocks:
    - RC oscillator (multiplied)
    - 32 kHz RC oscillator

You can modify this program as needed and overwrite the default system 
initialization application. We recommend using the release build because 
it is smaller and faster.

Note:   This application also saves and restores the device address and lock 
        information from the `NVR3` area when erasing `NVR3`.
        If the value of the `MANU_INFO_VERSION` field is 22 or higher, the `MANU_INFO_DCDC` field is renamed `MANU_INFO_VCC`, which is divided 
        into two 8-bit fields (specifically, bits 0 - 4 are the VCC trim 
        settings for LDO mode, and bits 8 - 12 are the VCC trim settings for 
        DC-DC buck converter mode). If the `MANU_INFO_VERSION` is lower than 
        22, the `MANU_INFO_DCDC` is valid for both LDO mode and DC-DC buck 
        converter mode.

This program writes a function equivalent to the default manufacturing
initialization function that is written to devices during manufacturing test.
To ensure that this program writes the same function that was programmed to
devices during manufacturing, this program's debug configuration matches the
release configuration.

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the
*Getting Started Guide* for your IDE for more information.

Verification
------------
To verify if this application has executed correctly, check whether the 
application has exited to the spin loop at the bottom of main, or if it has 
exited to the spin loop in the `testError()` function.

**Caution:** Erase the main flash or load another application right after 
executing this project; otherwise there is a chance that the `NVR3` space can 
be corrupted, rendering the RSL10 unusable as a result. 

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
