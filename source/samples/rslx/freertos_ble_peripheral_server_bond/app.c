/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2018
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Main for RTOS ble_peripherial_server_bond demo application.
 *
 * ble_peripherial_server_bond standalone application is copied from RSL10 SDK
 * demo applications, linked in this project under ble_peripherial_server_bond
 * folder with no changes in source code and reused in this RTOS demo
 * application.
 * ------------------------------------------------------------------------- */

#include <app.h>

/* attribute structure for Debug thread */
const osThreadAttr_t thread_debug_attr =
{
    .name = "thDbg",
    .priority = osPriorityLow,
    .stack_size = 640
};

/* attribute structure for BLE thread */
const osThreadAttr_t thread_ble_attr =
{
    .name = "thBLE",
    .priority = osPriorityNormal,
    .stack_size = 2048
};

volatile uint32_t malloc_failed_count = 0;

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the device, the RTOS and BLE stack, than runs
 *                 ble_peripherial_server_bond demo application.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* Ensure all priority bits are assigned as preemption priority bits.
     * Should not be changed! */
    NVIC_SetPriorityGrouping(0);

    /* Configure hardware and initialize BLE stack */
    Device_Initialize();
    SystemCoreClockUpdate();

    /* Debug/trace initialization. In order to enable UART or RTT trace,
     *  configure the 'RSL10_DEBUG' macro in app_trace.h */
    TRACE_INIT();
    PRINTF("RTOS + ble_peripheral_server_bond started (build date:%s,%s)\r\n", \
           __DATE__, __TIME__);
    PRINTF("SystemCoreClock = %ldHz\r\n", SystemCoreClock);

    /* RTOS  initialization */
    osKernelInitialize();
    PRINTF("RTOS kernel tick frequency = %ldHz\r\n", osKernelGetTickFreq());
    PRINTF("RTOS kernel system timer frequency = %ldHz\r\n", osKernelGetSysTimerFreq());

    /* Ensure that Priority Grouping was not changed during device initialization.
     * Call it after logs are initialized. */
    configASSERT(NVIC_GetPriorityGrouping() == 0);

    /* Create application main thread for BLE Stack */
    osThreadNew(vThread_BLE, NULL, &thread_ble_attr);

    /* Create debug task */
    osThreadNew(vThread_Debug, NULL, &thread_debug_attr);

    /* Start RTOS scheduler */
    if (osKernelGetState() == osKernelReady)
    {
        osKernelStart();
    }
}


/* ----------------------------------------------------------------------------
 * Function      : void vThread_Debug(void *argument)
 * ----------------------------------------------------------------------------
 * Description   : Debug Thread function.
 * Inputs        : - argument       - pointer to Thread arguments
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__NO_RETURN void vThread_Debug(void *argument)
{
    uint32_t runtime_s;
    uint32_t runtime_ms;

    while(true)
    {
        runtime_s = (uint32_t)osKernelGetTickCount() / osKernelGetTickFreq();
        runtime_ms = (uint32_t)osKernelGetTickCount() % osKernelGetTickFreq();
        PRINTF("[%.12lds%.3ldms] test\r\n", runtime_s, runtime_ms);

        PRINTF("Free Heap size: %ld (minimum free heap size = %ld)\r\n",
               xPortGetFreeHeapSize(),
               xPortGetMinimumEverFreeHeapSize());

        osDelay(APP_DEBUG_DELAY_TICKS);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void vThread_BLE(void *argument)
 * ----------------------------------------------------------------------------
 * Description   : Main BLE Thread function.
 * Inputs        : - argument       - pointer to Thread arguments
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
__NO_RETURN void vThread_BLE(void *argument)
{
    /* Run the following command when erasing flash/bond_list is desirable */
    /* BondList_RemoveAll(); */

    /* Configure application-specific advertising data and scan response  data*/
    APP_SetAdvScanData();

    /* Configure Battery Service Server */
    BASS_Initialize(APP_BAS_NB, APP_BASS_ReadBatteryLevel);
    BASS_NotifyOnBattLevelChange(TIMER_SETTING_S(1));     /* Periodically monitor the battery level. Only notify changes */
    BASS_NotifyOnTimeout(TIMER_SETTING_S(6));             /* Periodically notify the battery level to connected peers */
    APP_BASS_SetBatMonAlarm(BATMON_SUPPLY_THRESHOLD_CFG); /* BATMON alarm configuration */

    /* Configure Custom Service Server */
    CUSTOMSS_Initialize();
    CUSTOMSS_NotifyOnTimeout(TIMER_SETTING_S(6)); /* Notify client and fire CUSTOMSS_NTF_TIMEOUT periodically */

    /* Add application message handlers */
    MsgHandler_Add(TASK_ID_GAPM, APP_GAPM_GATTM_Handler);
    MsgHandler_Add(GATTM_ADD_SVC_RSP, APP_GAPM_GATTM_Handler);
    MsgHandler_Add(TASK_ID_GAPC, APP_GAPC_Handler);
    MsgHandler_Add(APP_LED_TIMEOUT, APP_LED_Timeout_Handler);
    MsgHandler_Add(APP_BATT_LEVEL_LOW, APP_BASS_BattLevelLow_Handler);

    /* Reset the GAP manager. Trigger GAPM_CMP_EVT / GAPM_RESET when finished. See APP_GAPM_GATTM_Handler */
    GAPM_ResetCmd();

    /* Event Kernel Scheduler runing in thread */
    while(true)
    {
        /* Dispatch all events in Kernel queue */
        Kernel_Schedule();

        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* OS delay */
        osDelay(APP_BLE_DELAY_TICKS);
    }
}
