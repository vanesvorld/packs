/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * main.c
 * - Simple application to configure the LPDSP32 JTAG port using DIOs
 * ----------------------------------------------------------------------------
 * $Revision: 1.6 $
 * $Date: 2018/03/02 17:05:02 $
 * ------------------------------------------------------------------------- */

#include <rsl10.h>

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO        (12)

/* Define the DIOs required for configuring the LPDSP32 JTAG connection */
#define JTAG_LPDSP32_TCK    (1)
#define JTAG_LPDSP32_TDI    (2)
#define JTAG_LPDSP32_TDO    (3)
#define JTAG_LPDSP32_TMS    (4)

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, configuring
 *                 the required DIOs and DIO interrupt,
 *                 updating SystemCoreClockUpdate and enabling interrupts.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system, then toggle DIO6 as controlled by
 *                 DIO5 (press to toggle input/output).
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* Initialize the system */
    Initialize();

    /* Enable the JTAG for the LPDSP32 using the DIOs we defined above */
    Sys_LPDSP32_DIOJTAG(DIO_WEAK_PULL_DOWN,
                        JTAG_LPDSP32_TDI, JTAG_LPDSP32_TMS, JTAG_LPDSP32_TCK,
                        JTAG_LPDSP32_TDO);

    /* Spin loop */
    while (1)
    {
        /* Refresh the watchdog timer at some rate */
        Sys_Watchdog_Refresh();
        Sys_Delay_ProgramROM(SystemCoreClock);
    }
}
