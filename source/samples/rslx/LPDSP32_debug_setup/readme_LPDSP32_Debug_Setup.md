LPDSP32 Debug Setup - Simple GPIO I/O Sample Code
=================================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project demonstrates a simple application that enables the LPDSP32 
JTAG port to be accessed from DIOs which are configured as such by the 
Arm Cortex-M3 processor.

The source code exists in `main.c`, with no defined header files.

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development 
Board.

For the purposes of this sample, the expectation is that an external JLink
JTAG programmer will be used to communicate with the JTAG port for the 
LPDSP32.

When connecting the JLink programmer to the board, the following connections
must be made (note that these DIO numbers correspond to the ones being set up
in `main.c`):

     JLink Connector          Evaluation and Development Board
    
        GND     (4)                    GND
        VTRef   (1)                    VBAT_OUT
        TCK     (9)                    DIO 1
        TDI     (5)                    DIO 2
        TDO     (13)                   DIO 3
        TMS     (7)                    DIO 4


Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the
*Getting Started Guide* for more information.


Verification
------------
To verify that this application is functioning correctly, configure the
LPDSP32 JTAG and prove that it can be connected from the LPDSP32 
development tool (CHESS IDE).

In order for the LPDSP32 debugger to be able to access the device, JTalk must
be executed on the PC and a JLink JTAG programmer attached as above. The 
ON Semiconductor testing setup uses a JLink Ultra+ programmer, and JTalk 
version L-2016.09.

Make the hardware connections above, then import, build and run the
sample code. This will enable the DIOs for JTAG.

In a command window, run the following command to configure the JTalk server:

    jtalk -c jlink -f 2000

Now from the LPDSP32 CHESS development tools IDE, select `lpdsp32_client` as
the debugger and start a debug session using any simple test program. If all 
is well, the debugger connects with no errors and starts a debugging session.


Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed due to the
application going to Sleep Mode or resetting continuously (either by design or
due to programming error). To circumvent this scenario a software recovery 
mode using DIO12 is implemented. This recovery mode is used in following 
steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (the application restarts, but pauses at the 
    beginning of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, it is required to
    disconnect DIO12 from ground and press the RESET button to have this
    application work properly.


***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
