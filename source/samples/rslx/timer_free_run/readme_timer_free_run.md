Timer Free Run Sample Code
==========================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project demonstrates an application that:

1.  Uses DIO5 as an input signal to select the LED blink frequency.
2.  Uses the timer[0] to generate the blink frequency in free run.
3.  Uses the following LED blink frequencies: f, f/2, f/4 and roll-back to f.

The source code exists in `app.c`, with no defined header files.

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the
*Getting Started Guide* for your IDE for more information.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a Software Recovery
Mode using DIO12 can be implemented with the following steps:
1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from 
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
