/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * fifo.c
 * - Software FIFO implementation
 *
 * Notes:
 *     If read ptr == write ptr we assume the buffer is empty and overwrite,
 *     this means data may be lost if the data being written in pefectly aligns
 *     with the offsets. Therefore, if we write data that then leaves the write
 *     pointer equal to the read pointer we will report a FIFO Overflow
 *
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2019/07/29 19:37:48 $
 * ------------------------------------------------------------------------- */

#include <string.h>
#include <malloc.h>
#include "fifo.h"

#define FL_NONE        (0)
#define FL_STATIC    (1)

/* Define the control structure we use to manage the FIFO */
typedef struct
{
    uint8_t flags;
    uint16_t size;
    uint8_t *buffer;
    uint8_t *rdPtr;
    uint8_t *wrPtr;
} tFIFO, *pFIFO;

/* ----------------------------------------------------------------------------
 * Function      : pFIFO getFIFO(swFIFO fifo)
 * ----------------------------------------------------------------------------
 * Description   : Helper method to provide the internal representation of
 *                 a swFIFO handle
 * Inputs        : fifo - The swFIFO handle to interpret.
 * Outputs       : Pointer to the swFIFO control struct
 * Assumptions   : No checking is done to ensure the FIFO structure is valid
 * ------------------------------------------------------------------------- */
static pFIFO getFIFO(swFIFO fifo)
{
    return ((pFIFO)fifo);
}

/* ----------------------------------------------------------------------------
 * Function      : swFIFO initialise(pFIFO fifo, uint8_t flags, uint16_t size)
 * ----------------------------------------------------------------------------
 * Description   : Helper method to initialise a new swFIFO structure
 * Inputs        : fifo - The swFIFO handle to interpret.
 *                 flags - indicator ifthis FIFO is statically or dynamically
 *                 alocated
 *                 size - the size of the usable buffer in bytes.
 * Outputs       : swFIFO handle to an initialised structure
 * Assumptions   : No checking is done to ensure the FIFO structure is valid
 * ------------------------------------------------------------------------- */
static swFIFO initialise(pFIFO fifo, uint8_t flags, uint16_t size)
{
    if (fifo != NULL)
    {
        fifo->flags  = flags;
        fifo->size   = size;
        fifo->buffer = ((uint8_t *)fifo) + sizeof(tFIFO);
        fifo->rdPtr  = fifo->buffer;
        fifo->wrPtr  = fifo->buffer;
    }

    return (fifo);
}

/* ----------------------------------------------------------------------------
 * Function      : swFIFO fifoCreate(uint16_t maxSize)
 * ----------------------------------------------------------------------------
 * Description   : Constructor method to allocate space for a swFIFO and
 *                 initialise it. This uses malloc to obtain the underlying
 *                 memory and as such the returned handle should be destroyed
 *                 using the "fifoDestroy" method when no longer needed.
 * Inputs        : maxSize - The size of the fifo in bytes
 * Outputs       : swFIFO handle to an initialised structure
 * Assumptions   : No checking is done to ensure the FIFO structure is valid
 * ------------------------------------------------------------------------- */
swFIFO fifoCreate(uint16_t maxSize)
{
    pFIFO fifo = (pFIFO)malloc(sizeof(tFIFO) + maxSize);
    return (initialise(fifo, FL_NONE, maxSize));
}

/* ----------------------------------------------------------------------------
 * Function      : swFIFO fifoCreateStatic(void *buffer, uint16_t maxSize)
 * ----------------------------------------------------------------------------
 * Description   : Constructor method to initialise space for a swFIFO.
 * Inputs        : buffer - The buffer into which the FIFO should be placed.
 *                 maxSize - The size of the fifo in bytes.
 * Outputs       : swFIFO handle to an initialised structure.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
swFIFO fifoCreateStatic(void *buffer, uint16_t maxSize)
{
    pFIFO fifo = (pFIFO)((maxSize > sizeof(tFIFO)) ? buffer : NULL);
    return (initialise(fifo, FL_STATIC, maxSize - sizeof(tFIFO)));
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t fifoDestroy(swFIFO *fifoPtr)
 * ----------------------------------------------------------------------------
 * Description   : Destructor method to destroy a swFIFO.
 * Inputs        : fifoPtr - pointer to a swFIFO handle.
 * Outputs       : FIFO_ERROR_NULL if the incoming pointer is NULL
 *                 FIFO_NO_ERROR if the destroy can succeed
 *                 This will set the swFIFO handle to NULL.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint8_t fifoDestroy(swFIFO *fifoPtr)
{
    if (*fifoPtr == NULL)
    {
        return (FIFO_ERROR_NULL);
    }

    pFIFO fifo = getFIFO(*fifoPtr);
    if (!(fifo->flags & FL_STATIC))
    {
        free(fifo);
    }

    *fifoPtr = NULL;
    return (FIFO_NO_ERROR);
}

/* ----------------------------------------------------------------------------
 * Function      : uint16_t fifoUsed(swFIFO fifo)
 * ----------------------------------------------------------------------------
 * Description   : Returns the number of bytes used in the FIFO
 * Inputs        : fifo - the handle to the FIFO in question.
 * Outputs       : The number of bytes used in the FIFO
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint16_t fifoUsed(swFIFO fifo)
{
    pFIFO ptr  = getFIFO(fifo);
    int used = ptr->wrPtr - ptr->rdPtr;
    if (used < 0)
    {
        used += ptr->size + 1;
    }
    return (used);
}

/* ----------------------------------------------------------------------------
 * Function      : uint16_t fifoAvailable(swFIFO fifo)
 * ----------------------------------------------------------------------------
 * Description   : Returns the number of bytes available in the FIFO
 * Inputs        : fifo - the handle to the FIFO in question.
 * Outputs       : The number of bytes unused in the FIFO
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint16_t fifoAvailable(swFIFO fifo)
{
    pFIFO ptr = getFIFO(fifo);
    return (ptr->size - fifoUsed(fifo));
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t fifoPush(swFIFO fifo, void *buffer, uint16_t size)
 * ----------------------------------------------------------------------------
 * Description   : Pushes the specified buffer onto the FIFO if it can.
 * Inputs        : fifo - the handle to the FIFO in question.
 *                 buffer - a pointer to the base of a buffer to be pushed.
 *                 size - the number of bytes to push
 * Outputs       : FIFO_ERROR_OVERFLOW if there is no room in the FIFO
 *                 FIFO_NO_ERROR if the data can be pushed.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint8_t fifoPush(swFIFO fifo, void *buffer, uint16_t size)
{
    pFIFO ptr = getFIFO(fifo);

    /* Make sure the data we want to add fits before adding it */
    if (size >= fifoAvailable(ptr))
    {
        return (FIFO_ERROR_OVERFLOW);
    }

    /* okay, data will fit, so we can add it (This can be optimised later) */
    uint8_t *buffPtr = (uint8_t *)buffer;
    uint8_t *extent  = ptr->buffer + ptr->size;
    while (size-- > 0)
    {
        *ptr->wrPtr++ = *buffPtr++;
        if (ptr->wrPtr > extent)
        {
            ptr->wrPtr = ptr->buffer;
        }
    }

    return (FIFO_NO_ERROR);
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t fifoPop(swFIFO fifo, void *buffer, uint16_t size)
 * ----------------------------------------------------------------------------
 * Description   : Pops the specified number of bytes from the FIFO into the
 *                 buffer.
 * Inputs        : fifo - the handle to the FIFO in question.
 *                 buffer - a pointer to the base of a buffer to write the data.
 *                 size - the number of bytes to pop
 * Outputs       : FIFO_ERROR_UNDERFLOW if the FIFO does not contain size bytes.
 *                 FIFO_NO_ERROR if the data can be popped.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint8_t fifoPop(swFIFO fifo, void *buffer, uint16_t size)
{
    pFIFO ptr = getFIFO(fifo);

    /* Make sure the data we want to read is available before reading it */
    if (size > fifoUsed(ptr))
    {
        return (FIFO_ERROR_UNDERFLOW);
    }

    /* okay, data is available so we can read it (This can be optimised later)
     * */
    uint8_t *buffPtr = (uint8_t *)buffer;
    uint8_t *extent  = ptr->buffer + ptr->size;
    while (size-- > 0)
    {
        *buffPtr++ = *ptr->rdPtr++;
        if (ptr->rdPtr > extent)
        {
            ptr->rdPtr = ptr->buffer;
        }
    }

    return (FIFO_NO_ERROR);
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t fifoPush8(swFIFO fifo, uint8_t value)
 * ----------------------------------------------------------------------------
 * Description   : Pushes the given byte into FIFO.
 * Inputs        : fifo - the handle to the FIFO in question.
 *                 value - a byte value to be pushed
 * Outputs       : FIFO_ERROR_OVERFLOW if there is no room in the FIFO.
 *                 FIFO_NO_ERROR if the data can be pushed.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint8_t fifoPush8(swFIFO fifo, uint8_t value)
{
    return (fifoPush(fifo, &value, sizeof(uint8_t)));
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t fifoPop8(swFIFO fifo, uint8_t *value)
 * ----------------------------------------------------------------------------
 * Description   : Pops a byte from the FIFO into the value pointed to.
 * Inputs        : fifo - the handle to the FIFO in question.
 *                 value - a pointer to a byte
 * Outputs       : FIFO_ERROR_UNDERFLOW if the FIFO does not contain a byte.
 *                 FIFO_NO_ERROR if the data can be popped.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint8_t fifoPop8(swFIFO fifo, uint8_t *value)
{
    return (fifoPop(fifo, value, sizeof(uint8_t)));
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t fifoPush16(swFIFO fifo, uint16_t value)
 * ----------------------------------------------------------------------------
 * Description   : Pushes the given short into FIFO.
 * Inputs        : fifo - the handle to the FIFO in question.
 *                 value - a two byte value to be pushed
 * Outputs       : FIFO_ERROR_OVERFLOW if there is no room in the FIFO.
 *                 FIFO_NO_ERROR if the data can be pushed.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint8_t fifoPush16(swFIFO fifo, uint16_t value)
{
    return (fifoPush(fifo, &value, sizeof(uint16_t)));
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t fifoPop16(swFIFO fifo, uint16_t *value)
 * ----------------------------------------------------------------------------
 * Description   : Pops a short from the FIFO into the value pointed to.
 * Inputs        : fifo - the handle to the FIFO in question.
 *                 value - a pointer to a short int (two bytes)
 * Outputs       : FIFO_ERROR_UNDERFLOW if the FIFO does not contain two bytes.
 *                 FIFO_NO_ERROR if the data can be popped.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint8_t fifoPop16(swFIFO fifo, uint16_t *value)
{
    return (fifoPop(fifo, value, sizeof(uint16_t)));
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t fifoPush32(swFIFO fifo, uint32_t value)
 * ----------------------------------------------------------------------------
 * Description   : Pushes the given word into FIFO.
 * Inputs        : fifo - the handle to the FIFO in question.
 *                 value - a 32 bit word value to be pushed
 * Outputs       : FIFO_ERROR_OVERFLOW if there is no room in the FIFO.
 *                 FIFO_NO_ERROR if the data can be pushed.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint8_t fifoPush32(swFIFO fifo, uint32_t value)
{
    return (fifoPush(fifo, &value, sizeof(uint32_t)));
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t fifoPop32(swFIFO fifo, uint32_t *value)
 * ----------------------------------------------------------------------------
 * Description   : Pops a word from the FIFO into the value pointed to.
 * Inputs        : fifo - the handle to the FIFO in question.
 *                 value - a pointer to a word (four bytes)
 * Outputs       : FIFO_ERROR_UNDERFLOW if the FIFO does not contain four bytes.
 *                 FIFO_NO_ERROR if the data can be popped.
 * Assumptions   : No checking is done to ensure the FIFO structure is valid.
 * ------------------------------------------------------------------------- */
uint8_t fifoPop32(swFIFO fifo, uint32_t *value)
{
    return (fifoPop(fifo, value, sizeof(uint32_t)));
}
