/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_init.c
 * - Contains application initialization code for hardware
 * ----------------------------------------------------------------------------
 * $Revision: 1.22 $
 * $Date: 2019/09/23 17:39:51 $
 * ------------------------------------------------------------------------- */

#include "app.h"

#include "codecs/codec.h"
#include "codecs/baseDSP/baseDSPCodec.h"
#include "sharedBuffers.h"

/* Define a buffer to back the lpdsp32 incoming sample FIFO */
static uint8_t lpdsp32InSampleBuffer[FRAME_LENGTH * 2 * 2];

/* And two buffers for the output sample FIFOs */
static uint8_t lpdsp32OutBufferLeft[TX_DATA_FIFO_LENGTH * 2];
static uint8_t lpdsp32OutBufferRight[TX_DATA_FIFO_LENGTH * 2];
/* Define the LPDSP32 Context */
LPDSP32Context lpdsp32;

const struct wm8731_i2c_message
    wm8731_i2c_message_buffer[WM8731_I2C_MESSAGE_BUFFER_SIZE] =
{ { WM8731_RESET_REG_ADDR, WM8731_RESET_REG_VAL },
  { WM8731_POWER_DOWN_CTRL_REG_ADDR, WM8731_POWER_DOWN_CTRL_REG_VAL },
  { WM8731_DAI_REG_ADDR, WM8731_DAI_REG_VAL },
  { WM8731_LLINE_IN_REG_ADDR, WM8731_LLINE_IN_REG_VAL },
  { WM8731_RLINE_IN_REG_ADDR, WM8731_RLINE_IN_REG_VAL },
  { WM8731_LLINE_OUT_REG_ADDR, WM8731_LLINE_OUT_REG_VAL },
  { WM8731_RLINE_OUT_REG_ADDR, WM8731_RLINE_OUT_REG_VAL },
  { WM8731_DIGITAL_AUDIO_PATH_CTRL_REG_ADDR,
    WM8731_DIGITAL_AUDIO_PATH_CTRL_REG_VAL },
  { WM8731_ANALOG_AUDIO_PATH_CTRL_REG_ADDR,
    WM8731_ANALOG_AUDIO_PATH_CTRL_REG_VAL },
  { WM8731_SAMPLING_CTRL_REG_ADDR, WM8731_SAMPLING_CTRL_REG_VAL },
  { WM8731_ACTIVE_CTRL_REG_ADDR, WM8731_ACTIVE_CTRL_REG_VAL } };
volatile uint8_t i2c_tx_buffer_data[I2C_BUFFER_SIZE];
volatile uint8_t i2c_tx_buffer_index;

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_ASCC(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the Audio Sink Clock Counters
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize_ASCC(void)
{
    /* Configuration of Audio Sink Clock Counters (ASCC). */
    Sys_Audiosink_ResetCounters();
    Sys_Audiosink_InputClock(0,
                             ((uint32_t)(SAMPL_CLK <<
                                         DIO_AUDIOSINK_SRC_CLK_Pos)));
    Sys_Audiosink_Config(AUDIO_SINK_PERIODS_16, 0, 0);

    /* Start Audio Sink Phase and Period measurement. */
    AUDIOSINK_CTRL->PHASE_CNT_START_ALIAS  = PHASE_CNT_START_BITBAND;
    AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = PERIOD_CNT_START_BITBAND;

    /* ASCC interrupts. */
    NVIC_ClearPendingIRQ(AUDIOSINK_PHASE_IRQn);
    NVIC_EnableIRQ(AUDIOSINK_PHASE_IRQn);
    NVIC_ClearPendingIRQ(AUDIOSINK_PERIOD_IRQn);
    NVIC_EnableIRQ(AUDIOSINK_PERIOD_IRQn);
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_ASRC(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the ASRC and its input and output DMA channels
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize_ASRC(void)
{
    /* Setup DMA channel for transferring data into the ASRC
     * interface from memory. */
    Sys_DMA_ChannelConfig(
        ASRC_IN_IDX,
        TX_DMA_ASRC_IN,
        SUBFRAME_LENGTH << PCM_SPI_SHIFT,
        0,
        (uint32_t)&asrc_in_buf[0],
        (uint32_t)&ASRC->IN
        );
    Sys_ASRC_Config(0x0, PCM_SPI_ASRC_CONFIG);

    /* Clear any pending ASRC DMA channel requests. */
    Sys_DMA_ClearChannelStatus(ASRC_IN_IDX);

    /* Setup DMA channel for transferring data from ASRC output into memory. */
    Sys_DMA_ChannelConfig(ASRC_OUT_IDX, TX_DMA_ASRC_OUT, 1, 0,
                          (uint32_t)&ASRC->OUT, (uint32_t)&data_fifo_rec);

    /* Configure the ASRC block. */
    /* Enable DMA channel for ASRC output. */
    Sys_DMA_ClearChannelStatus(ASRC_OUT_IDX);
    Sys_DMA_ChannelEnable(ASRC_OUT_IDX);

    /* Clear pending DMA interrupt requests for ASRC output. */
    NVIC_ClearPendingIRQ(DMA_IRQn(ASRC_OUT_IDX));

    /* Set priority and enabled interrupt for DMA requests on ASRC output. */
    NVIC_SetPriority(DMA_IRQn(ASRC_OUT_IDX), 1);
    NVIC_EnableIRQ(DMA_IRQn(ASRC_OUT_IDX));

    /* Clear pending DMA interrupt requests for ASRC input */
    NVIC_ClearPendingIRQ(DMA_IRQn(ASRC_IN_IDX));

    /* Set priority and enabled interrupt for DMA requests on ASRC input. */
    NVIC_SetPriority(DMA_IRQn(ASRC_IN_IDX), 0);
    NVIC_EnableIRQ(DMA_IRQn(ASRC_IN_IDX));

    /* Setup DMA channel for restoring data from memory into
     * ASRC state machine. */
    Sys_DMA_ChannelConfig(MEMCPY_RESTORE_STATE_MEM,
                          DMA_RESTORE_STATE_MEM_CONFIG, 32, 0,
                          (uint32_t)&asrc_state_mem_rx[0][0],
                          (uint32_t)&ASRC->PHASE_INC);

    /* Clear the current status for the DMA. */
    Sys_DMA_ClearChannelStatus(MEMCPY_RESTORE_STATE_MEM);

    /* Clear pending DMA requests and configure interrupt. */
    NVIC_ClearPendingIRQ(DMA_IRQn(MEMCPY_RESTORE_STATE_MEM));
    NVIC_SetPriority(DMA_IRQn(MEMCPY_RESTORE_STATE_MEM), 2);
    NVIC_EnableIRQ(DMA_IRQn(MEMCPY_RESTORE_STATE_MEM));

    /* Setup DMA channel for saving data from ASRC state machine. */
    Sys_DMA_ChannelConfig(MEMCPY_SAVE_STATE_MEM, DMA_SAVE_STATE_MEM_CONFIG,
                          32, 0, (uint32_t)&ASRC->PHASE_INC,
                          (uint32_t)&asrc_state_mem_rx[0][0]);
    Sys_DMA_ChannelEnable(MEMCPY_SAVE_STATE_MEM);
    Sys_DMA_ClearChannelStatus(MEMCPY_SAVE_STATE_MEM);

    /* Clear pending DMA request and configure interrupt. */
    NVIC_ClearPendingIRQ(DMA_IRQn(MEMCPY_SAVE_STATE_MEM));
    NVIC_SetPriority(DMA_IRQn(MEMCPY_SAVE_STATE_MEM), 1);
    NVIC_EnableIRQ(DMA_IRQn(MEMCPY_SAVE_STATE_MEM));
}

/* ----------------------------------------------------------------------------
 * Function      : void App_CodecInitialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the codec subsystem
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void App_CodecInitialize(void)
{
    /* Initialize the dsp context */
    lpdsp32.state = DSP_STARTING;
    lpdsp32.dsp_side = PKT_LEFT;

    lpdsp32.incoming = fifoCreateStatic(lpdsp32InSampleBuffer, sizeof(lpdsp32InSampleBuffer));
    lpdsp32.outgoing[PKT_LEFT]  = fifoCreateStatic(lpdsp32OutBufferLeft, sizeof(lpdsp32OutBufferLeft));
    lpdsp32.outgoing[PKT_RIGHT] = fifoCreateStatic(lpdsp32OutBufferRight, sizeof(lpdsp32OutBufferRight));

    /* Set the mode and channel, at the moment the channel is encoded as the
     * top four bits of the field */
    lpdsp32.channels[PKT_LEFT].modeAndChannel = 0x00 | CODEC_MODE;
    lpdsp32.channels[PKT_RIGHT].modeAndChannel = 0x10 | CODEC_MODE;

    /* Set the frame size, block length and sample rate of the two channels to be the same */
    lpdsp32.channels[PKT_LEFT].frameSize = SUBFRAME_LENGTH;
    lpdsp32.channels[PKT_RIGHT].frameSize = SUBFRAME_LENGTH;
    lpdsp32.channels[PKT_LEFT].blockSize = CODEC_BLOCK_SIZE;
    lpdsp32.channels[PKT_RIGHT].blockSize = CODEC_BLOCK_SIZE;
    lpdsp32.channels[PKT_LEFT].sampleRate = CODEC_SAMPLE_RATE;
    lpdsp32.channels[PKT_RIGHT].sampleRate = CODEC_SAMPLE_RATE;

    /* Create a codec located in the configuration area */
    memset(Buffer.configuration, 0, CODEC_CONFIGURATION_SIZE);
    lpdsp32.codec = POPULATE_CODEC_FUNCTION(Buffer.configuration, CODEC_CONFIGURATION_SIZE);

    if (!codecIsCodec(lpdsp32.codec))
    {
        return;
    }

    /* initialise the codec */
    codecInitialise(lpdsp32.codec);

    /* Configure the static parts of the codec */
    codecSetStatusBuffer(lpdsp32.codec, Buffer.configuration, CODEC_CONFIGURATION_SIZE);
    codecSetOutputBuffer(lpdsp32.codec, Buffer.output, CODEC_OUTPUT_SIZE);
    codecSetInputBuffer(lpdsp32.codec, &Buffer.input[0][0], CODEC_INPUT_SIZE);

    /* we have a handshake protocol to ensure the DSP is alive and in synch
     * before we try to use it. */
    dspHandshake(lpdsp32.codec);

    lpdsp32.channels[PKT_LEFT].action  = CONFIGURE;
    lpdsp32.channels[PKT_RIGHT].action  = CONFIGURE;
    codecSetParameters(lpdsp32.codec, &lpdsp32.channels[PKT_LEFT]);
    codecSetParameters(lpdsp32.codec, &lpdsp32.channels[PKT_RIGHT]);
    codecConfigure(lpdsp32.codec);

    /* Now, after the additional configuration step has been issued,
     * setup the left and right channels to perform an encode with reset. */
    lpdsp32.channels[PKT_LEFT].action = CODEC_ACTION;
    lpdsp32.channels[PKT_RIGHT].action = CODEC_ACTION;
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_Common_Raw_Input_Type(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize common raw input peripherals
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize_Common_Raw_Input_Type(void)
{
#if (INPUT_INTRF == SPI_RX_RAW_INPUT || INPUT_INTRF == PCM_RX_RAW_INPUT)
    /* Configure the Audio Sink Clock Counters; required for the SPI and PCM
     * stream set */
    Initialize_ASCC();

    /* Configure the ASRC and the required DMA channels; required for the SPI
     * and PCM stream set */
    Initialize_ASRC();
#endif    /* if (INPUT_INTRF == SPI_RX_RAW_INPUT || INPUT_INTRF == PCM_RX_RAW_INPUT) */
    NVIC_SetPriority(DSP0_IRQn, 2);
    NVIC_EnableIRQ(DSP0_IRQn);

    /* Initialize the codec subsystem. */
    App_CodecInitialize();
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_Raw_SPI_Input_Type(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure SPI peripheral and DMA, wait for
 *                 beginning of a new frame on SPI
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize_Raw_SPI_Input_Type(void)
{
    /* Initialize SPI0 slave interface. */
    Sys_SPI_DIOConfig(0, SPI0_SELECT_SLAVE,
                      DIO_LPF_DISABLE | DIO_WEAK_PULL_UP, SPI_CLK_DO, SPI_CS_DO,
                      SPI_SER_DI,
                      SPI_SER_DO);

    /* Configure the SPI0 slave interface. */
    Sys_SPI_Config(0, SPI0_SELECT_SLAVE | SPI0_ENABLE |
                   SPI0_CLK_POLARITY_NORMAL | SPI0_CONTROLLER_DMA |
                   SPI0_MODE_SELECT_AUTO | SPI0_PRESCALE_16);

    /* Setup transfer parameters for 16-bit data, 1 byte per channel. */
    Sys_SPI_TransferConfig(0, SPI0_START | SPI0_RW_DATA | SPI0_CS_1 |
                           SPI0_WORD_SIZE_16);

    /* Setup DMA channel for transferring data from the SPI0 slave
     * interface into memory. */
    Sys_DMA_ChannelConfig(
        RX_DMA_NUM,
        DMA_RX_CONFIG,
        SUBFRAME_LENGTH * 2, 0, (uint32_t)&(SPI0->RX_DATA),
        (uint32_t)&spi_buf[0]);

    /* Block until SPI0 slave chip select transitions from low to
     * high state. */
    while (SPI0_CTRL1->SPI0_CS_ALIAS == SPI0_CS_0_BITBAND)
    {
    }
    while (SPI0_CTRL1->SPI0_CS_ALIAS == SPI0_CS_1_BITBAND)
    {
    }

    rx_DMA_ready = rx_DMA_ready_SPI;
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_Raw_PCM_Input_Type(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize PCM peripheral and DMA channel, configure the
 *                 audio shield if raw PCM source is audio shield
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize_Raw_PCM_Input_Type(void)
{
    /* Initialize and Configure PCM interface. */
    Sys_PCM_ConfigClk(PCM_SELECT_SLAVE, DIO_WEAK_PULL_UP, PCM_CLK_DO,
                      PCM_FRAME_SYNC, PCM_SER_DI, PCM_SER_DO, DIO_MODE_INPUT);
    Sys_PCM_Config(PCM_CFG_RX);

    /* Enable the PCM interface. */
    Sys_PCM_Enable();

    /* Setup DMA channel for PCM data transfer.
     * Using counter interrupt to setup dual buffer system to prevent read and
     * write access conflicts that result in audio artifacts.
     * Two times for each left and right channels, hence 4*SUBFRAME_LENGTH. */
    Sys_DMA_ChannelConfig(RX_DMA_NUM, DMA_RX_CONFIG,
                          4 * SUBFRAME_LENGTH,
                          2 * SUBFRAME_LENGTH,
                          (uint32_t)&(PCM->RX_DATA),
                          (uint32_t)&pcm_buf[0]);

    rx_DMA_ready = rx_DMA_ready_PCM;
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_PCM_I2C_Shield(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize PCM Codec Shield over I2C
 *                 audio shield if raw PCM source is audio shield
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize_PCM_I2C_Shield(void)
{
    uint32_t i;
    uint32_t int_mask   = __get_PRIMASK();
    uint32_t fault_mask = __get_FAULTMASK();

    /* Setup the audio Codec shield. */
    /* Configure I2C as Master to configure the shield. */
    Sys_I2C_Config(I2C_MASTER_SPEED_120 | I2C_CONTROLLER_CM3 |
                   I2C_STOP_INT_ENABLE | I2C_AUTO_ACK_DISABLE |
                   I2C_SAMPLE_CLK_ENABLE | I2C_SLAVE_DISABLE);

    /* Configure the DIOs for I2C, strong pull-up used to drive the line,
     * if external pull-up is used the DIO_NO_PULL can be used. */
    Sys_I2C_DIOConfig(DIO_6X_DRIVE | DIO_LPF_ENABLE | DIO_STRONG_PULL_UP,
                      I2C_SCL_DIO,
                      I2C_SDA_DIO);

    /* Enable interrupts */
    NVIC_EnableIRQ(I2C_IRQn);
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    /* Configure the PCM settings to shield over the I2C interface. */
    for (i = 0; i < WM8731_I2C_MESSAGE_BUFFER_SIZE; i++)
    {
        /* Wait until pending transaction is completed and bus is free. */
        while ((Sys_I2C_Get_Status() & (1 << I2C_STATUS_BUS_FREE_Pos)) !=
               I2C_BUS_FREE)
        {
            /* Refresh the watch-dog timer */
            Sys_Watchdog_Refresh();
        }

        /* Load a single frame to be transmitted over I2C into a buffer */
        i2c_tx_buffer_index   = 0;
        i2c_tx_buffer_data[0] = wm8731_i2c_message_buffer[i].register_address;
        i2c_tx_buffer_data[1] = wm8731_i2c_message_buffer[i].register_data;

        /* Initiate I2C transaction by generating a start condition on the bus.
         * Data is loaded from the I2C buffer to I2C registers in the
         * I2C IRQ. */
        Sys_I2C_StartWrite(WM8731_I2C_SLAVE_ADDRESS);
    }

    /* Wait until pending transaction is completed and bus is free. */
    while ((Sys_I2C_Get_Status() & (1 << I2C_STATUS_BUS_FREE_Pos)) !=
           I2C_BUS_FREE)
    {
        /* Refresh the watch-dog timer */
        Sys_Watchdog_Refresh();
    }

    __set_PRIMASK(int_mask);
    __set_FAULTMASK(fault_mask);
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_Raw_DMIC_Input_Type(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize DMIC peripheral and DMA channels
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize_Raw_DMIC_Input_Type(void)
{
    /* Configure AUDIOCLK to 3.2MHz */
    Sys_Clocks_SystemClkPrescale1(AUDIOCLK_PRESCALE_5);

    /* Setup DMIC interface */
    Sys_Audio_DMICDIOConfig(
        DIO_WEAK_PULL_UP,
        DMIC_CLK_PIN,
        DMIC_DATA_PIN,
        DIO_MODE_AUDIOCLK);
    Sys_Audio_Set_Config(DMIC_AUDIO_CFG);
    Sys_Audio_Set_DMICConfig(DMIC_CFG, 0);

    /* Set the DMIC gains to maximum value */
    AUDIO->DMIC0_GAIN = DMIC_MAX_GAIN;
    AUDIO->DMIC1_GAIN = DMIC_MAX_GAIN;

    /* Configure DMA channel for DMIC data transfer */
    Sys_DMA_ChannelConfig(RX_DMA_NUM,
                          DMA_RX_CONFIG,
                          SUBFRAME_LENGTH,
                          0,
                          (uint32_t)&(AUDIO->DMIC_DATA),
                          (uint32_t)dmic_buffer_in);

    rx_DMA_ready = rx_DMA_ready_DMIC;
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize_Transmitter_Audio_Input(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize transmitter audio Input
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize_Transmitter_Audio_Input(void)
{
    /* Check if source in/out is in RAW format in which case enable
     * the ASRC, ASCC and DSP function. */
    Initialize_Common_Raw_Input_Type();
#if (INPUT_INTRF == SPI_RX_RAW_INPUT)
    Initialize_Raw_SPI_Input_Type();
#elif (INPUT_INTRF == PCM_RX_RAW_INPUT)
    Initialize_Raw_PCM_Input_Type();
#elif (INPUT_INTRF == DMIC_RX_RAW_INPUT)
    Initialize_Raw_DMIC_Input_Type();
#endif    /* if (INPUT_INTRF == SPI_RX_RAW_INPUT) */
    /* Clear the current status for the DMA receive channel for
     * SPI or PCM input. */
    Sys_DMA_ClearChannelStatus(RX_DMA_NUM);

    /* Enable DMA receive channel. */
    Sys_DMA_ChannelEnable(RX_DMA_NUM);

    /* Clear pending DMA receive channel request and configure interrupt. */
    NVIC_ClearPendingIRQ(DMA_IRQn(RX_DMA_NUM));
    NVIC_SetPriority(DMA_IRQn(RX_DMA_NUM), 2);

    /* Enable DMA receive channel interrupt. */
    NVIC_EnableIRQ(DMA_IRQn(RX_DMA_NUM));
}

/* ----------------------------------------------------------------------------
 * Function      : void App_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system for proper application execution.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void App_Initialize(void)
{
    /* Initialize the ISR Vector Table address in VTOR. */
    SCB->VTOR = (unsigned int)(&ISR_Vector_Table);

    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all interrupts and clear any pending interrupts. */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Disable operation of DIO 13-15 in JTAG mode. */
    DIO_JTAG_SW_PAD_CFG->CM3_JTAG_DATA_EN_ALIAS =
        CM3_JTAG_DATA_DISABLED_BITBAND;
    DIO_JTAG_SW_PAD_CFG->CM3_JTAG_TRST_EN_ALIAS =
        CM3_JTAG_TRST_DISABLED_BITBAND;

    /* Test DIO13 to pause the program to make it easy to re-flash. */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Configure the current trim settings for VCC, VDDA. */
    ACS_VCC_CTRL->ICH_TRIM_BYTE  = VCC_ICHTRIM_16MA_BYTE;
    ACS_VDDA_CP_CTRL->PTRIM_BYTE = VDDA_PTRIM_16MA_BYTE;

    /* Start and configure VDDRF. */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up. */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Disable RF power amplifier. */
    ACS_VDDPA_CTRL->ENABLE_ALIAS = VDDPA_DISABLE_BITBAND;
    ACS_VDDPA_CTRL->VDDPA_SW_CTRL_ALIAS    = VDDPA_SW_VDDRF_BITBAND;

    /* Enable RF power switches. */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation. */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits. */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable the 48 MHz oscillator divider using the desired
     * pre-scale value. */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = RF_CK_DIV_PRESCALE_VALUE;

    /* Wait until 48 MHz oscillator is started. */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to (divided 48 MHz) oscillator clock. */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Configure clock dividers. */
    CLK->DIV_CFG0 = (SLOWCLK_PRESCALE_VALUE |
                     BBCLK_PRESCALE_VALUE   |
                     USRCLK_PRESCALE_1);

    CLK_DIV_CFG2->DCCLK_BYTE = DCCLK_PRESCALE_BYTE_VALUE;

    /* Wake-up and apply clock to the BLE base-band interface. */
    BBIF->CTRL    = (BB_CLK_ENABLE | BBCLK_DIVIDER_VALUE | BB_WAKEUP);

    /* Initialize Remote Microphone BLE application. */
    APP_RM_Init(APP_RM_AUDIO_CHANNEL);

    /* Enable 6dBM or 0dBM mode. */
#if (OUTPUT_POWER_6DBM)
    Sys_RFFE_SetTXPower(6);
#else    /* 0DBM */
    Sys_RFFE_SetTXPower(0);
#endif    /* CFG_6DBM */

    /* Enable Flash overlay. */
    memcpy((uint8_t *)PRAM0_BASE, (uint8_t *)FLASH_MAIN_BASE, PRAM0_SIZE);
    memcpy((uint8_t *)PRAM1_BASE, (uint8_t *)(FLASH_MAIN_BASE + PRAM0_SIZE),
           PRAM1_SIZE);
    memcpy((uint8_t *)PRAM2_BASE,
           (uint8_t *)(FLASH_MAIN_BASE + PRAM0_SIZE + PRAM1_SIZE),
           PRAM2_SIZE);
    memcpy((uint8_t *)PRAM3_BASE,
           (uint8_t *)(FLASH_MAIN_BASE + PRAM0_SIZE + PRAM1_SIZE + PRAM2_SIZE),
           PRAM3_SIZE);
    SYSCTRL->FLASH_OVERLAY_CFG  = 0xf;

    /* Enable CM3 loop cache. */
    SYSCTRL->CSS_LOOP_CACHE_CFG = CSS_LOOP_CACHE_ENABLE;

#if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD)
    Initialize_PCM_I2C_Shield();
#endif    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD) */

    /* Initialize the LED DIO and turn it off. */
    Sys_DIO_Config(LED_DIO_NUM, DIO_MODE_GPIO_OUT_0);

    Sys_DIO_Config(DEBUG_DIO_FIRST, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(DEBUG_DIO_SECOND, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(DEBUG_DIO_THIRD, DIO_MODE_GPIO_OUT_0);
    Sys_GPIO_Set_Low(DEBUG_DIO_FIRST);

    /* Initialize Transmitter Audio Input Source */
    Initialize_Transmitter_Audio_Input();

    /* Unmask interrupts. */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}
