/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_func.c
 * - Application functions
 * ----------------------------------------------------------------------------
 * $Revision: 1.24 $
 * $Date: 2019/09/23 17:39:51 $
 * ------------------------------------------------------------------------- */

#include "app.h"

#include "fifo.h"
#include "sharedBuffers.h"

/* The packet side is used to track the data coming in from the ASRC. */
PacketSide flag_packet_side = PKT_LEFT;

/* The data fifo rec provides the ASRC data when it is available. */
int32_t data_fifo_rec;

/* The input to the ASRC needs to be big enough to hold the left and right
 * sub-frames regardless of the PCM/SPI mode, adjust this using the multiplier.
 */
int16_t asrc_in_buf[SUBFRAME_LENGTH_LEFT_AND_RIGHT << PCM_SPI_SHIFT];

int64_t Cr = 0;
int64_t Ck = 0;
int64_t asrc_out_cnt   = 0;
int64_t audio_sink_cnt = 0;
int64_t audio_sink_period_cnt     = 0;
int64_t audio_sink_phase_cnt      = 0;
int64_t audio_sink_phase_cnt_prev = 0;
bool flag_ascc_phase = false;

uint32_t asrc_state_mem_rx[2][33];
int16_t spi_buf[2 * SUBFRAME_LENGTH];
int32_t pcm_buf[4 * SUBFRAME_LENGTH];
int32_t dmic_buffer_in[SUBFRAME_LENGTH];

#if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD)
volatile uint8_t i2c_tx_buffer_data[I2C_BUFFER_SIZE];
volatile uint8_t i2c_tx_buffer_index;
#endif    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD) */

/* Forward declarations */
static void Start_Enc_Lpdsp32_Channel();

static void Transmitter_Asrc_reconfig(void);

static void Start_Enc_Lpdsp32_Channel();

static void Start_Enc_Lpdsp32();

static bool nextPacketReady();

bool (*rx_DMA_ready)(void);

/* Interrupt Handlers*/

/* ----------------------------------------------------------------------------
 * Function      : void DMA_IRQ_FUNC(ASRC_IN_IDX)
 * ----------------------------------------------------------------------------
 * Description   : ASRC input DMA interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA_IRQ_FUNC(ASRC_IN_IDX)(void)
{
    Sys_ASRC_StatusConfig(ASRC_DISABLE);
    Sys_DMA_ClearChannelStatus(ASRC_IN_IDX);
}

/* ----------------------------------------------------------------------------
 * Function      : void DMA_IRQ_FUNC(ASRC_OUT_IDX)(void)
 * ----------------------------------------------------------------------------
 * Description   : ASRC output DMA interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA_IRQ_FUNC(ASRC_OUT_IDX)(void)
{
    /* Use two buffers with write indices to store samples from the ASRC
     * until we have a subframe worth, then we can send them to the LPDSP32
     * for processing.
     */
    static int16_t asrc_out_buffers[2][SUBFRAME_LENGTH];
    static uint16_t asrc_out_indices[2] = { 0, 0 };

    PacketSide side  = flag_packet_side;
    uint16_t *index = &asrc_out_indices[side];

    asrc_out_buffers[side][*index] = data_fifo_rec;
    *index = *index + 1;

#if (INPUT_INTRF == PCM_RX_RAW_INPUT)
    Sys_Delay_ProgramROM(100);
#endif    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT) */

    /* When we have a subframe of data, send it to the LPDSP32 for processing */
    if (*index == SUBFRAME_LENGTH)
    {
        *index = 0;
        if (fifoAvailable(lpdsp32.incoming) > SUBFRAME_LENGTH_BYTES && lpdsp32.state != DSP_STARTING)
        {
            /* Push side and collected subframe onto the fifo for processing. */
            fifoPush8(lpdsp32.incoming, side);
            fifoPush(lpdsp32.incoming, asrc_out_buffers[side], SUBFRAME_LENGTH_BYTES);

            /* Start an encode operation if we can */
            Start_Enc_Lpdsp32_Channel();
        }
        else
        {
            /* Reset the ASRC block if the FIFO is not available.
             * It is likely that the ASRC is in unstable mode and
             * generates too much data */
            Sys_ASRC_Reset();
            ASRC->PHASE_CNT = 0;
        }
    }

    /* Housework out of the way, re-enable the interrupt if we can */
    Sys_DMA_ClearChannelStatus(ASRC_OUT_IDX);
    if (ASRC_CTRL->ASRC_PROC_STATUS_ALIAS == ASRC_BUSY_BITBAND)
    {
        /* Re-enable DMA for ASRC output */
        Sys_DMA_ChannelEnable(ASRC_OUT_IDX);
    }
    else
    {
        /* Re-configure ASRC if ASCC interrupt has been happened */
        if (flag_ascc_phase)
        {
            Transmitter_Asrc_reconfig();
            asrc_state_mem_rx[PKT_LEFT][0]  = ASRC->PHASE_INC;
            asrc_state_mem_rx[PKT_RIGHT][0] = ASRC->PHASE_INC;
            flag_ascc_phase = false;
        }

        if (side == PKT_LEFT)
        {
            Sys_DMA_Set_ChannelSourceAddress(ASRC_IN_IDX,
                                             (uint32_t)&asrc_in_buf[
                                                 SUBFRAME_LENGTH <<
                                                 PCM_SPI_SHIFT]);
        }

        Sys_DMA_Set_ChannelDestAddress(MEMCPY_SAVE_STATE_MEM,
                                       (uint32_t)&asrc_state_mem_rx[side][0]);

        Sys_ASRC_StatusConfig(ASRC_DISABLE);
        Sys_DMA_ChannelEnable(MEMCPY_SAVE_STATE_MEM);

        /* Assume that both channels will be processed between two block
         * received interrupts. So, the next one will be PKT_RIGHT.
         */
        flag_packet_side = PKT_RIGHT;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void AUDIOSINK_PHASE_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : ASCC phase interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void AUDIOSINK_PHASE_IRQHandler(void)
{
    AUDIOSINK_CTRL->PERIOD_CNT_START_ALIAS = PERIOD_CNT_START_BITBAND;

    /* Get ASRC output count. */
    asrc_out_cnt = Sys_ASRC_OutputCount();
    Sys_ASRC_ResetOutputCount();

    /* Get audio sink phase count. */
    audio_sink_phase_cnt = Sys_Audiosink_PhaseCounter();

    /* Get audio sink count */
    audio_sink_cnt  = Sys_Audiosink_Counter() << SHIFT_BIT;

    AUDIOSINK_CTRL->CNT_RESET_ALIAS = CNT_RESET_BITBAND;
    audio_sink_cnt +=
        ((((audio_sink_phase_cnt_prev - audio_sink_phase_cnt))
          << SHIFT_BIT) / audio_sink_period_cnt);

    /* store audio sink count phase for the next time. */
    audio_sink_phase_cnt_prev = audio_sink_phase_cnt;

    AUDIOSINK->PHASE_CNT = 0;
    AUDIOSINK_CTRL->PHASE_CNT_START_ALIAS = PHASE_CNT_START_BITBAND;
    flag_ascc_phase = true;
}

/* ----------------------------------------------------------------------------
 * Function      : void AUDIOSINK_PERIOD_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : ASCC period interrupt handler (TX/RX)
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void AUDIOSINK_PERIOD_IRQHandler(void)
{
    audio_sink_period_cnt = Sys_Audiosink_PeriodCounter()
                            / (AUDIOSINK->CFG + 1);
    AUDIOSINK->PERIOD_CNT = 0;
}

/* ----------------------------------------------------------------------------
 * Function      : void DSP0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : DSP0 interrupt handler [Transmitter Encoder Left Channel]
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DSP0_IRQHandler(void)
{
    if (lpdsp32.state == DSP_STARTING)
    {
        lpdsp32.state = DSP_IDLE;
        return;
    }

    /* Push the output data onto the fifo associated with the current side. */
    swFIFO fifo = lpdsp32.outgoing[lpdsp32.dsp_side];
    fifoPush(fifo, Buffer.output, ENCODED_SUBFRAME_LENGTH);

    /* Deal with any pending packets, if there are none go to idle */
    if (nextPacketReady())
    {
        Start_Enc_Lpdsp32();
    }
    else
    {
        lpdsp32.state = DSP_IDLE;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : bool rx_DMA_ready_SPI(void)
 * ----------------------------------------------------------------------------
 * Description   : Handle the rx DMA for an SPI transfer
 * Inputs        : None
 * Outputs       : True in all cases
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
bool rx_DMA_ready_SPI(void)
{
    /* E7100 sends the audio stream in 4 samples blocks.
     * The left and right data is separated here*/
    for (uint32_t i = 0; i < SUBFRAME_LENGTH_LEFT_AND_RIGHT; i += 2 * 4)
    {
        memcpy(&asrc_in_buf[SUBFRAME_LENGTH + (i >> 1)], &spi_buf[i + 4],
               4 * sizeof(int16_t));
        memcpy(&asrc_in_buf[i >> 1], &spi_buf[i], 4 * sizeof(int16_t));
    }

    return (true);
}

/* ----------------------------------------------------------------------------
 * Function      : bool rx_DMA_ready_PCM(void)
 * ----------------------------------------------------------------------------
 * Description   : Handle the rx DMA for a PCM transfer
 * Inputs        : None
 * Outputs       : True if both sets of data have arrived for the PCM transfer
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
bool rx_DMA_ready_PCM(void)
{
    uint16_t i;
    int16_t *left, *right;
    int32_t *pcm;
    bool status;

    /* Effectively using two PCM data buffers to prevent read/write conflicts */
    /* Packing ASRC buffer with data received from first part of the PCM RX
     * buffer */
    if ((Sys_DMA_Get_ChannelStatus(RX_DMA_NUM) & DMA_COUNTER_INT_STATUS) != 0)
    {
        pcm    = &pcm_buf[0];
        left   = &asrc_in_buf[(0 * SUBFRAME_LENGTH)];
        right  = &asrc_in_buf[(2 * SUBFRAME_LENGTH)];
        status = false;
    }
    else
    {
        pcm    = &pcm_buf[SUBFRAME_LENGTH_LEFT_AND_RIGHT];
        left   = &asrc_in_buf[(1 * SUBFRAME_LENGTH)];
        right  = &asrc_in_buf[(3 * SUBFRAME_LENGTH)];
        status = true;
    }

    for (i = 0; i < SUBFRAME_LENGTH_LEFT_AND_RIGHT; i += 2)
    {
        *(left++)  = *(pcm++) >> 8;
        *(right++) = *(pcm++) >> 8;
    }

    return (status);
}

/* ----------------------------------------------------------------------------
 * Function      : bool rx_DMA_ready_DMIC(void)
 * ----------------------------------------------------------------------------
 * Description   : Handle the rx DMA for a DMIC transfer
 * Inputs        : None
 * Outputs       : False in all cases
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
bool rx_DMA_ready_DMIC(void)
{
    uint8_t i;
    int16_t left_data[SUBFRAME_LENGTH];
    int16_t right_data[SUBFRAME_LENGTH];

    /* Pack the incoming DMIC data into left and right buffers */
    for (i = 0; i < SUBFRAME_LENGTH; i++)
    {
        left_data[i]  = dmic_buffer_in[i] & 0xFFFF;
        right_data[i] = dmic_buffer_in[i] >> 16;
    }

    /* Push side and collected subframe onto the fifo for processing. */
    fifoPush8(lpdsp32.incoming, PKT_LEFT);
    fifoPush(lpdsp32.incoming, left_data, SUBFRAME_LENGTH_BYTES);

    fifoPush8(lpdsp32.incoming, PKT_RIGHT);
    fifoPush(lpdsp32.incoming, right_data, SUBFRAME_LENGTH_BYTES);

    /* Start an encode operation if we can */
    Start_Enc_Lpdsp32_Channel();

    return (false);
}

/* ----------------------------------------------------------------------------
 * Function      : void  DMA_IRQ_FUNC(RX_DMA_NUM)(void)
 * ----------------------------------------------------------------------------
 * Description   : DMA Channel assigned to SPI and PCM receive interrupt handler
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA_IRQ_FUNC(RX_DMA_NUM)(void)
{
    /* Specific function of the RX DMA will depend on the use of SPI, DMIC or
     * PCM. */
    if (rx_DMA_ready())
    {
        Sys_DMA_Set_ChannelSourceAddress(ASRC_IN_IDX,
                                         (uint32_t)&asrc_in_buf[0]);

        /* Re-enable DMA for ASRC output */
        Sys_DMA_ChannelEnable(ASRC_OUT_IDX);

        /* Re-enable ASRC input DMA and start ASRC */
        Sys_DMA_ChannelEnable(ASRC_IN_IDX);
        Sys_ASRC_StatusConfig(ASRC_ENABLE);

        flag_packet_side = PKT_LEFT;
    }

    /* In either case we need to clear the channel status */
    Sys_DMA_ClearChannelStatus(RX_DMA_NUM);
}

/* ----------------------------------------------------------------------------
 * Function      : void DMA_IRQ_FUNC(MEMCPY_SAVE_STATE_MEM)(void)
 * ----------------------------------------------------------------------------
 * Description   : DMA channel interrupt used for saving the ASRC data into the
 *                 memory
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA_IRQ_FUNC(MEMCPY_SAVE_STATE_MEM)(void)
{
    if (DMA->DEST_BASE_ADDR[MEMCPY_SAVE_STATE_MEM]
        == (uint32_t)&asrc_state_mem_rx[PKT_LEFT][0])
    {
        Sys_DMA_Set_ChannelSourceAddress(MEMCPY_RESTORE_STATE_MEM,
                                         (uint32_t)&asrc_state_mem_rx[PKT_RIGHT
                                         ][0]);
    }
    else
    {
        Sys_DMA_Set_ChannelSourceAddress(MEMCPY_RESTORE_STATE_MEM,
                                         (uint32_t)&asrc_state_mem_rx[PKT_LEFT]
                                         [0]);
    }
    Sys_DMA_ChannelEnable(MEMCPY_RESTORE_STATE_MEM);
}

/* ----------------------------------------------------------------------------
 * Function      : void DMA_IRQ_FUNC(MEMCPY_SAVE_STATE_MEM)(void)
 * ----------------------------------------------------------------------------
 * Description   : DMA channel interrupt used for restoring the ASRC data from
 *                 the memory
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA_IRQ_FUNC(MEMCPY_RESTORE_STATE_MEM)(void)
{
    if (DMA->SRC_BASE_ADDR[MEMCPY_RESTORE_STATE_MEM]
        == (uint32_t)&asrc_state_mem_rx[PKT_RIGHT][0])
    {
        /* Re-enable DMA for ASRC output */
        Sys_DMA_ClearChannelStatus(ASRC_OUT_IDX);
        Sys_DMA_ChannelEnable(ASRC_OUT_IDX);

        /* Re-enable ASRC input DMA and start ASRC */
        Sys_DMA_ChannelEnable(ASRC_IN_IDX);
        Sys_ASRC_StatusConfig(ASRC_ENABLE);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Handle the interrupts of I2C. Handle data transfers
 *                 for configuring the Audio Shield
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void I2C_IRQHandler(void)
{
    /* Enabled if input to the transmitter is raw format. Either from PCM
     * or SPI interface. */
#if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD)
    uint32_t i2c_status = Sys_I2C_Get_Status();

    if ((i2c_status & (1 << I2C_STATUS_READ_WRITE_Pos)) == I2C_IS_WRITE)
    {
        if ((i2c_status & (1 << I2C_STATUS_ACK_STATUS_Pos)) == I2C_HAS_ACK)
        {
            if (i2c_tx_buffer_index < I2C_BUFFER_SIZE)
            {
                I2C->DATA = i2c_tx_buffer_data[i2c_tx_buffer_index++];
            }
            else
            {
                I2C_CTRL1->LAST_DATA_ALIAS = I2C_LAST_DATA_BITBAND;
            }
        }
    }
#endif    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD) */
}

/* ----------------------------------------------------------------------------
 * Function      : bool nextPacketReady()
 * ----------------------------------------------------------------------------
 * Description   : Check if there is a new packet ready, this is indicated by
 *                 there being at least one full sub-frame plus a side byte.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static bool nextPacketReady()
{
    return (fifoUsed(lpdsp32.incoming) > SUBFRAME_LENGTH_BYTES);
}

/* ----------------------------------------------------------------------------
 * Function      : void Start_Enc_Lpdsp32_Channel()
 * ----------------------------------------------------------------------------
 * Description   : If the LPDSP32 is not busy and a packet is ready send it to
 *                 to LPDSP32 to encode
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void Start_Enc_Lpdsp32_Channel()
{
    if ((lpdsp32.state == DSP_IDLE) && nextPacketReady())
    {
        lpdsp32.state = DSP_BUSY;
        Start_Enc_Lpdsp32();
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Start_Enc_Lpdsp32()
 * ----------------------------------------------------------------------------
 * Description   : Issue encoding start command to LPDSP32
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : This should only be called when a packet is ready to be
 *                 processed
 * ------------------------------------------------------------------------- */
static void Start_Enc_Lpdsp32()
{
    /* Read the side and packet data from the FIFO */
    fifoPop8(lpdsp32.incoming, &lpdsp32.dsp_side);
    fifoPop(lpdsp32.incoming, &Buffer.input[0][0], SUBFRAME_LENGTH_BYTES);

    /* Set the parameters for the encode and initiate it */
    codecSetParameters(lpdsp32.codec, &lpdsp32.channels[lpdsp32.dsp_side]);
    codecEncode(lpdsp32.codec);

    /* turn off the ENCODE_RESET if it is set */
    lpdsp32.channels[lpdsp32.dsp_side].action &= ~ENCODE_RESET;
}

/* ----------------------------------------------------------------------------
 * Function      : void Transmitter_Asrc_reconfig(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure ASRC TX
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void Transmitter_Asrc_reconfig(void)
{
    Cr = audio_sink_cnt;
    Ck = FRAME_LENGTH << SHIFT_BIT;

    /* Limit Cr if it is out of range */
    uint8_t shift = PCM_SPI_SHIFT + SHIFT_BIT;
    if ((Cr <= ((FRAME_LENGTH - ASRC_CFG_THR) << shift))
        || (Cr >= ((FRAME_LENGTH + ASRC_CFG_THR) << shift)))
    {
        Cr = FRAME_LENGTH << shift;
    }

    /* Configure ASRC base on new Cr Ck */
    int64_t asrc_inc_carrier = ((Cr - Ck) << PCM_SPI_USED_BITS) / Ck;
    asrc_inc_carrier &= 0xFFFFFFFF;
    Sys_ASRC_Config(asrc_inc_carrier, PCM_SPI_ASRC_CONFIG);
}
