/* ----------------------------------------------------------------------------
 *
 * Copyright (c) 2015 Semiconductor Components Industries, LLC (d/b/a
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * rm_app.c
 * - Remote microphone BLE application
 * ----------------------------------------------------------------------------
 * $Revision: 1.16 $
 * $Date: 2020/01/02 21:23:57 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>

uint8_t round_key[AES128_KEY_LENGTH];
struct app_env_tag app_env;

/* ----------------------------------------------------------------------------
 * Function      : void Encry_AES_128 (uint8_t *ptr, uint8_t block_num)
 * ----------------------------------------------------------------------------
 * Description   : Encryption the data on over-write the result
 * Inputs        : ptr       - Pointer to packet data in memory
 *                 block_num - Number of blocks (16 bytes) need to be encrypted
 * Outputs       : None
 * Assumptions   : Exchange memory is used. Adjust AES128_EM_PTR accordingly
 * ------------------------------------------------------------------------- */
void Encry_AES_128 (uint8_t *ptr, uint8_t block_num)
{
    int i = 0;
    uint8_t *ptr_tmp = ptr;
    for (i = 0; i < block_num; i++, ptr_tmp += AES128_KEY_LENGTH)
    {
        aes128_hw_cipher(round_key, AES128_EM_PTR, ptr_tmp);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t RM_Callback_TRX(uint8_t type, uint8_t *length,
 *                                         uint8_t *ptr)
 * ----------------------------------------------------------------------------
 * Description   : Handle RM app callbacks for transmission
 * Inputs        : type - Callback type
 *               : length - Optional length of packet
 *               : ptr - Pointer to packet data in memory
 * Outputs       : uint8_t - Zero if successful
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
uint8_t RM_Callback_TRX(uint8_t type, uint8_t *length, uint8_t *ptr)
{
    switch (type)
    {
        case RM_TX_PAYLOAD_READY_LEFT:
        {
            fifoPop(lpdsp32.outgoing[PKT_LEFT], ptr, *length);
#if CRY_AES_128_ECB
            Encry_AES_128(ptr, (uint8_t)(*length / AES128_KEY_LENGTH));
#endif    /* if CRY_AES_128_ECB */
        }
        break;

        case RM_TX_PAYLOAD_READY_RIGHT:
        {
            fifoPop(lpdsp32.outgoing[PKT_RIGHT], ptr, *length);
#if CRY_AES_128_ECB
            Encry_AES_128(ptr, (uint8_t)(*length / AES128_KEY_LENGTH));
#endif    /* if CRY_AES_128_ECB */
        }
        break;
    }
    return (0);
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t RM_Callback_StatusUpdate(uint8_t status)
 * ----------------------------------------------------------------------------
 * Description   : Handle RM app callbacks for status updates
 * Inputs        : status - status
 * Outputs       : uint8_t - Zero if successful
 * Assumptions   : None
 * ------------------------------------------------------------------------- */

uint8_t RM_Callback_StatusUpdate(uint8_t status)
{
    switch (status)
    {
        case LINK_DISCONNECTED:
        {
            PRINTF("__RM_LINK_DISCONNECTED\n");
            /* Decide if the number of lost links is large, do an action */
            app_env.rm_lostLink_counter++;
        }
        break;

        case LINK_ESTABLISHMENT_UNSUCCESS:
        {
            app_env.rm_unsuccessLink_counter++;
        }
        break;

        case LINK_ESTABLISHED:
        {
            PRINTF("__RM_LINK_ESTABLISHED\n");
        }
        break;
    }
    app_env.rm_link_status = status;
    return (0);
}

/* ----------------------------------------------------------------------------
 * Function      : void APP_RM_Init(uint8_t side)
 * ----------------------------------------------------------------------------
 * Description   : Initialize RM application
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */

void APP_RM_Init(uint8_t side)
{
    struct rm_callback callback;
    uint8_t temp[16] = RM_HOPLIST;

    app_env.rm_link_status              = LINK_DISCONNECTED;
    app_env.rm_lostLink_counter         = 0;
    app_env.rm_unsuccessLink_counter    = 0;
    app_env.audio_streaming             = 0;

    app_env.rm_param.audioChnl          = side;
    app_env.rm_param.role = APP_RM_ROLE;
    app_env.rm_param.interval_time      = 10000;
    app_env.rm_param.retrans_time       = 5000;
#if (CODEC_CONFIG == CODEC_CONFIG_CELT)
    app_env.rm_param.audio_rate         = 32;
#else
    app_env.rm_param.audio_rate         = 48;
#endif
    app_env.rm_param.radio_rate         = 2000;
    app_env.rm_param.scan_time          = 6500;
    app_env.rm_param.preamble           = 0x55;
    app_env.rm_param.accessword         = (0x00cde629 | (0xf2 << 24));

    app_env.rm_param.payloadFlowRequest = APP_RM_DATA_REQUEST_TYPE;
    app_env.rm_param.renderDelay        = 200;

    if (app_env.rm_param.payloadFlowRequest == RM_APP_REQUEST)
    {
        app_env.rm_param.preFetchDelay = 1300;
    }
    else
    {
        app_env.rm_param.preFetchDelay = 400;
    }

    app_env.rm_param.pktLostLowThrshld     = 10;
    app_env.rm_param.pktLostHighThrshld    = 200;
    app_env.rm_param.pktLostLowThrshldSlow = 1;

    app_env.rm_param.searchTryCntThrshld   = 10;
    app_env.rm_param.waitCntGranularity    = 200;

    app_env.rm_param.stepSize = 1;
    app_env.rm_param.numChnlInHopList = 7;

    app_env.rm_param.mod_idx  = BLE_MOD_IDX;
    app_env.rm_param.dma_memcpy_num   = MEMCPY_DMA_NUM;

    app_env.rm_param.debug_dio_num[0] = DEBUG_DIO_FIRST;
    app_env.rm_param.debug_dio_num[1] = DEBUG_DIO_SECOND;
    app_env.rm_param.debug_dio_num[2] = DEBUG_DIO_THIRD;
    app_env.rm_param.debug_dio_num[3] = 0xff;

    memcpy(app_env.rm_param.hopList, temp, 16);

    callback.trx_event     = RM_Callback_TRX;
    callback.status_update = RM_Callback_StatusUpdate;

    RM_Configure(&app_env.rm_param, callback);

    rm_env.intf.status_update(LINK_DISCONNECTED);
    RM_Enable(500);

#if CRY_AES_128_ECB
    uint32_t key[4] = KEY_AES_128_ECB;
    memcpy(round_key, key, sizeof(uint32_t) * 4);
#endif    /* if CRY_AES_128_ECB */
}
