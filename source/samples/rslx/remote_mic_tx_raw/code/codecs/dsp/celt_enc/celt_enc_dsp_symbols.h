#ifndef _CELT_ENC_DSP_DSP_SYMBOLS_H_
#define _CELT_ENC_DSP_DSP_SYMBOLS_H_ 1

/* Auto generated file
 */

#define celt_enc_dsp_dsp_encode_base    (0x00000160)
#define celt_enc_dsp_dsp_encode_end     (0x00000192)
#define celt_enc_dsp_dsp_decode_base    (0x00000192)
#define celt_enc_dsp_dsp_decode_end     (0x00000194)

#endif
