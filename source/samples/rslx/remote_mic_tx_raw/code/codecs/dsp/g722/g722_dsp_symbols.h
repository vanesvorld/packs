#ifndef _G722_DSP_DSP_SYMBOLS_H_
#define _G722_DSP_DSP_SYMBOLS_H_ 1

/* Auto generated file
 */

#define g722_dsp_dsp_encode_base    (0x000007ce)
#define g722_dsp_dsp_encode_end     (0x00000802)
#define g722_dsp_dsp_decode_base    (0x00000880)
#define g722_dsp_dsp_decode_end     (0x000008b4)

#endif
