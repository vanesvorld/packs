/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * fifo.h
 * - Software FIFO implementation header file
 * ----------------------------------------------------------------------------
 * $Revision: 1.5 $
 * $Date: 2019/07/29 19:37:49 $
 * ------------------------------------------------------------------------- */

#ifndef _FIFO_H_
#define _FIFO_H_ 1

#include <stdint.h>

#define FIFO_NO_ERROR           0
#define FIFO_ERROR_NULL         1
#define FIFO_ERROR_OVERFLOW     2
#define FIFO_ERROR_UNDERFLOW    3

typedef void *swFIFO;

swFIFO fifoCreate(uint16_t maxSize);

swFIFO fifoCreateStatic(void *buffer, uint16_t maxSize);

uint8_t fifoDestroy(swFIFO *fifo);

uint16_t fifoAvailable(swFIFO fifo);

uint16_t fifoUsed(swFIFO fifo);

uint8_t fifoPush(swFIFO fifo, void *buffer, uint16_t size);

uint8_t fifoPop(swFIFO fifo, void *buffer, uint16_t size);

uint8_t fifoPush8(swFIFO fifo, uint8_t value);

uint8_t fifoPop8(swFIFO fifo, uint8_t *value);

uint8_t fifoPush16(swFIFO fifo, uint16_t value);

uint8_t fifoPop16(swFIFO fifo, uint16_t *value);

uint8_t fifoPush32(swFIFO fifo, uint32_t value);

uint8_t fifoPop32(swFIFO fifo, uint32_t *value);

#endif    /* _FIFO_H_ */
