/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.h
 * - Application header file
 * ----------------------------------------------------------------------------
 * $Revision: 1.25 $
 * $Date: 2019/12/30 20:50:48 $
 * ------------------------------------------------------------------------- */

#ifndef APP_H_
#define APP_H_

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>
#include <stdlib.h>
#include <stdbool.h>
#include <rm_pkt.h>

#include "codec.h"
#include "fifo.h"
#include "aes128.h"
#include "aesavs.h"
#include "codecs/G722DSP/g722DSPCodec.h"
#include "codecs/celtEncDSP/celtEncDSPCodec.h"
/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/

/* Configure RF 48 MHz XTAL divided clock frequency in Hz
 * Options: 16 MHz, 24 MHz, 48 MHz */
#define RFCLK_FREQ                      48000000

/* Define clock divider and flash timings depending on RF clock frequency */
#if (RFCLK_FREQ == 16000000)
#define RF_CK_DIV_PRESCALE_VALUE        CK_DIV_1_6_PRESCALE_3_BYTE
#define SLOWCLK_PRESCALE_VALUE          SLOWCLK_PRESCALE_8
#define BBCLK_PRESCALE_VALUE            BBCLK_PRESCALE_2
#define DCCLK_PRESCALE_BYTE_VALUE       DCCLK_PRESCALE_4_BYTE
#define BBCLK_DIVIDER_VALUE             BBCLK_DIVIDER_8
#elif (RFCLK_FREQ == 24000000)
#define RF_CK_DIV_PRESCALE_VALUE        CK_DIV_1_6_PRESCALE_2_BYTE
#define SLOWCLK_PRESCALE_VALUE          SLOWCLK_PRESCALE_12
#define BBCLK_PRESCALE_VALUE            BBCLK_PRESCALE_3
#define DCCLK_PRESCALE_BYTE_VALUE       DCCLK_PRESCALE_6_BYTE
#define BBCLK_DIVIDER_VALUE             BBCLK_DIVIDER_8
#elif (RFCLK_FREQ == 48000000)
#define RF_CK_DIV_PRESCALE_VALUE        CK_DIV_1_6_PRESCALE_1_BYTE
#define SLOWCLK_PRESCALE_VALUE          SLOWCLK_PRESCALE_24
#define BBCLK_PRESCALE_VALUE            BBCLK_PRESCALE_6
#define DCCLK_PRESCALE_BYTE_VALUE       DCCLK_PRESCALE_12_BYTE
#define BBCLK_DIVIDER_VALUE             BBCLK_DIVIDER_8
#endif    /* if (RFCLK_FREQ == 8000000) */

/*  (Nordic channel/2 - 1)
 *  {1, 7, 13, 19, 25, 31, 37}
 *  {2, 8, 14, 20, 26, 35, 38}
 *  {3, 9, 15, 21, 24, 33, 36}
 */
#define RM_HOPLIST                      { 3, 9, 15, 21, 24, 33, 36 }
#define KEY_AES_128_ECB                 { 0x4138684C, \
                                          0xD874F539, \
                                          0x4EF3BC36, \
                                          0xBF01FB9D }

#define APP_RM_ROLE                     RM_MASTER_ROLE
#define CRY_AES_128_ECB                 0

#define RM_LEFT                         0
#define RM_RIGHT                        1

#define APP_RM_AUDIO_CHANNEL            RM_LEFT

#define OUTPUT_POWER_6DBM               0

#define NO_RX_INPUT                     0
#define SPI_RX_RAW_INPUT                2     /* With RSL10_RemoteDongle in E7100. */
#define PCM_RX_RAW_INPUT                3
#define DMIC_RX_RAW_INPUT               4

#ifndef INPUT_INTRF
#define INPUT_INTRF                     SPI_RX_RAW_INPUT
#endif

#define APP_RM_DATA_REQUEST_TYPE        RM_PRO_REQUEST

/* There are a couple of control points we can use when switching between
 * SPI and PCM inputs. By defining the following values we can remove some
 * less clear areas of conditional compilation in this code.
 */
#if (INPUT_INTRF == SPI_RX_RAW_INPUT)
#define PCM_SPI_SHIFT                   0
#define PCM_SPI_USED_BITS               29
#define PCM_SPI_ASRC_CONFIG             (LOW_DELAY | ASRC_DEC_MODE1)
#else    /* if (INPUT_INTRF == SPI_RX_RAW_INPUT) */
#define PCM_SPI_SHIFT                   1
#define PCM_SPI_USED_BITS               29
#define PCM_SPI_ASRC_CONFIG             (WIDE_BAND | ASRC_DEC_MODE1)
#endif    /* if (INPUT_INTRF == SPI_RX_RAW_INPUT) */

#define EZAIRO_7100                     0
#define AUDIO_CODEC_SHIELD              1

#ifndef PCM_RX_RAW_SOURCE
#define PCM_RX_RAW_SOURCE               EZAIRO_7100
#endif

#define DMIC_CLK_PIN                    0
#define DMIC_DATA_PIN                   1

#define THREE_BLOCK_APPN(x, y, z)       x##y##z
#define DMA_IRQn(x)                     THREE_BLOCK_APPN(DMA, x, _IRQn)
#define TIMER_IRQn(x)                   THREE_BLOCK_APPN(TIMER, x, _IRQn)
#define DMA_IRQ_FUNC(x)                 THREE_BLOCK_APPN(DMA, x, _IRQHandler)
#define TIMER_IRQ_FUNC(x)               THREE_BLOCK_APPN(TIMER, x, _IRQHandler)

#define MEMCPY_DMA_NUM                  0
#define MEMCPY_RESTORE_STATE_MEM        1
#define MEMCPY_SAVE_STATE_MEM           2
#define ASRC_IN_IDX                     3
#define ASRC_OUT_IDX                    4
#define RX_DMA_NUM                      5

#define PCM_CFG_RX                      (PCM_BIT_ORDER_MSB_FIRST | \
                                         PCM_TX_ALIGN_LSB |        \
                                         PCM_WORD_SIZE_24 |        \
                                         PCM_FRAME_ALIGN_LAST |    \
                                         PCM_FRAME_WIDTH_LONG |    \
                                         PCM_MULTIWORD_2 |         \
                                         PCM_SUBFRAME_DISABLE |    \
                                         PCM_CONTROLLER_DMA |      \
                                         PCM_DISABLE |             \
                                         PCM_SELECT_SLAVE)

#define TX_DMA_SPI                      (DMA_DEST_SPI0 |            \
                                         DMA_TRANSFER_M_TO_P |      \
                                         DMA_LITTLE_ENDIAN |        \
                                         DMA_COMPLETE_INT_DISABLE | \
                                         DMA_COUNTER_INT_DISABLE |  \
                                         DMA_DEST_WORD_SIZE_8 |     \
                                         DMA_SRC_WORD_SIZE_32 |     \
                                         DMA_SRC_ADDR_INC |         \
                                         DMA_DEST_ADDR_STATIC |     \
                                         DMA_ADDR_LIN |             \
                                         DMA_DISABLE)

#if (INPUT_INTRF == SPI_RX_RAW_INPUT)
#define DMA_RX_CONFIG                   (DMA_LITTLE_ENDIAN |       \
                                         DMA_DISABLE |             \
                                         DMA_DISABLE_INT_DISABLE | \
                                         DMA_ERROR_INT_DISABLE |   \
                                         DMA_COMPLETE_INT_ENABLE | \
                                         DMA_COUNTER_INT_ENABLE |  \
                                         DMA_START_INT_DISABLE |   \
                                         DMA_DEST_WORD_SIZE_32 |   \
                                         DMA_SRC_WORD_SIZE_16 |    \
                                         DMA_SRC_SPI0 |            \
                                         DMA_PRIORITY_0 |          \
                                         DMA_TRANSFER_P_TO_M |     \
                                         DMA_DEST_ADDR_INC |       \
                                         DMA_SRC_ADDR_STATIC |     \
                                         DMA_ADDR_CIRC)
#elif (INPUT_INTRF == PCM_RX_RAW_INPUT)
#define DMA_RX_CONFIG                   (DMA_LITTLE_ENDIAN |       \
                                         DMA_DISABLE |             \
                                         DMA_DISABLE_INT_DISABLE | \
                                         DMA_COMPLETE_INT_ENABLE | \
                                         DMA_COUNTER_INT_ENABLE |   \
                                         DMA_START_INT_DISABLE |   \
                                         DMA_DEST_WORD_SIZE_32 |   \
                                         DMA_SRC_WORD_SIZE_32 |    \
                                         DMA_SRC_PCM |             \
                                         DMA_PRIORITY_0 |          \
                                         DMA_TRANSFER_P_TO_M |     \
                                         DMA_DEST_ADDR_INC |       \
                                         DMA_SRC_ADDR_STATIC |     \
                                         DMA_ADDR_CIRC)
#elif (INPUT_INTRF == DMIC_RX_RAW_INPUT)
#define DMA_RX_CONFIG                   (DMA_LITTLE_ENDIAN |       \
                                         DMA_ENABLE |              \
                                         DMA_DISABLE_INT_DISABLE | \
                                         DMA_ERROR_INT_DISABLE |   \
                                         DMA_COMPLETE_INT_ENABLE | \
                                         DMA_COUNTER_INT_DISABLE | \
                                         DMA_START_INT_DISABLE |   \
                                         DMA_DEST_WORD_SIZE_32 |   \
                                         DMA_SRC_WORD_SIZE_32 |    \
                                         DMA_DEST_ADDR_INC |       \
                                         DMA_PRIORITY_0 |          \
                                         DMA_TRANSFER_P_TO_M |     \
                                         DMA_SRC_ADDR_STATIC |     \
                                         DMA_SRC_DMIC |            \
                                         DMA_ADDR_CIRC)
#endif    /* if (INPUT_INTRF == SPI_RX_RAW_INPUT) */

#define DMA_SAVE_STATE_MEM_CONFIG       (DMA_LITTLE_ENDIAN |       \
                                         DMA_DISABLE |             \
                                         DMA_DISABLE_INT_DISABLE | \
                                         DMA_ERROR_INT_DISABLE |   \
                                         DMA_COMPLETE_INT_ENABLE | \
                                         DMA_COUNTER_INT_DISABLE | \
                                         DMA_START_INT_DISABLE |   \
                                         DMA_DEST_WORD_SIZE_32 |   \
                                         DMA_SRC_WORD_SIZE_32 |    \
                                         DMA_PRIORITY_0 |          \
                                         DMA_SRC_PBUS |            \
                                         DMA_TRANSFER_P_TO_M |     \
                                         DMA_DEST_ADDR_INC |       \
                                         DMA_SRC_ADDR_INC |        \
                                         DMA_ADDR_LIN)

#define DMA_RESTORE_STATE_MEM_CONFIG    (DMA_LITTLE_ENDIAN |       \
                                         DMA_DISABLE |             \
                                         DMA_DISABLE_INT_DISABLE | \
                                         DMA_ERROR_INT_DISABLE |   \
                                         DMA_COMPLETE_INT_ENABLE | \
                                         DMA_COUNTER_INT_DISABLE | \
                                         DMA_START_INT_DISABLE |   \
                                         DMA_DEST_WORD_SIZE_32 |   \
                                         DMA_SRC_WORD_SIZE_32 |    \
                                         DMA_PRIORITY_0 |          \
                                         DMA_DEST_PBUS |           \
                                         DMA_TRANSFER_M_TO_P |     \
                                         DMA_DEST_ADDR_INC |       \
                                         DMA_SRC_ADDR_INC |        \
                                         DMA_ADDR_LIN)

#define DMA_MEMCPY_CONFIG               (DMA_DEST_PBUS |        \
                                         DMA_ENABLE |           \
                                         DMA_ADDR_LIN |         \
                                         DMA_SRC_ADDR_INC |     \
                                         DMA_DEST_ADDR_STATIC | \
                                         DMA_TRANSFER_M_TO_P |  \
                                         DMA_SRC_WORD_SIZE_32 | \
                                         DMA_DEST_WORD_SIZE_8 | \
                                         DMA_COMPLETE_INT_ENABLE)

/* DMA for ASRC input on TX side */
#define TX_DMA_ASRC_IN                  (DMA_DEST_ASRC |           \
                                         DMA_TRANSFER_M_TO_P |     \
                                         DMA_LITTLE_ENDIAN |       \
                                         DMA_COMPLETE_INT_ENABLE | \
                                         DMA_COUNTER_INT_DISABLE | \
                                         DMA_DEST_WORD_SIZE_16 |   \
                                         DMA_SRC_WORD_SIZE_32 |    \
                                         DMA_SRC_ADDR_INC |        \
                                         DMA_DEST_ADDR_STATIC |    \
                                         DMA_ADDR_LIN |            \
                                         DMA_DISABLE)

/* DMA for ASRC output on TX side */
#define TX_DMA_ASRC_OUT                 (DMA_SRC_ASRC |            \
                                         DMA_TRANSFER_P_TO_M |     \
                                         DMA_LITTLE_ENDIAN |       \
                                         DMA_COMPLETE_INT_ENABLE | \
                                         DMA_COUNTER_INT_ENABLE |  \
                                         DMA_DEST_WORD_SIZE_32 |   \
                                         DMA_SRC_WORD_SIZE_16 |    \
                                         DMA_SRC_ADDR_STATIC |     \
                                         DMA_DEST_ADDR_STATIC |    \
                                         DMA_ADDR_LIN |            \
                                         DMA_DISABLE)

#define CONCAT(x, y)                    x##y
#define DIO_SRC(x)                      CONCAT(DIO_SRC_DIO_, x)

/* DIO pin configuration for SPI */
#define SPI_SER_DI                      2
#define SPI_SER_DO                      1
#define SPI_CLK_DO                      3
#define SPI_CS_DO                       0

/* DIO pin configuration for PCM interface */
#if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD)
#define PCM_SER_DI                      11
#define PCM_SER_DO                      10
#define PCM_CLK_DO                      12
#define PCM_FRAME_SYNC                  9
#else    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD) */
#define PCM_SER_DI                      2
#define PCM_SER_DO                      1
#define PCM_CLK_DO                      3
#define PCM_FRAME_SYNC                  0
#endif    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD) */

#define BUTTON_DIO                      5
#if (INPUT_INTRF == PCM_RX_RAW_INPUT)
#define SAMPL_CLK                       PCM_FRAME_SYNC
#else    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT) */
#define SAMPL_CLK                       7
#endif    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT) */

#define LED_DIO_NUM                     6

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    13

#define DEBUG_DIO_FIRST                 4
#if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD)
#define DEBUG_DIO_SECOND                1
#define DEBUG_DIO_THIRD                 0
#else    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD) */
#define DEBUG_DIO_SECOND                11
#define DEBUG_DIO_THIRD                 10
#endif    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT && PCM_RX_RAW_SOURCE == AUDIO_CODEC_SHIELD) */

/* Codec configuraton */
#define CODEC_CONFIG_G722               0
#define CODEC_CONFIG_CELT               1

/* LPDSP32 CODEC related defines */
#if (CODEC_CONFIG == CODEC_CONFIG_G722)
    #define POPULATE_CODEC_FUNCTION         populateG722DSPCodec
    #define CODEC_MODE                      3
    #define CODEC_ACTION                    (ALT_PACKED | ENCODE_RESET | ENCODE)
    #define CODEC_SAMPLE_RATE               0 //Not used in G722
    #define FRAME_LENGTH                    160
    #define CODEC_BLOCK_SIZE                4

    #if (INPUT_INTRF == PCM_RX_RAW_INPUT)
        #define SUBFRAME_LENGTH                 32
    #elif (INPUT_INTRF == DMIC_RX_RAW_INPUT)
        #define SUBFRAME_LENGTH                 64
    #else    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT) */
        #define SUBFRAME_LENGTH                 8
    #endif    /* if (INPUT_INTRF == PCM_RX_RAW_INPUT) */
    #define SUBFRAME_LENGTH_BYTES           (SUBFRAME_LENGTH * sizeof(uint16_t))
    #define SUBFRAME_LENGTH_LEFT_AND_RIGHT  (SUBFRAME_LENGTH * 2)

    #if (CODEC_MODE == 3)
        #define ENCODED_FRAME_LENGTH            (3 * (FRAME_LENGTH / 8))
        #define ENCODED_SUBFRAME_LENGTH         (3 * (SUBFRAME_LENGTH / 8))
    #endif    /* if (CODEC_MODE == 3) */
#elif (CODEC_CONFIG == CODEC_CONFIG_CELT)
    #define POPULATE_CODEC_FUNCTION         populateCeltEncDSPCodec
    #define CODEC_MODE                      1
    #define SUBFRAME_LENGTH                 160
    #define SUBFRAME_LENGTH_BYTES           (SUBFRAME_LENGTH * 2)
    #define SUBFRAME_LENGTH_LEFT_AND_RIGHT  (SUBFRAME_LENGTH * 2)
    #define FRAME_LENGTH                    160 //160
    #define ENCODED_FRAME_LENGTH            40
    #define ENCODED_SUBFRAME_LENGTH         40
    #define CODEC_BLOCK_SIZE                40
    #define CODEC_SAMPLE_RATE               16000
    #define CODEC_ACTION                    (ENCODE_RESET | ENCODE)
    #if (RFCLK_FREQ < 24000000)
        #error "RFCLK_FREQ should be at least 24000000 for the CELT configuration"
    #endif
#endif /* #if (CODEC_CONFIG == CODEC_CONFIG_G722) */


/* Threshold for valid Cr/Ck distance */
#define ASRC_CFG_THR                    16

/* Threshold for allowed read/write pointer crossing */
#define PTR_RST_THR                     10

/* the number of shifts of ASRC registers for using fix point variables */
#define SHIFT_BIT                       20

/* TX data FIFO related defines */
#define TX_DATA_FIFO_LENGTH             (2 * ENCODED_FRAME_LENGTH)
#define TX_FIFO_RWPTR_INIT              (ENCODED_FRAME_LENGTH)
#define TX_FIFO_W2R_THR                 (ENCODED_SUBFRAME_LENGTH)
#define TX_FIFO_R2W_THR                 (ENCODED_SUBFRAME_LENGTH)

#define DMIC_CFG                        (DMIC0_DCRM_CUTOFF_20HZ | \
                                         DMIC1_DCRM_CUTOFF_20HZ | \
                                         DMIC1_DELAY_DISABLE |    \
                                         DMIC0_FALLING_EDGE |     \
                                         DMIC1_RISING_EDGE)

#define DECIMATE_BY_200                 ((uint32_t)(0x11U << \
                                                    AUDIO_CFG_DEC_RATE_Pos))

#define DMIC_AUDIO_CFG                  (DMIC0_ENABLE |    \
                                         DMIC1_ENABLE |    \
                                         DECIMATE_BY_200 | \
                                         DMIC_AUDIOCLK |   \
                                         DMIC0_DMA_REQ_ENABLE)

#define DMIC_MAX_GAIN                   0xFFF

#define I2C_SCL_DIO                                 2
#define I2C_SDA_DIO                                 3
#define I2C_BUFFER_SIZE                             2

#define WM8731_I2C_SLAVE_ADDRESS                    0x1A
#define WM8731_I2C_MESSAGE_BUFFER_SIZE              11

#define WM8731_LLINE_IN_REG_ADDR                    0x00
#define WM8731_RLINE_IN_REG_ADDR                    0x02
#define WM8731_LLINE_OUT_REG_ADDR                   0x04
#define WM8731_RLINE_OUT_REG_ADDR                   0x06
#define WM8731_ANALOG_AUDIO_PATH_CTRL_REG_ADDR      0x08
#define WM8731_DIGITAL_AUDIO_PATH_CTRL_REG_ADDR     0x0A
#define WM8731_POWER_DOWN_CTRL_REG_ADDR             0x0C
#define WM8731_DAI_REG_ADDR                         0x0E
#define WM8731_SAMPLING_CTRL_REG_ADDR               0x10
#define WM8731_ACTIVE_CTRL_REG_ADDR                 0x12
#define WM8731_RESET_REG_ADDR                       0x1E

#define WM8731_LLINE_IN_REG_VAL                     0x17
#define WM8731_RLINE_IN_REG_VAL                     0x17
#define WM8731_LLINE_OUT_REG_VAL                    0x79
#define WM8731_RLINE_OUT_REG_VAL                    0x79
#define WM8731_ANALOG_AUDIO_PATH_CTRL_REG_VAL       0x13
#define WM8731_DIGITAL_AUDIO_PATH_CTRL_REG_VAL      0x00
#define WM8731_POWER_DOWN_CTRL_REG_VAL              0x00
#define WM8731_DAI_REG_VAL                          0x49
#define WM8731_SAMPLING_CTRL_REG_VAL                0x99
#define WM8731_ACTIVE_CTRL_REG_VAL                  0x01
#define WM8731_RESET_REG_VAL                        0x00

/* ----------------------------------------------------------------------------
 * Data types
 * --------------------------------------------------------------------------*/
struct app_env_tag
{
    struct rm_param_tag rm_param;
    uint8_t rm_link_status;
    uint16_t rm_lostLink_counter;
    uint16_t rm_unsuccessLink_counter;
    uint8_t audio_streaming;
};

struct wm8731_i2c_message
{
    uint8_t register_address;
    uint8_t register_data;
};

typedef enum
{
    PKT_LEFT = 0,
    PKT_RIGHT = 1
} PacketSide;

typedef enum
{
    DSP_STARTING = 0,
    DSP_IDLE,
    DSP_BUSY
} DSP_State;

typedef struct
{
    CODEC codec;
    DSP_State state;
    PacketSide dsp_side;
    OperationParameters channels[2];
    swFIFO incoming;
    swFIFO outgoing[2];
} LPDSP32Context;

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/
extern void *ISR_Vector_Table;
extern void App_Initialize(void);

extern void APP_RM_Init(uint8_t side);

uint32_t * Read_buffer(uint8_t side);

extern int16_t spi_buf[2 * SUBFRAME_LENGTH];

/* ----------------------------------------------------------------------------
 * Global shared variables
 * --------------------------------------------------------------------------*/
extern int16_t asrc_in_buf[];
extern int32_t data_fifo_rec;
extern uint32_t asrc_state_mem_rx[2][33];
extern int16_t spi_buf[];
extern int32_t pcm_buf[];
extern int32_t dmic_buffer_in[];
extern volatile uint8_t i2c_tx_buffer_data[];
extern volatile uint8_t i2c_tx_buffer_index;

/* Define a LPDSP32 Context */
extern LPDSP32Context lpdsp32;

extern bool rx_DMA_ready_DMIC(void);

extern bool rx_DMA_ready_PCM(void);

extern bool rx_DMA_ready_SPI(void);

extern bool (*rx_DMA_ready)(void);

extern void Encry_AES_128 (uint8_t *ptr, uint8_t block_num);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* APP_H */
