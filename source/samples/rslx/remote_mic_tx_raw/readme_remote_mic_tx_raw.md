Stereo Raw Audio Stream Broadcast Transmitter Custom Protocol Sample Application
================================================================================


NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample code demonstrates the functionality of a transmitter that 
broadcasts audio data wirelessly using the Audio Stream Broadcast Custom 
Protocol (remote microphone custom protocol). It showcases the RSL10 firmware 
implementation required for remote microphone or wireless auxiliary audio 
input use cases. This sample code illustrates how the transmitter configures 
the required peripheral and audio processing hardware blocks for audio input, 
and how the unencoded audio input data is then encoded before it is 
broadcast over the air. More specifically, the stereo audio stream is 
received from the audio input source (through the DMIC, SPI, or PCM 
interface), sent to the Asynchronous Sample Rate Converter (ASRC) to 
synchronize the audio sampling rate between the audio renderer and RSL10 if
needed, encoded and encrypted, and finally, transmitted wirelessly via 
the remote microphone custom protocol, so that receivers can wirelessly 
receive the encoded (left or right channel) audio stream. The audio input on 
the RSL10 receiver can be configured to be received from digital microphones 
and from external devices over the SPI and PCM interfaces. The LPDSP32 encoder 
can be configured to use the G.722 or CELT CODEC options.

**This sample project is structured as follows:**

The source code exists in a `code` folder, all application-related include
header files are in the `include` folder, and the `main()` function `app.c` is
located in the parent directory. All source code in the codec folder pertains
to the LPDSP32 DSP encoder program.

Code
----
    app_func.c      - Support functions and message handlers pertaining to
                      peripheral and audio processing hardware blocks
    app_init.c      - All initialization functions are called here, but the
                      implementations are in their respective C files
    fifo.c          - Support functions required to handle data sent to and
                      received from the LPDSP32 DSP for encoding
    rm_app.c        - Support functions and message handlers pertaining to the
                      remote microphone custom protocol

Include
-------
    app.h           - Overall application header file
    fifo.h          - Header file for support functions required to handle 
                      data sent to and received from the LPDSP32 DSP for 
                      encoding

The appropriate libraries and `include` files are loaded according to the 
build configuration selected.

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development 
Board. 

The raw (unencoded) audio input to the RSL10 transmitter can be configured to
be sent from the SPI and PCM interfaces using the Ezairo 7100 Evaluation and
Development Board. Additionally, the Open Music Labs Codec Shield (Wolfson
WM8731 codec) can also be used to send audio input over the PCM interface to
the RSL10 QFN Evaluation and Development Board.

For the transmitter configuration where the input audio to the RSL10 is sent
from the Ezairo 7100 over the SPI bus, ensure that the following pins are
connected between the two devices:

    ----------------------------------------------
    Pin Signal               | RSL10 | Ezairo 7100
    ----------------------------------------------
    SPI Chip Select          | DIO0  | DIO25  
    SPI MISO                 | DIO1  | DIO26
    SPI MOSI                 | DIO2  | DIO23 
    SPI Clock                | DIO3  | DIO24 
    Sampling Frequency Clock | DIO7  | DIO21 
    VDDO                     | VDDO  | VDDO3
    Ground                   | GND   | GND   
    ----------------------------------------------

In this configuration, the Ezairo 7100 is the SPI Master and the RSL10 is the 
SPI Slave.

This sample application is compatible with a variety of sources for PCM input
data. The PCM format and input pins used for the PCM interface are flexible,
and these configuration parameters can be modified within the sample
application to meet the needs of the PCM input source. For sample purposes,
this application includes build configurations supporting input from an Ezairo
7100 Evaluation and Development Board or an Open Music Labs Codec Shield
(Wolfson WM8731 codec).

For the transmitter configuration where the input audio to the RSL10 is sent
from the Ezairo 7100 over the PCM interface, ensure that the following pins 
are connected between the two devices:

    ----------------------------------------------------
    Pin Signal (RSL10 Perspective) | RSL10 | Ezairo 7100
    ----------------------------------------------------
    PCM Frame Sync                 | DIO0  | DIO25
    PCM Data Out                   | DIO1  | DIO26
    PCM Data In                    | DIO2  | DIO23
    PCM Clock                      | DIO3  | DIO24
    VDDO                           | VDDO  | VDDO3
    Ground                         | GND   | GND
    ----------------------------------------------------
In this configuration, the Ezairo 7100 is the PCM Master and the RSL10 is the 
PCM Slave.

The Open Music Labs Codec Shield can be used as a PCM input source by stacking
it on the RSL10 QFN board, facilitating an easy plug-and-play connection.
However, additional modifications are required to set up this configuration.
For the transmitter configuration where the input audio to the RSL10 is sent
from the from the Open Music Labs Codec Shield over the PCM interface, ensure
that the following hardware instructions are considered:
- Ensure that the digital pins on the shield are connected to the appropriate
  DIOs on the RSL10:
  
    ---------------------------------------------------------------------
    Pin Signal (RSL10 Perspective) | RSL10 | Open Music Labs Codec Shield
    ---------------------------------------------------------------------
    PCM Frame Sync                 | DIO9  | J3-6
    PCM Data In                    | DIO11 | J3-4
    PCM Data Out (not used)        | DIO10 | J3-5
    PCM Clock                      | DIO12 | J3-3
    I2C SCL                        | DIO2  | J2-1
    I2C SDA                        | DIO3  | J2-2
    ---------------------------------------------------------------------

- Ensure that the oscillator (X1) on the shield is compatible with outputting
  a sampling rate of 32 KHz. This sample application configures the shield
  appropriately, assuming the use of a 12 MHz oscillator (the frequency of 
  the oscillator populated by default is 11.2896 MHz).
- Clear the quad buffer/line driver. Ensure that the appropriate pins are
  shorted on the shield's PCB after removal of the buffer:
  ADCDAT(2) -> MISO(3), LRC(8) -> SS(9), and BCLK(11) -> SCK(12).
- Ensure that a jumper is placed on the quad buffer/line driver enable (EN)
- Populate a 3 way jumper on P7 on the RSL10 QFN board to configure the 3.3V
  pin as an output supply voltage to power the shield.
- Ensure that the potentiometers (MOD0 and MOD1) are not populated on the 
  shield.
In this configuration, the audio codec shield is the PCM Master and the RSL10
is the PCM Slave.

For the transmitter configuration where the input audio to the RSL10 is sent
from the digital microphones over the DMIC interface, ensure that that the 
DMIC interface is configured as follows:

    ------------------
    Pin Signal | RSL10
    ------------------
    Power      | VDDO
    Clock      | DIO0
    Data       | DIO1
    Ground     | GND
    ------------------
When using two digital microphones for stereo input, ensure that the data is
being driven by both microphones appropriately (rising clock edge vs falling
clock edge).

In addition, note the following when setting up the various hardware 
configurations:
- Ensure that no jumpers are placed on P9 on the RSL10 QFN Evaluation and
  Development Board for the configurations where VDDO is being supplied
  externally.
- On the Ezairo 7100 EDK, ensure that jumpers are placed on VBATOD-EN and on 
  pins 1-2 on the `VDDO*_SEL` of the `VDDO*` being provided to the RSL10 in 
  all configurations.
- DIO7 and DIO8 are mapped to the SCL and SDA pins respectively on the RSL10
  QFN Evaluation and Development Board.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the
*Getting Started Guide* for your IDE for more information.

Select the project configuration according to the required optimization level.
Use **Debug** configuration for optimization level **None**, or **Release**
configuration for optimization level **More** or **O2**.

Verification
============
To verify that this application is functioning correctly, the transmitter’s 
input audio stream can be monitored on the receiver. In addition to ensuring 
that the hardware setup is as outlined in the *Hardware Requirements* section 
for the various transmitter configurations, this sample code needs to be 
compiled and flashed onto the RSL10 Evaluation and Development Board with the 
appropriate pre-processor definition settings, to ensure that the transmitter 
can function as expected. The corresponding Ezairo 7100 firmware also needs to
be flashed onto the Ezairo 7100 Evaluation and Development Board, for those 
configurations where it is used. The Ezairo 7100 firmware required to send its
input audio to the RSL10 is provided in the `RSL10_Utility_Apps.zip` file.

The sample project has build configurations that allow you to select the
G.722 or CELT encoder. This option is defined in the project settings by the 
following preprocessor symbols:
`CODEC_CONFIG == CODEC_CONFIG_G722` or `CODEC_CONFIG == CODEC_CONFIG_CELT`
For the CELT CODEC option, due to the encoding latency, only the left channel is 
encoded and transmitted for both left and right receivers.

For the transmitter configuration where the input audio to the RSL10 is sent
from the Ezairo 7100 Evaluation and Development Board over the SPI bus, 
compile and flash this sample application with the `INPUT_INTRF` define as
follows:

    #define INPUT_INTRF                     SPI_RX_RAW_INPUT

In addition, program the Ezairo 7100 with the `bi_directional_master_stereo` 
application provided in the utility applications folder. AI0 and AI1 capture 
the left and right audio channel data respectively (16 kHz sampling rate) on 
the Ezairo 7100 Evaluation and Development Board.

For the transmitter configuration where the input audio to the RSL10 is sent
from the Ezairo 7100 over the PCM interface, compile and flash this sample
application with the `INPUT_INTRF` and `PCM_RX_RAW_SOURCE` defines as follows:

    #define INPUT_INTRF                     PCM_RX_RAW_INPUT
    #define PCM_RX_RAW_SOURCE               EZAIRO_7100

In addition, program the Ezairo 7100 with the `bi_directional_pcm_master` 
application provided in the utility applications folder. Note that the
`bi_directional_pcm_master` program only captures the left channel audio data
through AI0 on the Ezairo 7100 Evaluation and Development Board.

For the transmitter configuration where the input audio to the RSL10 is sent
from the Open Music Labs Codec Shield over the PCM interface, compile and 
flash this sample application with the `INPUT_INTRF` and `PCM_RX_RAW_SOURCE` 
defines as follows:

    #define INPUT_INTRF                     PCM_RX_RAW_INPUT
    #define PCM_RX_RAW_SOURCE               AUDIO_CODEC_SHIELD

The Open Music Labs Codec Shield captures the left and right audio through its
3.5mm stereo input jack (`LINE_IN`). In this configuration, the Open Music 
Labs Codec Shield is the PCM Master and the RSL10 is the PCM Slave.

For the transmitter configuration where the input audio to the RSL10 is sent
from the digital microphones over the DMIC interface, compile and flash this
sample application with the `INPUT_INTRF` define as follows:

    #define INPUT_INTRF                     DMIC_RX_RAW_INPUT

Note that the frequency channel hopping list (`RM_HOPLIST`), modulation index
(`app_env.rm_param.mod_idx`), and remote microphone custom protocol streaming
address (`app_env.rm_param.accessword`) in the receiver and transmitter RSL10
programs need to be matched, in order to successfully transmit/receive audio 
via the remote microphone custom protocol.

To enable end-to-end AES-128 ECB encryption, you can enable the crypto 
flag as below:

    #define CRY_AES_128_ECB                 1

Please, ensure that the crypto flag has been enabled on the receiver side
and the same key (`KEY_AES_128_ECB`) is used.

Notes
=====
The RSL10 and/or Ezairo 7100 may be damaged if the corresponding
transmitter/receiver side programs do not match because of potential pin
direction conflicts. Similarly, when using the Open Music Audio Shield as
the input source, the hardware modifications outlined in the readme must be
followed to prevent damage to RSL10 and/or the Open Music Audio Shield.

Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO13 can be implemented with the following steps:

1.  Connect DIO13 to ground.
2.  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO13 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
