/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 *  - This application demonstrates the use I2C CMSIS-Driver to operate the
 *    I2C interface. It configures the I2C as slave and calls "SlaveReceive".
 *    A new transfer starts by the master (SPI0) when the button (DIO5)
 *    is pressed.
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>

/* Global variables */
DRIVER_GPIO_t *gpio;
ARM_DRIVER_I2C *i2c;

/* Data buffers */
uint8_t buff_tx[] __attribute__ ((aligned(4))) = "RSL10 I2C TEST";
uint8_t buff_rx[sizeof(buff_tx)] __attribute__ ((aligned(4)));
size_t buff_size = sizeof(buff_tx);

/* ----------------------------------------------------------------------------
 * Function      : void Button_EventCallback(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : Interrupt handler triggered by a button press. Cancel any
 *                 ongoing transfers, switch to Master mode and start a
 *                 MasterTransmit operation through I2C.
 * Inputs        : event - event number which triggered the callback
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Button_EventCallback(uint32_t event)
{
    static bool ignore_next_dio_int = false;
    if (ignore_next_dio_int)
    {
        ignore_next_dio_int = false;
    }
    /* Button is pressed: Ignore next interrupt.
     * This is required to deal with the debounce circuit limitations. */
    else if (event == GPIO_EVENT_0_IRQ)
    {
		ignore_next_dio_int = true;
        /* Abort current transfer */
        i2c->Control(ARM_I2C_ABORT_TRANSFER, 0);

        /* Start transmission as master */
        i2c->MasterTransmit(RTE_I2C0_SLAVE_ADDR_DEFAULT, buff_tx, buff_size, false);
        PRINTF("BUTTON PRESSED: START TRANSMISSION\n");
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_MasterCallback(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a master callback. The parameter event
 *                 indicates one or more events that occurred during driver
 *                 operation.
 * Inputs        : event - I2C Events notification mask
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void I2C_MasterCallback(uint32_t event)
{
    bool direction = (i2c->GetStatus().direction == 1U);

    /* Check if transfer is done */
    if (event & ARM_I2C_EVENT_TRANSFER_DONE)
    {
        /* Device is configured as transmitter */
        if (direction == I2C_STATUS_DIRECTION_TX)
        {
            /* Delay time for switching modes of operation */
            Sys_Delay_ProgramROM((uint32_t)(0.5 * SystemCoreClock));

            /* MasterTransmit finished, start MasterReceive */
            i2c->MasterReceive(RTE_I2C0_SLAVE_ADDR_DEFAULT, buff_rx, buff_size, false);
        }
        /* Check if device is configured as receiver */
        else if (direction == I2C_STATUS_DIRECTION_RX)
        {
            /* Check the address nack event */
            if (event & ARM_I2C_EVENT_ADDRESS_NACK)
            {
                /* MasterTransmit finished, start MasterReceive */
                i2c->MasterReceive(RTE_I2C0_SLAVE_ADDR_DEFAULT, buff_rx, buff_size, false);
                return;
            }

            /* Delay time for switching modes of operation */
            Sys_Delay_ProgramROM((uint32_t)(0.5 * SystemCoreClock));

            /* MasterReceive finished, go back to SlaveReceive default mode */
            /* Enter slave receiver mode */
            i2c->SlaveReceive(buff_rx, buff_size);

            /* Blink LED if received data matches I2C_TX_DATA */
            if (!strcmp((const char *)buff_rx, (const char *)buff_tx))
            {
                /* Toggle LED state 2 times for 500 milliseconds */
                ToggleLed(2, 500);
                PRINTF("LED BLINKED: CORRECT_DATA_RECEIVED\n");
            }
        }
    }
    /* Check if transfer error occurred */
    else if ((event & ARM_I2C_EVENT_TRANSFER_INCOMPLETE) ||
             (event & ARM_I2C_EVENT_BUS_ERROR))
    {
        /* Abort current transfer */
        i2c->Control(ARM_I2C_ABORT_TRANSFER, 0);

        /* Go back to SlaveReceive default mode */
        i2c->SlaveReceive(buff_rx, buff_size);

        /* Toggle LED state 10 times for 100 milliseconds to indicate error */
        ToggleLed(10, 100);
        PRINTF("TRANSFER INCOMPLETE OR BUS ERROR\n");
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_SlaveCallback(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a slave callback. The parameter event
 *                 indicates one or more events that occurred during driver
 *                 operation.
 * Inputs        : event - I2C Events notification mask
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void I2C_SlaveCallback(uint32_t event)
{
    bool direction = (i2c->GetStatus().direction == 1U);

    /* Check if transfer is done */
    if (event & ARM_I2C_EVENT_TRANSFER_DONE)
    {
        if (direction == I2C_STATUS_DIRECTION_TX)
        {
            /* SlaveTransmit finished, go back to SlaveReceive default mode */
            /* Enter slave receiver mode */
            i2c->SlaveReceive(buff_rx, buff_size);
        }
        /* Check if device is configured as receiver */
        else if (direction == I2C_STATUS_DIRECTION_RX)
        {
            /* SlaveReceive finished, start SlaveTransmit */
            /* Enter slave transmitter mode */
            i2c->SlaveTransmit(buff_tx, buff_size);

            /* Blink LED if received data matches I2C_TX_DATA */
            if (!strcmp((const char *)buff_rx, (const char *)buff_tx))
            {
                /* Toggle LED state 2 times for 500 milliseconds */
                ToggleLed(2, 500);
                PRINTF("LED BLINKED: CORRECT_DATA_RECEIVED\n");
            }
        }
    }
    /* Check if transfer error occurred */
    else if ((event & ARM_I2C_EVENT_TRANSFER_INCOMPLETE) ||
             (event & ARM_I2C_EVENT_BUS_ERROR))
    {
        /* Abort current transfer */
        i2c->Control(ARM_I2C_ABORT_TRANSFER, 0);

        /* Go back to SlaveReceive default mode */
        i2c->SlaveReceive(buff_rx, buff_size);

        /* Toggle LED state 10 times for 100 milliseconds to indicate error */
        ToggleLed(10, 100);
        PRINTF("TRANSFER INCOMPLETE OR BUS ERROR\n");
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void I2C_CallBack(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a callback registered by the function
 *                 Initialize. The parameter event indicates one or more events
 *                 that occurred during driver operation.
 * Inputs        : event - I2C Events notification mask
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void I2C_EventCallback(uint32_t event)
{
    static volatile uint32_t I2C_Event;
    bool mode = (i2c->GetStatus().mode == 1U);
    I2C_Event |= event;

    /* Refresh the watchdog */
    Sys_Watchdog_Refresh();

    if(mode == I2C_STATUS_MODE_MASTER)
    {
        I2C_MasterCallback(event);
    }
    else if (mode == I2C_STATUS_MODE_SLAVE)
    {
        I2C_SlaveCallback(event);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void ToggleLed(uint32_t n, uint32_t delay_ms)
 * ----------------------------------------------------------------------------
 * Description   : Toggle the led pin.
 * Inputs        : n        - number of toggles
 *               : delay_ms - delay between each toggle [ms]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void ToggleLed(uint32_t n, uint32_t delay_ms)
{
    for (; n > 0; n--)
    {
        /* Refresh the watchdog */
        Sys_Watchdog_Refresh();

        /* Toggle led diode */
        gpio->ToggleValue(LED_DIO);

        /* Delay */
        Sys_Delay_ProgramROM((delay_ms / 1000.0) * SystemCoreClock);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, switching to
 *                 the 48 MHz clock and configuring the required DIOs to use
 *                 the i2c.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Prepare the 48 MHz crystal
     * Start and configure VDDRF */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_1_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to 48 MHz oscillator clock */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Initialize gpio structure */
    gpio = &Driver_GPIO;

    /* Initialize gpio driver */
    gpio->Initialize(Button_EventCallback);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    /* Initialize i2c driver structure */
    i2c = &Driver_I2C0;

    /* Initialize i2c, register callback function */
    i2c->Initialize(I2C_EventCallback);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system ans drivers, then send an I2C frame 
                   controlled on button press.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* Initialize the clocks and configure button press interrupt */
    Initialize();
    PRINTF("DEVICE INITIALIZED\n");

    /* Wait for new transfer as slave */
    i2c->SlaveReceive(buff_rx, buff_size);

    /* Spin loop */
    while (true)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();
    }
}
