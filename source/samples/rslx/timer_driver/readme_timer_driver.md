                        Timer Driver Sample Code
                        ============================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in Software_Use_Agreement.rtf
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project demonstrates an application that:

1.  Uses timer[0] to give the interval between each blink. 
    Timer 0 works in free run mode.
2.  Uses timer[1] to change the timer[0] interval and start timer[2]. 
    Timer[1] works in single shot mode.
3.  Uses timer[2] to gradually reduce the timer[0] interval 4 times.
    Timer[2] works in multi shot mode.

The source code exists in `app.c`, with additional `app.h` and `RTE_device.h`
header files included.

Verification
------------
To verify that the application is working correctly, connect the board
to the power supply. After the board is powered up, the LED (DIO6) continuously
flashes on the Evaluation and Development Board with a variable frequency.
The frequency changes according to the following order:

1.  At the beginning the LED blinks rapidly (timer[0] interval).
2.  After few seconds, the LED blinking slows down to a low frequency.
3.  The LED gradually increases its blink 4 times.  

Hardware Requirements
---------------------
This application can be executed on any Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the
Getting Started Guide for your IDE for more information.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which will
    pause at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application will work
    properly.

==============================================================================
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
