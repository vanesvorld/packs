/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 *  - Simple example on how to use RSL10 timer driver
 * ------------------------------------------------------------------------- */
#include <app.h>
#include <RTE_Device.h>
#include <TIMER_RSLxx.h>
#include <GPIO_RSLxx.h>
#include <printf.h>

#if !RTE_TIMER
    #error "Please configure TIMER in RTE_Device.h"
#endif    /* if !RTE_TIMER */

/* Global variables and types */
DRIVER_TIMER_t *timer;
DRIVER_GPIO_t *gpio;
DRIVER_GPIO_t Driver_GPIO;
DRIVER_TIMER_t Driver_TIMER;

/* ----------------------------------------------------------------------------
 * Function      : void Timer_EventCallback(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a callback registered by the function
 *                 Initialize. Based on event argument different actions are
 *                 executed.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Timer_EventCallback(uint32_t event)
{
    /* Timer 0 timeout value */
    static uint32_t timeout = 0;

    /* Check if timer0 event has been triggered */
    if (event & TIMER_TIMER0_EVENT)
    {
        /* Toggle LED Pin */
        gpio->ToggleValue(LED_DIO);
        PRINTF("TIMER0 TRIGGERED\n");

        /* Exit interrupt callback */
        return;
    }

    /* Check if timer1 event has been triggered */
    if (event & TIMER_TIMER1_EVENT)
    {
        /* Reconfigure timer 0 timeout value */
        timeout = TIMER0_BASE_TIMEOUT;
        timer->SetValue(TIMER_0, timeout);

        /* Start timer 2 (multi shot mode) */
        timer->Start(TIMER_2);
        PRINTF("TIMER1 TRIGGERED: RECONFIGURED TIMER0 INTERVAL VALUE AND STARTED TIMER2\n");

        /* Exit interrupt callback */
        return;
    }

    /* Check if timer2 event has been triggered */
    if (event & TIMER_TIMER2_EVENT)
    {
        /* Reconfigure timer 0 timeout value */
        timeout -= TIMER0_TIMEOUT_STEP;
        timer->SetValue(TIMER_0, timeout);
        PRINTF("TIMER2 TRIGGERED: REDUCED TIMER0 INTERVAL VALUE\n");

        /* Exit interrupt callback */
        return;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the drivers.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    gpio = &Driver_GPIO;
    gpio->Initialize(NULL);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    /* Initialize timer structure */
    timer = &Driver_TIMER;

    /* Configure timer callback function */
    timer->Initialize(Timer_EventCallback);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the drivers. Enable and run timers.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* Initialize the drivers */
    Initialize();
    PRINTF("DEVICE INITIALIZED\n");
    /* Start timer 0 (free run mode) */
    timer->Start(TIMER_0);
    PRINTF("STARTED TIMER0\n");
    /* Start timer 1 (single shot mode) */
    timer->Start(TIMER_1);
    PRINTF("STARTED TIMER1\n");

    /* Spin loop */
    while (true)
    {
        /* Wait for an event */
        SYS_WAIT_FOR_EVENT;

        /* Refresh Watchdog */
        Sys_Watchdog_Refresh();
    }
}
