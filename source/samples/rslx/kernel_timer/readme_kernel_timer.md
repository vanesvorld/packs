Kernel Timer Sample Code
========================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project implements a kernel timer. The timer is set to two 
seconds, and the kernel calls the timer's handler routine upon timer 
expiration. Within the handler, the state of the LED [DIO6] is changed 
(`ON -> OFF | OFF -> ON`).


**This sample project is structured as follows:**

The source code exists in a `code` folder, all application-related include
header files are in the `include` folder, and the `main()` function `app.c` is 
located in the parent directory.

Code
----
    app_init.c    - All initialization functions are called here, but the 
                    implementation is in the respective C files
    app_process.c - Message handlers for application

Include
-------
    app.h         - Overall application header file
    
Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board 
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.

Select the project configuration according to the required optimization level. 
Use **Debug** configuration for optimization level **None**, or **Release**
configuration for optimization level **More** or **O2**.

Verification
------------
To verify that this application is functioning correctly, use an RSL10 
Evaluation and Development Board and load the sample application. After 
resetting the board [RESET], the LED [DIO6] on the Evaluation and Development 
Board blinks. It remains ON or OFF for the timer's interval, which is set to 
two seconds.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
