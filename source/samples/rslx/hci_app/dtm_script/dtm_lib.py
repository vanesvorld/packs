# ------------------------------------------------------------------------------
# Copyright (c) 2016 Semiconductor Components Industries, LLC (d/b/a ON
# Semiconductor), All Rights Reserved
# 
# This code is the property of ON Semiconductor and may not be redistributed
# in any form without prior written permission from ON Semiconductor.
# The terms of use and warranty for this code are covered by contractual
# agreements between ON Semiconductor and the licensee.
# ------------------------------------------------------------------------------
#  dtm_lib.py
#    Library which performs device test mode transmission and receiver tests for
#    RSL10.
# ------------------------------------------------------------------------------
#  $Revision: 1.1 $
#  $Date: 2018/11/19 15:09:46 $
# ------------------------------------------------------------------------------

import serial
import time
import binascii
import math
import re

# HCI commands
HCI_LE_TRANSMITTER_TEST              ='0x201e'
HCI_LE_RECEIVER_TEST                 ='0x201d'
HCI_LE_END_TEST                      ='0x201f'
HCI_LE_ENH_TRANSMITTER_TEST          ='0x2034'
HCI_LE_ENH_RECEIVER_TEST             ='0x2033'
HCI_LE_RESET                         ='0x0c03'

class Payload_Type:
    # Payload type enum values
    PRBS9                = 0 #Random 9 bits
    Four_Ones_Four_Zeros = 1 #11110000
    One_Zero_Alternating = 2 #10101010
    PRBS15               = 3 #Random 15 bits
    All_Ones             = 4 #11111111
    All_Zeros            = 5 #00000000
    Four_Zeros_Four_Ones = 6 #00001111
    Zero_One_Alternating = 7 #01010101


    @classmethod
    def get_name(cls, payload_type_value):
        '''
        Class method returns a string name of the corresponded payload_type 
        value.
        Parameters:
           payload_type_value      - 0 to 7 value for payload type
        '''
        if payload_type_value == cls.PRBS9:
            payload_type = "Random 9 bits"
        elif payload_type_value == cls.Four_Ones_Four_Zeros:
            payload_type = "Four Ones Four Zeros"
        elif payload_type_value == cls.One_Zero_Alternating:
            payload_type = "One Zero Alternating"
        elif payload_type_value == cls.PRBS15:
            payload_type = "Random 15 bits"
        elif payload_type_value == cls.All_Ones:
            payload_type = "All Ones"
        elif payload_type_value == cls.All_Zeros:
            payload_type = "All Zeros"
        elif payload_type_value == cls.Four_Zeros_Four_Ones:
            payload_type = "Four Zeros Four Ones"
        elif payload_type_value == cls.Zero_One_Alternating:
            payload_type = "Zero One Alternating"
        else:
            payload_type = "Invalid payload type entered"
        return payload_type

class PHY_Type:
    # PHY type enum
    LE_1M = 1
    LE_2M = 2

    @classmethod
    def get_name(cls, PHY_type_value):
        '''
        Class method returns a string name of the corresponded PHY_type value.
        Parameters:
           PHY_type_value           - 1 (LE_1M) or 2 (LE_2M) value for PHY type
        '''
        if PHY_type_value == cls.LE_1M:
            PHY_type_value = "LE 1M"
        elif PHY_type_value == cls.LE_2M:
            PHY_type_value = "LE 2M"
        else:
            PHY_type_value = "Invalid PHY type entered"
        return PHY_type_value

class Mod_Index:
    # Mod index enum
    Standard    = 0
    Stable      = 1

    @classmethod
    def get_name (cls,mod_index_value):
        ''''
        Class method returns a string name of the corresponded mod_index value.
        Parameters:
           mod_index_value     - 0 (standard) or 1 (stable) value for mod_index
        '''
        if mod_index_value == cls.Standard:
            mod_index = "Standard"
        elif mod_index_value == cls.Stable:
            mod_index = "Stable"
        else:
            mod_index = "Invalid mod index entered"
        return mod_index

class DTMError(Exception):
    def __init__ (self, test_name, function, message):
        '''
        Initializes parameters for DTMError class.
        Parameters:
           test_name       - The test being executed
           function        - The function calling the error
           message         - The error message thrown by the function
        '''
        self.Test_Name = test_name
        self.Function = function
        self.Message = message

    def error_message(self):
        '''
        Returns a string with the test name, function and message
        '''
        return "%s | %s | %s" % (self.Test_Name, self.Function, self.Message)

# The following error classes inheret the base DTMError class properties.
class ConnectionError (DTMError):
    pass

class PacketError(DTMError):
    pass

class ParameterError(DTMError):
    pass

class DTMTest(object):
    def __init__(self,setup):
        '''
        Sets up variables for DTM Test object from parameters stored in a dict.
        Parameters:
            setup                   - Dictionary containing the setup parameters
        Dict Keys:
            LT_COM_Port             - The lower tester COM Port
            DUT_COM_Port            - The device under test COM Port
            DTM_Channel             - The Physical BLE channel for testing (0 to 39)
            Payload_Length          - Packet length must be between 0 and 255
            Packet_Payload_Type     - Packet type between 0 and 7
            Baud_Rate               - Rate of data transfer with serial port
            Test_Run_Time           - Minimum duration of each test
            PHY                     - Must be either 1 (1 Mbps) or 2 (2 Mbps) for ENH
            Mod                     - Must be either 0 (standard) or 1 (stable) for
                                      ENH.
            Log                   - Log message flag. Set to off by default
        Global Variables:
            Start_Time              - Stores time when test started
            TX_PER                  - Value of transmission percentage error rate
            RX_PER                  - Value of receiver percentage error rate
            Packet_Interval         - Shortest time to transfer a packet
            Max_Packet_Interval     - Longest time to transfer a packet
        '''
        self.LT_COM_Port = setup['LT_COM_Port']
        self.DUT_COM_Port = setup['DUT_COM_Port']
        self.DTM_Channel = setup['DTM_Channel']
        self.Payload_Length = setup['Packet_Payload_Length']
        self.Packet_Payload_Type = setup['Packet_Payload_Type']
        self.Baud_Rate = setup['Baud_Rate']
        self.Test_Run_Time = setup['Test_Run_Time']
        self.PHY = setup['PHY_Type']
        self.Mod = setup['MOD_Index']
        self.Log = setup['Log']
        self.Start_Time = 0.0
        self.TX_PER = 100
        self.RX_PER = 100

        # I(L) is the calculated packet interval (L) in ms
        # I(L) = ceil((L+249)/625)*0.625 ms
        # Length of packet (L) is calculated as follows:
        # L = (size of payload [variable bytes] + 
        #      preamble [1 or 2 bytes] + 
        #      PDU length [1 byte] + 
        #      PDU header [1 byte] + 
        #      synch word [4 bytes] +
        #      CRC [3 bytes]) / Phy Rate 
        #

        L = self.Payload_Length
        if self.PHY == PHY_Type.LE_2M:
            L = (L + 11) / 2
        else:
            L = (L + 10) / 1

        self.Packet_Interval = (math.ceil((L*8 + 249)/625.0) * 0.625)

        # T(L) is the maximum packet interval in ms
        # T(L) = max(I(L) + 10ms , 12.5ms)
        self.Max_Packet_Interval = max(self.Packet_Interval + 10.0, 12.5)

    def log(self, message):
        '''
        Display Log messages when flag is set.
        Parameters
            message     - String to display on console
        '''
        if self.Log:
            print message

    def convert_to_hex(self, value):
        '''
        Convert a list or string into the corrosponding hex values
        Parameters:
          value           - string or list of string types
        Returns:
          hex_value       - hex representation of the value
        '''
        hex_value = value

        if type (value) is list:
            hex_value = ''.join(value)

        #Convert to hex and append '0x'. If the hex_value is blank then set to 0 
        hex_value = binascii.hexlify(hex_value)
        if hex_value:
            hex_value = '0x' + hex_value
        else:
            hex_value = '0x0'
        return hex_value

    def parameter_check(self):
        '''
        Checks if the parameters entered are valid
        '''
        # If channel is out of range then throw error.
        if self.DTM_Channel < 0 or self.DTM_Channel > 39:
            raise ParameterError (self.Test_Name,"Parameter Check",
                                  "Channel must be between 1 and 40.")

        # Check the payload length to be between 0 and 255.
        if self.Payload_Length < 0 or self.Payload_Length > 255:
            raise ParameterError (self.Test_Name, "Parameter Check",
                                  "Payload length must be between 0 and 255.")

        # Check the payload type so it is is within the of the supported values.
        if  self.Packet_Payload_Type < Payload_Type.PRBS9 or                   \
            self.Packet_Payload_Type > Payload_Type.Zero_One_Alternating:
            raise ParameterError (self.Test_Name, "Parameter Check",
                                  "Payload type must be either 0 (PRBS9), "    \
                                  "1 (11110000), 2 (10101010), 3 (PRBS15), "   \
                                  "4 (11111111), 5 (00000000), 6 (00001111), " \
                                  "7 (01010101)")

        # Check the PHY bit rate rate is LE 1M or LE 2M
        if self.PHY != PHY_Type.LE_1M and self.PHY != PHY_Type.LE_2M:
            raise ParameterError (self.Test_Name, "Parameter Check",
                                  "PHY type must be either 1 (LE_1M) or 2 (LE_2M).")

        # Check for standard or stable modulation.
        if self.Mod != Mod_Index.Stable and self.Mod != Mod_Index.Standard:
            raise ParameterError (self.Test_Name, "Parameter Check",
                                  "MOD Type length must be 0 (Standard) or 1 (Stable).")

    def serial_init(self, com_port):
        '''
        Initialize serial port object with specified com port, baud rate and
        timeout.Uses default values for other parameters.
        Parameters
           com_port        - Port number for serial port initialization
        Returns
           serial_instance - Initialized serial port with port number com_port
        '''
        self.log ("Initialize serial port: %s" % com_port)
        try:
            serial_instance = serial.Serial(port=com_port,          \
                                            baudrate=self.Baud_Rate,\
                                            timeout=self.Max_Packet_Interval)
        except serial.SerialException as e:
            raise ConnectionError (self.Test_Name, "Serial_Init", 
                                   "Serial Port Error: " + e.message)
        if self.Log:
            print "Port settings: "
            for k,v in serial_instance.getSettingsDict().iteritems():
                print "%- 20s: %s" % (k,v)
        return serial_instance

    def serial_cleanup(self, serial_instance):
        '''
        Close serial port of serial_instance.
        Parameters
          received_packets    - Number of packets received from serial port
        '''
        self.log("Closing serial port: %s" % serial_instance.port)
        serial_instance.close()

    def serial_tx(self, serial_instance, command):
        '''
        Create packet according to command and send it over serial port.
        Parameters
           serial_instance - Initialized serial port
           command         - End of Test/Reset/Tests to be conducted
        '''
        packet = self.generate_packet(command)
        try:
            self.log("Tx packet: %s" % self.convert_to_hex(packet))
            serial_instance.write(packet)
        except serial.SerialException as e:
            raise PacketError (self.Test_Name,"Serial Tx",
                               "Error sending packet "+ e.message)

    def serial_rx(self, serial_instance, command):
        '''
        Reads the serial port and returns the total number of packets after if
        the end of test command is given.
        Parameters
           serial_instance         - Initialized serial port
           command                 - End of Test/Reset/Tests to be conducted
        Returns
           total_packets_received  - Total number of valid received packets
        '''
        if command == HCI_LE_END_TEST:
            expected_num_bytes = 9
        else:
            expected_num_bytes = 7

        try:
            data = serial_instance.read(expected_num_bytes)
            self.log("Rx packet: %s" % self.convert_to_hex(data))

        except serial.SerialException as e:
            raise ConnectionError (self.Test_Name, "Serial RX "    \
                                   "Cannot read serial port "+ e.message)
        # Verify that the expected response is received
        self.check_event_response(data,command)
        if command == HCI_LE_END_TEST:
            # Check if the data has 9 bits of data
            if len(data) != expected_num_bytes:
                raise PacketError (self.Test_Name,"Serial RX ",
                                   "Actual data length %d Expected data "     \
                                   "length %d" % (len(data),expected_num_bytes))

            # Extract the total packets received from the packet by reading the
            # 2 most significant bytes of the packet.
            total_packets_received = ord(data[8]) << 8 | ord(data[7])
            self.log("Total number packets %d" % total_packets_received)

        else:
            total_packets_received = 0

        return total_packets_received

    def generate_packet(self, op_code):
        '''
        Generates packet of form:
        |   HDR  | OP_CODE | DATA LENGTH |  PAYLOAD  |
        | 1 byte | 2 bytes |   1 byte    | var bytes |
        Parameters
           op_code        - The command to give to the device
        Returns
           packet         - Valid packet containing the command for the device
        '''
        packet = []
        payload = []

        # Add header to packet.
        packet += chr(0x1)
        # Iterates through op_code variable (in reverse) 2 characters at a time. 
        # Then converts each pair of characters to a single character. It stops 
        # iterating through before the hex designation (0x).
        for i in reversed(range(2, len(op_code), 2)):
            packet += chr(int(op_code[i] + op_code[i+1], 16))

        # Construct the unique payload according for each op code.
        if op_code == HCI_LE_TRANSMITTER_TEST:
            self.log("\nHCI_LE_TRANSMITTER_TEST | Opcode: %s" % op_code)
            # Generates payload of form:
            # |  Channel | Length |  Type  |
            # |  1 byte  | 1 byte | 1 byte |
            payload.append(self.DTM_Channel)
            payload.append(self.Payload_Length)
            payload.append(self.Packet_Payload_Type)

        elif op_code == HCI_LE_ENH_TRANSMITTER_TEST:
            self.log("\nHCI_LE_ENH_TRANSMITTER_TEST | Opcode: %s" % op_code)
            # The number of octets for the ENH TX test. It is the total amount
            # of bytes of the payload -  1. 
            # Generates payload of form:
            # |  Octets |  Channel | Length |  Type  |   PHY  |
            # |  1 byte |  1 byte  | 1 byte | 1 byte | 1 byte |
            payload.append(self.DTM_Channel)
            payload.append(self.Payload_Length)
            payload.append(self.Packet_Payload_Type)
            payload.append(self.PHY)

        elif op_code == HCI_LE_RECEIVER_TEST:
            self.log("\nHCI_LE_RECEIVER_TEST | Opcode: %s" % op_code)
            # Generates payload of form:
            # |  Channel |
            # |  1 byte  |
            payload.append(self.DTM_Channel)

        elif op_code == HCI_LE_ENH_RECEIVER_TEST:
            self.log("\nHCI_LE_ENH_RECEIVER_TEST | Opcode: %s" % op_code)
            # The number of octets for the ENH RX test. It is the total amount
            # of bytes of the payload -  1. 
            # Generates payload of form:
            # |  Octets |  Channel | Modulation |   PHY  |
            # |  1 byte |  1 byte  |   1 byte   | 1 byte |
            payload.append(self.DTM_Channel)
            payload.append(self.PHY)
            payload.append(self.Mod)

        elif op_code == HCI_LE_END_TEST:
            self.log("\nHCI_LE_END_TEST | Opcode: %s" % op_code)

        elif op_code == HCI_LE_RESET:
            self.log("\nHCI_LE_RESET | Opcode: %s" % op_code)

        # Add length to packet.
        packet += chr(len(payload))

        # Add data to packet (convert each hex value to char).
        for i in range(0, len(payload)):
            packet += chr(payload[i])

        return packet

    def extract_response(self, response):
        '''
        Iterates through the response and formats the bytes into the expected
        format.
        Parameters:
            response                - the data read from TX of the device
        Returns:
            extract                 - the cleaned and formatted response
        '''
        #Split the reponse into a list by bytes
        l_response = re.findall('..', self.convert_to_hex(response)[2:])
        s_index = 0
        index = 0
        #Iterate through the list to find the start bytes (if exists) of the
        #reponse. Expecting packet is in the following format:
        # 04 0E [COMMAND ID] 01 [OP CODE 2 LSB] [OP CODE 2 MSB] [STATUS]
        for r in l_response:
            if r == '04':
                s_index = index
                break
            index+=1
        #Create a string based on the starting index of the start byte. If the
        #string is empty then set it to '0'
        extract = ''.join(l_response[s_index:] + l_response[:s_index])
        if not extract:
            extract = '0'

        return extract

    def check_event_response (self, response, op_code):
        '''
        Check if the response from the device is the expected response given
        the command sent
        Parameters:
          response                - the data read from TX of the device
          op_code                 - the command entered to generate the response
        '''
        packet_op_code = []
        # Status code for successful command response
        success_code = '0x00'
        # Command code determines whether it is HCI_LE_END_TEST command or other
        # commands. 
        if op_code == HCI_LE_END_TEST:
            command_id = '0x06'
        else:
            command_id = '0x04'

        # Expecting packet is in the following format:
        # 04 0E [COMMAND ID] 01 [OP CODE 2 LSB] [OP CODE 2 MSB] [STATUS]
        ext_response = self.extract_response(response)

        self.log("Extracted: 0x%s" % ext_response)
        #Dont care bytes are denoted by [] in the output
        self.log("Expecting: 0x040e%s05%s%s[][][]" % (command_id[2:], op_code[4:],
                                                        op_code[2:4]))
        # Extract the op code
        response_op_code = '0x'+ ext_response[10:12] + ext_response[8:10]
        # Extract op code id
        response_id = '0x'+ ext_response[4:6]

        #Minimum correct response must be minimum of 7 bytes
        if (len (response) < 7):
            raise PacketError (self.Test_Name, "Check Event Response",
                               "Response is %d bytes, must be minimum of 7 bytes"
                               %(len(response)))
        # Compare with actual op code
        if response_op_code != op_code:
            raise PacketError (self.Test_Name, "Check Event Response",
                               "Rx command: %s, Tx command: %s" \
                               % (response_op_code, op_code))

        # Compare response with actual command id
        if response_id != command_id:
            raise PacketError (self.Test_Name, "Check Event Response",
                               "Expected identifier %s, Received %s" \
                               % (command_id, response_id))

    def start_timer(self):
        '''
        Starts the timer.
        '''
        self.Start_Time = time.time()

    def stop_timer(self):
        '''
        Calculate elasped time in seconds and return.
        '''
        return time.time() - self.Start_Time

    def calculate_PER(self, received_packets):
        '''
        Calculates percantage error rate assuming each packet is sent at the 
        packet interval.
        
        Parameters
           received_packets    - Number of packets received from serial port
        Returns
           packet_error_rate   - Ratio between packets sent and packets received
                                 as a percentage
        '''
        # Convert Test_Run_Time to ms then integer division of time taken to send 
        # one packet to get theoretical number of packets sent.
        sent_packets = (self.Test_Run_Time*1000) / self.Packet_Interval

        # Get the number of digits of each value
        digits_tx = int (math.log10(sent_packets)) + 1

        if received_packets > 0:
            digits_rx = int (math.log10(received_packets)) + 1
        else:
            digits_rx = digits_tx

        # Adjust values if the number of received packets is larger than the 
        # number of packets sent due to noise over UART. If the received value
        # is larger by a power of 10 then divide the value by the same power of
        # 10 and get the PER. If the value is larger but does not differ by a 
        # power of 10 then PER is 0. 
        if digits_rx > digits_tx:
            received_packets /= (digits_rx- digits_tx) * 10
            packet_error_rate = 100 -  (received_packets / sent_packets)*100
        elif received_packets > sent_packets:
            packet_error_rate = 0.0
        else:
            #Calculate PER normally if the above two cases do not apply.
            packet_error_rate = 100 -  (received_packets / sent_packets)*100

        self.log("\nCalculate PER:\n\tPackets received %d\n\tPackets sent\t %d"
                   "\n\tPacket interval  %f ms"
                   % (received_packets,sent_packets,self.Packet_Interval))

        return packet_error_rate

    def test_setup(self):
        '''
        Sets up DUT and LT serial instances for generic tests
        '''
        self.parameter_check()
        self.log("\nInitialize LT")
        self.LT_Serial = self.serial_init(self.LT_COM_Port)

        self.log("\nReset LT")
        self.serial_tx(self.LT_Serial, HCI_LE_RESET)
        self.serial_rx(self.LT_Serial, HCI_LE_RESET)

        self.log("\nInitialize DUT")
        self.DUT_Serial = self.serial_init(self.DUT_COM_Port)

        self.log("\nReset DUT")
        self.serial_tx(self.DUT_Serial, HCI_LE_RESET)
        self.serial_rx(self.DUT_Serial, HCI_LE_RESET)

    def test_complete(self, test_type):
        '''
        Stops or starts LT or DUT depending on transmission or receiver tests.
        Returns the number of packets received from LT or DUT
        Parameters
          test_type           - TX or RX test
        Returns
          packets_received    - Number of packets received by DUT or LT
        '''
        if test_type == "TX":
            self.log("\nStopping LT and receiving total packet count")
            self.serial_tx(self.LT_Serial, HCI_LE_END_TEST)
            packets_received = self.serial_rx(self.LT_Serial,HCI_LE_END_TEST)

            self.log("\nStopping DUT")
            self.serial_tx(self.DUT_Serial, HCI_LE_END_TEST)
            self.serial_rx(self.DUT_Serial, HCI_LE_END_TEST)
        else:
            self.log("\nStopping DUT and receiving total packet count")
            self.serial_tx(self.DUT_Serial, HCI_LE_END_TEST)
            packets_received = self.serial_rx(self.DUT_Serial,HCI_LE_END_TEST)

            self.log("\nStopping LT")
            self.serial_tx(self.LT_Serial, HCI_LE_END_TEST)
            self.serial_rx(self.LT_Serial, HCI_LE_END_TEST)

        self.log("\nClose LT")
        self.serial_cleanup(self.LT_Serial)

        self.log("\nClose DUT")
        self.serial_cleanup(self.DUT_Serial)

        return packets_received

    def transmit_test(self):
        '''
        Run trasmit test.
        1) Initialize serial ports for DUT and LT
        2) Reset both ports
        3) Generate tx test packet
        4) Start transmitting from DUT
        5) Start receiveing from LT
        6) Start timer
        7) Stop transmitting from DUT
        8) Stop timer and LT receiver
        9) Get number of packets and calculate packet error rate
        '''
        if self.PHY == PHY_Type.LE_2M:
            self.Test_Name = "ENH Transmit Test"

            self.test_setup()
        
            self.log("\nStart DUT ENH transmission")
            self.serial_tx(self.DUT_Serial, HCI_LE_ENH_TRANSMITTER_TEST)
            self.serial_rx(self.DUT_Serial, HCI_LE_ENH_TRANSMITTER_TEST)

            self.log("\nStart Timer")
            self.start_timer()

            self.log("\nStart LT ENH receiver")
            self.serial_tx(self.LT_Serial, HCI_LE_ENH_RECEIVER_TEST)
            self.serial_rx(self.LT_Serial, HCI_LE_ENH_RECEIVER_TEST)

        else:
            self.Test_Name = "Transmit Test"

            self.test_setup()

            self.log("\nStart DUT transmission")
            self.serial_tx(self.DUT_Serial, HCI_LE_TRANSMITTER_TEST)
            self.serial_rx(self.DUT_Serial, HCI_LE_TRANSMITTER_TEST)

            self.log("\nStart Timer")
            self.start_timer()

            self.log("\nStart LT receiver")
            self.serial_tx(self.LT_Serial, HCI_LE_RECEIVER_TEST)
            self.serial_rx(self.LT_Serial, HCI_LE_RECEIVER_TEST)

        # Wait till elapsed time has passed.
        while self.stop_timer() < self.Test_Run_Time:
            pass

        # Get the Percentage Error Rate for TX tests.
        self.TX_PER = self.calculate_PER(self.test_complete("TX"))

    def receive_test(self):
        '''
        Run receive test.
        1) Initialize serial ports for DUT and LT
        2) Reset both ports
        3) Generate rx test packet
        4) Start receiveing from DUT
        5) Start transmitting from LT
        6) Start timer
        7) Stop receiving from LT
        8) Stop timer and LT transmission
        9) Get number of packets and calculate packet error rate
        '''
       
        if self.PHY == PHY_Type.LE_2M:
            self.Test_Name="ENH Receive Test"

            self.test_setup()
            
            self.log("\nStart DUT ENH receiver")
            self.serial_tx(self.DUT_Serial, HCI_LE_ENH_RECEIVER_TEST)
            self.serial_rx(self.DUT_Serial, HCI_LE_ENH_RECEIVER_TEST)

            self.log("\nStart Timer")
            self.start_timer()

            self.log("\nStart LT ENH transmission")
            self.serial_tx(self.LT_Serial, HCI_LE_ENH_TRANSMITTER_TEST)
            self.serial_rx(self.LT_Serial, HCI_LE_ENH_TRANSMITTER_TEST)

        else:
            self.Test_Name = "Receive Test"
        
            self.test_setup()
            
            self.log("\nStart DUT receiver")
            self.serial_tx(self.DUT_Serial, HCI_LE_RECEIVER_TEST)
            self.serial_rx(self.DUT_Serial, HCI_LE_RECEIVER_TEST)

            self.log("\nStart Timer")
            self.start_timer()

            self.log("\nStart LT transmission")
            self.serial_tx(self.LT_Serial, HCI_LE_TRANSMITTER_TEST)
            self.serial_rx(self.LT_Serial, HCI_LE_TRANSMITTER_TEST)
        
        # Delay for running time.
        while self.stop_timer() < self.Test_Run_Time:
            pass

        # Get the Percentage Error Rate for RX tests.
        self.RX_PER = self.calculate_PER(self.test_complete("RX"))

if __name__ == '__main__':
    pass