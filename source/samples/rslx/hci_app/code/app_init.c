/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_init.c
 * - HCI application initialization
 * ----------------------------------------------------------------------------
 * $Revision: 1.2 $
 * $Date: 2019/08/27 15:35:04 $
 * ------------------------------------------------------------------------- */

#include "app.h"

/* ----------------------------------------------------------------------------
 * Function      : void App_Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system for proper application execution
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void App_Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all interrupts and clear any pending interrupts. */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    DIO_JTAG_SW_PAD_CFG->JTMS_LPF_ALIAS = JTMS_LPF_DISABLED_BITBAND;
    DIO_JTAG_SW_PAD_CFG->JTCK_LPF_ALIAS = JTCK_LPF_DISABLED_BITBAND;

    /* Reduce DCDC max current to avoid spikes */
    ACS_VCC_CTRL->ICH_TRIM_BYTE  = VCC_ICHTRIM_256MA_BYTE;

    /* Enable VDDRF supply without changing trimming settings */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable or disable buck converter or LDO */
    #if (CFG_BUCK_COV)
    ACS_VCC_CTRL->BUCK_ENABLE_ALIAS = VCC_BUCK_BITBAND;
    #else    /* LDO */
    ACS_VCC_CTRL->BUCK_ENABLE_ALIAS = VCC_LDO_BITBAND;
    #endif    /* CFG_BUCK_COV */

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_6_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to (divided 48 MHz) oscillator clock */
    Sys_Clocks_SystemClkConfig(SYSCLK_CLKSRC_RFCLK);

    /* Configure clock dividers */
    CLK->DIV_CFG0 = (SLOWCLK_PRESCALE_8 | BBCLK_PRESCALE_1 | USRCLK_PRESCALE_1);
    CLK->DIV_CFG2 = ((0x7U << CLK_DIV_CFG2_CPCLK_PRESCALE_Pos) |
                     (0x1U << CLK_DIV_CFG2_DCCLK_PRESCALE_Pos));
    BBIF->CTRL    = (BB_CLK_ENABLE | BBCLK_DIVIDER_8 | BB_WAKEUP);

    /* Initialize UART driver */
    UART_Init(CFG_SYS_CLK, CFG_UART_BAUD_RATE, CFG_DIO_TXD_UART,
              CFG_DIO_RXD_UART);

    /* RTS enable for 4 wire UART simulation*/
    #if (CFG_RTS)
    Sys_DIO_Config(CFG_UART_RTS_DIO, DIO_MODE_GPIO_OUT_0);
    #endif    /* CFG_RTS */

    /* Configure and start timer 0 with a period of 200 ms */
    Sys_Timer_Set_Control(0, TIMER_FREE_RUN | TIMER_PRESCALE_8 | TIMER_200_MS);
    Sys_Timers_Start(SELECT_TIMER0);

    /* Enable timer interrupt */
    NVIC_EnableIRQ(TIMER0_IRQn);

    BLE_Initialize();
	
	/* Set radio output power of RF */
    Sys_RFFE_SetTXPower(CFG_OUTPUT_POWER_DBM);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}
