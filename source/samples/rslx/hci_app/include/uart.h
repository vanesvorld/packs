/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * uart.h
 * - Header for UART wrapper
 * ----------------------------------------------------------------------------
 * $Revision: 1.1 $
 * $Date: 2018/11/19 15:09:46 $
 * ------------------------------------------------------------------------- */

#ifndef UART_H
#define UART_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

#include <rsl10.h>

#include "compiler.h"
#include "arch.h"
#include "reg_assert_mgr.h"
#include "rwip.h"
#include "eif.h"

/* DMA config for TX */
#define DMA_CH0_CONFIG (DMA_LITTLE_ENDIAN           | \
                        DMA_DISABLE                 | \
                        DMA_DISABLE_INT_DISABLE     | \
                        DMA_ERROR_INT_ENABLE        | \
                        DMA_COMPLETE_INT_ENABLE     | \
                        DMA_COUNTER_INT_DISABLE     | \
                        DMA_START_INT_DISABLE       | \
                        DMA_DEST_WORD_SIZE_8        | \
                        DMA_SRC_WORD_SIZE_32        | \
                        DMA_DEST_UART               | \
                        DMA_PRIORITY_0              | \
                        DMA_TRANSFER_M_TO_P         | \
                        DMA_DEST_ADDR_STATIC        | \
                        DMA_SRC_ADDR_INC            | \
                        DMA_ADDR_LIN)

/* DMA config for RX */
#define DMA_CH1_CONFIG (DMA_LITTLE_ENDIAN           | \
                        DMA_ENABLE                  | \
                        DMA_DISABLE_INT_DISABLE     | \
                        DMA_ERROR_INT_ENABLE        | \
                        DMA_COMPLETE_INT_ENABLE     | \
                        DMA_COUNTER_INT_DISABLE     | \
                        DMA_START_INT_DISABLE       | \
                        DMA_DEST_WORD_SIZE_32       | \
                        DMA_SRC_WORD_SIZE_8         | \
                        DMA_SRC_UART                | \
                        DMA_PRIORITY_0              | \
                        DMA_TRANSFER_P_TO_M         | \
                        DMA_DEST_ADDR_INC           | \
                        DMA_SRC_ADDR_STATIC         | \
                        DMA_ADDR_LIN)

/* Size of DMA transfer in bytes */
#define DMA_TRANSFER_LENGTH 7

/* Maximum payload size over DMA */
#define MAX_DATA_SIZE  100

/* DMA channel numbers for TX and RX */
#define TX_DMA_NUM    0
#define RX_DMA_NUM    1

/* Buffers for DMA read and DMA write */
extern uint8_t *bufferRd;
extern uint8_t *bufferWr;

void UART_Init(uint32_t sysClock, uint32_t baudRate, uint8_t rxDIO,
               uint8_t txDIO);

void DMA_Chan_Init(uint32_t dmaChannel, uint32_t dmaConfig, uint32_t dmaLength,
                   uint32_t dmaSrc, uint32_t dmaDest);

uint8_t DMA_Status_Complete(uint32_t dmaChannel);

void DMA0_IRQHandler(void);

void DMA1_IRQHandler(void);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */
#endif    /* UART_H */
