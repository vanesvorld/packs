AES128 - AES-128 Sample Code
============================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project demonstrates an application that:

1.  Uses DIO6 as a toggling output signal. This DIO has been selected so that 
    the LED on the evaluation and development board flashes.
 
2.  Uses DIO5 as an input signal to select between a hardware and a software
    cipher engine (AES128, ECB Mode).
    This DIO has been selected so that the DIO-connected push-button input switch on the evaluation and development board can be used to modify the cipher engine.

Note:
 
 - When LED is off, the software computes the AES-128 decipher (software only)
 - When LED is on, the software computes the AES-128 cipher (hardware or 
   software) - the lowest toggling rate corresponds to the hardware AES-128 
   cipher.

 - You can select a multiplication operation from a lookup table or select
   a standard multiplication through a define (`AES_LOOKUP_TABLE`), located
   in file `aes128.c`. The default is the lookup table.


 - number of cycles by block (128-bit):
 
                     keyExpand              cipher      keyExpand     decipher
    hardware         included in cipher     ~150        N/A           N/A
    software AES128
     (lookup table)  ~1600                  ~3800       ~1600         ~4700
    software AES128
      (computation)  ~1600                  ~9600       ~1600         ~39500  
 
The source code exists in `app.c`, `aesavs.c`, `aesavs.h`, `aes128.h` and `aes128.c`.

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
