/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Simple application showing the AES128 cipher/decipher capability.
 * ----------------------------------------------------------------------------
 * $Revision: 1.12 $
 * $Date: 2019/12/23 20:57:37 $
 * ------------------------------------------------------------------------- */
#include <rsl10.h>
#include <stdlib.h>
#include <aes128.h>
#include <aesavs.h>
#include <printf.h>

/* ----------------------------------------------------------------------------
 * Define declaration
 * ------------------------------------------------------------------------- */
#define CONCAT(x, y)                   x##y
#define DIO_SRC(x)                     CONCAT(DIO_SRC_DIO_, x)

/* DIO number that is connected to push button of EVB */
#define BUTTON_DIO                      5

/* DIO number that is connected to LED of EVB */
#define LED_DIO                         6

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    12

/* ----------------------------------------------------------------------------
 * Global variables declaration
 * ------------------------------------------------------------------------- */
volatile uint8_t is_push_button_pressed;

/* ----------------------------------------------------------------------------
 * Forward declaration
 * ------------------------------------------------------------------------- */
void Initialize(void);

/* ----------------------------------------------------------------------------
 * Interrupt handler declaration
 * ------------------------------------------------------------------------- */
extern void DIO0_IRQHandler(void);

/* ----------------------------------------------------------------------------
 * Function      : void DIO0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Toggle the toggle status global flag.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO0_IRQHandler(void)
{
    static uint8_t ignore_next_dio_int = 0;
    if (ignore_next_dio_int == 1)
    {
        ignore_next_dio_int = 0;
    }
    else if (DIO_DATA->ALIAS[BUTTON_DIO] == 0)
    {
        /* Button is pressed: Ignore next interrupt.
         * This is required to deal with the de-bounce circuit limitations. */
        ignore_next_dio_int    = 1;

        /* Set button pressed flag */
        is_push_button_pressed = 1;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, configuring
 *                 the required DIOs and DIO interrupt,
 *                 updating SystemCoreClockUpdate and enabling interrupts.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Setup DIO5 as a GPIO input with interrupts on transitions, DIO6 as a
     * GPIO output. Use the integrated debounce circuit to ensure that only a
     * single interrupt event occurs for each push of the pushbutton.
     * The debounce circuit always has to be used in combination with the
     * transition mode to deal with the debounce circuit limitations.
     * A debounce filter time of 50 ms is used. */
    Sys_DIO_Config(LED_DIO, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(BUTTON_DIO, DIO_MODE_GPIO_IN_0 | DIO_WEAK_PULL_UP |
                   DIO_LPF_DISABLE);
    Sys_DIO_IntConfig(0,
                      DIO_EVENT_TRANSITION | DIO_SRC(BUTTON_DIO) |
                      DIO_DEBOUNCE_ENABLE,
                      DIO_DEBOUNCE_SLOWCLK_DIV1024, 49);
    NVIC_EnableIRQ(DIO0_IRQn);

    /* Unmask all interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system, then toggle DIO6 as controlled by
 *                 DIO5 (press to toggle input/output).
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    uint8_t is_error     = 0;
    uint8_t is_hw_cipher = 1;

    /*Initialize global variables */
    is_push_button_pressed = 0;

    /* Initialize the system */
    Initialize();
    PRINTF("DEVICE INITIALIZED\n");

    /* Initialize pseudo-random number generator. */
    srand(0x12345678);

    /* Spin loop */
    while (1)
    {
        /* Refresh the watch-dog timer */
        Sys_Watchdog_Refresh();

        /* Initialize internal structure to random values. */
        (void)AES_test(aes128_ecb_init);
        PRINTF("AES128 INITIALIZED\n");

        /* Check if push button pressed an toggle between cipher engine */
        if (is_push_button_pressed == 1)
        {
            /* Toggle state of is_hw_cipher */
            is_hw_cipher = (is_hw_cipher ^ 0x01);
            is_push_button_pressed = 0;
        }

        Sys_GPIO_Set_High(LED_DIO);
        if (is_hw_cipher == 1)
        {
            PRINTF("HW CIPHER COMPUTING\n");
            if (AES_test(aes128_ecb_hw_cipher) != 0)
            {
                PRINTF("HW CIPHER ERROR\n");
                is_error = 1;
            }
        }
        else
        {
            PRINTF("SW CIPHER COMPUTING\n");
            if (AES_test(aes128_ecb_sw_cipher) != 0)
            {
                PRINTF("SW CIPHER ERROR\n");
                is_error = 1;
            }
        }
        Sys_Watchdog_Refresh();
        Sys_GPIO_Set_Low(LED_DIO);

        PRINTF("SW DECIPHER COMPUTING\n");
        if (AES_test(aes128_ecb_sw_decipher) != 0)
        {
            PRINTF("SW DECIPHER ERROR\n");
            is_error = 1;
        }
        Sys_Watchdog_Refresh();

        /* In case of error, break the loop */
        if (is_error == 1)
        {
            break;
        }
    }

    /* Error management */
    while (1)
    {
        Sys_GPIO_Set_High(LED_DIO);
        while (1)
        {
            Sys_Watchdog_Refresh();
        }
    }
}
