/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Simple application using Cortex-M3 timer or timer peripheral
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2018/03/02 17:05:04 $
 * ------------------------------------------------------------------------- */
#include "app.h"

/* ----------------------------------------------------------------------------
 * Function      : void TIMER0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : timer0 interrupt configured in free run mode
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER0_IRQHandler(void)
{
    /*Change timer1 config. only when the current shot-mode is finished */
    while (Sys_Timer_Get_Status(1));
    Sys_Timers_Start(SELECT_TIMER1);
}

/* ----------------------------------------------------------------------------
 * Function      : void TIMER1_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : timer1 interrupt configured in multi-shot mode
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void TIMER1_IRQHandler(void)
{
    Sys_GPIO_Toggle(LED_DIO);
}

/* ----------------------------------------------------------------------------
 * Function      : void DIO0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : define how much Ticks needs to be applied.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DIO0_IRQHandler(void)
{
    static uint8_t timer_config = 2;
    static uint8_t ignore_next_dio_int = 0;
    if (ignore_next_dio_int)
    {
        ignore_next_dio_int = 0;
    }
    else if (DIO_DATA->ALIAS[BUTTON_DIO] == 0)
    {
        /* Button is pressed: Ignore next interrupt.
         * This is required to deal with the debounce circuit limitations. */
        ignore_next_dio_int = 1;

        /* Change timer1 config. only when the current shot-mode is finished */
        while (Sys_Timer_Get_Status(1))
        {
        }

        switch (timer_config)
        {
            case 1:
            {
                Sys_Timer_Set_Control(1, (TIMER_MULTI_COUNT_2 |
                                          TIMER_SHOT_MODE     |
                                          TIMER_SLOWCLK_DIV2  |
                                          TIMER_PRESCALE_64)  | 1000);
            }
            break;

            case 2:
            {
                Sys_Timer_Set_Control(1, (TIMER_MULTI_COUNT_4 |
                                          TIMER_SHOT_MODE     |
                                          TIMER_SLOWCLK_DIV2  |
                                          TIMER_PRESCALE_64)  | 1000);
            }
            break;

            case 3:
            {
                Sys_Timer_Set_Control(1, (TIMER_MULTI_COUNT_6 |
                                          TIMER_SHOT_MODE     |
                                          TIMER_SLOWCLK_DIV2  |
                                          TIMER_PRESCALE_64)  | 1000);
            }
            break;

            default:
            {
                Sys_Timer_Set_Control(1, (TIMER_MULTI_COUNT_8 |
                                          TIMER_SHOT_MODE     |
                                          TIMER_SLOWCLK_DIV2  |
                                          TIMER_PRESCALE_64)  | 1000);
                timer_config = 0;
            }
            break;
        }
        timer_config++;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts and
 *                 configuring the required DIOs.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Setup DIO5 as a GPIO input with interrupts on transitions, DIO6 as a
     * GPIO output. Use the integrated debounce circuit to ensure that only a
     * single interrupt event occurs for each push of the pushbutton.
     * The debounce circuit always has to be used in combination with the
     * transition mode to deal with the debounce circuit limitations.
     * A debounce filter time of 50 ms is used. */
    Sys_DIO_Config(LED_DIO, DIO_MODE_GPIO_OUT_0);
    Sys_DIO_Config(BUTTON_DIO, DIO_MODE_GPIO_IN_0 | DIO_WEAK_PULL_UP |
                   DIO_LPF_DISABLE);
    Sys_DIO_IntConfig(0,
                      DIO_EVENT_TRANSITION | DIO_SRC(BUTTON_DIO) |
                      DIO_DEBOUNCE_ENABLE,
                      DIO_DEBOUNCE_SLOWCLK_DIV1024, 49);

    /* Configure timer[0] in free run mode.
     * - Force multi-count to 8 in order to show that field is not active */

    Sys_Timer_Set_Control(0, (TIMER_MULTI_COUNT_8 |
                              TIMER_FREE_RUN      |
                              TIMER_SLOWCLK_DIV32 |
                              TIMER_PRESCALE_64)  | 1000);

    /* Configure timer[1] in multi-shot mode */
    Sys_Timer_Set_Control(1, (TIMER_MULTI_COUNT_2 |
                              TIMER_SHOT_MODE     |
                              TIMER_SLOWCLK_DIV2  |
                              TIMER_PRESCALE_64)  | 1000);

    /* set Priority for DIO0 Interrupt
     * Service DIO0 irq will be made only when others finished.
     * As we wait on the multi-shot to finish, just allow interrupts
     * with higher priority be serviced */
    NVIC_SetPriority(DIO0_IRQn, (1 << __NVIC_PRIO_BITS) - 1);

    NVIC_EnableIRQ(TIMER0_IRQn);
    NVIC_EnableIRQ(TIMER1_IRQn);
    NVIC_EnableIRQ(DIO0_IRQn);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system, then toggle DIO6 as controlled by
 *                 DIO5 (press to modify the blink frequency).
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    /* Initialize the system */
    Initialize();

    /* Give one train pulse at beginning */
    Sys_Timers_Start(SELECT_TIMER1);

    /* Launch the free run timer */
    Sys_Timers_Start(SELECT_TIMER0);

    /* Spin loop */
    while (1)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* Core goes into sleep mode and waits on an interrupt */
        __WFI();
    }
}
