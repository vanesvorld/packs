/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 *  - Simple example on how to use RSL10 SAI CMSIS-Driver
 * ------------------------------------------------------------------------- */
#include <app.h>
#include <printf.h>

/* Global variables */
ARM_DRIVER_SAI *sai;
DRIVER_GPIO_t *gpio;

uint8_t sai_tx_buffer[] __attribute__ ((aligned(4))) = "RSL10 SAI TEST";
uint8_t sai_rx_buffer[sizeof(sai_tx_buffer)] __attribute__ ((aligned(4)));
size_t sai_buff_size = sizeof(sai_tx_buffer);

/* ----------------------------------------------------------------------------
 * Function      : void Button_EventCallback(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : Start PCM transfer on button press.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Button_EventCallback(uint32_t event)
{
    PRINTF("PRESS BUTTON: START TRANSMISSION\n");
	/* Abort current receive operation */
    sai->Control(ARM_SAI_ABORT_RECEIVE, 0, 0);

    /* Prepare master configuration */
    uint32_t cfgMaster = (SAI_CFG_DEFAULT & ~ARM_SAI_MODE_Msk) | ARM_SAI_MODE_MASTER;

    /* Reconfigure to master mode */
    sai->Control(cfgMaster, ARM_SAI_SLOT_SIZE_DEFAULT, RTE_SAI_MCLK_PRESCALE_DEFAULT);

    /* Refresh the watchdog */
    Sys_Watchdog_Refresh();

    /* Start transmission */
    sai->Send(sai_tx_buffer, sai_buff_size);
}

/* ----------------------------------------------------------------------------
 * Function      : void PCM_EventCallBack(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a callback registered by the function
 *                 Initialize. The parameter event indicates one or
 *                 more events that occurred during driver operation. Each
 *                 event is encoded in a separate bit and therefore it is
 *                 possible to signal multiple events within the same call.
 * Inputs        : event    sai events notification mask
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SAI_EventCallBack(uint32_t event)
{
    /* Check if receiving complete event was indicated */
    if (event & ARM_SAI_EVENT_RECEIVE_COMPLETE)
    {
        /* If string received matches sai_tx_buffer */
        if (!strcmp((const char *)sai_tx_buffer, (const char *)sai_rx_buffer))
        {
            /* Toggle LED state 2 times for 500 milliseconds */
            ToggleLed(2, 500);
            PRINTF("LED BLINKED: CORRECT_DATA_RECEIVED\n");
        }
        else
        {
            /* Toggle LED state 10 times for 100 milliseconds to indicate error */
            ToggleLed(10, 100);
            PRINTF("TRANSMISSION ERROR\n");
        }

        /* Start to receive */
        sai->Receive(sai_rx_buffer, sai_buff_size);
    }

    /* Check if transmission complete event was indicated */
    if (event & ARM_SAI_EVENT_SEND_COMPLETE)
    {
        /* Toggle LED state 2 times for 500 milliseconds */
        ToggleLed(2, 500);
        PRINTF("LED BLINKED: TRANSMISSION COMPLETED\n");

        /* Reconfigure to slave mode */
        sai->Control(SAI_CFG_DEFAULT, ARM_SAI_SLOT_SIZE_DEFAULT, RTE_SAI_MCLK_PRESCALE_DEFAULT);

        /* Start to receive */
        sai->Receive(sai_rx_buffer, sai_buff_size);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void ToggleLed(uint32_t n, uint32_t delay_ms)
 * ----------------------------------------------------------------------------
 * Description   : Toggle the led pin.
 * Inputs        : n        - number of toggles
 *                  : delay_ms - delay between each toggle [ms]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void ToggleLed(uint32_t n, uint32_t delay_ms)
{
    for (; n > 0; n--)
    {
        /* Refresh the watchdog */
        Sys_Watchdog_Refresh();

        /* Toggle led diode */
        gpio->ToggleValue(LED_DIO);

        /* Delay */
        Sys_Delay_ProgramROM((delay_ms / 1000.0) * SystemCoreClock);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, switching to
 *                 the 48 MHz clock.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Prepare the 48 MHz crystal
     * Start and configure VDDRF */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_1_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to 48 MHz oscillator clock */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Initialize gpio structure */
    gpio = &Driver_GPIO;

    /* Initialize gpio driver */
    gpio->Initialize(Button_EventCallback);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    /* Set watchdog timeout */
    Sys_Watchdog_Set_Timeout(WATCHDOG_TIMEOUT_4194M3);

    /* Initialize sai driver structure */
    sai = &Driver_SAI;

    /* Initialize sai, register callback function */
    sai->Initialize(SAI_EventCallBack);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the PCM CMSIS-Driver. Send simply text string
 *                 on button press.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    int i;

    /* Initialize the system */
    Initialize();
    PRINTF("DEVICE INITIALIZED\n");

    /* Start receiving data */
    sai->Receive(sai_rx_buffer, sai_buff_size);

    while (true)
    {
        /* Here we demonstrate the ABORT_TRANSFER feature. Wait for first few bytes
         * to be transferred and if particular byte does not match abort the transfer. */
        i = sai->GetRxCount() - BUFFER_OFFSET;
        if ((i >= 0) && (sai_rx_buffer[i] != sai_tx_buffer[i]))
        {
            /* Abort current receive operation */
            sai->Control(ARM_SAI_ABORT_RECEIVE, 0, 0);

            /* Re-start receive operation */
            sai->Receive(sai_rx_buffer, sai_buff_size);
        }

        /* Refresh the watchdog */
        Sys_Watchdog_Refresh();
    }
}
