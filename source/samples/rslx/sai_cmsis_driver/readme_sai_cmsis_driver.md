SAI CMSIS-Driver Sample Code
============================

NOTE: If you use this sample application for your own purposes, follow the
      licensing agreement specified in `Software_Use_Agreement.rtf` in 
      the home directory of the installed Software Development Kit (SDK).

Overview
--------

This sample project demonstrates how to use the SAI CMSIS-Driver 
to transfer data in both master and slave modes between two RSL10 Evaluation 
and Development Boards. 

To use this sample application, the `ARM.CMSIS` pack must be installed in your 
IDE. In **CMSIS Pack Manager**, on the right panel, you can see the **Packs 
and Examples** view. In the **Packs** view, you will see **CMSIS packs**. Find 
`ARM.CMSIS` and click on the **Install** button.

Additional information about the application:

1) Upon starting, the application configures the driver to wait for the 
   reception of data in slave mode by calling the receive function - if the 
   board is configured to slave.

2) When the DIO5 button on one of the Evaluation and Development Boards is 
   pressed, the application starts transmitting as the master.

3) The application transfers the string message "RSL10 SAI TEST".

4) Upon a successful transfer, the SAI CMSIS-Driver sends an 
   `ARM_SAI_EVENT_SEND_COMPLETE` to the application if it was configured 
   as master or `ARM_SAI_EVENT_RECEIVE_COMPLETE` if it was configured as 
   receive, using a callback function. The event is generated on both master 
   and slave sides.

5) When receiving `ARM_SAI_EVENT_RECEIVE_COMPLETE`, the application 
   does the following:
   
   - On the slave side, it blinks the LED (DIO6) for 0.5 seconds, to indicate 
     that the received message matches the expected string: "RSL10 SAI TEST".

   - Also on the slave side, if it has finished a `SlaveReceive`, it now 
     demonstrates a `SlaveTransmit` operation.
    
Hardware Requirements
---------------------
Two RSL10 Evaluation and Development Board are required. Connect them as
follows:

        Board #1             Board #2
    PCM_CLK  (SCL)   ->    PCM_CLK  (SCL)
    PCM_FRAME(SDA)   ->    PCM_FRAME(SDA)
    PCM_SERI (DIO1)  ->    PCM_SERO (DIO0)
    PCM_SERO (DIO0)  ->    PCM_SERI (DIO1)

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for your IDE for more information.
  
Verification
------------
To verify that the application is working correctly, load the sample code on
both Evaluation and Development Boards and connect them as detailed in the 
*Hardware Requirements* section. Press the push button on one of the boards 
(DIO5). The LED (DIO6) blinks on the second board, confirming that both boards
have transmitted the string message correctly.
 
The source code exists in `app.c` and `app.h`. The driver configuration is 
specified in `RTE_Device.h`.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1)  Connect DIO12 to ground.

2)  Press the RESET button (this restarts the application, which
    pauses at the start of its initialization routine).

3)  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can now work
    properly.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
