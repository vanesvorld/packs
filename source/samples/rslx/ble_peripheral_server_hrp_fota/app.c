/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2018
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Main application file
 * ----------------------------------------------------------------------------
 * $Revision: 1.2 $
 * $Date: 2019/03/15 21:19:14 $
 * ------------------------------------------------------------------------- */

#include <app.h>
#if defined(CFG_FOTA)
#include <ble_dfus.h>
#include "sys_boot.h"
#include "sys_fota.h"
#endif /*defined(CFG_FOTA)*/

/* ----------------------------------------------------------------------------
 * Application Version
 * ------------------------------------------------------------------------- */
#if defined(CFG_FOTA)
    SYS_FOTA_VERSION(VER_ID, VER_MAJOR, VER_MINOR, VER_REVISION);
#endif /*defined(CFG_FOTA)*/

extern const struct DISS_DeviceInfo_t deviceInfo;

/* ----------------------------------------------------------------------------
 * Function      : void BASS_Setup(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure the Battery Service Server
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
static void BASS_Setup(void)
{
    BASS_Initialize(APP_BAS_NB, APP_BASS_ReadBatteryLevel);
    BASS_NotifyOnBattLevelChange(TIMER_SETTING_S(1));     /* Periodically monitor the battery level. Only notify changes */
    BASS_NotifyOnTimeout(TIMER_SETTING_S(6));             /* Periodically notify the battery level to connected peers */
    APP_BASS_SetBatMonAlarm(BATMON_SUPPLY_THRESHOLD_CFG); /* BATMON alarm configuration */
}

/* ----------------------------------------------------------------------------
 * Function      : void DISS_Setup(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure the Device Information Service Server
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
/**
 * @brief Configure the Device Information Service Server
 */
static void DISS_Setup(void)
{
    DISS_Initialize(APP_DIS_FEATURES, (const struct DISS_DeviceInfo_t*) &deviceInfo);
}

/* ----------------------------------------------------------------------------
 * Function      : void HRPS_Setup(void)
 * ----------------------------------------------------------------------------
 * Description   : Configure the Heart Rate Service Server
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
/**
 * @brief Configure the Heart Rate Service Server
 */
static void HRPS_Setup(void)
{
    HRPS_Initialize(TIMER_SETTING_S(1),
            HRPS_BODY_SENSOR_LOC_CHAR_SUP | HRPS_ENGY_EXP_FEAT_SUP | HRPS_HR_MEAS_NTF_CFG,
            HRS_LOC_CHEST,
            &app_adv_info,
            APP_HRPS_HeartRateMeasurementUpdate,
            APP_HRPS_EnergyExpResetInd);
    APP_HRPS_Initialize();
}

int main(void)
{
    /* Configure hardware and initialize BLE stack */
    Device_Initialize();

    /* Debug/trace initialization. In order to enable UART or RTT trace,
     * configure the 'RSL10_DEBUG' macro in app_trace.h */
    TRACE_INIT();
    PRINTF("\n\rble_peripheral_server_bond has started %s!\r\n", __TIME__);

    /* Configure application-specific advertising data and scan response data.
     * The advertisement period will change after 30 s as per 5.1.1 */
    APP_SetAdvScanData();

    /* Initialize the default device name characteristic data */
    APP_InitDevInfo();

    /* Configure Battery Service Server */
    BASS_Setup();

    /* Configure Device Information Service Server */
    DISS_Setup();

    /* Configure Heart Rate Service Server */
    HRPS_Setup();

    /* Configure DFU Server */
#ifdef CFG_FOTA
    DFUS_Initialize();
#endif

    /* Add application message handlers */
    MsgHandler_Add(TASK_ID_GAPM, APP_GAPM_GATTM_Handler);
    MsgHandler_Add(GATTM_ADD_SVC_RSP, APP_GAPM_GATTM_Handler);
    MsgHandler_Add(TASK_ID_GAPC, APP_GAPC_Handler);
    MsgHandler_Add(APP_LED_TIMEOUT, APP_LED_Timeout_Handler);
    MsgHandler_Add(APP_BATT_LEVEL_LOW, APP_BASS_BattLevelLow_Handler);
    MsgHandler_Add(APP_TIMEOUT_WHITELIST, APP_WhitelistTimerHandler);

    /* Reset the GAP manager. Trigger GAPM_CMP_EVT / GAPM_RESET when finished. See APP_GAPM_GATTM_Handler */
    GAPM_ResetCmd();

    while (1)
    {
        Kernel_Schedule();    /* Dispatch all events in Kernel queue */

        /* Start Update when button is pressed */
#if defined(CFG_FOTA)
        if (DIO_DATA->ALIAS[BUTTON_DIO] == 0)
        {
        #if defined(CFG_BOOTLOADER)
        	Sys_Boot_StartUpdater();
        #else
        	Sys_Fota_StartDfu(1);
        #endif
        }
#endif /*defined(CFG_FOTA)*/

        Sys_Watchdog_Refresh();
        SYS_WAIT_FOR_EVENT;    /* Wait for an event before re-executing the scheduler */
    }
}
