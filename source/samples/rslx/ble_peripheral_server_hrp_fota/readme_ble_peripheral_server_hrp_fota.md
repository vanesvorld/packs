Heart Rate Peripheral FOTA Sample Code
======================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This description is for Firmware Over The Air (FOTA). If you want to know 
about the Heart Rate Service, refer to the `readme_ble_peripheral_server_hrp.txt`
in the `ble_peripheral_server_hrp` sample application.

This sample project demonstrates a simple application that provides FOTA to 
update the firmware of RSL10 over Bluetooth Low Energy. This code is provided 
in a form to work with the RSL10 Evaluation and Development Boards, but can 
also be used with the RSL10 Dongle on the PC side to search and connect to an 
RSL10 device over Bluetooth Low Energy.

The `ble_peripheral_server_hrp_fota.fota` is created after this 
application has been successfully built. This FOTA image consists of two 
sub-images which are the FOTA stack sub-image and the Application sub-image. 
The Device Firmware Update (DFU) component, which is embedded in the FOTA 
stack, acts as DFU service server. This component performs the actual update 
via the DFU custom service.

The DFU component is activated automatically at device startup if no valid 
application was found on the device. Or the application can start it 
by calling the function `Sys_Fota_StartDfu()`.

Update Sequence
---------------
The DFU client scans for a device advertising the configured service ID 
(normally the DFU service ID). When found, it connects to the device and reads 
its characteristics (Device ID, Versions, Build ID).

The client must only accept FOTA images with matching Device IDs (unless the 
Device ID characteristic is all 0s; then the device is compatible with all FOTA 
images).

If the Build ID of the application sub-image differs from the Build ID 
characteristic, the client has to download the FOTA stack sub-image first. 
After successfully downloading, the client must disconnect from the device 
which restarts with the new FOTA stack. Now the client must reconnect to the 
same device.

If the Build ID of the application sub-image matches the Build ID 
characteristic, the client has to download the Application sub-image. After 
successfully downloading, the client must disconnect from the device which 
restarts with the new application.

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board
and an RSL10 Dongle.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
Getting Started Guide for your IDE for more information.

Application-specific files
--------------------------

    ble_bfus.c        - Bluetooth DFU server service source file

    ble_dfus.h        - Bluetooth DFU server service header

__lib\Release__
     
    fota.bin          - FOTA stack sub-image containing 
                        the Bluetooth Low Energy stack and the DFU component

Python Script
-------------
__Installation__

    Installed Python, version >=2.7 or >=3.4
    Installed module pyserial, version >=3.2
    Installed module ecdsa, version >=0.13
__updater.py__

Updater.py is the Python UART updater tool. You can load the FOTA image, which 
is `ble_peripheral_server_hrp.fota` using the `updater.py` Python 
script over UART. The script is provided in the `scripts` folder of the 
`bootloader` sample application, together with additional dependencies. 
You also need the `pyserial` Python module installed. For a detailed 
description of `updater.py` refer to the `readme_bootloader.md`.
 
__mkfotaimg.py__
 
To build the FOTA Image use the tool `mkfotaimg.py`. The `mkfotaimg.py` 
needs the Python package `ecdsa` for image signing, you can install this 
package via the Python package installer `pip install ecdsa`. This script 
can be included in the `Post-build steps` of Eclipse to generate the 
`.fota` format file. Go to Settings on the Properties tab and add the build 
command in the `Post-build steps`:
Navigate to `Project > Settings > C/C++ Build > Settings >`
`Build Steps > Post-build steps`.

- Usage

    mkfotaimg.py [-h] [--version] [-d UUID] [-s KEY-PEM] 
                 [-i UUID][-n NAME] [-o OUT-IMG]
                 FOTA-IMG APP-IMG

- positional arguments

    FOTA-IMG   FOTA stack sub-image containing the BLE stack 
               and the DFU component (.bin file)
    APP-IMG    application sub-image (.bin file)

- optional arguments

    -h, --help   show this help message and exit
    --version    show program's version number and exit
    -d UUID, --devid UUID
                 device UUID to embed in the image (default: no ID)
    -s KEY-PEM, --sign KEY-PEM
                 name of signing key PEM file (default: no signing)
    -i UUID, --srvid UUID
                 advertised UUID to embed in the image 
                 (default: DFU service UUID)
    -n NAME, --name NAME  
                 advertised name to embed in the image 
                 (default: 'ON FOTA RSL10')
    -o OUT-IMG   name of output image file (default: <APP-IMG>.fota)


Utility
-------
__Fota.Console__

FOTA can be tested with the `Fota.Console` tool. This tool runs on the PC 
to download firmware images over Bluetooth Low Energy. It means that an RSL10 
Dongle on the PC side is required to establish a connection to the RSL10 
Evaluation and Development Board over Bluetooth Low Energy.
    
- Requirements

    .Net Framework v4.6
    SiliconLabs VCP driver version >= v6.7.3

- Usage

    Fota.Console.exe /COM=COMx /IN=<APP-IMG>.fota [/NAME=<device name>][/ADDR=<aa:bb:cc:dd:ee:ff>]
    /COM: COM port that the RSL10 Dongle is connected to (e.g. `COM1`)
    /IN: The path to a file that will be sent over Bluetooth Low Energy.
    /NAME: The name of the peripheral to connect.
    /ADDR: The Bluetooth Low Energy address of the peripheral to connect. e.g. 60:C0:BF:00:14:62    

Verification
------------
To verify the operation of FOTA using an RSL10 Evaluation Board and the 
`ble_peripheral_server_hrp_fota` sample application, 
follow these steps: 
1. Obtain an RSL10 Evaluation and Development Board, and an RSL10 Dongle.
2. Build the `ble_peripheral_server_hrp_fota` application. After 
   building successfully, `ble_peripheral_server_hrp_fota.fota` is 
   generated.
3. If you are testing the FOTA function for the first time, your device is 
   loaded with an application that does not have the FOTA functionality.
   
   Load the `bootloader.hex` in `bootloader` into the RSL10 Evaluation
   and Development Board by `FlashLoader`. After reset, the LED of the RSL10 
   Evaluation and Development Board continuously on, indicating an active 
   Bootloader waiting for a FW image. Use the following command:
     
    updater.py COMxx ble_peripheral_server_hrp_fota.fota
4. You can see `Peripheral_HRP_FOTA` on the RSL10 Bluetooth Low Energy 
   Explorer. Your device is now ready to perform a FOTA update, as it contains 
   all the required components: the bootloader and the Fota BLE Stack.
5. The Fota.Console tool is available in the RSL10 Utility Apps zip package. 
   Make sure to extract this package and use a command prompt tool to enter 
   the Fota.Console folder.
6. Run `Fota.Console.exe`tool to start a FOTA update using the following 
   command: 

    Fota.Console /COM=COMxx /IN=ble_peripheral_server_hrp_fota.fota /NAME=Peripheral_HRP_FOTA
   Make sure to copy the .fota image file in the same folder as the Fota.Console 
   tool or specify the full path to this file in the command above.
7. The Fota.Console tool will search, connect and transmit the firmware image 
   to the RSL10 Evaluation and Development Board.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which will
    pause at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application will work
    properly.

***********************************************************
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
