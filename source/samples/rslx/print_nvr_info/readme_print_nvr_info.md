Print NVR Information Using Semi-Hosting 
========================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project prints the contents of the NVR sectors via semi-hosting.
The program runs from PRAM and executing it will not change the flash 
contents.

The source code exists in `main.c`, with no defined header files.

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board 
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
*Getting Started Guide* for more information.

Verification
------------
To verify if this application is functioning correctly, start a debug session
using the following debug configuration:

In the Debugger tab:

- Check **Allocate console for semihosting and SWO**

In the Startup tab:

- Check **Initial Reset and Halt**
- Check **Enable Semihosting**
- Check **GDB Client** in **Console routed to:**
- Check **RAM application (reload after each reset/restart)**
- Uncheck **Pre-run/Restart reset**
- Check **Set breakpoint at: main**
- Check **continue**

Run the program and observe the semihosting output.

***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
