/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * main.c
 * This sample project prints the contents of the NVR sectors via semi-hosting.
 * The program runs from PRAM and executing it will not change flash memory.
 * ----------------------------------------------------------------------------
 * $Revision: 1.19 $
 * $Date: 2018/02/27 15:42:17 $
 * ------------------------------------------------------------------------- */
#include <rsl10.h>
#include <stdio.h>

#define CONCAT(x, y)                    x##y
#define DIO_SRC(x)                      CONCAT(DIO_SRC_DIO_, x)

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    12

/* Bonding list */
#define BD_TYPE_PUBLIC                  0
#define BD_TYPE_PRIVATE                 1

/* Function declarations */
void Initialize(void);

/* Used for semi-hosting */
extern void initialise_monitor_handles(void);

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system, then print NVR information using
 *                 semi-hosting
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    uint32_t tmp;
    unsigned int found;

    /* Initialize the system */
    Initialize();

    /* Initialize semi-hosting */
    initialise_monitor_handles();
    printf("Semi-hosting initialized\n");

    /* Print Chip ID information */
    printf("\n");
    tmp = AHBREGS->CHIP_ID_NUM;
    printf("Chip ID: 0x%04X\n", (tmp >> 8) & 0xFFFF);
    printf("Chip Family: %u\n", tmp >> 24);
    printf("Chip Version: %u\n", (tmp >> 16) & 0xFF);
    printf("Chip Major Revision: %u\n", (tmp >> 8) & 0xFF);
    printf("Chip Minor Revision: %u\n", (tmp >> 3) & 0x1F);
    printf("Enabled chip features:");
    found = 0;
    if ((tmp & CHIP_ID_AOBLE_PRESENT) == CHIP_ID_AOBLE_PRESENT)
    {
        printf(" AOBLE");
        found = 1;
    }
    if ((tmp & CHIP_ID_LPDSP32_PRESENT) == CHIP_ID_LPDSP32_PRESENT)
    {
        printf(" LPDSP32");
        found = 1;
    }
    if ((tmp & CHIP_ID_OD_PRESENT) == CHIP_ID_OD_PRESENT)
    {
        printf(" OD");
        found = 1;
    }
    if (!found)
    {
        printf(" (none)");
    }
    printf("\n");

    /* Print NVR1 information */
    printf("\n");
    printf("NVR1 contents:\n");
    printf("SYS_INFO_START_ADDR: ");
    if (*(uint32_t *)SYS_INFO_START_ADDR != 0xFFFFFFFF)
    {
        printf("0x%08X\n", *(uint32_t *)SYS_INFO_START_ADDR);
    }
    else
    {
        printf("(none)");
    }
    printf("\n");
    printf("SYS_INFO_START_MEM_CFG: ");
    if (*(uint32_t *)SYS_INFO_START_MEM_CFG != 0xFFFFFFFF)
    {
        printf("0x%08X\n", *(uint32_t *)SYS_INFO_START_MEM_CFG);
    }
    else
    {
        printf("(none)");
    }
    printf("\n");

    /* Print NVR2 information */
    printf("\n");
    printf("NVR2 contents:\n");
    printf("Bluetooth bonding list:\n");
    found = 0;
    for (unsigned int e = 0; e < SIZEOF_BONDLIST; e++)
    {
        tmp = ((BondInfo_Type *)BOND_INFO_BASE)[e].STATE;
        if (tmp != BOND_INFO_STATE_EMPTY && tmp != BOND_INFO_STATE_INVALID)
        {
            printf("  Entry %u\n", e);
            printf("    STATE: 0x%02X\n", tmp);
            printf("    LTK (Long Term Key): 0x");
            for (signed int i = 15; i >= 0; i--)
            {
                printf("%02X", ((BondInfo_Type *)BOND_INFO_BASE)[e].LTK[i]);
            }
            printf("\n");
            printf("    EDIV (Encrypted Diversifier): 0x%04X\n",
                   ((BondInfo_Type *)BOND_INFO_BASE)[e].EDIV);
            printf("    Address: 0x");
            for (signed int i = 6; i >= 0; i--)
            {
                printf("%02X", ((BondInfo_Type *)BOND_INFO_BASE)[e].ADDR[i]);
            }
            tmp = ((BondInfo_Type *)BOND_INFO_BASE)[e].ADDR_TYPE;
            printf("    ADDR_TYPE: 0x%02X ", tmp);
            if (tmp == BD_TYPE_PUBLIC)
            {
                printf("(Public)\n");
            }
            else if (tmp == BD_TYPE_PRIVATE)
            {
                printf("(Private)\n");
            }
            else
            {
                printf("(Unknown)\n");
            }
            printf("    CSRK (Connection Signature Resolving Key): 0x");
            for (signed int i = 15; i >= 0; i--)
            {
                printf("%02X", ((BondInfo_Type *)BOND_INFO_BASE)[e].CSRK[i]);
            }
            printf("\n");
            printf("    IRK (Identity Resolving Key): 0x");
            for (signed int i = 15; i >= 0; i--)
            {
                printf("%02X", ((BondInfo_Type *)BOND_INFO_BASE)[e].IRK[i]);
            }
            printf("\n");
            printf("    RAND (Random Key): 0x");
            for (signed int i = 7; i >= 0; i--)
            {
                printf("%02X", ((BondInfo_Type *)BOND_INFO_BASE)[e].RAND[i]);
            }
            printf("\n");
            found = 1;
        }
    }
    if (!found)
    {
        printf("  (none)\n");
    }

    /* Print NVR3 information */
    printf("\n");
    printf("NVR3 contents:\n");
    printf("Bluetooth address: ");
    found = 0;
    for (signed int i = 2; i >= 0; i--)
    {
        if (*(uint16_t *)(DEVICE_INFO_BLUETOOTH_ADDR + 2 * i) != 0xFFFF)
        {
            found = 1;
        }
    }
    if (found)
    {
        printf("0x");
        for (signed int i = 2; i >= 0; i--)
        {
            printf("%04X", *(uint16_t *)(DEVICE_INFO_BLUETOOTH_ADDR + 2 * i));
        }
    }
    else
    {
        printf("(none)");
    }
    printf("\n");
    printf("Bluetooth IRK (Identity Resolving Key): 0x");
    for (signed int i = 3; i >= 0; i--)
    {
        printf("%08X", *(uint32_t *)(DEVICE_INFO_BLUETOOTH_IRK + 4 * i));
    }
    printf("\n");
    printf("Bluetooth CSRK (Connection Signature Resolving Key): 0x");
    for (signed int i = 3; i >= 0; i--)
    {
        printf("%08X", *(uint32_t *)(DEVICE_INFO_BLUETOOTH_CSRK + 4 * i));
    }
    printf("\n");
    tmp = *(uint32_t *)LOCK_INFO_SETTING;
    printf("Debug port lock setting: 0x%08X ", tmp);
    if (tmp == DBG_ACCESS_LOCK)
    {
        printf("(Locked)\n");
    }
    else
    {
        printf("(Unlocked)\n");
        if (tmp == 0x00000000 || tmp == 0xFFFFFFFF)
        {
            printf(
                "WARNING: The device may not boot at VBAT < 1.25 V. LOCK_INFO_SETTING should be updated to not be all zeros or all ones for use in a real application.\n");
        }
    }
    printf("Unlock key: 0x");
    for (signed int i = 3; i >= 0; i--)
    {
        printf("%08X", *(uint32_t *)(LOCK_INFO_KEY + 4 * i));
    }
    printf("\n");
    tmp = *(uint16_t *)MANU_INFO_LENGTH;
    printf("MANU_INFO_INIT length: 0x%04X\n", tmp);
    printf("MANU_INFO_INIT version: %u.%u.%u\n",
           *(uint16_t *)MANU_INFO_FUNCTION_VERSION >> 12,
           (*(uint16_t *)MANU_INFO_FUNCTION_VERSION >> 8) & 0xF,
           *(uint16_t *)MANU_INFO_FUNCTION_VERSION & 0xFF);
    printf("MANU_INFO_INIT CRC: 0x%04X\n", *(uint16_t *)(MANU_INFO_INIT + tmp +
                                                         2));
    printf("Bluetooth ECDH private key: 0x");
    for (signed int i = 7; i >= 0; i--)
    {
        printf("%08X", *(uint32_t *)(DEVICE_INFO_ECDH_PRIVATE + 4 * i));
    }
    printf("\n");
    printf("Bluetooth ECDH public X key: 0x");
    for (signed int i = 7; i >= 0; i--)
    {
        printf("%08X", *(uint32_t *)(DEVICE_INFO_ECDH_PUBLIC_X + 4 * i));
    }
    printf("\n");
    printf("Bluetooth ECDH public Y key: 0x");
    for (signed int i = 7; i >= 0; i--)
    {
        printf("%08X", *(uint32_t *)(DEVICE_INFO_ECDH_PUBLIC_Y + 4 * i));
    }
    printf("\n");

    /* Print NVR4 information */
    printf("\n");
    printf("NVR4 contents:\n");
    printf("Calibration values:\n");
    found = 0;
    Sys_ReadNVR4(MANU_INFO_BANDGAP, 1, (unsigned int *)&tmp);
    if (((tmp >> 24) == 0) && ((tmp >> 16) > 0))
    {
        printf("  Bandgap slope: 0x%02X\n", (tmp >> 8) & 0xFF);
        printf("  Bandgap %u mV: 0x%02X\n", (tmp >> 16) * 10, tmp & 0xFF);
        found = 1;
    }
    for (unsigned int i = 0; i < 4; i++)
    {
        Sys_ReadNVR4(MANU_INFO_VDDRF + i * 4, 1, (unsigned int *)&tmp);
        if (((tmp >> 24) == 0) && ((tmp >> 16) > 0))
        {
            printf("  VDDRF %u mV: 0x%02X\n", (tmp >> 16) * 10, tmp & 0xFFFF);
            found = 1;
        }
    }
    for (unsigned int i = 0; i < 4; i++)
    {
        Sys_ReadNVR4(MANU_INFO_VDDPA + i * 4, 1, (unsigned int *)&tmp);
        if (((tmp >> 24) == 0) && ((tmp >> 16) > 0))
        {
            printf("  VDDPA %u mV: 0x%02X\n", (tmp >> 16) * 10, tmp & 0xFFFF);
            found = 1;
        }
    }
    for (unsigned int i = 0; i < 4; i++)
    {
        Sys_ReadNVR4(MANU_INFO_VDDC + i * 4, 1, (unsigned int *)&tmp);
        if (((tmp >> 24) == 0) && ((tmp >> 16) > 0))
        {
            printf("  VDDC %u mV: 0x%02X\n", (tmp >> 16) * 10, tmp & 0xFFFF);
            found = 1;
        }
    }
    for (unsigned int i = 0; i < 4; i++)
    {
        Sys_ReadNVR4(MANU_INFO_VDDM + i * 4, 1, (unsigned int *)&tmp);
        if (((tmp >> 24) == 0) && ((tmp >> 16) > 0))
        {
            printf("  VDDM %u mV: 0x%02X\n", (tmp >> 16) * 10, tmp & 0xFFFF);
            found = 1;
        }
    }
    for (unsigned int i = 0; i < 4; i++)
    {
        Sys_ReadNVR4(MANU_INFO_DCDC + i * 4, 1, (unsigned int *)&tmp);
        if (((tmp >> 24) == 0) && ((tmp >> 16) > 0))
        {
            printf("  VCC %u mV: 0x%02X\n", (tmp >> 16) * 10, tmp & 0xFFFF);
            found = 1;
        }
    }
    for (unsigned int i = 0; i < 4; i++)
    {
        Sys_ReadNVR4(MANU_INFO_OSC_32K + i * 4, 1, (unsigned int *)&tmp);
        if (((tmp >> 16) < 0xFFFF) && ((tmp >> 16) > 0))
        {
            printf("  RC-32 %u Hz: 0x%02X\n", tmp >> 16, tmp & 0xFFFF);
            found = 1;
        }
    }
    for (unsigned int i = 0; i < 4; i++)
    {
        Sys_ReadNVR4(MANU_INFO_OSC_RC + i * 4, 1, (unsigned int *)&tmp);
        if (((tmp >> 16) < 0xFFFF) && ((tmp >> 16) > 0))
        {
            printf("  RC %u kHz: 0x%02X\n", tmp >> 16, tmp & 0xFFFF);
            found = 1;
        }
    }
    for (unsigned int i = 0; i < 4; i++)
    {
        Sys_ReadNVR4(MANU_INFO_OSC_RC_MULT + i * 4, 1, (unsigned int *)&tmp);
        if (((tmp >> 16) < 0xFFFF) && ((tmp >> 16) > 0))
        {
            printf("  RC-MULT %u kHz: 0x%02X\n", tmp >> 16, tmp & 0xFFFF);
            found = 1;
        }
    }
    if (!found)
    {
        printf("(none)\n");
    }
    printf("  Version: ");
    Sys_ReadNVR4(MANU_INFO_VERSION, 1, (unsigned int *)&tmp);
    if (tmp != 0xFFFFFFFF)
    {
        printf("0x%X", tmp);
    }
    else
    {
        printf("(none)");
    }
    printf("\n");
    Sys_ReadNVR4(MANU_INFO_CRC, 1, (unsigned int *)&tmp);
    printf("  CRC: 0x%08X\n", tmp);
    printf("Bluetooth address: ");
    found = 0;
    Sys_ReadNVR4(MANU_INFO_BLUETOOTH_ADDR + 4, 1, (unsigned int *)&tmp);
    if ((tmp & 0xFFFF) != 0xFFFF)
    {
        found = 1;
    }
    Sys_ReadNVR4(MANU_INFO_BLUETOOTH_ADDR, 1, (unsigned int *)&tmp);
    if (tmp != 0xFFFFFFFF)
    {
        found = 1;
    }
    if (found)
    {
        Sys_ReadNVR4(MANU_INFO_BLUETOOTH_ADDR + 4, 1, (unsigned int *)&tmp);
        printf("0x%04X", tmp & 0xFFFF);
        if ((tmp & 0xFFFF) != *(uint16_t *)(DEVICE_INFO_BLUETOOTH_ADDR + 4))
        {
            found = 0;
        }
        Sys_ReadNVR4(MANU_INFO_BLUETOOTH_ADDR, 1, (unsigned int *)&tmp);
        printf("%08X\n", tmp);
        if (tmp != *(uint32_t *)DEVICE_INFO_BLUETOOTH_ADDR)
        {
            found = 0;
        }
        if (!found)
        {
            printf(
                "WARNING: Bluetooth address stored in NVR4 could not be found in NVR3. DEVICE_INFO_BLUETOOTH_ADDR should be updated for use in a real application.\n");
        }
    }
    else
    {
        printf("(none)\n");
    }
    Sys_ReadNVR4(0x81A2C, 1, (unsigned int *)&tmp);
    printf("  CRC: 0x%08X\n", tmp);
    printf("Manufacturing and test information:\n");
    found = 0;
    for (int i = 0; i < 7; i++)
    {
        Sys_ReadNVR4(MANU_INFO_DATA + 4 * i, 1, (unsigned int *)&tmp);
        if (tmp != 0xFFFFFFFF)
        {
            found = 1;
        }
    }
    if (found)
    {
        printf("  MANU_INFO_DATA:\n");
        for (unsigned int i = 0; i < 7; i++)
        {
            Sys_ReadNVR4(MANU_INFO_DATA + 4 * i, 1, (unsigned int *)&tmp);
            printf("    0x%08X\n", tmp);
        }
        Sys_ReadNVR4(MANU_INFO_DATA_CRC, 1, (unsigned int *)&tmp);
        printf("  MANU_INFO_DATA_CRC: 0x%08X\n", tmp);
        printf("  MANU_INFO_TEST_DATA1:\n");
        for (unsigned int i = 0; i < 7; i++)
        {
            Sys_ReadNVR4(MANU_INFO_TEST_DATA1 + 4 * i, 1, (unsigned
                                                           int *)&tmp);
            printf("    0x%08X\n", tmp);
        }
        Sys_ReadNVR4(MANU_INFO_TEST_DATA1_CRC, 1, (unsigned int *)&tmp);
        printf("  MANU_INFO_TEST_DATA1_CRC: 0x%08X\n", tmp);
        printf("  MANU_INFO_TEST_DATA2:\n");
        for (unsigned int i = 0; i < 7; i++)
        {
            Sys_ReadNVR4(MANU_INFO_TEST_DATA2 + 4 * i, 1, (unsigned
                                                           int *)&tmp);
            printf("    0x%08X\n", tmp);
        }
        Sys_ReadNVR4(MANU_INFO_TEST_DATA2_CRC, 1, (unsigned int *)&tmp);
        printf("  MANU_INFO_TEST_DATA2_CRC: 0x%08X\n", tmp);
        printf("  MANU_INFO_TEST_DATA3:\n");
        for (unsigned int i = 0; i < 7; i++)
        {
            Sys_ReadNVR4(MANU_INFO_TEST_DATA3 + 4 * i, 1, (unsigned
                                                           int *)&tmp);
            printf("    0x%08X\n", tmp);
        }
        Sys_ReadNVR4(MANU_INFO_TEST_DATA3_CRC, 1, (unsigned int *)&tmp);
        printf("  MANU_INFO_TEST_DATA3_CRC: 0x%08X\n", tmp);
        printf("  MANU_INFO_TEST_DATA4:\n");
        for (unsigned int i = 0; i < 7; i++)
        {
            Sys_ReadNVR4(MANU_INFO_TEST_DATA4 + 4 * i, 1, (unsigned
                                                           int *)&tmp);
            printf("    0x%08X\n", tmp);
        }
        Sys_ReadNVR4(MANU_INFO_TEST_DATA4_CRC, 1, (unsigned int *)&tmp);
        printf("  MANU_INFO_TEST_DATA4_CRC: 0x%08X\n", tmp);
    }
    else
    {
        printf("  (none)\n");
    }
    printf("Flash configuration:\n");
    Sys_ReadNVR4(MANU_FLASH_RR0, 1, (unsigned int *)&tmp);
    printf("  Address of first production patch sector: ");
    if (tmp != 0xFFFFFFFF)
    {
        printf("0x%08X", tmp);
    }
    else
    {
        printf("(none)");
    }
    printf("\n");
    Sys_ReadNVR4(MANU_FLASH_RR1, 1, (unsigned int *)&tmp);
    printf("  Address of second production patch sector: ");
    if (tmp != 0xFFFFFFFF)
    {
        printf("0x%08X", tmp);
    }
    else
    {
        printf("(none)");
    }
    printf("\n");
    Sys_ReadNVR4(MANU_FLASH_RR2, 1, (unsigned int *)&tmp);
    printf("  Address of first user patch sector: ");
    if (tmp != 0xFFFFFFFF)
    {
        printf("0x%08X", tmp);
    }
    else
    {
        printf("(none)");
    }
    printf("\n");
    Sys_ReadNVR4(MANU_FLASH_RR3, 1, (unsigned int *)&tmp);
    printf("  Address of second user patch sector: ");
    if (tmp != 0xFFFFFFFF)
    {
        printf("0x%08X", tmp);
    }
    else
    {
        printf("(none)");
    }
    printf("\n");
    for (unsigned int i = 0; i < 8; i++)
    {
        Sys_ReadNVR4(MANU_FLASH_CBD0 + i * 4, 1, (unsigned int *)&tmp);
        printf("  MANU_FLASH_CBD0%u: 0x%08X\n", i, tmp);
    }

    printf("\n");
    printf("End of program\n");

    /* Spin loop */
    while (1)
    {
        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();
    }
}
