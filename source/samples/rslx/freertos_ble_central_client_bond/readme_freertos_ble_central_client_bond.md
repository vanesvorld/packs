FreeRTOS Pairing and Bonding with Central Device Sample Code
============================================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This description is for FreeRTOS. If you want to know about the Pairing and 
Bonding with Client Device Sample Code, refer to the 
`readme_ble_central_client_bond.txt` in the 
`ble_central_client_bond` sample application.

This sample project demonstrates a simple application using CMSIS-FreeRTOS 
provided by Arm. CMSIS-FreeRTOS is a common API for real-time operating 
systems (RTOS2). To build this sample application, `ARM.CMSIS` and 
`ARM.CMSIS-FreeRTOS` packs must be installed in your IDE. In CMSIS pack 
manager, on the right panel you can see the Packs and Examples view. In the 
Packs view, you can see CMSIS packs. Find `ARM.CMSIS` and 
`ARM.CMSIS-FreeRTOS` and click on the Install button. When you copy 
the `freertos_ble_central_client_bond` on the Examples view, 
the C/C++ perspective opens and displays your newly copied project. On the right
side, the `freertos_ble_central_client_bond.rteconfig` file 
displays software components. You need to verify that FreeRTOS in 
CMSIS/RTOS2 (API) and the components of RTOS2 are selected.

This sample application calls `osKernelInitialize()` to initialize RTOS2 
and creates the main `vThread_BLE` thread to run FreeRTOS.
The operation of the `ble_central_client_bond` runs in the main 
`vThread_BLE`.

Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
Getting Started Guide for your IDE for more information.

Select the project configuration according to the required optimization level. 
Use the "Debug" configuration for optimization level "None" or the "Release"
configuration for optimization level "More" or "O2".

Verification
------------
To verify if this application is functioning correctly, use another RSL10 
board with the `ble_peripheral_server_bond` application or another 
third-party peripheral device application to advertise while this application 
establishes a connection and ensures pairing/bonding/encryption is successfully 
performed. In addition to establishing a connection, this application can be 
used to read/write characteristics and receive notifications.

Alternatively, you can observe the behavior of the LED on the RSL10 Evaluation 
and Development Board (DIO6). The LED behavior is controlled by the 
`APP_LED_Timeout_Handler` function in `app_msg_handler.c` and can 
be one of the following:

   - If the device has not started a connection procedure, the LED is off.
   - If the device started a connection procedure but it is not connected to 
     any peer, the LED blinks every 200ms.
   - If the device is connected to fewer than `APP_NB_PEERS` peers, the LED
     blinks every 2 seconds according to the number of connected peers 
     (i.e., blinks once if one peer is connected, twice if two peers are 
     connected, etc.).
   - If the device is connected to `APP_NB_PEERS` peers, the LED is steady 
     on.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which pauses at the
    start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application can work
    properly.

***********************************************************
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
