/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * supplementalcalibrate.c
 * - Supplement the standard calibration information with additional targets
 *   for VDDRF (+1 dBm TX power), VDDPA (+4 dBm TX power), and the RC
 *   oscillator of 10.24 MHz.
 * ----------------------------------------------------------------------------
 * $Revision: 1.8 $
 * $Date: 2018/06/21 14:40:14 $
 * ------------------------------------------------------------------------- */
#include "app.h"

uint32_t SupplementalCalibrate(CalSetting_Type *cal_values)
{
    uint32_t cal_result;
    uint32_t temp = 0;

    /* Refresh the watchdog timer */
    Sys_Watchdog_Refresh();

    /* Disable all external interrupts */
    Sys_NVIC_DisableAllInt();

    /* Clear the pending status for all external interrupts */
    Sys_NVIC_ClearAllPendingInt();

    /* Initialize the supplemental calibration data structure */
    memset(cal_values, 0xFF, sizeof(CalSetting_Type));

    /* Initialize the power supply components prior to calibrating VDDRF,
     * VDDPA for supplemental targets. Load pre-calibrated defines
     * for the bandgap, DCDC, VDDC, and VDDM to ensure the calibrations
     * are accurate */
    Calibrate_Power_Initialize();

    /* Load the default band-gap trim */
    temp = *(uint32_t *)(MANU_INFO_BANDGAP);
    if (temp != 0xFFFFFFFF)
    {
        ACS->BG_CTRL = temp & (ACS_BG_CTRL_VTRIM_Mask
                               | ACS_BG_CTRL_SLOPE_TRIM_Mask);
    }

    /* Calibrate the desired DC-DC trim */
    cal_result = Calibrate_Power_DCDC(0, (uint32_t *)&ADC->DATA_TRIM_CH[0],
                                      ADC_DCDC_TARGET);
    if (cal_result != ERRNO_NO_ERROR)
    {
        return (cal_result);
    }
    else
    {
        /* Save the VCC target value in the cal_values struct */
        cal_values->VCC_CAL_TARGET = ADC_DCDC_TARGET;

        /* Save the VCC trim value in the cal_values struct */
        cal_values->VCC_CAL_TRIM_VALUE = ACS_VCC_CTRL->VTRIM_BYTE;
    }

    /* Calibrate the desired VDDC trims */
    cal_result = Calibrate_Power_VDDC(0, (uint32_t *)&ADC->DATA_TRIM_CH[0],
                                      ADC_VDDC_TARGET);
    if (cal_result != ERRNO_NO_ERROR)
    {
        return (cal_result);
    }
    else
    {
        /* Save the VDDC target value in the cal_values struct */
        cal_values->VDDC_CAL_TARGET     = ADC_VDDC_TARGET;

        /* Save the VDDC trim value in the cal_values struct */
        cal_values->VDDC_CAL_TRIM_VALUE = ACS_VDDC_CTRL->VTRIM_BYTE;
    }

    /* Calibrate the desired VDDM trims */
    cal_result = Calibrate_Power_VDDM(0, (uint32_t *)&ADC->DATA_TRIM_CH[0],
                                      ADC_VDDM_TARGET);
    if (cal_result != ERRNO_NO_ERROR)
    {
        return (cal_result);
    }
    else
    {
        /* Save the VDDM target value in the cal_values struct */
        cal_values->VDDM_CAL_TARGET     = ADC_VDDM_TARGET;

        /* Save the VDDM trim value in the cal_values struct */
        cal_values->VDDM_CAL_TRIM_VALUE = ACS_VDDM_CTRL->VTRIM_BYTE;
    }

    cal_values->TX_POWER_VALUE    = RF_TX_POWER_LEVEL_DBM;

    /* Refresh the watchdog timer */
    Sys_Watchdog_Refresh();

    /* Initialize system for clock calibration */
    Calibrate_Clock_Initialize();

    /* Enable the (multiplied mode) of the start oscillator */
    ACS_RCOSC_CTRL->CLOCK_MULT_ALIAS = RC_START_OSC_12MHZ_BITBAND;

    /* Calibrate the Start RC Oscillator, give it the target in kHz */
    cal_result = Calibrate_Clock_Start_OSC(START_OSC_TARGET);

    /* Return an error if calibration failed */
    if (cal_result != ERRNO_NO_ERROR)
    {
        return (cal_result);
    }
    else
    {
        /* Convert the start oscillator target to kHz so it can fit in 2
         * bytes */
        cal_values->START_OSC_CAL_TARGET     = START_OSC_TARGET;

        /* Save trim value in cal_values struct to be written to NVR3 */
        cal_values->START_OSC_CAL_TRIM_VALUE = (ACS->RCOSC_CTRL &
                                                ACS_RCOSC_CTRL_FTRIM_START_Mask)
                                               >> ACS_RCOSC_CTRL_FTRIM_START_Pos;
    }

    SystemCoreClock   = START_OSC_TARGET * 1000;
    CLK_DIV_CFG0->SLOWCLK_PRESCALE_BYTE = 9;
    FLASH->DELAY_CTRL = (FLASH->DELAY_CTRL & ~FLASH_DELAY_CTRL_SYSCLK_FREQ_Mask)
                        | FLASH_DELAY_FOR_SYSCLK_10MHZ;

    return (ERRNO_NO_ERROR);
}
