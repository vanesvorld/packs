/* ----------------------------------------------------------------------------
 * Copyright (c) 2015-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * system_start.c
 * - A system initialization function to be stored to the MANU_INFO_INIT
 *   area of non-volatile record 3 (NVR3).
 * ----------------------------------------------------------------------------
 * $Revision: 1.23 $
 * $Date: 2019/08/12 20:44:41 $
 * ------------------------------------------------------------------------- */
#include "app.h"

#if defined ( __ICCARM__ )
#pragma location=".sysinfo1"
const uint32_t initLength;
#pragma location=".sysinfo3"
void System_Start(void);
#else /*defined ( __ICCARM__ )*/
uint16_t initLength  __attribute__ ((section(".sysinfo1")));
uint16_t initVersion __attribute__ ((section(".sysinfo2")));

void System_Start(void) __attribute__ ((section(".sysinfo3")));
#endif /*defined ( __ICCARM__ )*/

/* ----------------------------------------------------------------------------
 * Function      : void System_Start(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system to calibration settings stored in NVR4
 *                 and supplemental calibration settings stored in NVR3.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void System_Start(void)
{
    uint32_t temp;


    /* Enable flash recall so we can read the NVR4 portion of flash (all other
    * bits are reset to default).
    * Note that this is only allowed since this code is executed from NVR3.
    * When executing from the flash MAIN section Sys_ReadNVR4 must be used. */
    FLASH->IF_CTRL = FLASH_RECALL_ENABLE;
    while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);

    /* Load the default band-gap trim */
    temp = *(uint32_t *)(MANU_INFO_BANDGAP);
    if (temp != 0xFFFFFFFF)
    {
        ACS->BG_CTRL = temp & (ACS_BG_CTRL_VTRIM_Mask
                               | ACS_BG_CTRL_SLOPE_TRIM_Mask);
    }

    /* Load the supplemental calibrated VCC setting */
    /* If the value of the MANU_INFO_VERSION field is 22 or higher, the MANU_INFO_DCDC
     * field is renamed MANU_INFO_VCC, which is divided into two 8-bit fields
     * (specifically, bits 0 - 4 are the VCC trim settings for LDO mode, and
     * bits 8 - 12 are the VCC trim settings for DC-DC buck converter mode). If
     * the MANU_INFO_VERSION is lower than 22, the MANU_INFO_DCDC is valid for
     * both LDO mode and DC-DC buck converter mode. */
    temp = *(uint32_t *)&CALSETTING->VCC_CAL_TRIM_VALUE;
    if (temp != 0xFFFFFFFF)
    {
        ACS_VCC_CTRL->VTRIM_BYTE = (uint8_t)(temp & (ACS_VCC_CTRL_VTRIM_Mask
                                                     >> ACS_VCC_CTRL_VTRIM_Pos));
    }

    /* Load the supplemental calibrated VDDC setting */
    temp = *(uint32_t *)&CALSETTING->VDDC_CAL_TRIM_VALUE;
    if (temp != 0xFFFFFFFF)
    {
        ACS_VDDC_CTRL->VTRIM_BYTE = (uint8_t)(temp & (ACS_VDDC_CTRL_VTRIM_Mask
                                                      >>
                                                      ACS_VDDC_CTRL_VTRIM_Pos));
    }

    temp = *(uint32_t *)(MANU_INFO_VDDC_STANDBY);
    if (temp != 0xFFFFFFFF)
    {
        ACS_VDDC_CTRL->STANDBY_VTRIM_BYTE = (uint8_t)(temp &
                                                      (
                                                          ACS_VDDC_CTRL_STANDBY_VTRIM_Mask
                                                          >>
                                                          ACS_VDDC_CTRL_STANDBY_VTRIM_Pos));
    }

    /* Reconfigure the default VDDACS retention control register setting to
     * minimize the VDDACS supply */
    ACS_VDDRET_CTRL->VDDTRET_BYTE = VDDTRET_DISABLE_BYTE;

    /* Load the supplemental calibrated VDDM setting */
    temp = *(uint32_t *)&CALSETTING->VDDM_CAL_TRIM_VALUE;
    if (temp != 0xFFFFFFFF)
    {
        ACS_VDDM_CTRL->VTRIM_BYTE = (uint8_t)(temp & (ACS_VDDM_CTRL_VTRIM_Mask
                                                      >>
                                                      ACS_VDDM_CTRL_VTRIM_Pos));
    }

    temp = *(uint32_t *)(MANU_INFO_VDDM_STANDBY);
    if (temp != 0xFFFFFFFF)
    {
        ACS_VDDM_CTRL->STANDBY_VTRIM_BYTE = (uint8_t)(temp &
                                                      (
                                                          ACS_VDDM_CTRL_STANDBY_VTRIM_Mask
                                                          >>
                                                          ACS_VDDM_CTRL_STANDBY_VTRIM_Pos));
    }

    /* Set the pads to low drive to ensure a clean start-up for all VDDO/VDDM
     * combinations. */
    DIO->PAD_CFG = PAD_LOW_DRIVE;

    ACS->VDDA_CP_CTRL = VDDA_PTRIM_16MA;

    /* Load the supplemental calibrated multiplied setting for the startup RC
     * oscillator (to enable a 10.24 MHz clock), and the default 32 kHz
     * configuration. */
    temp = *(uint32_t *)&CALSETTING->START_OSC_CAL_TRIM_VALUE;
    if ((temp != 0xFFFFFFFF) && ((temp >> 16) == START_OSC_TARGET))
    {
        /* Set SLOWCLK prescaler, the FLASH_DELAY_CTRL register, and
         * SystemCoreClock accordingly. Due to frequency variation over
         * temperature, the 12 MHz flash timing needs to be used. */
        CLK_DIV_CFG0->SLOWCLK_PRESCALE_BYTE = 9;
        FLASH->DELAY_CTRL = (FLASH_DELAY_FOR_SYSCLK_12MHZ |
                             DEFAULT_READ_MARGIN);
        SystemCoreClock   = START_OSC_TARGET * 1000;

        /* Store the loaded trim setting and set FTRIM_FLAG in ACS_RCOSC_CTRL
         * byte 1, then explicitly set the system clock source to RCCLK */
        ACS_RCOSC_CTRL->RESERVED0[0]     = (uint8_t)(temp |
                                                     (RC_OSC_CALIBRATED >> 8));
        ACS_RCOSC_CTRL->CLOCK_MULT_ALIAS = RC_START_OSC_12MHZ_BITBAND;
        CLK->SYS_CFG = (EXTCLK_PRESCALE_1 | JTCK_PRESCALE_1 |
                        SYSCLK_CLKSRC_RCCLK);
    }

    temp = *(uint32_t *)(MANU_INFO_OSC_32K);
    if (temp != 0xFFFFFFFF)
    {
        ACS_RCOSC_CTRL->FTRIM_32K_BYTE = (uint8_t)temp;
    }

    /* Disable the flash recall before returning. */
    FLASH->IF_CTRL = FLASH_RECALL_DISABLE;
    while (FLASH_IF_STATUS->BUSY_ALIAS == FLASH_IF_BUSY_BITBAND);
}

#if defined ( __ICCARM__ )
#pragma location=".sysinfo4"
const uint16_t crcValue;
#else
uint16_t crcValue __attribute__ ((section(".sysinfo4")));
#endif /*defined ( __ICCARM__ )*/
