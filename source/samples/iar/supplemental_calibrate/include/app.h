/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.h
 * - Application header file
 * ----------------------------------------------------------------------------
 * $Revision: 1.30 $
 * $Date: 2019/08/12 20:44:43 $
 * ------------------------------------------------------------------------- */

#ifndef APP_H
#define APP_H

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>
#include <rsl10_flash_rom.h>
#include <rsl10_map_nvr.h>
#include <rsl10_calibrate.h>
#include <stdlib.h>
#include <rsl10_ke.h>
#include <rsl10_ble.h>
#include <rsl10_flash_rom.h>
#include <rsl10_protocol.h>

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Definitions
 * ------------------------------------------------------------------------- */
#define DEFAULT_ERASED_MEM               { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, \
                                           0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, \
                                           0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, \
                                           0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, \
                                           0xFF, 0xFF, 0xFF, 0xFF }

/* ADC channel to read random analog noise */
#define ADC_RAND_CHANNEL                 6

#define INFO_ADDR_WORD_LEN               2
#define INFO_LOCK_WORD_LEN               6
#define INFO_IRK_WORD_LEN                4
#define INFO_CSRK_WORD_LEN               4
#define INFO_PRIVATE_KEY_WORD_LEN        8
#define INFO_PUBLIC_X_WORD_LEN           8
#define INFO_PUBLIC_Y_WORD_LEN           8
#define DEFAULT_ERASED_MEM_WORD_LEN      8

/* ----------------------------------------------------------------------------
 * Supplemental Manufacturing Initialization Version Code (1.1.0 beta)
 * ------------------------------------------------------------------------- */
#define MANU_INFO_INIT_FW_VER_MAJOR     0x81
#define MANU_INFO_INIT_FW_VER_MINOR     0x01
#define MANU_INFO_INIT_FW_VER_REVISION  0x00

#define MANU_INFO_INIT_FW_VER           ((MANU_INFO_INIT_FW_VER_MAJOR << 12) | \
                                         (MANU_INFO_INIT_FW_VER_MINOR << 8)  | \
                                         MANU_INFO_INIT_FW_VER_REVISION)

/* DIO number that is connected to Button of EVB */
#define BUTTON_DIO                      5

/* DIO number that is connected to LED of EVB */
#define LED_DIO                         6

/* Set RF output transmission power level (in unit of dBm) */
#ifndef RF_TX_POWER_LEVEL_DBM
#define RF_TX_POWER_LEVEL_DBM           0
#endif
/* Target power supply voltages [mV*10]*/
#define ADC_VDDC_TARGET                 105      /* 105 [mV*10] */
#define ADC_VDDM_TARGET                 110      /* 110 [mV*10] */
#define ADC_DCDC_TARGET                 120      /* 120 [mV*10] */

/* Target frequencies */
#define START_OSC_TARGET                10240    /* 10.24 MHz   */

/* Place the supplemental calibration data immediately before MANU_INFO_INIT */
#define CALSETTING_BASE                 (MANU_INFO_INIT - \
                                         sizeof(CalSetting_Type))
#define CALSETTING                      ((CalSetting_Type *)CALSETTING_BASE)

#define DEFINE_MESSAGE_HANDLER(message, handler) { message, \
                                                   (ke_msg_func_t)handler }

/* Number of APP Task Instances */
#define APP_IDX_MAX                     1

/* Bluetooth device address type */
#define BD_TYPE_PUBLIC                  0
#define BD_TYPE_PRIVATE                 1

/* Length of Bluetooth address (in octets) */
#define BDADDR_LENGTH                   6

/* Non-resolvable private Bluetooth device address */
#define PRIVATE_BDADDR                  { 0x94, 0x11, 0x22, 0xff, 0xff, 0x05 }

/* GAPM configuration definitions */
#define RENEW_DUR                       15000
#define MTU_MAX                         0x200
#define MPS_MAX                         0x200
#define ATT_CFG                         0x80
#define TX_OCT_MAX                      0x1b
#define TX_TIME_MAX                     (14 * 8 + TX_OCT_MAX * 8)

/* List of message handlers that are used by the Bluetooth application manager */
#define BLE_MESSAGE_HANDLER_LIST                                              \
    DEFINE_MESSAGE_HANDLER(GAPM_CMP_EVT, GAPM_CmpEvt),                        \

/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/

/* APP Task messages */
enum appm_msg
{
    APPM_DUMMY_MSG = TASK_FIRST_MSG(TASK_ID_APP),
};

/* Define the available application states */
enum appm_state
{
    /* Initialization state */
    APPM_INIT,

    /* Number of defined states */
    APPM_STATE_MAX
};

struct dev_info
{
    unsigned int deviceAddr[INFO_ADDR_WORD_LEN];
    unsigned int deviceLockInfo[INFO_LOCK_WORD_LEN];
    unsigned int irk[INFO_IRK_WORD_LEN];
    unsigned int csrk[INFO_CSRK_WORD_LEN];
    unsigned int priv_key[INFO_PRIVATE_KEY_WORD_LEN];
    unsigned int pub_key_x[INFO_PUBLIC_X_WORD_LEN];
    unsigned int pub_key_y[INFO_PUBLIC_Y_WORD_LEN];
};

/* ----------------------------------------------------------------------------
 * Structure that holds calibration values to be written in NVR3
 * ------------------------------------------------------------------------- */
typedef struct
{
    uint16_t VDDC_CAL_TRIM_VALUE;
    uint16_t VDDC_CAL_TARGET;

    uint16_t VDDM_CAL_TRIM_VALUE;
    uint16_t VDDM_CAL_TARGET;

    uint16_t VCC_CAL_TRIM_VALUE;
    uint16_t VCC_CAL_TARGET;

    uint16_t START_OSC_CAL_TRIM_VALUE;
    uint16_t START_OSC_CAL_TARGET;

    int16_t TX_POWER_VALUE;
    uint32_t reserved2;                  /* Required for alignment to an even
                                          * number of words */
} CalSetting_Type;

typedef enum
{
    LED_SUCCESS = 5000,        /* LED success status */
    LED_FAIL = 1000,           /* LED fail status */
} LEDStatus;

/* Support for the application manager and the application environment */
extern const struct ke_state_handler appm_default_handler;
extern ke_state_t appm_state;

/* Bluetooth Device Address */
#if defined ( __ICCARM__ )
extern const unsigned int initLength;
extern const unsigned short crcValue;
#else /*defined ( __ICCARM__ )*/
extern unsigned short initLength;
extern unsigned short initVersion;
extern unsigned short crcValue;
#endif /*defined ( __ICCARM__ )*/
extern const struct ke_task_desc TASK_DESC_APP;

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/
extern void System_Start(void);

extern uint32_t SupplementalCalibrate(CalSetting_Type *cal_values);

extern uint8_t Check_MemEmpty(unsigned int *data, unsigned int word_len);

extern void Read_InfoFlash(struct dev_info *info);

extern FlashStatus Restore_InfoFlash(struct dev_info *info);

extern void Rand_FillBuffer(unsigned int *mem, unsigned int word_len);

extern void Rand_GenSeed(uint32_t *seed);

extern void Test_Error(uint32_t result);

extern void BLE_Initialize(void);

extern void BLE_KeyRead(uint8_t *private_key, uint8_t *public_key_x,
                        uint8_t *public_key_y, uint32_t seed);

extern int GAPM_CmpEvt(ke_msg_id_t const msgid,
                       struct gapm_cmp_evt const *param,
                       ke_task_id_t const dest_id,
                       ke_task_id_t const src_id);

extern void App_Initialize(void);

extern int Msg_Handler(ke_msg_id_t const msgid, void *param,
                       ke_task_id_t const dest_id,
                       ke_task_id_t const src_id);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* APP_H */
