Supplemental Calibration Sample Code
====================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in `Software_Use_Agreement.rtf`
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project:

- Calibrates VCC, VDDM, VDDC, VDDRF, VDDPA and `RCOSC_MULT` values using 
  `calibratelib`, which can take any value within their corresponding 
  tolerances. However, BandGap voltage is not calculated, but rather read from
  a completed `NVR4` table in flash memory. This information is then stored in 
  the `NVR3` region of the system's flash memory before the `MANU_INFO_INIT`.
  
- Stores an initialization function called `System_Start()` within `NVR3` 
  starting from the manufacturing information address. This is run each time
  on reset by the boot ROM application before execution begins from main flash
  (user application), and loads pre-calibrated trim values stored in `NVR3` 
  into their corresponding control registers. For more information about the
  initialization function, refer to the *RSL10 Firmware Reference*.

Once `System_Start()` is called when the board starts up from a reset, the 
default values loaded from `NVR4` consist of the BandGap voltage and the 32 k 
Oscillator clock. The other internal voltages and clock trim values are loaded 
from the `NVR3` region of flash as non-default calibrations.

Code
----
    system_start.c    - The initialization function and the spin loop function
                        is implemented here.
    supplementalcaliberate.c 
                      - Function to generate supplemental calibration
                        information is implemented here.

Include
-------
    app.h             - Overall application header file

Hardware Requirements
---------------------
This application can only be executed successfully on production silicon 
(RSL10 Evaluation and Development Boards with complete `NVR4` tables). If the 
application runs on a device without a complete `NVR4` table, the application 
undergoes fail safe and enters the `Test_Error()` spin loop, indicating that 
the needed calibration targets are unavailable.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the
*Getting Started Guide* for your IDE for more information.

Select the project configuration according to the chip ID version of
RSL10. The appropriate libraries and `include` files are loaded according to
the build configuration that you choose. Ensure that the device CID matches 
the application build.

Verification
------------
This sample application runs from the main flash. Generate the default debug
configuration as highlighted by the *Getting Started Guide*, and under the 
startup tab, uncheck **pre-run reset and halt** and **continue** before 
starting debugging. Additionally, ensure that the source file has been copied 
into the working directory when your IDE opens at startup. At the begining, 
the program waits for the user to press push button (DIO5) as a start command.

To verify that this application is functioning correctly, initiate a debug
session and set a breakpoint immediately after the line
`ACS->AOUT_CTRL = AOUT_DCDC_SENSE;`. Once the breakpoint has been hit,
use a multimeter to measure the voltage on testpad number 2. If the measured
voltage does not match the target value given to the calibration function,
or if the breakpoint is not hit, then the trim values have not been written
accurately to the right register, or the calibration function for the specific
voltage has failed. If the right voltage is measured, then set a breakpoint
after every line starting with `ACS->AOUT_CTRL` and step through the function,
to make sure each breakpoint is hit. Once a single breakpoint is hit, measure
the voltage on testpad number 2 to check if the voltage sensed on the pad 
matches the calibrated target value.

In order to test the functionality of the `System_Start()` function on a reset 
of the board, first check that `flash_write()` in the `main()` function is 
accurately writing the struct and function itself into `NVR3`. Since the RSL10 
Evaluation and Development Board has only one test pad, `System_Start()` has 
to be complied and loaded multiple times, and can only have a single 
`ACS->AOUT_CTRL` line at a time.

To test to see if `System_Start()` is setting voltages accurately, start by
uncommenting the line `ACS->AOUT_CTRL = AOUT_VCC_SENSE;` and then rebuild
the entire application, and write the function to `NVR3` again. After 
resetting the board, measure to see if the voltage at test pad 2 matches the 
target DCDC voltage. If the voltage is accurate, comment out this line again 
and uncomment the next `ACS->AOUT_CTRl` line. Rebuild the project and reset 
the board; test again to ensure that the new voltage being piped to the test 
pad is accurate. Repeat these steps until all the voltages being piped to the 
test pad have been tested. Finally, the `RCOSC_MULT` signal can be detected 
any time on DIO pin 1, and can be used to verify that the function is running 
on reset.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going to Sleep Mode or resetting continuously (either by design or
due to programming error). To circumvent this scenario, a software recovery 
mode using DIO12 is implemented in the start-up function written to NVR3. To 
use the software recovery mode, do the following:

1.  Connect DIO12 to ground. 
2.  Press the RESET button (this re-runs the start-up function in NVR3, 
       and pauses futher code execution).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from 
       ground, and press the RESET button so that the application execution
       resumes successfully.

If the value of the `MANU_INFO_VERSION` field is 22 or higher, the 
`MANU_INFO_DCDC` field is renamed `MANU_INFO_VCC`, which is divided into two 
8-bit fields (specifically, bits 0 - 4 are the VCC trim settings for LDO mode,
and bits 8 - 12 are the VCC trim settings for DC-DC buck converter mode). If 
the `MANU_INFO_VERSION` is lower than 22, the `MANU_INFO_DCDC` is valid for 
both LDO mode and DC-DC buck converter mode.

**Caution:** Erase the main flash or load another application right after 
executing this project; otherwise, there is a chance that the `NVR3` space can
be corrupted, rendering the RSL10 completely non-functional as a result. 
***
Copyright (c) 2019 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
