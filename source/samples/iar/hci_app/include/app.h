/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.h
 * - HCI application main header
 * ----------------------------------------------------------------------------
 * $Revision: 1.1 $
 * $Date: 2018/11/19 15:09:46 $
 * ------------------------------------------------------------------------- */

#ifndef APP_H
#define APP_H

/* Defines */
#ifndef CFG_6DBM
#define CFG_6DBM
#endif    /* CFG_6DBM */

#ifndef CFG_BUCK_COV
#define CFG_BUCK_COV
#endif    /* CFG_BUCK_COV */

#ifndef CFG_CTS
#define CFG_CTS
#endif    /* CFG_CTS */

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Include files
 * --------------------------------------------------------------------------*/
#include <rsl10.h>
#include <rsl10_ke.h>
#include <rsl10_ble.h>
#include <rsl10_profiles.h>
#include <rsl10_sys_clocks.h>
#include <rsl10_calibrate_power.h>
#include "uart.h"
#include "ble_std.h"

#define TIMER_200_MS 781

/* DIO number that is used for easy re-flashing (recovery mode) */
#define RECOVERY_DIO                    12

extern void App_Initialize(void);

void TIMER0_IRQHandler(void);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* APP_H */
