/* ----------------------------------------------------------------------------
 * Copyright (c) 2016-2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * uart.c
 * - Wrapper for UART driver
 * ----------------------------------------------------------------------------
 * $Revision: 1.1 $
 * $Date: 2018/11/19 15:09:46 $
 * ------------------------------------------------------------------------- */

#include "uart.h"

/* Buffers for DMA read and DMA write */
uint8_t *bufferRd;
uint8_t *bufferWr;

/* ----------------------------------------------------------------------------
 * Function      : void UART_Init (uint32_t sysClock, uint32_t baudRate,
 *                                 uint8_t txDIO, uint8_t rxDIO);
 * ----------------------------------------------------------------------------
 * Description   : Sets up DMA interface, UART channels, TX/RX DIOs, interrupts.
 * Inputs        : - sysClock    - The clock frequency of for UART
 *                 - baudRate    - UART baud rate
 *                 - txDIO       - Pin for transmitter DIO
 *                 - rxDIO       - Pin for receiver DIO
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void UART_Init(uint32_t sysClock, uint32_t baudRate, uint8_t txDIO, uint8_t
               rxDIO)
{
    bufferWr = (uint8_t *)malloc(DMA_TRANSFER_LENGTH);
    bufferRd = (uint8_t *)malloc(DMA_TRANSFER_LENGTH);

    /* if the space cannot be allocated do not init the port and DMAs */
    if (bufferWr == NULL || bufferRd == NULL)
    {
        return;
    }

    /* Initialize DMA for TX and RX channels */
    DMA_Chan_Init(TX_DMA_NUM, DMA_CH0_CONFIG, DMA_TRANSFER_LENGTH,
                  (uint32_t)bufferRd, (uint32_t)&UART->TX_DATA);
    DMA_Chan_Init(RX_DMA_NUM, DMA_CH1_CONFIG, DMA_TRANSFER_LENGTH,
                  (uint32_t)&UART->RX_DATA, (uint32_t)bufferWr);

    /* Enable RX and TX DMA interrupt handlers */
    NVIC_ClearPendingIRQ(DMA0_IRQn);
    NVIC_EnableIRQ(DMA0_IRQn);

    NVIC_ClearPendingIRQ(DMA1_IRQn);
    NVIC_EnableIRQ(DMA1_IRQn);

    /* Enable device UART port */
    Sys_UART_Enable(sysClock, baudRate, UART_DMA_MODE_ENABLE);

    /* Configure TX and RX pin with correct current values */
    Sys_UART_DIOConfig(DIO_2X_DRIVE | DIO_LPF_DISABLE, txDIO, rxDIO);
}

/* ----------------------------------------------------------------------------
 * Function      : void DMA_Chan_Init (uint32_t dmaChannel, uint32_t dmaConfig,
 *                                     uint32_t dmaLength, uint32_t dmaSrc,
 *                                     uint32_t dmaDest)
 * ----------------------------------------------------------------------------
 * Description   : Configures, clears and enables the selected DMA channel.
 * Inputs        : - dmaChannel   - DMA channel number
 *                 - dmaConfig    - Configuration settings for the channel
 *                 - dmaLength    - Length of data for transmission
 *                 - dmaSrc       - Source address for DMA transaction
 *                 - dmaDest      - Destination address for DMA transaction
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void DMA_Chan_Init(uint32_t dmaChannel, uint32_t dmaConfig, uint32_t dmaLength,
                   uint32_t dmaSrc, uint32_t dmaDest)
{
    /* Configure the DMA channel, clear status and enable DMA channel */
    Sys_DMA_ChannelConfig(dmaChannel, dmaConfig, dmaLength, 0, dmaSrc,
                          dmaDest);
    Sys_DMA_ClearChannelStatus(dmaChannel);
    Sys_DMA_ChannelEnable(dmaChannel);
}

/* ----------------------------------------------------------------------------
 * Function      : uint8_t DMA_Status_Complete (uint32_t dmaChannel)
 * ----------------------------------------------------------------------------
 * Description   : Checks if the DMA transaction has completed and clears
 *                 DMA channel status register.
 * Inputs        : dmaChannel   - The dma channel number to check for completion
 * Outputs       : return value - DMA_STATUS_COMPLETE_INT_STATUS
 * Assumptions   : Called within the UART interrupt handler
 * ------------------------------------------------------------------------- */
uint8_t DMA_Status_Complete(uint32_t dmaChannel)
{
    uint8_t status = Sys_DMA_Get_ChannelStatus(dmaChannel);
    if (status == 1)
    {
        Sys_DMA_ClearChannelStatus(dmaChannel);
    }
    return (status);
}

/* ----------------------------------------------------------------------------
 * Function      : void DMA_read(void)
 * ----------------------------------------------------------------------------
 * Description   : Populates the read buffer with DMA buffer using the
 *                 parameters
 *                 in the RX interface.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : Called within eif_read in API
 * ------------------------------------------------------------------------- */
void DMA_read(void)
{
    bufferRd = (uint8_t *)malloc(RX_INTERFACE.size);

    Sys_DMA_ChannelConfig(RX_DMA_NUM, DMA_CH1_CONFIG, RX_INTERFACE.size,
                          0, (uint32_t)&(UART->RX_DATA), (uint32_t)bufferRd);
}

/* ----------------------------------------------------------------------------
 * Function      : void DMA_write(void)
 * ----------------------------------------------------------------------------
 * Description   : Populates the DMA buffer with write buffer data using the
 *                 parameters in the TX interface.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : Called within eif_write in API
 * ------------------------------------------------------------------------- */
void DMA_write(void)
{
    bufferWr = (uint8_t *)malloc(TX_INTERFACE.size);
    memcpy(bufferWr, TX_INTERFACE.bufptr, TX_INTERFACE.size * sizeof(uint8_t));
    Sys_DMA_ChannelConfig(TX_DMA_NUM, DMA_CH0_CONFIG, TX_INTERFACE.size,
                          0, (uint32_t)bufferWr, (uint32_t)&(UART->TX_DATA));
    Sys_DMA_ChannelEnable(TX_DMA_NUM);
}

/* ----------------------------------------------------------------------------
 * Function      : void DMA0_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Checks for completed DMA transaction for TX and services
 *                 callback.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : Triggered on interrupt
 * ------------------------------------------------------------------------- */
void DMA0_IRQHandler(void)
{
    if (DMA_Status_Complete(TX_DMA_NUM) && eif_callback_check(TX_INTERFACE))
    {
        free(bufferWr);
        eif_callback_handler(TX_INTERFACE);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void DMA1_IRQHandler(void)
 * ----------------------------------------------------------------------------
 * Description   : Checks for completed DMA transaction for RX and services
 *                 callback.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : Triggered on interrupt
 * ------------------------------------------------------------------------- */
void DMA1_IRQHandler(void)
{
    if (DMA_Status_Complete(RX_DMA_NUM) && eif_callback_check(RX_INTERFACE))
    {
        /* Copy contents of RX interface to read buffer */
        memcpy(RX_INTERFACE.bufptr, bufferRd, RX_INTERFACE.size *
               sizeof(uint8_t));
        free(bufferRd);
        eif_callback_handler(RX_INTERFACE);
    }
}
