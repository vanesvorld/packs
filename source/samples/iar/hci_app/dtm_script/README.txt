                       Direct Test Mode Sample Script
                       ==============================

Direct test mode (DTM) uses an upper tester (UT), lower tester (LT) and a device
under test (DUT). The UT sends control information to both the DUT and LT to 
configure and conduct the transmit or receive tests. The UT sends packets to the
DUT and receives a packet count from the LT. The type of data in the packet can 
be used to test different RF characteristics. 

The dtm_lib module is the underlying library that implements direct test mode 
tests using the host-controller interface. The dtm_example script shows how to 
call the library functions to perform direct test mode tests. The library 
supports enhanced data rate of LE 2M tests if enhanced tests are enabled.


Requirements: 
--------------------------------------------------------------------------------
- Pyserial version 3.1.1 
    source: https://pypi.python.org/pypi/pyserial
- Python version 2.7
    source: https://www.python.org/download/releases/2.7/
- J-Link version 5.02 or higher
    source: SDK installation package
- RSL10 Flash Loader version 1.0.6 or higher
    source: in SDK go to Run>External Tools>Flash Loader
- RSL10 board connected through micro-USB
- Bluetooth receiver or a second RSL10

Hardware Setup:
--------------------------------------------------------------------------------
1) Set an RSL10 board as DUT.
2) Set a Bluetooth receiver or a second RSL10 as LT.
3) Ensure both the DUT and the LT are connected to the PC.
4) Make the required UART pin connections for the connected RSL10 board(s):
        * Header P4: Pin TX0 connected to Header P1: DIO 4
        * Header P4: Pin RX0 connected to Header P1: DIO 5 
        * Header P4: Pin CTS connected to Header P1: DIO 9
5) Ensure an antenna is connected to the DUT.

Script Setup:
--------------------------------------------------------------------------------
1) Build the HCI_app using the Log build configuration and generate an
   image (.hex). 
2) Open RSL10 Flash Loader and navigate to the folder where the binary is
   generated
4) Flash the image on to the board and ensure that the image has been loaded and
   passes verification.
5) Get the COM port numbers for both the DUT and the LT:
        * The COM ports can be found in Device Manager->Ports on Windows
        * RSL10s will show up as JLink CDC UART Port (COM##)
6) Navigate to the folder, set the COM ports and run the script with the set 
   arguments.
7) Available COM ports are listed and the user must input the DUT and LT COM 
   ports.

The command line arguments are as follows:

Channel - Physical BLE channels [0 to 39].

BaudRate - Serial port baud rate for both device under test and
           lower tester.

TestTime - Duration of each test in seconds.

PDULength - Byte length of payload [1 to 255].

PDUType   - Pseudo Random (9 bits)     [0]
          - 01010101                   [1]
          - 00001111                   [2]
          - Pseudo Random (15 bits)    [3]
          - 11111111                   [4]
          - 00000000                   [5]
          - 10101010                   [6]
          - 11110000                   [7]

PHY - Physical radio symbol rate - LE 1M [1]
                                 - LE 2M [2]
Mod - Standard modulation [0]
    - Stable modulation   [1]

LTCOM  - Serial port for the lower tester. User will be prompted to set the port
         in the script if it is not set in the arguments.

DUTCOM - Serial port for the device under test. User will be prompted to set the 
         port in the script if it is not set in the arguments.

Logging - Toggle logging to be on or off [0 or 1].


================================================================================
Copyright (c) 2016-2017 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).