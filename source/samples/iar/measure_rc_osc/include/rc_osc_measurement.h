/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 * ----------------------------------------------------------------------------
 * rc_osc_measurements.h
 * - Header file for RC oscillator frequency measurements
 * ----------------------------------------------------------------------------
 * $Revision: 1.3 $
 * $Date: 2018/02/27 15:29:33 $
 * ------------------------------------------------------------------------- */

#ifndef RC_OSC_MEASUREMENT_H
#define RC_OSC_MEASUREMENT_H

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/

/* Number of measurement samples needed for calculating
 * the average frequency of RC oscillator */
#define RC_OSC_MEASUREMENTS_SAMPLES     1000

/* ----------------------------------------------------------------------------
 * Global variables and types
 * --------------------------------------------------------------------------*/

/* Parameters for RC oscillator period measurements */
typedef struct
{
    bool enabled;
    int32_t SYSCLK_periods;
    uint64_t SYSCLK_periods_sum;
    uint16_t sample_cnt;
    int32_t avg_freq;
} rc_osc_measurements_t;

/* RC oscillator measurements parameters */
extern rc_osc_measurements_t rc_osc_measurements;

/* ----------------------------------------------------------------------------
 * Function prototype definitions
 * --------------------------------------------------------------------------*/
void RC_OSC_Initialize(void);

void AUDIOSINK_PERIOD_IRQHandler(void);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif    /* RC_OSC_MEASUREMENT_H */
