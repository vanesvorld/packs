/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_hrps.h
 * - HRPS Application-specific header file
 * ------------------------------------------------------------------------- */

#ifndef INCLUDE_APP_HRPS_H_
#define INCLUDE_APP_HRPS_H_

/* ----------------------------------------------------------------------------
 * If building with a C++ compiler, make all of the definitions in this header
 * have a C binding.
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C"
{
#endif    /* ifdef __cplusplus */

/* ----------------------------------------------------------------------------
 * Defines
 * --------------------------------------------------------------------------*/
/** Energy expended notification timeout in seconds. */
#define APP_HRPS_ENERGY_UPDATE_TIMEOUT_S     10

/* ----------------------------------------------------------------------------
 * Function prototypes
 * --------------------------------------------------------------------------*/

int APP_HRPS_Initialize(void);

const struct HRPS_Measure_t *APP_HRPS_HeartRateMeasurementUpdate(void);

void APP_HRPS_EnergyExpResetInd(void);

void APP_HRPS_MsgHandler(ke_msg_id_t const msg_id, void const *param,
        ke_task_id_t const dest_id, ke_task_id_t const src_id);

/* ----------------------------------------------------------------------------
 * Close the 'extern "C"' block
 * ------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif    /* ifdef __cplusplus */

#endif /* INCLUDE_APP_HRPS_H_ */
