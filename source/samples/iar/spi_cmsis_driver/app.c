/* ----------------------------------------------------------------------------
 * Copyright (c) 2019 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 *  - This application demonstrates the use SPI CMSIS-Driver to simultaneously
 *    operate SPI0 and SPI1. The example configures SPI0 as master and SPI1 as
 *    slave. A new transfer starts by the master (SPI0) when the button (DIO5)
 *    is pressed.
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>


/* Global variables */
DRIVER_GPIO_t *gpio;
ARM_DRIVER_SPI *spi0;
ARM_DRIVER_SPI *spi1;

uint8_t spi0_tx_buffer[] __attribute__ ((aligned(4))) = "RSL10 SPI TEST";
uint8_t spi1_tx_buffer[] __attribute__ ((aligned(4))) = "RSL10 SPI TEST";
uint8_t spi0_rx_buffer[sizeof(spi0_tx_buffer)] __attribute__ ((aligned(4)));
uint8_t spi1_rx_buffer[sizeof(spi0_tx_buffer)] __attribute__ ((aligned(4)));
size_t buff_size = sizeof(spi0_tx_buffer);

static bool ignore_next_dio_int = false;

/* ----------------------------------------------------------------------------
 * Function      : void Button_EventCallback(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : Interrupt handler triggered by a button press. Cancel any
 *                 ongoing transfers, switch to Master mode and start a
 *                 MasterTransmit operation through spi.
 * Inputs        : event - event number which triggered the callback
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Button_EventCallback(uint32_t event)
{
    /* Check if this interrupt should be ignored */
    if (!ignore_next_dio_int)
    {
        /* Check if expected interrupt occured */
        if (event == GPIO_EVENT_0_IRQ)
        {
            /* Button is pressed: Ignore next interrupt.
             * This is required to deal with the debounce circuit limitations. */
            ignore_next_dio_int = true;

            PRINTF("BUTTON PRESSED: START TRANSMISSION\n");
            /* Activate SSEL line and start transfer on SPI0/master */
            spi0->Control(ARM_SPI_CONTROL_SS, ARM_SPI_SS_ACTIVE);
            spi0->Transfer(spi0_tx_buffer, spi0_rx_buffer, buff_size);
        }
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void ToggleLed(uint32_t n, uint32_t delay_ms)
 * ----------------------------------------------------------------------------
 * Description   : Toggle the led pin.
 * Inputs        : n        - number of toggles
 *               : delay_ms - delay between each toggle [ms]
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void ToggleLed(uint32_t n, uint32_t delay_ms)
{
    for (; n > 0; n--)
    {
        /* Refresh the watchdog */
        Sys_Watchdog_Refresh();

        /* Toggle led diode */
        gpio->ToggleValue(LED_DIO);

        /* Delay */
        Sys_Delay_ProgramROM((delay_ms / 1000.0) * SystemCoreClock);
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI0_Master_CallBack(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a callback registered by the function
 *                 Initialize, for SPI0. The parameter event indicates one or
 *                 more events that occurred during driver operation.
 * Inputs        : event    -    SPI Events notification mask
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SPI0_Master_CallBack(uint32_t event)
{
    /* Check event */
    switch (event)
    {
        /* Transfer complete event */
        case ARM_SPI_EVENT_TRANSFER_COMPLETE:
        {
            /* Blink LED if received data matches SPI_TX_DATA */
            if (!strcmp((const char *)spi1_tx_buffer, (const char *)spi0_rx_buffer))
            {
                /* Toggle LED state 2 times for 500 milliseconds */
                ToggleLed(2, 500);
                PRINTF("LED BLINKED: MASTER_TRANSFER_COMPLETE\n");
            }

            /* Deactivate SSEL line */
            spi0->Control(ARM_SPI_CONTROL_SS, ARM_SPI_SS_INACTIVE);
        }
        break;

        /* Data lost event */
        case ARM_SPI_EVENT_DATA_LOST:
        {
            /* Toggle LED state 10 times for 50 milliseconds */
            ToggleLed(10, 50);
            PRINTF("MASTER_ARM_SPI_EVENT_DATA_LOST\n");
            /* Abort current transfer */
            spi0->Control(ARM_SPI_ABORT_TRANSFER, 0);
        }
        break;
    }

    /* Enable next interrupt */
    ignore_next_dio_int = false;
}

/* ----------------------------------------------------------------------------
 * Function      : void SPI1_Slave_CallBack(uint32_t event)
 * ----------------------------------------------------------------------------
 * Description   : This function is a callback registered by the function
 *                 Initialize, for SPI1. The parameter event indicates one or
 *                 more events that occurred during driver operation.
 * Inputs        : event    -    SPI Events notification mask
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void SPI1_Slave_CallBack(uint32_t event)
{
    /* If the application is running on a single board, disable the next dio interrupt
     * during slave callback execution */
    ignore_next_dio_int = true;

    switch (event)
    {
        /* Transfer complete event */
        case ARM_SPI_EVENT_TRANSFER_COMPLETE:
        {
            /* Blink LED if received data matches SPI_TX_DATA */
            if (!strcmp((const char *)spi0_tx_buffer, (const char *)spi1_rx_buffer))
            {
                /* Toggle LED state 2 times for 500 milliseconds */
                ToggleLed(2, 500);
                PRINTF("LED BLINKED: SLAVE_TRANSFER_COMPLETE\n");
            }
        }
        break;

        /* Data lost event */
        case ARM_SPI_EVENT_DATA_LOST:
        {
            /* Toggle LED state 10 times for 50 milliseconds */
            ToggleLed(10, 50);
            PRINTF("SLAVE_ARM_SPI_EVENT_DATA_LOST\n");
            /* Abort current transfer */
            spi1->Control(ARM_SPI_ABORT_TRANSFER, 0);
        }
        break;
    }

    /* Get ready for next transfer (slave) */
    spi1->Transfer(spi1_tx_buffer, spi1_rx_buffer, buff_size);

    /* Enable next interrupt */
    ignore_next_dio_int = false;
}

/* ----------------------------------------------------------------------------
 * Function      : void Initialize(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system by disabling interrupts, switching to
 *                 the 48 MHz clock.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void Initialize(void)
{
    /* Mask all interrupts */
    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

    /* Disable all existing interrupts, clearing all pending source */
    Sys_NVIC_DisableAllInt();
    Sys_NVIC_ClearAllPendingInt();

    /* Test DIO12 to pause the program to make it easy to re-flash */
    DIO->CFG[RECOVERY_DIO] = DIO_MODE_INPUT  | DIO_WEAK_PULL_UP |
                             DIO_LPF_DISABLE | DIO_6X_DRIVE;
    while (DIO_DATA->ALIAS[RECOVERY_DIO] == 0);

    /* Prepare the 48 MHz crystal
     * Start and configure VDDRF */
    ACS_VDDRF_CTRL->ENABLE_ALIAS = VDDRF_ENABLE_BITBAND;
    ACS_VDDRF_CTRL->CLAMP_ALIAS  = VDDRF_DISABLE_HIZ_BITBAND;

    /* Wait until VDDRF supply has powered up */
    while (ACS_VDDRF_CTRL->READY_ALIAS != VDDRF_READY_BITBAND);

    /* Enable RF power switches */
    SYSCTRL_RF_POWER_CFG->RF_POWER_ALIAS   = RF_POWER_ENABLE_BITBAND;

    /* Remove RF isolation */
    SYSCTRL_RF_ACCESS_CFG->RF_ACCESS_ALIAS = RF_ACCESS_ENABLE_BITBAND;

    /* Start the 48 MHz oscillator without changing the other register bits */
    RF->XTAL_CTRL = ((RF->XTAL_CTRL & ~XTAL_CTRL_DISABLE_OSCILLATOR) |
                     XTAL_CTRL_REG_VALUE_SEL_INTERNAL);

    /* Enable 48 MHz oscillator divider at desired prescale value */
    RF_REG2F->CK_DIV_1_6_CK_DIV_1_6_BYTE = CK_DIV_1_6_PRESCALE_1_BYTE;

    /* Wait until 48 MHz oscillator is started */
    while (RF_REG39->ANALOG_INFO_CLK_DIG_READY_ALIAS !=
           ANALOG_INFO_CLK_DIG_READY_BITBAND);

    /* Switch to (divided 48 MHz) oscillator clock */
    Sys_Clocks_SystemClkConfig(JTCK_PRESCALE_1   |
                               EXTCLK_PRESCALE_1 |
                               SYSCLK_CLKSRC_RFCLK);

    /* Initialize gpio structure */
    gpio = &Driver_GPIO;

    /* Initialize gpio driver */
    gpio->Initialize(Button_EventCallback);

    /* Stop masking interrupts */
    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
    __set_FAULTMASK(FAULTMASK_ENABLE_INTERRUPTS);

    /* Initialize spi0 driver structure */
    spi0 = &Driver_SPI0;

    /* Initialize spi1 driver structure */
    spi1 = &Driver_SPI1;

    /* Initialize spi0, register callback function */
    spi0->Initialize(SPI0_Master_CallBack);

    /* Initialize spi1, register callback function */
    spi1->Initialize(SPI1_Slave_CallBack);

    printf_init();
}

/* ----------------------------------------------------------------------------
 * Function      : int main(void)
 * ----------------------------------------------------------------------------
 * Description   : Initialize the system, then send an I2C frame controlled
 *                 on button press.
 * Inputs        : None
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
int main(void)
{
    int8_t i;

    /* Initialize the clocks and configure button press interrupt */
    Initialize();
    PRINTF("DEVICE INITIALIZED\n");

    /* Get ready for next transfer (slave) */
    spi1->Transfer(spi1_tx_buffer, spi1_rx_buffer, buff_size);

    /* Spin loop */
    while (true)
    {
        /* Here we demonstrate the ABORT_TRANSFER feature. Wait for few first bytes
         * to be transferred and if particular bytes does not match abort the transfer. */
        i = spi1->GetDataCount() - BUFFER_OFFSET;
        if ((i >= 0) && (spi1_rx_buffer[i] != spi1_tx_buffer[i]))
        {
            /* Toggle LED state 6 times for 50 milliseconds */
            ToggleLed(6, 50);

            /* Abort current transfer */
            spi1->Control(ARM_SPI_ABORT_TRANSFER, 0);

            /* Get ready for next transfer (slave) */
            spi1->Transfer(spi1_tx_buffer, spi1_rx_buffer, buff_size);
        }

        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();
    }
}
