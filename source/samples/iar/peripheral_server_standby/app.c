/* ----------------------------------------------------------------------------
 * Copyright (c) 2017 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * This code is the property of ON Semiconductor and may not be redistributed
 * in any form without prior written permission from ON Semiconductor.
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 * ----------------------------------------------------------------------------
 * app.c
 * - Main application file
 * ----------------------------------------------------------------------------
 * $Revision: 1.44 $
 * $Date: 2019/12/23 20:34:41 $
 * ------------------------------------------------------------------------- */

#include "app.h"
#include <printf.h>

extern volatile bool wakeup_from_standby;

extern volatile bool cs_ntf_send_flag;

int main(void)
{
    /* Initialize the system */
    App_Initialize();

    /* Debug/trace initialization. In order to enable UART or RTT trace,
     * configure the 'OUTPUT_INTERFACE' macro in printf.h */
    printf_init();
    PRINTF("__peripheral_server_standby has started!\n");

    /* Wait for 3 seconds to allow re-flashing directly after pressing RESET */
    Sys_Delay_ProgramROM(3 * SystemCoreClock);

    /* Configure the standby mode */
    Standby_Mode_Configure(&standby_mode_env);

    /* Main application loop:
     * - Run the kernel scheduler
     * - Send notifications for the battery voltage and RSSI values
     * - Refresh the watchdog and wait for an interrupt before continuing */
    while (true)
    {
        /* Run the kernel scheduler */
        Kernel_Schedule();

        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* If RC oscillator is being used, occasionally measure and update its
         * period. */

        /* If a customer service data notification is to be sent */
        if ((ble_env.state == APPM_CONNECTED) &&
            (cs_env.tx_cccd_value & ATT_CCC_START_NTF) &&
            (cs_env.sentSuccess == 1) &&
            (cs_ntf_send_flag == true))
        {
            /* Emulate a change in custom service characteristics */
            cs_env.val_notif++;
            memset(cs_env.tx_value, cs_env.val_notif,
                                    APP_CS_TX_VALUE_NOTF_LENGTH);

            PRINTF("__SEND TX VALUE %d\n",cs_env.tx_value[0]);
            /* Send notification */
            CustomService_SendNotification(ble_env.conidx,
                                           CS_IDX_TX_VALUE_VAL,
                                           &cs_env.tx_value[0],
                                           APP_CS_TX_VALUE_NOTF_LENGTH);

            /* Reset the send success flag */
            cs_env.sentSuccess = 0;

            /* Indicate that notification was sent */
            cs_ntf_send_flag = false;
        }

        /* If not in the middle of a period measurement for RSOSC, allow the
         * application to go to standby power mode. */
        if (allow_standby || (RTC_CLK_SRC != RTC_CLK_SRC_RC_OSC))
        {
        	/* Mark the entry of power mode */
        	Sys_GPIO_Set_Low(LED_DIO);

            GLOBAL_INT_DISABLE();
            BLE_Power_Mode_Enter(&standby_mode_env, POWER_MODE_STANDBY);
            GLOBAL_INT_RESTORE();

            /* Mark the exit of power mode */
            Sys_GPIO_Set_High(LED_DIO);

            /* If WAKEUP_IRQ has just executed
             * (i.e., the system has just waken up from standby) */
            if (wakeup_from_standby)
            {
                /* Mask all interrupts */
                __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);
                while (!(BLE_Is_Awake()))
                {
                    WFI();

                    /* Process interrupt */
                    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
                    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);

                }

                /* Stop masking interrupts */
                __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);

                wakeup_from_standby = false;
            }

            /* If WAKEUP_IRQ has NOT executed but the BB is currently in sleep mode
             * (i.e., the standby entering process is aborted by external DIO wakeup signal) */
            else if (!(BLE_Is_Awake()))
            {
                NVIC_ClearPendingIRQ(WAKEUP_IRQn);
                NVIC_DisableIRQ(WAKEUP_IRQn);

                if (VDDPA_enable)
                {
                    ACS_VDDPA_CTRL->VDDPA_SW_CTRL_ALIAS = VDDPA_SW_HIZ_BITBAND;
                    ACS_VDDPA_CTRL->ENABLE_ALIAS = VDDPA_ENABLE_BITBAND;
                }

                Sys_PowerModes_Standby_Wakeup(&standby_mode_env);

                /* Force to wakeup BB */
                BBIF->CTRL |= (BB_WAKEUP);
                for (int i = 0; i < 300; i++)
                {
                    asm volatile ("nop");
                }
                BBIF->CTRL &= (~BB_WAKEUP);

               /* Mask all interrupts */
                __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);
                while (!(BLE_Is_Awake()))
                {
                    WFI();

                    /* Process interrupt */
                    __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);
                    __set_PRIMASK(PRIMASK_DISABLE_INTERRUPTS);
                }

                /* Stop masking interrupts */
                __set_PRIMASK(PRIMASK_ENABLE_INTERRUPTS);

                NVIC_ClearPendingIRQ(WAKEUP_IRQn);
                NVIC_EnableIRQ(WAKEUP_IRQn);
            }
        }

        /* If DIO wake-up event has occurred */
        if (DIO_wakeup_flag)
        {
            /* Things to do in this event */

            DIO_wakeup_flag = 0;
        }

        /* Wait for an interrupt before executing the scheduler again */
        SYS_WAIT_FOR_EVENT;
    }
    return (0);
}
